/**
 * 
 */
package fr.ge.common.nash.plugin.transitions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Transition class.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class Transition {

    /** Tag transition as root **/
    private boolean isRoot;

    /** The name. **/
    private String name;

    /** Parents transitions. **/
    private List<Transition> parents = new ArrayList<>();

    /** Childs transitions. **/
    private Map<Transition, String> childs = new HashMap<>();

    /**
     * 
     * Constructeur de la classe.
     *
     * @param name
     *            The name
     * @param isRoot
     *            Root transition
     */
    public Transition(final String name, final boolean isRoot) {
        this.name = name;
        this.isRoot = isRoot;
    }

    /**
     * 
     * Constructeur de la classe.
     *
     * @param name
     */
    public Transition(final String name) {
        this(name, false);
    }

    /**
     * Accesseur sur l'attribut {@link #name}.
     *
     * @return String name
     */
    public boolean isRoot() {
        return isRoot;
    }

    /**
     * Accesseur sur l'attribut {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * Mutateur sur l'attribut {@link #name}.
     *
     * @param name
     *            la nouvelle valeur de l'attribut name
     */
    public Transition setName(final String name) {
        this.name = name;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #parents}.
     *
     * @return the parents
     */
    public List<Transition> getParents() {
        return parents;
    }

    /**
     * Accesseur sur l'attribut {@link #childs}.
     *
     * @return the childs
     */
    public Map<Transition, String> getChilds() {
        return childs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        final Transition transition = (Transition) o;
        return this.name.equals(transition.getName());
    }

    /**
     * Get a child transition.
     * 
     * @param child
     *            The child transition name.
     * @return a transition
     */
    public Transition get(final String child) {
        if (this.getName().equals(child)) {
            return this;
        }
        return this.getChilds().keySet().stream().filter(t -> t.getName().equals(child)).findFirst().orElse(null);
    }

    /**
     * Add child transition.
     * 
     * @param parent
     *            the parent transition
     * @param condition
     *            the condition
     * @return this
     */
    public Transition addChild(final Transition child, final String condition) {
        if (!this.getChilds().keySet().contains(child)) {
            this.getChilds().put(child, condition);
        }
        return this;
    }

    /**
     * Add parent transition.
     * 
     * @param parent
     *            the parent transition
     * @return this
     */
    public Transition addParent(final Transition parent) {
        if (!this.getParents().contains(parent)) {
            this.getParents().add(parent);
        }
        return this;
    }

    public Transition add(final String parent, final String child, final String condition) {
        // -->Find parent transition
        Transition transitionParent = this.get(parent);
        boolean newParent = false;
        if (null == transitionParent) {
            transitionParent = new Transition(parent);
            newParent = true;
        }

        Transition transitionChild = this.get(child);
        boolean newChild = false;
        if (null == transitionChild) {
            transitionChild = new Transition(child);
            newChild = true;
        }

        transitionChild.addParent(transitionParent);
        transitionParent.addChild(transitionChild, condition);

        if (newParent) {
            this.addChild(transitionParent, condition);
        }
        if (newChild) {
            this.addChild(transitionChild, condition);
        }

        return transitionChild;
    }
}
