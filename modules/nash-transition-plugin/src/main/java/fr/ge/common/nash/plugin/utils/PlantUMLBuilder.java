/**
 * 
 */
package fr.ge.common.nash.plugin.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.plugin.transitions.Transition;

/**
 * Custom PlantUML activity diagrams builder.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class PlantUMLBuilder {

    private static final String CONDITION_IF_ELSE = //
            "if \"%s\" then\n" + //
                    "\t-->[true] \"%s\"\n" + //
                    "else\n" + //
                    "\t-->_ELSE_\n" + //
                    "endif\n\n";

    private static final String CONDITION_IF_ELSE_ARROW = //
            "\"%s\" --> " + //
                    CONDITION_IF_ELSE;

    /**
     * 
     * Constructeur de la classe.
     *
     */
    private PlantUMLBuilder() {

    }

    /**
     * Print PlantUML activity diagram.
     * 
     * @param root
     *            The root transition
     * @return the PlantUML activity diagram as string.
     */
    public static String print(final Transition root) {
        final StringBuilder result = new StringBuilder();
        result.append("~~~plant-uml\n");
        if (root.isRoot()) {
            result.append("@startuml\n");
            result.append("(*) --> \"" + root.getName() + "\"\n");
        }
        result.append(print(root, new ArrayList<Transition>()));
        if (root.isRoot()) {
            if (root.getChilds().isEmpty()) {
                result.append("\"" + root.getName() + "\" --> (*)\n");
            } else {
                root.getChilds().keySet().stream() //
                        .forEach(c -> {
                            if (null != c.getParents().stream().filter(p -> p.getName().equals(root.getName())).findFirst().orElse(null)) {
                                result.append("\"" + root.getName() + "\" --> \"" + c.getName() + "\"\n");
                            }
                            if (c.getChilds().isEmpty()) {
                                result.append("\"" + c.getName() + "\" --> (*)\n\n");
                            }
                        });
            }
            result.append("@enduml\n");
            result.append("~~~\n");
        }
        return result.toString();
    }

    /**
     * Print partial PlantUML activity diagram.
     * 
     * @param root
     *            The root transition
     * @param executed
     *            The list of transition already executed
     * @return
     */
    private static String print(final Transition root, final List<Transition> executed) {
        final StringBuilder result = new StringBuilder();
        root.getChilds().keySet().stream() //
                .forEach(c -> {
                    if (null == executed.stream().filter(p -> p.getName().equals(c.getName())).findFirst().orElse(null)) {
                        if (null != c.getParents().stream().filter(p -> p.getName().equals(root.getName())).findFirst().orElse(null)) {
                            if (c.getChilds().size() == 1) {
                                result.append(String.format("\"%s\" --> \"%s\"\n\n", c.getName(), c.getChilds().keySet().iterator().next().getName()));
                            } else if (c.getChilds().size() == 2) {
                                Object[] keys = c.getChilds().keySet().toArray();

                                final Transition transition1 = (Transition) keys[0];
                                final Transition transition2 = (Transition) keys[1];

                                final String ifelse = String.format(CONDITION_IF_ELSE_ARROW, c.getName(), Optional.ofNullable(c.getChilds().get(transition1)).orElse(StringUtils.EMPTY),
                                        transition1.getName(), transition2.getName());
                                result.append(ifelse.toString().replace("_ELSE_", "[false] \"" + transition2.getName() + "\""));
                            } else {
                                Object[] keys = c.getChilds().keySet().toArray();

                                final List<String> conditions = new ArrayList<>();
                                for (int idx = 0; idx < keys.length - 1; idx++) {
                                    final Transition transition = (Transition) keys[idx];
                                    conditions.add(String.format(CONDITION_IF_ELSE, c.getChilds().get(transition), transition.getName()));
                                }

                                StringBuilder str = new StringBuilder();
                                for (int idx = conditions.size() - 1; idx >= 0; idx--) {
                                    final String cond = conditions.get(idx);
                                    if (StringUtils.isEmpty(str.toString())) {
                                        str = new StringBuilder(String.format("\"%s\" --> ", c.getName()));
                                        str.append(cond);
                                    }
                                    if ((idx - 1) >= 0) {
                                        final String subCond = conditions.get(idx - 1);
                                        str = new StringBuilder(str.toString().replace("_ELSE_", subCond));
                                    }
                                }

                                if (keys.length > 0) {
                                    final Transition transition = (Transition) keys[keys.length - 1];
                                    result.append(str.toString().replace("_ELSE_", "\"" + transition.getName() + "\""));
                                }
                            }
                            executed.add(c);
                            result.append(print(c, executed));
                        }
                    }
                });
        return result.toString();
    }
}
