/**
 *
 */
package fr.ge.common.nash.plugin.transitions;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.ge.common.nash.plugin.utils.PlantUMLBuilder;
import fr.ge.common.nash.plugin.utils.XPathBuilder;
import fr.ge.common.utils.plugin.pdf.MarkdownToPdf;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Mojo(name = "transition-nash", defaultPhase = LifecyclePhase.COMPILE)
public class TransitionPlugin extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    @Parameter(property = "scope")
    private String scope;

    @Parameter(required = true, alias = "transition.folder")
    private String transitionFolder;

    @Parameter(required = true, alias = "transition.root")
    private String transitionRoot;

    @Parameter(required = true, alias = "transition.output")
    private String transitionOutput;

    private static final Pattern TRANSITION_NAME = Pattern.compile("^(.*)(transition)[^\\/]([a-zA-Z]*\\-[0-9]\\.[0-9])$");

    private static final Pattern TRANSITION_WITH_POSTPROCESS = Pattern.compile("^(.*)(transition)[^\\/](.*)[^\\/](files)(.*)(postprocess\\.xml)$");

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        this.getLog().info(String.format("Absolute path : %s", this.project.getBasedir().getAbsolutePath()));
        this.getLog().info(String.format("Transition folder : %s", this.transitionFolder));
        this.getLog().info(String.format("Transition root : %s", this.transitionRoot));
        this.getLog().info(String.format("Transition output : %s", this.transitionOutput));

        final Path outputDir = Paths.get(String.join(File.separator, this.project.getBasedir().getAbsolutePath(), this.transitionOutput));
        if (outputDir.toFile().exists()) {
            try {
                FileUtils.deleteDirectory(outputDir.toFile());
            } catch (IOException e) {
                this.getLog().error(e);
                throw new IllegalStateException("Cannot remove output directory", e);
            }
        }
        outputDir.toFile().mkdirs();

        final MarkdownToPdf generator = new MarkdownToPdf().setLog(this.getLog());

        final List<String> remainingTransitions = this.getAllTransitions();
        Arrays.asList(this.transitionRoot.split(";")).stream().forEach(root -> {
            try {
                final Transition rootTransition = new Transition(root, true);
                Files.walk(Paths.get(String.join(File.separator, this.project.getBasedir().getAbsolutePath(), this.transitionFolder))) //
                        .filter(path -> !Files.isDirectory(path)) //
                        .forEach(path -> {
                            final Matcher matcherTransitionPostProcess = TRANSITION_WITH_POSTPROCESS.matcher(path.toFile().getAbsolutePath());
                            if (matcherTransitionPostProcess.matches() && StringUtils.isNotEmpty(matcherTransitionPostProcess.group(3))) {
                                final String parent = matcherTransitionPostProcess.group(3).replace("@", "-");

                                try {
                                    final Document document = XPathBuilder.buildDocument(new FileInputStream(path.toFile().getAbsolutePath()));
                                    final NodeList transitionList = XPathBuilder.queryNodeList(document, "//processes/process[@type='transition']");
                                    for (int idx = 0; idx < transitionList.getLength(); idx++) {
                                        final Node transitionNode = transitionList.item(idx);

                                        final String child = Optional.ofNullable(XPathBuilder.queryString(transitionNode, "@ref")) //
                                                .filter(StringUtils::isNotEmpty) //
                                                .map(ref -> ref.replace("@", "-")) //
                                                .orElse(null);
                                        final String condition = Optional.ofNullable(XPathBuilder.queryString(transitionNode, "@if")) //
                                                .filter(StringUtils::isNotEmpty) //
                                                .orElse(null);

                                        rootTransition.add(parent, child, condition);
                                        remainingTransitions.remove(parent);
                                        remainingTransitions.remove(child);
                                    }
                                } catch (Exception e) {
                                    // -->Do nothing here
                                }
                            }
                        });
                final String markdown = PlantUMLBuilder.print(rootTransition);
                final Path outputPath = Paths.get(String.join(File.separator, outputDir.toFile().getAbsolutePath()), root + ".pdf");

                generator.generate(new ByteArrayInputStream(markdown.toString().getBytes(StandardCharsets.UTF_8)), new FileOutputStream(outputPath.toFile()), null);

            } catch (Exception e) {
                this.getLog().error(e);
                throw new IllegalStateException("Cannot generate PlantUML activity diagram", e);
            }
        });

        remainingTransitions.stream().forEach(root -> {
            try {
                final String markdown = PlantUMLBuilder.print(new Transition(root, true));
                final Path outputPath = Paths.get(String.join(File.separator, outputDir.toFile().getAbsolutePath()), root + ".pdf");

                generator.generate(new ByteArrayInputStream(markdown.toString().getBytes(StandardCharsets.UTF_8)), new FileOutputStream(outputPath.toFile()), null);
            } catch (Exception e) {
                this.getLog().error(e);
                throw new IllegalStateException("Cannot generate PlantUML activity diagram", e);
            }
        });
    }

    private List<String> getAllTransitions() {
        try {
            return Files.walk(Paths.get(String.join(File.separator, this.project.getBasedir().getAbsolutePath(), this.transitionFolder))) //
                    .filter(path -> Files.isDirectory(path)) //
                    .map(path -> {
                        final Matcher matcherTransition = TRANSITION_NAME.matcher(path.toFile().getAbsolutePath());
                        if (matcherTransition.matches() && StringUtils.isNotEmpty(matcherTransition.group(3))) {
                            final String transitionName = matcherTransition.group(3);
                            return transitionName;
                        }
                        return null;
                    }) //
                    .filter(Objects::nonNull) //
                    .collect(Collectors.toList());
        } catch (Exception e) {
            this.getLog().error(e);
            throw new IllegalStateException("Cannot read transition directory", e);
        }
    }
}
