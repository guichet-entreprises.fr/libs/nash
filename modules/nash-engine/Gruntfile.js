/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated 
 * with loading,  using,  modifying and/or developing or reproducing the 
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore 
 * encouraged to load and test the software's suitability as regards their 
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */ 
'use strict';

var path = require('path');

var project = {
        build: {
            directory: 'target/classes',
            outputDirectory: 'target/classes',
            sourceDirectory: 'src/main/resources'
        }
};

/**
 * Build CSS file item.
 * 
 * @param basePath source base path
 * @param name resource name
 * @returns file item
 */
function buildStyle(basePath, name) {
    return {
        src: [ basePath + '/' + name ],
        dest: '<%= project.build.outputDirectory %>/css/libs/' + path.basename(basePath) + '/' + path.basename(name).replace(/\.[^.]+$/, '.css')
    }
}


function extend() {
    var sources = Array.from(arguments);
    var dst = {}, src;

    if (1 === sources.length && Array.isArray(sources[0])) {
        sources = sources[0];
    }

    while (src = sources.shift()) {
        Object.keys(src).forEach(function (key) {
            dst[key] = src[key];
        });
    }
    return dst;
}



var overrides = {
    'handlebars': {
        'main': 'dist/handlebars.js'
    }
};



module.exports = function(grunt) {

    const widgetsBaseDir = '/fr/ge/common/nash/engine/adapter';

    var pkg = grunt.file.readJSON('package.json');

    var widgetsStyles = grunt.file.expandMapping([ '**/*.less', '**/*.css', '!mixins/**' ], '<%= project.build.outputDirectory %>' + widgetsBaseDir, {
        cwd: project.build.sourceDirectory + widgetsBaseDir
    }).map(function (elm) {
        return {
            src: elm.src,
            dest: elm.dest.replace(/\.[^.]+$/, '.css')
        };
    });


    var widgets = (function () {

        var widgetsByVersions = {};

        grunt.file.expand( //
                { cwd: project.build.sourceDirectory + widgetsBaseDir }, //
                [ '*/*/script.js', '*/*/style.css', '*/*/style.less' ] //
            ).forEach(src => {
                var m  = /^([^/]+)\/([^/]+)ValueAdapterResources\/[^/]+(?:\.([^.]+))$/.exec(src);
                if (m) {
                    var version = m[1].replace(/^v/, '').replace(/_+/g, '.');
                    var name = null == m[2] ? null : m[2].toLowerCase();
                    var resourceType = m[3];

                    var widgetsByName = widgetsByVersions[version] || {};
                    var widgetResources = widgetsByName[name] || { name: name };

                    widgetResources[resourceType] = project.build.sourceDirectory + widgetsBaseDir + '/' + src;

                    widgetsByName[name] = widgetResources;
                    widgetsByVersions[version] = widgetsByName;
                }
            });

        var widgetsVersions = Object.keys(widgetsByVersions);
        widgetsVersions.sort((a, b) => {
            var sa = a.split('.').map(elm => parseInt(elm)), //
                sb = b.split('.').map(elm => parseInt(elm));
            var idx, max = Math.max(sa.length, sb.length);

            for (idx = 0; idx < max; idx++) {
                if (sa[idx] != sb[idx]) {
                    return sa[idx] - sb[idx];
                }
            }

            if (sa.length < sb.length) {
                return -1;
            } else if (sb.length < sa.length) {
                return 1;
            } else {
                return 0;
            }
        });
        widgetsVersions.push('latest');

        function getProcess(ver) {
            return  (src, filepath) => src.replace(/^(define)\(/gm, '$1(\'' + filepath.replace(/^.*\/([^/]+)ValueAdapterResources\/[^/]+$/, (str, name) => 'rest/type/' + ver + '/' + name.toLowerCase() + '/script') + '\', ');
        }

        var scripts = {};
        var styles = {};
        var latest = {};

        widgetsVersions.forEach(version => {
            for (var name in widgetsByVersions[version]) {
                latest[name] = widgetsByVersions[version][name];
            }
            let elms = Object.values(latest).filter(elm => null != elm.js);
            scripts['adapters-' + version] = {
                    options: {
                        process: getProcess(version),
                        footer: "define('_fm_adapters', [ " //
                                + elms.map(elm => "'rest/type/" + version + "/" + elm.name + "/script'").join(', ') //
                                + " ], function (" //
                                + elms.map(elm => elm.name + "ValueAdapter").join(', ') //
                                + ") { return { " //
                                + elms.map(elm => "'" + elm.name + "': " + elm.name + "ValueAdapter").join(', ') //
                                + " }; });"
                    },
                    files: [
                        {
                            src: elms.map(elm => elm.js),
                            dest: '<%= project.build.outputDirectory %>/public/js/engine/adapters-' + version + '.js'
                        }
                    ]
                };
            styles['adapters-' + version] = {
                    files: [
                        {
                            src: Object.values(latest).map(elm => elm.less || elm.css).filter(elm => null != elm),
                            dest: '<%= project.build.outputDirectory %>/public/css/engine/adapters-' + version + '.css'
                        }
                    ]
                };
        });

        return {
            scripts: scripts,
            styles: styles
        };
    })();

    var widgetsVersionedScripts = grunt.file.expand( //
            {}, //
            [ project.build.sourceDirectory + '/fr/ge/common/nash/engine/adapter/*/*/script.js' ] //
        );

    var widgetsLatestScripts = {};
    grunt.file.expand( //
            {}, //
            [ project.build.sourceDirectory + '/fr/ge/common/nash/engine/adapter/*/*/script.js' ] //
        ).forEach(elm => {
            var key = elm.replace(/^.*\/([^/]+)\/([^/]+)ValueAdapterResources\/[^/]+$/, 'rest/type/latest/$2/script');
            widgetsLatestScripts[key] = elm;
        });

    /*
     * Build RequireJS dependencies
     */
    var dependencies = Object.keys(pkg.dependencies || {}) //
        .filter(elm => elm !== 'requirejs') //
        .reduce(function (dst, dep) {
            var path = '';
            var nfo = extend(require('./node_modules/' + dep + '/package.json'), overrides[dep] || {});
            if (nfo.main) {
                if (typeof nfo.main === 'string') {
                    path = nfo.main;
                } else if (nfo.main.find) {
                    path = nfo.main.find(elm => elm.match(/\.js$/));
                } else {
                    return dst;
                }
                dst[dep] = 'node_modules/' + dep + path.replace(/^\.?[\/]*/, '/').replace(/\.js$/, '');
            }
            return dst;
        }, {});

    /*
     * GRUNT configuration
     */
    grunt.initConfig({
        project : project,
        less : extend(widgets.styles, {
            adapters: {
                files: [
                    {
                        expand: true,
                        src: [ '**/*.css', '**/*.less' ],
                        cwd: project.build.sourceDirectory + widgetsBaseDir,
                        dest: project.build.outputDirectory + widgetsBaseDir,
                        ext: '.css',
                        extDot: 'last'
                    }
                ]
            },
            others: {
                files: [
                    {
                        expand: true,
                        src: [ 'public/css/**/*.css', 'public/css/**/*.less' ],
                        cwd: project.build.sourceDirectory,
                        dest: project.build.outputDirectory,
                        ext: '.css',
                        extDot: 'last'
                    }
                ]
            }
        }),
        cssmin: {
            options: {
                level: {
                    1: {
                        all: true,
                        roundingPrecision: false,
                        selectorsSortingMethod: 'standard',
                        specialComments: 0
                    }
                },
                sourceMap: true
            },
            adapters: {
                files: widgetsStyles.map(function (elm) {
                    return {
                        src: elm.dest,
                        dest: elm.dest.replace(/\.([^.]+)$/, '.min.$1')
                    };
                }).concat([
                    {
                        expand: true,
                        src: [ '**/*.css', '!**/*.min.css' ],
                        cwd: '<%= project.build.outputDirectory %>/public/css',
                        dest: '<%= project.build.outputDirectory %>/public/css',
                        ext: '.min.css',
                        extDot: 'last'
                    }
                ])
            }
        },
        concat: widgets.scripts,
        uglify: {
            options: {
                sourceMap: true
            },
            adapters: {
                files: [
                    {
                        expand: true,
                        src: [ '<%= project.build.outputDirectory %>/public/js/engine/adapters-*.js' ],
                        ext: '.min.js',
                        extDot: 'last'
                    }
                ]
            }
        },
        requirejs: {
            options: {
                nodeRequire: require,
                baseUrl: '.',
                optimize: 'uglify'
            },
            ext: {
                options: {
                    include: Object.keys(dependencies),
                    paths: dependencies,
                    out: '<%= project.build.outputDirectory %>/fr/ge/common/nash/engine/util/externals.js'
                }
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', [ 'less', 'cssmin', 'concat', 'uglify', 'requirejs' ]);

}
