//check information of receiver

var nbrChannels = _input.result.nbrChannels;
var channels = [];

switch (nbrChannels) {
  case "0":
    channels = _input.result.defaultChannels.channels;
    break;
  default:
    channels = _input.result.activeChannels.channels;
    break;
}

var result = [];

for(i=0; i< channels.length; i++) {
	result.push({
		"id" : channels[i].id,
		"label" : channels[i]
	});
}

var data = nash.instance.load("output.xml")

data.bind("result",{
	funcLabel:_input.result.authority.funcLabel,
    channels : result,
});
