// input functionnal id of authority
var grp = _INPUT_.sendData;


var destFuncId = !grp.funcId ? '' : grp.funcId;
var attachment = !grp.attachment ? '' : grp.attachment;
var channel = !grp.channel ?'' :grp.channel;

_log.info("destFuncId is  {}",destFuncId);

// call directory with funcId to find all information of Authorithy

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

//result			   			   
var receiverInfo = response.asObject();

// prepare all information of receiver to create data.xml
var funcId = !receiverInfo.entityId ? null : receiverInfo.entityId;
var funcLabel = !receiverInfo.label ? null :receiverInfo.label;
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
var tel = !contactInfo.profile.tel ? null : contactInfo.profile.tel;
var codeEDI = !contactInfo.ediCode ? null : contactInfo.ediCode;

_log.info("profile is  {}",contactInfo.profile);

// bloc address
var recipientName = !contactInfo.transferChannels ? null :(!contactInfo.transferChannels.address.addressDetail.recipientName ? null : contactInfo.transferChannels.address.addressDetail.recipientName);
var recipientNameCompl = !contactInfo.transferChannels ? null :(!contactInfo.transferChannels.address.addressDetail.recipientNameCompl ? null : contactInfo.transferChannels.address.addressDetail.recipientNameCompl);
var recipientAddressName = !contactInfo.transferChannels ? null :(!contactInfo.transferChannels.address.addressDetail.addressName ? null : contactInfo.transferChannels.address.addressDetail.addressName);
var recipientAddressNameCompl = !contactInfo.transferChannels ? null :(!contactInfo.transferChannels.address.addressDetail.addressNameCompl ? null : contactInfo.transferChannels.address.addressDetail.addressNameCompl);
var recipientPostalCode = !contactInfo.transferChannels ? null :(!contactInfo.transferChannels.address.addressDetail.postalCode ? null : contactInfo.transferChannels.address.addressDetail.postalCode);
var recipientCity = !contactInfo.transferChannels ? null :(!contactInfo.transferChannels.address.addressDetail.cityName ? null : contactInfo.transferChannels.address.addressDetail.cityName);

// ftp 
var uid = !contactInfo.transferChannels ? null :(!contactInfo.transferChannels.ftp.token ? null : contactInfo.transferChannels.ftp.token);

// email
var funcEmail = !contactInfo.profile.email ? null : contactInfo.profile.email;


var data = nash.instance.load("data-generated.xml");

data.bind("view",{
	search:{
		input:{
			funcId:funcId,
			funcLabel:funcLabel,
			channel:{
				id:channel.id,
				label:channel.label
			}
		},
		output:{
			address:{
				recipientName:recipientName,
				recipientNameCompl:recipientNameCompl,
				recipientAddressName:recipientAddressName,
				recipientAddressNameCompl:recipientAddressNameCompl,
				recipientPostalCode:recipientPostalCode,
				recipientCity:recipientCity
			},
			ftp:{
				uid:uid
			},
			email:{
				funcEmail:funcEmail
			},
			backoffice: {
				funcId:funcId,
				funcLabel:funcLabel
			}
		}
	},
	others:{
		attachment:attachment
	}
})
