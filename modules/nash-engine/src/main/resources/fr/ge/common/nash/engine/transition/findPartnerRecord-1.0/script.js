//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'findPartnerRecord',
    label : "Rechercher les informations d'une autorité",
    preprocess : 'preprocess.xml',
    data : 'display.xml',
    postprocess : 'postprocess.xml',
    icon : "search",
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/postprocess.xml');
step.copy('files/display.xml');
step.copy('files/output.xml');
step.copy('files/find.js');
step.copy('files/next.js');

// ------------------------------------------------------------------------
log.info("Step created");
