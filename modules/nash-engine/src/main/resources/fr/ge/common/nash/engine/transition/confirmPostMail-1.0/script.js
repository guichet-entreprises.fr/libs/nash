// ------------------------------------------------------------------------
// Create transition
// ------------------------------------------------------------------------

var step = _transition.createStep({
    id : 'confirmPostMail',
    label : "Confirmer la demande d'envoi de courrier papier",
    preprocess : 'preprocess.xml',
    postprocess : 'postprocess.xml',
    data : 'display.xml',
    icon : 'envelope-o',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/confirm.js');
step.copy('files/display.xml');
step.copy('files/postprocess.xml');
// ------------------------------------------------------------------------
log.info('Step created');
