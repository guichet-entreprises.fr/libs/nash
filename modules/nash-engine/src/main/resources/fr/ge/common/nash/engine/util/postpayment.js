(function() {

    return root.define("postpayment", function (nash) {

        return function(input) {
            var loader = nash.record;
            var uid = loader.description().getRecordUid();
            var stepId = loader.currentStepPosition();

            log.info('Payment transition for the record {} based on proxy result', uid);

            var status = input.proxyResult.status;
            log.info('Proxy result status : {}', status);

            if (status == 'BLOCKER' || status == 'CRITICAL') {
                // -->Redirect to the dashboard
                return {
                    url : _CONFIG_.get('dashboard.public.url')
                };

            } else if (status == 'TECHNICAL' || status == 'FRAUD') {
                loader.remove('proxy-calling.xml');
                loader.flush();
                // loader.buildEngineContext(stepId).getBaseProvider().remove('payment-calling.xml');
                log.info("Remove proxy calling file");

                log.info("Back to payment step for the record {}", uid);
                return {
                    url : "/record/" + uid + "/" + stepId + "/page/0"
                };
            } else {
                loader.stepsMgr().requestDone(stepId);
                log.info("Go to the next step for the record {}", uid);
                return {
                    url : "/record/" + uid + "/" + (stepId + 1) + "/page/0"
                };
            }
        };

    });

})();