/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/func/renderer', 'lib/fileUpload', 'jquery-ui/widgets/sortable' ], function($, template) {

    function refresh(msg) {
        var data = msg.details;
        var pathAsHtmlId = data.id.replace(/[.\[*\]_]+/g, '-').replace(/-$/, '').replace(/^[^-]+-/, '');
        var placeholder = $('#file-' + pathAsHtmlId + ' .placeholder').empty();
        var render = template('<li id="file-{{parent}}-{{id}}" class="list-group-item"><a href="' + placeholder.data('item-url-template') + '">{{label}}</a></li>');

        $('#hidden-' + pathAsHtmlId).val('');
        data.attachments.forEach(function(attachment, idx) {
            // MAJ the value of input hidden with label and id of attachment.
            // this input hidden is used to make pj mandatory
            $('#hidden-' + pathAsHtmlId).val($('#hidden-' + pathAsHtmlId).val() + "; " + attachment.label + ":" + attachment.id);
            $(render($.extend({
                parent : data.id
            }, attachment))).appendTo(placeholder);
        });
        $('#hidden-' + pathAsHtmlId).parsley().validate();
    }

    var obj = {
        initialize : function(field, opts) {
        	field.find('input[type="file"][data-toggle="file"]').data('ajax', refresh).fileUpload();
            if (opts.readOnly) {
            	field.find('button[role="empty"]').attr('disabled', true);
            }
            field.find('button[role="empty"]').on('click', function(evt) {
                var btn = $(this);
                $.ajax(btn.data('target'), {
                    type : 'post'
                }).done(refresh);
            });
            // initialize the value of input hidden with label and id of attachment previously uploaded 
            $('.placeholder li',field).each(function(idx, li) {
                var m = $(li).attr('id').match(/file-(.*)-([0-9]+)/);
                if (m) {
                	var AttachmentId = m[2];
                	var fieldId = m[1];
                	$('#hidden-'+fieldId).val($('#hidden-'+fieldId).val()+";"+ $('a',li).text()+":" + AttachmentId); 
                }
            });

            field.find('[data-toggle="sortable"]').sortable({
                update : function(evt, ui) {
                    var ul = $(evt.target), lst = [], fld = null;
                    $('> li', ul).each(function(idx, li) {
                        var m = $(li).attr('id').match(/file-(.*)-([0-9]+)/);
                        if (m) {
                        	
                            fld = fld || m[1];
                            lst.push(m[2]);
                        }
                    });

                    if (lst.length > 0) {
                        $.ajax(ul.data('reorder'), {
                            type : 'post',
                            data : {
                                values : lst
                            }
                        }).done(function(data) {
                            // Nothing to do
                            console.log(data);
                        })
                    }
                }
            });
        },
        validate : function(data, opts) {
            return {
            	validated :true,
            	value :  data.asString()
            };
        }
    };

    return obj;
});
