/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/jqueryMaskPlugin', 'lib/jsonReader', '_fm_description', 'lib/i18n' ], function($, jqueryMaskPlugin, jsonReader, description, i18n) {

    var obj = {
        initialize : function(field, opts) {
            var value = field.find('input[name$="val"]');
            var data = field.find('input[name$="data"]');
            var tree = field.find('div[name$="tree"]');
            var button = field.find('button');
            value.mask('000 000 000');
            if (data.val().length > 0) {
                jsonReader.update(tree, data.val());
            }
            button.click(function() {
                updateUiExternal(value, data, tree);
            });
            value.change(function() {
                updateUiExternal(value, data, tree);
            });
        },
        validate : function(data, opts) {
            var value = data.withPath('val').asString().replace(/ /g, '');
            var validated;
            var externalData;
            if (value.length == 0) {
                validated = true;
            } else if (isNaN(parseInt(value)) || value.length != 9) {
                validated = false;
            } else {
                externalData = callExternal(value);
                validated = (externalData && externalData !== '');
            }
            var validation = {
                validated : validated
            };
            if (validation.validated) {
                validation.value = {
                    value : value,
                    data : externalData
                };
            } else {
                validation.message = 'This value does not comply with a SIREN code.';
            }
            return validation;
        }
    };

    function updateUiExternal(value, data, tree) {
        var externalData = callExternal(value.val().replace(/ /g, ''));
        data.val(JSON.stringify(externalData));
        if (externalData && externalData !== '') {
            jsonReader.update(tree, externalData, i18n);
        } else {
            tree.empty();
        }
    }

    var cache = {};
    function callExternal(value) {
        var data = cache[value];
        if (data && data !== '') {
            return data;
        }
        $.ajax({
            'url': baseUrl + 'api/v1_2/widget/SIRENApiEntreprise/' + value,
            'method': 'GET',
            'data': {
                'formTitle': description.title
            },
            'async': false
        }).done(function(result) {
            data = result;
        }).fail(function() {
            data = '';
        });
        cache[value] = data;
        return data;
    }

    return obj;
});
