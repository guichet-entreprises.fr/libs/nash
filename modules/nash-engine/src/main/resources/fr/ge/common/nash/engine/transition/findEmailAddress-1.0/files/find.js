var funcId = _input.result.funcId;

_log.info('===> find email address to "{}"', funcId);

// call directory with funcId to find all information of Authorithy
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', funcId) //
.accept('json') //
.get();

// result
var receiverInfo = response.asObject();
_log.info("receiverInfo is  {}", receiverInfo);

// prepare all information of receiver to create data.xml

var nfo = receiverInfo.details || undefined;
var email = {};

_log.info("nfo is  {}", nfo);

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}

var who = map(receiverInfo, {
    'funcId' : 'entityId', //
    'funcLabel' : 'label' //
});

_log.info("who is  {}", who);

if (nfo !== undefined && nfo.transferChannels && nfo.transferChannels.email && nfo.transferChannels.email.emailAddress) {
    email = nfo.transferChannels.email.emailAddress;
    _log.info("email is  {}", email);
}

var others = [];
var fileId = null;
var filePath = null;
for (var file in _input.parameters.attachment.others) {
	fileId = _input.parameters.attachment.others[file].id;
	filePath = _input.parameters.attachment.others[file].label;
    	
	others.push({
		id:fileId,
		label:filePath
	});
}
var attachment = {
	'cerfa' : _input.parameters.attachment.cerfa,
	'others' : others
};

var data = nash.instance.load("display.xml");
data.bind('request', {
    'input' : who,
    'output' : {
        'email' : email
    },
    'attachment' : attachment
});

_log.info("Finish bind display.xml");