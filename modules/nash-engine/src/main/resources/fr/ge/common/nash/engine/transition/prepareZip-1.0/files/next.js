log.info("==> prepareZip postprocess");

var zipAttachment = _input.request.content.zipAttachment;

var data = nash.instance.load('output.xml');

data.bind("parameters", {
   content : {
	   zipAttachment : zipAttachment
   }
});