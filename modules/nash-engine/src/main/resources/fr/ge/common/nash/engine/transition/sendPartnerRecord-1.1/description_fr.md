# Description

Le but de cette transition est de générer un dossier et l'envoyer dans le Bckoffice partenaires.

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| cerfa | Chemin du fichier cerfa | Oui |
| regent | Chemin du fichier regent | Non |
| others | Prévisualisation des fichiers | Oui |
| xmltc | Chemin du fichier xmlTc | Non |
| funcPath | Chemin de l'authorité compétente | Oui |
| funcLabel | Libellé de l'authorité compétente | Non |

# Résultat(s)

créer un nouveau dossier partenaire et le déposer dans le backoffice