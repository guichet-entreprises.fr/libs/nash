# Description of find functional id transition:
</br>

The goal of  this transition is to find functional id of receiver .

<br />
<br />


# Input of find functional id transition:
<br/>

* To find functional id of receiver we need:
    * authorityType : authority Type (example :"Conseil en propriété industrielle") 
    * zipCode : String (example :"92000")
    * attachement Path : "/3-review/generated/generated.formulaire-1-Conseil en propriete industrielle.pdf"

<br/>
<br/>


# Output of find functional id transition: 
<br/>

Create data.xml with functional id of authority and path of attachment to send.  


