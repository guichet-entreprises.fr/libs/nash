//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'sendToAuthority',
    label : 'Préparer la transmission des dossiers à une autorité',
    data : 'input.xml',
    postprocess : 'postprocess.xml',
    icon : 'cogs',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/postprocess.xml');

// ------------------------------------------------------------------------
log.info('Step created');
