log.info("==> preparePostMail preprocess");

function pad(s) { return (s < 10) ? '0' + s : s; }

/**
 * Getting files
 */
var introduction = '/'+_PROVIDER_.getAbsolutePath('introduction.pdf');
var advertissment = '/'+_PROVIDER_.getAbsolutePath('advertissment.pdf');
var cerfa = _input.parameters.attachment.cerfa;

log.info("introduction => "+introduction);
log.info("advertissment => "+advertissment);
log.info("cerfa => "+cerfa);

/**
 * Setting data into the introduction file
 */
var cerfaFields = {};

var newDate = new Date();
var date = pad(newDate.getDate().toString());
var month = newDate.getMonth() + 1;
date = date.concat(pad(month.toString()));
date = date.concat(newDate.getFullYear().toString());

var recipientName = _INPUT_.result.address.recipientName ? _INPUT_.result.address.recipientName+ ", " : '';
var recipientAddressName = _INPUT_.result.address.recipientAddressName ? _INPUT_.result.address.recipientAddressName + " " : '';
var recipientPostalCode = _INPUT_.result.address.recipientPostalCode ? _INPUT_.result.address.recipientPostalCode + " " : '';
var recipientCity = _INPUT_.result.address.recipientCity ? _INPUT_.result.address.recipientCity : '';

cerfaFields['date']              = date;
cerfaFields['autoriteHabilitee'] = recipientName + recipientAddressName + recipientPostalCode + recipientCity;
cerfaFields['numeroDossier']     = nash.record.description().recordUid;

/** Saving the introduction file with the infos */
var savedIntroductionFile = nash.doc.load(introduction).apply(cerfaFields).save('introduction.pdf');
nash.record.saveFile(introduction, savedIntroductionFile.getContent());

/**
 * Preparing the files to merge
 */
var entry = [];
/** Introduction file */
entry.push({
	id:"introduction",
	label:introduction
});
/** Advertissment file */
entry.push({
	id:"advertissment",
	label:advertissment
});
/** Cerfa file */
entry.push({
	id:"cerfa",
	label:cerfa
});
/** Other files */
var fileId = null;
var filePath = null;
for (var file in _input.parameters.attachment.others) {
	fileId = _input.parameters.attachment.others[file].id;
	filePath = _input.parameters.attachment.others[file].label;
    	
	entry.push({
		id:fileId,
		label:filePath
	});
}

/**
 * Binding the data
 */
var data = nash.instance.load("display.xml");
data.bind("request",{
	content:{
		entry: entry
	}
})
