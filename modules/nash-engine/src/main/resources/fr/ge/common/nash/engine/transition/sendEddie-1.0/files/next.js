_log.info("==> postprocess Eddie");

nash.instance //
    .load('output.xml') //
    .bind('result', {
        'tracker' : {
            'referenceId' : 'ENVOI-DOSSIER-EDDIE',
            'message' : "Un dossier a été déposé dans Eddie à l'autorité compétente " + _input.request.input.funcLabel
        }
    });
