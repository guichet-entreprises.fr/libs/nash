# Description of view transition:
<br />

The goal of this transition is to generate a record summary.

<br />
<br />

# Input of view transition:
<br />

* To generate a record view, we need:
    * destFuncId :     "C7501"
    * attachment :  "/absoluth path of pj/"

<br />
<br />

# Output of view transition: 
<br />

Create data.xml with a record view to be displayed 


