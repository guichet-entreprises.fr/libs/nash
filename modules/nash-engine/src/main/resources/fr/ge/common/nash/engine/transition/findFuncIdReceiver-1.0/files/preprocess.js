// input 
var grp = _INPUT_.authority;

_log.info("group is  {}",grp);

var type_auth = !grp.authorityType ? null : grp.authorityType;

_log.info("type_auth  {}",type_auth);

var codePostalAuth = !grp.zipCode ? null : grp.zipCode;

_log.info("zipCode  {}",codePostalAuth);


var serviceParams = {
    "region" : codePostalAuth
};


_log.info("serviceParams  {}",JSON.stringify(serviceParams));

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',type_auth) //
        .connectionTimeout(300000) //
        .receiveTimeout(30000)
        .accept('text') //
        .post(serviceParams);


// Result value of functional id 
var codeAuthority = response.asString();



_log.debug("codeAuthority is {}",codeAuthority);

var pathOfPj = !grp.attachment ? null:grp.attachment;


// create  data.xml  with functional id of authority

return spec.create({
	id: 'findDest',
	label: "Conseil en propriété industrielle - établissement de l'autorité compétente",
	groups: [spec.createGroup({
			id: 'functionalId',
			label: "Identification  de l'autorité compétente",
			data: [spec.createData({
					id: 'funcId',
					label: "Code de l'autorité compétente",
					description: 'Code interne',
					type: 'String',
					mandatory: true,
					value: codeAuthority
				}),
				spec.createData({
					id: 'attachment',
					label: "Le chemin de la pièce jointe",
					type: 'String',
					mandatory : true,
					value: pathOfPj
				}),
			]
		})]
});