# Description

La transition permet de trouver l'adresse mail d'une autorité à partir Code de l'autorité compétente

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| funcId | Code de l'autorité compétente | Oui |
| cerfa | Chemin du fichier cerfa | Oui |
| others | Prévisualisation des fichiers | Oui |

# Résultat(s)

l'adresse email de l'autorité