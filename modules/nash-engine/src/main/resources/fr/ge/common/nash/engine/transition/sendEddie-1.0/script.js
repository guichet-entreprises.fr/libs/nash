//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'sendEddie',
    label : "Envoyer un dossier à Eddie",
    preprocess : 'preprocess.xml',
    postprocess : 'postprocess.xml',
    data : 'display.xml',
    icon : 'envelope-o',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/send.js');
step.copy('files/display.xml');
step.copy('files/postprocess.xml');
step.copy('files/next.js');
step.copy('files/output.xml');
// ------------------------------------------------------------------------
log.info('Step created');
