//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'findInfoReceiver',
    label : 'find information of receiver',
    preprocess : 'preprocess.xml',
    data : 'data-generated.xml',
    postprocess : 'postprocess.xml',
    icon : 'search',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy('files/preprocess.xml');
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.js');
step.copy('files/postprocess.xml');
step.copy('files/postprocess-prepare.js');

// ------------------------------------------------------------------------
log.info('Step created');
