# Description of prepareZip transition:
<br />

The goal of this transition is to prepare a zip to send to EDDIE.

<br />
<br />

# Input of prepareZip transition:
<br />

* To prepare a zip file we need:
    * attachment : String 
    * entry : PreviewFile

<br />
<br />


# Output of prepareZip transition: 
<br />

Create display.xml with the absolute path of the zip file created


