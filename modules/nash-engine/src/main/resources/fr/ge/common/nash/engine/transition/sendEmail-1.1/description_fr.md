# Description

La transition permet de envoyer un mail

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| funcId | Code de l'autorité compétente | Oui |
| funcLabel | libellé de l'autorité compétente | Oui |
| emailAddress | adresse mail | Oui |
| object | objet du mail | Non |
| content | contenu du mail | Oui |
| attachment | pièce jointe | Non |


# Résultat(s)

Création display.xml avec le code reponse  de l'api eddie.
