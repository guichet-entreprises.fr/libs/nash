# Description of find notification list:
<br />

The goal of this transition is to retrieve the mailing list configured for a partner in order to send an e-mail informing that a record has arrived to the backoffice.

<br />
<br />

# Transition inputs :
<br />

* funcId      : "2018-06-GHK-FU6001"

<br />
<br />


# Transition output : 
<br />

Mailing list configured to notify a partner.


