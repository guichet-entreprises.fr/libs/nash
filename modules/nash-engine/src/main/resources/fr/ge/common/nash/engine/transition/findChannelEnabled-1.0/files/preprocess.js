// input functionnal id of authority
var grp = _input.result;
var funcId = !grp.funcId ? '' : grp.funcId;
var mandatoryChannel = !grp.mandatoryChannel ? null : grp.mandatoryChannel;

var nbrChannels = 0
var channels = [];

var funcLabel;

var backOfficeChannel = {"id" : "backOfficeChannel", "label" : "Backoffice"};
var ftpChannel = {"id" : "ftpChannel", "label" : "FTP"};
var emailChannel = {"id" : "emailChannel", "label" : "Courrier éléctronique"};
var addressChannel = {"id" : "addressChannel", "label" : "Courrier Papier"};

if (null != mandatoryChannel) {
    [
        backOfficeChannel,
        ftpChannel,
        emailChannel, 
        addressChannel
    ].forEach(function (referential) {
        if (referential.id == mandatoryChannel) {
            nbrChannels++;
            channels.push(referential);
            log.debug('Mandatory channel found for authority ' + funcId + ' => ' + referential.label);
        }
    });

    try {
        var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', funcId) //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .accept('json') //
            .get();
        if (response.status == 200) {
            var receiverInfo = response.asObject();
            funcLabel = !receiverInfo.label ? null :receiverInfo.label;
        }
    } catch (error) { log.info('Cannot get authority label for authority ' + funcId + ' => ' + error); }
} else {
    //-->Call Directory to retrieved the active transfer channels
    try {
        var response = nash.service.request('${directory.baseUrl}/private/v1/services/authority/{funcId}/channel/active', funcId) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000) //
        .accept('json') //
        .get();
    
        if(response.status == 200){
            var retrievedInfos = response.asObject();
            log.info('Retrieved active transfer channels for the autority '+funcId+' are : '+ retrievedInfos);
    
            funcLabel = retrievedInfos.label;
            
            if (null != retrievedInfos.backoffice) {
                nbrChannels++;
                channels.push(backOfficeChannel);
            }
            if (null != retrievedInfos.ftp) {
                nbrChannels++;
                channels.push(ftpChannel);
            }
            if (null != retrievedInfos.email) {
                nbrChannels++;
                channels.push(emailChannel);
            }
            if (null != retrievedInfos.address) {
                nbrChannels++;
                channels.push(addressChannel);
            }
        }
    } catch (error) {
        // Response
        log.info('error while calling directory to retrieve active transfer channels for the authority '+funcId+' => ' + error);
    }
}
log.info('nbrChannels => '+ nbrChannels);

//retrieve current step ID :
var currStepId = nash.record.stepsMgr().current().getId();

var metas = [
    { 'name': 'authority', 'value': funcId},
    { 'name': 'replay-'+funcId, 'value': currStepId}
];
nash.record.meta(metas);




var result = {
        authority:{
            funcId:funcId,
            funcLabel:funcLabel
        },
        nbrChannels:nbrChannels+''
    };

if(nbrChannels > 0){
    result.activeChannels = {channels : channels};
}

log.info('result => '+ result);

var data = nash.instance.load("display.xml");
data.bind("result", result)
    