# Description

La transition permet de trouver l'identifiant fonctionnel à partir des paramettre d'entres 

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| algo | Nom de l'algorithme | Oui |
| formJuridique | Forme juridique | Oui |
| optionCMACCI | Option CMA CCI | Oui |
| codeCommune | Code commune | Oui |
| typePersonne | Type de personne | Oui |
| secteur1 | Secteur | Oui |
| cerfa | Chemin du fichier cerfa | Oui |
| regent | Chemin du fichier regent | Non |
| others | Prévisualisation des fichiers | Oui |
| xmltc | Chemin du fichier xmlTc | Non |

# Résultat(s)

Code de l'autorité compétente