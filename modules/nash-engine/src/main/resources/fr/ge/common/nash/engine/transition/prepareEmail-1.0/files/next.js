_log.info("===> postprocess prepareEmail");

var attachments = [];
var referentialOption = null;
for (var file in _input.request.content.entry) {
    referentialOption = _input.request.content.entry[file];
	var attachment = {
		"id":nash.util.resourceFromPath(referentialOption.label).label,
		"label":referentialOption.label
	};
	attachments.push(attachment);
}
var data = nash.instance.load('output.xml');

data.bind(
		"parameters",{
			content:{
				entry:attachments
			}
		}
	);

data.bind(
		"result", {
	    	email : {
	    		object : _input.request.output.object,
	    		content : _input.request.output.content
	    	}
		}
	);