# Description

La transition permet de envoyer un mail

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| funcId | Code de l'autorité compétente | Oui |
| funcLabel | libellé de l'autorité compétente | Oui |
| email | adresse mail | Oui |
| emailObject | objet du mail | Non |
| emailContent | contenu du mail | Oui |
| emailPJ | pièce jointe | Non |
| emailSender | mail de l'emetteur | Non |

# Résultat(s)

Création display.xml avec le code reponse  de l'api exchange.
