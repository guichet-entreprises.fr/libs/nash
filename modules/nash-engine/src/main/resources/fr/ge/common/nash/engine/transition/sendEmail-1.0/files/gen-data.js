//------------------------------------------------------------------------
// Generate the page to confirm the email
//------------------------------------------------------------------------
var grp = _INPUT_.email;

_log.info("group is  {}",grp);

var destFuncId = !grp.destFuncId ? '' :grp.destFuncId;
_log.info("destFuncId is {}",destFuncId);

// input email of receiver
email = !grp.email ? '' :grp.email;

_log.info("email is  {}",email);

title = !grp.title ? '' :grp.title;
body = !grp.body ? '' :grp.body;


attachment = !grp.attachment ? '' :grp.attachment;

// Genereate data.xml with data-to-send 
return spec.create({
		id: 'Send',
		label: "Preparation de l'envoi de mail",
		groups: [spec.createGroup({
				id: 'email',
				label: "Contenu email",
				data: [spec.createData({
						id: 'emailAddress',
						label: "Email Destinataire",
						type: 'Email',
						mandatory: true,
						value: email
					}), spec.createData({
						id: 'emailObject',
						label: "Description de l'établissement",
						type: 'String',
						mandatory: true,
						value: title
					}), spec.createData({
						id: 'emailContent',
						label: "Le contenu du mail",
						type: 'Text',
						mandatory: true,
						value: body
					}), spec.createData({
						id: 'emailPj',
						label: "Le chemin du Cerfa",
						type: 'String',
						mandatory: true,
						value: attachment
					}), spec.createData({
						id: 'destFuncId',
						label: "Code de l'autorité compétente",
						type: 'String',
						mandatory: true,
						value: destFuncId
					})
				]
			})]
});