# Description

La transition permet de créer un fichier zip à envoyer sur 'EDDIE'  

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| regent | Chemin du fichier regent | Non |
| others | Prévisualisation des fichiers | Oui |
| xmltc | Chemin du fichier xmlTc | Non |

# Résultat(s)

Création d'un display.xml avec le chemin absolu du fichier zip.
