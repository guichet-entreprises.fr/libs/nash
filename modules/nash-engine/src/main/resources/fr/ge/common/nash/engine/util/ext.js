(function (){

    var ext = root.define('require', function (nash) {

        var prev = require;

        if (require.__prev) {
            prev = require.__prev;
        }

        if (nash.__internalRecordRequire) {
            var newImpl = function (path) {
                /*
                 * Need to use a Java function to load record lib script with ScriptExecutor
                 */
                if (path.endsWith('.js')) {
                    return nash.__internalRecordRequire(path);
                } else {
                    return prev(path);
                }
            };
            
            newImpl.__prev = prev;
            
            return newImpl;
        } else {
            return prev;
        }

    });

})();
