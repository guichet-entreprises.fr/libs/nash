var globalContext = this;
var api = {};

globalContext.api = api;

/**
 * Dependency management functions
 */
(function() {

    var modules = {};

    /**
     * Declare a named module and its dependencies.
     * 
     * 
     * @param name
     *            module name
     * @param deps
     *            module dependencies
     * @param fn
     *            module instanciation function
     */
    define = api.define = function(name, deps, fn) {
        if (undefined == fn) {
            fn = deps;
            deps = [];
        }

        modules[name] = {
            deps : deps,
            fn : fn
        };
    }

    /**
     * Require a module for instanciation after looking for its dependencies.
     * 
     * 
     * @param deps
     *            module dependencies
     * @param fn
     *            instanciation function
     * @returns result of instanciation function execution
     */
    require = api.require = function(deps, fn) {
        var values = [];
        if (deps) {
            deps.forEach(function(dep) {
                var module = modules[dep], value;
                if (module) {
                    if (!module.value) {
                        module.value = require(module.deps, module.fn);
                    }

                    value = module.value;
                }

                values.push(value)
            });
        }

        return fn.apply(null, values);
    }

})();

/**
 * Utility functions
 */
define('util', function() {

    var Util = {};

    Util.extend = function(dst) {
        var k, args = Array.prototype.slice.call(arguments, 0);

        args.forEach(function(elm) {
            if (elm) {
                if (elm.hasOwnProperty) {
                    for (k in elm) {
                        if (elm.hasOwnProperty(k)) {
                            dst[k] = elm[k];
                        }
                    }
                } else {
                    for (k in elm) {
                        dst[k] = elm[k];
                    }
                }
            }
        });

        return dst;
    };

    api.Util = Util;
    return Util;
});

define('spec', [ 'util' ], function(util) {

    var Node = function() {
    };

    Node.prototype.toString = function() {
        return JSON.stringify(this);
    };

    var Builder = function() {
    };

    Builder.create = function(values) {
        return util.extend(new Node(), values, {
            _type : 'form'
        });
    };

    Builder.createGroup = Builder.group = function(values) {
        return util.extend(new Node(), values, {
            _type : 'group'
        })
    };
    Builder.createData = Builder.data = function(values) {
        return util.extend(new Node(), values, {
            _type : 'data'
        })
    };

    Builder.prototype.toString = function() {
        return JSON.stringify(this);

    };
    api.spec = Builder;
    return Builder;

});

define('meta', [ 'spec' ], function(spec) {

    var _form;
    var _metas = [];
    var _currentMetaIndex = -1;
    var meta = function() {
        ++_currentMetaIndex;

        var currentSpec = _bridge.getOutputSpecification();
        _log.debug('Current resource name : {}', currentSpec.resourceName);

        var currentGroup = _bridge.createGroup({
            id : 'meta' + (_currentMetaIndex + 1),
            label : 'meta compétent',
            data : []
        });

        currentSpec.groups.add(currentGroup);

        _log.debug('Group retrieve');

        _log.debug('Return function');

        return {
            _group : currentGroup,

            add : function(key, value) {
                _log.debug('Adding value "{}" for "{}"', value, key);
                currentGroup.data.add(_bridge.createData({
                    id : key,
                    value : value,
                    type : 'String',
                    label : key + ' value'
                }));

                return this;
            }
        };
    };

    return meta;

});

define('pdf', [ 'util' ], function(util) {

    var Pdf = function(filename, values) {
        this.filename = filename;
        this.values = values;
    };

    Pdf.prototype.toString = function() {
        return filename;
    };

    var PdfFactory = function() {
    };

    PdfFactory.create = function(name, values) {
        var o = {
            _type : 'pdf',
            _src : name
        };
        if (values) {
            o = util.extend(o, values);
        }
        return o;
    };

    PdfFactory.save = function(name, obj) {
        if (globalContext._bridge && globalContext._bridge.savePdf) {
            return _bridge.savePdf(name, obj);
        } else {
            return null;
        }

    }

    api.pdf = PdfFactory;

    return PdfFactory;

});
