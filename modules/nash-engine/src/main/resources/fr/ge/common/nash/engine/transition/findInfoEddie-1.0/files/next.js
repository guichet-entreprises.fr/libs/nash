_log.info("==> postprocess findInfoEddie");

//call directory with funcId to find all information of Authorithy
var destFuncId = _input.request.input.funcId

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
    .connectionTimeout(10000) //
    .receiveTimeout(10000) //
    .accept('json') //
    .get();

//result
var receiverInfo = response.asObject();

var contactInfo = !receiverInfo.details ? null : receiverInfo.details;

var transferChannels = {
    "backoffice": {
        "state": "disabled"
    },
    "email": {
        "emails": [],
        "state": "disabled"
    },
    "address": {
        "addressDetail": {},
        "state": "disabled"
    },
    "ftp": {
        "state": "disabled",
        "ftpMode": "",
        "pathFolder": "",
        "comment": "",
        "delegationAuthority": "",
        "token": ""
    }
}
if(contactInfo.transferChannels == null){
    contactInfo.transferChannels = transferChannels;
}

contactInfo.transferChannels["ftp"] = {
    "state" : contactInfo.transferChannels.ftp.state,
    "token" : _input.request.output.ftp.token,
    "pathFolder" : _input.request.output.ftp.pathFolder,
    "ftpMode" : contactInfo.transferChannels.ftp.ftpMode,
    "comment" : contactInfo.transferChannels.ftp.comment,
    "delegationAuthority" : contactInfo.transferChannels.ftp.delegationAuthority
};

log.info("transferChannels is {}", contactInfo.transferChannels);

receiverInfo.details = contactInfo;

log.info("receiverInfo is {}",receiverInfo);

//update directory 
var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
               .connectionTimeout(10000) //
               .receiveTimeout(10000) //
               .dataType('application/json')//
               .accept('json') //
               .put(receiverInfo);

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}


var ftp = map(_input.request.output.ftp, {
    'token' : 'token',
    'pathFolder' : 'pathFolder'
});

_log.info("==> ftp information : {}", ftp);
nash.instance //
    .load('output.xml') //
    .bind('result', {
        'ftp' : ftp
    });
