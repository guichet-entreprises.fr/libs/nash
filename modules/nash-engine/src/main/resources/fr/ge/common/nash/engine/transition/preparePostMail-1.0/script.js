// ------------------------------------------------------------------------
// Create transition
// ------------------------------------------------------------------------

var step = _transition.createStep({
    id : 'preparePostMail',
    label : 'Préparer un courier papier',
    preprocess : 'preprocess.xml',
    postprocess : 'postprocess.xml',
    data : 'display.xml',
    icon : 'envelope-o',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/prepare.js');
step.copy('files/display.xml');
step.copy('files/postprocess.xml');
step.copy('files/next.js');
step.copy('files/output.xml');
step.copy('files/introduction.pdf');
step.copy('files/advertissment.pdf');
// ------------------------------------------------------------------------
log.info('Step created');
