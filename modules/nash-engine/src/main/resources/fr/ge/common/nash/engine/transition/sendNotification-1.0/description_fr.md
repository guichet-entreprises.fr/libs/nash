# Description

Le but de cette transition est d'envoyer une notification par e-mail à une liste de diffusion configurée pour une autorité.

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| funcId | Code de l'autorité compétente | Oui |
| funcLabel | libellé de l'autorité compétente | Oui |
| emailList | liste d'adresses mails | Oui |
| object | Notification : un nouveau dossier Guichet Entreprises est arrivé au backoffice | Non |
| content | contenu du mail | Oui |

# Résultat(s)

l'envoi d'un mail