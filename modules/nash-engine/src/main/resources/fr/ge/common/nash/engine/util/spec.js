(function () {

    return root.define("spec", function (nash) {

        var util = require('util');

        var Node = function() {
        };

        Node.prototype.toString = function() {
            return JSON.stringify(this);
        };

        var Builder = function() {
        };

        Builder.create = function(values) {
            return util.extend(new Node(), values, {
                _type : 'form'
            });
        };

        Builder.createGroup = Builder.group = function(values) {
            return util.extend(new Node(), values, {
                _type : 'group'
            })
        };
        Builder.createData = Builder.data = function(values) {
            return util.extend(new Node(), values, {
                _type : 'data'
            })
        };

        Builder.prototype.toString = function() {
            return JSON.stringify(this);

        };

        return Builder;

    });

})();

