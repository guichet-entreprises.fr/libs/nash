// input functionnal id of authority
var grp = _INPUT_.view;

var destFuncId = !grp.search.input.funcId ? '' : grp.search.input.funcId;
var channel = !grp.search.input.channel ? '' : grp.search.input.channel;
var attachment = !grp.search.input.attachment ? '' : grp.search.input.attachment;

_log.info("destFuncId is  {}", destFuncId);

// call directory with funcId to find all information of Authorithy

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
	.connectionTimeout(10000) //
	.receiveTimeout(10000) //
	.accept('json') //
	.get();

//result
var receiverInfo = response.asObject();

var contactInfo = !receiverInfo.details ? null : receiverInfo.details;

var transferChannels = {
	"backoffice": {
		"state": "disabled"
	},
	"email": {
		"emails": [],
		"state": "disabled"
	},
	"address": {
		"addressDetail": {
			"recipientName": "",
			"recipientNameCompl": "",
			"addressName": "",
			"addressNameCompl": "",
			"cityName": "",
			"postalCode": ""
		},
		"state": "disabled"
	},
	"ftp": {
		"token": "",
		"state": "disabled"
	}
}
if(contactInfo.transferChannels == null){
	contactInfo.transferChannels = transferChannels;
}


switch (channel.id) {
	case "emailChannel":
		var email = {}
		var funcEmail = _INPUT_.view.search.output.email.funcEmail;
//		email.state = "enabled";
		email.emails = [];
		email.emails.push(funcEmail);
		transferChannels.email = email;
		break;
	case "backOfficeChannel":
		var backoffice = {};
//		backoffice.state = "enabled";
		transferChannels.backoffice = backoffice;
		break;
	case "ftpChannel":
		var ftp = {}
		var token = _INPUT_.view.search.output.ftp.uid;
		ftp.token = token;
//		ftp.state = "enabled";
		transferChannels.ftp = ftp;
		break;
	default:
		// bloc address
		var address = {};
//		address["state"]="enabled";
		var addressDetail = {};
		addressDetail["recipientName"] = _INPUT_.view.search.output.address.recipientName
		addressDetail["recipientNameCompl"] = _INPUT_.view.search.output.address.recipientNameCompl
		addressDetail["recipientAddressName"] = _INPUT_.view.search.output.address.recipientAddressName
		addressDetail["recipientAddressNameCompl"] = _INPUT_.view.search.output.address.recipientAddressNameCompl
		addressDetail["recipientPostalCode"] = _INPUT_.view.search.output.address.recipientPostalCode
		addressDetail["recipientCity"] = _INPUT_.view.search.output.address.recipientCity
		address["addressDetail"] = addressDetail;
		transferChannels["address"] = address;
}

log.info("transferChannels is {}",transferChannels);

contactInfo.transferChannels = transferChannels;
receiverInfo.details = contactInfo;

log.info("receiverInfo is {}",receiverInfo);

var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .dataType('application/json')//
	           .accept('json') //
	           .put(receiverInfo);

var data = nash.instance.load("data-to-send.xml");

data.bind("result",{
			address:{
				recipientName:_INPUT_.view.search.output.address.recipientName,
				recipientNameCompl:_INPUT_.view.search.output.address.recipientNameCompl,
				recipientAddressName:_INPUT_.view.search.output.address.recipientAddressName,
				recipientAddressNameCompl:_INPUT_.view.search.output.address.recipientAddressNameCompl,
				recipientPostalCode:_INPUT_.view.search.output.address.recipientPostalCode,
				recipientCity:_INPUT_.view.search.output.address.recipientCity
			},
			ftp:{
				uid:_INPUT_.view.search.output.ftp.uid
			},
			email:{
				funcEmail:_INPUT_.view.search.output.email.funcEmail
			},
			backoffice: {
				funcId:_INPUT_.view.search.output.backoffice.funcId,
				funcLabel:_INPUT_.view.search.output.backoffice.funcLabel
			},
			others:{
				attachment:_INPUT_.view.others.attachment
			}
})
