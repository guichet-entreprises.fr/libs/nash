_log.info('===> prepareEmail preprocess');

var object = "Réception d'un nouveau dossier Guichet Entreprises";
var content = "<html>" + //
            "Bonjour " + _input.result.funcLabel + ",<br\><br\>" + //
            "Le service Guichet Entreprises vous informe que le nouveau dossier " + nash.record.description().recordUid + " est en attente de traitement.<br/>" + //
            "Vous trouverez joint à ce courriel ledit dossier.<br/>" + //
            "Le service Guichet Entreprises vous remercie.<br/><br/>L’équipe du Guichet Entreprises</html>";

/**
 * Preparing the files to merge
 */
var entry = [];

/** Cerfa file */
entry.push({
	id:"cerfa",
	label:_input.parameters.attachment.cerfa
});
/** Other files */
var fileId = null;
var filePath = null;
for (var file in _input.parameters.attachment.others) {
	fileId = _input.parameters.attachment.others[file].id;
	filePath = _input.parameters.attachment.others[file].label;
    	
	entry.push({
		id:fileId,
		label:filePath
	});
}


var data = nash.instance.load("display.xml");
data.bind('request', {
    'output' : {
        'email' : _input.result.email.email,
        'object' : object,
        'content' : content,
    },
    'content' : {
        'entry' : entry
    }
});

_log.info("Finish bind display.xml");