(function () {

    return root.define("pushingbox", function (nash) {

        log.debug("Pushing box implementation");

        return function () {

            // prepare info to send to pushingBox
            var pushingBoxDevID    = _CONFIG_ ? _CONFIG_.get('pushingbox.devid') : null;

            //A configurer par WOW 'pushingbox.baseUrl=http://api.pushingbox.com/pushingbox
            var pushingBoxBaseUrl = _CONFIG_ ? _CONFIG_.get('pushingbox.baseUrl') : null;

            if (pushingBoxDevID && pushingBoxBaseUrl) {
                log.debug("Pushing box calling");

                var recordUid         = '<unknown>';
                var formName         = '<unknown>';
                var formId             = '<unknown>';
                var dateTime         = new Date();

                if (nash.record && nash.record.description) {
                    recordUid     = nash.record.description().recordUid;
                    formName     = nash.record.description().title;
                    formId        = nash.record.description().formUid;
                }

                log.debug("recordUid is  {}", recordUid);
                log.debug("formName is  {}", formName);
                log.debug("formId is  {}", formId);
                log.debug("dateTime is  {}", dateTime);

                // call pushingBox
                var response = null;
                try {
                    response = nash.service.request(pushingBoxBaseUrl)
                    .param("recordId",  recordUid)
                    .param("formName",  formName)
                    .param("formId",    formId)
                    .param("dateTime",  dateTime)
                    .param("devid",     pushingBoxDevID)
                    .get();
                } catch (e) {
                    log.error("An error occured when call pushing box");
                }

                return null != response && 200 == response.status
            }

            return false;
        };

    });

})();