
function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}

var funcId = _input.result.funcId;

_log.info('==> find eddie information for "{}"', funcId);

// call directory with funcId to find all information of Authority
var data = nash.instance.load("display.xml");
data.bind('request', {
    'input' : {
        'funcId' : funcId //
    }
});
try {
    var response = nash.service.request('${directory.baseUrl}/private/v1/services/authority/{funcId}/channel/active', funcId) //
    .accept('json') //
    .get();
    // Show success message if response is 200
    var status = response.status;
    data.bind('request', {
        'output' : {
            "status" : status
        }
    });

    if (status == 200) {
        var ftpConfig = response.asObject();
        log.info("ftp config : {}", ftpConfig);
        var ftp = {
            'token' : ftpConfig.ftp.token, //
            'pathFolder' : ftpConfig.ftp.pathFolder //
        };
        
        log.info("ftp values to bind : {}", ftp);
        data.bind('request', {
            'output' : {
                'ftp' : ftp
            }
        });
    }
} catch (error) {
    log.info('An error occured while searching for active configuration for authority "{}" => ' + error, funcId);
    data.bind('request', {
        'output' : {
            "status" : 500
        }
    });
}