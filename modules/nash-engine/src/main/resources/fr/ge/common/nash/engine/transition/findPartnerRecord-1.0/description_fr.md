# Description

Le but de cette transition est de récupérer les informations d'une autorité

# valeur(s) attendue(e)

| Nom | Description | Obligatoire |
|---|---|---|
| cerfa | Chemin du fichier cerfa | Oui |
| regent | Chemin du fichier regent | Non |
| others | Prévisualisation des fichiers | Oui |
| xmltc | Chemin du fichier xmlTc | Non |
| funcId | Code de l'autorité compétente | Oui |

# Résultat(s)

*Chemin de l'authorité compétente.
*Libellé de l'authorité compétente.