/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/referentials', 'handlebars', '_fm_data' ], function($, referentials, Handlebars, _data) {

    function localScopeModel(model, path) {
    	if (path.length == 0 || path == ".") {
    		return model;
    	}
        var tokens = path.replace(/^_(record|step|page|group)\./, '').split('.').map(function (elm) {
            var m = /^([^\[]+)(?:\[([^\]]+)\])?/.exec(elm);
            return { key: m[1], idx: m[2] };
        });
        for (var token = tokens.shift(); undefined !== token && undefined !== model[token.key]; token = tokens.shift()) {
            if (undefined === token.idx) {
                model = model[token.key];
            } else {
                model = model[token.key][token.idx];
            }
        }
        return model;
    }
    
    function displayElements(field, opts) {
    	var fieldName = field.attr('data-field');
        var hiddenInputId = field.find("input[name='"+fieldName+".id']");
        var hiddenInputLabel = field.find("input[name='"+fieldName+".label']");
        var select = field.find("select[name='"+fieldName+"']");
        select.empty();
        select.append($('<option>', {
            value : ''
        }));
        referentials.update(field, opts, function(savedFieldOptions, external) {
            if (!external || !external.url) {
                return;
            }
            if (!external.base) {
                external.base = '';
            }
            if (!external.id) {
                external.id = '';
            }
            if (!external.label) {
                external.label = '';
            }
            
            var template = Handlebars.compile(external.url.value);
            var closestGroupName = field.closest('[data-group]').first().attr('data-group');
            _data['_group'] = localScopeModel(_data._page, closestGroupName);

            var targetUrl = template(_data);
            
            var fieldOptionBasePath = ['result', external.base].filter(function(val) { return val; }).join('.');
            var fieldOptionIdPath = external.id;
            var fieldOptionLabelPath = external.label;
            
        	var suggestion;
            if (savedFieldOptions && savedFieldOptions.length == 1) {
            	suggestion = savedFieldOptions[0];
            }
            $.ajax({
                'url': targetUrl,
                'method': 'GET'
            }).done(function(result) {
                //var fieldOptionsBase = eval(fieldOptionBasePath);
                var fieldOptionsBase = localScopeModel(result, external.base);
                var suggestions = [];
                if ($.isArray(fieldOptionsBase) && fieldOptionsBase.length > 0) {
                    suggestions = $.map(fieldOptionsBase, function(dataItem) {
                        if ($.isPlainObject(dataItem)) {
                        	var templateId = Handlebars.compile(fieldOptionIdPath);
                        	var selectId = templateId(dataItem);
                        	
                        	var templateLabel = Handlebars.compile(fieldOptionLabelPath);
                        	var value = templateLabel(dataItem);
                            
                        	select.append($('<option>', {
                                value : selectId, 
                                selected : suggestion && suggestion.id == selectId 
                            }).text(value));
                        	
                        } else if (typeof dataItem === "string") {
                        	var template = Handlebars.compile(dataItem);
                        	var value = template(dataItem);
                            select.append($('<option>', {
                                value : dataItem, 
                                selected : suggestion && suggestion.id == dataItem
                            }).text(value));
                        } 
                    });
                }
            });
            select.on('change', function() { 
            	var selected = $(this).find(":selected");
            	hiddenInputId.val(selected.val());
            	hiddenInputLabel.val(selected.text());
            });
        }, true);
    }
    
    var obj = {
        initialize : function(field, opts) {
            displayElements(field, opts);
        },
        validate : function(data, opts) {
            return {
                validated : true,
                value : data.asString()
            };
        }
    };

    return obj;
});
