log.info('Send a message to TRACKER for the record {}', nash.record.description().recordUid);

var tracker = _input.result.tracker;
var referenceId = !tracker.referenceId ? undefined : tracker.referenceId;

if (undefined !== referenceId) {
    nash.tracker.link(referenceId);
}
nash.tracker.post(tracker.message);
