//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'processed',
    label : 'Mark a record as processed',
    data : 'data.xml',
    status : 'todo',
    icon : 'check',
    user : 'bo'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy('files/data.xml');

// ------------------------------------------------------------------------
log.info('Step created');
