//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'findChannelEnabled',
    label : "Trouver les canaux actifs d'une autorité",
    preprocess : 'preprocess.xml',
    postprocess: 'postprocess.xml',
    data : 'display.xml',
    icon : 'search',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/preprocess.js');
step.copy('files/display.xml');
step.copy('files/output.xml');
step.copy('files/postprocess.xml');
step.copy('files/postprocess-prepare.js');

// ------------------------------------------------------------------------
log.info('Step created to find channels enabled for a partner');
