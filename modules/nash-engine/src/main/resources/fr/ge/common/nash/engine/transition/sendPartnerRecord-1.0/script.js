log.info('Send partner record {} ({}) to {}', nash.record.description().title, nash.record.description().recordUid, _INPUT_.partner.code);

var record = nash.instance.from('${nashBo.baseUrl}').createRecord('local:model');

record.description.setAuthor(_INPUT_.partner.code);
record.description.setTitle(nash.record.description().title);
record.description.setFormUid(nash.record.description().recordUid);

var summary = record.load('1-summary/data.xml');
summary.set('view.recordUid', nash.record.description().recordUid);
summary.set('view.denomination', nash.record.description().title);
summary.set('view.typeFormality', null);
var date = new Date();

summary.set('view.transmissionDate',date.toLocaleDateString());
summary.set('view.destFuncId', _INPUT_.partner.code);

log.info('the path of attachment is {}', _INPUT_.partner.attachment);
summary.set('view.attachment', util.findNode(this, _INPUT_.partner.attachment));


record.save();
