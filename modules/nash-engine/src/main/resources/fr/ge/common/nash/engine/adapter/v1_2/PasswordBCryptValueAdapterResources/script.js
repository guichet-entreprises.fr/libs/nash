/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/i18n', 'pwstrength-bootstrap' ], function($, i18n, pwstrengthBootstrap) {

    var obj = {
        initialize : function(field, opts) {
            var first = field.find('input[type="password"]:nth(0)');
            var firstButton = first.parent().find('button');
            var hasErrors = field.find('input[type="hidden"]:nth(0)');
            if (field.find('.pwstrength_viewport_progress').children().length > 0) {
                first.pwstrength('destroy');
            }
            first.pwstrength({
                common: {
                    minChar: opts.min || 8,
                    onScore: function (options, word, totalScoreCalculated) {
                        hasErrors.val(field.find('ul.error-list').children().length > 0);
                        return totalScoreCalculated;
                    }
                },
                ui: {
                    container: ".pwd-container",
                    showErrors: true,
                    showVerdictsInsideProgressBar: true,
                    spanError: function (options, key) {
                        var text = options.i18n.t(key);
                        if (!text || text == 'wordThreeNumbers'
                                || text == 'wordTwoSpecialChar' || text == 'wordUpperLowerCombo'
                                || text == 'wordLetterNumberCombo' || text == 'wordLetterNumberCharCombo') {
                            return '';
                        }
                        return '<span style="color: #ff0000">' + text + '</span>';
                    },
                    viewports: {
                        progress: ".pwstrength_viewport_progress",
                        errors: ".errors"
                    }
                },
                i18n: {
                    t: function (key) {
                        return i18n(key);
                    }
                }
            });
            firstButton.bind("mousedown touchstart", function() {
                first.attr("type", "text");
            });
            firstButton.bind("mouseup touchend", function() {
                first.attr("type", "password");
            });
        },
        validate : function(data, opts) {
            var value = data.asString();
            var check = data.withPath('check').asString();
            var hasErrors = data.withPath('hasErrors').asString();
            var validation = {};
            if (hasErrors == 'true') {
                validation.validated = false;
                validation.message = 'Your password is not strong enough.';
            } else if (value != check) {
                validation.validated = false;
                validation.message = 'The second password is not equal to the first one.';
            } else {
                validation.validated = true;
                validation.value = value;
            }
            return validation;
        }
    };

    return obj;

});
