# Description of send partner record transition:
<br />

The goal of this transition is to send record using backoffice channel.

<br />
<br />

# Transition inputs :
<br />

* funcId           : "2018-06-GHK-FU6001"
* funcLabel        : "URSSAF DE L'OISE"
* typeFormality    : "Création"
* cerfaAttachment  : "2-review/generated/document.pdf"
* regentAttachment : "3-regent/XML_REGENT.xml"

<br />
<br />


# Transition output : 
<br />

Create new record to partner receiver and upload it to backoffice.


