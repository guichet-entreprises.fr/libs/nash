# Description

La transition permet d'arrêter le traitement du dossier afin que le support valide l'adresse postale manuellement.

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| attachment | Chemin du fichier du CERFA | Oui |
| referenceId | Référence du courrier | Non |
| recipientName | Nom du destinataire | Oui |
| recipientNameCompl | Complement du nom du destinataire | Non |
| recipientAddressName | Adresse | Oui |
| recipientAddressNameCompl | Complément d'adresse | Non |
| recipientPostalCode | Code postal | Oui |
| recipientCity | Ville | Oui |

# Résultat(s)

Créez display.xml avec les informations de courrier pour confirmer l'envoi