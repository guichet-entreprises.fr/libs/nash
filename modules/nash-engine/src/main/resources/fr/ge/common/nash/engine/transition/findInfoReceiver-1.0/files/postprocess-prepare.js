//check information of receiver

var authorityObject = new Object();

var destFuncId = _INPUT_.email.destFuncId

authorityObject.funcId = destFuncId
authorityObject.label = _INPUT_.email.labelFunc

var contact = {}
contact["email"] = _INPUT_.email.email;
var contactInfo = JSON.stringify(contact);
authorityObject.contactInfo = contactInfo;


_log.info("authorityObject is  {}",authorityObject);

var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .dataType('application/json')//
	           .accept('json') //
	           .put(authorityObject);

// input email of receiver

email = _INPUT_.email.email

emailObject = "Nouveau dossier disponible"
var urlDashboardBo = _CONFIG_.get('dashboard.bo.url');
emailContent = "<html>"+"Bonjour "+destFuncId +",<br\><br\>" +"Le nouveau dossier " + nash.record.description().recordUid + " est disponible sur dashboard-back-office.<br/>" +"Pour y accéder, cliquez sur le lien : "+"<a href="+'\"'+urlDashboardBo+'\"'+">cliquez ici</a>"+"<html>"

emailPj = _INPUT_.email.emailPj

// create data.xml content data-to-send with exchange service

return spec.create({
		id: 'prepareSend',
		label: "Préparation de l'envoi du message",
		groups: [spec.createGroup({
				id: 'email',
				label: "Le Contenu du message",
				data: [spec.createData({
						id: 'email',
						label: "L'adresse électronique de l'autorité compétente ",
						type: 'Email',
						mandatory: true,
						value: email
					}), spec.createData({
						id: 'title',
						label: "Description de la demande",
						type: 'String',
						value: emailObject
					}), spec.createData({
						id: 'body',
						label: "Le contenu du message",
						type: 'Text',
						value: emailContent
					}), spec.createData({
						id: 'attachment',
						label: "Le chemin de la pièce jointe",
						type: 'String',
						value: emailPj
					}),spec.createData({
						id: 'destFuncId',
						label: "Code de l'autorité compétente",
						type: 'String',
						value: destFuncId
					})
				]
			})]
});