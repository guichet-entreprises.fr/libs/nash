var globalContext = this;

/**
 * Dependency management functions
 */
(function() {

    var PUBLIC_PREFIX = 'engine/';
    var basePrivatePath = 'classpath:fr/ge/common/nash/engine/util';
    var basePublicPath = 'classpath:public/js';

    var modules = {};

    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            position = position || 0;
            return this.substr(position, searchString.length) === searchString;
        };
    }

    function buildAbsolutePath(relativePath) {
        if (!relativePath.match(/\.[/]+$/)) {
            relativePath += '.js';
        }

        if (relativePath.startsWith(PUBLIC_PREFIX)) {
            return basePublicPath + '/' + relativePath;
        } else {
            return basePrivatePath + '/' + relativePath;
        }
    }

    /**
     * Declare a named module and its dependencies.
     * 
     * 
     * @param name
     *            module name
     * @param deps
     *            module dependencies
     * @param fn
     *            module instanciation function
     */
    define = function(name, deps, fn) {
        if (undefined == fn) {
            fn = deps;
            deps = [];
        }

        log.debug('Defining JS module {}', name);

        modules[name] = {
            deps : deps,
            fn : fn
        };
    };

    define.amd = true;

    /**
     * Require a module for instanciation after looking for its dependencies.
     * 
     * 
     * @param deps
     *            module dependencies
     * @param fn
     *            instanciation function
     * @returns result of instanciation function execution
     */
    require = function(deps, fn) {
        var values = [];
        var returnAsArray = false;

        if (!deps) {
            deps = [];
        } else if (Array.isArray(deps)) {
            returnAsArray = true;
        } else {
            deps = [ deps ];
        }

        deps.forEach(function(dep) {
            var module = modules[dep], value;

            if (!module) {
                load(buildAbsolutePath(dep));
                module = modules[dep];
            }

            if (module) {
                if (!module.value) {
                    module.value = require(module.deps, module.fn);
                }

                value = module.value;
            }

            values.push(value)
        });

        if (fn) {
            return fn.apply(globalContext, values);
        } else if (returnAsArray) {
            return values;
        } else {
            return values[0];
        }
    };

    console = {
        log : print
    };

    var lib = {
        define : define,
        require : require
    };

    define('require', [], function() {
        return lib;
    });

    log.debug('Require lib module loaded');

    return lib;

})();
