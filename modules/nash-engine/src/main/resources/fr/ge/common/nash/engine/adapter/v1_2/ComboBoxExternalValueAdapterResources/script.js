/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/referentials', 'devbridge-autocomplete' ], function($, referentials) {

    var obj = {
        initialize : function(field, opts) {
            var idInput = field.find('input[name$="id"]');
            var labelInput = field.find('input[name$="label"]');
            referentials.update(field, opts, function(savedFieldOptions, external) {
                // https://www.devbridge.com/sourcery/components/jquery-autocomplete/
                if (!external || !external.url) {
                    return;
                }
                if (!external.url.inputVarName) {
                    external.url.inputVarName = '';
                }
                if (!external.base) {
                    external.base = '';
                }
                if (!external.id) {
                    external.id = '';
                }
                if (!external.label) {
                    external.label = '';
                }
                var searchTermsTokenName = external.url.inputVarName || 'searchTerms';
                var fieldOptionBasePath = ['jsonResponse', external.base].filter(function(val) { return val; }).join('.');
                var fieldOptionIdPath = ['dataItem', external.id].filter(function(val) { return val; }).join('.');
                var fieldOptionLabelPath = ['dataItem', external.label].filter(function(val) { return val; }).join('.');
                labelInput.autocomplete({
                    ignoreParams: true,
                    serviceUrl: function (searchTerms) {
                        return external.url.value.replace('{{' + searchTermsTokenName + '}}', searchTerms);
                    },
                    deferRequestBy: 300,
                    preventBadQueries: false,
                    transformResult: function(response) {
                        var jsonResponse = JSON.parse(response);
                        var fieldOptionsBase = eval(fieldOptionBasePath);
                        var suggestions = [];
                        if ($.isArray(fieldOptionsBase) && fieldOptionsBase.length > 0) {
                            suggestions = $.map(fieldOptionsBase, function(dataItem) {
                                if ($.isPlainObject(dataItem)) {
                                    return {
                                        data: eval(fieldOptionIdPath),
                                        value: eval(fieldOptionLabelPath)
                                    }
                                } else if (typeof dataItem === "string") {
                                    return {
                                        data: dataItem,
                                        value: dataItem
                                    }
                                } else {
                                    return null;
                                }
                            });
                        }
                        return {
                            'suggestions': suggestions
                        };
                    },
                    onSelect: function (suggestion) {
                        idInput.val(suggestion.data);
                        labelInput.val(suggestion.value);
                        labelInput.data('valid', true);
                    }
                }).change(function() {
                    if (!$(this).data('valid')) {
                        idInput.val('');
                        if (!opts.allowUserOptions) {
                            labelInput.val('');
                        }
                    }
                }).keyup(function(e) {
                    var selectKeys = [
                        38, // Up
                        40 // Down
                    ];
                    var ignoredKeys = [
                        13, // Enter
                        16, // Shift
                        17, // Ctrl
                        18, // Alt
                        27, // Esc
                        37, // Left
                        39 // Right
                    ];
                    if (selectKeys.indexOf(e.which) >= 0) {
                        labelInput.data('valid', true);
                    } else if (ignoredKeys.indexOf(e.which) == -1) {
                        labelInput.data('valid', false);
                    }
                }).blur(function() {
                    var field = $(this);
                    if (field.data('valid') == true && $('.autocomplete-suggestions').css('display') !== 'none') {
                        field.autocomplete().selectHint();
                    }
                });
            }, true);
        },
        validate : function(data, opts) {
            return {
                validated : true,
                value : data.asString()
            };
        }
    };

    return obj;
});
