_log.info("===> postprocess sendEmail");

nash.instance //
    .load('output.xml') //
    .bind('result', {
        'tracker' : {
            'referenceId' : 'ENVOI-DOSSIER-EMAIL',
            'message' : "Un courrier électronique a été envoyé à l'autorité compétente " + _input.request.input.funcLabel
        }
    });
