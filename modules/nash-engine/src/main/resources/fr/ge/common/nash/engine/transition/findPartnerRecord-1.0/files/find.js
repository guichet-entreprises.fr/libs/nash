var funcId = _input.result.funcId;

_log.info('===> find backoffice information to "{}"', funcId);

// call directory with funcId to find all information of Authorithy
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', funcId) //
.accept('json') //
.get();

// result
var receiverInfo = response.asObject();

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}

var who = map(receiverInfo, {
    'funcId' : 'entityId', //
    'funcLabel' : 'label' //
});

var nfo = receiverInfo.details || undefined;
_log.info("nfo is  {}", nfo);

var funcPath = who.funcId;
if (nfo !== undefined && nfo.parent) {
	funcPath = nfo.parent + '/' + who.funcId;
}
_log.info("funcPath is {}", funcPath);

var attachment = map(_input.parameters.attachment, {
    'cerfa' : 'cerfa',
    'regent' : 'regent',
    'xmltc' : 'xmltc',
    'others' : 'others'
});
log.info("attachment : {}", attachment);
var data = nash.instance.load("display.xml");
data.bind('request', {
    'input' : {
        'funcId' : who.funcId
    },
    'output' : {
        'funcLabel' : who.funcLabel,
        'funcPath' : funcPath
    }
});

var others = [];
for (var file in _input.parameters.attachment.others) {
	fileId = _input.parameters.attachment.others[file].id;
	filePath = _input.parameters.attachment.others[file].label;
    if (file != null){
    	others.push({
			id:fileId,
			label:filePath
		});
    }
}
data.bind('request', {
    'attachment' : {
        'cerfa' : _input.parameters.attachment.cerfa,
        'regent' : _input.parameters.attachment.regent,
        'xmltc' : _input.parameters.attachment.xmltc,
        'others' : others
    }
});
