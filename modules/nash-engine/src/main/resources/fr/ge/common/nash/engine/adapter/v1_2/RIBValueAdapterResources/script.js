/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/jqueryMaskPlugin' ], function($) {

    var obj = {
        initialize : function(field, opts) {
            field.find('input').mask('00000 00000 AAAAAAAAAAA 00', {
                onKeyPress: function (value, event) {
                    event.currentTarget.value = value.toUpperCase();
                }
            });
        },
        validate : function(data, opts) {
            var value = data.asString().replace(/ /g, '');
            var pattern = "^([0-9]){5}([0-9]){5}([0-9a-zA-Z]){11}([0-9]){2}$";
            var validation = {
                validated : (value.length == 0 || new RegExp(pattern).exec(value) != null && checkKey(value))
            };
            if (validation.validated) {
                validation.value = value;
            } else {
                validation.message = 'This value does not comply with a RIB.';
            }
            return validation;
        }
    };

    function checkKey(string) {
        var charTranslation = '12345678912345678923456789';
        var upperString = string.toUpperCase();
        var aOrd = 'A'.charCodeAt(0);
        for (var i = 0; i < 21; ++i) {
            var currChar = upperString[i];
            var currCharOrd = currChar.charCodeAt(0);
            if (currCharOrd >= aOrd) {
                upperString = upperString.replace(currChar, charTranslation[currCharOrd - aOrd]);
            }
        }
        var bank = 89 * parseInt(upperString.substring(0, 5));
        var branch = 15 * parseInt(upperString.substring(5, 10));
        var account1 = 76 * parseInt(upperString.substring(10, 16));
        var account2 = 3 * parseInt(upperString.substring(16, 21));
        var key = parseInt(upperString.substring(21, 23));
        return key == (97 - (bank + branch + account1 + account2) % 97);
    }

    return obj;
});
