/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/jqueryMaskPlugin' ], function($) {

    var obj = {
        initialize : function(field, opts) {
            field.find('input').mask('000 000 000');
        },
        validate : function(data, opts) {
            var value = data.asString().replace(/ /g, '');
            var validation = {
                validated : (value.length == 0 || (!isNaN(parseInt(value)) && value.length == 9 && checkLuhn(value)))
            };
            if (validation.validated) {
                validation.value = value;
            } else {
                validation.message = 'This value does not comply with a SIREN code.';
            }
            return validation;
        }
    };

    function checkLuhn(string) {
        var retNum = 0;
        for (var i = string.length - 1; i >= 0; i--) {
            var num = parseInt(string[i]);
            if ((string.length - i) % 2 === 0) {
                switch (num) {
                case 1: num = 2; break;
                case 2: num = 4; break;
                case 3: num = 6; break;
                case 4: num = 8; break;
                case 5: num = 1; break;
                case 6: num = 3; break;
                case 7: num = 5; break;
                case 8: num = 7; break;
                default: break;
                }
            }
            retNum += num;
        }
        return (retNum != 0 && retNum % 10 === 0);
    }

    return obj;
});
