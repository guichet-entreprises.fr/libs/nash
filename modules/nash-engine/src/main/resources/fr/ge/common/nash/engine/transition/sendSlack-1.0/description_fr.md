# Description

Cette transition permet d'envoyer un slack à un utilisateur 

# valeur(s) attendue(e)

| Nom | Description | Obligatoire |
|---|---|---|
| channel | Destinataire | Non |
| text | message | Oui |

# Résultat(s)

Création data-generated.xml avec le code response de l'api exchange.