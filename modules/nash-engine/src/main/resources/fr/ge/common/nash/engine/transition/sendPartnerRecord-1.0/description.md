# Description of send partner record transition:
<br />

The goal of this transition is to send partner generated record to backoffice.

<br />
<br />

# Transition inputs :
<br />

* code       : "XXX"
* attachment : "2-review/generated/document.pdf"

<br />
<br />


# Transition output : 
<br />

Create new record to partner receiver and upload it to backoffice.


