log.info("==> prepareZip preprocess");

var zip = nash.instance.zip();

['cerfa', 'regent', 'xmltc' ].forEach(function(key){
	if (null != _input.parameters.attachment[key]) {
		var entry = _input.parameters.attachment[key];
		zip.add(nash.util.resourceFromPath(entry).label, entry);
		log.info("Adding entry {} to zip file", key);
	}
});

for (var file in _input.parameters.attachment.others) {
    path = _input.parameters.attachment.others[file];
    zip.add(nash.util.resourceFromPath(path).label, path);
}

var extract = JSON.parse(nash.xml.extract(_input.parameters.attachment.regent, '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02'));
var args = {
	funcId :  _input.result.funcId,
	numeroLiasse : extract.C02
};

nash.record.meta([{'name':'liasse', 'value': extract.C02}]);

var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute', 'definir nom liasse') //
	.connectionTimeout(10000) //
	.receiveTimeout(10000)
	.accept('text') //
	.post(args);

var files = JSON.parse(response.asString());

var zipAttachment = zip.save('/' + files.zip).getAbsolutePath();

//Insert to display.xml
var data = nash.instance.load("display.xml");

data.bind("request",{
    content:{
    	zipAttachment:'/' + files.zip,
    	zipFiniAttachment:'/' + files.fini
    }
})
