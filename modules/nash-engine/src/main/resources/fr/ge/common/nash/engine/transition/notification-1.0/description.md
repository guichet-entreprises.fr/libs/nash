# Description of Tracker transition:
<br />

The goal of this transition is to send a message to Tracker.

<br />
<br />

# Input of Tracker transition:
<br />

* To send a message to TRACKER, we need:
    * referenceId : String corresponding to the reference to link the current record
    * content: Text corresponding to the message content to send

<br />
<br />


# Output of Tracker transition: 
<br />

Create data-generated.xml with response code of TRACKER api


