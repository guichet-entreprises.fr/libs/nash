# Description

La transition permet de trouver l'identifiant fonctionnel à partir des paramettre d'entres 

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| authorityType | Nom de l'algorithme | Oui |
| zipCode | Forme juridique | Oui |
| attachment | Option CMA CCI | Oui |

# Résultat(s)

Code de l'autorité compétente