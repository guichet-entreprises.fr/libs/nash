_log.info("===> preprocess findFuncIdReceiver");

//prepare info to send
var algo = _input.parameters.algo.algo;
_log.info("_input.parameters.algo.algo {}", algo);
var params = _input.parameters.algo.params;
_log.info("_input.parameters.algo.params {}", params);


var args = {};
for(var i=0; i<params.size();i++){
	args[params.get(i).key] = params.get(i).value;
}

logger.info("args passed to get the authorities code {}",args);

_log.info("parameters {}", args);

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('text') //
        .post(args);

_log.info("response  {}",response);

// Result value of functional id 
var codeAuthority = response.asString();
_log.info("codeAuthority  {}",codeAuthority);

// load data.xml
var data = nash.instance.load("display.xml");

// set value inside data.xml
data.bind("request",{
    input:{
        algo:algo,
        params:params,
    },
    output:{
        funcId:codeAuthority
    }
});
