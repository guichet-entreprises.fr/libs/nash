_log.info("===> preprocess findFuncIdReceiver");

//prepare info to send 
var algoGrp = _input.parameters.algo;
var algo = algoGrp.algo;
var secteur1 = algoGrp.secteur1;
var typePersonne = algoGrp.typePersonne;
var formJuridique = algoGrp.formJuridique;
var optionCMACCI = algoGrp.optionCMACCI;
var codeCommune = algoGrp.codeCommune;

var args = algoGrp;
_log.info("parameters {}", args);

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('text') //
        .post(args);
_log.info("response  {}",response);

// Result value of functional id 
var codeAuthority = response.asString();
_log.info("codeAuthority  {}",codeAuthority);

// load data.xml
var data = nash.instance.load("display.xml");

// set value inside data.xml
data.bind("request",{
    input:{
        algo:algo,
        formJuridique:formJuridique,
        optionCMACCI:optionCMACCI,
        codeCommune:codeCommune,
        typePersonne:typePersonne,
        secteur1:secteur1
    },
    output:{
        funcId:codeAuthority
    }
});
