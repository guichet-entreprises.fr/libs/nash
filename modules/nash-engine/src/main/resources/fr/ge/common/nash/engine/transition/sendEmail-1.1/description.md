# Description of send email transition:
<br />

The goal of this transition is to send an email using email channel.

<br />
<br />

# Transition inputs :
<br />

* funcId          : "2018-06-GHK-FU6001"
* funcLabel       : "URSSAF DE L'OISE"
* emailAddress    : "test-cma@yopmail.com"
* object          : "Réception d'un nouveau dossier Guichet Entreprises"
* content         : "Le nouveau dossier est disponible sur la fonction partenaire."
* attachment      : "2-review/generated/document.pdf"

<br />
<br />


# Transition output : 
<br />

Send an email.


