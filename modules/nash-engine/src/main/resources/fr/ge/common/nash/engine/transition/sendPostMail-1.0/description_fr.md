# Description

Le but de cette transition est d'envoyer  courrier papier.

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| referenceId | Référence du courrier | Non |
| recipientName | Nom du destinataire | Oui |
| recipientNameCompl | Complement du nom du destinataire | Non |
| recipientAddressName | Adresse | Oui |
| recipientAddressNameCompl | Complément d'adresse | Non |
| recipientPostalCode | Code postal | Oui |
| recipientCity | Ville | Oui |
| attachment | pièce jointe | Oui |

# Résultat(s)

Création data-generated.xmlavec le code response de l'api exchange.