# Description of Send slack transition:
</br>

The goal of  this transition is to send slack to specific user or channel.

<br />
<br />


# Input of Send slack transition:
<br/>

* To send slack to specific user or channel we need:
    * channel : channelId (example :@adil or #team-minecraft) 
    * text : String (example :"hello everybody welcome to our demonstration")

<br/>
<br/>


# Output of Send slack transition: 
<br/>

Create data.xml with response code of slack api


