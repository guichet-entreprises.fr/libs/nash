log.info('===> email notification preprocess');

//Retrieve mailingList
var mailingList = _input.result.notification.email;

log.info("typeof mailingList : " + typeof mailingList);
log.info("mailingList retrieved is : " + mailingList);

//Adapt mailing list for exchange format
var mailingList_adapted = [];
for(var it in mailingList){
	mailingList_adapted.push(mailingList[it]);
}
mailingList_adapted = mailingList_adapted.join(", ");

log.info("mailingList_adapted is : " + mailingList_adapted);

//Prepare mail info
var object = "Nouveau dossier disponible sur votre espace partenaire";
var backofficeId = _input.result.backoffice.backofficeId;
log.info("backofficeId : " + backofficeId);
var boURL = _CONFIG_ ? _CONFIG_.get('dashboard.bo.url') : null;
log.info("boURL : " + boURL);

var content = "Bonjour," +
		"\nUn nouveau dossier est disponible sur votre espace dédié sous la référence "+backofficeId+". Pour y accéder, veuillez suivre ce lien :\n" + //
		boURL+".\n" + //
		"\nCordialement," + //
		"\nLe service Guichet Entreprises";
log.info("content : " + content);

var data = nash.instance.load('display.xml');
data.bind("request", {
    'input': {
        'email' : mailingList,
        'object' : object,
        'content' : content
    },
    'output' : {
        'url' : _CONFIG_ ? _CONFIG_.get('exchange.baseUrl') : null
    },
    'backoffice' : {
        'backofficeId' : backofficeId
    }
});

//-->Call Exchange service to send an email without attachment
try {
    var response = nash.service.request('${exchange.baseUrl}/email/sendWithoutAttachement') //
    .connectionTimeout(2000) //
    .receiveTimeout(5000) //
    .dataType("application/json") //
    .param("sender", _CONFIG_.get('exchange.email.sender')) //
    .param("recipient", mailingList_adapted) //
    .param("object", object) //
    .param("content", content)
	.post(null);

    if(response.status == 200){
        // Response
        data.bind("request", {
            'output' : {
                'status' : response.status + '',
                'confirm' : 'OK',
                'response' : 'Votre courrier électronique a été envoyé avec succès.'
            }
        });
    } else {
        // Response
        data.bind("request", {
            'output' : {
                'status' : response.status + '',
                'response' : 'Une erreur technique est survenue lors de l\'envoi de votre courrier électronique. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
            }
        });
    }
} catch (error) {
    // Response
    log.info('error while sending an email => ' + error);
    data.bind("request", {
            "output" : {
                'response' : error + '\nCATCH : Une erreur technique est survenue lors de l\'envoi de votre courrier électronique. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
            }
    });
}
