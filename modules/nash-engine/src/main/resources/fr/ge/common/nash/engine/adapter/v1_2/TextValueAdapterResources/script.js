/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery' ], function($) {

    var obj = {
        initialize : function(field, opts) {
	        initTextarea($('textarea', field), $('#minLength', field), $('#maxLines', field), opts);
        },
        validate : function(data, opts) {
        	value = data.asString();
            var validation = {
                validated : (validateTextarea(value, opts) != false)
            };
            if (validation.validated) {
	            validation.value = value;
	        } else {
	            validation.message = 'This value does not comply.';
	        }
	        return validation;
        }
    }; 

    function initTextarea(textarea, divMinLength, divMaxLines, opts) {
        var maxLines = opts.maxLines;
        var maxCharPerLine = opts.maxCharPerLine;
        var minChars = opts.minChars;
        var caretPos = -1;
        var countChar = 0;
        
        textarea.on('keyup', function() {
    	   //reset errors msg on keydown
        	divMinLength.html('');
        	divMaxLines.html('');
        
		   //get Textearea text
           var text = $(this).val();

           //Split with \n carriage return
           var lines = text.split("\n");

           //display the chars counter
           if(minChars && text.length < minChars){
        	   divMinLength.html("This value is too short. It should have " + minChars + " characters or more.\n");
           }

           //display the lines counter
           if(maxLines){
         	  var diff = maxLines - lines.length;
         	  if(diff < 0){
         		  diff = 0;
         	  }
         	  divMaxLines.html(diff +" line(s) remaining.");
           }
           
    		//control lines length
            countChar = 0;
            for (var i = 0; i < lines.length; i++) {
            	
            	//get count chars until lines[i]
	        	countChar = countChar + lines[i].length;
	        	
				//if lines[i] length > maxCharPerLine
				if (lines[i].length > maxCharPerLine) {
	                
	                //initialize lines[i+1] if doesn't exist
	                if (typeof lines[i + 1] === 'undefined') {
	                     
						lines[i + 1] = null;
						
						//push on lines[i + 1] the last inserted char in lines[i]
						lines[i + 1] = lines[i].substring(maxCharPerLine, maxCharPerLine+1);
	                }
	                //if lines[i+1] exist
	                else{
						if(lines.length < maxLines){
			          		lines.splice(i+1, 0, lines[i].substring(maxCharPerLine, maxCharPerLine+1));
			          		caretPos = countChar+2;
			            }
	                }
			        //delete the last inserted char in line[i]
                    lines[i] = lines[i].substring(0, maxCharPerLine);
	            }
            }
 
            //delete lines > maxLines
            while (lines.length > maxLines) {
                lines.pop();
            } 

            //Join with \n.
            //Set textarea.
            $(this).val(lines.join("\n"));
 
            //Set cursor position
            if(caretPos > -1){
            	$(this).focus();
            	$(this)[0].setSelectionRange(caretPos, caretPos);
            	
            	//Reset var position
            	caretPos = -1;
          	  }
        });
    }  
    
    
    function validateTextarea(value, opts) {
        var maxLines = opts.maxLines;
        var maxCharPerLine = opts.maxCharPerLine;
        var minChars = opts.minChars;

        //Split with \n carriage return
        var lines = value.split("\n");

        //value < minChars
        if(minChars && value.length < minChars){
            return false;
        }
        
        
        //if line length > maxCharPerLine
        for (var i = 0; i < lines.length; i++) {
			if(lines[i].length > maxCharPerLine) {
                return false;
            }
        }
        
        return true;
    }
    return obj;
});
