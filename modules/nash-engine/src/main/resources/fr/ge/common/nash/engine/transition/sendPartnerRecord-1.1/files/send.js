log.info('===> backoffice preprocess');

var funcId = _input.result.funcId;
var funcLabel = _input.result.backoffice.funcLabel;
var funcPath = _input.result.backoffice.funcPath;
var recordUid = nash.record.description().recordUid;
var recordTitle = nash.record.description().title;
var author = !nash.record.description().author ? "" : nash.record.description().author;
var backofficeBaseUrl = _CONFIG_ ? _CONFIG_.get('nashBo.baseUrl') : '';
var cerfa = _input.parameters.attachment.cerfa;
var regent = !_input.parameters.attachment.regent ? undefined : _input.parameters.attachment.regent;
var xmltc = !_input.parameters.attachment.xmltc ? undefined : _input.parameters.attachment.xmltc;
var denomination = !_input.result.denomination ? "" : _input.result.denomination;

var data = nash.instance.load("display.xml");
data.bind("request", {
    "input" : {
        "funcId" : funcId,
        "funcLabel" : funcLabel
    },
    'output' : {
        'url' : backofficeBaseUrl
    }
});

var others = [];
var files = [];
for (var file in _input.parameters.attachment.others) {
	fileId = _input.parameters.attachment.others[file].id;
	filePath = _input.parameters.attachment.others[file].label;
    if (file != null){
    	others.push({
			id:fileId,
			label:filePath
		});
    }
    files.push(nash.util.resourceFromPath(_input.parameters.attachment.others[file].getLabel()));
}
data.bind('request', {
    'attachment' : {
        'cerfa' : _input.parameters.attachment.cerfa,
        'regent' : _input.parameters.attachment.regent,
        'xmltc' : _input.parameters.attachment.xmltc,
        'others' : others
    }
});

var recordBo = nash.instance.from('${nashBo.baseUrl}').createRecord('local:backoffice');
recordBo.description.setAuthor(funcId);
recordBo.description.setTitle(recordTitle);
recordBo.description.setFormUid(recordUid);

//--> Cerfa, Regent and XMLTC if necessary
files.push(nash.util.resourceFromPath(cerfa));
if (undefined !== regent) {
	files.push(nash.util.resourceFromPath(regent));
}
if (undefined !== xmltc) {
	files.push(nash.util.resourceFromPath(xmltc));
}

var informations = {
	    "recordUid" : recordUid,
	    "recordTitle" : recordTitle,
	    "denomination" : denomination,
	    "destFuncId" : funcId,
	    "destLabel" : funcLabel,
	    "transmissionDate" : new Date().toLocaleDateString()
	};

var metas = [];

// Adding the denomination to the metas
if(denomination != "") {
	metas.push({'name':'denomination', 'value': denomination});
}

//Adding the author (declarant) to the metas
if(author != "") {
	metas.push({'name':'author', 'value': author});
}

//Adding the recordTitle to the metas
if(recordTitle != "") {
	metas.push({'name':'title', 'value': recordTitle});
}

//Adding meta to link BO record and user record
if(recordUid != "") {
	metas.push([{'name':'uid', 'value': recordUid}]);
}

//Adding the authorityId
if(funcId != "") {
	metas.push([{'name':'authorityId', 'value': funcId}]);
}

//Adding the authorityId
if(funcLabel != "") {
	metas.push([{'name':'authorityLabel', 'value': funcLabel}]);
}


//--> Extracting liasse number
if (undefined !== regent) {
    var extract = JSON.parse(nash.xml.extract(regent, '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02'));
    log.info('Extracted liasse number from XML Regent => ' + extract.C02);
    
    // Adding liasse number to the informations
    informations.liasseNumber = extract.C02;
    
    // Adding the liasseNumber to the metas
    metas.push({'name':'liasse', 'value': extract.C02});
   
    //-->Bind liasse number
    data.bind("request", {
        'output' : {
            'liasse' : extract.C02
        }
    });
}

// Saving the metas
nash.record.meta(metas);

// Binding summary.xml
var summary = recordBo.load('1-summary/data.xml');
summary.bind("view", { 
	"informations" : informations
});
summary.set('view.informations.attachment', files);

/*
 * Set documents meta after binding them in order to have correct destination path
 */
summary.get('view.informations.attachment').forEach(function (item) { metas.push({ 'name': 'document', 'value': item.getAbsolutePath() }); });
recordBo.meta(metas);

//-->Allow partners user and referent to the BO record
recordBo.role([
    {
        "entity" : funcPath,
        "role" : 'user'
    },
    {
        "entity" : funcPath,
        "role" : 'referent'
    }
]);

try {
    var recordBoUid = recordBo.save();
    nash.tracker.link(recordBoUid);
    log.info('Backoffice record identifier {}', recordBoUid);
    data.bind("request", {
        "output" : {
             'status' : "200",
             'confirm' : 'OK',
             'backofficeId' : recordBoUid,
             'response' : 'Le dossier a été crée avec succès dans le Backoffice.'
        }
    });
    //Adding the recordBoUid to the metas
    nash.record.meta([{'name':'uid', 'value': recordBoUid}]);
} catch (error) {
    log.error("Backoffice error : " + error);
    data.bind("request", {
        "output" : {
             'status' : "500",
             'response' : "Une erreur technique est survenue lors de l\'envoi dans le Backoffice. Veuillez contacter le support technique à l'adresse suivante : support.guichet-entreprises@helpline.fr."
        }
    });
}


