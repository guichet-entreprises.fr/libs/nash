# Description

La transition permet de trouver le canal activé pour une autorité.

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| funcId | Code de l'autorité compétente| Oui |
| mandatoryChannel | Canal requis pour forcer le canal d'envoi, valeurs :(backOfficeChannel,addressChannel,emailChannel,ftpChannel) | Non |

# Résultat(s)

Liste des cannaux actives.