/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/i18n' ], function($, i18n) {
    
    var obj = {
        initialize : function(field, opts) {
            var fieldName = field.attr('data-field');
            var monetaryField = field.find('input[type=text]');

            // Restrict input to numeric chars
            monetaryField.on("input", function(evt) {
               var self = $(this);
               self.val(self.val().replace(/[^0-9\.\,]/g, ''));
               if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
               {
                 evt.preventDefault();
               }
            });
            
            // On mouse out, format the amount
            monetaryField.on('blur', function() {
                // Call a function to format the amount
                if(monetaryField.val() != ""){
                     formatField(monetaryField);
                }
            });
        
            // On mouse in, replace string with a float
            monetaryField.click(function () {
                $(this).val(stringToFloat($(this).val()));
            });
        },
        validate : function(data, opts) {
            var amount = stringToFloat(data.asString());
            return {
                validated : data.asString() == '' || (amount != null && amount >= 0 && amount <= 100),
                value : stringToFloat(amount)
            };
        }
    };

    // Return input as float
    function stringToFloat(str) {
        var result = str.replace(/[^0-9\.\,]/g, '').replace(/,/g, '.');
        var fragments = result.split('.');
        
        if(fragments.length > 1){
            result = "";
            for(var i = 0; i < fragments.length - 1; i++){
                result = result + "" + fragments[i];
            }
            result = result + "." + fragments[fragments.length - 1];
        }
        return result;
    }
    
    // Function to format the amount
    function formatField(monetaryField){
        // Input amount without spaces
        var userLang = navigator.language || navigator.userLanguage;
        var amount = stringToFloat(monetaryField.val());
        
        amount = Number(amount).toLocaleString(userLang, {maximumFractionDigits : 2});
        
        //insert the formated amount
        monetaryField.val(amount);
        
      };
    
    return obj;
});
