# Description

La transition permet d'envoyer un email avec une pièce jointe

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| cerfa | Chemin du fichier cerfa | Oui |
| others | autres documents | Non |
| funcLabel | Libellé de l'authorité compétente | Oui |
| email | Adresse électronique | Non |

# Résultat(s)

un fichier output.xml avec le mail suivant:

Objet: Réception d'un nouveau dossier Guichet Entreprises

Contenu: Bonjour {},
Le service Guichet Entreprises vous informe que le nouveau dossier {}  est en attente de traitement.
Vous trouverez joint à ce courriel ledit dossier
Le service Guichet Entreprises vous remercie.
L’équipe du Guichet Entreprises