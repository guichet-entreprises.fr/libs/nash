# Description

La transition permet d'envoyer un fichier sur 'EDDIE'

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| attachment | Chemin du fichier | Oui |
| regent | Chemin du fichier regent | Non |
| funcId | Code de l'autorité compétente | Oui |
| codeEdi | Code EDI de l'autorité compétente | Non |
| token | token | Oui |
| comment | Commentaire | Non |
| pathFolder | Folder path  | Oui |

# Résultat(s)

Création display.xml avec le code reponse  de l'api eddie.
