# Description

La transition permet d'envoyer un dossier à une autorité.


# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| algo | Nom de l'algorithme | Non |
| formJuridique | Forme juridique | Non |
| optionCMACCI | Option CMA CCI | Non |
| codeCommune | Code commune | Non |
| typePersonne | Type de personne | Non |
| secteur1 | Secteur | Non |
| cerfa | Chemin du fichier cerfa | Oui |
| regent | Chemin du fichier regent | Non |
| others | Prévisualisation des fichiers | Oui |
| xmltc | Chemin du fichier xmlTc | Non |


# Résultat(s)

Code de l'autorité compétente