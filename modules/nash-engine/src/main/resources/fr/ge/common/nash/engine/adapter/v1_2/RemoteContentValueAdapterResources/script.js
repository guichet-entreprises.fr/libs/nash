/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'handlebars', '_fm_data', 'lib/i18n', 'lib/func/localScopeModel', '_fm_description' ], function($, Handlebars, _data, i18n, localScopeModel, description) {

    var remoteContentCache = {};

    function loadRemoteContent(url, field, opts, cb) {
        var path = field.attr('data-field');
        var nfo = {
                'url' : url,
                'data': opts.data ? buildData(path, opts.data) : undefined
            };
        var key = JSON.stringify(nfo);
        var remoteContent = remoteContentCache[key];

        if (undefined === remoteContent) {
            var headers = {};
            //-->Set header Authorization
            if (opts.basicAuthorization) {
            	headers['Authorization'] = opts.basicAuthorization; 
            }
            if (undefined !== nfo.data) {
                headers['Accept'] = 'application/json';
                headers['Content-Type'] = 'application/json';
            }
            var remoteContentBody = $('div[name$=".body"]', field);
            var spinner = $("<div class='fa fa-spinner fa-spin'></div>");
            remoteContentBody.append(spinner);
            $.ajax(nfo.url, {
            	'headers' : headers,
                'type' : opts.method ? opts.method : 'GET',
                'data' : nfo.data,
                'dataType' : opts.dataType ? opts.dataType : "html"
            }).done(function (res, status, xhr) {
                spinner.remove();
                var ct = xhr.getResponseHeader("content-type") || "";
                if (ct.indexOf('json') > -1 && opts.template) {
                    var tpl = Handlebars.compile(opts.template);
                    res = tpl(res);
                }
                cb && cb(remoteContentCache[key] = res);
            }).fail(function () {
                spinner.remove();
                console.warn('enable to retrieve HTML content from [' + nfo.url + ']');
                cb && cb(i18n('No content'));
            });
        } else {
            cb && cb(remoteContent);
        }
    }

    function buildData(path, src) {
        var tpl = Handlebars.compile(src);
        var model = $.extend({}, _data);
        model._group = localScopeModel(path);
        model._description = description;

        return tpl(model);
    }

    var obj = {
        initialize : function(field, opts) {
            var path = field.attr('data-field');
            var remoteContentPrepend = $('a[name$=".prepend"]', field);
            var remoteContentBody = $('div[name$=".body"]', field);
            var remoteContentAppend = $('a[name$=".append"]', field);
            var remoteContentUrl = opts.src || '';
            
            remoteContentBody.empty();
            function showRemoteContent() {
                if (undefined !== remoteContentUrl) {
                    var url = buildData(path, remoteContentUrl);
                    loadRemoteContent(url, field, opts, function (remoteContent) {
                        remoteContentBody.empty();
                        remoteContentBody.append(remoteContent);
                    });
                } else {
                    remoteContentBody.empty();
                    remoteContentBody.append(i18n('No content'));
                }

                remoteContentPrepend.addClass('hide');
                remoteContentBody.removeClass('hide');
                remoteContentAppend.removeClass('hide');
            }
            
            if (true === opts.display) {
            	showRemoteContent();
            }

            remoteContentPrepend.off('click').on('click', function (evt) {
                evt.preventDefault();
                showRemoteContent();
            });

            remoteContentAppend.off('click').on('click', function (evt) {
                evt.preventDefault();
                remoteContentPrepend.removeClass('hide');
                remoteContentBody.addClass('hide');
                remoteContentAppend.addClass('hide');
            });
        },
        validate : function(data, opts) {
            return {
                validated : true,
                value : ""
            };
        }
    };


    return obj;
});
