/**
 * Utility functions
 */
define('util', function() {

    var Util = {};

    Util.extend = function(dst) {
        var k;

        Array.prototype.slice.call(arguments, 0).forEach(function(elm) {
            if (elm) {
                if (elm.hasOwnProperty) {
                    for (k in elm) {
                        if (elm.hasOwnProperty(k)) {
                            dst[k] = elm[k];
                        }
                    }
                } else {
                    for (k in elm) {
                        dst[k] = elm[k];
                    }
                }
            }
        });

        return dst;
    };

    Util.findNode = function (root, path) {
        var keys = path.split('.');

        var ctx = root;
        for (var key = '_$' + keys.shift(); null != key && null != ctx; key = keys.shift()) {
            ctx = ctx[key]
        }

        return ctx;
    };

    return Util;

});

