_log.info("===> postprocess sendPostMail");

nash.instance //
    .load('output.xml') //
    .bind('result', {
        'tracker' : {
        	'referenceId' : 'ENVOI-DOSSIER-IN',
            'message' : "Une demande d'envoi de courrier papier a été déposée avec la référence " + _input.request.output.exchangeId + " à l'autorité compétente " + _input.request.input.funcLabel
        }
    });
