# Description

La transition permet d'envoyer un dossier à une autorité 
le v2 de la transition permet d'ajouter des paramètres sous format clé--> valeur


# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| params[key] | Clé(optionCMACCI,codeCommune,codeCommune,typePersonne,secteur1) | Oui |
| params[value] | valeur de la clé | Oui |
| cerfa | Chemin du fichier cerfa | Oui |
| regent | Chemin du fichier regent | Non |
| others | Prévisualisation des fichiers | Oui |
| xmltc | Chemin du fichier xmlTc | Non |

# Résultat(s)

Code de l'autorité compétente