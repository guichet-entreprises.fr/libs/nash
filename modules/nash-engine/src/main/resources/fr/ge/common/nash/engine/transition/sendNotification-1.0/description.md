# Description of send email transition:
<br />

The goal of this transition is to send an email notification to a mailing list configured for an authority.

<br />
<br />

# Transition inputs :
<br />

* funcId          : "2018-06-GHK-FU6001"
* funcLabel       : "URSSAF DE L'OISE"
* emailList		  : ["tata@yopmail.com", "toto@yopmail.com", ...]
* object          : "Notification : un nouveau dossier Guichet Entreprises est arrivé au backoffice"
* content         : "Bonjour,
Un nouveau dossier est disponible sur votre espace dédié sous la référence <dossier_backoffice>. Pour y accéder, veuillez <Cliquer ici>.
Cordialement,
Le service Guichet Entreprises"

<br />
<br />


# Transition output : 
<br />

Send an email.


