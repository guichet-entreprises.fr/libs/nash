# Description

Le but de cette transition est de récupérer la liste de diffusion configurée pour un partenaire afin d'envoyer un e-mail informant qu'un enregistrement est arrivé au backoffice

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| funcId | Code de l'autorité compétente | Oui |

# Résultat(s)

Liste de diffusion configurée pour informer un partenaire..