(function () {

    return root.define("meta", function (nash) {

        var _form;
        var _currentMetaIndex = -1;

        var meta = function() {

            ++_currentMetaIndex;

            var currentSpec = nash.instance.getOutputSpecification();
            log.debug('Current resource name : {}', currentSpec.resourceName);

            if (0 == _currentMetaIndex) {
                currentSpec.groups.clear();
            }

            var currentGroup = nash.instance.createGroup({
                id : 'meta' + (_currentMetaIndex + 1),
                label : 'meta compétent',
                data : []
            });

            currentSpec.groups.add(currentGroup);

            log.debug('Group retrieve');

            log.debug('Return function');

            return {
                _group : currentGroup,

                add : function(key, value) {
                    log.debug('Adding value "{}" for "{}"', value, key);

                    currentGroup.data.add(nash.instance.createData({
                        id : key,
                        value : value,
                        type : 'String',
                        label : key + ' value'
                    }));

                    return this;
                }
            };
        };

        return meta;

    });

})();
