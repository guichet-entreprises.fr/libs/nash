/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/jqueryMaskPlugin' ], function($) {

    var obj = {
        initialize : function(field, opts) {
            field.find('input').mask('SS00 AAAA AAAA AAZZ ZZZZ ZZZZ ZZZZ ZZZZ ZZ', {
                translation: {
                    'Z': {
                        pattern: /[A-Za-z0-9]/,
                        optional: true
                    },
                    'S': {
                        pattern: /[A-Z]/
                    }
                }
            });
        },
        validate : function(data, opts) {
            var value = data.asString().replace(/ /g, '');
            var pattern = "^([a-zA-Z]){2}([0-9]){2}([0-9a-zA-Z]){11,30}$";
            var validation = {
                validated : (value.length == 0 || (value.length >= 15 && value.length <= 34
                        && new RegExp(pattern).exec(value) != null && checkCountryPattern(value) && checkKey(value)))
            };
            if (validation.validated) {
                validation.value = value;
            } else {
                validation.message = 'This value does not comply with an IBAN.';
            }
            return validation;
        }
    };

    function checkCountryPattern(string) {
        // ISO 13616-Compliant IBAN Format, version 66, May 2016
        // https://www.swift.com/sites/default/files/resources/swift_standards_ibanregistry.pdf
        var countries = {
            'AL' : { ibanLength : 28, bbanPattern : '8!n16!c' },             'AD' : { ibanLength : 24, bbanPattern : '4!n4!n12!c' },
            'AT' : { ibanLength : 20, bbanPattern : '5!n11!n' },             'AZ' : { ibanLength : 28, bbanPattern : '4!a20!c' },
            'BH' : { ibanLength : 22, bbanPattern : '4!a14!c' },             'BE' : { ibanLength : 16, bbanPattern : '3!n7!n2!n' },
            'BA' : { ibanLength : 20, bbanPattern : '3!n3!n8!n2!n' },        'BR' : { ibanLength : 29, bbanPattern : '8!n5!n10!n1!a1!c' },
            'BG' : { ibanLength : 22, bbanPattern : '4!a4!n2!n8!c' },        'CR' : { ibanLength : 21, bbanPattern : '3!n14!n' },
            'HR' : { ibanLength : 21, bbanPattern : '7!n10!n' },             'CY' : { ibanLength : 28, bbanPattern : '3!n5!n16!c' },
            'CZ' : { ibanLength : 24, bbanPattern : '4!n6!n10!n' },          'DK' : { ibanLength : 18, bbanPattern : '4!n9!n1!n' },
            'FO' : { ibanLength : 18, bbanPattern : '4!n9!n1!n' },           'GL' : { ibanLength : 18, bbanPattern : '4!n9!n1!n' },
            'DO' : { ibanLength : 28, bbanPattern : '4!c20!n' },             'EE' : { ibanLength : 20, bbanPattern : '2!n2!n11!n1!n' },
            'FI' : { ibanLength : 18, bbanPattern : '6!n7!n1!n' },           'FR' : { ibanLength : 27, bbanPattern : '5!n5!n11!c2!n' },
            'GE' : { ibanLength : 22, bbanPattern : '2!a16!n' },             'DE' : { ibanLength : 22, bbanPattern : '8!n10!n' },
            'GI' : { ibanLength : 23, bbanPattern : '4!a15!c' },             'GR' : { ibanLength : 27, bbanPattern : '3!n4!n16!c' },
            'GT' : { ibanLength : 28, bbanPattern : '4!c20!c' },             'HU' : { ibanLength : 28, bbanPattern : '3!n4!n1!n15!n1!n' },
            'IS' : { ibanLength : 26, bbanPattern : '4!n2!n6!n10!n' },       'IE' : { ibanLength : 22, bbanPattern : '4!a6!n8!n' },
            'IL' : { ibanLength : 23, bbanPattern : '3!n3!n13!n' },          'IT' : { ibanLength : 27, bbanPattern : '1!a5!n5!n12!c' },
            'JO' : { ibanLength : 30, bbanPattern : '4!a4!n18!c' },          'KZ' : { ibanLength : 20, bbanPattern : '3!n13!c' },
            'XK' : { ibanLength : 20, bbanPattern : '4!n10!n2!n' },          'KW' : { ibanLength : 30, bbanPattern : '4!a22!c' },
            'LV' : { ibanLength : 21, bbanPattern : '4!a13!c' },             'LB' : { ibanLength : 28, bbanPattern : '4!n20!c' },
            'LI' : { ibanLength : 21, bbanPattern : '5!n12!c' },             'LT' : { ibanLength : 20, bbanPattern : '5!n11!n' },
            'LU' : { ibanLength : 20, bbanPattern : '3!n13!c' },             'MK' : { ibanLength : 19, bbanPattern : '3!n10!c2!n' },
            'MT' : { ibanLength : 31, bbanPattern : '4!a5!n18!c' },          'MR' : { ibanLength : 27, bbanPattern : '5!n5!n11!n2!n' },
            'MU' : { ibanLength : 30, bbanPattern : '4!a2!n2!n12!n3!n3!a' }, 'MD' : { ibanLength : 24, bbanPattern : '2!c!18!c' },
            'MC' : { ibanLength : 27, bbanPattern : '5!n5!n11!c2!n' },       'ME' : { ibanLength : 22, bbanPattern : '3!n13!n2!n' },
            'NL' : { ibanLength : 18, bbanPattern : '4!a10!n' },             'NO' : { ibanLength : 15, bbanPattern : '4!n6!n1!n' },
            'PK' : { ibanLength : 24, bbanPattern : '4!a16!c' },             'PS' : { ibanLength : 29, bbanPattern : '4!a21!c' },
            'PL' : { ibanLength : 28, bbanPattern : '8!n16!n' },             'PT' : { ibanLength : 25, bbanPattern : '4!n4!n11!n2!n' },
            'QA' : { ibanLength : 29, bbanPattern : '4!a21!c' },             'RO' : { ibanLength : 24, bbanPattern : '4!a16!c' },
            'LC' : { ibanLength : 32, bbanPattern : '4!a24!c' },             'SM' : { ibanLength : 27, bbanPattern : '1!a5!n5!n12!c' },
            'ST' : { ibanLength : 25, bbanPattern : '8!n11!n2!n' },          'SA' : { ibanLength : 24, bbanPattern : '2!n18!c' },
            'RS' : { ibanLength : 22, bbanPattern : '3!n13!n2!n' },          'SC' : { ibanLength : 31, bbanPattern : '4!a2!n2!n16!n3!a' },
            'SK' : { ibanLength : 24, bbanPattern : '4!n6!n10!n' },          'SI' : { ibanLength : 19, bbanPattern : '5!n8!n2!n' },
            'ES' : { ibanLength : 24, bbanPattern : '4!n4!n1!n1!n10!n' },    'SE' : { ibanLength : 24, bbanPattern : '3!n16!n1!n' },
            'CH' : { ibanLength : 21, bbanPattern : '5!n12!c' },             'TL' : { ibanLength : 23, bbanPattern : '3!n14!n2!n' },
            'TN' : { ibanLength : 24, bbanPattern : '2!n3!n13!n2!n' },       'TR' : { ibanLength : 26, bbanPattern : '5!n1!n16!c' },
            'UA' : { ibanLength : 29, bbanPattern : '6!n19!c' },             'AE' : { ibanLength : 23, bbanPattern : '3!n16!n' },
            'GB' : { ibanLength : 22, bbanPattern : '4!a6!n8!n' },           'VG' : { ibanLength : 24, bbanPattern : '4!a16!n' },
        };
        var country = string.substring(0, 2);
        var format = countries[country];
        if (!format) {
            return true;
        }
        if (string.length != format.ibanLength) {
            return false;
        }
        // The following character representations are used :
        // n : digits (numeric characters 0 to 9 only)
        // a : upper case letters (alphabetic characters A-Z only)
        // c : upper and lower case alphanumeric characters (A-Z, a-z and 0-9)
        // The following length indications are used :
        // nn! : fixed length
        // nn : maximum length
        var bbanPattern = format.bbanPattern;
        bbanPattern = bbanPattern.replace(/(\d)+/gi, function replace(d) {
            return '#' + d
        });
        var segments = bbanPattern.split('#');
        var pattern = '^';
        for (var i = 1; i < segments.length; ++i) {
            var matches = /(\d+)(!)?([nac])/gi.exec(segments[i]);
            var length = matches[1];
            var fixedLength = matches[2];
            var chars = matches[3];
            pattern += '[';
            if (chars == 'n') {
                pattern += '0-9';
            } else if (chars == 'a'){
                pattern += 'A-Z';
            } else {
                pattern += 'A-Za-z0-9';
            }
            pattern += ']{';
            if (fixedLength) {
                pattern += length;
            } else {
                pattern += '0,' + length;
            }
            pattern += '}';
        }
        pattern += '$';
        return new RegExp(pattern).exec(string.substring(4)) != null;
    }

    function checkKey(string) {
        var upperString = string.toUpperCase();
        var upperString = upperString.substring(4) + upperString.substring(0, 4);
        var translatedString = '';
        var aOrd = 'A'.charCodeAt(0);
        for (var i = 0; i < upperString.length; ++i) {
            var currChar = upperString[i];
            var currCharOrd = currChar.charCodeAt(0);
            if (currCharOrd >= aOrd) {
                translatedString += (currCharOrd - aOrd + 10);
            } else {
                translatedString += currChar;
            }
        }
        var remaining = translatedString;
        var processing = '';
        while (remaining.length > 0) {
            var addToProcessingLength = 9 - processing.length;
            processing = processing + remaining.substring(0, addToProcessingLength);
            processing = parseInt(processing) % 97;
            processing = '' + processing;
            remaining = remaining.substring(addToProcessingLength);
        }
        return processing == '1';
    }

    return obj;
});
