//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'sendMail',
    label : 'send a email',
    preprocess : 'preprocess.xml',
    data : '1-check-data-generated.xml',
    postprocess : 'postprocess.xml',
    icon : 'envelope-o',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/gen-data.js');
step.copy('files/postprocess.xml');
step.copy('files/send-email.js');

// ------------------------------------------------------------------------
log.info('Step created');
