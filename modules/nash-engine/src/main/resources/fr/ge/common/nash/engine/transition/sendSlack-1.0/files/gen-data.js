//------------------------------------------------------------------------
//Generate the page to confirm slack message
//------------------------------------------------------------------------
var grp = _INPUT_.slack;


_log.info("group is  {}",grp);

//input slackId of receiver
slackId = !grp.slackId ? null :grp.slackId;

_log.info("slackId is  {}",slackId);

message = !grp.message ? null :grp.message;


//Genereate data.xml with data-to-send 
return spec.create({
		id: 'Send',
		label: "Preparation de l'envoi de slack message",
		groups: [spec.createGroup({
				id: 'slack',
				label: "Contenu du slack message",
				data: [spec.createData({
						id: 'slackId',
						label: "id slack",
						type: 'string',
						mandatory: true,
						value: slackId
					}), spec.createData({
						id: 'message',
						label: "le contenu du message",
						type: 'Text',
						mandatory: true,
						value: message
					})
				]
			})]
});
