# Description

Permet de récupérer les informations d'une autorité.

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| funcId | Code de l'autorité compétente | Oui |
| attachment | Le chemin de la pièce jointe | Oui |
| channel | Le choix du canal | Oui |

# Résultat(s)

création d'un data-generated.xml contenant l'ensemble des informations sur l'autorité.