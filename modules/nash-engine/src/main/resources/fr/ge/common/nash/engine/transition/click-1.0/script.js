log.info('Post a click for the record {}', nash.record.description().recordUid);

var funcLabel = _input.result.funcLabel;

var eventName = 'PROCESS/' + nash.record.description().reference.code + '/send/' + funcLabel;
var recordUid = nash.record.description().recordUid;
var counter = 1;

var clickerResponse = null;

var date = new Date();

var effectDate =[date .getFullYear(),
          ((date .getMonth() + 1) >9 ? '' : '0') + (date .getMonth() + 1),
          ((date .getDate())>9 ? '' : '0') + (date .getDate())
         ].join('');

try {
	clickerResponse = nash.service.request('${ws.clicker.url}/v1/event') //
		.dataType('application/json') //
		.accept('json') //
		.param('ref', eventName) //
		.param('effectDate', effectDate) //
		.param('counter', counter) //
		.param('comment', recordUid) //
		.post(null) //
	;
	
	log.info('Click successfully for event {} with effect date : {}', eventName, effectDate);
	
} catch (e) {
	log.error('An technical error occured when calling clicker with reference {} at effect date {} with comment : {}', eventName, effectDate, recordUid);	
	log.error(e);
}
