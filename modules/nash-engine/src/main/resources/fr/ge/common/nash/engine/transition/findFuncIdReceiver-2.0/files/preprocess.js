_log.info("===> presprocess findFuncIdReceiver");

//prepare info to send 
var algo = _INPUT_.view.algo;
var secteur1 = _INPUT_.view.arguments.secteur1;
var typePersonne = _INPUT_.view.arguments.typePersonne;
var formJuridique = _INPUT_.view.arguments.formJuridique;
var optionCMACCI = _INPUT_.view.arguments.optionCMACCI;
var codeCommune = _INPUT_.view.arguments.codeCommune;
var attachment = _INPUT_.view.attachement;

var args = _INPUT_.view.arguments;
_log.info("serviceParams {}",JSON.stringify(args));

// call directory service to find functional id of authority 
var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute',algo) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000)
        .accept('text') //
        .post(args);
_log.info("response  {}",response);

// Result value of functional id 
var codeAuthority = response.asString();
_log.info("codeAuthority  {}",codeAuthority);


// load data.xml
var data = nash.instance.load("data-generated.xml");

// set value inside data.xml
data.bind("view",{
	search:{
		algo:algo,
		input:{
			formJuridique:formJuridique,
			optionCMACCI:optionCMACCI,
			codeCommune:codeCommune,
			typePersonne:typePersonne,
			secteur1:secteur1
		},
		output:{
			funcId:codeAuthority
		}
	},
	others:{
		attachment:attachment
	}
})
