log.info("=> starting merge step");

// Initialize new empty file
var tmpDocument = nash.doc.create();

var attachments = [];
var fileId = null;
var filePath = null;
for (var file in _input.parameters.content.entry) {
	fileId = _input.parameters.content.entry[file].id;
	filePath = _input.parameters.content.entry[file].label;
    var file = nash.doc.load(filePath);
    if(file != null){
    	var savedFile = file.save(filePath);
    	tmpDocument.append(true, savedFile);
    	
    	attachments.push({
			id:fileId,
			label:filePath
		});
    }
}

//Save the merged file
nash.record.saveFile('document.pdf', tmpDocument.save('document.pdf').getContent()); 

//Insert to display.xml
var data = nash.instance.load("display.xml");

data.bind("request",{
	input:{
		entry:attachments
	},
	content:{
		attachment:{
			id:"document",
			label:'/document.pdf'
		}
	}
})