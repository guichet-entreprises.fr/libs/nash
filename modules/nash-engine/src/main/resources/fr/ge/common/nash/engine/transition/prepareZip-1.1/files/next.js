log.info("==> prepareZip postprocess");

var zipAttachment = _input.request.content.zipAttachment;
var zipFiniAttachment = _input.request.content.zipFiniAttachment;

var data = nash.instance.load('output.xml');

data.bind("parameters", {
   content : {
	   zipAttachment : zipAttachment,
	   zipFiniAttachment : zipFiniAttachment
   }
});