(function () {

    return root.define("pdf", function (nash) {

        var util = require('util');

        var PdfFactory = function() {
        };

        PdfFactory.create = function(srcResourceName, values) {
            var o = {
                _type : 'pdf',
                _src : srcResourceName
            };
            if (values) {
                o = util.extend(o, values);
            }
            return o;
        };

        PdfFactory.save = function(dstResourceName, obj) {
            if (nash && nash.doc) {
                var doc = nash.doc.load(obj._src);
                doc.apply(obj);
                return doc.save(dstResourceName);
            } else {
                return null;
            }
        }

        pdf = PdfFactory;

        return PdfFactory;

    });

})();
