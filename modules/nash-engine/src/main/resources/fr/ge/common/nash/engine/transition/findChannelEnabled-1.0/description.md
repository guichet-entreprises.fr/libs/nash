# Description of find channel enabled for a partner:
<br />

The goal of this transition is to find channel enabled for a partner.

<br />
<br />

# Transition inputs :
<br />

* funcId      : "2018-06-GHK-FU6001"
* attachment  : "2-review/generated/document.pdf"

<br />
<br />


# Transition output : 
<br />

List channel enabled for a partner.


