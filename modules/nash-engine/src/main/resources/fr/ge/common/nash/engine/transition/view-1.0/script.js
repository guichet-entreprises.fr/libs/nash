//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'view',
    label : 'Record view',
    preprocess : 'preprocess.xml',
    postprocess : 'postprocess.xml',
    data : 'data-generated.xml',
    icon : 'cogs',
    user : 'ge',
    status : 'todo'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/preprocess.js');
step.copy('files/postprocess.xml');
step.copy('files/postprocess.js');

// ------------------------------------------------------------------------
log.info('Step created');
