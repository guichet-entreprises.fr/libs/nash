# Description

La transition permet de trouver l'adresse postale d'une autorité.

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| cerfa | Chemin du fichier du CERFA | Oui |
| others | Prévisualisation des fichiers | Non |
| funcId | Code de l'autorité compétente | True |

# Résultat(s)

output.xml avec les informations postale