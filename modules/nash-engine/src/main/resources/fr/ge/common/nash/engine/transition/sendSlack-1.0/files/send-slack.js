// input slackId and message to send 
var grp = _INPUT_.slack;

_log.debug('group is {}', grp);

var messageContent = {}

messageContent["text"] = grp.message;
messageContent["channel"] = grp.slackId;

var jsonContent = JSON.stringify(messageContent);
_log.debug('*** jsonContent is    : [{}]', jsonContent);

_log.debug('Send receipt transition via [{}]', _CONFIG_.get('slack.baseUrl'));

var slackBaseUrl = _CONFIG_.get('slack.baseUrl');


_log.debug('*** Slack service URL   : [{}]', slackBaseUrl);





// -------- send slack message -----------------------------//
var response = nash.service.request(slackBaseUrl) //
			   .connectionTimeout(10000) //
			   .receiveTimeout(10000) //
			   .accept('json') //
			   .post(jsonContent);




//output to confirm

return spec.create({
	id: 'ConfirmSendSlack',
	label: "Confirmation  de l'envoi",
	groups: [spec.createGroup({
			id: 'slack',
			label: "le status de l'envoie",
			data: [spec.createData({
					id: 'slackId',
					label: "slack ",
					type: 'String',
					mandatory: true,
					value: '' + response.status
				})
			]
		})]
});
