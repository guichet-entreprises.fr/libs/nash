log.info("==> prepareZip preprocess");

var zip = nash.instance.zip();
//var cerfa = _input.parameters.attachment.cerfa;
//zip.add(nash.util.resourceFromPath(cerfa).label, cerfa);
//
//var regent = !_input.parameters.attachment.regent ? undefined : _input.parameters.attachment.regent;
//if (undefined !== regent) {
//	zip.add(nash.util.resourceFromPath(regent).label, regent);
//}
//
//var xmltc = !_input.parameters.attachment.xmltc ? undefined : _input.parameters.attachment.xmltc;
//if (undefined !== xmltc) {
//	zip.add(nash.util.resourceFromPath(xmltc).label, xmltc);
//}

['cerfa', 'regent', 'xmltc' ].forEach(function(key){
	if (null != _input.parameters.attachment[key]) {
		var entry = _input.parameters.attachment[key];
		zip.add(nash.util.resourceFromPath(entry).label, entry);
		log.info("Adding entry {} to zip file", key);
	}
});

for (var file in _input.parameters.attachment.others) {
    path = _input.parameters.attachment.others[file];
    zip.add(nash.util.resourceFromPath(path).label, path);
}

var extract = JSON.parse(nash.xml.extract(_input.parameters.attachment.regent, '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02'));
var args = {
	funcId :  _input.result.funcId,
	numeroLiasse : extract.C02
};

nash.record.meta([{'name':'liasse', 'value': extract.C02}]);

var response = nash.service.request('${directory.baseUrl}/v1/algo/{code}/execute', 'definir nom liasse') //
	.connectionTimeout(10000) //
	.receiveTimeout(10000)
	.accept('text') //
	.post(args);

var zipName = '/' + response.asString();

var zipAttachment = zip.save(zipName).getAbsolutePath();

//Insert to display.xml
var data = nash.instance.load("display.xml");

data.bind("request",{
    content:{
    	zipAttachment:zipAttachment
    }
})
