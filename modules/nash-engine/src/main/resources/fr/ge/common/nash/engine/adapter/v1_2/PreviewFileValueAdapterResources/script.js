/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define(['jquery','bootstrap-fileinput', 'lib/fr'], function($) {
	
	function createPreviewFile(field,urls,confs){
		field.find(".preview-file").fileinput({
            uploadUrl: '#',
            theme:'fa',
            uploadAsync: false,
            language: 'fr',
            showRemove: false,
            showClose: false,
            fileActionSettings: {
                showRemove: false,
                showUpload: false,
                showDownload: true,
                showZoom: true,
                showDrag: false,
            },
            initialPreviewShowDelete: false,
            initialPreviewAsData: true,
            initialPreview:urls,
            initialPreviewConfig: confs
        });
	};
	
	String.prototype.endsWith = function(suffix) {
	    return this.indexOf(suffix, this.length - suffix.length) !== -1;
	};
    var obj = {
            initialize : function(field, opts) {
            	urls = []; 
            	var htmlUrl = undefined;
            	var xmlUrl = undefined;
            	var dataType = undefined;
            	var tab = ["jpg","png","gif"];
            	var arrText= new Array();
            	$('input[name="url"]', field).each(function(){
            	    console.log("value is" +$(this).val());
            	    if(!$(this).val().startsWith("http") && !$(this).val().startsWith("/")){
            	    	console.log("path is "+field.find('input[name="path"]').val());
            	    	arrText.push(field.find('input[name="path"]').val()+"/"+$(this).val());
            	    }else{
            		    arrText.push($(this).val());
            	    }
            	})
            	arrText.forEach(function(url){
            		   if(url.startsWith("http") && !url.endsWith("html")){
            		     urls.push(url);
            		   }else if(url.endsWith("html")){
                           htmlUrl = window.location.origin +field.find("a").attr("href")+url;
            		   }else if(url.endsWith("xml")){
                           xmlUrl = window.location.origin +field.find("a").attr("href")+url;
            		   }else{
            			   urls.push(window.location.origin +field.find("a").attr("href")+url);
            			   
            		   }
                 });
            	
            	var confs = [];
            	urls.forEach(function(url){
            		if(tab.indexOf(url.substring(url.lastIndexOf(".")).replace(".","")) == -1 && !url.endsWith("mp4")){
            			var conf = {
            			 "type": url.substring(url.lastIndexOf(".")).replace(".",""),
            			 "with":"1200",
            			 "key": 1,
            			 "downloadUrl":(opts.downloadUrl) ? url : false
            			}
            		 confs.push(conf);  
            		}else if(url.endsWith("mp4")){
            			var conf ={   
            		            type: "video", 
            		            filetype: "video/mp4",
            		            key: 3,
            		            downloadUrl:(opts.downloadUrl) ? url : false
            		            }
            			confs.push(conf);
            			
            		}else{
            			var conf = {
                   			 "type": "image",
                   			 "with":"1200",
                   			 "key": 1,
                   			 "downloadUrl":(opts.downloadUrl) ? url : false
                   			}
                   		confs.push(conf);  
            		}
              		
            	});
            	console.log("url is "+urls);
            	console.log("confs is "+confs);
            	
            	if(htmlUrl || xmlUrl){
            	       $.ajax({
            	           url : htmlUrl ? htmlUrl : xmlUrl,
            	           type : 'GET',
            	           dataType : htmlUrl ? "html" : "text",
            	           async:false,
            	           success : function(code_html, statut){
            	        	   urls.push(code_html);
            	        	   console.log("urls is " + urls);
            	        	   var htmlConf = {
                             			 "type": htmlUrl ? "html" : "text",
                             			 "with":"1200",
                             			 "key": 1,
                             			 "downloadUrl":(opts.downloadUrl) ? (htmlUrl ? htmlUrl : xmlUrl) : false
                             			};
            	        	   confs.push(htmlConf);
            	        	   createPreviewFile(field,urls,confs);
                           }
                        });
            	} else {
            	    createPreviewFile(field,urls,confs) ;
            	    urls= [];
            	    confs= [];
            	}
            	
            },
            validate : function(data, opts) {
                return {
                	validated : true,
                	value : null
                };
            }
        };

        return obj;
    });
