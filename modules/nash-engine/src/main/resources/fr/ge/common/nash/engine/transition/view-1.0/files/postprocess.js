// input 
var destFuncId = $preprocess.destFuncId;
_log.info("destFuncId is  {}", destFuncId);

var attachment = $preprocess.attachment;
_log.info("attachment is {}", attachment);

// Get record information  to create the summary
var recordUid = $preprocess.recordUid;
_log.info("recordUid is {}", recordUid);

// TODO denomination : no data to be displayed
var denomination = null;
_log.info("denomination is {}", denomination);

var typeFormality = $preprocess.typeFormality;
_log.info("typeFormality is {}", typeFormality);

// TODO transmission date = creation date ??
var transmissionDate  = null;
_log.info("transmissionDate is {}", transmissionDate);

//Change user for the summary step
nash.record.currentStep().setUser("bo");

// create data.xml for record summary 
return spec.create({
	id: 'view',
	label: "Résumé du dossier",
	groups: [spec.createGroup({
			id: 'view',
			label: "Résumé du dossier",
			data: [spec.createData({
					id: 'recordUid',
					label: "Numéro de dossier",
					description: 'Numéro de dossier',
					type: 'String',
					mandatory: true,
					value: recordUid
				}),
				spec.createData({
					id: 'denomination',
					label: "Dénomination",
					description: 'Dénomination',
					type: 'String',
					mandatory: false,
					value: denomination
				}),
				spec.createData({
					id: 'typeFormality',
					label: "Type de formalité",
					description: 'Type de formalité',
					type: 'String',
					mandatory: true,
					value: typeFormality
				}),
				spec.createData({
					id: 'transmissionDate',
					label: "Date de transmission",
					description: 'Date de transmission',
					type: 'String',
					mandatory: true,
					value: transmissionDate
				}),
				spec.createData({
					id: 'destFuncId',
					label: "Code partenaire",
					description: 'Code partenaire',
					type: 'String',
					mandatory: true,
					value: destFuncId
				}),
				spec.createData({
					id: 'attachment',
					label: "Pièce jointe associée au dossier",
					type: 'String',
					mandatory : true,
					value: attachment
				}),
			]
		})]
});