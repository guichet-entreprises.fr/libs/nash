var funcId = _input.result.funcId;

_log.info('===> find post mail address to "{}"', funcId);

// call directory with funcId to find all information of Authorithy
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', funcId) //
.accept('json') //
.get();

// result
var receiverInfo = response.asObject();

var addr = {};
var nfo = receiverInfo.details || undefined;

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}

var who = map(receiverInfo, {
    'funcId' : 'entityId', //
    'funcLabel' : 'label' //
});

if (nfo == undefined || nfo.transferChannels == undefined || nfo.transferChannels.address == undefined || nfo.transferChannels.address.addressDetail == undefined) {
    addr = {
        'recipientName' : null,
        'recipientNameCompl' : null,
        'recipientAddressName' : null,
        'recipientAddressNameCompl' : null,
        'recipientPostalCode' : null,
        'recipientCity' : null
    };
} else {
    addr = map( //
    nfo.transferChannels.address.addressDetail, {
        'recipientName' : 'recipientName', //
        'recipientNameCompl' : 'recipientNameCompl', //
        'recipientAddressName' : 'addressName', //
        'recipientAddressNameCompl' : 'addressNameCompl', //
        'recipientPostalCode' : 'postalCode', //
        'recipientCity' : 'cityName' //
    });
    addr['referenceId'] = nash.record.description().recordUid;
}

var others = [];
var fileId = null;
var filePath = null;
for (var file in _input.parameters.attachment.others) {
	fileId = _input.parameters.attachment.others[file].id;
	filePath = _input.parameters.attachment.others[file].label;
    	
	others.push({
		id:fileId,
		label:filePath
	});
}
var attachment = {
	'cerfa' : _input.parameters.attachment.cerfa,
	'others' : others
};

_log.info("Preparing display file with input : {}, output : {} and attachment : {}", who, addr, attachment);
var data = nash.instance.load("display.xml");
data.bind('request', {
    'input' : who,
    'output' : addr,
    'attachment' : attachment
});
