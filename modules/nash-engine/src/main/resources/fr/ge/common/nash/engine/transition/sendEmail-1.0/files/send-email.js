// prepare info to send

var grp = _INPUT_.email;


_log.info("group is  {}",grp);

var email = grp.emailAddress;

_log.info("email is  {}",email);

var emailContent = grp.emailContent
var emailObject = grp.emailObject
var exchangeSender = _CONFIG_.get('exchange.email.sender'); ;

var destFuncId = grp.destFuncId;
_log.info("destFuncId is {}", destFuncId);

// call exchange service to send mail with pj
var response = nash.service.request('${exchange.baseUrl}/email/send') //
	.connectionTimeout(20000) //
	.receiveTimeout(50000) //
	.dataType("form") //
	.param("sender", [ exchangeSender ]) //
	.param("recipient", [ email ]) //
	.param("object", [ emailObject ]) //
	.param("content", [ emailContent ]) //
	.param("attachmentPDFName", [ "document.pdf" ]) //
	.post({ "file" : nash.util.resourceFromPath(grp.emailPj) });

// output to confirm

var path ="review.generated.formulaire";

return spec.create({
	id: 'ConfirmSendMail',
	label: "Preparation de l'envoi",
	groups: [spec.createGroup({
			id: 'partner',
			label: "le status de l'envoie",
			data: [spec.createData({
					id: 'code',
					label: "Code de l'autorité compétente",
					type: 'String',
					mandatory: true,
					value: destFuncId
				}), spec.createData({
					id: 'attachment',
					label: "Pièce jointe du dossier",
					type: 'String',
					mandatory: false,
					value: path
				})
			]
		})]
});
