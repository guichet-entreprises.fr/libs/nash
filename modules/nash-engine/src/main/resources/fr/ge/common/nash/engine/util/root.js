(function () {

    var _libs = [];

    root = {

        /**
         * Define a libraries which need nash instance to work.
         * The library will be use as global variable. Init function takes "nash" instance as value.
         * 
         * @param key library name
         * @param fn init function
         */
        define: function (key, fn) {
            log.debug("Define library '{}'", key);
            _libs.push({
                key: key,
                source: fn
            });
        },

        /**
         * Initialize libraries for specified NASH context.
         * 
         * @param nash NASH context
         */
        init: function (ctx, nash) {
            
            _libs.forEach(function (lib) {
                log.debug("Initialize library '{}'", lib.key);
                if (lib.key) {
                    ctx[lib.key] = lib.source(nash);
                } else {
                    lib.source(nash);
                }
            });
        }

    }

    return root;

})();
