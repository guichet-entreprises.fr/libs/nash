// input functionnal id of authority
var grp = _INPUT_.functionalId;


var destFuncId = !grp.funcId ? '' : grp.funcId;

_log.info("destFuncId is  {}",destFuncId);

// call directory with funcId to find all information of Authorithy

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
	           .connectionTimeout(300000) //
	           .receiveTimeout(300000) //
	           .accept('json') //
	           .get();

//result			   
			   
var receiverInfo = response.asObject();

// prepare all information of receiver to create data.xml

var funcId = !receiverInfo.funcId ? null : receiverInfo.funcId;
var funcLabel = !receiverInfo.label ? null :receiverInfo.label;
var contactInfo = !receiverInfo.contactInfo ? null : JSON.parse(receiverInfo.contactInfo);
var email = null;
if (contactInfo != null && "email" in contactInfo) {
	email = contactInfo.email;
}

var attachment = !grp.attachment ? null : grp.attachment;

// create data.xml with information of receiver

return spec.create({
		id: 'contactInfo',
		label: "les informations d'une autorité compétente",
		groups: [spec.createGroup({
				id: 'email',
				label: "Contenu d'une autorité compétente",
				data: [spec.createData({
						id: 'destFuncId',
						label: "Le code de l'autorité compétente",
						description: 'Code interne',
						type: 'String',
						mandatory: true,
						value: funcId
					}), spec.createData({
						id: 'labelFunc',
						label: "Le libellé fonctionnelle de l'autorité compétente",
						type: 'String',
						mandatory : true,
						value: funcLabel
					}), spec.createData({
						id: 'email',
						label: "L'adresse email de l'autorité compétente",
						type: 'Email',
						mandatory:true,
						value: email
					}),	spec.createData({
					    id: 'emailPj',
					    label: "Le chemin de la pièce jointe",
					    type: 'String',
					    value: attachment
				}),
				]
			})]
	});

