# Description

La transition permet de préparer les fichiers à envoyer  

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| cerfa | Chemin du fichier cerfa | Oui |
| others | autres documents | Non |

# Résultat(s)

Création d'un display.xml avec les fichiers à envoyer pour la fusion