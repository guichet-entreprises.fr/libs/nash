log.info("==> Eddie transition");

var token = _input.result.ftp.token;
var funcId = _input.result.funcId;
var funcLabel = _input.result.funcLabel;
log.info('funcId => ' + funcId);

var comment = _input.result.ftp.comment ? _input.result.ftp.comment : "";
var pathFolder = _input.result.ftp.pathFolder ? _input.result.ftp.pathFolder : "";

var zipAttachment = _input.parameters.content.zipAttachment;
var zipFiniAttachment = _input.parameters.content.zipFiniAttachment;

//-->Get Eddie URL
var eddieBaseUrl = _CONFIG_ ? _CONFIG_.get('eddie.file.send.url') : '';
log.info('eddieBaseUrl => ' + eddieBaseUrl);

var streamName = pathFolder+zipAttachment;
log.info('Nom du fichier à envoyer à Eddie => ' + streamName);

var data = nash.instance.load('display.xml');
data.bind('request', {
    'input': {
       'funcLabel' : funcLabel,
       'token' : token,
       'comment' : comment,
       'pathFolder' : pathFolder
    },
    'content': {
        'zipAttachment': zipAttachment,
        'zipFiniAttachment': zipFiniAttachment
    },
    'output' : {
        'url' : eddieBaseUrl
    }
});

// Call Eddie service
try {
    var content = nash.util.resourceFromPath(zipAttachment).getContent();
    var response = nash.service.request('${eddie.file.send.url}/exchange') //
    .connectionTimeout(2000) //
    .receiveTimeout(5000) //
    .dataType("application/octet-stream") //
    .param("uid", token) //
    .param("comment", comment) //
    .param("streamName", streamName)
    .post(content);
    // Show success message if response is 200
    if (response.status == 200) {
        // Building the zip.fini file with MD5 content
        var zipFiniContent = nash.util.createChecksum(content, 'MD5');
        
        nash.record.saveFile(zipFiniAttachment, zipFiniContent);
        
        //Call Eddie to send zip.fini file
        streamName = pathFolder + zipFiniAttachment;
        response = nash.service.request('${eddie.file.send.url}/exchange') //
            .connectionTimeout(2000) //
            .receiveTimeout(5000) //
            .dataType("application/octet-stream") //
            .param("uid", token) //
            .param("comment", comment) //
            .param("streamName", streamName)
            .post(zipFiniContent);
        // Response
        if (response.status == 200) {
            data.bind('request.output', {
                'status' : response.status + '',
                'confirm' : 'OK',
                'response' : 'Votre dossier a été envoyé avec succès.'
            });
        }
        // Show message with the potential error and block the step
        else {
            // Response
            data.bind('result.output', {
                'status' : response.status + '',
                'response' : 'Une erreur technique est survenue lors de l\'envoi du dossier. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
            });
        }
    }
    // Show message with the potential error and block the step
    else {
        // Response
        data.bind('result.output', {
            'status' : response.status + '',
            'response' : 'Une erreur technique est survenue lors de l\'envoi du dossier. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
        });
    }
// Show error message and block the step
}catch(error) {
    // Response
    log.info('error while sending to Eddie => ' + error);
    data.bind('request.output', {
        'response' : 'Une erreur technique est survenue lors de l\'envoi à Eddie. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
    });
}
