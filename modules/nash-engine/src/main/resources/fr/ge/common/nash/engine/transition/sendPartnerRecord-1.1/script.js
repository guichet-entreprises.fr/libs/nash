//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'sendPartnerRecord',
    label : 'Créer un dossier dans le Backoffice',
    preprocess : 'preprocess.xml',
    data : 'display.xml',
    postprocess : 'postprocess.xml',
    icon : 'pencil',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/send.js');
step.copy('files/display.xml');
step.copy('files/backoffice.zip');
step.copy('files/postprocess.xml');
step.copy('files/next.js');
step.copy('files/output.xml');

// ------------------------------------------------------------------------
log.info('Step created');
