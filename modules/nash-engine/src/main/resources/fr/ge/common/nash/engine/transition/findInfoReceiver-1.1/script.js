//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'findInfoReceiver',
    label : 'find information of receiver',
    preprocess : 'preprocess.xml',
    data : 'data-generated.xml',
    postprocess : 'postprocess.xml',
    icon : 'search',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/preprocess.js');
step.copy('files/postprocess.xml');
step.copy('files/postprocess-prepare.js');
step.copy('files/data-generated.xml');
step.copy('files/data-to-send.xml');

// ------------------------------------------------------------------------
log.info('Step created');
