define('jquery', [ 'util' ], function(util) {

    var class2type = {};
    var toString = class2type.toString;
    var hasOwn = class2type.hasOwnProperty;

    'Boolean Number String Function Array Date RegExp Object Error Symbol'.split('').forEach(function(name) {
        class2type['[object ' + name + ']'] = name.toLowerCase();
    });

    var jQuery = util.extend(function() {
    }, {
        type : function(obj) {
            if (obj == null) {
                return obj + '';
            }

            return typeof obj === 'object' || typeof obj === 'function' ? class2type[toString.call(obj)] || 'object' : typeof obj;
        },

        isArray : function (obj) {
            return Array.isArray(obj) || helper.isList(obj);
        },

        isPlainObject : function(obj) {
            var key;

            if (jQuery.type(obj) !== 'object' || obj.nodeType || jQuery.isWindow(obj)) {
                return false;
            }

            if (obj.constructor && !hasOwn.call(obj, 'constructor') && !hasOwn.call(obj.constructor.prototype || {}, 'isPrototypeOf')) {
                return false;
            }

            if (toString.call(obj.class) === '[object java.lang.Class]') {
                return true;
            } 

            for (key in obj) {
            }

            return key === undefined || hasOwn.call(obj, key);
        },

        isWindow : function(obj) {
            return obj != null && obj === obj.window;
        }
    });

    return jQuery;

});
