/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define(['jquery','bootstrap-fileinput', 'lib/fr'], function($) {

    var obj = {
        initialize : function(field, opts) {
        	var confs = [], urls = [];
    		var imageExtension = ["jpg","png","gif"];
    		var xmlExtension  = ["xml" , "html"];
        	field.find("a").each(function() {
                var element = $(this);
        		var url = window.location.origin + element.attr("href");
            	
            	var extension = url ? url.substring(url.lastIndexOf(".")).replace(".","") : undefined;
            	if (imageExtension.indexOf(extension) != -1) {
            		confs.push({
                		type: "image", 
                		width: "1200px",
                		key: 1,
                		downloadUrl:url
                	});
            		urls.push(url);
            	}
            	else if (xmlExtension.indexOf(extension) != -1) {
          	       $.ajax({
         	           url : url,
         	           type : 'GET',
         	           dataType : extension == "xml" ? "text" : "html", 
         	           async:false,
         	           success : function(code_html, statut){
         	        	   urls.push(code_html);
         	        	   var conf = {
                          			 "type": extension == "xml" ? "text" : "html",
                          			 "with":"1200",
                          			 "key": 2,
                          			 "downloadUrl": url
                          			};
         	        	   confs.push(conf);
                        }
                     });
            	} 
            	else{
            		confs.push({
                		type: extension, 
                		width: "1200px",
                		key: 3,
                		downloadUrl:url
                	});
            		urls.push(url);
            	}
            	
        	});
        	
        	$('#fileReadOnly', field).fileinput({
                uploadUrl: '#',
                theme:'fa',
                uploadAsync: false,
                language: 'fr',
                showRemove: false,
                showClose: false,
                fileActionSettings: {
                    showRemove: false,
                    showUpload: false,
                    showDownload: true,
                    showZoom: true,
                    showDrag: false,
                },
                initialPreviewShowDelete: false,
                initialPreviewDownloadUrl: true, 
                initialPreviewAsData: true,
                initialPreview: urls,
                initialPreviewConfig: confs
            });
        },
        validate : function(data, opts) {
            return {
            	validated : true,
            	value : null
            };
        }
    };

    return obj;
});
