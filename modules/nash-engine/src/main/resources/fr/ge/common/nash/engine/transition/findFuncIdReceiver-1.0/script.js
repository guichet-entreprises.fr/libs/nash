//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'findFuncIdReceiver',
    label : 'find functional id of receiver',
    preprocess : 'preprocess.xml',
    data : 'data-generated.xml',
    postprocess : 'postprocess.xml',
    icon : 'cogs',
    user : 'ge'
});

// ------------------------------------------------------------------------
// Copy file
// ------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/preprocess.js');
step.copy('files/postprocess.xml');

// ------------------------------------------------------------------------
log.info('Step created');
