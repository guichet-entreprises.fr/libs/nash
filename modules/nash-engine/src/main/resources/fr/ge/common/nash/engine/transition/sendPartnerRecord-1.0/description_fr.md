# Description

Le but de cette transition est de générer un dossier et l'envoyer dans le Bckoffice partenaires.

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| code | Code de l'autorité compétente | Oui |
| attachment | pièce jointe| Non |

# Résultat(s)

créer un nouveau dossier partenaire et le déposer dans le backoffice