_log.info("===> postprocess sendPartnerRecord");

var liasse = !_input.request.output.liasse ? null : _input.request.output.liasse;
var message = "Un dossier avec la référence " + _input.request.output.backofficeId + " a été déposé dans le Backoffice pour l'autorité compétente " + _input.request.input.funcLabel;
if (null != liasse) {
	message = "Un dossier avec la référence " + liasse + " a été déposé dans le Backoffice pour l'autorité compétente " + _input.request.input.funcLabel;
}

nash.instance //
    .load('output.xml') //
    .bind('result', {
        'tracker' : {
        	'referenceId' : 'ENVOI-DOSSIER-BO',
        	'message' : message
        },
        'backoffice' : {
        	'backofficeId' : _input.request.output.backofficeId
        }
    });
