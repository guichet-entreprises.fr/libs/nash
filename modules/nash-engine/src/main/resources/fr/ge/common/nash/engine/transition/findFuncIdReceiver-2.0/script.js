//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'findFuncIdReceiver',
    label : 'find functional id of receiver',
    preprocess : 'preprocess.xml',
    data : 'display.xml',
    postprocess : 'postprocess.xml',
    icon : 'cogs',
    user : 'ge'
});




step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/find.js');
step.copy('files/postprocess.xml');
step.copy('files/next.js');
step.copy('files/display.xml');
step.copy('files/output.xml');

// ------------------------------------------------------------------------
log.info('Step created');
