log.info('===> mail preprocess');

var addr = _input.result.address;
var docPath = _input.parameters.content.attachment;
var entry = [];
entry.push({
    id:"attachment",
    label:_input.parameters.content.attachment
});

//-->Get Exchange URL
var exchangeBaseUrl = _CONFIG_ ? _CONFIG_.get('exchange.baseUrl') : '';

var data = nash.instance.load('display.xml');
data.bind('request', {
    'input': {
       'funcLabel' : _input.result.funcLabel,
       'referenceId' : addr.referenceId,
       'recipientName' : addr.recipientName,
       'recipientNameCompl' : addr.recipientNameCompl,
       'recipientAddressName' : addr.recipientAddressName,
       'recipientAddressNameCompl' : addr.recipientAddressNameCompl,
       'recipientPostalCode' : addr.recipientPostalCode,
       'recipientCity' : addr.recipientCity
    },
    'content': {
        'entry': entry
    },
    'output' : {
        'url' : exchangeBaseUrl
    }
});

// Call exchange service to send mail with pj
try {
    var response = nash.service.request('${exchange.baseUrl}/private/postmail') //
        .dataType('form') //
        .param('referenceId', addr.referenceId) //
        .param('recipientName', addr.recipientName) //
        .param('recipientNameCompl', addr.recipientNameCompl) //
        .param('recipientAddressName', addr.recipientAddressName) //
        .param('recipientAddressNameCompl', addr.recipientAddressNameCompl) //
        .param('recipientPostalCode', addr.recipientPostalCode) //
        .param('recipientCity', addr.recipientCity) //
        .put({
            'attachmentFile' : nash.util.resourceFromPath(docPath)
        });

        // Show success message if response is 200
    if (response.status == 200) {
        // Response
        data.bind("request", {
           "output" : {
                'status' : response.status + '',
                'confirm' : 'OK',
                'exchangeId' : response.asString(),
                'response' : 'Votre courrier papier a été envoyé avec succès.'
           }
        });
    }
    // Show message with the potential error and block the step
    else {
        // Response
        data.bind("request", {
           "output" : {
              'status' : response.status + '',
                'response' : 'Une erreur technique est survenue lors de l\'envoi de votre courrier papier. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
           }
        });
    }
    // Show error message and block the step
} catch (error) {
    // Response
    log.info('error while sending a mail => ' + error);
    data.bind("request", {
       "output" : {
          'response' : 'Une erreur technique est survenue lors de l\'envoi de votre courrier papier. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
       }
    });
}
