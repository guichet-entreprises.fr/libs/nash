/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
define([ 'jquery', 'lib/jqueryMaskPlugin', "_fm_data" ], function($, jqueryMaskPlugin, _data) {

    var obj = {
        initialize : function(field, opts) {
            field.find('input').mask('0 00 00 0A 000 000 00', {
                onKeyPress: function (value, event) {
                    event.currentTarget.value = value.toUpperCase();
                }
            });
        },
        validate : function(data, opts) {
            var value = data.asString().replace(/ /g, '');;
            var validation = {
                validated : checkGeneral(value, opts.birthDateRef)
            };
            if (validation.validated) {
                validation.value = value;
            } else {
                validation.message = 'This value does not comply with a NIR.';
            }
            return validation;
        }
    };

    function checkGeneral(number, birthDateRef) {
        if (number == "" || number == "000000000000000") {
            return true;
        }
        var pattern = "^" +                                     // string beginning
        "([12378])" +                                           // 1 and 7 for men, 2 and 8 for women, 3 for transgenders
        "([0-9]{2})" +                                          // birthday year
        "(0[0-9]|1[012]|[23][0-9]|4[12]|[5-9][0-9])" +          // birthday month (if >= 20, the information on the birth certificate are incomplete. There are several cases : 20 <= NIR <= 30 || 31 <= NIR <= 42 || 50 <= NIR <= 99)
        "(2[AB]|0[1-9]|[1-9][0-9])" +                           // 2A or 2B for Corsica. 96 was used for Algeria but is not given anymore ; the conversion to 99 was possible but not systematic)
        "(00[1-9]|0[1-9][0-9]|[1-8][0-9]{2}|9[0-8][0-9]|990)" + // town order number (particular case outside of metropolis, checked outside the regex) or end of department number (971 to 978 and 98 for DOM-TOM) or country
        "([0-9]{3})" +                                          // birth certificate order number, within the month and the town or country
        "(0[1-9]|[1-8][0-9]|9[0-7])?" +                         // control number (optional)
        "$";                                                    // string end

        var tab = new RegExp(pattern).exec(number);
        if (tab == null) {
            return false;
        }

        var year = tab[2];
        var month = tab[3];
        var department = tab[4];
        var townNumber = tab[5];
        var certificateNumber = tab[6];
        var key = tab[7];

        if (birthDateRef && birthDateRef.length > 0) {
            var birthDatePath = birthDateRef.split('.'), k, birthDate = _data.page;
            while (k = birthDatePath.shift()) {
                birthDate = birthDate[k];
            }
            if (birthDate) {
                if (typeof birthDate === 'number') {
                    birthDate = new Date(birthDate);
                }
                if (typeof birthDate === 'string') {
                    var birthDateParts = birthDate.split("/");
                    birthDate = new Date(birthDateParts[2], (birthDateParts[1] - 1), birthDateParts[0]);
                }
                if (birthDate.getYear() != parseInt(year) || birthDate.getMonth() + 1 != parseInt(month)) {
                    return false;
                }
            }
        }

        return checkKey(number, department, certificateNumber, townNumber, key);
    }

    function checkKey(number, department, certificateNumber, townNumber, key) {
        // Default calculation basis for the key (is modified for Corsica).
        var aChecker = parseInt(number.substr(0,13));

        // If you were born in France, the birth certificate number is different from 0.
        if (department != "99" && parseInt(certificateNumber) == 0) {
          return false;
        }

        // Check people who were born outside of metropolis or in Corsica.
        // The key calculation is different for Corsica.
        if (department == "2A") {
            aChecker = parseInt(number.substr(0, 13).replace('A', 0));
            aChecker -= 1000000;
        } else if (department == "2B") {
            aChecker = parseInt(number.substr(0, 13).replace('B', 0));
            aChecker -= 2000000;
        } else if (department == "97" || department == "98") {
            townNumber = townNumber.substr(1, 2);
            if (parseInt(townNumber) > 90) { // > 90 = unknown town
                return false;
            }
        }
        return (parseInt(key) == (97 - (aChecker % 97)));
    }

    return obj;
});
