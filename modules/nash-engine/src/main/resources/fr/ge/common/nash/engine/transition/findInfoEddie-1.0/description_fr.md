# Description

La transition permet de recuperer les informations FTP pour la connexion à l'application 'EDIE'

# valeur(s) attendue(e)

| Nom | Description |Obligatoire|
|---|---|---|
| funcId | Code de l'autorité compétente | Oui |

# Résultat(s)

Token et le chemin d'accès (pathFolder)