//------------------------------------------------------------------------
// Create transition
//------------------------------------------------------------------------
var step = _transition.createStep({
    id : 'mergePdf',
    label : 'merge pdf files',
    label : "Fusionner les fichiers au format PDF",
    preprocess : 'preprocess.xml',
    postprocess : 'postprocess.xml',
    data : 'display.xml',
    icon : 'search',
    user : 'ge'
});

//------------------------------------------------------------------------
// Copy file
//------------------------------------------------------------------------
step.copy(_INPUT_NAME_, 'input.xml');
step.copy('files/preprocess.xml');
step.copy('files/postprocess.xml');
step.copy('files/merge.js');
step.copy('files/postprocess-prepare.js');
step.copy('files/display.xml');
step.copy('files/output.xml');

//------------------------------------------------------------------------
log.info("Step created");
