_log.info("===> preprocess findNotificationList");

// input functionnal id of authority
var grp = _input.result;
var destFuncId = !grp.funcId ? '' : grp.funcId;

//-->Call Directory service to retrieve the mailing list
try {
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
		.connectionTimeout(10000) //
		.receiveTimeout(10000) //
		.accept('json') //
		.get();
	
	var receiverInfo = response.asObject();
	_log.info("receiverInfo : " + receiverInfo);
	var funcId = !receiverInfo.entityId ? null : receiverInfo.entityId;
	var funcLabel = !receiverInfo.label ? null : receiverInfo.label;
	var details = !receiverInfo.details ? null : receiverInfo.details;
	
	var notificationList = !details.notifications ? [] : details.notifications;
	
	_log.info("Notification List : " + notificationList);
	
	var data = nash.instance.load("display.xml");
	data.bind("result",{
		funcId:{
			funcId:funcId,
			labelId:funcLabel
		},
		notification:{
			email:notificationList
		}
	})
} catch (error) {
    // Response
    log.info('error while retrieving Notification mailing list => ' + error);
}

