log.info('===> email preprocess');

var email = _input.result.email;

var data = nash.instance.load('display.xml');
data.bind("request", {
    'input': {
        'funcLabel' : _input.result.funcLabel,
        'email' : email.email,
        'object' : email.object,
        'content' : email.content,
    },
    'content': {
        'attachment': _input.parameters.content.attachment
    },
    'output' : {
        'url' : _CONFIG_ ? _CONFIG_.get('exchange.baseUrl') : null
    }
});

//-->Call Exchange service to send an email with an attachment
try {
    var recordUid = nash.record.description().recordUid;
    
    var response = nash.service.request('${exchange.baseUrl}/email/send') //
    .connectionTimeout(2000) //
    .receiveTimeout(5000) //
    .dataType("form") //
    .param("sender", _CONFIG_.get('exchange.email.sender')) //
    .param("recipient", email.email) //
    .param("object", email.object) //
    .param("content", email.content) //
    .param("attachmentPDFName", "DOSSIER-" + recordUid + ".pdf") //
    .post({
        'file' : nash.util.resourceFromPath(_input.parameters.content.attachment)
    });

    if(response.status == 200){
        // Response
        data.bind("request", {
            'output' : {
                'status' : response.status + '',
                'confirm' : 'OK',
                'response' : 'Votre courrier électronique a été envoyé avec succès.'
            }
        });
    } else {
        // Response
        data.bind("request", {
            'output' : {
                'status' : response.status + '',
                'response' : 'Une erreur technique est survenue lors de l\'envoi de votre courrier électronique. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
            }
        });
    }
} catch (error) {
    // Response
    log.info('error while sending an email => ' + error);
    data.bind("request", {
            "output" : {
                'response' : 'Une erreur technique est survenue lors de l\'envoi de votre courrier électronique. Veuillez contacter le support technique à l\'adresse suivante : support.guichet-entreprises@helpline.fr.'
            }
    });
}

