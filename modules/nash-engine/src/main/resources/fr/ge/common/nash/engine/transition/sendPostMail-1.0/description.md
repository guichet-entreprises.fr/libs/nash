# Description of send mail transition:
<br />

The goal of this transition is to send a mail with a file (if necessary).

<br />
<br />

# Input of send mail transition:
<br />

* To send email to specific  recipient we need:
    * referenceId : String
    * recipientName: String
    * recipientNameCompl: String
    * recipientAddressName : String
    * recipientAddressNameCompl: String
    * recipientPostalCode: String
    * recipientCity : String
    * attachment: "/absoluth path of pj/"

<br />
<br />


# Output of send mail transition: 
<br />

Create data-generated.xml with response code of exchange api


