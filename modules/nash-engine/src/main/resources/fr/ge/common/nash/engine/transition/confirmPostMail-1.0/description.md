# Description of confirm post mail transition:
<br />

The goal of this transition is to manually confirm a mail before sending it.

<br />
<br />

# Input of confirm post mail transition:
<br />

* To confirm post mail to specific  recipient we need:
    * referenceId : String
    * recipientName: String
    * recipientNameCompl: String
    * recipientAddressName : String
    * recipientAddressNameCompl: String
    * recipientPostalCode: String
    * recipientCity : String
    * attachment: "/absoluth path of pj/"

<br />
<br />


# Output of confirm post mail transition: 
<br />

Create display.xml with post mail information and expected manual input for user


