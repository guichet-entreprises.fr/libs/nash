_log.info("===> postprocess findPostMailAddress");


//call directory with funcId to find all information of Authorithy

var destFuncId = _input.request.input.funcId;
var destFuncLabel = _input.request.input.funcLabel;

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
    .connectionTimeout(10000) //
    .receiveTimeout(10000) //
    .accept('json') //
    .get();

//result
var receiverInfo = response.asObject();

var contactInfo = !receiverInfo.details ? null : receiverInfo.details;

var transferChannels = {
    "backoffice": {
        "state": "disabled"
    },
    "email": {
        "emails": [],
        "state": "disabled"
    },
    "address": {
        "addressDetail": {
            "recipientName": "",
            "recipientNameCompl": "",
            "addressName": "",
            "addressNameCompl": "",
            "cityName": "",
            "postalCode": ""
        },
        "state": "disabled"
    },
    "ftp": {
        "token": "",
        "state": "disabled"
    }
}
if(contactInfo.transferChannels == null){
    contactInfo.transferChannels = transferChannels;
}

contactInfo.transferChannels["address"] = {
    "state" : contactInfo.transferChannels.address.state,
    "addressDetail" : {
        "recipientName" : _input.request.output.recipientName,
        "recipientNameCompl" : _input.request.output.recipientNameCompl,
        "addressName" : _input.request.output.recipientAddressName,
        "addressNameCompl" : _input.request.output.recipientAddressNameCompl,
        "postalCode" : _input.request.output.recipientPostalCode,
        "cityName" : _input.request.output.recipientCity,
    }
};

log.info("transferChannels is {}", contactInfo.transferChannels);

receiverInfo.details = contactInfo;

log.info("receiverInfo is {}",receiverInfo);

//update directory 
var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
               .connectionTimeout(10000) //
               .receiveTimeout(10000) //
               .dataType('application/json')//
               .accept('json') //
               .put(receiverInfo);

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}


var address = map(_input.request.output, {
    'referenceId' : 'referenceId',
    'recipientName' : 'recipientName',
    'recipientNameCompl' : 'recipientNameCompl',
    'recipientAddressName' : 'recipientAddressName',
    'recipientAddressNameCompl' : 'recipientAddressNameCompl',
    'recipientPostalCode' : 'recipientPostalCode',
    'recipientCity' : 'recipientCity',
});

_log.info("===> address information : {}", address);
nash.instance //
    .load('output.xml') //
    .bind('result', {
        'funcId' : destFuncId,
        'funcLabel': destFuncLabel,
        'address' : address
    });
