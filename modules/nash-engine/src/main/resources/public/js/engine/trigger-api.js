define('engine/trigger-api', [ 'jquery', 'engine/Value', '_fm_data' ], function ($, Value, data) {

    function _submit() {
        $('form[name="record"] button[type="submit"]').trigger('click');
    }
    
    function _refresh(ctx, path) {
        if (!path) {
            path = ctx;
            ctx = this;
        }

        var m = /^_(record|step|page|group)\.(.*)$/.exec(path);
        var root;
        if (m) {
            root = m[1];
            path = m[2];
        }

        if (root && ctx.target && ctx.model) {
            /*
             * Having a binding context
             */
            if ('group' == root) {
                path = ctx.target.path.relative.replace(/\.[^.]+$/, '.' + path);
            }
        }

        $('[data-field="' + path + '"]').trigger('fm.update');
    }

    return {
        'Value': Value,
        'navigation': {
            'submit': _submit,
            'refresh' : _refresh
        }
    };

});
