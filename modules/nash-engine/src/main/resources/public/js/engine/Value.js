/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * API for easily writing conditions on fields.
 * 
 * var obj = { id: 'myId', label: 'My Label', content: { id: 'subId', label: 'A new  label' }, list: ['a', 'b', 'c'] };
 * console.log(Value(21).of('list').of(null)._eval()); => undefined
 * console.log(Value(2).of('list').of(obj)._eval()); => b
 * console.log(Value('id').of('content').of(obj)._eval()); => subId
 * console.log(Value('content.id').of(obj)._eval()); => subID
 * console.log(Value('list').of(obj).contains('b')); => true
 * console.log(Value('list').of(obj).contains('d')); => false
 * console.log(Value('list').of(obj).contains(null)); => false
 * console.log(Value('label').of(obj).contains('My')); => true
 * console.log(Value('label').of(obj).contains('Your')); => false
 */
define('engine/Value', ['jquery'], function($) {

    function Value(chainPart) {
        return new ValueWrapper(chainPart);
    }

    function ValueWrapper(chainPart) {
        this.chain = [];
        this.of(chainPart);
    }

    ValueWrapper.prototype.of = function(chainPart) {
        if (chainPart && typeof chainPart == 'string') {
            var chainPartSplit = chainPart.split('.');
            for (var i = chainPartSplit.length - 1; i >= 0; --i) {
                this.chain.unshift(chainPartSplit[i]);
            }
        } else {
            this.chain.unshift(chainPart);
        }
        return this;
    }

    ValueWrapper.prototype._eval = function() {
        var chainValue = this.chain[0];
        for (var i = 1; i < this.chain.length; ++i) {
            var attribute = this.chain[i];
            if ($.isArray(chainValue)) {
                if (typeof attribute == 'number') {
                    chainValue = chainValue[attribute - 1];
                } else if (typeof attribute == 'string') {
                    var lst = [];
                    chainValue.forEach(function (item) {
                        lst.push(item[attribute]);
                    });
                    chainValue = lst;
                } else {
                    return undefined;
                }
            } else if ($.isPlainObject(chainValue) && typeof attribute == 'string') {
                chainValue = chainValue[attribute];
            } else {
                return undefined;
            }
        }

        return chainValue;
    }

    ValueWrapper.prototype.contains = function(o) {
        var evaluation = false;
        var chainValue = this._eval();
        if (chainValue !== undefined && ($.isArray(chainValue) || typeof chainValue == 'string')) {
            evaluation = chainValue.indexOf(o) > -1;
        }
        return evaluation;
    }

    ValueWrapper.prototype.any = function (o) {
        var lst = $.isArray(o) ? o : [ o ];
        var value = this._eval();
        return lst.indexOf(value) >= 0;
    };

    ValueWrapper.prototype.eq = function (v) {
        return this._eval() == v;
    }

    return Value;

});
