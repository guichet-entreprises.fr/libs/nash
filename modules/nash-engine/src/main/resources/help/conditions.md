# Expressions

Les expressions permettent de conditionner l'affichage des champs d'une formalité.

Dans le cas suivant, le champ `carColor` sera affiché si et seulement si le champ `hasCar` est renseigné avec la valeur `vrai` :

        ...
        <data id="hasCar" type="Boolean" />
        <data id="carColor" type="String" if="$hasCar" />
        ...


## Contexte d'exécution

| Objet   | Description                |
| ------- | -------------------------- |
| context | Contexte du dossier        |
| form    | Dossier en cours de saisie |

## Objets et propriétés

Propriétés courantes dans le groupe (notation relative) :

    $lastname

    $address.city

Propriétés globales (notation absolue) :

    $form.lastname

    $form.address.city

## Opérateurs

Opérateur | Description
----------------|------------
`$a == $b` | Egal
`$a != $b` | Différent
`$a < $b` | Strictement inférieur
`$a <= $b` | Inférieur ou égal
`$a > $b` | Strictement supérieur
`$a >= $b` | Supérieur ou égal
`$a or $b` | OU logique
`$a and $b` | ET logique
`not $cond` | Négation
`$cond ? $a : $b` | Opérateur ternaire

Les opérateurs peuvent être regroupés en utilisant des parenthèses :

    (null == $a or $a == $b) and (null == $c or $c == $d)

N'aura pas la même valeur que :

    null == $a or ($a == $b and null == $c) or $c == $d

