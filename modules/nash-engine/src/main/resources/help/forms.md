# Spécification d'une formalité

## Structure générale

La spécification d'une formalité est représentée par un fichier archive (zip) contenant l'ensemble des fichiers XML décrivant celle-ci, éventuellement accompagnés des fichiers permettant l'affichage de ceux-ci.

| Fichier | Obligatoire | Description |
|---------|:-----------:|-------------|
| description.xml | x | Description générale de la formalité |
| 0-context/context.xml | x | Description du contexte entrant |
| 0-context/context.css | | Feuille de style associée au contexte |
| 1-data/data.xml | x | Description de la saisie utilisateur |
| 1-data/data.css | | Feuille de style associée à la saisie utilisateur |


En cas d'abscence, les fichiers optionnels sont remplacés par leur équivalent par défaut.


## Description générale

La description générale contient :

| Propriété | Obligatoire | Description |
|-----------|:-----------:|-------------|
| **version** | x | Version du moteur utilisé |
| **uid** | x | UID de la formalité, généré lors de l'upload de l'archive, après validation. |
| **title** | x | Libellé de la formalité |
| **description** | | Description de la formalité |


    <description version="1">
        <uid />
        <title>Cerfa 14004*02</title>
        <description />
    </description>


## Description du contexte entrant

    <context>
        ...
    </context>


## Description de la saisie utilisateur

Racine `<form />` du document :

| Propriété | Obligatoire | Description |
|-----------|:-----------:|-------------|
| **id** | x | ID de la saisie |
| **label** | | Libellé de la saisie. |
| **description** | | Description de la saisie utilisateur |
| **help** | | Aide globale de la saisie utilisateur |


    <form id="cerfa_14004" label="Déclaration en mairie des meubles de tourisme">
        <description>...</description>
        <help>...</help>
        ...
    </form>


L'élément racine contient les sous-éléments suivant :

* `<default />` indiquant les propriétés par défaut sur les groupes et les données.
* `<group />` décrivant les groupes de niveau 0.


### Propriétés par défaut

Dans l'exemple suivant, aucune aucune propriété par défaut n'est indiquée pour les groupes. Quant aux données, elles sont obligatoires et de type `String` par défault.

    <default>
        <group />
        <data mandatory="true" type="String" />
    </default>


### Description des groupes

    <group id="" label="">
    </group>


### Description des données

    <data id="" label="">
    </data>
