/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Value adapter version.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class Version implements Comparable<Version> {

    /** Dots version separator. */
    private static final char DOTS_VERSION_SEPARATOR = '.';

    /** Version parts pattern. */
    private static final Pattern VERSION_PARTS_PATTERN = Pattern.compile("[0-9]+");

    /** List version. */
    private final List<Integer> listVersion;

    /** Dots version. */
    private String dotsVersion;

    /**
     * Instantie un nouveau version.
     *
     * @param baseVersion          base version
     */
    public Version(final String baseVersion) {
        this.listVersion = new ArrayList<>();
        if (null == baseVersion) {
            this.dotsVersion = null;
        } else {
            final Matcher match = VERSION_PARTS_PATTERN.matcher(baseVersion);
            while (match.find()) {
                this.listVersion.add(Integer.parseInt(match.group()));
            }
            this.dotsVersion = StringUtils.join(this.listVersion, DOTS_VERSION_SEPARATOR);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.dotsVersion).toHashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        } else {
            final Version other = (Version) obj;
            return new EqualsBuilder().append(this.dotsVersion, other.dotsVersion).isEquals();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(final Version other) {
        if (this.listVersion != null && other.listVersion != null) {
            final int maxSize = Math.max(this.listVersion.size(), other.listVersion.size());
            for (int i = 0; i < maxSize; ++i) {
                int thisVersion = 0;
                if (i < this.listVersion.size()) {
                    thisVersion = this.listVersion.get(i);
                }

                int otherVersion = 0;
                if (i < this.listVersion.size()) {
                    otherVersion = other.listVersion.get(i);
                }

                if (thisVersion < otherVersion) {
                    return -1;
                } else if (thisVersion > otherVersion) {
                    return 1;
                }
            }
        }

        return 0;
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.dotsVersion;
    }

}
