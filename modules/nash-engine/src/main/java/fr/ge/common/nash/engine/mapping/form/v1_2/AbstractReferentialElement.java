/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import fr.ge.common.nash.engine.mapping.IXml;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialExternalElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialTextElement;

/**
 * Generic referential element.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@XmlTransient
@XmlAccessorType(XmlAccessType.NONE)
public abstract class AbstractReferentialElement implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The referential text elements. */
    @XmlElement(name = "text")
    private List<ReferentialTextElement> referentialTextElements;

    /** The rule as attr. */
    @XmlAttribute(name = "if")
    private String ruleAsAttr;

    /** The rule as tag. */
    @XmlElement(name = "if")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String ruleAsTag;

    /** The referential external element. */
    @XmlElement(name = "external")
    private ReferentialExternalElement referentialExternalElement;

    /**
     * Getter on attribute {@link #referentialTextElements}.
     *
     * @return List&lt;ReferentialTextElement&gt; referentialTextElements
     */
    public List<ReferentialTextElement> getReferentialTextElements() {
        return this.referentialTextElements;
    }

    /**
     * Setter on attribute {@link #referentialTextElements}.
     *
     * @param referentialTextElements
     *            the new value of attribute referentialTextElements
     */
    public void setReferentialTextElements(final List<ReferentialTextElement> referentialTextElements) {
        this.referentialTextElements = Optional.ofNullable(referentialTextElements).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

    /**
     * Getter on attribute {@link #ruleAsAttr}.
     *
     * @return String ruleAsAttr
     */
    public String getRuleAsAttr() {
        return this.ruleAsAttr;
    }

    /**
     * Setter on attribute {@link #ruleAsAttr}.
     *
     * @param ruleAsAttr
     *            the new value of attribute ruleAsAttr
     */
    public void setRuleAsAttr(final String ruleAsAttr) {
        this.ruleAsAttr = ruleAsAttr;
    }

    /**
     * Getter on attribute {@link #ruleAsTag}.
     *
     * @return String ruleAsTag
     */
    public String getRuleAsTag() {
        return this.ruleAsTag;
    }

    /**
     * Setter on attribute {@link #ruleAsTag}.
     *
     * @param ruleAsTag
     *            the new value of attribute ruleAsTag
     */
    public void setRuleAsTag(final String ruleAsTag) {
        this.ruleAsTag = ruleAsTag;
    }

    /**
     * Gets the display condition.
     *
     * @return the display condition
     */
    public String getDisplayCondition() {
        return null == this.ruleAsTag ? this.ruleAsAttr : this.ruleAsTag;
    }

    /**
     * Sets the display condition.
     *
     * @param displayCondition
     *            the new display condition
     */
    public void setDisplayCondition(final String displayCondition) {
        this.ruleAsAttr = null;
        this.ruleAsTag = displayCondition;
    }

    /**
     * Gets the referential external element.
     *
     * @return the referential external element
     */
    public ReferentialExternalElement getReferentialExternalElement() {
        return this.referentialExternalElement;
    }

    /**
     * Sets the referential external element.
     *
     * @param referentialExternalElement
     *            the new referential external element
     */
    public void setReferentialExternalElement(final ReferentialExternalElement referentialExternalElement) {
        this.referentialExternalElement = referentialExternalElement;
    }

}
