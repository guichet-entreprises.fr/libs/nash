/**
 * 
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.engine.mapping.IXml;
import fr.ge.common.nash.engine.mapping.form.v1_2.roles.RoleElement;

/**
 * @author ijijon
 *
 */
@XmlRootElement(name = "entity")
@XmlAccessorType(XmlAccessType.NONE)
public class FormSpecificationEntityRoles implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The fullPath of the Entity. */
    @XmlAttribute
    private String fullPath;

    /** The roles. */
    @XmlElement(name = "role")
    private List<RoleElement> roles = null;

    /**
     * Getter on attribute {@link #fullPath}.
     *
     * @return String fullPath
     */
    public String getFullPath() {
        return fullPath;
    }

    /**
     * Setter on attribute {@link #fullPath}.
     *
     * @param fullPath
     *            the new value of attribute fullPath
     */
    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    /**
     * Getter on attribute {@link #roles}.
     *
     * @return List&lt;RoleElement&gt; events
     */
    public List<RoleElement> getRoles() {
        return this.roles;
    }

    /**
     * Setter on attribute {@link #roles}.
     *
     * @param roles
     *            the new value of attribute roles
     */
    public void setRoles(final List<RoleElement> roles) {
        this.roles = Optional.ofNullable(roles).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

}
