package fr.ge.common.nash.engine.script;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.utils.CoreUtil;

/**
 * {@link ScriptExpressionExecutor} factory.
 *
 * @author Christian Cougourdan
 */
@Service
public class ScriptExpressionExecutorFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptExpressionExecutorFactory.class);

    private static final Pattern PATTERN_CAMELCASE = Pattern.compile("[^a-z]+([a-z])");

    @Value("${path.externalLibs:}")
    private String externalLibsPath;

    @Value("${scripts.execution.timeout:900}")
    private int timeout;

    @Autowired
    private Engine engine;

    private Map<String, Object> globalScriptContext;

    private String internalRuntimeLibraries;

    private Map<String, Object> runtimeLibraries;

    /**
     * Load all transverse javascript libraries.
     *
     * @throws ScriptException
     */
    @PostConstruct
    public void init() {
        final ScriptExpressionExecutor executor = new ScriptExpressionExecutor();
        final Bindings bindings = executor.getEngine().getContext().getBindings(ScriptContext.ENGINE_SCOPE);
        this.loadCoreBindings(bindings);

        final Map<String, Object> model = new HashMap<>();
        model.putAll(this.loadInternalLibs(executor));
        model.putAll(this.loadExternalLibs(executor));

        bindings.remove("globalContext");
        bindings.remove("self");

        this.globalScriptContext = bindings;
        this.runtimeLibraries = model;
    }

    private void loadCoreBindings(final Bindings bindings) {
        final Logger logger = LoggerFactory.getLogger(ScriptExpressionExecutor.class);
        bindings.put("log", logger);
        bindings.put("_log", logger);
        bindings.put("logger", logger);
        bindings.put("_logger", logger);

        bindings.put("_CONFIG_", this.engine.getEngineProperties());
        bindings.put("helper", new ScriptExecutionHelper());

        // bindings.put("nash", new HashMap<String, Object>());
    }

    /**
     * Load internal libraries, provided from engine.
     *
     * @param executor
     *            script executor
     * @return loaded libraries
     */
    private Map<String, Object> loadInternalLibs(final ScriptExpressionExecutor executor) {
        final String expr = Arrays.asList("require", "util", "/public/js/engine/Value", "jquery", "externals", "root").stream() //
                .map(lib -> {
                    try {
                        return String.format("load('%s');", Engine.class.getResource((lib.startsWith("/") ? "" : "util/") + lib + ".js").toURI());
                    } catch (final URISyntaxException e) {
                        throw new TechnicalException("Loading core libraries : [" + lib + "] not found");
                    }
                }) //
                .collect(Collectors.joining("\n"));

        CoreUtil.time("Initialize engine context with base libraries", () -> executor.eval(expr + "\nvar util = require('util'), Value = require('engine/Value');", null));

        final Map<String, Object> model = new HashMap<>();
        for (final String lib : Arrays.asList("meta", "pdf", "pushingbox", "spec", "postpayment", "ext")) {
            CoreUtil.time("Initialize engine context with " + lib, () -> {
                try {
                    final URL resourceUrl = Engine.class.getResource("util/" + lib + ".js");
                    if (null == resourceUrl) {
                        throw new TechnicalException("Loading core libraries : [" + lib + "] not found");
                    } else {
                        executor.eval(String.format("load('%s');", resourceUrl.toURI()), null);
                    }
                } catch (final URISyntaxException ex) {
                    throw new TechnicalException("Loading core libraries : error while reading resource [" + lib + "]");
                }
            });
        }
        this.internalRuntimeLibraries = null; // builder.toString();

        return model;
    }

    /**
     * Load external libraries, provided from local file system.
     *
     * @param executor
     *            script executor
     * @return loaded libraries
     */
    private Map<String, Object> loadExternalLibs(final ScriptExpressionExecutor executor) {
        if (StringUtils.isEmpty(this.externalLibsPath)) {
            return Collections.emptyMap();
        }

        final File[] libFiles = Paths.get(this.externalLibsPath).toFile().listFiles(name -> name.getName().endsWith(".js"));
        if (null == libFiles) {
            return Collections.emptyMap();
        }

        final StringBuilder loader = new StringBuilder("var libs = {};\n");
        for (final File src : libFiles) {
            loader.append(String.format("libs.%s = load('%s');\n", toCamelCase(src.getName()), src.toURI()));
        }
        loader.append("libs;");

        LOGGER.trace("External libs loading script :\n{}", loader.toString());
        final Object obj = executor.eval(loader.toString(), null);

        LOGGER.trace("External libs object : {}", obj);

        return Collections.singletonMap("libs", obj);
    }

    /**
     * Transform input string into camel case.
     *
     * @param src
     *            string to transform
     * @return camel case string
     */
    private static String toCamelCase(final String src) {
        return CoreUtil.searchAndReplace(src.toLowerCase().replaceAll("\\.[^.]*$", ""), PATTERN_CAMELCASE, m -> m.group(1).toUpperCase());
    }

    /**
     * Return a new {@link ScriptExpressionExecutor} instance with initialized
     * bindings and timeout properties.
     *
     * @return initialized script executor instance
     */
    public ScriptExpressionExecutor getScriptExpressionExecutor() {
        return new ScriptExpressionExecutor() //
                .setBindings(this.globalScriptContext) //
                .setTimeout(this.timeout);
    }

}
