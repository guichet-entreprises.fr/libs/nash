/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.exception.TechnicalException;

/**
 * HTTP request data.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class FormContentData {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FormContentData.class);

    /** Dot. */
    private static final String DOT = ".";

    /** Error : list as boolean. */
    private static final String ERROR_LIST_AS_BOOLEAN = "Cannot get value as Boolean when it is a list";

    /** Error : list as int. */
    private static final String ERROR_LIST_AS_INT = "Cannot get value as Int when it is a list";

    /** Error : list as int. */
    private static final String ERROR_LIST_AS_FLOAT = "Cannot get value as Float when it is a list";

    /** Error : list as string. */
    private static final String ERROR_LIST_AS_STRING = "Cannot get value as String when it is a list";

    /** Left bracket. */
    private static final String LBRA = "[";

    /** Pattern of a list element. */
    private static final Pattern PATTERN_LIST_ELEMENT = Pattern.compile("(\\[[0-9]+\\]|\\.[0-9]+).*");

    private static final Pattern PATTERN_INDEXED_ELEMENT = Pattern.compile("(.+)\\[([^\\]]*)\\]");

    /** True as number. */
    private static final String TRUE_AS_NUMBER = "1";

    /** Current path. */
    private String currentPath;

    /** Map. */
    private final Map<String, String[]> map;

    /**
     * Constructor.
     *
     * @param map
     *            the map
     */
    public FormContentData(final Map<String, String[]> map) {
        this.currentPath = StringUtils.EMPTY;
        this.map = map;
    }

    /**
     * Constructor.
     *
     * @param map
     *            the map
     * @param currentPath
     *            the current path
     */
    private FormContentData(final Map<String, String[]> map, final String currentPath) {
        this.currentPath = currentPath;
        this.map = map;
    }

    /**
     * Updates the current path.
     *
     * @param path
     *            the path
     * @return the data with an updated path
     */
    public FormContentData withPath(final String path) {
        final StringBuilder newPath = new StringBuilder(this.currentPath);
        if (StringUtils.isNotEmpty(newPath) && !path.startsWith(DOT) && !path.startsWith(LBRA)) {
            newPath.append(DOT);
        }
        newPath.append(path);
        return new FormContentData(this.map, newPath.toString());
    }

    /**
     * Gets the value associated to the current path as a map.
     *
     * @return the value
     */
    public Map<String, String[]> asMap() {
        final Map<String, String[]> res = new HashMap<>();
        for (final Entry<String, String[]> entry : this.map.entrySet()) {
            if (entry.getKey().startsWith(this.currentPath)) {
                String keyCurrentPath = entry.getKey().substring(this.currentPath.length());
                if (keyCurrentPath.startsWith(DOT)) {
                    keyCurrentPath = keyCurrentPath.substring(1);
                }
                res.put(keyCurrentPath, entry.getValue());
            }
        }
        return res;
    }

    /**
     * Gets children from a content data as a list.
     */
    public List<FormContentData> children() {
        final List<FormContentData> res = new ArrayList<>();
        final Map<String, Boolean> pathsChildren = new HashMap<>();
        for (final Entry<String, String[]> entry : this.asMap().entrySet()) {
            final Matcher matcher = Pattern.compile("^([^\\[\\.]+)(?:\\[([^\\]]*)\\])?.*").matcher(entry.getKey());
            if (matcher.matches()) {
                final String pathChild = matcher.group(1);
                final String index = matcher.group(2);
                pathsChildren.put(pathChild, StringUtils.isNotEmpty(index));
            }
        }
        for (final Entry<String, Boolean> pathChild : pathsChildren.entrySet()) {
            if (pathChild.getValue()) {
                res.addAll(this.asList(pathChild.getKey()));
            } else {
                res.add(this.withPath(pathChild.getKey()));
            }
        }
        return res;
    }

    /**
     * Gets the value associated to the current path as a map.
     *
     * @param path
     *            the path
     * @return the value
     */
    public Map<String, String[]> asMap(final String path) {
        return this.withPath(path).asMap();
    }

    /**
     * Gets the value associated to the current path as a list.
     *
     * @return the value
     */
    public List<FormContentData> asList() {
        final List<FormContentData> res = new ArrayList<>();
        final String[] value = this.map.get(this.currentPath);
        if (value == null) {
            // field with several occurrences ([0] or .0)
            final Set<String> listElementKeys = new HashSet<>();
            for (final Entry<String, String[]> entry : this.map.entrySet()) {
                if (entry.getKey().startsWith(this.currentPath)) {
                    final String keyCurrentPath = entry.getKey().substring(this.currentPath.length());
                    final Matcher matcher = PATTERN_LIST_ELEMENT.matcher(keyCurrentPath);
                    if (matcher.matches()) {
                        final String listElementKey = matcher.group(1);
                        if (!listElementKeys.contains(listElementKey)) {
                            listElementKeys.add(listElementKey);
                            res.add(this.withPath(matcher.group(1)));
                        }
                    }
                }
            }
        } else {
            // field of type multiple
            for (int i = 0; i < value.length; ++i) {
                final Map<String, String[]> subMap = new HashMap<>();
                subMap.put(StringUtils.EMPTY, new String[] { value[i] });
                res.add(new FormContentData(subMap, StringUtils.EMPTY));
            }
        }

        res.sort((a, b) -> {
            int idxA = 0, idxB = 0;
            Matcher m = PATTERN_INDEXED_ELEMENT.matcher(a.currentPath);
            if (m.matches()) {
                idxA = StringUtils.isEmpty(m.group(2)) ? 0 : Integer.parseInt(m.group(2));
            }

            m = PATTERN_INDEXED_ELEMENT.matcher(b.currentPath);
            if (m.matches()) {
                idxB = StringUtils.isEmpty(m.group(2)) ? 0 : Integer.parseInt(m.group(2));
            }

            return idxA - idxB;
        });

        return res;
    }

    /**
     * Gets the value associated to the current path as a list.
     *
     * @param path
     *            the path
     * @return the value
     */
    public List<FormContentData> asList(final String path) {
        return this.withPath(path).asList();
    }

    /**
     * Gets the value associated to the current path as a string.
     *
     * @return the value
     */
    public String asString() {
        String res = null;
        final String[] value = this.map.get(this.currentPath);
        if (ArrayUtils.isEmpty(value)) {
            if (!this.asList().isEmpty()) {
                throw new TechnicalException(ERROR_LIST_AS_STRING);
            }
        } else {
            if (value.length == 1) {
                res = value[0];
            } else {
                throw new TechnicalException(ERROR_LIST_AS_STRING);
            }
        }
        return res;
    }

    /**
     * Gets the value associated to the current path as a string.
     *
     * @param path
     *            the path
     * @return the value
     */
    public String asString(final String path) {
        return this.withPath(path).asString();
    }

    /**
     * Gets the value associated to the current path as an int.
     *
     * @return the value
     */
    public Integer asInt() {
        Integer res = null;
        String stringValue = null;
        try {
            stringValue = this.asString();
        } catch (final TechnicalException e) {
            throw new TechnicalException(ERROR_LIST_AS_INT, e);
        }
        if (!StringUtils.isEmpty(stringValue)) {
            try {
                res = Integer.valueOf(stringValue);
            } catch (final NumberFormatException e) {
                LOGGER.debug(StringUtils.EMPTY, e);
            }
        }
        return res;
    }

    /**
     * Gets the value associated to the current path as an int.
     *
     * @param path
     *            the path
     * @return the value
     */
    public Integer asInt(final String path) {
        return this.withPath(path).asInt();
    }

    /**
     * Gets the value associated to the current path as a boolean.
     *
     * @return the value
     */
    public boolean asBoolean() {
        boolean res = false;
        String stringValue = null;
        try {
            stringValue = this.asString();
        } catch (final TechnicalException e) {
            throw new TechnicalException(ERROR_LIST_AS_BOOLEAN, e);
        }
        if (TRUE_AS_NUMBER.equals(stringValue)) {
            res = true;
        } else {
            res = BooleanUtils.toBoolean(stringValue);
        }
        return res;
    }

    /**
     * Gets the value associated to the current path as a boolean.
     *
     * @param path
     *            the path
     * @return the value
     */
    public boolean asBoolean(final String path) {
        return this.withPath(path).asBoolean();
    }

    /**
     * Getter on attribute {@link #currentPath}.
     *
     * @return String currentPath
     */
    public String getCurrentPath() {
        return this.currentPath;
    }

    /**
     * Setter on attribute {@link #currentPath}.
     *
     * @param currentPath
     *            the new value of attribute currentPath
     */
    public void setCurrentPath(final String currentPath) {
        this.currentPath = currentPath;
    }

    /**
     * Gets the value associated to the current path as a float.
     *
     * @return the value
     */
    public Float asFloat() {
        Float res = null;
        String stringValue = null;
        try {
            stringValue = this.asString();
        } catch (final TechnicalException e) {
            throw new TechnicalException(ERROR_LIST_AS_FLOAT, e);
        }
        if (!StringUtils.isEmpty(stringValue)) {
            try {
                res = Float.parseFloat(stringValue);
            } catch (final NumberFormatException e) {
                LOGGER.debug(StringUtils.EMPTY, e);
            }
        }
        return res;
    }
}
