/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.description;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.bean.StepUserTypeEnum;

/**
 * The Class StepElement.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "step")
@XmlAccessorType(XmlAccessType.NONE)
public class StepElement implements IFlowElement {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @XmlAttribute
    private String id;

    /** The data. */
    @XmlAttribute
    private String data;

    /** The preprocess. */
    @XmlAttribute
    private String preprocess;

    /** The postprocess. */
    @XmlAttribute
    private String postprocess;

    /** The icon. */
    @XmlAttribute
    private String icon;

    /** position. */
    @XmlTransient
    private int position;

    /** The Status. */
    @XmlAttribute
    private String status;

    /** The user. */
    @XmlAttribute
    private String user;

    /** The label. */
    @XmlAttribute
    private String label;

    /** The removable attribut. */
    @XmlAttribute
    private Boolean removable;

    /** The button save and quit. */
    @XmlElement(name = "saveQuit")
    private ButtonElement saveQuit;

    /** The button next page. */
    @XmlElement(name = "nextPage")
    private ButtonElement nextPage;

    /** The button next step. */
    @XmlElement(name = "nextStep")
    private ButtonElement nextStep;

    /** The button finalize. */
    @XmlElement(name = "finalize")
    private ButtonElement finalize;

    /** The button previous. */
    @XmlElement(name = "previous")
    private ButtonElement previousButton;

    /**
     * Constructor.
     */
    public StepElement() {
        // Nothing to do
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public String getData() {
        return this.data;
    }

    /**
     * Gets the preprocess.
     *
     * @return the preprocess
     */
    public String getPreprocess() {
        return this.preprocess;
    }

    /**
     * Gets the postprocess.
     *
     * @return the postprocess
     */
    public String getPostprocess() {
        return this.postprocess;
    }

    /**
     * Gets the icon.
     *
     * @return the icon
     */
    public String getIcon() {
        return this.icon;
    }

    /**
     * Getter on attribute {@link #position}.
     *
     * @return the position
     */
    public int getPosition() {
        return this.position;
    }

    /**
     * Setter on attribute {@link #position}.
     *
     * @param position
     *     the position to set
     */
    public void setPosition(final int position) {
        this.position = position;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).toHashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        } else {
            final StepElement other = (StepElement) obj;
            return new EqualsBuilder().append(this.id, other.id).isEquals();
        }
    }

    /**
     * Getter on attribute {@link #status}.
     *
     * @return the status
     */
    public String getStatus() {
        if (StringUtils.isBlank(this.status)) {
            this.status = StepStatusEnum.TO_DO.getStatus();
        }
        return this.status;
    }

    /**
     * Setter on attribute {@link #status}.
     *
     * @param status
     *     the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Getter on attribute {@link #user}.
     *
     * @return String user
     */
    public String getUser() {
        if (StringUtils.isBlank(this.user)) {
            this.user = StepUserTypeEnum.USER.getName();
        }
        return this.user;
    }

    /**
     * Setter on attribute {@link #user}.
     *
     * @param user
     *     the new value of attribute user
     */
    public void setUser(final String user) {
        this.user = user;
    }

    /**
     * Getter on attribute {@link #label}.
     *
     * @return the label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Setter on attribute {@link #label}.
     *
     * @param label
     *     the label to set
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Setter on attribute {@link #id}.
     *
     * @param id
     *     the new value of attribute id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Setter on attribute {@link #data}.
     *
     * @param data
     *     the new value of attribute data
     */
    public void setData(final String data) {
        this.data = data;
    }

    /**
     * Setter on attribute {@link #preprocess}.
     *
     * @param preprocess
     *     the new value of attribute preprocess
     */
    public void setPreprocess(final String preprocess) {
        this.preprocess = preprocess;
    }

    /**
     * Setter on attribute {@link #postprocess}.
     *
     * @param postprocess
     *     the new value of attribute postprocess
     */
    public void setPostprocess(final String postprocess) {
        this.postprocess = postprocess;
    }

    /**
     * Setter on attribute {@link #icon}.
     *
     * @param icon
     *     the new value of attribute icon
     */
    public void setIcon(final String icon) {
        this.icon = icon;
    }

    /**
     * Getter on attribute {@link #removable}.
     *
     * @return the removable attribut value
     */
    public Boolean getRemovable() {
        return this.removable;
    }

    /**
     * Setter on attribute {@link #removable}.
     *
     * @param removable
     *     the removable value to set
     */
    public void setRemovable(final boolean removable) {
        this.removable = removable;
    }

    /**
     * Accesseur sur l'attribut {@link #saveQuit}.
     *
     * @return ButtonElement saveQuit
     */
    public ButtonElement getSaveQuit() {
        return this.saveQuit;
    }

    /**
     * Mutateur sur l'attribut {@link #saveQuit}.
     *
     * @param saveQuit
     *     la nouvelle valeur de l'attribut saveQuit
     */
    public void setSaveQuit(final ButtonElement saveQuit) {
        this.saveQuit = saveQuit;
    }

    /**
     * Accesseur sur l'attribut {@link #nextPage}.
     *
     * @return ButtonElement nextPage
     */
    public ButtonElement getNextPage() {
        return this.nextPage;
    }

    /**
     * Mutateur sur l'attribut {@link #nextPage}.
     *
     * @param nextPage
     *     la nouvelle valeur de l'attribut nextPage
     */
    public void setNextPage(final ButtonElement nextPage) {
        this.nextPage = nextPage;
    }

    /**
     * Accesseur sur l'attribut {@link #nextStep}.
     *
     * @return ButtonElement nextStep
     */
    public ButtonElement getNextStep() {
        return this.nextStep;
    }

    /**
     * Mutateur sur l'attribut {@link #nextStep}.
     *
     * @param nextStep
     *     la nouvelle valeur de l'attribut nextStep
     */
    public void setNextStep(final ButtonElement nextStep) {
        this.nextStep = nextStep;
    }

    /**
     * Accesseur sur l'attribut {@link #finalize}.
     *
     * @return ButtonElement finalize
     */
    public ButtonElement getFinalize() {
        return this.finalize;
    }

    /**
     * Mutateur sur l'attribut {@link #finalize}.
     *
     * @param finalize
     *     la nouvelle valeur de l'attribut finalize
     */
    public void setFinalize(final ButtonElement finalize) {
        this.finalize = finalize;
    }

    /**
     * Accesseur sur l'attribut {@link #previousButton}.
     *
     * @return ButtonElement previousButton
     */
    public ButtonElement getPreviousButton() {
        return this.previousButton;
    }

    /**
     * Mutateur sur l'attribut {@link #previousButton}.
     *
     * @param previousButton
     *     la nouvelle valeur de l'attribut previousButton
     */
    public void setPreviousButton(final ButtonElement previousButton) {
        this.previousButton = previousButton;
    }

}
