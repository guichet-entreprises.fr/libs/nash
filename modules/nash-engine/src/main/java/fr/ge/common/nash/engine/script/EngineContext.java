/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.script;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.core.bean.ElementPath.Token;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.manager.processor.bridge.DocumentProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.IBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.ScriptProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.ServiceProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.TrackerProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.TransformationProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.UtilProcessorBridge;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.PatternProvider;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class EngineContext.
 *
 * @author Christian Cougourdan
 * @param <T>
 *     the generic type
 */
public final class EngineContext<T> implements Closeable {

    private static final Pattern PATTERN_RESOURCE_PATH = Pattern.compile("(/)?((?:.*/)?[^/]+)");

    /** The record. */
    private final SpecificationLoader record;

    /** The provider. */
    private final IProvider provider;

    /** The provider. */
    private IProvider scriptProvider;

    /** The current element. */
    private final T currentElement;

    private final EngineContext<?> parent;

    /** The libraries. */
    private final Map<String, Object> libraries = new HashMap<>();

    private final ApplicationContext applicationContext;

    /**
     * Instantiates a new engine context.
     *
     * @param parent
     *     parent context
     * @param record
     *     target record
     * @param provider
     *     target provider
     * @param currentElement
     *     target element
     * @param applicationContext
     *     application context
     */
    private EngineContext(final EngineContext<?> parent, final SpecificationLoader record, final IProvider provider, final T currentElement, final ApplicationContext applicationContext) {
        this.parent = parent;
        this.record = record;
        this.provider = provider;
        this.scriptProvider = provider;
        this.currentElement = currentElement;
        this.applicationContext = applicationContext;
    }

    /**
     * Builds the.
     *
     * @param record
     *     the record
     * @return the engine context
     */
    public static EngineContext<SpecificationLoader> build(final SpecificationLoader record) {
        return new EngineContext<>(null, record, record.getProvider(), record, record.getApplicationContext());
    }

    /**
     * Builds the.
     *
     * @param record
     *     the record
     * @param provider
     *     the provider
     * @return the engine context
     */
    public static EngineContext<SpecificationLoader> build(final SpecificationLoader record, final IProvider provider) {
        return new EngineContext<>(null, record, provider, record, record.getApplicationContext());
    }

    /**
     * Target.
     *
     * @param <S>
     *     the generic type
     * @param me
     *     the me
     * @return the engine context
     */
    @SuppressWarnings("resource")
    public <S> EngineContext<S> target(final S me) {
        return new EngineContext<>(this, this.record, this.provider, me, this.applicationContext).setScriptProvider(this.scriptProvider);
    }

    public <S> EngineContext<S> target(final String resourcePath, final Class<S> expected) {
        if (StringUtils.isEmpty(resourcePath)) {
            return null;
        }

        final S bean = this.provider.asBean(resourcePath, expected);
        if (null == bean) {
            return null;
        }

        return new EngineContext<>(this, this.record, PatternProvider.from(this.provider, resourcePath), bean, this.applicationContext);
    }

    /**
     * Defines the libraries.
     *
     * @return the libraries
     */
    private void defineLibraries() {
        if (MapUtils.isEmpty(this.libraries)) {
            Arrays.asList( //
                    new ServiceProcessorBridge(this), //
                    new TransformationProcessorBridge(this), //
                    new UtilProcessorBridge(this), //
                    new DocumentProcessorBridge(this), //
                    new ScriptProcessorBridge(this), //
                    new TrackerProcessorBridge(this) //
            ).forEach(bridge -> this.libraries.put(bridge.getName(), bridge));
            this.libraries.put("record", this.record);
        }
    }

    /**
     * Builds the expression context.
     *
     * @param model
     *     the model
     * @param bridges
     *     the bridges
     * @return the i context
     */
    public ScriptExecutionContext buildExpressionContext(final Map<String, Object> model, final IBridge... bridges) {
        final ScriptExecutionContext context = new ScriptExecutionContext();

        context.addAll(model);

        this.updateExpressionContext(context);

        for (final IBridge bridge : bridges) {
            if (bridge != null) {
                context.addBridge(bridge);
                context.addExtra("_" + bridge.getName(), bridge);
            }
        }

        if (null != Engine.getLocalUserContextEvents()) {
            context.addExtra("user", Engine.getLocalUserContextEvents().getCurrentUser());
        }

        return context;
    }

    public ScriptExecutionContext buildExpressionContext(final IBridge... bridges) {
        final Map<String, Object> model = new HashMap<>();

        final EngineContext<? extends FormSpecificationData> stepEngineContext = this.getParent(FormSpecificationData.class);
        final Map<String, Object> partialRecordModel = Optional.ofNullable(stepEngineContext) //
                .map(ng -> RecursiveDataModelExtractor.create(ng.getProvider()).extract(ng.getElement())) //
                .orElseGet(HashMap::new);

        final Map<String, Object> stepModel = CoreUtil.cast(partialRecordModel.get(stepEngineContext.getElement().getId()));

        model.put(Scopes.RECORD.toContextKey(), partialRecordModel);
        model.put(Scopes.STEP.toContextKey(), stepModel);

        if (this.getElement() instanceof IElement<?>) {
            final IElement<?> elm = (IElement<?>) this.getElement();
            final ElementPath path = ElementPath.create(elm.getPath());
            model.put(Scopes.PAGE.toContextKey(), stepModel.get(Optional.ofNullable(path.getStepId()).orElse(path.getId())));
        }

        return this.buildExpressionContext(model, bridges);
    }

    public ScriptExecutionContext updateExpressionContext(final ScriptExecutionContext ctx) {
        if (null != this.record && null != this.provider) {
            this.defineLibraries();
            ctx.addExtra("_CONFIG_", this.record.getConfiguration());
            ctx.addExtra("_PROVIDER_", this.provider);
        }

        this.libraries.entrySet().forEach(entry -> ctx.addApi(entry.getKey(), entry.getValue()));

        return ctx;
    }

    /**
     * Gets the record.
     *
     * @return the record
     */
    public SpecificationLoader getRecord() {
        return this.record;
    }

    /**
     * Gets the element.
     *
     * @return the element
     */
    public T getElement() {
        if (null == this.currentElement) {
            throw new TechnicalException("Engine context has no element specified");
        }

        return this.currentElement;
    }

    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    public Configuration getConfiguration() {
        return this.record.getConfiguration();
    }

    /**
     * Gets the base provider.
     *
     * @return the base provider
     */
    public IProvider getBaseProvider() {
        return this.record.getProvider();
    }

    /**
     * Gets the provider.
     *
     * @return the provider
     */
    public IProvider getProvider() {
        return this.provider;
    }

    protected EngineContext<T> setScriptProvider(final IProvider scriptProvider) {
        this.scriptProvider = scriptProvider;
        return this;
    }

    public IProvider getScriptProvider() {
        return this.scriptProvider;
    }

    public EngineContext<?> getParent() {
        return this.parent;
    }

    public ApplicationContext getApplicationContext() {
        return this.applicationContext;
    }

    private <R> EngineContext<? extends R> get(final EngineContext<?> root, final Class<R> expected) {
        for (EngineContext<?> ctx = root; null != ctx; ctx = ctx.parent) {
            if (expected.isInstance(ctx.currentElement)) {
                return CoreUtil.cast(ctx);
            }
        }
        return null;
    }

    public <R> EngineContext<? extends R> get(final Class<R> expected) {
        return this.get(this, expected);
    }

    public <R> EngineContext<? extends R> getParent(final Class<R> expected) {
        return this.get(this.parent, expected);
    }

    public EngineContext<T> withProvider(final IProvider provider) {
        if (null == provider) {
            return this;
        } else {
            return new EngineContext<>(this.parent, this.record, provider, this.currentElement, this.applicationContext);
        }
    }

    @SuppressWarnings("resource")
    public EngineContext<T> withScriptProvider(final IProvider scriptProvider) {
        if (null == scriptProvider) {
            return this;
        } else {
            return new EngineContext<>(this.parent, this.record, this.provider, this.currentElement, this.applicationContext).setScriptProvider(scriptProvider);
        }
    }

    /**
     * Return provider relative to direct resource folder, .
     *
     * @param resourceName
     *     resource name
     * @return provider
     */
    public IProvider relativeProvider(final String resourceName) {
        if (null == resourceName) {
            return this.provider;
        }

        final Matcher matcher = PATTERN_RESOURCE_PATH.matcher(resourceName);
        if (matcher.matches()) {
            final boolean isAbsolute = "/".equals(matcher.group(1));
            final String relativePath = matcher.group(2);
            final IProvider baseProvider = isAbsolute ? this.record.getProvider() : this.provider;
            if (StringUtils.isEmpty(relativePath)) {
                return baseProvider;
            } else {
                return PatternProvider.from(baseProvider, relativePath);
            }
        }
        return this.provider;
    }

    /**
     * Build execution model related to this element.
     *
     * @return execution model
     */
    public Map<String, Object> buildModel() {
        final Map<String, Object> model = new HashMap<>(this.buildBaseModel());

        if (this.getElement() instanceof IElement<?>) {
            final Map<String, Object> stepModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));
            final IElement<?> elm = CoreUtil.cast(this.getElement());
            final ElementPath path = ElementPath.create(elm.getPath());

            model.put(Scopes.PAGE.toContextKey(), stepModel.get(CoreUtil.coalesce(path::getStepId, path::getPageId, path::getId)));
            model.put(Scopes.GROUP.toContextKey(), this.buildGroupModel(model));
        }

        return model;
    }

    private Map<String, Object> buildBaseModel() {
        final Map<String, Object> model = new HashMap<>();

        if (this.getElement() instanceof SpecificationLoader) {
            final Map<String, Object> recordModel = new HashMap<>();
            final SpecificationLoader loader = (SpecificationLoader) this.getElement();
            loader.stepsMgr().findAll().forEach(step -> {
                recordModel.putAll( //
                        RecursiveDataModelExtractor //
                                .create(loader.provider(step.getData())) //
                                .extract(loader.data(step)));
            });

            model.put(Scopes.RECORD.toContextKey(), recordModel);

            return model;
        } else if (this.getElement() instanceof StepElement) {
            final Map<String, Object> recordModel = new HashMap<>();

            final StepElement step = (StepElement) this.getElement();
            recordModel.putAll(this.getRecord().extractUntil(step));

            final FormSpecificationData stepData = this.record.data(step);
            recordModel.putAll(RecursiveDataModelExtractor.create(this.record.provider(step.getData())).extract(stepData));

            model.put(Scopes.RECORD.toContextKey(), recordModel);
            model.put(Scopes.STEP.toContextKey(), recordModel.get(stepData.getId()));

            return model;
        } else if (this.getElement() instanceof FormSpecificationData) {
            final EngineContext<? extends StepElement> stepElementEngineContext = this.get(StepElement.class);
            final FormSpecificationData stepData = (FormSpecificationData) this.getElement();

            final Map<String, Object> recordModel = RecursiveDataModelExtractor.create(this.getProvider()).extract(stepData);
            final Map<String, Object> stepModel = CoreUtil.cast(recordModel.get(stepData.getId()));

            if (null == stepElementEngineContext) {
                model.put(Scopes.RECORD.toContextKey(), recordModel);
            } else {
                Map<String, Object> parentRecordModel = this.getRecord().extractUntil(stepElementEngineContext.getElement());
                parentRecordModel.put(stepData.getId(), stepModel);
                model.put(Scopes.RECORD.toContextKey(), parentRecordModel);
            }

            model.put(Scopes.STEP.toContextKey(), stepModel);

            return model;
        } else {
            return Optional.ofNullable(this.getParent(StepElement.class)) //
                    .map(EngineContext::buildBaseModel) //
                    .orElseGet( //
                            () -> Optional.ofNullable(this.getParent(FormSpecificationData.class)) //
                                    .map(EngineContext::buildBaseModel) //
                                    .orElseThrow(() -> new TechnicalException()) //
                    );
        }
    }

    private EngineContext<? extends FormSpecificationData> retrieveStepContext() {
        final EngineContext<? extends FormSpecificationData> stepEngineContext = this.get(FormSpecificationData.class);
        if (null != stepEngineContext) {
            return stepEngineContext;
        }

        final EngineContext<? extends StepElement> stepElementEngineContext = this.get(StepElement.class);
        if (null != stepElementEngineContext) {
            final StepElement stepElement = stepElementEngineContext.getElement();
            return stepElementEngineContext.target(this.getRecord().data(stepElement)).withProvider(this.getRecord().provider(stepElement.getData()));
        }

        throw new TechnicalException();
    }

    private Map<String, Object> buildGroupModel(final Map<String, Object> model) {
        final IElement<?> elm = CoreUtil.cast(this.getElement());
        final ElementPath path = ElementPath.create(elm.getPath());
        final List<Token> tokens = new ArrayList<>(path.tokenize());
        final Token pageToken = tokens.remove(0);

        Map<String, Object> grpModel = CoreUtil.cast(model.get(Scopes.PAGE.toContextKey()));

        final EngineContext<? extends FormSpecificationData> stepEngineContext = this.retrieveStepContext();

        IElement<?> grpElement = stepEngineContext.getElement().getGroups().stream() //
                .filter(grp -> pageToken.getKey().equals(grp.getId())) //
                .findFirst() //
                .orElse(null);

        if (null != grpElement && tokens.size() > 2) {
            while (!tokens.isEmpty()) {
                final Token token = tokens.remove(0);
                final IElement<?> foundElement = grpElement.getData().stream() //
                        .filter(grp -> token.toString().equals(grp.getId())) //
                        .findFirst() //
                        .orElse(null);

                if (null == foundElement) {
                    throw new TechnicalException("Element [" + grpElement.getPath() + '.' + token + "] not found");
                } else if (foundElement instanceof GroupElement) {
                    grpElement = foundElement;
                    if (foundElement.isRepeatable()) {
                        final List<Object> lst = CoreUtil.cast(grpModel.get(token.getKey()));
                        if (null == token.getIndex()) {
                            throw new TechnicalException("Found repeatable element [" + grpElement.getPath() + "] but no index specified ");
                        } else if (token.getIndex() >= lst.size()) {
                            throw new TechnicalException("Index out of bound for element [" + grpElement.getPath() + "]");
                        } else {
                            grpModel = CoreUtil.cast(lst.get(token.getIndex()));
                        }
                    } else {
                        grpModel = CoreUtil.cast(grpModel.get(token.getKey()));
                    }
                } else if (!tokens.isEmpty()) {
                    throw new TechnicalException("Element [" + foundElement.getPath() + "] had to be a group element");
                }
            }
        }

        return grpModel;
    }

    /**
     * Return core Nash engine.
     *
     * @return core Nash engine
     */
    public Engine getEngine() {
        return this.record.getEngine();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        if (null != this.libraries) {
            this.libraries.values().stream() //
                    .filter(Closeable.class::isInstance) //
                    .map(Closeable.class::cast) //
                    .forEach(IOUtils::closeQuietly);
        }
    }

}