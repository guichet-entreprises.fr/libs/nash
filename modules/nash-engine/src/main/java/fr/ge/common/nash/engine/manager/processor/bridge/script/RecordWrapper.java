/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge.script;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.SpecificationSteps;
import fr.ge.common.nash.engine.manager.processor.bridge.ScriptProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.ServiceProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceResponse;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.PatternProvider;
import fr.ge.common.nash.engine.script.EngineContext;

/**
 * The Class RecordWrapper.
 *
 * @author Christian Cougourdan
 */
public class RecordWrapper {

    /** The context. */
    private final EngineContext<?> context;

    /** The base url. */
    private String baseUrl;

    /** The loaded data. */
    private final Map<String, DataWrapper> loadedData = new HashMap<>();

    /** The updated resources. */
    private final Map<String, Object> updatedResources = new HashMap<>();

    /** Source identifier to link with. **/
    private String sourceUid;

    /** The acl for the record. **/
    private final List<String> acls = new ArrayList<>();

    /**
     * Instantiates a new record wrapper.
     *
     * @param context
     *     the context
     */
    public RecordWrapper(final EngineContext<?> context) {
        this.context = context;
    }

    /**
     * Sets the base url.
     *
     * @param baseUrl
     *     the base url
     * @return the record wrapper
     */
    public RecordWrapper setBaseUrl(final String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    /**
     * Load.
     *
     * @param resourcePath
     *     the resource path
     * @return the object
     */
    public Object load(final String resourcePath) {
        final FormSpecificationData data = this.context.getProvider().asBean(resourcePath, FormSpecificationData.class);

        final IProvider provider = PatternProvider.from(this.context.getProvider(), resourcePath);

        final DataWrapper loaded = new DataWrapper(this.context.withProvider(provider).target(data));
        this.loadedData.put(resourcePath, loaded);
        return loaded;
    }

    /**
     * Adds the file.
     *
     * @param resourcePath
     *     the resource path
     * @param resource
     *     the resource
     * @return the record wrapper
     */
    public RecordWrapper addFile(final String resourcePath, final Object resource) {
        if (resource instanceof Item) {
            this.context.getProvider().save(resourcePath, ((Item) resource).getContent());
        } else if (resource instanceof byte[]) {
            this.context.getProvider().save(resourcePath, (byte[]) resource);
        } else {
            throw new TechnicalException(String.format("Injecting resource [%s] : object type [%s] not supported", resourcePath, resource.getClass()));
        }

        return this;
    }

    /**
     * Execute.
     *
     * @return the record wrapper
     */
    public RecordWrapper execute() {
        final SpecificationLoader loader = this.context.getRecord();

        StepElement step = loader.stepsMgr().current();
        for (int pos = step.getPosition(); step != null; step = loader.stepsMgr().find(++pos)) {
            loader.processes().preExecute(step);
            loader.validate(step);
            loader.processes().postExecute(step);
            loader.updateStatus(step, StepStatusEnum.DONE, this.context.getRecord().description().getAuthor());
            loader.flush();
        }

        return this;

    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public FormSpecificationDescription getDescription() {
        if (!this.updatedResources.containsKey("description.xml")) {
            this.updatedResources.put("description.xml", this.context.getRecord().description());
        }
        return (FormSpecificationDescription) this.updatedResources.get("description.xml");
    }

    /** The Constant PATTERN_URI_RECORD. */
    private static final Pattern PATTERN_URI_RECORD = Pattern.compile("^.*/Record/code/(.*)$", Pattern.CASE_INSENSITIVE);

    /**
     * Save.
     *
     * @return the string
     */
    public String save() {
        this.loadedData.entrySet().stream() //
                .filter(entry -> entry.getValue().isDirty()) //
                .forEach(entry -> this.context.getProvider().save(entry.getKey(), entry.getValue().getData()));

        this.updatedResources.entrySet().forEach(entry -> this.context.getProvider().save(entry.getKey(), entry.getValue()));
        final String[] roles = this.acls.toArray(new String[this.acls.size()]);

        try (ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.context)) {
            final String resourceUri = bridge //
                    .request(this.baseUrl + "/v1/Record") //
                    .param("roles", roles) //
                    .param("author", this.getDescription().getAuthor()) //
                    .accept("json") //
                    .dataType("application/zip") //
                    .post(this.context.getBaseProvider().asBytes()) //
                    .asString();

            return PATTERN_URI_RECORD.matcher(resourceUri).replaceFirst("$1");
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to persist record %s : %s", this.context.getRecord().description().getRecordUid(), ex.getMessage()));
        }
    }

    /**
     * Set the source uid to the record wrapper.
     *
     * @param sourceUid
     *     The source identifier
     * @return The record wrapper
     */
    public RecordWrapper setSourceUid(final String sourceUid) {
        this.sourceUid = sourceUid;
        return this;
    }

    /**
     * Gets the source uid.
     *
     * @return the sourceUid
     */
    public String getSourceUid() {
        return this.sourceUid;
    }

    /**
     * Sets the author from record wrapper.
     *
     * @param author
     *     the author identifier
     * @return The record wrapper
     */
    public RecordWrapper setAuthor(final String author) {
        this.getDescription().setAuthor(author);
        return this;
    }

    /**
     * Gets the meta.
     *
     * @return the meta
     */
    public FormSpecificationMeta getMeta() {
        final FormSpecificationMeta meta = this.context.getRecord().meta();
        this.updatedResources.put(Engine.FILENAME_META, meta);
        return meta;
    }

    /**
     * Set metas to the current record.
     *
     * @param bindings
     *     The metadata.
     * @return The record wrapper
     */
    public RecordWrapper meta(final List<Map<String, Object>> bindings) {
        final FormSpecificationMeta meta = this.context.getRecord().meta(bindings);
        this.updatedResources.put(Engine.FILENAME_META, meta);
        return this;
    }

    /**
     * Set metas to the current record.
     *
     * @param bindings
     *     The metadata.
     * @return The record wrapper
     */
    public RecordWrapper role(final List<Map<String, String>> bindings) {
        bindings.forEach(item -> {
            this.acls.add(item.get("entity") + ":" + item.get("role"));
        });

        return this;
    }

    /**
     * Return a step element from identifier
     *
     * @param stepId
     *     the step identifier
     * @return
     */
    public StepWrapper step(final String stepId) {
        final StepElement step = Optional.of(this.context.getRecord().stepsMgr()) //
                .filter(s -> null != s) //
                .map(s -> {
                    return s.find(stepId);
                }) //
                .orElseThrow(() -> new TechnicalException(String.format("Step identifier '%s' not found", stepId)));
        return new StepWrapper(this.context, step);
    }

    /**
     * Upload record to any application based on URI.
     *
     * @param uri
     *     The URI identifier
     * @return
     * @throws Exception
     */
    public ServiceResponse uploadTo(final String uri) throws Exception {
        try (final ScriptProcessorBridge bridge = new ScriptProcessorBridge(this.context)) {
            final String targetUrl = this.context.getConfiguration().get("providers.alias." + bridge.getAlias(uri));
            return bridge.getApplication(uri).uploadTo(targetUrl, this.context.getProvider(), this.acls);
        } catch (IOException | TechnicalException ex) {
            throw ex;
        }
    }

    /**
     * Returns metas value from name
     *
     * @param name
     *     Metadata name
     * @return
     */
    public Set<String> metas(final String name) {
        return Optional.of(this.getMeta().getMetas()) //
                .orElse(new ArrayList<>()) //
                .stream() //
                .filter(m -> m.getName().equals(name)) //
                .map(m -> m.getValue()) //
                .collect(Collectors.toSet()) //
        ;
    }

    /***
     * Return the specification loader.
     *
     * @return
     */
    public SpecificationLoader loader() {
        return Optional.of(this.context.getRecord()).orElse(null);
    }

    /**
     * Back to previous step at index.
     *
     * @param stepIndex
     *     the step index
     * @return
     */
    public RecordWrapper backToStep(final int stepIndex) {
        Optional.of(this.context.getRecord().stepsMgr()) //
                .filter(s -> null != s) //
                .map(SpecificationSteps::findAll) //
                .orElse(new ArrayList<StepElement>()) //
                .forEach(step -> {
                    if (step.getPosition() >= stepIndex) {
                        step.setStatus(StepStatusEnum.TO_DO.getStatus());
                    }
                });

        this.context.getRecord().flush();
        return this;
    }
}
