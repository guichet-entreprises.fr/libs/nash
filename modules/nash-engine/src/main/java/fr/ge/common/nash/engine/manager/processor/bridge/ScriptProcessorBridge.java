/**
 *
 */
package fr.ge.common.nash.engine.manager.processor.bridge;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.script.Bindings;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.MimeTypeUtil;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.loader.SpecificationProcesses;
import fr.ge.common.nash.engine.manager.processor.bridge.script.ApplicationWrapper;
import fr.ge.common.nash.engine.manager.processor.bridge.script.DataWrapper;
import fr.ge.common.nash.engine.manager.processor.bridge.script.MarkovApplicationWrapper;
import fr.ge.common.nash.engine.manager.processor.bridge.script.NashApplicationWrapper;
import fr.ge.common.nash.engine.manager.processor.bridge.script.ServiceWrapper;
import fr.ge.common.nash.engine.manager.processor.bridge.script.StorageApplicationWrapper;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.PatternProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.support.dozer.FormSpecificationMappingBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * The Class ScriptProcessorBridge.
 *
 * @author bsadil
 */
public class ScriptProcessorBridge extends AbstractBridge {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptProcessorBridge.class);

    /** The specification data to persist. */
    private final Map<String, FormSpecificationData> specificationDataToPersist = new HashMap<>();

    private static final Mapper DOZER = DozerBeanMapperBuilder.create() //
            .withMappingBuilder( //
                    new FormSpecificationMappingBuilder() //
                            .setDataAttributeExclusion("value") //
                            .setGroupAttributeExclusion("data") //
            ) //
            .build();

    /** Wrapper to build a zip file. **/
    private final ZipWrapper wrapper;

    /** The APPLICATIONS constant as a Map. */
    private static final Map<String, ApplicationWrapper> APPLICATIONS;

    static {
        final Map<String, ApplicationWrapper> m = new HashMap<>();

        m.put("markov", new MarkovApplicationWrapper());
        m.put("nash", new NashApplicationWrapper());
        m.put("nashBo", new NashApplicationWrapper());
        m.put("storage", new StorageApplicationWrapper());

        APPLICATIONS = Collections.unmodifiableMap(m);
    }

    /** The uri as pattern. **/
    private static final Pattern PATTERN_URI = Pattern.compile("(.*)\\:\\/\\/(.*)", Pattern.CASE_INSENSITIVE);

    /**
     * Instantiates a new processor.
     *
     * @param context
     *            the context
     */
    public ScriptProcessorBridge(final EngineContext<?> context) {
        super(context);
        this.wrapper = new ZipWrapper(this.getContext());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "instance";
    }

    /**
     * Gets the output specification.
     *
     * @return FormSpecificationData
     */
    public FormSpecificationData getOutputSpecification() {

        if (!(this.getElement() instanceof ProcessElement)) {
            throw new TechnicalException("Output specification only available for processes");
        }

        final String resourceName = ((ProcessElement) this.getElement()).getOutput();

        if (StringUtils.isEmpty(resourceName)) {
            throw new TechnicalException("Mandatory output attribute");
        }

        FormSpecificationData spec = this.specificationDataToPersist.get(resourceName);

        if (null == spec) {
            spec = this.getProvider().asBean(resourceName, FormSpecificationData.class);
        }

        if (null == spec) {
            spec = new FormSpecificationData();
        }

        spec.setResourceName(resourceName);
        if (null == spec.getGroups()) {
            spec.setGroups(new ArrayList<>());
        }

        this.specificationDataToPersist.put(resourceName, spec);

        return spec;
    }

    /**
     * Creates the group.
     *
     * @param values
     *            the values
     * @return the group element
     */
    public GroupElement createGroup(final Bindings values) {
        final GroupElement grp = DOZER.map(values, GroupElement.class);

        final Bindings data = (Bindings) values.get("data");
        grp.setData(data.values().stream().map(elm -> this.createData((Bindings) elm)).collect(Collectors.toList()));

        return grp;
    }

    /**
     * Creates the data.
     *
     * @param values
     *            the values
     * @return the data element
     */
    public DataElement createData(final Bindings values) {
        final DataElement data = DOZER.map(values, DataElement.class);
        data.setValue(new ValueElement((String) values.get("value")));

        return data;
    }

    /**
     * Creates the specification.
     *
     * @param resourceName
     *            the resource name
     * @return FormSpecificationData
     */
    public FormSpecificationData createSpecification(final String resourceName) {
        FormSpecificationData spec = this.specificationDataToPersist.get(resourceName);

        if (null == spec) {
            spec = new FormSpecificationData(resourceName);
        }

        this.specificationDataToPersist.put(resourceName, spec);

        return spec;
    }

    /**
     * Gets the specification data to persist.
     *
     * @return the specificationDataToPersist
     */
    public Collection<FormSpecificationData> getSpecificationDataToPersist() {
        return this.specificationDataToPersist.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        this.getSpecificationDataToPersist().forEach(spec -> this.getProvider().save(spec.getResourceName(), spec));
    }

    /**
     * From.
     *
     * @param baseUrl
     *            the base url
     * @return the service wrapper
     */
    public ServiceWrapper from(final String baseUrl) {
        final Matcher matcher = PATTERN_URI.matcher(baseUrl);
        if (!matcher.matches()) {
            return new ServiceWrapper(this.getContext()).from(baseUrl);
        }

        final String application = matcher.group(1);
        if (StringUtils.isEmpty(application) || null == APPLICATIONS.get(application)) {
            throw new TechnicalException(String.format("Unable to find source wrapper from code '%s'", application));
        }

        final String alias = matcher.group(2);
        final String targetUrl = this.getConfiguration().get("providers.alias." + alias);

        try {
            return new ServiceWrapper(this.getContext()).wrap(APPLICATIONS.get(application)).from(targetUrl).to(targetUrl);
        } catch (IllegalArgumentException | SecurityException e) {
            throw new TechnicalException(String.format("Cannot instanciate service wrapper from baseUrl '%s'", baseUrl));
        }
    }

    /**
     * Get the application wrapper from uri.
     * 
     * @param uri
     *            The URI identifier
     * @return
     */
    public ApplicationWrapper getApplication(final String uri) {
        final Matcher matcher = PATTERN_URI.matcher(uri);
        if (!matcher.matches()) {
            throw new TechnicalException(String.format("Unable to match source wrapper from uri '%s'", uri));
        }

        try {
            return APPLICATIONS.get(matcher.group(1));
        } catch (IllegalArgumentException | SecurityException e) {
            throw new TechnicalException(String.format("Cannot instanciate application wrapper from uri '%s'", uri));
        }
    }

    /**
     * Get the alias from URI.
     * 
     * @param uri
     *            the URI identifier
     * @return
     */
    public String getAlias(final String uri) {
        final Matcher matcher = PATTERN_URI.matcher(uri);
        if (!matcher.matches()) {
            return null;
        }
        return matcher.group(2);
    }

    /**
     * Load a resource representing a specification data and identified by its
     * name or a process ID (preceding by a sharp). If process ID is used,
     * context has to concern a process element.
     *
     * @param resourceName
     *            the resource name
     * @return the form specification data
     */
    public FormSpecificationData wrap(final String resourceName) {
        return SpecificationProcesses.load(this.getContext(), resourceName, FormSpecificationData.class);
    }

    /**
     * Load.
     *
     * @param resourcePath
     *            the resource path
     * @return the object
     */
    public Object load(final String resourcePath) {
        final FormSpecificationData data = this.getContext().getProvider().asBean(resourcePath, FormSpecificationData.class);

        final IProvider provider = PatternProvider.from(this.getContext().getProvider(), resourcePath);

        final DataWrapper loaded = new DataWrapper(this.getContext().withProvider(provider).target(data));
        this.getLoadedData().put(resourcePath, loaded);
        return loaded;
    }

    /**
     * Return the zip wrapper.
     *
     * @return the zip wrapper
     */
    public ZipWrapper zip() {
        return this.wrapper;
    }

    /**
     * Add a file to build a zip.
     *
     * @param filename
     * @param resourcePath
     * @return
     */
    public ZipWrapper add(final String filename, final String resourcePath) {
        this.wrapper.add(filename, resourcePath);
        return this.wrapper;
    }

    public Item save(final String zipName) {
        return this.wrapper.save(zipName);
    }

    public static class ZipWrapper {
        private final EngineContext<?> baseContext;

        private final Map<String, byte[]> updatedFiles = new HashMap<>();

        private ZipWrapper(final EngineContext<?> baseContext) {
            this.baseContext = baseContext;
        }

        public ZipWrapper add(final String filename, final String resourcePath) {
            String key = filename;
            if (StringUtils.isNotEmpty(filename) && filename.startsWith("/")) {
                key = filename.substring(1);
            }

            if (!this.updatedFiles.containsKey(key)) {
                final byte[] content = Optional.ofNullable(this.baseContext.getProvider().load(resourcePath)).map(FileEntry::asBytes).orElse(null);
                this.updatedFiles.put(key, content);
            }
            return this;
        }

        public Item save(final String zipName) {
            final byte[] zipContent = ZipUtil.create(this.updatedFiles);
            this.updatedFiles.clear();
            this.baseContext.getProvider().save(zipName, zipContent);
            final String zipAbsolutePath = this.baseContext.getProvider().getAbsolutePath(zipName);
            return FileValueAdapter.createItem(-1, zipName, MimeTypeUtil.type(zipName), () -> zipContent).setAbsolutePath(zipAbsolutePath);
        }
    }

    /**
     * Remove.
     *
     * @param resourcePath
     *            the resource path
     * @return
     */
    public void remove(final String resourcePath) {
        final byte[] resourceAsBytes = this.getContext().getProvider().asBytes(resourcePath);
        if (ArrayUtils.isNotEmpty(resourceAsBytes)) {
            this.getContext().getProvider().remove(resourcePath);
            LOGGER.info("Remove resource from path '{}'", resourcePath);
        }
    }
}
