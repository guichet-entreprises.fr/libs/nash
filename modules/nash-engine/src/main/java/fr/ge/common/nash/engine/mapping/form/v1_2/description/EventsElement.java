/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.description;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * "Events" element.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "events")
@XmlAccessorType(XmlAccessType.NONE)
public class EventsElement implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The elements. */
    @XmlElement(name = "event")
    private List<EventElement> elements;

    /**
     * Gets the elements.
     *
     * @return the elements
     */
    public List<EventElement> getElements() {
        return this.elements;
    }

    /**
     * Sets the elements.
     *
     * @param elements
     *            the new elements
     */
    public void setElements(final List<EventElement> elements) {
        this.elements = Optional.ofNullable(elements).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

}
