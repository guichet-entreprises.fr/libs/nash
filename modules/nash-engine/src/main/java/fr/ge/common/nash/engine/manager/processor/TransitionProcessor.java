/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import fr.ge.common.nash.engine.manager.processor.bridge.IBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.TransitionProcessorBridge;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.TransitionProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;

/**
 * A processor for processes of type "transition".
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class TransitionProcessor extends AbstractDataProcessor<TransitionProcessor> {

    /** The bridge. */
    private IBridge bridge;

    /**
     * {@inheritDoc}
     */
    @Override
    protected TransitionProcessor init() {
        this.bridge = new TransitionProcessorBridge(this.getContext());
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScript() {
        return new String(this.getContext().getScriptProvider().asBytes("script.js"), StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IBridge getBridge() {
        return this.bridge;
    }

    /**
     * Sets the bridge.
     *
     * @param bridge
     *            the new bridge
     */
    public void setBridge(final IBridge bridge) {
        this.bridge = bridge;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TransitionProcessor setContext(final EngineContext<?> context) {
        final IProvider transitionProvider = Optional.ofNullable(context.getElement()) //
                .map(ProcessElement.class::cast) //
                .map(ProcessElement::getRef) //
                .map(ref -> ref.replaceAll("@", "-")) //
                .map(ref -> new TransitionProvider(this.getClass(), ref)) //
                .orElse(null);

        return super.setContext(null == transitionProvider ? context : context.withScriptProvider(transitionProvider));
    }

    @Override
    protected Object execute(final String script, final ScriptExecutionContext ctx) {
        return super.execute(script, this.getContext().updateExpressionContext(ctx));
    }

}
