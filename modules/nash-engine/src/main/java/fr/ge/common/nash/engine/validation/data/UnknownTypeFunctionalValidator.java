/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.validation.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.validation.Error;
import fr.ge.common.nash.engine.validation.Error.Level;
import fr.ge.common.nash.engine.validation.Errors;
import fr.ge.common.nash.engine.validation.commons.IFunctionalValidator;
import fr.ge.common.support.i18n.MessageFormatter;

/**
 * Validate if specified data element type is known.
 *
 * @author Christian Cougourdan
 */
public class UnknownTypeFunctionalValidator implements IFunctionalValidator<FormSpecificationData> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final MessageFormatter messageFormatter, final FormSpecificationData rootElement, final String version) {
        final Errors errors = new Errors();
        for (final Entry<String, String> fieldType : this.searchForUnknownType(rootElement.getGroups(), version).entrySet()) {
            final String field = rootElement.getId() + DOT + fieldType.getKey();
            final String type = fieldType.getValue();
            final Error error = new Error(Level.ERROR, messageFormatter.format("The type '{0}' of the field '{1}' doesn't exist in version '{2}'", type, field, version));
            errors.add(error);
        }
        return errors;
    }

    /**
     * Search for data element which have unknown type.
     *
     * @param elements
     *            the elements
     * @param version
     *            the version
     * @return the errors
     */
    private Map<String, String> searchForUnknownType(final List<? extends IElement<?>> elements, final String version) {
        final Map<String, String> fieldsTypes = new HashMap<>();
        if (elements != null) {
            for (final IElement<?> element : elements) {
                if (element instanceof DataElement) {
                    final DataElement dataElement = (DataElement) element;
                    if (dataElement.getType() != null && ValueAdapterFactory.type(version, dataElement.getType()) == null) {
                        fieldsTypes.put(dataElement.getId(), dataElement.getType());
                    }
                }
                for (final Entry<String, String> fieldType : this.searchForUnknownType(element.getData(), version).entrySet()) {
                    fieldsTypes.put(element.getId() + DOT + fieldType.getKey(), fieldType.getValue());
                }
            }
        }
        return fieldsTypes;
    }
}
