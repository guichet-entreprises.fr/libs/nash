/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.binding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.utils.CoreUtil;

/**
 *
 * @author Christian Cougourdan
 */
public class ObjectDataBinding implements DataBinding {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectDataBinding.class);

    private static final Pattern HEAD = Pattern.compile("^(?:([^.\\[]+)(?:\\[([0-9]*)\\])?)(?:\\.(.+))?");

    private static final int HEAD_GRP_ROOT_KEY = 1;
    private static final int HEAD_GRP_ROOT_IDX = 2;
    private static final int HEAD_GRP_TAIL = 3;

    private final Object source;

    /* cache for source value as array */
    private List<DataBinding> sourceAsList;

    /* cache for source value as map */
    private Map<String, DataBinding> sourceAsMap;

    /* cache for inner values, retrieved by withPath method */
    private final Map<String, DataBinding> subValues = new HashMap<>();

    public ObjectDataBinding(final Object source) {
        this.source = source;
    }

    @Override
    public boolean isArray() {
        return this.source instanceof Collection;
    }

    @Override
    public boolean isMap() {
        return this.source instanceof Map;
    }

    @Override
    public List<DataBinding> asList() {
        if (null == this.sourceAsList && this.isArray()) {
            this.sourceAsList = ((Collection<?>) this.source).stream().map(ObjectDataBinding::new).collect(Collectors.toList());
        }
        return this.sourceAsList;
    }

    @Override
    public Map<String, DataBinding> asMap() {
        if (null == this.sourceAsMap && this.isMap()) {
            final Map<String, DataBinding> dst = new HashMap<>();
            final Map<String, Object> map = CoreUtil.cast(this.source);
            for (final Entry<String, Object> entry : map.entrySet()) {
                final Matcher m = HEAD.matcher(entry.getKey());
                if (m.matches() && null != m.group(HEAD_GRP_ROOT_IDX)) {
                    /*
                     * Try to respect specified position
                     */
                    final String key = m.group(HEAD_GRP_ROOT_KEY);
                    final int idx = Integer.parseInt(m.group(HEAD_GRP_ROOT_IDX));
                    ObjectDataBinding binding = CoreUtil.cast(dst.get(key));
                    if (null == binding) {
                        binding = new ObjectDataBinding(new ArrayList<>());
                        dst.put(key, binding);
                    }
                    ((List<Object>) binding.source).add(entry.getValue());
                } else {
                    dst.put(entry.getKey(), new ObjectDataBinding(entry.getValue()));
                }
            }

            this.sourceAsMap = dst;
        }

        return this.sourceAsMap;
    }

    @Override
    public DataBinding withPath(final String path) {
        if (!this.isMap()) {
            return null;
        }

        if (this.subValues.containsKey(path)) {
            return this.subValues.get(path);
        }

        final Matcher m = HEAD.matcher(path);
        if (m.matches()) {
            final String key = m.group(HEAD_GRP_ROOT_KEY);
            final String tail = m.group(HEAD_GRP_TAIL);

            final Map<String, ? extends DataBinding> map = this.asMap();
            final DataBinding binding = map.get(key);

            this.subValues.put(key, binding);

            if (StringUtils.isNotEmpty(tail)) {
                final DataBinding subBinding = binding.withPath(tail);
                this.subValues.put(path, subBinding);
                return subBinding;
            } else {
                return binding;
            }
        }

        return null;
    }

    @Override
    public String asString() {
        return null == this.source ? null : this.source.toString();
    }

    @Override
    public Integer asInt() {
        if (this.source instanceof Number) {
            return ((Number) this.source).intValue();
        } else if (null != this.source) {
            final String asString = this.source.toString();
            try {
                return Integer.parseInt(asString);
            } catch (final NumberFormatException ex) {
                LOGGER.info("Unable to parse '{}' as integer ({})", asString, ex.getMessage());
            }
        }
        return null;
    }

}
