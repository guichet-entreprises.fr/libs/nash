package fr.ge.common.nash.engine.adapter.v1_2;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.core.function.TriFunction;
import fr.ge.common.nash.engine.adapter.AbstractReferentialValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_2.value.TogglzValue;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.utils.CoreUtil;

/**
 * Widget able to display a simple button. Value represents togglz description,
 * like color and label.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class TogglzValueAdapter extends AbstractReferentialValueAdapter<TogglzValue> {

    /** Converters. */
    private static final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> CONVERTERS;

    static {
        final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> map = new HashMap<>();

        map.put(TogglzValue.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final TogglzValue togglzValue = (TogglzValue) value;

            if (StringUtils.isNotEmpty(togglzValue.getOff())) {
                builder.addTextValue("off", togglzValue.getOff());
            }
            if (StringUtils.isNotEmpty(togglzValue.getOn())) {
                builder.addTextValue("on", togglzValue.getOn());
            }
            if (StringUtils.isNotEmpty(togglzValue.getValue())) {
                builder.addTextValue("value", togglzValue.getValue());
            }

            return builder.build();
        });

        map.put(FormContentData.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final FormContentData data = (FormContentData) value;

            Arrays.stream(new String[] { "off", "on" }) //
                    .filter(key -> StringUtils.isNotEmpty(data.withPath(key).asString())) //
                    .forEach(key -> builder.addTextValue(key, data.withPath(key).asString()));

            if (StringUtils.isNotEmpty(data.withPath("value").asString())) {
                builder.addTextValue("value", data.withPath("value").asBoolean() ? "yes" : "no");
            }

            return builder.build();
        });

        map.put(DataBinding.class, (loader, dataElement, value) -> {

            final DataBinding data = (DataBinding) value;
            final ValueElementBuilder builder = new ValueElementBuilder();
            Arrays.stream(new String[] { "off", "on", "value" }) //
                    .filter(key -> StringUtils.isNotEmpty(data.withPath(key).asString())) //
                    .forEach(key -> builder.addTextValue(key, data.withPath(key).asString()));

            return builder.build();
        });

        map.put(HashMap.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final Map<String, String> data = CoreUtil.cast(value);

            Arrays.stream(new String[] { "off", "on", "value" }) //
                    .filter(key -> data.containsKey(key)) //
                    .forEach(key -> {
                        builder.addTextValue(key, data.get(key));
                    });

            return builder.build();
        });

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    @Override
    public String name() {
        return "Togglz";
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public String template() {
        return TogglzValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    @Override
    protected TogglzValue fromValueElement(final ValueElement value) {
        final ListValueElement lst = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .orElse(null);

        if (null == lst) {
            return null;
        }

        final Map<String, ? extends TextValueElement> map = lst.asMap(TextValueElement.class);

        final TogglzValue togglzValue = new TogglzValue() //
                .setOn(Optional.ofNullable(map.get("on")).map(TextValueElement::getValue).orElse(null)) //
                .setOff(Optional.ofNullable(map.get("off")).map(TextValueElement::getValue).orElse(null)) //
                .setValue(Optional.ofNullable(map.get("value")).map(TextValueElement::getValue).orElse(null)) //
        ;

        if (StringUtils.isEmpty(togglzValue.getOff()) && StringUtils.isEmpty(togglzValue.getOn()) && StringUtils.isEmpty(togglzValue.getValue())) {
            return null;
        } else {
            return togglzValue;
        }
    }

    @Override
    public boolean hasOwnUpdate() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TriFunction<SpecificationLoader, DataElement, Object, ValueElement> findConverter(final SpecificationLoader loader, final DataElement dataElement, final Object src) {
        for (final Map.Entry<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> entry : CONVERTERS.entrySet()) {
            if (entry.getKey().isInstance(src)) {
                return entry.getValue();
            }
        }

        return null;
    }
}
