/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsForElement;
import fr.ge.common.support.i18n.GettextResourceBundle;

/**
 * Record translation manager.
 *
 * @author Christian Cougourdan
 */
public class SpecificationTranslation {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpecificationTranslation.class);

    /** The loader. */
    private final SpecificationLoader loader;

    /** The cache. */
    private final Map<String, ResourceBundle> cache = new HashMap<>();

    /**
     * Instantiates a new specification translation.
     *
     * @param loader
     *            the loader
     */
    SpecificationTranslation(final SpecificationLoader loader) {
        this.loader = loader;
    }

    /**
     * Gets the record i18n bundle.
     *
     * @param lng
     *            the lng
     * @return the bundle
     */
    public ResourceBundle getBundle(final String lng) {
        if (!this.cache.containsKey(lng)) {
            this.cache.put(lng, this.loadBundle(lng));
        }
        return this.cache.get(lng);
    }

    /**
     * Load record i18n bundle.
     *
     * @param lng
     *            the lng
     * @return the resource bundle
     */
    private ResourceBundle loadBundle(final String lng) {
        final TranslationsElement translationElement = Optional.ofNullable(this.loader.description()).map(FormSpecificationDescription::getTranslations).orElse(null);
        if (null == translationElement) {
            LOGGER.debug("No translation informations in record");
            return null;
        }

        final String searchedLang = Optional.ofNullable(translationElement.getDefaultLang()).orElse(lng);
        if (null == searchedLang) {
            LOGGER.debug("No default language found neither context language specified");
            return null;
        }

        final String resourcePath = this.find(searchedLang);
        if (null == resourcePath) {
            LOGGER.debug("No {} language referenced in record", searchedLang);
            return null;
        }

        final byte[] resourceAsBytes = this.loader.getProvider().asBytes(resourcePath);
        if (ArrayUtils.isEmpty(resourceAsBytes)) {
            LOGGER.debug("Unable to load {} language resource from record", resourcePath);
            return null;
        }

        LOGGER.debug("Load {} resource bundle", searchedLang);

        return new GettextResourceBundle(new ByteArrayInputStream(resourceAsBytes));
    }

    /**
     * Gets the alternate language.
     *
     * @param userLanguage
     *            the user language
     * @return the alternate language
     */
    public String getAlternateLanguage(final String userLanguage) {
        final String recordLanguage = this.loader.description().getLang();

        if (null == recordLanguage || recordLanguage.equals(userLanguage)) {
            /*
             * Nothing to do, no language specified in record or same as user language
             */
            return null;
        }

        final String userLanguageResourcePath = this.find(userLanguage);
        if (null != userLanguageResourcePath) {
            /*
             * Found user language i18n resources
             */
            return userLanguage;
        }

        /*
         * Check if there has a default language, different from record and user
         * language and having resources.
         */
        return Optional.ofNullable(this.loader.description()) //
                .map(FormSpecificationDescription::getTranslations)
                /*
                 * default language is defined
                 */
                .map(TranslationsElement::getDefaultLang)
                /*
                 * and is different from record language
                 */
                .filter(lng -> !lng.equals(recordLanguage))
                /*
                 * and is different from user language
                 */
                .filter(lng -> !lng.equals(userLanguage))
                /*
                 * and is referenced and has a resource path
                 */
                .filter(lng -> null != this.find(lng)) //
                .orElse(null);
    }

    /**
     * Find.
     *
     * @param searchedLanguage
     *            the searched language
     * @return the string
     */
    public String find(final String searchedLanguage) {
        final TranslationsElement translationElement = Optional.ofNullable(this.loader.description()).map(FormSpecificationDescription::getTranslations).orElse(null);
        if (null == searchedLanguage) {
            LOGGER.debug("No language to look for");
            return null;
        } else if (null == translationElement || null == translationElement.getForElements()) {
            LOGGER.debug("No translation informations in record");
            return null;
        }

        return translationElement.getForElements().stream() //
                .filter(elm -> searchedLanguage.equalsIgnoreCase(elm.getLang())) //
                .map(TranslationsForElement::getPath) //
                .findFirst() //
                .orElse(null);
    }

}
