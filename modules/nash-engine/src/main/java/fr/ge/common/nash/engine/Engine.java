/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine;

import java.util.Collection;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.EventBus;
import fr.ge.common.nash.engine.loader.event.ILocalUserContextEvent;
import fr.ge.common.nash.engine.loader.event.listener.YieldListener;
import fr.ge.common.nash.engine.provider.IProvider;

/**
 * Engine.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class Engine {

    /** The constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(Engine.class);

    /** The constant FILENAME_DESCRIPTION. */
    public static final String FILENAME_DESCRIPTION = "description.xml";

    /** The constant FILENAME_METAS. */
    public static final String FILENAME_META = "meta.xml";

    /** The constant FILENAME_ROLES. */
    public static final String FILENAME_ROLES = "roles.xml";

    /** The events. */
    private static EventBus eventBus = new EventBus();

    /** The local user context event. */
    private static ILocalUserContextEvent localUserContextEvent;

    /** The configuration. */
    private Configuration configuration;

    @Autowired
    private ApplicationContext applicationContext;

    static {
        setEventListeners(null);
    }

    /**
     * Constructor.
     */
    private Engine() {
        // Nothing to do.
    }

    /**
     * Initialize.
     */
    public void init() {
        LOGGER.info("Loading {}", ValueAdapterFactory.class.getName());
    }

    /**
     * Gets a loader from a provider.
     *
     * @param provider
     *            the provider
     * @return the loader
     */
    public SpecificationLoader loader(final IProvider provider) {
        return this.applicationContext.getBean(SpecificationLoader.class, this.configuration, provider);
    }

    /**
     * Getter on attribute {@link #configuration}.
     *
     * @return configuration
     */
    public Configuration getEngineProperties() {
        return this.configuration;
    }

    /**
     * Sets the engine properties.
     *
     * @param engineProperties
     *            the engine properties
     */
    public void setEngineProperties(final Properties engineProperties) {
        this.configuration = new Configuration(engineProperties);
    }

    /**
     * Getter on attribute {@link #localUserContextEvent}.
     *
     * @return List&lt;ILocalUserContextEvent&gt; localUserContextEvent
     */
    public static ILocalUserContextEvent getLocalUserContextEvents() {
        return localUserContextEvent;
    }

    /**
     * Setter on attribute {@link #localUserContextEvent}.
     *
     * @param localUserContextEvent
     *            the new value of attribute localUserContextEvent
     */
    public static void setLocalUserContextEvents(final ILocalUserContextEvent localUserContextEvent) {
        if (null == localUserContextEvent) {
            Engine.localUserContextEvent = null;
        } else {
            Engine.localUserContextEvent = localUserContextEvent;
        }
    }

    /**
     * Setter on attribute {@link #eventListeners}.
     *
     * @param listeners
     *            the new value of attribute specificationLoaderEvents
     */
    public static void setEventListeners(final Collection<Object> listeners) {
        final EventBus bus = new EventBus();
        if (null != listeners) {
            for (final Object listener : listeners) {
                bus.register(listener);
            }
        }

        /*
         * Mandatory listener
         */
        if (null == listeners || !listeners.stream().anyMatch(YieldListener.class::isInstance)) {
            bus.register(new YieldListener());
        }

        eventBus = bus;
    }

    public static EventBus getEventBus() {
        return eventBus;
    }

}
