/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.support.dozer;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.Bindings;

import org.apache.commons.lang3.StringUtils;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;

/**
 * The Class FormSpecificationMappingBuilder.
 *
 * @author Christian Cougourdan
 */
public class FormSpecificationMappingBuilder extends BeanMappingBuilder {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FormSpecificationMappingBuilder.class);

    /** The data attribute exclusion. */
    private List<String> dataAttributeExclusion = Collections.emptyList();

    /** The group attribute exclusion. */
    private List<String> groupAttributeExclusion = Collections.emptyList();

    /*
     * (non-Javadoc)
     *
     * @see org.dozer.loader.api.BeanMappingBuilder#configure()
     */
    @Override
    protected void configure() {
        this.buildMapping(FormSpecificationData.class, Collections.emptyList());
        this.buildMapping(GroupElement.class, this.groupAttributeExclusion);
        this.buildMapping(DataElement.class, this.dataAttributeExclusion);
    }

    /**
     * Builds the mapping.
     *
     * @param clazz
     *            the clazz
     * @param exclusion
     *            the exclusion
     */
    private void buildMapping(final Class<?> clazz, final Collection<String> exclusion) {
        final Collection<String> enhancedExclusion = new HashSet<>(exclusion);
        enhancedExclusion.add("displayCondition");

        final Pattern pattern = Pattern.compile("(?:get|is)(.+)");
        final TypeMappingBuilder builder = this.mapping(Bindings.class, clazz, TypeMappingOptions.wildcard(false));
        Arrays.stream(clazz.getMethods()) //
                .filter(setter -> {
                    final Matcher m = pattern.matcher(setter.getName());
                    if (!m.matches()) {
                        return false;
                    }
                    Method getter = null;
                    try {
                        getter = clazz.getMethod("set" + m.group(1), setter.getReturnType());
                    } catch (NoSuchMethodException | SecurityException e) {
                        LOGGER.debug("Class [{}] has no method [set{}]", clazz.getSimpleName(), m.group(1));
                    }
                    if (null == getter) {
                        return false;
                    } else {
                        return true;
                    }
                }) //
                .map(Method::getName) //
                .map(pattern::matcher) //
                .filter(Matcher::matches) //
                .map(m -> m.group(1)) //
                .map(StringUtils::uncapitalize) //
                .filter(fieldName -> !enhancedExclusion.contains(fieldName)) //
                .forEach(fieldName -> {
                    LOGGER.trace("Class [{}] : add [{}] field mapping", clazz.getSimpleName(), fieldName);
                    builder.fields(fieldName, fieldName);
                });
    }

    /**
     * Sets the data attribute exclusion.
     *
     * @param dataAttributeExclusion
     *            the dataAttributeExclusion to set
     * @return the form specification mapping builder
     */
    public FormSpecificationMappingBuilder setDataAttributeExclusion(final String... dataAttributeExclusion) {
        this.dataAttributeExclusion = Arrays.asList(dataAttributeExclusion);
        return this;
    }

    /**
     * Sets the group attribute exclusion.
     *
     * @param groupAttributeExclusion
     *            the groupAttributeExclusion to set
     * @return the form specification mapping builder
     */
    public FormSpecificationMappingBuilder setGroupAttributeExclusion(final String... groupAttributeExclusion) {
        this.groupAttributeExclusion = Arrays.asList(groupAttributeExclusion);
        return this;
    }

}
