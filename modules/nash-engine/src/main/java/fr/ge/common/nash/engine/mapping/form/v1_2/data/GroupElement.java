/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import fr.ge.common.nash.core.support.jaxb.BooleanXmlAdapter;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class GroupElement.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "group")
@XmlAccessorType(XmlAccessType.NONE)
public class GroupElement extends AbstractDisplayableElement<GroupElement> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The rule as attr. */
    @XmlAttribute(name = "if")
    private String ruleAsAttr;

    /** The rule as tag. */
    @XmlElement(name = "if")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String ruleAsTag;

    /** The display. */
    @XmlTransient
    private boolean display = true;

    /** The inline. */
    private boolean inline;

    /** The datas. */
    @XmlElements({ //
            @XmlElement(name = "group", type = GroupElement.class), //
            @XmlElement(name = "data", type = DataElement.class) //
    })
    private List<IElement<?>> data;

    /** Display the group as collapsed or expanded. */
    @XmlAttribute(name = "fold")
    @XmlJavaTypeAdapter(BooleanXmlAdapter.class)
    private Boolean fold;

    /** The trigger as tag. */
    @XmlElement(name = "trigger")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String triggerAsTag;

    /** The trigger as attribute. */
    @XmlAttribute(name = "trigger")
    private String triggerAsAttr;

    /**
     * Constructor.
     */
    public GroupElement() {
        // Nothing to do
    }

    /**
     * Constructor.
     *
     * @param id
     *            the id
     */
    public GroupElement(final String id) {
        this.setId(id);
    }

    /**
     * Getter on attribute {@link #data}.
     *
     * @return the data
     */
    @Override
    public List<IElement<?>> getData() {
        return Optional.ofNullable(this.data).orElseGet(ArrayList::new);
    }

    /**
     * Sets the datas.
     *
     * @param data
     *            the data to set
     */
    public void setData(final List<? extends IElement<?>> data) {
        this.data = Optional.ofNullable(data).map(ArrayList<IElement<?>>::new).orElse(new ArrayList<>());
        this.data.forEach(elm -> {
            elm.setPath(this.getPath() + '.' + elm.getId());
            elm.setParentPath(this.getPath());
        });
    }

    /**
     * Gets the rule as attr.
     *
     * @return the ruleAsAttr
     */
    @Override
    public String getRuleAsAttr() {
        return this.ruleAsAttr;
    }

    /**
     * Sets the rule as attr.
     *
     * @param ruleAsAttr
     *            the ruleAsAttr to set
     */
    @Override
    public void setRuleAsAttr(final String ruleAsAttr) {
        this.ruleAsAttr = ruleAsAttr;
    }

    /**
     * Gets the rule as tag.
     *
     * @return the ruleAsTag
     */
    @Override
    public String getRuleAsTag() {
        return this.ruleAsTag;
    }

    /**
     * Sets the rule as tag.
     *
     * @param ruleAsTag
     *            the ruleAsTag to set
     */
    @Override
    public void setRuleAsTag(final String ruleAsTag) {
        this.ruleAsTag = ruleAsTag;
    }

    /**
     * Checks if is display.
     *
     * @return true, if is display
     */
    @Override
    public boolean isDisplay() {
        return this.display;
    }

    /**
     * Sets the display.
     *
     * @param display
     *            the new display
     */
    @Override
    public void setDisplay(final boolean display) {
        this.display = display;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Object> getValue() {
        return new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format( //
                "{ %s, \"data\": %s }", //
                super.toString(), //
                Optional.ofNullable(this.data).map(l -> l.stream().map(Object::toString).collect(Collectors.joining(", "))).map(s -> "[ " + s + " ]").orElse("null") //
        );
    }

    /**
     * Accesseur sur l'attribut {@link #fold}.
     *
     * @return String fold
     */
    public Boolean getFold() {
        return this.fold;
    }

    /**
     * Mutateur sur l'attribut {@link #fold}.
     *
     * @param fold
     *            la nouvelle valeur de l'attribut fold
     */
    public void setFold(final Boolean fold) {
        this.fold = fold;
    }

    /**
     * Checks if is inline.
     *
     * @return true, if is inline
     */
    public boolean isInline() {
        return this.inline;
    }

    /**
     * Sets the inline.
     *
     * @param inline
     *            the new inline
     */
    public void setInline(final boolean inline) {
        this.inline = inline;
    }

    /**
     * Gets the trigger as attr.
     *
     * @return the trigger as attr
     */
    @Override
    public String getTriggerAsAttr() {
        return this.triggerAsAttr;
    }

    /**
     * Gets the trigger as tag.
     *
     * @return the trigger as tag
     */
    @Override
    public String getTriggerAsTag() {
        return this.triggerAsTag;
    }

    /**
     * Sets the trigger as attr.
     *
     * @param triggerAsAttr
     *            the new trigger as attr
     */
    @Override
    public void setTriggerAsAttr(final String triggerAsAttr) {
        this.triggerAsAttr = triggerAsAttr;
    }

    /**
     * Sets the trigger as tag.
     *
     * @param triggerAsTag
     *            the new trigger as tag
     */
    @Override
    public void setTriggerAsTag(final String triggerAsTag) {
        this.triggerAsTag = triggerAsTag;
    }

    /**
     * Gets the trigger.
     *
     * @return the trigger source code.
     */
    @Override
    public String getTrigger() {
        return CoreUtil.coalesce(this::getTriggerAsTag, this::getTriggerAsAttr);
    }

    /**
     * Sets the trigger.
     *
     * @param value
     *            the new trigger
     */
    @Override
    public void setTrigger(final String value) {
        this.setTriggerAsAttr(null);
        this.setTriggerAsTag(value);
    }
}
