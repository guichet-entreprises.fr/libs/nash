/**
 *
 */
package fr.ge.common.nash.engine.manager.processor.bridge;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.script.Bindings;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.loader.SpecificationProcesses;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceRequest;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceResponse;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.ScriptValueConversionService;
import fr.ge.common.nash.engine.util.ElementUtil;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.ResourceUtil;

/**
 * Bridge for hangout proxy.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class HangoutProcessorBridge extends AbstractBridge {

    private static final String PROXY_SERVICE_CALLBACK_URL = "serviceCallbackUrl";

    private static final String PROXY_UI_CALLBACK_URL = "uiCallbackUrl";

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(HangoutProcessorBridge.class);

    /** Proxy base URL. **/
    private static final String STAMPER_PROXY_URL_PUBLIC = "stamper.proxy.url.public";

    /** Proxy create session path. **/
    private static final String STAMPER_PROXY_CREATE_SESSION_URL = "stamper.proxy.create.session.url";

    /** Proxy valide session path. **/
    private static final String STAMPER_PROXY_VALIDATE_SESSION_URL = "stamper.proxy.validate.session.url";

    /** Proxy UI callback URL. **/
    private static final String STAMPER_PROXY_UI_CALLBACK_URL = "stamper.proxy.ui.callback.url";

    /** Proxy Service callback URL. **/
    private static final String STAMPER_PROXY_SERVICE_CALLBACK_URL = "stamper.proxy.service.callback.url";

    /** Stamper Proxy Service callback URL. **/
    private static final String STAMPER_PROXY_RESULT_FILE_NAME = "stamp-calling.xml";

    /** Proxy Service remove proxy calling file. **/
    private static final String PROXY_SERVICE_REMOVE_PROXY_FILE_URL = "proxy.service.remove.proxy.file.url";

    /** Payment Proxy base URL. **/
    private static final String PAYMENT_PROXY_URL_PUBLIC = "payment.proxy.url.public";

    /** Payment Proxy create session path. **/
    private static final String PAYMENT_PROXY_CREATE_SESSION_URL = "payment.proxy.create.session.url";

    /** Payment Proxy valide session path. **/
    private static final String PAYMENT_PROXY_VALIDATE_SESSION_URL = "payment.proxy.validate.session.url";

    /** Payment Proxy UI callback URL. **/
    private static final String PAYMENT_PROXY_UI_CALLBACK_URL = "payment.proxy.ui.callback.url";

    /** Payment Proxy Service callback URL. **/
    private static final String PAYMENT_PROXY_SERVICE_CALLBACK_URL = "payment.proxy.service.callback.url";

    /** Payment Proxy Service callback URL. **/
    private static final String PAYMENT_PROXY_RESULT_FILE_NAME = "payment-calling.xml";

    /** Proxy UI redirect URL. **/
    private static final String PROXY_REDIRECT_UI = "nash.web.public.url";

    private static final String PAYMENT_PROXY_SERVICE_LOCK_RECORD = "payment.proxy.service.lock.record";

    private static final String PAYMENT_PROXY_SERVICE_CREATE_CART = "payment.proxy.service.create.cart";

    /**
     * Constructeur de la classe.
     *
     * @param context
     *            the context
     */
    public HangoutProcessorBridge(final EngineContext<?> context) {
        super(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "hangout";
    }

    public FormSpecificationData pay(final Map<String, Object> params) throws JsonProcessingException {
        LOGGER.debug("Calling pay from hangout bridge");

        final Map<String, Object> paramsOut = this.buildPayParameters(params);

        paramsOut.put("RESULT_FILE_NAME", PAYMENT_PROXY_RESULT_FILE_NAME);
        LOGGER.debug("Parameters : {}", paramsOut);

        final Map<String, String> urls = this.buildUrls( //
                PAYMENT_PROXY_UI_CALLBACK_URL, //
                PAYMENT_PROXY_SERVICE_CALLBACK_URL, //
                PAYMENT_PROXY_RESULT_FILE_NAME //
        );

        paramsOut.putAll(urls);

        final String token = this.retrieveSessionToken(PAYMENT_PROXY_CREATE_SESSION_URL, paramsOut);
        if (null == token) {
            return null;
        }

        // -->Creating the cart
        if (StringUtils.isNotEmpty(this.getConfiguration().get(PAYMENT_PROXY_SERVICE_CREATE_CART))) {
            final ServiceRequest request = new ServiceRequest( //
                    ResourceUtil.buildUrl( //
                            this.getConfiguration().get(PAYMENT_PROXY_SERVICE_CREATE_CART), //
                            this.getConfiguration().asMap(), //
                            token //
                    ) //
            );

            // -->Call create cart service
            final ServiceResponse response = request //
                    .accept("json") //
                    .dataType(MediaType.APPLICATION_OCTET_STREAM) //
                    .post(params.get("cart")) //
            ;

            if (null == response || Status.OK.getStatusCode() != response.getStatus()) {
                LOGGER.debug("Proxy session request error : received HTTP code {}", response.getStatus());
                return null;
            }
        }

        return this.buildProxyParametersFile(PAYMENT_PROXY_URL_PUBLIC, PAYMENT_PROXY_VALIDATE_SESSION_URL, token, PAYMENT_PROXY_RESULT_FILE_NAME);
    }

    private Map<String, Object> buildPayParameters(final Map<String, Object> params) {
        final Map<String, Object> paramsOut = new HashMap<String, Object>();

        int indexPayments = 1;
        for (final Entry<String, Object> param : params.entrySet()) {
            final Object paramValue = param.getValue();

            paramsOut.put("payment." + indexPayments, paramValue);
            LOGGER.debug("Add payment parameter '{}'", paramValue);
            indexPayments++;
        }

        return paramsOut;
    }

    /**
     * Create a new proxy session and redirect.
     *
     * @param params
     *            the params
     * @return the form specification data
     */
    public FormSpecificationData stamp(final Map<String, Object> params) {
        LOGGER.debug("Calling stamp from hangout bridge");

        // The proxy parameters has the following structure
        // { <== Map
        // ..'civility' : 'Mr/Ms/...',
        // ..'files' : [ <== List
        // ....{ <== Map
        // ......'doc': 'content as bytes' <== converted from FileVA#Item
        // ......'zoneId': 'signature'
        // ....}
        // ..]
        // }

        // -->Check if proxy-calling.xml already exist
        final FormSpecificationData spec = this.checkProxyCallingData();
        if (null != spec) {
            return spec;
        }
        // <--

        final Map<String, Object> paramsOut = this.buildStampParameters(params);

        paramsOut.put("RESULT_FILE_NAME", STAMPER_PROXY_RESULT_FILE_NAME);

        final Map<String, String> urls = this.buildUrls( //
                STAMPER_PROXY_UI_CALLBACK_URL, //
                STAMPER_PROXY_SERVICE_CALLBACK_URL, //
                STAMPER_PROXY_RESULT_FILE_NAME //
        );

        paramsOut.putAll(urls);

        final String token = this.retrieveSessionToken(STAMPER_PROXY_CREATE_SESSION_URL, paramsOut);
        if (null == token) {
            return null;
        }

        return this.buildProxyParametersFile(STAMPER_PROXY_URL_PUBLIC, STAMPER_PROXY_VALIDATE_SESSION_URL, token, STAMPER_PROXY_RESULT_FILE_NAME);
    }

    private FormSpecificationData checkProxyCallingData() {
        final StepElement stepElement = this.getParentElement(StepElement.class);
        if (null == stepElement || StringUtils.isBlank(stepElement.getId())) {
            throw new TechnicalException("Cannot get step element from engine context");
        }

        if (!(this.getElement() instanceof ProcessElement)) {
            throw new TechnicalException("Cannot get process element from engine context");
        }

        final ProcessElement processElement = (ProcessElement) this.getElement();
        final FormSpecificationData spec = SpecificationProcesses.load(this.getContext(), processElement.getOutput(), FormSpecificationData.class);

        if (null == spec) {
            LOGGER.info("The proxy parameters file not found");
            return null;
        }

        // -->Check existing mandatory proxy parameters elements
        try {
            ElementUtil.assertDataElement("The token property not found", spec, "params.token");
            ElementUtil.assertDataElement("The URL redirection property not found", spec, "params.urlRedirection");

            final DataElement dataElement = ElementUtil.assertDataElement("The URL validation not found", spec, "params.urlValidation");
            if (null != dataElement.getValue() && null != dataElement.getValue().getContent()) {
                final String value = dataElement.getValue().getContent().toString();
                final ServiceResponse response = new ServiceRequest(value).get();

                if (Status.OK.getStatusCode() == response.getStatus() && BooleanUtils.toBoolean(response.asString())) {
                    return spec;
                } else {
                    this.getContext().getProvider().remove(processElement.getOutput());
                }
            }
        } catch (final AssertionError error) {
            LOGGER.info(error.getMessage());
        }

        return null;
    }

    private Map<String, Object> buildStampParameters(final Map<String, Object> params) {
        final Map<String, Object> paramsOut = new HashMap<String, Object>();
        for (final Entry<String, Object> param : params.entrySet()) {
            final Object paramValue = param.getValue();
            if (paramValue instanceof String) {
                paramsOut.put(param.getKey(), paramValue);
            } else if (paramValue instanceof Bindings && ScriptValueConversionService.isArray(paramValue)) {
                final List<Object> files = CoreUtil.cast(((Bindings) paramValue).values());
                int indexFiles = 1;
                for (final Object file : files.stream().filter(Map.class::isInstance).collect(Collectors.toList())) {
                    final Map<String, Object> fileProperties = CoreUtil.cast(file);
                    for (final Entry<String, Object> fileProperty : fileProperties.entrySet()) {
                        if (fileProperty.getValue() instanceof String) {
                            paramsOut.put("files." + indexFiles + "." + fileProperty.getKey(), fileProperty.getValue());
                        } else if (fileProperty.getValue() instanceof Item) {
                            final Item fileItem = (Item) fileProperty.getValue();
                            final byte[] docContent = fileItem.getContent();
                            paramsOut.put("files." + indexFiles + ".doc", docContent);
                        }
                    }
                    indexFiles++;
                }
                paramsOut.put("NB_FILES", indexFiles);
            } else {
                paramsOut.put(param.getKey(), paramValue);
            }
        }
        return paramsOut;
    }

    /**
     * Build UI and service callback URL.
     *
     * @param uiCallbackUrlPropertyKey
     *            configuration property key for UI callback URL pattern
     * @param serviceCallbackUrlPropertyKey
     *            configuration property key for service callback URL pattern
     * @param proxyResultFilename
     *            result filename
     * @return
     */
    protected Map<String, String> buildUrls(final String uiCallbackUrlPropertyKey, final String serviceCallbackUrlPropertyKey, final String proxyResultFilename) {
        final Map<String, String> urls = new HashMap<>();

        final Map<?, ?> configAsMap = this.getConfiguration().asMap();

        final String code = this.getContext().getRecord().description().getRecordUid();
        final StepElement stepElement = this.getParentElement(StepElement.class);
        if (null == stepElement || StringUtils.isBlank(stepElement.getId())) {
            throw new TechnicalException("Cannot get step element from engine context");
        }

        if (!(this.getElement() instanceof ProcessElement)) {
            throw new TechnicalException("Cannot get process element from engine context");
        }

        final ProcessElement processElement = (ProcessElement) this.getElement();
        final FormSpecificationProcess processes = this.getParentElement(FormSpecificationProcess.class);

        urls.put(PROXY_UI_CALLBACK_URL, //
                ResourceUtil.buildUrl( //
                        this.getConfiguration().get(uiCallbackUrlPropertyKey), //
                        configAsMap, //
                        code, //
                        stepElement.getId(), //
                        processes.getType(), //
                        processElement.getId() //
                ) //
        );

        LOGGER.debug("Build ui callback url with '{}'", urls.get(PROXY_UI_CALLBACK_URL));

        urls.put(PROXY_SERVICE_CALLBACK_URL, //
                ResourceUtil.buildUrl( //
                        this.getConfiguration().get(serviceCallbackUrlPropertyKey), //
                        configAsMap, //
                        code, //
                        stepElement.getId(), //
                        processes.getType(), //
                        processElement.getId(), //
                        proxyResultFilename //
                ) //
        );

        LOGGER.debug("Build service callback url with '{}'", urls.get(PROXY_SERVICE_CALLBACK_URL));

        final int stepPosition = this.getRecord().currentStepPosition() - 1;
        urls.put("URL_REDIRECT_PREVIOUS_STEP_UI", //
                String.join("/", this.getConfiguration().get(PROXY_REDIRECT_UI), "record", this.getRecord().description().getRecordUid(), String.valueOf(stepPosition), "page", "0"));

        urls.put("PROXY_SERVICE_LOCK_RECORD", ResourceUtil.buildUrl( //
                this.getConfiguration().get(PAYMENT_PROXY_SERVICE_LOCK_RECORD), //
                this.getConfiguration().asMap(), //
                this.getRecord().description().getRecordUid() //
        ));

        urls.put("PROXY_SERVICE_REMOVE_PROXY_FILE_URL", ResourceUtil.buildUrl( //
                this.getConfiguration().get(PROXY_SERVICE_REMOVE_PROXY_FILE_URL), //
                this.getConfiguration().asMap(), //
                this.getRecord().description().getRecordUid(), //
                StringUtils.substringBefore(stepElement.getData(), "/") //
        ));

        urls.put("URL_REDIRECT_CURRENT_STEP_UI", //
                String.join("/", this.getConfiguration().get(PROXY_REDIRECT_UI), "record", this.getRecord().description().getRecordUid(), String.valueOf(this.getRecord().currentStepPosition()),
                        "page", "0"));

        return urls;
    }

    protected String retrieveSessionToken(final String sessionRequestUrlProperyKey, final Map<String, Object> parameters) {
        final String sessionRequestUrl = ResourceUtil.buildUrl( //
                this.getConfiguration().get(sessionRequestUrlProperyKey), //
                this.getConfiguration().asMap(), //
                parameters //
        );

        LOGGER.debug("Build proxy session create url '{}'", sessionRequestUrl);
        final ServiceRequest request = new ServiceRequest(sessionRequestUrl);

        // -->Call proxy service to create a session
        final ServiceResponse response = request.dataType("form") //
                .param(PROXY_UI_CALLBACK_URL, parameters.get(PROXY_UI_CALLBACK_URL)) //
                .param(PROXY_SERVICE_CALLBACK_URL, parameters.get(PROXY_SERVICE_CALLBACK_URL)) //
                .post(parameters);

        if (null == response || Status.OK.getStatusCode() != response.getStatus()) {
            LOGGER.debug("Proxy session request error : received HTTP code {}", response.getStatus());
            return null;
        }

        return response.asString();
    }

    protected FormSpecificationData buildProxyParametersFile(final String publicUrlPropertyKey, final String validationUrlPropertyKey, final String token, final String resultFilename) {
        final String redirectionUrl = this.getConfiguration().get(publicUrlPropertyKey) + "?token=" + token;
        final String validationUrl = ResourceUtil.buildUrl( //
                this.getConfiguration().get(validationUrlPropertyKey), //
                this.getConfiguration().asMap(), //
                token //
        );

        final GroupElement groupElement = new GroupElement("params");
        groupElement.setData( //
                Arrays.asList( //
                        new DataElement("urlRedirection", "String", new ValueElement(redirectionUrl)), //
                        new DataElement("urlValidation", "String", new ValueElement(validationUrl)), //
                        new DataElement("token", "String", new ValueElement(token)), //
                        new DataElement("resultFileName", "String", new ValueElement(resultFilename)) //
                ) //
        );

        final FormSpecificationData formElement = new FormSpecificationData();
        formElement.setId("proxyCalling");
        formElement.setGroups(Arrays.asList(groupElement));

        return formElement;
    }

}
