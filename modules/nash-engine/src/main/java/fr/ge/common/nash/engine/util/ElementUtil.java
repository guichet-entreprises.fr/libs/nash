package fr.ge.common.nash.engine.util;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;

/**
 * Specification element utility methods.
 *
 * @author Christian Cougourdan
 */
public final class ElementUtil {

    /**
     * Find {@link DataElement} in model specification by its absolute path.
     *
     * @param msg
     *            error message if no element nor {@link DataElement}
     * @param spec
     *            model specification to explore
     * @param path
     *            absolute path of searched element
     * @return element found
     */
    public static DataElement assertDataElement(final String msg, final FormSpecificationData spec, final String path) {
        final IElement<?> elm = SpecificationLoader.find(spec, path);
        if (!(elm instanceof DataElement)) {
            throw new AssertionError(msg);
        } else {
            return (DataElement) elm;
        }
    }

}
