/**
 *
 */
package fr.ge.common.nash.engine.manager.processor.bridge;

import java.util.HashMap;
import java.util.Optional;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceRequest;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceResponse;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.ResourceUtil;

/**
 * Bridge using TRACKER functions.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class TrackerProcessorBridge extends AbstractBridge {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TrackerProcessorBridge.class);

    /**
     * Instantiates a new processor.
     *
     * @param context
     *            the context
     */
    public TrackerProcessorBridge(final EngineContext<?> context) {
        super(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "tracker";
    }

    /**
     * Post a message.
     *
     * @param content
     *            the content to post
     */
    public void post(final String content) {
        final FormSpecificationDescription description = this.getContext().getRecord().description();
        this.postWithReference(description.getRecordUid(), content);
    }

    /**
     * Add a link into Tracker between current record and a reference.
     *
     * @param reference
     *            the reference identifier
     */
    public void link(final String reference) {
        LOGGER.info("Create a link with reference {}", reference);
        final FormSpecificationDescription description = this.getContext().getRecord().description();
        final String uid = description.getRecordUid();
        final String author = Optional.ofNullable(this.getConfiguration().get("tracker.author")).orElse(description.getAuthor());
        if (Status.OK.getStatusCode() == Optional.ofNullable(this.getConfiguration().get("tracker.baseUrl")) //
                .filter(StringUtils::isNotEmpty) //
                .map(tracker -> ResourceUtil.buildUrl(tracker + "/v1/uid/{uid}/ref/{ref}", new HashMap<>(), uid, reference)) //
                .map(ServiceRequest::new) //
                .map(s -> s.dataType("form")) //
                .map(s -> s.param("author", author)) //
                .map(s -> s.continueOnError(true)) //
                .map(s -> s.post(null)) //
                .map(ServiceResponse::getStatus) //
                .orElse(Status.INTERNAL_SERVER_ERROR.getStatusCode())) {
            LOGGER.debug("Link message successfully");
        } else {
            LOGGER.warn("Link message unsuccessfully");
        }
    }

    /**
     * Post a message with a reference.
     *
     * @param reference
     *            The reference to link a record
     * @param content
     *            The message content to send
     */
    public void post(final String reference, final String content) {
        this.link(reference);
        this.postWithReference(reference, content);
    }

    private void postWithReference(final String reference, final String content) {
        LOGGER.info("Post a message a link with content '{}'", content);
        final FormSpecificationDescription description = this.getContext().getRecord().description();
        final String author = Optional.ofNullable(this.getConfiguration().get("tracker.author")).orElse(description.getAuthor());
        if (Status.OK.getStatusCode() == Optional.ofNullable(this.getConfiguration().get("tracker.baseUrl")) //
                .filter(StringUtils::isNotEmpty) //
                .map(tracker -> ResourceUtil.buildUrl(tracker + "/v1/uid/{ref}/msg", new HashMap<>(), reference)) //
                .map(ServiceRequest::new) //
                .map(s -> s.param("author", author)) //
                .map(s -> s.param("content", content)) //
                .map(s -> s.continueOnError(true)) //
                .map(s -> s.post(null)) //
                .map(ServiceResponse::getStatus) //
                .orElse(Status.INTERNAL_SERVER_ERROR.getStatusCode())) {
            LOGGER.debug("Message posted successfully");
        } else {
            LOGGER.warn("Message posted unsuccessfully");
        }
    }

}
