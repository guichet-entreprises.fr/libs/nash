package fr.ge.common.nash.engine.loader.event.listener;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.RecordStepStatusChangeEvent;
import fr.ge.common.nash.engine.loader.event.Subscribe;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsForElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.YieldElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.store.IStore;
import fr.ge.common.utils.ZipUtil;

/**
 * Listener linked to record step status change. When status go from
 * TO-DO/IN-PROGRESS to DONE/SEALED, it looks for {@link YieldElement} and
 * execute them.
 *
 * @author Christian Cougourdan
 */
public class YieldListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(YieldListener.class);

    private static final Mapper DOZER = DozerBeanMapperBuilder.create() //
            .withMappingBuilder(new BeanMappingBuilder() {
                @Override
                protected void configure() {
                    this.mapping(FormSpecificationDescription.class, FormSpecificationDescription.class) //
                            .exclude("recordUid") //
                            .exclude("flow") //
                            .exclude("steps") //
                            .exclude("referentials") //
                            .exclude("history");
                }
            }) //
            .build();

    private Map<String, IStore> stores;

    @Subscribe
    public void onRecordStatusChange(final RecordStepStatusChangeEvent event) {
        if (event.getOldStatus().isClosed() || !event.getNewStatus().isClosed()) {
            return;
        }

        LOGGER.debug("Trigger yield listener on step {} : {} -> {}", event.getStep().getId(), event.getOldStatus(), event.getNewStatus());

        final SpecificationLoader srcLoader = event.getLoader();
        final Collection<YieldElement> yields = srcLoader.stepsMgr().findFollowingYield(event.getStep());

        final FormSpecificationDescription src = srcLoader.description();

        final Set<StepElement> stepsToRemove = new HashSet<>();
        final Set<String> filesToRemove = new HashSet<>();

        final Map<String, Collection<String>> filesByStepId = new HashMap<>();

        for (final YieldElement yield : yields) {
            final IStore store = null == this.stores ? null : Optional.ofNullable(yield.getTo()).map(this.stores::get).orElse(null);
            if (null == store) {
                LOGGER.info("No [{}] store configured", yield.getTo());
                continue;
            }

            final Set<String> stepsToEmbed = this.parseStepIds(srcLoader, yield.getCopySteps());
            final Set<String> stepsToExtract = this.parseStepIds(srcLoader, yield.getExtractSteps());

            stepsToEmbed.addAll(stepsToExtract);

            final Set<String> filesToEmbed = this.parseFilePaths(srcLoader, yield.getCopyFiles());
            final Set<String> filesToExtract = this.parseFilePaths(srcLoader, yield.getExtractFiles());

            filesToEmbed.addAll(filesToExtract);
            filesToRemove.addAll(filesToExtract);

            /*
             * Retrieve step to embed in the right order
             */
            final List<StepElement> steps = src.getSteps().getElements().stream() //
                    .filter(step -> stepsToEmbed.contains(step.getId())) //
                    .collect(Collectors.toList());

            /*
             * Store step to extract in global remove list
             */
            steps.stream() //
                    .filter(step -> stepsToExtract.contains(step.getId())) //
                    .forEach(stepsToRemove::add);

            /*
             * Store step's resources by step ID
             */
            steps.stream() //
                    .filter(step -> !filesByStepId.containsKey(step.getId())) //
                    .forEach(step -> filesByStepId.put(step.getId(), srcLoader.stepsMgr().findResources(step)));

            stepsToExtract.stream().map(filesByStepId::get).forEach(filesToRemove::addAll);
            stepsToEmbed.stream().map(filesByStepId::get).forEach(filesToEmbed::addAll);

            final IProvider provider = this.build(srcLoader, steps, filesToEmbed);
            final SpecificationLoader dstLoader = srcLoader.getEngine().loader(provider);
            dstLoader.description().setFlow(steps);
            dstLoader.flush();

            store.upload(provider, null);
        }

        src.setFlow( //
                src.getFlow().stream() //
                        .filter(elm -> !stepsToRemove.contains(elm)) //
                        .filter(elm -> !yields.contains(elm)) //
                        .collect(Collectors.toList()) //
        );

        filesToRemove.forEach(srcLoader.getProvider()::remove);
        if (!stepsToRemove.isEmpty()) {
            final IProvider srcProvider = srcLoader.getProvider();
            srcProvider.resources("__cache__/**/tpl-*.html").forEach(elm -> srcProvider.remove(elm));
        }

        srcLoader.flush();
    }

    private IProvider build(final SpecificationLoader srcLoader, final List<StepElement> stepsToEmbed, final Set<String> filesToEmbed) {
        final IProvider provider = this.createProvider(srcLoader);
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);

        desc.setSteps(new StepsElement(stepsToEmbed));

        for (final String path : filesToEmbed) {
            this.transferFile(path, srcLoader.getProvider(), provider);
        }

        return provider;
    }

    private IProvider createProvider(final SpecificationLoader srcLoader) {
        final FormSpecificationDescription srcDesc = srcLoader.description();
        final FormSpecificationDescription newDesc = DOZER.map(srcDesc, FormSpecificationDescription.class);
        newDesc.setOrigin(srcDesc.getRecordUid());

        final byte[] descAsBytes = JaxbFactoryImpl.instance().asByteArray(newDesc);
        final IProvider provider = new ZipProvider(ZipUtil.create(Collections.singletonMap(Engine.FILENAME_DESCRIPTION, descAsBytes)));

        for (final TranslationsForElement lng : Optional.ofNullable(newDesc.getTranslations()).map(TranslationsElement::getForElements).orElse(Collections.emptyList())) {
            this.transferFile(lng.getPath(), srcLoader.getProvider(), provider);
        }

        return provider;
    }

    private void transferFile(final String path, final IProvider src, final IProvider dst) {
        final byte[] bytes = src.asBytes(path);
        dst.save(path, bytes);
    }

    /**
     * Compute resource path filters and return existing resource path.
     *
     * @param src
     *            base specification loader
     * @param str
     *            resource path filters
     * @return resource paths list
     */
    private Set<String> parseFilePaths(final SpecificationLoader src, final String str) {
        final Set<String> lst = new HashSet<>();

        if (StringUtils.isEmpty(str)) {
            return lst;
        }

        /*
         * TODO use ANT patterns ...
         */
        return new HashSet<>(src.getProvider().resources(str));
    }

    private Set<String> parseStepIds(final SpecificationLoader src, final String str) {
        final FormSpecificationDescription desc = src.description();
        final Set<String> lst = new HashSet<>();

        if (StringUtils.isEmpty(str)) {
            return lst;
        }

        for (final String elm : str.split(",")) {
            final String[] ids = elm.split("->");
            if (1 == ids.length) {
                lst.add(ids[0].trim().replaceFirst("^#", ""));
            } else {
                lst.addAll(this.parseStepsRange(desc, ids[0].trim().replaceFirst("^#", ""), ids[1].trim().replaceFirst("^#", "")));
            }
        }

        return lst;
    }

    private Collection<String> parseStepsRange(final FormSpecificationDescription desc, final String from, final String to) {
        final List<StepElement> steps = desc.getSteps().getElements();
        int idxFrom = "*".equals(from) ? 0 : this.indexOf(steps, step -> from.equals(step.getId()));
        int idxTo = "*".equals(from) ? steps.size() - 1 : this.indexOf(steps, step -> to.equals(step.getId()));

        if (idxFrom < 0) {
            /*
             * bound specified but not found
             */
            idxFrom = 0;
        }

        if (idxTo < 0) {
            /*
             * bound specified but not found
             */
            idxTo = steps.size() - 1;
        }

        /*
         * Invert found index if necessary
         */
        if (idxFrom > idxTo) {
            final int idx = idxFrom;
            idxFrom = idxTo;
            idxTo = idx;
        }

        return steps.subList(idxFrom, idxTo + 1).stream().map(StepElement::getId).collect(Collectors.toList());
    }

    private <R> int indexOf(final Collection<R> lst, final Predicate<R> predicate) {
        final Iterator<R> iterator = lst.iterator();
        for (int idx = 0; iterator.hasNext(); idx++) {
            if (predicate.test(iterator.next())) {
                return idx;
            }
        }
        return -1;
    }

    /**
     * @param stores
     *            the stores to set
     */
    public void setStores(final Map<String, IStore> stores) {
        this.stores = stores;
    }

}
