/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import fr.ge.common.nash.engine.adapter.AbstractSingleReferentialValueAdapter;
import fr.ge.common.nash.engine.adapter.Option;

/**
 * The Remote content value adapter.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RemoteContentValueAdapter extends AbstractSingleReferentialValueAdapter {

    /** The display. */
    @Option(description = "Display content by default")
    private boolean display;

    /** The src. */
    @Option(description = "Remote content URL")
    private String src;

    @Option(description = "Label displayed before content")
    private String more;

    @Option(description = "Label displayed after content")
    private String less;

    /** The method. */
    @Option(description = "Method type")
    private String method;

    /** The Header Authorization. */
    @Option(description = "Header Authorization")
    private String basicAuthorization;

    /** The data type. */
    @Option(description = "The data type expected of the server response")
    private String dataType;

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {
        return "RemoteContent";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This data type allows to display read only content from external url.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return RemoteContentValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * Accesseur sur l'attribut {@link #display}.
     *
     * @return boolean display
     */
    public boolean isDisplay() {
        return this.display;
    }

    /**
     * Mutateur sur l'attribut {@link #display}.
     *
     * @param display
     *            la nouvelle valeur de l'attribut display
     */
    public void setDisplay(final boolean display) {
        this.display = display;
    }

    /**
     * Gets the src.
     *
     * @return the src
     */
    public String getSrc() {
        return this.src;
    }

    /**
     * Sets the src.
     *
     * @param src
     *            the src to set
     */
    public void setSrc(final String src) {
        this.src = src;
    }

    /**
     * @return the more
     */
    public String getMore() {
        return more;
    }

    /**
     * @param more
     *            the more to set
     */
    public void setMore(String more) {
        this.more = more;
    }

    /**
     * @return the less
     */
    public String getLess() {
        return less;
    }

    /**
     * @param less
     *            the less to set
     */
    public void setLess(String less) {
        this.less = less;
    }

    /**
     * Accesseur sur l'attribut {@link #method}.
     *
     * @return String method
     */
    public String getMethod() {
        return method;
    }

    /**
     * Mutateur sur l'attribut {@link #method}.
     *
     * @param method
     *            la nouvelle valeur de l'attribut method
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Accesseur sur l'attribut {@link #basicAuthorization}.
     *
     * @return String basicAuthorization
     */
    public String getBasicAuthorization() {
        return basicAuthorization;
    }

    /**
     * Mutateur sur l'attribut {@link #basicAuthorization}.
     *
     * @param basicAuthorization
     *            la nouvelle valeur de l'attribut basicAuthorization
     */
    public void setBasicAuthorization(String basicAuthorization) {
        this.basicAuthorization = basicAuthorization;
    }

    /**
     * Accesseur sur l'attribut {@link #dataType}.
     *
     * @return String dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Mutateur sur l'attribut {@link #dataType}.
     *
     * @param dataType
     *            la nouvelle valeur de l'attribut dataType
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}
