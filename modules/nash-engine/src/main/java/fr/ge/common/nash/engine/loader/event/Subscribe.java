/**
 *
 */
package fr.ge.common.nash.engine.loader.event;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Mark a method as listening specific events.
 *
 * @author Christian Cougourdan
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface Subscribe {

}
