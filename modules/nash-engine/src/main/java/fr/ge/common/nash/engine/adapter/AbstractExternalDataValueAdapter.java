/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.engine.adapter.v1_2.value.ExternalData;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.utils.CoreUtil;

/**
 * An external data value adapter.
 *
 * @author jpauchet
 */
public abstract class AbstractExternalDataValueAdapter extends AbstractValueAdapter<ExternalData> {

    /** Converters. */
    private static final Map<Class<?>, Function<Object, ValueElement>> CONVERTERS;

    static {
        final Map<Class<?>, Function<Object, ValueElement>> map = new HashMap<>();

        map.put(ExternalData.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();
            final ExternalData externalData = (ExternalData) value;
            if (!StringUtils.isEmpty(externalData.getVal())) {
                builder.addTextValue("val", externalData.getVal());
            }
            if (!StringUtils.isEmpty(externalData.getData())) {
                builder.addTextValue("data", externalData.getData());
            }
            return builder.build();
        });

        map.put(FormContentData.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();
            final FormContentData data = (FormContentData) value;
            Arrays.stream(new String[] { "val", "data" }) //
                    .filter(key -> StringUtils.isNotEmpty(data.withPath(key).asString())) //
                    .forEach(key -> builder.addTextValue(key, data.withPath(key).asString()));

            return builder.build();
        });

        map.put(DataBinding.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();
            final DataBinding data = (DataBinding) value;
            Arrays.stream(new String[] { "val", "data" }) //
                    .filter(key -> StringUtils.isNotEmpty(data.withPath(key).asString())) //
                    .forEach(key -> builder.addTextValue(key, data.withPath(key).asString()));

            return builder.build();
        });

        map.put(HashMap.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();
            final Map<String, String> data = CoreUtil.cast(value);
            Arrays.stream(new String[] { "val", "data" }) //
                    .filter(key -> data.containsKey(key)) //
                    .forEach(key -> builder.addTextValue(key, data.get(key)));

            return builder.build();
        });

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalData fromValueElement(final ValueElement value) {
        final ListValueElement lst = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .orElse(null);

        if (null == lst) {
            return null;
        }

        final Map<String, ? extends TextValueElement> map = lst.asMap(TextValueElement.class);

        final ExternalData externalData = new ExternalData( //
                Optional.ofNullable(map.get("val")).map(TextValueElement::getValue).orElse(null), //
                Optional.ofNullable(map.get("data")).map(TextValueElement::getValue).orElse(null) //
        );

        if (StringUtils.isEmpty(externalData.getVal()) && StringUtils.isEmpty(externalData.getData())) {
            return null;
        } else {
            return externalData;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        final Function<Object, ValueElement> converter = this.findConverter(value);
        if (null == converter) {
            return null;
        } else {
            return converter.apply(value);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        if (!(value instanceof ExternalData)) {
            return null;
        }
        final ExternalData externalData = (ExternalData) value;
        return externalData.toString();
    }

    /**
     * Finds a converter.
     *
     * @param src
     *            src
     * @return function
     */
    private Function<Object, ValueElement> findConverter(final Object src) {
        for (final Map.Entry<Class<?>, Function<Object, ValueElement>> entry : CONVERTERS.entrySet()) {
            if (entry.getKey().isInstance(src)) {
                return entry.getValue();
            }
        }
        return null;
    }

}
