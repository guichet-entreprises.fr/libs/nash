/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.expression.ScriptExpression;
import fr.ge.common.utils.CoreUtil;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

/**
 *
 * @author Christian Cougourdan
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@SuppressWarnings("restriction")
public class ScriptExpressionExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptExpressionExecutor.class);

    private static final String FMT_ERROR_SHORT = "<eval>:%d:%d ";

    private static final String FMT_ERROR_LONG = "at line number %d at column number %d";

    private final ExecutorService service = Executors.newSingleThreadExecutor();

    private final ScriptEngine engine = CoreUtil.time("Instanciate script engine", () -> new NashornScriptEngineFactory().getScriptEngine((className) -> className.startsWith("java.util.")));

    private int timeout = 900;

    /**
     * Evaluate script expression provided as {@link String}.
     *
     * @param expr
     *            script expression
     * @param context
     *            evaluation context
     * @return script evaluation result
     * @throws ScriptException
     */
    public Object eval(final ScriptExpression expr, final ScriptExecutionContext context) {
        return this.eval(expr.getExpression(), context);
    }

    /**
     * Evaluate script expression provided as {@link String}.
     *
     * @param expr
     *            script expression
     * @param context
     *            evaluation context
     * @return script evaluation result
     * @throws ScriptException
     */
    public Object eval(final String expr, final ScriptExecutionContext context) {
        final Future<Object> future = this.evalAsync(expr, context);

        try {
            return future.get(this.timeout, TimeUnit.SECONDS);
            // return this.expressionAsCallable(expr, context).call();
        } catch (final InterruptedException ex) {
            throw new TechnicalException("Script execution has been interrupted", ex);
        } catch (final TimeoutException ex) {
            throw new TechnicalException("Script execution has timed out", ex);
        } catch (final Exception ex) {
            final Throwable cause = ex.getCause();
            // final int lineNumber = this.getExceptionLineNumber(cause);
            if (null == cause) {
                LOGGER.warn("Script evaluation : {}\n{}", ex.getMessage(), expr, ex.getStackTrace());
                throw new TechnicalException("Unknown script execution error", ex.getCause());
            } else {
                final StringBuffer builder = new StringBuffer();
                final String[] splittedExpr = expr.split("[\n\r]+");
                final int padding = Integer.toString(splittedExpr.length).length();
                for (int idx = 0; idx < splittedExpr.length; idx++) {
                    builder.append(String.format("%" + padding + "d: ", idx + 1)).append(splittedExpr[idx]).append('\n');
                }

                final ScriptException scriptCause = CoreUtil.findException(cause, ScriptException.class);
                if (null == scriptCause) {
                    final int lineNumber = this.getExceptionLineNumber(cause);
                    LOGGER.warn( //
                            "Script evaluation at line {} : {}\n{}\n", //
                            lineNumber, //
                            cause.getMessage(), //
                            builder.toString(), //
                            cause //
                    );
                    throw new ExpressionException(cause.getMessage(), expr, lineNumber, -1);
                } else {
                    LOGGER.warn( //
                            "Script evaluation at line {} at column {} : {}\n{}\n", //
                            scriptCause.getLineNumber(), //
                            scriptCause.getColumnNumber(), //
                            scriptCause.getMessage(), //
                            builder.toString(), //
                            cause //
                    );
                    throw new ExpressionException(scriptCause.getMessage(), expr, scriptCause.getLineNumber(), scriptCause.getColumnNumber());
                }
            }
        }
    }

    Callable<Object> expressionAsCallable(final String expr, final ScriptExecutionContext context) {
        final Bindings bindings = this.engine.getBindings(ScriptContext.ENGINE_SCOPE);
        final IProvider provider = Optional.ofNullable(context).map(ScriptExecutionContext::getExtra).map(ctx -> (IProvider) ctx.get("_PROVIDER_")).orElse(null);
        final Map<String, Object> nashApi = new HashMap<>();

        if (null != context) {
            bindings.putAll(context.getExtra());
            bindings.putAll(context.getModel());
            nashApi.putAll(context.getApi());
        }

        if (null == provider) {
            nashApi.put("__internalRecordRequire", (Function<String, Object>) (path) -> null);
        } else {
            nashApi.put("__internalRecordRequire", (Function<String, Object>) path -> {
                final byte[] libAsBytes = provider.asBytes(path);
                if (null == libAsBytes || 0 == libAsBytes.length) {
                    LOGGER.info("Required library {} not found", path);
                    return null;
                } else {
                    try {
                        return ScriptExpressionExecutor.this.engine.eval(new String(libAsBytes, StandardCharsets.UTF_8));
                    } catch (final ScriptException ex) {
                        throw new TechnicalException(this.enhanceScriptException(ex, path));
                    }
                }
            });
        }

        bindings.put("nash", nashApi);

        final Map<String, String> logContextMap = MDC.getCopyOfContextMap();

        return () -> {
            MDC.setContextMap(logContextMap);
            try {
                return this.engine.eval(expr);
            } catch (final ScriptException ex) {
                throw this.enhanceScriptException(ex);
            } catch (final RuntimeException ex) {
                throw Optional.ofNullable(ex.getCause()) //
                        .filter(Exception.class::isInstance) //
                        .map(Exception.class::cast) //
                        .orElse(ex);
            }
        };

    }

    private ScriptException enhanceScriptException(final ScriptException ex) {
        return this.enhanceScriptException(ex, null);
    }

    private ScriptException enhanceScriptException(final ScriptException ex, final String source) {
        final int fixedColumnNumber = ex.getLineNumber() > 1 ? ex.getColumnNumber() : ex.getColumnNumber();

        String translatedMessage;
        if (ex.getLineNumber() > 1) {
            translatedMessage = ex.getMessage();
        } else {
            final String[] splittedMessage = ex.getMessage().split("[\r\n]+");
            final StringBuilder builder = new StringBuilder( //
                    splittedMessage[0] //
                            .replace(String.format(FMT_ERROR_SHORT, ex.getLineNumber(), ex.getColumnNumber()), String.format(FMT_ERROR_SHORT, ex.getLineNumber(), fixedColumnNumber)) //
                            .replace(String.format(FMT_ERROR_LONG, ex.getLineNumber(), ex.getColumnNumber()), String.format(FMT_ERROR_LONG, ex.getLineNumber(), fixedColumnNumber)) //
            );

            if (splittedMessage.length > 1) {
                for (int idx = 1; idx < splittedMessage.length; idx++) {
                    builder.append('\n') //
                            .append( //
                                    splittedMessage[idx] //
                                            .replace( //
                                                    String.format(FMT_ERROR_LONG, ex.getLineNumber(), ex.getColumnNumber()), //
                                                    String.format(FMT_ERROR_LONG, ex.getLineNumber(), fixedColumnNumber)) //
                            ); //
                }
            }

            translatedMessage = builder.toString();
        }

        return new ScriptException(translatedMessage, source, ex.getLineNumber(), fixedColumnNumber);
    }

    /**
     * @param expr
     *            script expression to evaluate
     * @param context
     *            the context
     * @return A Future object which can be used to wait for or stop the
     *         execution of the script. The return value is the value returned
     *         by the script (if any) or null.
     */
    Future<Object> evalAsync(final String expr, final ScriptExecutionContext context) {
        return this.service.submit(this.expressionAsCallable(expr, context));
    }

    /**
     * Return script line number error.
     *
     * @param ex
     *            source exception
     * @return script error line number
     */
    private int getExceptionLineNumber(final Throwable ex) {
        return Arrays.stream(ex.getStackTrace()) //
                .filter(ste -> ste.getClassName().startsWith("jdk.nashorn.internal.scripts.")) //
                .findFirst() //
                .map(StackTraceElement::getLineNumber) //
                .orElse(-1);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        this.service.shutdown();
    }

    ScriptEngine getEngine() {
        return this.engine;
    }

    public ScriptExpressionExecutor setBindings(final Map<String, Object> bindings) {
        final Bindings engineScopeBindings = this.engine.getBindings(ScriptContext.ENGINE_SCOPE);

        engineScopeBindings.clear();
        engineScopeBindings.putAll(bindings);

        return this;
    }

    public ScriptExpressionExecutor setTimeout(final int timeout) {
        this.timeout = timeout;
        return this;
    }

}