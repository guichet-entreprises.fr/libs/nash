/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_0;

import java.util.Calendar;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;
import fr.ge.common.nash.engine.adapter.Option;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;

/**
 * The Date value adapter.
 *
 * @author Christian Cougourdan
 */
public class DateValueAdapter extends AbstractValueAdapter<Calendar> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DateValueAdapter.class);

    /** The Constant FORMATTER. */
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd/MM/yyyy");

    /** Minimum age selection in days. */
    @Option(description = "Minimum age selection in days")
    private int min = 0;

    /** Maximum age selection in days. */
    @Option(description = "Maximum age selection in days")
    private int max = 0;

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {
        return "Date";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This data type allows to answer questions by selecting a date in a calendar.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return DateValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        if (null == value) {
            return null;
        } else {
            return new DateTime(value).toString(FORMATTER);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Calendar fromValueElement(final ValueElement valueElement) {
        final String value = Optional.ofNullable(valueElement) //
                .map(ValueElement::getContent) //
                .filter(String.class::isInstance) //
                .map(String.class::cast) //
                .orElse(null);

        if (null != value) {
            try {
                return FORMATTER.parseDateTime(value).toCalendar(Locale.getDefault());
            } catch (final IllegalArgumentException ex) {
                try {
                    return new DateTime().withMillis(Long.parseLong(value)).toCalendar(Locale.getDefault());
                } catch (final NumberFormatException exMillis) {
                    LOGGER.info("Date conversion : number format exception on value \"{}\"", value, ex);
                }
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        String xmlValue;

        if (null == value) {
            xmlValue = null;
        } else if (value instanceof String) {
            xmlValue = Optional.ofNullable(parse((String) value)).map(dt -> dt.toString(FORMATTER)).orElse(null);
        } else if (value instanceof FormContentData) {
            xmlValue = ((FormContentData) value).asString();
        } else if (value instanceof DataBinding) {
            xmlValue = ((DataBinding) value).asString();
        } else {
            xmlValue = new DateTime(value).toString(FORMATTER);
        }

        return null == xmlValue ? null : new ValueElement(xmlValue);
    }

    /**
     * Parses date as {@link String} to {@link DateTime}.
     *
     * @param value
     *            the value
     * @return the date time
     */
    public static DateTime parse(final String value) {
        if (StringUtils.isNotEmpty(value)) {
            try {
                return FORMATTER.parseDateTime(value);
            } catch (final IllegalArgumentException ex) {
                try {
                    return new DateTime().withMillis(Long.parseLong(value));
                } catch (final NumberFormatException exMillis) {
                    LOGGER.info("Date conversion : number format exception on value \"{}\" ({})", value, ex.getMessage());
                }
            }
        }

        return null;
    }

    /**
     * Parses date as {@link String} to {@link Calendar}.
     *
     * @param value
     *            the value
     * @return the date time
     */
    public static Calendar parseAsCalendar(final String value) {
        return Optional.ofNullable(DateValueAdapter.parse(value)) //
                .map(dt -> dt.toCalendar(Locale.getDefault())) //
                .orElse(null);
    }

    /**
     * Accesseur sur l'attribut {@link #min}.
     *
     * @return int min
     */
    public int getMin() {
        return min;
    }

    /**
     * Mutateur sur l'attribut {@link #min}.
     *
     * @param min
     *            la nouvelle valeur de l'attribut min
     */
    public void setMin(int min) {
        this.min = min;
    }

    /**
     * Accesseur sur l'attribut {@link #max}.
     *
     * @return int max
     */
    public int getMax() {
        return max;
    }

    /**
     * Mutateur sur l'attribut {@link #max}.
     *
     * @param max
     *            la nouvelle valeur de l'attribut max
     */
    public void setMax(int max) {
        this.max = max;
    }
}
