/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.bridge.script.DataWrapper;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;

/**
 * The Class AbstractBridge.
 *
 * @author Christian Cougourdan
 */
public abstract class AbstractBridge implements IBridge {

    /** The context. */
    private final EngineContext<?> context;

    /** The loaded data. */
    private final Map<String, DataWrapper> loadedData = new HashMap<>();

    /**
     * Instantiates a new abstract bridge.
     *
     * @param context
     *            the context
     */
    public AbstractBridge(final EngineContext<?> context) {
        this.context = context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        // Nothing to do
    }

    /**
     * Gets the context.
     *
     * @return the context
     */
    protected EngineContext<?> getContext() {
        return this.context;

    }

    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    public Configuration getConfiguration() {
        return Optional.ofNullable(this.context).map(EngineContext::getRecord).map(SpecificationLoader::getConfiguration).orElse(null);
    }

    /**
     * Gets the base provider.
     *
     * @return the base provider
     */
    public IProvider getBaseProvider() {
        return Optional.ofNullable(this.context).map(EngineContext::getBaseProvider).orElse(null);
    }

    /**
     * Gets the provider.
     *
     * @return the provider
     */
    public IProvider getProvider() {
        return Optional.ofNullable(this.context).map(EngineContext::getProvider).orElse(null);
    }

    /**
     * Gets the record.
     *
     * @return the record
     */
    public SpecificationLoader getRecord() {
        return Optional.ofNullable(this.context).map(EngineContext::getRecord).orElse(null);
    }

    /**
     * Gets the element.
     *
     * @return the element
     */
    public Object getElement() {
        return Optional.ofNullable(this.context).map(EngineContext::getElement).orElse(null);
    }

    /**
     * Gets the parent element.
     *
     * @param <R>
     *            the generic type
     * @param expected
     *            the expected
     * @return the parent element
     */
    public <R> R getParentElement(Class<R> expected) {
        return Optional.ofNullable(this.context.getParent(expected)) //
                .map(EngineContext::getElement) //
                .map(expected::cast) //
                .orElse(null);
    }

    /**
     * Accesseur sur l'attribut {@link #loadedData}.
     *
     * @return Map<String,DataWrapper> loadedData
     */
    @Override
    public Map<String, DataWrapper> getLoadedData() {
        return Optional.ofNullable(loadedData).orElse(new HashMap<String, DataWrapper>());
    }

}
