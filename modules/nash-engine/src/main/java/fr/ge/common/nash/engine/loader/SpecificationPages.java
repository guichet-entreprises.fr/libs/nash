package fr.ge.common.nash.engine.loader;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.loader.template.DataElementWrapper;
import fr.ge.common.nash.engine.loader.template.NodeWrapper;
import fr.ge.common.nash.engine.loader.template.TemplateModelExtractor;
import fr.ge.common.nash.engine.manager.DefaultAttributeManager;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.support.thymeleaf.Constants;
import fr.ge.common.nash.engine.support.thymeleaf.FragmentUtil;
import fr.ge.common.nash.engine.support.thymeleaf.NashDialect;
import fr.ge.common.nash.engine.support.thymeleaf.NashMessageResolver;
import fr.ge.common.utils.CoreUtil;

/**
 * Generate UI base template for specific pages, later used to render browser
 * pages. Moreover this class generate data model which will be use in final
 * rendering.
 *
 * @author Christian Cougourdan
 */
public class SpecificationPages {

    private final SpecificationLoader loader;

    private static final TemplateEngine PRECOMPILE_TEMPLATE_ENGINE;

    private static final TemplateEngine UI_BUILD_TEMPLATE_ENGINE;

    private static final Object CONTEXT_STAT_UNIQ = new Object() {

        public int getCount() {
            return 1;
        }

        public int getSize() {
            return 1;
        }

    };

    /*
     * Thymeleaf engine initialization, used to generate base template.
     */
    static {
        /*
         * Widgets template resolver
         */
        final ClassLoaderTemplateResolver valueAdapterResolver = new ClassLoaderTemplateResolver();
        valueAdapterResolver.setOrder(1);
        valueAdapterResolver.setResolvablePatterns(new HashSet<>(Arrays.asList("*/template-v2", "*/template-v3")));
        valueAdapterResolver.setPrefix("/");
        valueAdapterResolver.setSuffix(".html");
        valueAdapterResolver.setTemplateMode(TemplateMode.HTML);
        valueAdapterResolver.setCacheable(false);
        valueAdapterResolver.setCharacterEncoding("UTF-8");

        /*
         * Base template resolter, ie for data, group, ...
         */
        final ClassLoaderTemplateResolver nashResolver = new ClassLoaderTemplateResolver();
        nashResolver.setOrder(2);
        nashResolver.setResolvablePatterns(new HashSet<>(Arrays.asList("fm:*", "*")));
        nashResolver.setPrefix('/' + NashDialect.class.getPackage().getName().replace('.', '/') + "/resources/");
        nashResolver.setSuffix(".html");
        nashResolver.setTemplateMode(TemplateMode.HTML);
        nashResolver.setCacheable(false);
        nashResolver.setCharacterEncoding("UTF-8");

        /*
         * Cache template engine
         */
        final TemplateEngine precompileTemplateEngine = new TemplateEngine();
        precompileTemplateEngine.setTemplateResolvers(new HashSet<>(Arrays.asList(valueAdapterResolver, nashResolver)));
        precompileTemplateEngine.setAdditionalDialects(Collections.singleton(new NashDialect()));

        PRECOMPILE_TEMPLATE_ENGINE = precompileTemplateEngine;

        /*
         * Inline template resolver
         */
        final StringTemplateResolver stringResolver = new StringTemplateResolver();
        stringResolver.setOrder(1);
        stringResolver.setTemplateMode(TemplateMode.HTML);
        stringResolver.setCacheable(false);

        /*
         * Inline template engine
         */
        final TemplateEngine buildTemplateEngine = new TemplateEngine();
        buildTemplateEngine.setTemplateResolvers(new HashSet<>(Arrays.asList(stringResolver)));
        buildTemplateEngine.setAdditionalDialects(Collections.singleton(new NashDialect()));
        buildTemplateEngine.setMessageResolver(new NashMessageResolver());

        UI_BUILD_TEMPLATE_ENGINE = buildTemplateEngine;
    }

    /**
     * Generic constructor.
     *
     * @param loader
     *            record loader
     */
    SpecificationPages(final SpecificationLoader loader) {
        this.loader = loader;
    }

    /**
     * Look for base page template in record. If not found, generate one, store it
     * in record and return it.
     *
     * @param step
     *            needed step
     * @param page
     *            needed page inside step
     * @return page template content
     */
    public String find(final StepElement step, final int page) {
        final EngineContext<FormSpecificationData> ctx = this.loader.buildEngineContext(step.getPosition());

        return this.find(ctx, page);
    }

    /**
     * Look for base page template in record. If not found, generate one, store it
     * in record and return it.
     *
     * @param engineContext
     *            engine context corresponding to needed step
     * @param idxPage
     *            needed page inside step
     * @return page template content
     */
    private String find(final EngineContext<FormSpecificationData> engineContext, final int idxPage) {
        final FormSpecificationData stepData = engineContext.getElement();
        final StepElement stepElement = engineContext.getParent(StepElement.class).getElement();

        final String templateResourceName = String.format("__cache__/tpl-%d-%d.html", stepElement.getPosition(), idxPage);
        byte[] templateAsBytes = CoreUtil.time("Looking for template " + templateResourceName, () -> this.loader.getProvider().asBytes(templateResourceName));

        if (ArrayUtils.isEmpty(templateAsBytes)) {
            final String templateAsString = CoreUtil.time("Build template " + templateResourceName, () -> this.buildPrecompiledTemplate(stepData, stepElement, idxPage));
            CoreUtil.time("Upload template " + templateResourceName, () -> this.loader.getProvider().save(templateResourceName, templateAsString.getBytes(StandardCharsets.UTF_8)));
            return templateAsString;
        } else {
            try {
                return new String(templateAsBytes, StandardCharsets.UTF_8);
            } finally {
                templateAsBytes = null;
            }
        }
    }

    /**
     * Build and persist all step data page.
     *
     * @param engineContext
     *            step data engine context
     */
    public void create(final EngineContext<FormSpecificationData> engineContext) {
        if (null == engineContext) {
            return;
        }

        final FormSpecificationData stepData = engineContext.getElement();
        final StepElement stepElement = engineContext.getParent(StepElement.class).getElement();

        for (int idxPage = 0; idxPage < stepData.getGroups().size(); idxPage++) {
            this.create(engineContext, stepElement, idxPage);
        }
    }

    private void create(final EngineContext<FormSpecificationData> engineContext, final StepElement stepElement, final int idxPage) {
        final FormSpecificationData stepData = engineContext.getElement();

        final String templateResourceName = String.format("__cache__/tpl-%d-%d.html", stepElement.getPosition(), idxPage);
        final String templateAsString = CoreUtil.time("Build template " + templateResourceName, () -> this.buildPrecompiledTemplate(stepData, stepElement, idxPage));
        CoreUtil.time("Upload template " + templateResourceName, () -> this.loader.getProvider().save(templateResourceName, templateAsString.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * Build precompiled template for specific step, identified by the engine
     * context, and page.
     *
     * @param engineContext
     *            data description engine context
     * @param idxPage
     *            page to retrieve
     * @return precompiled template
     */
    public String buildPrecompiledTemplate(final EngineContext<FormSpecificationData> engineContext, final int idxPage) {
        final FormSpecificationData stepData = engineContext.getElement();
        final StepElement stepElement = engineContext.getParent(StepElement.class).getElement();

        return this.buildPrecompiledTemplate(stepData, stepElement, idxPage);
    }

    /**
     * Build precompiled template for specific step and page.
     *
     * @param stepData
     *            data description
     * @param stepElement
     *            step element
     * @param idxPage
     *            page to retrieve
     * @return precompiled template
     */
    public String buildPrecompiledTemplate(final FormSpecificationData stepData, final StepElement stepElement, final int idxPage) {
        final Context context = this.buildPrecompiledTemplateContext(stepData, stepElement, idxPage);

        return PRECOMPILE_TEMPLATE_ENGINE.process("all", context).replaceAll("(?<=\\s|</?)nash-(th|form|nash)(?=:)", "$1");
    }

    /**
     *
     * @param request
     * @param response
     * @param servletContext
     * @param engineContext
     * @param idxPage
     * @param elm
     * @param additionalModelAttributes
     *            additional model attributes on UI generation
     * @return
     */
    public String buildFragment(final HttpServletRequest request, final HttpServletResponse response, final ServletContext servletContext, final EngineContext<FormSpecificationData> engineContext,
            final int idxPage, final IElement<?> elm, final Map<String, Object> additionalModelAttributes) {

        final FormSpecificationData stepData = engineContext.getElement();
        final StepElement stepElement = engineContext.getParent(StepElement.class).getElement();

        final WebContext webContext = this.buildTemplateContext(request, response, servletContext, engineContext, idxPage, elm);
        webContext.setVariables(additionalModelAttributes);
        webContext.setVariable(Constants.MODEL_JSON_VALUE, this.buildFragmentAsJson(elm));
        webContext.setVariable("stat", CONTEXT_STAT_UNIQ);

        final Context context = this.buildPrecompiledTemplateContext(stepData, stepElement, idxPage);
        context.setVariable(Constants.MODEL_DATA, elm);
        context.setVariable(Constants.MODEL_DATA_PATH, ElementPath.create(elm.getPath()));

        if (elm instanceof DataElement) {
            final DataElementWrapper root = (DataElementWrapper) webContext.getVariable(Constants.MODEL_PAGE);
            final IValueAdapter<?> dataType = root.getAdapter();
            context.setVariable(Constants.MODEL_DATA_TYPE, dataType);
            context.setVariable(Constants.MODEL_TEMPLATE_NAME, FragmentUtil.getDataTemplate(elm, dataType));
        } else {
            context.setVariable(Constants.MODEL_CHILDREN, FragmentUtil.getFilteredChildren((GroupElement) elm));
            context.setVariable(Constants.MODEL_TEMPLATE_NAME, FragmentUtil.getDataTemplate(elm, null));
        }

        final String template = PRECOMPILE_TEMPLATE_ENGINE.process(elm instanceof GroupElement ? "fragment-group" : "fragment-data", context).replaceAll("(?<=\\s|</?)nash-(th|form|nash)(?=:)", "$1");

        return UI_BUILD_TEMPLATE_ENGINE.process(template, webContext);
    }

    /**
     * Extract data from element and return it as base64 encoded JSON.
     *
     * @param element
     *            base element
     * @return base64 encoded JSON value
     */
    private String buildFragmentAsJson(final IElement<?> element) {
        final Map<String, Object> fragmentModel = RecursiveDataModelExtractor.create(null).extract(element);

        try {
            final byte[] fragmentModelAsBytes = new ObjectMapper().writeValueAsBytes(fragmentModel);
            return Base64.getEncoder().encodeToString(fragmentModelAsBytes);
        } catch (final JsonProcessingException ex) {
            throw new TechnicalException("", ex);
        }
    }

    /**
     * Generate enhanced model, containing element's definition and associated
     * values.
     *
     * @param step
     *            needed step
     * @param idxPage
     *            needed page inside step
     * @return enhanced model
     */
    public NodeWrapper buildTemplateModel(final StepElement step, final int idxPage) {
        return this.buildTemplateModel(this.loader.buildEngineContext(step.getPosition()), idxPage);
    }

    private NodeWrapper buildTemplateModel(final EngineContext<FormSpecificationData> engineContext, final int idxPage) {
        final GroupElement page = Optional.ofNullable(engineContext.getElement().getGroups()).filter(lst -> null != lst && idxPage < lst.size()).map(lst -> lst.get(idxPage)).orElse(null);
        return new TemplateModelExtractor(engineContext).extract(page);
    }

    /**
     * Build HTML page.
     *
     * @param request
     *            HTTP request
     * @param response
     *            HTTP response
     * @param servletContext
     *            servlet context
     * @param step
     *            needed step
     * @param idxPage
     *            needed page inside step
     * @param additionalModelAttributes
     *            additional model attributes on UI generation
     * @return HTML page
     */
    public String build(final HttpServletRequest request, final HttpServletResponse response, final ServletContext servletContext, final StepElement step, final int idxPage,
            final Map<String, Object> additionalModelAttributes) {
        return this.build(request, response, servletContext, this.loader.buildEngineContext(step.getPosition()), idxPage, additionalModelAttributes);
    }

    /**
     * Build HTML page.
     *
     * @param request
     *            HTTP request
     * @param response
     *            HTTP response
     * @param servletContext
     *            servlet context
     * @param engineContext
     *            engine context corresponding to needed step
     * @param idxPage
     *            page index
     * @param additionalModelAttributes
     *            additional model attributes on UI generation
     * @return HTML page
     */
    public String build(final HttpServletRequest request, final HttpServletResponse response, final ServletContext servletContext, final EngineContext<FormSpecificationData> engineContext,
            final int idxPage, final Map<String, Object> additionalModelAttributes) {

        final String template = this.find(engineContext, idxPage);

        final WebContext templateContext = this.buildTemplateContext(request, response, servletContext, engineContext, idxPage, null);
        templateContext.setVariables(additionalModelAttributes);

        return UI_BUILD_TEMPLATE_ENGINE.process(template, templateContext);
    }

    /**
     * Build precompile template context.
     *
     * @param stepData
     *            step data description
     * @param stepElement
     *            step description
     * @param idxPage
     *            page index
     * @return template context
     */
    private Context buildPrecompiledTemplateContext(final FormSpecificationData stepData, final StepElement stepElement, final int idxPage) {
        final Context context = new Context(Locale.getDefault());
        context.setVariable(Constants.MODEL_LOADER, this.loader);
        context.setVariable(Constants.MODEL_CODE, this.loader.description().getRecordUid());
        context.setVariable(Constants.MODEL_STEP, stepData);
        context.setVariable(Constants.MODEL_STEP_INDEX, stepElement.getPosition());
        context.setVariable(Constants.MODEL_PAGE, new DefaultAttributeManager(stepData).register(idxPage));
        context.setVariable(Constants.MODEL_PAGE_INDEX, idxPage);

        return context;
    }

    /**
     * Build template context for final HTML rendering.
     *
     * @param request
     *            HTTP request
     * @param response
     *            HTTP response
     * @param servletContext
     *            servlet context
     * @param engineContext
     *            engine context corresponding to needed step
     * @param idxPage
     *            page index
     * @param root
     *            model root element
     * @return template context
     */
    private WebContext buildTemplateContext(final HttpServletRequest request, final HttpServletResponse response, final ServletContext servletContext,
            final EngineContext<FormSpecificationData> engineContext, final int idxPage, final IElement<?> root) {

        final FormSpecificationData stepData = engineContext.getElement();

        final WebContext context = new WebContext(request, response, servletContext);
        context.setVariable(Constants.MODEL_LOADER, this.loader);
        context.setVariable(Constants.MODEL_CODE, this.loader.description().getRecordUid());
        context.setVariable(Constants.MODEL_STEP, stepData);
        context.setVariable(Constants.MODEL_STEP_INDEX, engineContext.getParent(StepElement.class).getElement().getPosition());
        context.setVariable(Constants.MODEL_PAGE_INDEX, idxPage);

        if (null == root) {
            context.setVariable(Constants.MODEL_PAGE, this.buildTemplateModel(engineContext, idxPage));
        } else {
            context.setVariable(Constants.MODEL_PAGE, new TemplateModelExtractor(engineContext).extract(root));
        }

        return context;
    }

}
