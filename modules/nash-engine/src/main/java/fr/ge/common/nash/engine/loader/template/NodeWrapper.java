/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader.template;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;

/**
 * Element wrapper which aggregate element and associate informations like
 * parsed path.
 *
 *
 * @author Christian Cougourdan
 */
public class NodeWrapper {

    private final IElement<?> elm;

    private ElementPath path;

    /**
     *
     * @param elm
     */
    NodeWrapper(final IElement<?> elm) {
        elm.setId(elm.getId().replaceAll("\\[\\]", "[0]"));
        if (StringUtils.isNotEmpty(elm.getParentPath())) {
            elm.setParentPath(elm.getParentPath().replaceAll("\\[\\]", "[0]"));
            elm.setPath(elm.getParentPath() + '.' + elm.getId());
        } else {
            elm.setPath(elm.getId());
        }
        this.elm = elm;
        this.path = ElementPath.create(elm.getPath().replaceFirst("^[^.]+\\.", ""));
    }

    /**
     * @return the elm
     */
    public IElement<?> getElm() {
        return this.elm;
    }

    /**
     * @return the path
     */
    public ElementPath getPath() {
        return this.path;
    }

    /**
     * @param newPath
     */
    protected void setPath(final ElementPath newPath) {
        this.path = newPath;
    }

}
