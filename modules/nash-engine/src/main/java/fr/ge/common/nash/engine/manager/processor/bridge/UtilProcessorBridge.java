/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.util.MimeTypeUtil;
import fr.ge.common.nash.core.util.UidFactoryImpl;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.ScriptExecutionHelper;
import fr.ge.common.utils.bean.FileEntry;

/**
 * The Class UtilProcessorBridge.
 *
 * @author Christian Cougourdan
 */
public class UtilProcessorBridge extends AbstractBridge {

    private static final ScriptExecutionHelper HELPER = new ScriptExecutionHelper();

    /** The empty instance. */
    private static IBridge emptyInstance;

    /** The factory. */
    private final UidFactoryImpl factory = new UidFactoryImpl();

    /** Checksum buffer size. */
    private static final int CHECKSUM_BUFFER_SIZE = 8192;

    /**
     * Instantiates a new util processor bridge.
     *
     * @param context
     *            the context
     */
    public UtilProcessorBridge(final EngineContext<?> context) {
        super(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "util";
    }

    /**
     * Build file item resource from path.
     *
     * @param resourcePath
     *            the resource path
     * @return the item
     */
    public Item resourceFromPath(final String resourcePath) {
        return this.resourceFromPath(resourcePath, Optional.ofNullable(Paths.get(resourcePath)).map(Path::getFileName).map(Object::toString).orElse("document.bin"));
    }

    /**
     * Resource from path.
     *
     * @param resourcePath
     *            the resource path
     * @param resourceName
     *            the resource name
     * @return the item
     */
    public Item resourceFromPath(final String resourcePath, final String resourceName) {
        return FileValueAdapter.createItem( //
                -1, //
                resourceName, //
                MimeTypeUtil.type(resourcePath), //
                () -> Optional.ofNullable(this.getProvider().load(resourcePath)).map(FileEntry::asBytes).orElse(null) //
        ).setAbsolutePath(this.getProvider().getAbsolutePath(resourcePath));
    }

    /**
     * Checks if specified object is a {@link List} instance.
     *
     * @param obj
     *            the obj
     * @return true, if list instance
     */
    public boolean isList(final Object obj) {
        return HELPER.isList(obj);
    }

    /**
     * Empty.
     *
     * @return the i bridge
     */
    public static IBridge empty() {
        if (null == emptyInstance) {
            emptyInstance = new UtilProcessorBridge(null);
        }

        return emptyInstance;
    }

    /**
     * Resource from binary.
     *
     * @param resourceName
     *            the resource name
     * @param resourceAsString
     *            the resource as string
     * @return the item
     */
    public Item resourceFromBinary(final String resourceName, final Object value) {
        if (null != value) {
            if (value instanceof String) {
                final String valueAsString = (String) value;
                return FileValueAdapter.createItem(-1, resourceName, MimeTypeUtil.type(resourceName), () -> Base64.getDecoder().decode(valueAsString));
            } else if (value instanceof byte[]) {
                return FileValueAdapter.createItem(-1, resourceName, MimeTypeUtil.type(resourceName), () -> (byte[]) value);
            } else {
                throw new TechnicalException("Cannot read value from binary");
            }
        }
        return null;
    }

    /**
     * convert String to byte
     *
     * @param content
     *            data encoded in Base64
     * @return byte[]
     */
    public byte[] convertBase64ToByte(final String content) {
        return HELPER.convertBase64ToByte(content);
    }

    /**
     * convert byte to String
     *
     * @param content
     *            byte
     * @return String base 64
     */
    public String convertByteToBase64(final byte[] content) {
        return HELPER.convertByteToBase64(content);
    }

    public String convertToJson(final Object object) {
        return HELPER.convertToJson(object);
    }

    /**
     * Generate a new uid.
     *
     * @return
     */
    public String generateUid() {
        return this.factory.uid();
    }

    /**
     * Read the value and calculate checksum with a specific algorithm.
     *
     * @param value
     *            The value to calculate
     * @param type
     *            The specific algorithm
     * @return
     * @throws TemporaryException
     */
    public byte[] createChecksum(final Object value, final String type) throws TemporaryException {
        byte[] content = null;
        if (value instanceof String) {
            content = ((String) value).getBytes();
        } else if (value instanceof byte[]) {
            content = (byte[]) value;
        } else {
            throw new TechnicalException("Cannot read value from binary");
        }

        try (InputStream fis = new ByteArrayInputStream(content)) {
            final MessageDigest digest = MessageDigest.getInstance(type);
            int n = 0;
            final byte[] buffer = new byte[CHECKSUM_BUFFER_SIZE];

            while (n != -1) {
                n = fis.read(buffer);
                if (n > 0) {
                    digest.update(buffer, 0, n);
                }
            }
            return new HexBinaryAdapter().marshal(digest.digest()).getBytes(StandardCharsets.UTF_8);
        } catch (final IOException ex) {
            throw new TemporaryException("Unable to read record file : " + ex.getMessage());
        } catch (final NoSuchAlgorithmException ex) {
            throw new TechnicalException("Digest algorithm [" + type + "] not found" + ex.getMessage());
        }
    }
}
