/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.script.Bindings;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.core.bean.EventTypeEnum;
import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.event.Event;
import fr.ge.common.nash.engine.loader.event.EventBus;
import fr.ge.common.nash.engine.loader.event.MessageEvent;
import fr.ge.common.nash.engine.loader.event.RecordStepStatusChangeEvent;
import fr.ge.common.nash.engine.loader.event.ScriptEvent;
import fr.ge.common.nash.engine.manager.DataModelManager;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.manager.parser.ElementParser;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationHistory;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationReferential;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.EventElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.EventsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.ReferentialsForElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsForElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.PatternProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.NashScriptEngine;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.script.ScriptValueConversionService;
import fr.ge.common.nash.engine.script.expression.ScriptExpression;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.utils.CoreUtil;

/**
 * Load form specification using custom provider.
 *
 * @author Christian Cougourdan
 */
@Component
@Primary
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SpecificationLoader {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpecificationLoader.class);

    /** The Constant NAME_ROOT_ELEMENT. */
    private static final String NAME_ROOT_ELEMENT = "description.xml";

    /** The Constant NAME_ROOT_META_ELEMENT. */
    private static final String NAME_ROOT_META_ELEMENT = "meta.xml";

    /** The default history path. */
    private static final String DEFAULT_HISTORY_PATH = "history.xml";

    /** The root element. */
    private FormSpecificationDescription rootElement;

    /** The configuration. */
    private final Configuration configuration;

    /** The provider. */
    private final IProvider provider;

    /** The processes. */
    private SpecificationProcesses processes;

    /** The steps. */
    private SpecificationSteps steps;

    /** The elements. */
    private SpecificationElements elements;

    private SpecificationPages pages;

    /** The script engine. */
    private NashScriptEngine scriptEngine;

    private SpecificationTranslation i18n;

    /** The root element. */
    private FormSpecificationMeta rootMetaElement;

    @Autowired
    private Engine engine;

    @Autowired
    private ApplicationContext applicationContext;

    private final EventBus eventBus = new SpecificationEvents();

    /**
     * Default constructor.
     *
     * @param configuration
     *            the configuration
     * @param provider
     *            the provider
     */
    protected SpecificationLoader(final Configuration configuration, final IProvider provider) {
        this.configuration = configuration;
        this.provider = provider;
    }

    /**
     * Load form specification from description entry point.
     *
     * @return form description
     */
    public FormSpecificationDescription description() {

        if (null == this.rootElement) {
            this.rootElement = this.provider.asBean(NAME_ROOT_ELEMENT, FormSpecificationDescription.class);

            if (null == this.rootElement) {
                throw new RecordNotFoundException("Specification's description file not found");
            }

            this.stepsMgr().updatePositions();

            if (this.rootElement.getEvents() != null) {
                final List<EventElement> eventElements = Optional.ofNullable(this.rootElement).map(FormSpecificationDescription::getEvents).map(EventsElement::getElements).orElse(new ArrayList<>());
                for (int idx = 0; idx < eventElements.size(); idx++) {
                    eventElements.get(idx).setPosition(idx);
                }
            }
        }

        return this.rootElement;
    }

    /**
     * Retrieves the referentials list.
     *
     * @return list of referential elements
     */
    public List<ReferentialsForElement> referentials() {
        return Optional.ofNullable(this.description()) //
                .map(desc -> desc.getReferentials() == null ? null : desc.getReferentials().getForElements()) //
                .orElse(new ArrayList<>());
    }

    /**
     * Retrieves a referential path.
     *
     * @param id
     *            the id
     * @return the referential path
     */
    public String referentialPath(final String id) {
        String path = null;
        if (id != null) {
            for (final ReferentialsForElement referentialElement : this.referentials()) {
                if (id.equals(referentialElement.getId())) {
                    path = referentialElement.getPath();
                    break;
                }
            }
        }
        return path;
    }

    /**
     * Loads a referential from its id.
     *
     * @param id
     *            id the id
     * @return the referential
     */
    public FormSpecificationReferential referential(final String id) {
        FormSpecificationReferential referentialElement = null;
        if (id != null) {
            final String path = this.referentialPath(id);
            if (path != null) {
                referentialElement = this.provider.asBean(path, FormSpecificationReferential.class);
            }
        }
        return referentialElement;
    }

    /**
     * Retrieves the translations list.
     *
     * @return list of translation elements
     */
    public List<TranslationsForElement> translations() {
        return Optional.ofNullable(this.description()) //
                .map(desc -> desc.getTranslations() == null ? null : desc.getTranslations().getForElements()) //
                .orElse(new ArrayList<>());
    }

    /**
     * Retrieves a translation path.
     *
     * @param lang
     *            the language
     * @return the translation path
     */
    public String translationPath(final String lang) {
        String path = null;
        if (lang != null) {
            for (final TranslationsForElement translationElement : this.translations()) {
                if (lang.equals(translationElement.getLang())) {
                    path = translationElement.getPath();
                    break;
                }
            }
        }
        return path;
    }

    /**
     * Retrieves a translation file.
     *
     * @param lang
     *            the language
     * @return the translation file
     */
    public InputStream translationFile(final String lang) {
        return this.getFile(this.translationPath(lang));
    }

    /**
     * Retrieves the history path.
     *
     * @return the history path
     */
    public String historyPath() {
        return Optional.ofNullable(this.description()) //
                .map(FormSpecificationDescription::getHistoryPath) //
                .orElse(DEFAULT_HISTORY_PATH);
    }

    /**
     * Loads the history from its path.
     *
     * @return the history
     */
    public FormSpecificationHistory history() {
        FormSpecificationHistory historyElement = null;
        final String path = this.historyPath();
        if (path != null) {
            historyElement = this.provider.asBean(path, FormSpecificationHistory.class);
            if (historyElement == null) {
                historyElement = new FormSpecificationHistory();
            }
        }
        return historyElement;
    }

    /**
     * Retrieves a file from a path.
     *
     * @param path
     *            the path
     * @return the file
     */
    private InputStream getFile(final String path) {
        InputStream file = null;
        if (path != null) {
            final byte[] fileAsBytes = this.provider.asBytes(path);
            if (fileAsBytes != null) {
                file = new ByteArrayInputStream(fileAsBytes);
            }
        }
        return file;
    }

    /**
     * Retrieve first step todo for an engine user.
     *
     * @param engineUser
     *            the engine user
     * @return first step todo for an engine user
     */
    public StepElement firstStepToDo(final String engineUser) {
        return this.stepsMgr().findFirst(engineUser, StepStatusEnum.TO_DO, StepStatusEnum.IN_PROGRESS);
    }

    /**
     * Retrieve last step todo for an engine user.
     *
     * @param engineUser
     *            the engine user
     * @return first step todo for an engine user
     */
    public StepElement lastStepToDo(final String engineUser) {
        return this.stepsMgr().findLast(engineUser, StepStatusEnum.TO_DO, StepStatusEnum.IN_PROGRESS);
    }

    /**
     * Retrieve last step Done.
     *
     * @return last step done for an engine user
     */
    public StepElement lastStepDone() {
        return this.stepsMgr().findLast(null, StepStatusEnum.DONE);
    }

    /**
     * Update record status.
     *
     * @param stepIndex
     *            step index
     * @param status
     *            status
     * @param author
     *            the author
     * @param readOnly
     *            read only mode
     * @return current loader instance
     */
    public SpecificationLoader updateStepStatus(final int stepIndex, final StepStatusEnum status, final String author, final boolean readOnly) {
        if (readOnly) {
            return this;
        } else {
            return this.updateStepStatus(stepIndex, status, author);
        }
    }

    /**
     * Update record status.
     *
     * @param stepIndex
     *            step index
     * @param status
     *            status
     * @param author
     *            the author
     * @return current loader instance
     */
    public SpecificationLoader updateStepStatus(final int stepIndex, final StepStatusEnum status, final String author) {
        this.updateStatus(this.stepsMgr().find(stepIndex), status, author);
        return this;
    }

    /**
     * Update record status.
     *
     * @param stepToUpdate
     *            step element
     * @param status
     *            new status
     * @param author
     *            author
     * @return current loader instance
     */
    public SpecificationLoader updateStatus(final StepElement stepToUpdate, final StepStatusEnum status, final String author) {
        final StepElement currentStep = this.stepsMgr().current();

        if (StepStatusEnum.SEALED.getStatus().equals(stepToUpdate.getStatus())) {
            LOGGER.info("Can't update sealed step #{} with {} status", stepToUpdate.getPosition(), status);
            return this;
        }

        if (stepToUpdate.getPosition() > currentStep.getPosition()) {
            LOGGER.info("Can't update next step #{}, current is #{}", stepToUpdate.getPosition(), currentStep.getPosition());
            return this;
        }

        final StepStatusEnum oldStatus = StepStatusEnum.fromCode(stepToUpdate.getStatus());

        /*
         * Update previous steps
         */
        if (StepStatusEnum.SEALED == status) {
            this.stepsMgr().findAll().stream() //
                    .filter(s -> s.getPosition() < stepToUpdate.getPosition()) //
                    .forEach(s -> s.setStatus(status.getStatus()));
        }
        if (StepStatusEnum.DONE == status) {
            this.stepsMgr().findAll().stream() //
                    .filter(s -> s.getPosition() < stepToUpdate.getPosition()) //
                    .filter(s -> !s.getStatus().equals(StepStatusEnum.SEALED.getStatus()) && !s.getStatus().equals(StepStatusEnum.ERROR.getStatus())) //
                    .forEach(s -> s.setStatus(status.getStatus()));
        }

        /*
         * Update next steps
         */
        if (StepStatusEnum.TO_DO == status || StepStatusEnum.IN_PROGRESS == status) {
            this.stepsMgr().findAll().stream() //
                    .filter(s -> s.getPosition() > stepToUpdate.getPosition()) //
                    .forEach(s -> s.setStatus(StepStatusEnum.TO_DO.getStatus()));
        }

        /*
         * Build status updating message
         */
        final String message = this.buildStepChangeMessage(stepToUpdate, status);

        /*
         * Update required step
         */
        stepToUpdate.setStatus(status.getStatus());

        /*
         * Trigger step status update event
         */
        if (StringUtils.isNotEmpty(message)) {
            this.getEventBus().post(new MessageEvent(this).setAuthor(author).setMessage(message));
        }

        this.getEventBus().post(new ScriptEvent(this).setType(EventTypeEnum.BEFORE_UPDATE_STEP_STATUS));
        if (!status.equals(oldStatus)) {
            this.getEventBus().post(new RecordStepStatusChangeEvent(this, stepToUpdate, oldStatus, status));
        }

        return this;
    }

    /**
     * Persist root element.
     *
     * @return current loader instance
     */
    public SpecificationLoader flush() {
        this.save(NAME_ROOT_ELEMENT, this.rootElement);
        return this;
    }

    /**
     * Gets a message which describes a current step change.
     *
     * @param step
     *            the step element
     * @param status
     *            the status
     * @return the message
     */
    protected String buildStepChangeMessage(final StepElement step, final StepStatusEnum status) {
        String message = null;
        final String stepLabel = CoreUtil.coalesce(step::getLabel, step::getId);
        final StepElement nextStep = this.stepsMgr().find(step.getPosition() + 1);
        final boolean currentStepStatusWasChanged = !status.getStatus().equals(step.getStatus());
        final boolean currentStepWasFinished = currentStepStatusWasChanged && StepStatusEnum.DONE.equals(status);
        final boolean isLastStep = nextStep == null;

        if (currentStepWasFinished) {
            if (isLastStep) {
                message = MessageFormat.format("L''étape \"{0}\" est finalisée.", stepLabel);
            } else {
                message = MessageFormat.format("Le dossier passe de l''étape \"{0}\" à l''étape \"{1}\".", stepLabel, CoreUtil.coalesce(nextStep::getLabel, nextStep::getId));
            }
        }

        return message;
    }

    /**
     * Return current step index, identify by progress element.
     *
     * @return current step index
     */
    public int currentStepPosition() {
        final StepElement currentStep = this.stepsMgr().current();
        return currentStep.getPosition();
    }

    /**
     * Update record step with values contained in HTTP request.
     *
     * @param stepIndex
     *            step index
     * @param request
     *            HTTP request containing new values
     * @return updated record data structure
     */
    public FormSpecificationData updateFromRequest(final int stepIndex, final HttpServletRequest request) {
        final StepElement currentStep = this.stepsMgr().current();
        final StepElement stepToUpdate = this.stepsMgr().find(stepIndex);

        final FormSpecificationData data = this.data(stepToUpdate);

        if (null == currentStep) {
            LOGGER.info("Update step #{} from request : no current step found", stepIndex);
        } else if (null == stepToUpdate) {
            LOGGER.info("Update step #{} from request : step not found", stepIndex);
        } else if (stepToUpdate.getPosition() > currentStep.getPosition()) {
            LOGGER.info("Update step #{} from request : step to update is positionned after current step (#{})", stepIndex, currentStep.getPosition());
        } else if (StepStatusEnum.SEALED.getStatus().equals(stepToUpdate.getStatus())) {
            LOGGER.info("Update step #{} from request : step is SEALED", stepIndex);
        } else if (null != data) {
            final EngineContext<FormSpecificationData> engineContext = this.buildEngineContext(stepIndex);
            final FormSpecificationData updated = new DataModelManager(engineContext).updateFromRequest(request);
            this.save(stepToUpdate.getData(), updated);
            return updated;
        }

        return data;
    }

    public void validate(final StepElement step) {
        this.validate(this.data(step));
    }

    /**
     * Throw TechnicalExecption when dataElement is mandatory and value is null.
     *
     * @param data
     *            FormSpecificationData
     */
    public void validate(final FormSpecificationData data) {
        if (null != data) {
            final Map<String, Object> model = this.buildEngineContext(data).buildModel();
            final GroupElement currentGrp = data.getGroups().stream().findFirst().orElse(null);
            if (null != currentGrp) {
                final Map<String, Object> stepModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));
                model.put(Scopes.PAGE.toContextKey(), stepModel.get(currentGrp.getId()));
            }
            final ScriptExecutionContext ctx = new ScriptExecutionContext().addAll(model);
            this.validate(currentGrp, ctx);
        }
    }

    /**
     * Validate a group element recursively.
     *
     * @param grp
     *            the group element to validate
     * @param ctx
     *            the script context
     */
    private void validate(final GroupElement grp, final ScriptExecutionContext ctx) {
        if (null != grp && null != grp.getData()) {
            grp.getData().forEach(elm -> {

                final boolean displayElement = Optional.ofNullable(elm) //
                        .map(IElement::getDisplayCondition) //
                        .filter(StringUtils::isNotEmpty) //
                        .map(cond -> {
                            final ScriptExpression expr = this.getScriptEngine().parse(cond, true);
                            return this.getScriptEngine().eval(expr, ctx, Boolean.class);
                        }) //
                        .orElse(true);

                if (displayElement) {
                    if (elm instanceof GroupElement) {
                        this.validate((GroupElement) elm, ctx);
                    } else {
                        final DataElement emptyData = (DataElement) elm;
                        if (null != emptyData && emptyData.isMandatory()) {
                            if (null == elm.getValue()) {
                                throw new TechnicalException("The data element '" + emptyData.getId() + "' is mandatory");
                            }
                            if (elm.getValue() instanceof ValueElement) {
                                final ValueElement valueElement = (ValueElement) elm.getValue();
                                if (null == valueElement || "null".equals(valueElement.toString()) || StringUtils.isEmpty(valueElement.toString())) {
                                    throw new TechnicalException("The data element '" + emptyData.getId() + "' is mandatory");
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * Save form specification element, looking for default stylesheet associated
     * with bean type.
     *
     * @param <T>
     *            the generic type
     * @param stepIndex
     *            step index
     * @param bean
     *            bean to persist
     */
    public <T> void save(final int stepIndex, final T bean) {
        this.save(this.stepsMgr().find(stepIndex).getData(), bean);
    }

    /**
     * Save form specification element, looking for default stylesheet associated
     * with bean type.
     *
     * @param <T>
     *            the generic type
     * @param resourcePath
     *            element file name
     * @param bean
     *            bean to persist
     */
    public <T> void save(final String resourcePath, final T bean) {
        if (null == bean) {
            return;
        }

        this.provider.save(resourcePath, bean);

        if (NAME_ROOT_ELEMENT.equals(resourcePath) && bean instanceof FormSpecificationDescription) {
            this.rootElement = (FormSpecificationDescription) bean;
        }
    }

    /**
     * Save form specification element, looking for default stylesheet associated
     * with bean type.
     *
     * @param name
     *            the name
     * @param content
     *            the content
     */
    public void saveFile(final String name, final byte[] content) {
        this.provider.save(name, content);
    }

    /**
     * Load specification data from step element.
     *
     * @param stepIndex
     *            specification data origin
     * @return specification data associated with specified step
     */
    public FormSpecificationData data(final int stepIndex) {
        return this.data(this.stepsMgr().find(stepIndex));
    }

    /**
     * Load specification data from step element.
     *
     * @param stepElement
     *            specification data origin
     * @return specification data associated with specified step
     */
    public FormSpecificationData data(final StepElement stepElement) {
        if (null == stepElement || null == stepElement.getData()) {
            return null;
        } else {
            final FormSpecificationData data = this.provider.asBean(stepElement.getData(), FormSpecificationData.class);

            if (null != data) {
                data.setResourceName(stepElement.getData());
            }

            return data;
        }
    }

    /**
     * Extract all data from previous step element related to specified one.
     *
     * @param stepLimit
     *            step from which data are not loaded
     * @return extracted model
     */
    public Map<String, Object> extractUntil(final StepElement stepLimit) {
        final Map<String, Object> model = new HashMap<>();

        for (final StepElement step : this.stepsMgr().findAll()) {
            if (stepLimit.getId().equals(step.getId())) {
                break;
            } else {
                model.putAll(this.extract(step));
            }
        }

        return model;
    }

    /**
     * Extract data from specified step element.
     *
     * @param stepElement
     *            step for which data are loaded
     * @return extracted model
     */
    public Map<String, Object> extract(final StepElement stepElement) {
        return RecursiveDataModelExtractor.create(this.provider(stepElement.getData())) //
                .extract(this.data(stepElement));
    }

    /**
     * Retrieve record provider relative to specified resource path.
     *
     * @param resourceName
     *            resource path
     * @return relative record provider
     */
    public IProvider provider(final String resourceName) {
        if (null == resourceName) {
            return this.provider;
        } else {
            return PatternProvider.from(this.provider, resourceName);
        }
    }

    /**
     * Search data element relative to record step position.
     *
     * @param stepIndex
     *            record step position
     * @param absolutePath
     *            Searched element absolute path
     * @return found element
     */
    public IElement<?> find(final int stepIndex, final String absolutePath) {
        final FormSpecificationData spec = this.data(stepIndex);

        return find(spec.getGroups(), absolutePath);
    }

    /**
     * Search data element relative to record step data structure
     *
     * @param spec
     *            record data object
     * @param absolutePath
     *            Searched element absolute path
     * @return found element
     */
    public static IElement<?> find(final FormSpecificationData spec, final String absolutePath) {
        return find(spec.getGroups(), absolutePath);
    }

    /**
     * Search data element relative to src elements.
     *
     * @param src
     *            search root elements
     * @param absolutePath
     *            Searched element absolute path
     * @return found element
     */
    public static IElement<?> find(final List<? extends IElement<?>> src, final String absolutePath) {
        return SpecificationElements.find(src, absolutePath);
    }

    /**
     * Creates a new specification loader with specified provider.
     *
     * @param provider
     *            specification provider to use
     * @return loader instance
     */
    public static SpecificationLoader create(final IProvider provider) {
        return new SpecificationLoader(null, provider);
    }

    /**
     * Return group to display. Iterate from specified group index and return the
     * first one which have no conditional display or its evaluation return true.
     *
     * @param stepIndex
     *            step index
     * @param expectedGroupIndex
     *            expected group index
     * @return first group to display
     */
    public int group(final int stepIndex, final int expectedGroupIndex) {
        final StepElement step = this.stepsMgr().find(stepIndex);

        if (expectedGroupIndex <= 0) {
            return 0;
        }

        final FormSpecificationData currentSpecificationData = this.data(step);
        final List<GroupElement> groups = currentSpecificationData.getGroups();

        final int maxGroupIndex = groups.size() - 1;

        /*
         * Ask for the last group, return it without display condition test.
         */
        if (expectedGroupIndex > maxGroupIndex) {
            return maxGroupIndex;
        }

        int realGroupIndex = Math.max(0, expectedGroupIndex);

        /*
         * No display condition for required group, return its position.
         */
        final GroupElement currentGroup = groups.get(realGroupIndex);
        if (null == currentGroup.getDisplayCondition()) {
            return realGroupIndex;
        }

        /*
         * Extract predicate execution model.
         */
        realGroupIndex = this.retrieveRealGroupIndex(currentSpecificationData, step, realGroupIndex);

        if (expectedGroupIndex != realGroupIndex) {
            this.save(stepIndex, currentSpecificationData);
        }

        return realGroupIndex;
    }

    /**
     * Check for group display condition and return first group index which is
     * displayed.
     *
     * @param spec
     *            specification data
     * @param step
     *            step element
     * @param expectedGroupIndex
     *            expected group index
     * @return real group index
     */
    private int retrieveRealGroupIndex(final FormSpecificationData spec, final StepElement step, final int expectedGroupIndex) {
        final List<GroupElement> groups = spec.getGroups();
        final int maxGroupIndex = groups.size() - 1;

        final Map<String, Object> model = this.buildModel(step, Math.min(maxGroupIndex, expectedGroupIndex));
        final Map<String, Object> currentModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));

        /*
         * Test each group for display condition
         */
        final ScriptExecutionContext context = new ScriptExecutionContext().addAll(model);

        int realGroupIndex = expectedGroupIndex;

        for (; realGroupIndex < maxGroupIndex; realGroupIndex++) {
            final GroupElement group = groups.get(realGroupIndex);

            if (null == group.getDisplayCondition()) {
                break;
            }

            final ScriptExpression expr = this.getScriptEngine().parse(group.getDisplayCondition());
            try {
                if (this.getScriptEngine().eval(expr, context, Boolean.class)) {
                    break;
                }
            } catch (final ExpressionException ex) {
                throw new ExpressionException(step.getData(), ex);
            }

            this.elements().clear(spec, group, null);
            // DataModelUpdater.emptyElement(group.getId(), group);
            currentModel.put(group.getId(), RecursiveDataModelExtractor.create(this.provider(step.getData())).extract(group));
        }

        return realGroupIndex;
    }

    /**
     * Returns previous group which can be displayed.
     *
     * @param stepIndex
     *            step index
     * @param groupIndex
     *            group index
     * @return first backward group which can be displayed
     */
    public int previousGroup(final int stepIndex, final int groupIndex) {
        int previousGroupIndex = groupIndex - 1;
        if (previousGroupIndex < 1) {
            return previousGroupIndex;
        }

        final StepElement step = this.stepsMgr().find(stepIndex);

        final FormSpecificationData currentSpecificationData = this.data(step);
        final List<GroupElement> groups = currentSpecificationData.getGroups();

        final Map<String, Object> model = this.buildModel(step, previousGroupIndex);
        final ScriptExecutionContext context = new ScriptExecutionContext().addAll(model);

        for (; previousGroupIndex > 0; previousGroupIndex--) {
            final GroupElement group = groups.get(previousGroupIndex);

            if (null == group.getDisplayCondition()) {
                break;
            }

            final ScriptExpression expr = this.getScriptEngine().parse(group.getDisplayCondition());
            try {
                if (this.getScriptEngine().eval(expr, context, Boolean.class)) {
                    break;
                }
            } catch (final ExpressionException ex) {
                throw new ExpressionException(step.getData(), ex);
            }
        }

        return previousGroupIndex;
    }

    /**
     * Extract predicate execution model.
     *
     * @param step
     *            step until which data are extracted
     * @param untilGroup
     *            step's group until which data are extracted
     * @return execution model
     */
    private Map<String, Object> buildModel(final StepElement step, final int untilGroup) {
        final FormSpecificationData stepSpecificationData = this.data(step);
        final List<GroupElement> groups = stepSpecificationData.getGroups();

        final RecursiveDataModelExtractor extractor = RecursiveDataModelExtractor.create(this.provider(step.getData()));
        final Map<String, Object> model = this.extractUntil(step);

        final Map<String, Object> stepModel = new HashMap<>();
        for (int i = 0; i < untilGroup; i++) {
            final GroupElement group = groups.get(i);
            final Map<String, Object> groupModel = extractor.extract(group);
            stepModel.put(group.getId(), groupModel);
        }
        model.put(stepSpecificationData.getId(), stepModel);

        final Map<String, Object> recordModel = new HashMap<>(Collections.singletonMap(Scopes.RECORD.toContextKey(), model));
        recordModel.put(Scopes.STEP.toContextKey(), stepModel);

        return recordModel;
    }

    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    public Configuration getConfiguration() {
        return this.configuration;
    }

    /**
     * Gets the record provider.
     *
     * @return record provider
     */
    public IProvider getProvider() {
        return this.provider;
    }

    /**
     * Retrieve processes manager.
     *
     * @return record processes manager
     */
    public SpecificationProcesses processes() {
        if (null == this.processes) {
            this.processes = new SpecificationProcesses(this);
        }
        return this.processes;
    }

    /**
     * Retrieve steps manager.
     *
     * @return reord steps manager
     */
    public SpecificationSteps stepsMgr() {
        if (null == this.steps) {
            this.steps = new SpecificationSteps(this);
        }
        return this.steps;
    }

    public boolean executeActiveStep(final String engineUser) {
        final StepElement step = this.stepsMgr().getActiveStep(engineUser);
        if (null != step) {
            LOGGER.debug("Execute step {}", step.getId());
            this.processes().preExecute(step);
            this.validate(step);
            this.processes().postExecute(step);

            this.updateStatus(step, StepStatusEnum.DONE, this.description().getAuthor()).flush();

            LOGGER.debug("Step {} execution : success", step.getId());
        }

        return null != this.stepsMgr().getActiveStep(engineUser);
    }

    /**
     * Retrieve elements manager.
     *
     * @return record elements manager
     */
    public SpecificationElements elements() {
        if (null == this.elements) {
            this.elements = new SpecificationElements(this);
        }
        return this.elements;
    }

    /**
     * Retrieve translation manager.
     *
     * @return i18n manager
     */
    public SpecificationTranslation i18n() {
        if (null == this.i18n) {
            this.i18n = new SpecificationTranslation(this);
        }
        return this.i18n;
    }

    /**
     * Retrieve record pages manager.
     *
     * @return record pages manager
     */
    public SpecificationPages pages() {
        if (null == this.pages) {
            this.pages = new SpecificationPages(this);
        }

        return this.pages;
    }

    /**
     * Builds the engine context related to instance record.
     *
     * @return the engine context
     */
    public EngineContext<SpecificationLoader> buildEngineContext() {
        return EngineContext.build(this);
    }

    /**
     * Builds the engine context related to data specification.
     *
     * @param data
     *            the data
     * @return the engine context
     */
    public EngineContext<FormSpecificationData> buildEngineContext(final FormSpecificationData data) {
        return this.buildEngineContext().target(data).withProvider(this.provider(data.getResourceName()));
    }

    /**
     * Builds the engine context related to data specification for specified step
     * index.
     *
     * @param stepIndex
     *            the step index
     * @return the engine context
     */
    public EngineContext<FormSpecificationData> buildEngineContext(final int stepIndex) {
        final StepElement stepElement = this.stepsMgr().find(stepIndex);
        return this.buildEngineContext(stepElement);
    }

    /**
     * Builds the engine context related to data specification for specified step.
     *
     * @param step
     *            the step
     * @return corresponding engine context
     */
    public EngineContext<FormSpecificationData> buildEngineContext(final StepElement step) {
        final FormSpecificationData stepData = this.data(step);
        if (null == stepData) {
            return null;
        } else {
            return this.buildEngineContext().target(step).target(stepData).withProvider(this.provider(step.getData()));
        }
    }

    /**
     * Insert new meta data.
     *
     * @param metas
     *            meta data to insert
     * @return form specification meta data object
     */
    public FormSpecificationMeta meta(final List<Map<String, Object>> bindings) {
        final List<MetaElement> metas = new ArrayList<MetaElement>();

        bindings.forEach(item -> {
            if (2 == item.size() && item.containsKey("name") && item.containsKey("value")) {
                final String name = (String) item.get("name");
                final Object value = item.get("value");

                this.buildMeta(name, value).forEach(metas::add);
            } else {
                item.entrySet().forEach(entry -> this.buildMeta(entry.getKey(), entry.getValue()).forEach(metas::add));
            }
        });

        final FormSpecificationMeta specMeta = Optional.ofNullable(this.meta()).orElseGet(FormSpecificationMeta::new);
        final List<MetaElement> specMetas = specMeta.getMetas();
        if (null == specMetas) {
            specMeta.setMetas(metas);
        } else {
            specMetas.addAll(metas);
        }
        this.rootMetaElement = specMeta;
        this.save(Engine.FILENAME_META, specMeta);
        return specMeta;
    }

    /**
     * Insert meta data.
     *
     * @param bindings
     *            meta
     * @return form specification meta data object
     */
    public FormSpecificationMeta meta(final Map<String, Object> bindings) {
        return this.meta(Collections.singletonList(bindings));
    }

    /**
     * Build meta data elements.
     *
     * @param name
     *            meta data key
     * @param value
     *            meta data value(s)
     * @return build {@link MetaElement} collection
     */
    private Collection<MetaElement> buildMeta(final String name, final Object value) {
        if (value instanceof String) {
            final MetaElement meta = new MetaElement();
            meta.setName(name);
            meta.setValue((String) value);
            return Collections.singletonList(meta);
        } else if (value instanceof Collection) {
            final Collection<Object> items = CoreUtil.cast(value);
            return items.stream().map(item -> this.buildMeta(name, item)) //
                    .flatMap(Collection::stream) //
                    .collect(Collectors.toList());
        } else if (ScriptValueConversionService.isArray(value)) {
            final Collection<Object> items = ((Bindings) value).values();
            return items.stream().map(item -> this.buildMeta(name, item)) //
                    .flatMap(Collection::stream) //
                    .collect(Collectors.toList());
        } else if (value instanceof Item) {
            final MetaElement meta = new MetaElement();
            meta.setName(name);
            meta.setValue(((Item) value).getAbsolutePath());
            return Collections.singletonList(meta);
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Load meta specification from entry point.
     *
     * @return form meta
     */
    public FormSpecificationMeta meta() {
        if (null == this.rootMetaElement) {
            this.rootMetaElement = this.provider.asBean(NAME_ROOT_META_ELEMENT, FormSpecificationMeta.class);
        }
        return this.rootMetaElement;
    }

    /**
     * Find all documents in specified record data and registers them in meta data.
     * Documents are values of <code>File</code> and inherits.
     *
     * @param record
     *            data context
     * @return true if any document been registered in record meta data
     */
    public boolean addDocumentMeta(final int stepIndex) {
        return this.addDocumentMeta(this.buildEngineContext(stepIndex));
    }

    /**
     * Find all documents in specified record data and registers them in meta data.
     * Documents are values of <code>File</code> and inherits.
     *
     * @param record
     *            data context
     * @return true if any document been registered in record meta data
     */
    public boolean addDocumentMeta(final EngineContext<FormSpecificationData> ctx) {
        try {
            if (null == ctx || null == ctx.getElement()) {
                return false;
            }
        } catch (final TechnicalException ex) {
            return false;
        }

        final List<MetaElement> metas = new ArrayList<>();
        final ElementParser<List<MetaElement>> parser = new ElementParser<>();
        parser.register(DataElement.class, (lst, elm, root) -> {
            try {
                final IValueAdapter<List<Item>> adapter = CoreUtil.cast(ValueAdapterFactory.type(root, (DataElement) elm));
                final List<Item> items = adapter.get(ctx.getProvider(), (DataElement) elm);
                if (null != items) {
                    items.forEach(fileItem -> {
                        final MetaElement meta = new MetaElement();
                        meta.setName("document");
                        meta.setValue(fileItem.getAbsolutePath());
                        lst.add(meta);
                    });
                }
            } catch (final ClassCastException ex) {
                LOGGER.debug("data type is not a file items container : {}", ex.getMessage());
            }
            return lst;
        }).parse(metas, ctx.getElement());

        if (metas.isEmpty()) {
            return false;
        }

        final FormSpecificationMeta specMeta = Optional.ofNullable(this.meta()).orElseGet(FormSpecificationMeta::new);
        final List<MetaElement> specMetas = specMeta.getMetas();
        if (null == specMetas) {
            specMeta.setMetas(metas);
        } else {
            specMetas.addAll(metas);
        }

        this.rootMetaElement = specMeta;
        this.save(Engine.FILENAME_META, specMeta);

        return true;
    }

    /**
     * Remove a resource relative to active step.
     *
     * @param name
     *            resource path
     */
    public void remove(final String name) {
        this.buildEngineContext(this.currentStepPosition()).getProvider().remove(name);
    }

    public EventBus getEventBus() {
        return this.eventBus;
    }

    /**
     * Add event to the listeners.
     *
     * @param events
     *            events to register
     */
    public void addEvent(final Event... events) {
    }

    /**
     * Check if any step has error status.
     *
     * @return true if found a step having ERROR status
     */
    public boolean hasErrorState() {
        return null != this.stepsMgr() && CollectionUtils.isNotEmpty(this.stepsMgr().findAll())
                && this.stepsMgr().findAll().stream().anyMatch(s -> s.getStatus().equals(StepStatusEnum.ERROR.getStatus()));
    }

    /**
     * Remove meta.
     *
     * @param bindings
     *            the values
     */
    public void removeMeta(final List<Map<String, Object>> bindings) {
        final FormSpecificationMeta specMeta = Optional.ofNullable(this.meta()).orElseGet(FormSpecificationMeta::new);
        if (CollectionUtils.isNotEmpty(specMeta.getMetas())) {

            final List<MetaElement> metaRemove = new ArrayList<>();
            bindings.forEach(item -> {
                if (2 == item.size() && item.containsKey("name") && item.containsKey("value")) {
                    metaRemove.addAll(this.buildMeta((String) item.get("name"), item.get("value")));
                }
            });

            metaRemove.forEach(item -> {
                Optional.ofNullable(specMeta.getMetas()).map(m -> m.removeIf(x -> item.getName().equals(x.getName()) && item.getValue().equals(x.getValue())));
                LOGGER.debug("Remove meta with key : '{}' and value : '{}'", item.getName(), item.getValue());
            });

            this.rootMetaElement = specMeta;
            this.save(Engine.FILENAME_META, specMeta);
        }
    }

    /**
     * Return Nash script engine.
     *
     * @return the scriptEngine
     */
    public NashScriptEngine getScriptEngine() {
        if (null == this.scriptEngine) {
            LOGGER.warn("##### INSTANCIATE NEW SCRIPT ENGINE #####");
            this.scriptEngine = this.applicationContext.getBean(NashScriptEngine.class);
        }

        return this.scriptEngine;
    }

    /**
     * Return core Nash engine.
     *
     * @return core Nash engine
     */
    public Engine getEngine() {
        return this.engine;
    }

    /**
     * Return application context.
     *
     * @return application context
     */
    public ApplicationContext getApplicationContext() {
        return this.applicationContext;
    }

}
