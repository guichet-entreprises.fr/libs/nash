/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script;

import java.util.HashMap;
import java.util.Map;

import fr.ge.common.nash.engine.manager.processor.bridge.IBridge;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class ScriptExecutionContext.
 *
 * @author Christian Cougourdan
 */
public class ScriptExecutionContext {

    /** The api. */
    private final Map<String, Object> api = new HashMap<>();

    /** The model. */
    private final Map<String, Object> model = new HashMap<>();

    /** The extra. */
    private final Map<String, Object> extra = new HashMap<>();

    /**
     * Gets the api.
     *
     * @return the api
     */
    public Map<String, Object> getApi() {
        return this.api;
    }

    /**
     * Adds the bridge.
     *
     * @param bridge
     *            the bridge
     * @return the script execution context
     */
    public ScriptExecutionContext addBridge(final IBridge bridge) {
        return this.addApi(bridge.getName(), bridge);
    }

    /**
     * Adds the api.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @return the script execution context
     */
    public ScriptExecutionContext addApi(final String name, final Object value) {
        this.api.put(name, value);
        return this;
    }

    /**
     * Gets the model.
     *
     * @return the model
     */
    public Map<String, Object> getModel() {
        return this.model;
    }

    /**
     * Adds the.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @return the script execution context
     */
    public ScriptExecutionContext add(final String name, final Object value) {
        this.model.put(name, value);
        return this;
    }

    public ScriptExecutionContext addAll(final Map<String, Object> model) {
        if (null != model) {
            this.model.putAll(model);
        }
        return this;
    }

    public ScriptExecutionContext merge(final String name, final Object model) {
        final Object previousValue = this.model.get(name);
        if (null == previousValue || !(model instanceof Map)) {
            this.model.put(name, model);
        } else if (previousValue instanceof Map) {
            final Map<String, Object> previousModel = CoreUtil.cast(previousValue);
            previousModel.putAll(deepMerge(CoreUtil.cast(previousValue), CoreUtil.cast(model)));
        } else {
            this.model.put(name, model);
        }

        return this;
    }

    @SuppressWarnings("unchecked")
    private static Map<String, Object> deepMerge(final Map<String, Object> original, final Map<String, Object> newMap) {
        newMap.forEach((key, value) -> {
            if (newMap.get(key) instanceof Map && original.get(key) instanceof Map) {
                final Map<String, Object> originalChild = (Map<String, Object>) original.get(key);
                final Map<String, Object> newChild = (Map<String, Object>) newMap.get(key);
                original.put(key, deepMerge(originalChild, newChild));
            } else {
                original.put(key, newMap.get(key));
            }
        });
        return original;
    }

    /**
     * Gets the extra.
     *
     * @return the extra
     */
    public Map<String, Object> getExtra() {
        return this.extra;
    }

    /**
     * Adds the extra.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @return the script execution context
     */
    public ScriptExecutionContext addExtra(final String name, final Object value) {
        this.extra.put(name, value);
        return this;
    }

}
