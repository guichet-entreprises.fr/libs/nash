package fr.ge.common.nash.engine.adapter.v1_2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueTransform;
import fr.ge.common.nash.engine.adapter.v1_2.value.ButtonValue;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ITypedValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;

/**
 * Widget able to display a simple button. Value represents button description,
 * like color, icon and label.
 *
 * @author Christian Cougourdan
 */
public class ButtonValueAdapter extends AbstractValueAdapter<ButtonValue> {

    @Override
    public String name() {
        return "Button";
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public String template() {
        return ButtonValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    @Override
    protected ButtonValue fromValueElement(final ValueElement value) {
        return ValueTransform.to(ButtonValue.class, value);
    }

    @Override
    protected ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        if (null == value) {
            final ValueElement btn = dataElement.getValue();
            if (null == btn) {
                throw new IllegalArgumentException("Value can't be reset");
            } else {
                return btn;
            }
        } else if (value instanceof FormContentData) {
            final ValueElementBuilder builder = new ValueElementBuilder();
            builder.addTextValue("value", ((FormContentData) value).withPath("value").asBoolean() ? "yes" : "no");
            dataElement.setValue(builder.build());
            throw new IllegalArgumentException();
            // return dataElement.getValue();
        } else {
            final ValueElement btn = ValueTransform.from(ButtonValue.class, value);
            final ListValueElement lst = Optional.ofNullable(dataElement.getValue()) //
                    .map(ValueElement::getContent) //
                    .filter(v -> v instanceof ListValueElement) //
                    .map(ListValueElement.class::cast) //
                    .orElse(null);

            if (null != lst && lst.getElements().size() == 1) {
                final ITypedValueElement first = lst.getElements().get(0);
                if ("value".equals(first.getId())) {
                    final Map<String, ITypedValueElement> m = Optional.ofNullable(btn) //
                            .map(ValueElement::getContent) //
                            .filter(v -> v instanceof ListValueElement) //
                            .map(ListValueElement.class::cast) //
                            .map(ListValueElement::asMap) //
                            .orElse(Collections.emptyMap());

                    m.put("value", first);
                    final List<ITypedValueElement> newLst = new ArrayList<>(m.values());
                    btn.setContent(newLst);
                }
            }

            return btn;
        }
    }

    @Override
    public boolean hasOwnUpdate() {
        return true;
    }

    @Override
    public ButtonValue getDefaultValue(final DataElement elm) {
        if (null == elm) {
            return null;
        } else {
            return this.fromValueElement(elm.getValue());
        }
    }

}
