/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.referential;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * Referential "value" element.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "text")
@XmlAccessorType(XmlAccessType.NONE)
public class ReferentialTextElement implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @XmlAttribute(name = "id")
    private String id;

    /** The icon. */
    @XmlAttribute(name = "icon")
    private String icon;

    /** The label. */
    @XmlValue
    private String label;

    /** The rule as attr. */
    @XmlAttribute(name = "if")
    private String ruleAsAttr;

    /** The translation. */
    @XmlTransient
    private String translation;

    /** The display conditions. */
    @XmlTransient
    private List<String> displayConditions;

    /**
     * Constructor.
     */
    public ReferentialTextElement() {
        // Nothing to do.
    }

    /**
     * Constructor.
     *
     * @param id
     *            the id
     * @param label
     *            the label
     */
    public ReferentialTextElement(final String id, final String label) {
        this.id = id;
        this.label = label;
    }

    /**
     * Constructor.
     *
     * @param id
     *            the id
     * @param label
     *            the label
     * @param icon
     *            the icon
     */
    public ReferentialTextElement(final String id, final String label, final String icon) {
        this.id = id;
        this.label = label;
        this.icon = icon;
    }

    /**
     * Getter on attribute {@link #id}.
     *
     * @return String id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Setter on attribute {@link #id}.
     *
     * @param id
     *            the new value of attribute id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Getter on attribute {@link #icon}.
     *
     * @return String icon
     */
    public String getIcon() {
        return this.icon;
    }

    /**
     * Setter on attribute {@link #icon}.
     *
     * @param icon
     *            the new value of attribute icon
     */
    public void setIcon(final String icon) {
        this.icon = icon;
    }

    /**
     * Getter on attribute {@link #label}.
     *
     * @return String label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Setter on attribute {@link #label}.
     *
     * @param label
     *            the new value of attribute label
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Getter on attribute {@link #ruleAsAttr}.
     *
     * @return String ruleAsAttr
     */
    public String getRuleAsAttr() {
        return this.ruleAsAttr;
    }

    /**
     * Setter on attribute {@link #ruleAsAttr}.
     *
     * @param ruleAsAttr
     *            the new value of attribute ruleAsAttr
     */
    public void setRuleAsAttr(final String ruleAsAttr) {
        this.ruleAsAttr = ruleAsAttr;
    }

    /**
     * Gets the display condition.
     *
     * @return the display condition
     */
    public List<String> getDisplayConditions() {
        return this.displayConditions;
    }

    /**
     * Sets the display condition.
     *
     * @param displayConditions
     *            the new display condition
     * @return the referential text element
     */
    public ReferentialTextElement setDisplayCondition(final List<String> displayConditions) {
        this.displayConditions = Optional.ofNullable(displayConditions).map(lst -> new ArrayList<>(lst)).orElse(null);
        return this;
    }

    /**
     * Adds the display condition.
     *
     * @param condition
     *            the condition
     * @return the referential text element
     */
    public ReferentialTextElement addDisplayCondition(final String condition) {
        if (null == this.displayConditions) {
            this.displayConditions = new ArrayList<>();
        }

        this.displayConditions.add(condition);

        return this;
    }

    /**
     * Adds the display conditions.
     *
     * @param conditions
     *            the conditions
     * @return the referential text element
     */
    public ReferentialTextElement addDisplayConditions(final List<String> conditions) {
        if (null == this.displayConditions) {
            this.displayConditions = new ArrayList<>();
        }

        this.displayConditions.addAll(conditions);

        return this;
    }

    /**
     * Getter on attribute {@link #translation}.
     *
     * @return String translation
     */
    public String getTranslation() {
        return this.translation;
    }

    /**
     * Setter on attribute {@link #translation}.
     *
     * @param translation
     *            the new value of attribute translation
     * @return the referential text element
     */
    public ReferentialTextElement setTranslation(final String translation) {
        this.translation = translation;
        return this;
    }

}
