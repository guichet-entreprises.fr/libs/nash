/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.parser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import fr.ge.common.nash.core.bean.ElementPath.Token;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.utils.CoreUtil;

/**
 * Document tree parser, managing script execution context especially group
 * model.
 */
public class TreeParserWithContext {

    /** The grp action. */
    BiConsumer<IElement<?>, ScriptExecutionContext> grpAction;

    /** The fld action. */
    BiConsumer<IElement<?>, ScriptExecutionContext> fldAction;

    /**
     * Instantiates a new tree parser with context.
     *
     * @param grpAction
     *            the grp action
     * @param fldAction
     *            the fld action
     */
    private TreeParserWithContext(final BiConsumer<IElement<?>, ScriptExecutionContext> grpAction, final BiConsumer<IElement<?>, ScriptExecutionContext> fldAction) {
        this.grpAction = grpAction;
        this.fldAction = fldAction;
    }

    /**
     * Creates the.
     *
     * @param grpAction
     *            the grp action
     * @param fldAction
     *            the fld action
     * @return the tree parser with context
     */
    public static TreeParserWithContext create(final BiConsumer<IElement<?>, ScriptExecutionContext> grpAction, final BiConsumer<IElement<?>, ScriptExecutionContext> fldAction) {
        return new TreeParserWithContext(grpAction, fldAction);
    }

    /**
     * Parses tree document with empty execution context model.
     *
     * @param grp
     *            the grp
     * @return the map
     */
    public Map<String, Object> parse(final GroupElement grp) {
        return this.parse(grp, new ScriptExecutionContext());
    }

    /**
     * Parses tree document with filled execution context model.
     *
     * @param grp
     *            the grp
     * @param model
     *            the model
     * @return the map
     */
    public Map<String, Object> parse(final GroupElement grp, final Map<String, Object> model) {
        return this.parse(grp, new ScriptExecutionContext().addAll(model));
    }

    /**
     * Parses the.
     *
     * @param grp
     *            the grp
     * @param ctx
     *            the ctx
     * @return the map
     */
    public Map<String, Object> parse(final GroupElement grp, final ScriptExecutionContext ctx) {
        final Map<String, Object> oldGroupModel = CoreUtil.cast(ctx.getModel().get(Scopes.GROUP.toContextKey()));
        final Map<String, Object> localModel = this.getLocalModel(grp, CoreUtil.coalesce(() -> oldGroupModel, () -> CoreUtil.cast(ctx.getModel().get(Scopes.STEP.toContextKey()))));

        this.grpAction.accept(grp, ctx);

        if (null != grp.getData()) {
            ctx.add(Scopes.GROUP.toContextKey(), localModel);
            grp.getData().forEach(elm -> {
                if (elm instanceof GroupElement) {
                    this.parse((GroupElement) elm, ctx);
                } else {
                    this.fldAction.accept(elm, ctx);
                }
            });
        }

        ctx.add(Scopes.GROUP.toContextKey(), oldGroupModel);

        return localModel;
    }

    /**
     * Gets the local model from parent one.
     *
     * @param elm
     *            the elm
     * @param parent
     *            the parent
     * @return the local model
     */
    private Map<String, Object> getLocalModel(final IElement<?> elm, final Map<String, Object> parent) {
        final Token token = Token.create(elm.getId());
        final Object localModelAsObject = Optional.ofNullable(parent).map(node -> node.get(token.getKey())).orElse(null);

        if (null != localModelAsObject) {
            if (elm.isRepeatable()) {
                final List<Map<String, Object>> localModelAsList = CoreUtil.cast(localModelAsObject);
                final int idx = null == token.getIndex() ? 0 : token.getIndex();
                if (idx < localModelAsList.size()) {
                    return localModelAsList.get(idx);
                }
            } else {
                return CoreUtil.cast(localModelAsObject);
            }
        }

        return new HashMap<>();
    }

}
