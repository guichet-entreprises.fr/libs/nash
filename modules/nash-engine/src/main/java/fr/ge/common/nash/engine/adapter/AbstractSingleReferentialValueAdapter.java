/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import fr.ge.common.nash.core.function.TriFunction;
import fr.ge.common.nash.engine.adapter.v1_2.value.ReferentialOption;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ITypedValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.nash.engine.referential.ReferentialReader;
import fr.ge.common.utils.CoreUtil;

/**
 * A referential value adapter.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public abstract class AbstractSingleReferentialValueAdapter extends AbstractReferentialValueAdapter<ReferentialOption> {

    /** Converters. */
    private static final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> CONVERTERS;

    static {
        final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> map = new HashMap<>();

        map.put(ReferentialOption.class, (loader, dataElement, value) -> {
            final ReferentialOption option = (ReferentialOption) value;
            return StringUtils.isEmpty(option.getId()) ? null : new ValueElement(new TextValueElement(option.getId(), option.getLabel()));
        });

        map.put(FormContentData.class, (loader, dataElement, value) -> {
            final List<ITypedValueElement> values = transcode(loader, dataElement, (FormContentData) value);

            if (values.isEmpty()) {
                return null;
            } else {
                return new ValueElement(values.get(0));
            }
        });

        map.put(DataBinding.class, (loader, dataElement, value) -> {
            final DataBinding data = (DataBinding) value;
            final String dataId = null != data.withPath("id") ? data.withPath("id").asString() : data.asString();
            final Map<String, String> labels = new ReferentialReader().findLabels(loader, dataElement);
            if (StringUtils.isEmpty(dataId)) {
                return null;
            } else if (labels == null) {
                return new ValueElement(new TextValueElement(dataId, dataId));
            } else {
                return new ValueElement(new TextValueElement(dataId, labels.get(dataId)));
            }
        });

        map.put(HashMap.class, (loader, dataElement, value) -> {
            final Map<String, String> data = CoreUtil.cast(value);
            final String dataId = data.keySet().stream().findFirst().get();
            final Map<String, String> labels = new ReferentialReader().findLabels(loader, dataElement);
            if (StringUtils.isEmpty(dataId)) {
                return null;
            } else if (labels == null) {
                return new ValueElement(new TextValueElement(dataId, dataId));
            } else if (data.keySet().size() == 2 && data.keySet().contains("label")) {
                return new ValueElement(new TextValueElement(data.get("id"), data.get("label")));
            } else {
                return new ValueElement(new TextValueElement(dataId, labels.get(dataId)));
            }

        });

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReferentialOption fromValueElement(final ValueElement value) {
        final TextValueElement text = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof TextValueElement) //
                .map(TextValueElement.class::cast) //
                .orElse(null);

        if (null == text) {
            return null;
        }

        final String id = text.getId();
        final String label = text.getValue();

        return StringUtils.isEmpty(id) ? null : new ReferentialOption(id, label);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TriFunction<SpecificationLoader, DataElement, Object, ValueElement> findConverter(final SpecificationLoader loader, //
            final DataElement dataElement, final Object src) {
        for (final Map.Entry<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> entry : CONVERTERS.entrySet()) {
            if (entry.getKey().isInstance(src)) {
                return entry.getValue();
            }
        }

        return null;
    }

}
