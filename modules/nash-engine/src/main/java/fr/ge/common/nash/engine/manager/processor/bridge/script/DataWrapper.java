/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge.script;

import java.util.Optional;

import javax.script.Bindings;

import org.apache.commons.lang3.StringUtils;
import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.DataModelUpdater;
import fr.ge.common.nash.engine.manager.binding.ScriptDataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DefaultElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.ScriptValueConversionService;

/**
 * @author Christian Cougourdan
 *
 */
public class DataWrapper {

    private final EngineContext<FormSpecificationData> context;

    private boolean dirty = false;

    public DataWrapper(final EngineContext<FormSpecificationData> context) {
        this.context = context;
    }

    public Object get(final String absolutePath) throws FunctionalException {
        final IElement<?> node = SpecificationLoader.find(this.context.getElement(), absolutePath);

        if (null == node) {
            throw new FunctionalException(String.format("Find data: element %s not found", absolutePath));
        } else if (node instanceof DataElement) {
            final DataElement dataElement = this.registerDefaultType((DataElement) node);
            final IValueAdapter<?> type = ValueAdapterFactory.type(this.context.getRecord(), dataElement);
            return type.get(this.context.getProvider(), dataElement);
        } else {
            throw new FunctionalException(String.format("Find data: element %s is not a data element", absolutePath));
        }
    }

    public DataWrapper set(final String absolutePath, final Object value) throws FunctionalException {
        final IElement<?> node = SpecificationLoader.find(this.context.getElement(), absolutePath);

        if (null == node) {
            throw new FunctionalException(String.format("Data set value : element %s not found", absolutePath));
        } else if (node instanceof DataElement) {
            final DataElement dataElement = this.registerDefaultType((DataElement) node);

            final Object bindValue = value instanceof Bindings && ScriptValueConversionService.isArray(value) ? ((Bindings) value).values() : value;

            final IValueAdapter<?> type = ValueAdapterFactory.type(this.context.getRecord(), dataElement);
            type.set(this.context.getRecord(), this.context.getProvider(), dataElement, bindValue);
        } else {
            throw new FunctionalException(String.format("Data set value : element %s is not a data element", absolutePath));
        }

        this.dirty = true;

        return this;
    }

    private DataElement registerDefaultType(final DataElement dataElement) throws FunctionalException {
        if (StringUtils.isEmpty(dataElement.getType())) {
            dataElement.setType( //
                    Optional.ofNullable(this.context.getElement().getDefault()) //
                            .map(DefaultElement::getData) //
                            .map(DataElement::getType) //
                            .orElseThrow(() -> new FunctionalException(String.format("Field [%s] has no type and no default one defined", dataElement.getPath())) //
                    ) //
            );
        }

        return dataElement;
    }

    public DataWrapper bind(final String absolutePath, final Object value) throws FunctionalException {
        final DataModelUpdater updater = new DataModelUpdater(this.context);
        updater.bind(absolutePath, new ScriptDataBinding(value));

        this.dirty = true;

        return this;
    }

    public boolean isDirty() {
        return this.dirty;
    }

    public FormSpecificationData getData() {
        return this.context.getElement();
    }

}
