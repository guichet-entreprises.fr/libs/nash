/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor.pdf;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;

/**
 * The Class ImagePdfAppender.
 *
 * @author Christian Cougourdan
 */
public class ImagePdfBuilder {

    public boolean accept(final String extension) {
        return Arrays.asList(new String[] { "jpg", "png", "jpeg" }).contains(extension);
    }

    public PDDocument build(Item item) {
        final byte[] asBytes = item.getContent();

        if (null == asBytes) {
            throw new TechnicalException(String.format("Unable to load resource \"%s\"", item));
        }

        final PDDocument doc = new PDDocument();

        try {
            final BufferedImage image = ImageIO.read(new ByteArrayInputStream(asBytes));
            final PDPage page = new PDPage();
            doc.addPage(page);

            final PDImageXObject imageObject = JPEGFactory.createFromImage(doc, image, .95F, 150);

            final PDRectangle portrait = PDRectangle.A4;

            if (imageObject.getWidth() > imageObject.getHeight()) {
                final PDRectangle landscape = new PDRectangle(portrait.getLowerLeftY(), portrait.getLowerLeftX(), portrait.getHeight(), portrait.getWidth());
                page.setMediaBox(landscape);
            } else {
                page.setMediaBox(portrait);
            }

            final PDRectangle displayBox = page.getCropBox();
            final Dimension newSize = fitness(new Dimension(imageObject.getWidth(), imageObject.getHeight()), new Dimension((int) displayBox.getWidth(), (int) displayBox.getHeight()));

            try (PDPageContentStream pageContentStream = new PDPageContentStream(doc, page)) {
                pageContentStream.drawImage( //
                        imageObject, //
                        (float) ((displayBox.getWidth() - newSize.getWidth()) / 2), //
                        (float) ((displayBox.getHeight() - newSize.getHeight()) / 2), //
                        (float) newSize.getWidth(), //
                        (float) newSize.getHeight() //
                );
            }
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Error while appending resource \"%s\"", item), ex);
        }

        return doc;
    }

    /**
     * Fitness.
     *
     * @param source
     *            the source
     * @param box
     *            the box
     * @return the dimension
     */
    private static Dimension fitness(final Dimension source, final Dimension box) {
        final double widthRatio = source.getWidth() / box.getWidth();
        final double heightRatio = source.getHeight() / box.getHeight();
        final double ratio = Math.max(widthRatio, heightRatio);

        return new Dimension((int) (source.getWidth() / ratio), (int) (source.getHeight() / ratio));
    }

}
