/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data;

import java.beans.Transient;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import fr.ge.common.nash.engine.util.TypeDeclaration;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class DataElement.
 *
 * @author atuffrea
 */
@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.NONE)
public class DataElement extends AbstractDisplayableElement<DataElement> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The type. */
    @XmlAttribute
    private String type;

    /** The mandatory. */
    @XmlAttribute
    private Boolean mandatory;

    /** The width. */
    @XmlAttribute(name = "width")
    private String width;

    /** The rule as attr. */
    @XmlAttribute(name = "if")
    private String ruleAsAttr;

    /** The rule as tag. */
    @XmlElement(name = "if")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String ruleAsTag;

    /** The display. */
    @XmlTransient
    private boolean display = true;

    /** The conftype. */
    @XmlElement(name = "conftype")
    private ConftypeElement conftype;

    /** The value. */
    @XmlElement(name = "value")
    private ValueElement value;

    /** The trigger as tag. */
    @XmlElement(name = "trigger")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String triggerAsTag;

    /** The trigger as attribute. */
    @XmlAttribute(name = "trigger")
    private String triggerAsAttr;

    /** The url. */
    @XmlAttribute
    private String url;

    /**
     * Constructor.
     */
    public DataElement() {
        // Nothing to do
    }

    /**
     * Constructor.
     *
     * @param id
     *            the id
     * @param type
     *            the type
     * @param value
     *            the value
     */
    public DataElement(final String id, final String type, final ValueElement value) {
        this.setId(id);
        this.type = type;
        this.value = value;
    }

    /**
     * Gets the type name.
     *
     * @return the type name
     */
    @Transient
    public String getTypeName() {
        return Optional.ofNullable(TypeDeclaration.fromString(this.type)).map(TypeDeclaration::getName).orElse(null);
    }

    /**
     * Sets the type name.
     *
     * @param typeName
     *            the type name
     */
    public void setTypeName(final String typeName) {
        // Nothing to do.
    }

    /**
     * Gets the mandatory.
     *
     * @return the mandatory
     */
    public Boolean getMandatory() {
        return this.mandatory;
    }

    /**
     * Gets the width.
     *
     * @return the width
     */
    public String getWidth() {
        return this.width;
    }

    /**
     * Sets the width.
     *
     * @param width
     *            the width to set
     */
    public void setWidth(final String width) {
        this.width = width;
    }

    /**
     * Gets the rule as attr.
     *
     * @return the rule as attr
     */
    @Override
    public String getRuleAsAttr() {
        return this.ruleAsAttr;
    }

    /**
     * Gets the rule as tag.
     *
     * @return the rule as tag
     */
    @Override
    public String getRuleAsTag() {
        return this.ruleAsTag;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Getter on attribute {@link #conftype}.
     *
     * @return ConftypeElement conftype
     */
    public ConftypeElement getConftype() {
        return this.conftype;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueElement getValue() {
        return this.value;
    }

    /**
     * Checks if is mandatory.
     *
     * @return true, if is mandatory
     */
    public Boolean isMandatory() {
        return null == this.mandatory ? Boolean.FALSE : this.mandatory;
    }

    /**
     * Sets the mandatory.
     *
     * @param mandatory
     *            the new mandatory
     */
    public void setMandatory(final Boolean mandatory) {
        this.mandatory = mandatory;
    }

    /**
     * Sets the rule as attr.
     *
     * @param ruleAsAttr
     *            the new rule as attr
     */
    @Override
    public void setRuleAsAttr(final String ruleAsAttr) {
        this.ruleAsAttr = ruleAsAttr;
    }

    /**
     * Sets the rule as tag.
     *
     * @param ruleAsTag
     *            the new rule as tag
     */
    @Override
    public void setRuleAsTag(final String ruleAsTag) {
        this.ruleAsTag = ruleAsTag;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Setter on attribute {@link #conftype}.
     *
     * @param conftype
     *            the new value of attribute conftype
     */
    public void setConftype(final ConftypeElement conftype) {
        this.conftype = conftype;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    public void setValue(final ValueElement value) {
        this.value = value;
    }

    /**
     * Checks if is display.
     *
     * @return true, if is display
     */
    @Override
    public boolean isDisplay() {
        return this.display;
    }

    /**
     * Sets the display.
     *
     * @param display
     *            the new display
     */
    @Override
    public void setDisplay(final boolean display) {
        this.display = display;
    }

    /**
     * Gets the trigger as attr.
     *
     * @return the trigger as attr
     */
    @Override
    public String getTriggerAsAttr() {
        return this.triggerAsAttr;
    }

    /**
     * Gets the trigger as tag.
     *
     * @return the trigger as tag
     */
    @Override
    public String getTriggerAsTag() {
        return this.triggerAsTag;
    }

    /**
     * Sets the trigger as attr.
     *
     * @param triggerAsAttr
     *            the new trigger as attr
     */
    @Override
    public void setTriggerAsAttr(final String triggerAsAttr) {
        this.triggerAsAttr = triggerAsAttr;
    }

    /**
     * Sets the trigger as tag.
     *
     * @param triggerAsTag
     *            the new trigger as tag
     */
    @Override
    public void setTriggerAsTag(final String triggerAsTag) {
        this.triggerAsTag = triggerAsTag;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("{ %s, \"type\": %s, \"mandatory\": %s, \"if (attr)\": %s, \"if (element)\": %s, \"trigger (attr)\": %s, \"trigger (element)\": %s, \"value\": %s }", //
                super.toString(), //
                null == this.type ? null : '"' + this.type + '"', //
                this.mandatory, //
                Optional.ofNullable(this.ruleAsAttr).map(s -> '"' + s + '"').orElse(null), //
                Optional.ofNullable(this.ruleAsTag).map(s -> '"' + s + '"').orElse(null), //
                Optional.ofNullable(this.triggerAsAttr).map(s -> '"' + s + '"').orElse(null), //
                Optional.ofNullable(this.triggerAsTag).map(s -> '"' + s + '"').orElse(null), //
                this.value //
        );
    }

    /**
     * Gets the trigger.
     *
     * @return the trigger source code.
     */
    @Override
    public String getTrigger() {
        return CoreUtil.coalesce(this::getTriggerAsTag, this::getTriggerAsAttr);
    }

    /**
     * Sets the trigger.
     *
     * @param value
     *            the new trigger
     */
    @Override
    public void setTrigger(final String value) {
        this.setTriggerAsAttr(null);
        this.setTriggerAsTag(value);
    }

    /**
     * Accesseur sur l'attribut {@link #url}.
     *
     * @return String url
     */
    public String getUrl() {
        return this.url;
    }

    /**
     * Mutateur sur l'attribut {@link #url}.
     *
     * @param url
     *            la nouvelle valeur de l'attribut url
     */
    public void setUrl(final String url) {
        this.url = url;
    }
}
