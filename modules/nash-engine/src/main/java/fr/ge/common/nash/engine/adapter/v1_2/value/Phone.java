/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2.value;

import org.apache.commons.lang3.StringUtils;

/**
 * Phone.
 */
public class Phone {

    /** Country. */
    private String country;

    /** E164. */
    private String e164;

    /** International. */
    private String international;

    /**
     * Constructor.
     */
    public Phone() {
        // Nothing to do
    }

    /**
     * Constructor.
     *
     * @param country
     *            the country
     * @param e164
     *            the e164 value
     * @param international
     *            the international value
     */
    public Phone(final String country, final String e164, final String international) {
        this.country = country;
        this.e164 = e164;
        this.international = international;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return StringUtils.isEmpty(this.international) ? StringUtils.EMPTY : this.international;
    }

    /**
     * Getter on attribute {@link #country}.
     *
     * @return String country
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * Setter on attribute {@link #country}.
     *
     * @param country
     *            the new value of attribute country
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * Getter on attribute {@link #e164}.
     *
     * @return String e164
     */
    public String getE164() {
        return this.e164;
    }

    /**
     * Setter on attribute {@link #e164}.
     *
     * @param e164
     *            the new value of attribute e164
     */
    public void setE164(final String e164) {
        this.e164 = e164;
    }

    /**
     * Getter on attribute {@link #international}.
     *
     * @return String international
     */
    public String getInternational() {
        return this.international;
    }

    /**
     * Setter on attribute {@link #international}.
     *
     * @param international
     *            the new value of attribute international
     */
    public void setInternational(final String international) {
        this.international = international;
    }

}
