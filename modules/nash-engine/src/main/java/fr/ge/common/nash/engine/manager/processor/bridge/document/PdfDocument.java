/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge.document;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.Bindings;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDChoice;
import org.apache.pdfbox.pdmodel.interactive.form.PDComboBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDListBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDPushButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDSignatureField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.TextField;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.v1_0.BooleanValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.manager.processor.bridge.document.pdf.PdfBoundingBox;
import fr.ge.common.nash.engine.manager.processor.bridge.document.pdf.PdfFieldChoice;
import fr.ge.common.nash.engine.manager.processor.bridge.document.pdf.PdfFieldDescription;
import fr.ge.common.nash.engine.manager.processor.pdf.ImagePdfBuilder;
import fr.ge.common.nash.engine.script.ScriptValueConversionService;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class PdfDocument.
 *
 * @author Christian Cougourdan
 */
public class PdfDocument implements IDocument<PdfDocument> {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfDocument.class);

    /** The document. */
    private PDDocument document;

    private byte[] documentAsBytes;

    /** The documents to close. */
    private final List<Closeable> documentsToClose = new ArrayList<>();

    /** The documents to append. */
    private final List<Item> documentsToAppend = new ArrayList<>();

    /** The image pdf appender. */
    private final ImagePdfBuilder imagePdfAppender = new ImagePdfBuilder();

    /** The parser. */
    private static final Parser parser = Parser.builder().build();

    /** The renderer. */
    private static final HtmlRenderer renderer = HtmlRenderer.builder().build();

    /**
     * Instantiates a new pdf document.
     */
    public PdfDocument() {
        this(null);

        this.getDocument();
        this.documentAsBytes = this.saveWithoutMerge();
    }

    /**
     * Instantiates a new pdf document.
     *
     * @param resourceAsBytes
     *            the resource as bytes
     */
    public PdfDocument(final byte[] resourceAsBytes) {
        this.documentAsBytes = Optional.ofNullable(resourceAsBytes).map(bytes -> bytes.clone()).orElse(null);
    }

    private PDDocument getDocument() {
        if (null == this.document) {
            if (ArrayUtils.isEmpty(this.documentAsBytes)) {
                this.document = new PDDocument();
                this.document.getDocumentInformation().setAuthor("Nash");
            } else {
                try {
                    this.document = PDDocument.load(this.documentAsBytes);
                } catch (final IOException ex) {
                    LOGGER.error("Unable to load PDF document", ex);
                    throw new TechnicalException("Unable to load PDF document", ex);
                }
            }
        }
        return this.document;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PdfDocument apply(final Map<String, Object> src) {
        final PDAcroForm form = this.getDocument().getDocumentCatalog().getAcroForm();
        if (null != form) {
            final Iterator<Map.Entry<String, Object>> iterator = src.entrySet().stream().filter(entry -> entry.getKey().charAt(0) != '_').iterator();
            while (iterator.hasNext()) {
                final Map.Entry<String, Object> entry = iterator.next();
                final String value = valueAsString(entry.getValue());
                final PDField field = form.getField(entry.getKey());
                if (null != field) {
                    if (field instanceof PDCheckBox) {
                        final boolean booleanValue = new BooleanValueAdapter().fromString(value);
                        try {
                            if (booleanValue) {
                                ((PDCheckBox) field).check();
                            } else {
                                ((PDCheckBox) field).unCheck();
                            }
                        } catch (final IOException ex) {
                            throw new TechnicalException(String.format("Error while setting [%s] checkbox value [%s] : %s", field.getFullyQualifiedName(), value, ex.getMessage()), ex);
                        }
                    } else {
                        try {
                            // -->Remove non printable characters
                            field.setValue(value.replaceAll("\\p{C}", ""));
                        } catch (final IOException ex) {
                            throw new TechnicalException(String.format("Error while setting [%s] field value [%s] : %s", field.getFullyQualifiedName(), value, ex.getMessage()), ex);
                        }
                    }
                    field.setReadOnly(true);
                }
            }

            form.getFields().forEach((field) -> field.setReadOnly(true));

        }

        return this;
    }

    /**
     * Append.
     *
     * @param resourceItems
     *            the resource items
     * @return the pdf document
     */
    public PdfDocument append(final Item... resourceItems) {
        final Pattern patternExtension = Pattern.compile(".*\\.([^.]+)");

        for (final Item resourceItem : resourceItems) {
            final Matcher m = patternExtension.matcher(resourceItem.getLabel());
            if (m.matches() && "pdf".equals(m.group(1).toLowerCase())) {
                this.documentsToAppend.add(resourceItem);
            } else if (this.imagePdfAppender.accept(m.group(1).toLowerCase())) {
                final PDDocument doc = this.imagePdfAppender.build(resourceItem);
                this.documentsToAppend.add(FileValueAdapter.createItem(-1, "page.pdf", "application/pdf", this.save(doc)));
            }
        }

        return this;
    }

    /**
     * Append with adding blank page (optional).
     *
     * @param addBlankPage
     *            flag to add blank page when appending file
     * @param resourceItems
     *            the resource items
     * @return the pdf document
     */
    public PdfDocument append(final boolean addBlankPage, final Item... resourceItems) {
        final Pattern patternExtension = Pattern.compile(".*\\.([^.]+)");

        for (final Item resourceItem : resourceItems) {
            final Matcher m = patternExtension.matcher(resourceItem.getLabel());
            if (m.matches() && "pdf".equals(m.group(1).toLowerCase())) {
                this.appendDocument(addBlankPage, resourceItem);
            } else if (this.imagePdfAppender.accept(m.group(1))) {
                final PDDocument doc = this.imagePdfAppender.build(resourceItem);
                this.appendDocument(addBlankPage, FileValueAdapter.createItem(-1, "page.pdf", "application/pdf", this.save(doc)));
            }
        }

        return this;
    }

    /**
     * Append files into initial document adding blank page.
     *
     * @param addBlankPage
     *            Flag to add blank page for even number of pages
     * @param resourceItem
     *            The resource item
     */
    private void appendDocument(final boolean addBlankPage, final Item resourceItem) {
        /*
         * Adding blank page for the original document if the resource has an
         * even number of pages
         */
        if (addBlankPage && (this.getDocument().getNumberOfPages() % 2 != 0)) {
            this.appendBlankPage(this.getDocument());
            LOGGER.debug("Add a blank page for the original resource");
        }
        try (PDDocument source = PDDocument.load(resourceItem.getContent())) {
            /*
             * Necessary to add a blank page the current resource has a even
             * number of pages. Not necessary to add a blank page if the
             * resource item is the last one.
             */
            if (addBlankPage && (source.getNumberOfPages() % 2 != 0)) {
                this.appendBlankPage(source);
                this.documentsToAppend.add(FileValueAdapter.createItem(resourceItem.getId(), resourceItem.getLabel(), "application/pdf", this.save(source)));
                LOGGER.debug("Add a blank page for the resource {}", resourceItem.getLabel());
            } else {
                this.documentsToAppend.add(resourceItem);
            }
        } catch (final IOException ex) {
            throw new TechnicalException("Error while appending resources", ex);
        }
    }

    /**
     * Merge with adding blank page if necessary.
     *
     * @param addBlankPage
     * @return the pdf document
     */
    private PdfDocument merge(final boolean addBlankPage) {
        if (CollectionUtils.isEmpty(this.documentsToAppend)) {
            return this;
        }

        LOGGER.debug("{} PDF(s) to merge", this.documentsToAppend.size());

        try {
            // final PDFMergerUtility utility = new PDFMergerUtility();
            final PDDocument destination = this.getDocument();

            final List<PDDocument> documents = new ArrayList<>();
            try {

                while (!this.documentsToAppend.isEmpty()) {
                    final Item resourceItem = this.documentsToAppend.remove(0);
                    LOGGER.debug("merge [{}]", resourceItem.getLabel());

                    final PDDocument source = PDDocument.load(resourceItem.getContent());
                    final PDAcroForm form = source.getDocumentCatalog().getAcroForm();

                    if (null != form) {
                        form.flatten();
                    }

                    documents.add(source);
                    // utility.appendDocument(destination, source);
                    source.getPages().forEach(page -> destination.addPage(new PDPage(new COSDictionary(page.getCOSObject()))));
                }
                this.document = null;
                this.documentAsBytes = this.save(destination);
            } finally {
                while (!documents.isEmpty()) {
                    IOUtils.closeQuietly(documents.remove(0));
                }
            }
        } catch (final IOException ex) {
            throw new TechnicalException("Error while appending resources", ex);
        }

        LOGGER.debug("Merge finished with success");
        return this;
    }

    /**
     * Save without merge.
     *
     * @return the byte[]
     */
    private byte[] saveWithoutMerge() {
        if (null != this.document) {
            this.documentAsBytes = this.save(this.document);
            this.document = null;
        }
        return this.documentAsBytes;
    }

    /**
     * Save.
     *
     * @param doc
     *            the doc
     * @return the byte[]
     */
    private byte[] save(final PDDocument doc) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); BufferedOutputStream buffer = new BufferedOutputStream(out)) {
            doc.save(buffer);
            doc.close();
            buffer.flush();
            return out.toByteArray();
        } catch (final Exception ex) {
            LOGGER.warn("Saving PDF document : {}", ex.getMessage(), ex);
            throw new TechnicalException(String.format("Unable to save PDF resource : %s", ex.getMessage()), ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Item save(final String resourceName) {
        this.merge(false);
        return FileValueAdapter.createItem(-1, resourceName, "application/pdf", this.saveWithoutMerge());
    }

    /**
     * Value as string.
     *
     * @param value
     *            the value
     * @return the string
     */
    private static String valueAsString(final Object value) {
        if (null == value) {
            return "";
        } else if (value instanceof List<?>) {
            final List<String> values = new ArrayList<String>();
            ((List<?>) value).forEach(elm -> {
                values.add(valueAsString(elm));
            });
            return StringUtils.join(values.toArray(), ", ");
        } else if (value instanceof Calendar) {
            return new SimpleDateFormat("dd/MM/yyyy").format(((Calendar) value).getTime());
        } else {
            return "" + value;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        if (null != this.document) {
            this.document.close();
            this.document = null;
        }
        for (final Closeable doc : this.documentsToClose) {
            if (null != doc) {
                doc.close();
            }
        }
        this.documentsToClose.clear();
    }

    /**
     * Build a list of phrases to fill document.
     *
     * @param bindingsPhrases
     * @return
     */
    public List<Object> buildPhrases(final Bindings bindingsPhrases) {
        if (null == bindingsPhrases || !ScriptValueConversionService.isArray(bindingsPhrases)) {
            throw new TechnicalException("Cannot fill with empty phrases.");
        }

        final List<Object> bindingsList = CoreUtil.cast(bindingsPhrases.values());
        final List<Object> phrasesResult = new ArrayList<Object>();
        int indexList = 1;
        for (final Object bindings : bindingsList) {
            final Map<String, Object> exigenceMap = CoreUtil.cast(bindings);
            final StringBuffer phraseSb = new StringBuffer();
            Collection<Object> articles = new ArrayList<Object>();
            for (final Entry<String, Object> details : exigenceMap.entrySet()) {
                final String key = details.getKey();
                if ("articles".equals(key)) {
                    articles = ((Bindings) details.getValue()).values();
                    LOGGER.debug("Number of articles : {}", articles.size());
                    final Iterator<Object> iterator = articles.stream().filter(elm -> null != elm).iterator();
                    for (int i = 0; iterator.hasNext(); i++) {
                        final Object elm = iterator.next();
                        indexList += i;
                        phraseSb.append(" ").append(indexList).append(") ").append(elm).append("  \n");
                    }
                } else if ("blocs".equals(key) && !articles.isEmpty()) {
                    indexList += 1;
                    final Collection<Object> blocs = ((Bindings) details.getValue()).values();
                    LOGGER.debug("Number of blocs : {}", blocs.size());
                    final Iterator<Object> iterator = blocs.stream().filter(elm -> null != elm).iterator();
                    while (iterator.hasNext()) {
                        final Object elm = iterator.next();
                        phraseSb.append("  ").append(articles.size()).append(".").append(indexList).append(" ").append(elm).append("  \n");
                    }
                }
            }
            phrasesResult.add(phraseSb.toString());
        }
        LOGGER.debug("Number of phrases : " + phrasesResult.size());
        return phrasesResult;
    }

    /**
     * Fill the document with phrases in a specific zone. If the phrases
     * overflow the area, it will be added into an overflow document.
     *
     * @param zoneId
     *            the document zone identifier
     * @param phrases
     *            the phrases to add
     * @param overflowContent
     *            the overflow document
     * @param overflowZoneId
     *            the overflow zone identifier
     * @return a new PdfDocument object
     */
    public PdfDocument fillOverflow(final String zoneId, final List<Object> phrases, final PdfDocument overflowContent, final String overflowZoneId) {
        if (StringUtils.isEmpty(zoneId)) {
            throw new TechnicalException("Cannot get the zone identifier for document.");
        }
        final byte[] resourceAsBytes = this.saveWithoutMerge();
        if (ArrayUtils.isEmpty(resourceAsBytes)) {
            throw new TechnicalException("Cannot get document resources.");
        }

        try {
            final PdfReader readerDocument = new PdfReader(resourceAsBytes);
            final ByteArrayOutputStream outputDocument = new ByteArrayOutputStream();
            final PdfStamper stamperDocument = new PdfStamper(readerDocument, outputDocument);
            final AcroFields formDocument = stamperDocument.getAcroFields();
            LOGGER.debug("Reading document");

            final Rectangle rectangleZoneId = formDocument.getFieldPositions(zoneId).get(0).position;
            final BaseFont baseFontDocument = this.getFont(formDocument, zoneId);
            LOGGER.info(String.format("Getting field name %s positions before removing it", zoneId));

            // -->Remove field from document
            formDocument.removeField(zoneId);
            LOGGER.debug(String.format("Removing field %s into document", zoneId));

            final PdfContentByte pdfContentByteDocument = stamperDocument.getOverContent(1);
            final ColumnText columTextDocument = new ColumnText(pdfContentByteDocument);
            columTextDocument.setSimpleColumn(rectangleZoneId);
            final List<String> phrasesOverflow = new ArrayList<String>();
            boolean pageOverflow = false;
            boolean needOverflowDocument = false;
            for (final Object item : phrases) {
                final String phrase = (String) item;
                if (needOverflowDocument) {
                    phrasesOverflow.add(phrase);
                } else {
                    final String phraseMarkdown = this.markdownToHtml(phrase);
                    final ElementList list = XMLWorkerHelper.parseToElementList(phraseMarkdown, null);
                    for (final Element element : list) {
                        pageOverflow = this.simulateAddElement(columTextDocument, element);
                        if (pageOverflow) {
                            phrasesOverflow.add(phrase);
                            needOverflowDocument = true;
                            continue;
                        } else {
                            columTextDocument.addElement(element);
                            columTextDocument.go(false);
                        }
                    }
                }
            }
            LOGGER.debug("Adding phrase(s) into document");

            // -->Close mask readers
            outputDocument.close();
            stamperDocument.close();
            readerDocument.close();

            final List<PdfReader> readers = new ArrayList<PdfReader>();
            final PdfReader modelReader = new PdfReader(outputDocument.toByteArray());
            readers.add(modelReader);
            int totalPages = modelReader.getNumberOfPages();

            final ByteArrayOutputStream outputAnnexe = new ByteArrayOutputStream();
            if (!phrasesOverflow.isEmpty()) {
                if (null == overflowContent) {
                    throw new TechnicalException("Cannot get the overflow document.");
                }
                if (StringUtils.isEmpty(overflowZoneId)) {
                    throw new TechnicalException("Cannot get the zone identifier for the overflow document.");
                }

                final byte[] overflowResourceAsBytes = overflowContent.saveWithoutMerge();
                LOGGER.debug(String.format("Need to add %d phrase(s) into overflow file", phrasesOverflow.size()));
                final PdfReader readerOverflow = new PdfReader(overflowResourceAsBytes);
                final PdfStamper stamperOverflow = new PdfStamper(readerOverflow, outputAnnexe);
                final AcroFields formOverflow = stamperOverflow.getAcroFields();

                // final BaseFont baseFontOverflow = this.getFont(formOverflow,
                // overflowZoneId);
                final Rectangle rectangleOverflowZoneId = formOverflow.getFieldPositions(overflowZoneId).get(0).position;

                final ColumnText columnOverflow = new ColumnText(stamperOverflow.getOverContent(1));
                columnOverflow.setSimpleColumn(rectangleOverflowZoneId);

                formOverflow.removeField(overflowZoneId);
                LOGGER.debug(String.format("Removing field %s into overflow document", overflowZoneId));

                phrasesOverflow.forEach((phrase) -> {
                    try {
                        XMLWorkerHelper.parseToElementList(this.markdownToHtml(phrase), null).forEach((element) -> {
                            this.addElement(element, columnOverflow, readerOverflow, stamperOverflow, rectangleOverflowZoneId);
                        });
                    } catch (final IOException ex) {
                        throw new TechnicalException("Error parsing markdown elements", ex);
                    }
                });

                // -->Add ending text for certificate
                // final PdfPTable table = new PdfPTable(1);
                // table.setWidthPercentage(100);
                // final PdfPCell cell = new PdfPCell();
                // final Paragraph paragraph = new Paragraph();
                // paragraph.add(new Chunk("FIN DU CERTIFICAT", new
                // Font(baseFontOverflow)));
                // paragraph.add(new Chunk(" / ", new Font(baseFontOverflow)));
                // paragraph.add(new Chunk("END OF THE CERTIFICATE", new
                // Font(baseFontOverflow, 12.0f, Font.ITALIC)));
                // paragraph.setAlignment(Element.ALIGN_CENTER);
                // cell.setUseAscender(true);
                // cell.addElement(paragraph);
                // table.addCell(cell);
                // addElement(table, columnOverflow, readerOverflow,
                // stamperOverflow, rectangleOverflowZoneId);

                // -->Flatten the overflow file
                outputAnnexe.close();
                stamperOverflow.close();
                readerOverflow.close();

                final PdfReader annexReader = new PdfReader(outputAnnexe.toByteArray());
                readers.add(annexReader);
                totalPages += annexReader.getNumberOfPages();
            }

            final PdfDocument documentMerge = this.mergeFiles(readers, totalPages, baseFontDocument);
            return documentMerge.addFooterNumberOfPages();
        } catch (final Exception ex) {
            throw new TechnicalException(String.format("Unable to fill a document with overflow : %s", ex.getMessage()), ex);
        }
    }

    private void addElement(final Element element, final ColumnText columnText, final PdfReader reader, final PdfStamper stamper, final Rectangle mediabox) {
        PdfImportedPage newPageOverflow = null;
        if (element instanceof ListItem) {
            final ListItem listItems = (ListItem) element;
            listItems.forEach(item -> {
                this.addElement(item, columnText, reader, stamper, mediabox);
            });
        }
        try {
            final boolean pageOverflow = this.simulateAddElement(columnText, element);
            if (pageOverflow) {
                newPageOverflow = this.loadOverflowPage(newPageOverflow, reader, stamper);
                columnText.addElement(element);
                this.addNewOverflowPage(reader, stamper, columnText, newPageOverflow, mediabox);
            } else {
                // -->Adding text into annex file
                columnText.addElement(element);
                columnText.go(false);
            }
        } catch (final DocumentException ex) {
            throw new TechnicalException("Error while adding element ", ex);
        }
    }

    /**
     * Getting imported page.
     *
     * @param page
     * @param reader
     * @param stamper
     * @return
     */
    private PdfImportedPage loadOverflowPage(final PdfImportedPage page, final PdfReader reader, final PdfStamper stamper) {
        if (page == null) {
            return stamper.getImportedPage(reader, 1);
        }
        return page;
    }

    /**
     * Return the font for a field.
     *
     * @param form
     * @param fieldName
     * @return
     * @throws Exception
     */
    private BaseFont getFont(final AcroFields form, final String fieldName) {
        try {
            final PdfDictionary merged = form.getFieldItem(fieldName).getMerged(0);
            final TextField textField = new TextField(null, null, null);
            form.decodeGenericDictionary(merged, textField);
            return textField.getFont();
        } catch (IOException | DocumentException ex) {
            throw new TechnicalException(String.format("Cannot get font for field %s", fieldName), ex);
        }
    }

    /**
     * Adding element from a new overflow page.
     *
     * @param reader
     * @param stamper
     * @param text
     * @param page
     * @param rect
     * @throws DocumentException
     */
    private void addNewOverflowPage(final PdfReader reader, final PdfStamper stamper, final ColumnText text, final PdfImportedPage page, final Rectangle rect) throws DocumentException {
        final Rectangle pageSize = reader.getPageSize(1);
        int newPageNumber = reader.getNumberOfPages();
        do {
            stamper.insertPage(++newPageNumber, pageSize);
            final PdfContentByte newContent = stamper.getOverContent(newPageNumber);
            newContent.addTemplate(page, 0, 0);
            text.setCanvas(newContent);
            text.setSimpleColumn(rect);
            newPageNumber++;
        } while (ColumnText.hasMoreText(text.go(false)));
    }

    /**
     * Returns true if adding the element would overflow the page, and rolls
     * back everything
     */
    private boolean simulateAddElement(final ColumnText columnText, final Element element) {
        try {
            int status;
            final float position = columnText.getYLine();
            columnText.addElement(element);
            status = columnText.go(true);
            columnText.setText(null); // Removes any pending element.
            columnText.setYLine(position); // Come back as before the
                                           // simulation.
            return ColumnText.hasMoreText(status);
        } catch (final DocumentException ex) {
            throw new TechnicalException("Error while simulate adding a new element", ex);
        }
    }

    /**
     * Merges files and a footer with the number of page.
     *
     * @param readers
     * @param totalPages
     * @return
     * @throws Exception
     */
    private PdfDocument mergeFiles(final List<PdfReader> readers, final int totalPages, final BaseFont baseFont) {
        try (ByteArrayOutputStream result = new ByteArrayOutputStream()) {
            final Document doc = new Document();
            LOGGER.info("Merging {} file(s)", readers.size());
            final PdfCopy copy = new PdfCopy(doc, result);
            copy.setMergeFields();
            doc.open();
            for (final PdfReader pdfReader : readers) {
                copy.addDocument(pdfReader);
            }
            doc.close();
            for (final PdfReader reader : readers) {
                reader.close();
            }

            LOGGER.info("Returning a new PdfDocument with merge phrases");
            final byte[] modelBytes = result.toByteArray();
            return new PdfDocument(modelBytes);
        } catch (DocumentException | IOException ex) {
            throw new TechnicalException("Error while merging files with overflow", ex);
        }
    }

    /**
     * Converts markdown to HTML.
     *
     * @param src
     *            the source
     * @return the rendering
     */
    private String markdownToHtml(final String src) {
        if (null == src) {
            return null;
        } else {
            return renderer.render(parser.parse(src));
        }
    }

    /**
     * Add a footer with the number of pages.
     *
     * @return a new PdfDocument object
     */
    private PdfDocument addFooterNumberOfPages() {
        try (ByteArrayOutputStream result = new ByteArrayOutputStream()) {
            final PdfReader reader = new PdfReader(this.saveWithoutMerge());
            final PdfStamper stamper = new PdfStamper(reader, result);
            final int totalNumberOfPages = reader.getNumberOfPages();
            for (int pageNumber = 1; pageNumber <= totalNumberOfPages; pageNumber++) {
                final PdfContentByte pagecontent = stamper.getOverContent(pageNumber);
                final Rectangle mediabox = reader.getPageSize(pageNumber);
                final Phrase footer = new Phrase(String.format("%d/%d", pageNumber, totalNumberOfPages));
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT, footer, mediabox.getWidth() - 30, 10, 0);
            }
            stamper.close();
            reader.close();

            LOGGER.info("Returning a new PdfDocument with pages number");
            final byte[] modelBytes = result.toByteArray();
            return new PdfDocument(modelBytes);
        } catch (DocumentException | IOException ex) {
            throw new TechnicalException("Error while adding footer with nulber of pages", ex);
        }
    }

    /**
     * List document fields.
     *
     * @return document fields
     */
    public Map<String, PdfFieldDescription> getFields() {
        final Map<String, PdfFieldDescription> fields = new LinkedHashMap<>();

        final PDDocument doc = this.getDocument();
        final PDAcroForm form = doc.getDocumentCatalog().getAcroForm();
        if (form != null) {
            final Iterator<PDField> iterator = form.getFieldIterator();
            while (iterator.hasNext()) {
                final PDField field = iterator.next();
                final Function<PDField, PdfFieldDescription> fieldLoader = PDFIELD_LOADER.get(field.getClass());
                if (null == fieldLoader) {
                    continue;
                } else {
                    final PdfFieldDescription nfo = fieldLoader.apply(field);
                    nfo.setDescription(field.getAlternateFieldName()) //
                            .setRequired(field.isRequired());

                    final PDAnnotationWidget widget = field.getWidgets().stream().findFirst().orElse(null);
                    if (null != widget) {
                        nfo.setBoundingBox(PdfBoundingBox.build(widget)) //
                                .setPage(Optional.ofNullable(widget.getPage()).map(doc.getPages()::indexOf).orElse(-1));
                    }
                    fields.put(nfo.getName(), nfo);
                }
            }
        }

        return fields;
    }

    private static final Map<Class<?>, Function<PDField, PdfFieldDescription>> PDFIELD_LOADER;

    static {
        final Map<Class<?>, Function<PDField, PdfFieldDescription>> map = new HashMap<>();

        map.put(PDTextField.class, field -> {
            if (((PDTextField) field).isPassword()) {
                return new PdfFieldDescription(field.getFullyQualifiedName(), "password"); //
            } else {
                return new PdfFieldDescription(field.getFullyQualifiedName(), "text") //
                        .setMulti(((PDTextField) field).isMultiline()) //
                        .setValue(((PDTextField) field).getValue());
            }
        });

        map.put(PDCheckBox.class, field -> {
            final PdfFieldDescription nfo = new PdfFieldDescription(field.getFullyQualifiedName(), "checkbox") //
                    .setMulti(true);

            ((PDButton) field).getOnValues().stream().map(code -> new PdfFieldChoice(code)).forEach(nfo::addChoice);
            return nfo.setValue(((PDButton) field).getValue());
        });

        map.put(PDPushButton.class, field -> {
            final PdfFieldDescription nfo = new PdfFieldDescription(field.getFullyQualifiedName(), "button"); //
            return nfo;
        });

        map.put(PDRadioButton.class, field -> {
            final PdfFieldDescription nfo = new PdfFieldDescription(field.getFullyQualifiedName(), "radio"); //
            ((PDButton) field).getOnValues().stream().map(code -> new PdfFieldChoice(code)).forEach(nfo::addChoice);
            return nfo.setValue(((PDButton) field).getValue());
        });

        map.put(PDListBox.class, field -> {
            final PdfFieldDescription nfo = new PdfFieldDescription(field.getFullyQualifiedName(), "list") //
                    .setMulti(((PDChoice) field).isMultiSelect());

            for (final COSBase opt : ((COSArray) ((PDChoice) field).getCOSObject().getDictionaryObject(COSName.OPT))) {
                final String code = ((COSString) ((COSArray) opt).get(0)).getString();
                final String value = ((COSString) ((COSArray) opt).get(1)).getString();
                nfo.addChoice(new PdfFieldChoice(code).setValue(value));
            }

            return nfo.setValue(((PDChoice) field).getValue());
        });

        map.put(PDComboBox.class, field -> {
            final PdfFieldDescription nfo = new PdfFieldDescription(field.getFullyQualifiedName(), "combo") //
                    .setMulti(((PDChoice) field).isMultiSelect());

            for (final COSBase opt : ((COSArray) ((PDChoice) field).getCOSObject().getDictionaryObject(COSName.OPT))) {
                final String code = ((COSString) ((COSArray) opt).get(0)).getString();
                final String value = ((COSString) ((COSArray) opt).get(1)).getString();
                nfo.addChoice(new PdfFieldChoice(code).setValue(value));
            }

            return nfo.setValue(((PDChoice) field).getValue().stream().findFirst().orElse(null));
        });

        map.put(PDSignatureField.class, field -> {
            final PdfFieldDescription nfo = new PdfFieldDescription(field.getFullyQualifiedName(), "signature"); //
            return nfo;
        });

        PDFIELD_LOADER = Collections.unmodifiableMap(map);
    }

    /**
     * Add a blank page with empty content.
     *
     * @param document
     *            the document to update
     */
    public void appendBlankPage(final PDDocument document) {
        final PDPage page = new PDPage();
        try {
            final PDPageContentStream contentStream = new PDPageContentStream(document, page);

            // Define a text content stream using the selected font, moving the
            // cursor ...
            contentStream.beginText();
            contentStream.endText(); // Make sure that the content stream is
                                     // closed:
            contentStream.close();
            document.addPage(page);
        } catch (final IOException ex) {
            throw new TechnicalException("Error while appending empty page with empty content", ex);
        }
    }

    public Integer getNumberOfPages() {
        return this.document.getNumberOfPages();
    }
}
