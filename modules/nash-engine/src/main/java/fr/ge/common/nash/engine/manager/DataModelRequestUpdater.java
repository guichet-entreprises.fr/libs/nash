/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.manager.extractor.IDataModelExtractor;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.manager.parser.ElementParser;
import fr.ge.common.nash.engine.manager.parser.IElementAction;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.NashScriptEngine;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.script.expression.ScriptExpression;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class DataModelRequestUpdater.
 *
 * @author Christian Cougourdan
 */
public class DataModelRequestUpdater {

    /** The dozer. */
    private static final Mapper DOZER = DozerBeanMapperBuilder.create().withMappingBuilder(new BeanMappingBuilder() {

        @Override
        protected void configure() {
            this.mapping(GroupElement.class, GroupElement.class) //
                    .exclude("data") //
                    .exclude("path");
            this.mapping(DataElement.class, DataElement.class) //
                    .exclude("value") //
                    .exclude("path");
        }

    }).build();

    /** The Constant PATTERN_PREFIX. */
    private static final Pattern PATTERN_PREFIX = Pattern.compile("^([^.]+)(\\.[^.].*)?");

    /** The Constant PAGE_PATH_REPLACEMENT. */
    private static final String PAGE_PATH_REPLACEMENT = String.format("%s$2", Scopes.PAGE.toContextKey());

    /** The engine context. */
    private final EngineContext<FormSpecificationData> engineContext;

    /** The execution context. */
    private ScriptExecutionContext executionContext;

    /** The group model. */
    private Map<String, Object> groupModel;

    /** The extractor. */
    private final IDataModelExtractor modelExtractor;

    /** The path extractor. */
    private ElementParser<Collection<String>> pathExtractor;

    private boolean clearMissingValues = true;

    /**
     * Instantiates a new data model request updater.
     *
     * @param engineContext
     *            the engine context
     */
    public DataModelRequestUpdater(final EngineContext<FormSpecificationData> engineContext) {
        this.engineContext = engineContext;
        this.modelExtractor = RecursiveDataModelExtractor.create(engineContext.getProvider());

        final IElementAction<Collection<String>> pathAction = (lst, elm, spc) -> {
            lst.add(PATTERN_PREFIX.matcher(elm.getPath()).replaceFirst(PAGE_PATH_REPLACEMENT));
            return lst;
        };

        this.pathExtractor = new ElementParser<>();
        this.pathExtractor.register(GroupElement.class, pathAction).register(DataElement.class, pathAction);
    }

    public DataModelRequestUpdater setClearMissingValues(final boolean flag) {
        this.clearMissingValues = flag;
        return this;
    }

    /**
     * Update.
     *
     * @param request
     *            the request
     */
    public void update(final HttpServletRequest request) {
        final String groupId = request.getParameter("group.id");

        if (null == groupId) {
            throw new TechnicalException("Update form record from request : no group ID specified");
        }

        int groupPosition = -1;
        GroupElement groupElement = null;
        if (this.engineContext.getElement().getGroups() != null) {
            groupPosition = indexOf(this.engineContext.getElement().getGroups(), grp -> groupId.equals(grp.getId()));
            if (groupPosition >= 0) {
                groupElement = this.engineContext.getElement().getGroups().get(groupPosition);
            }
        }

        if (null != groupElement) {
            final FormContentData formContentData = FormContentReader.read(request, "success", "group.id");
            this.engineContext.getElement().getGroups().set(groupPosition, this.update(groupElement, formContentData));
        }
    }

    public void update(final FormContentData formContentData) {
        final List<GroupElement> pages = this.engineContext.getElement().getGroups();
        for (int idx = 0; idx < pages.size(); idx++) {
            final GroupElement page = pages.get(idx);
            pages.set(idx, this.update(page, formContentData.withPath(page.getId())));
        }
    }

    private GroupElement update(final GroupElement page, final FormContentData formContentData) {
        final Map<String, Object> globalModel = this.engineContext.buildModel();
        final Map<String, Object> stepModel = CoreUtil.cast(globalModel.get(Scopes.STEP.toContextKey()));
        final Map<String, Object> pageModel = new HashMap<>();
        this.groupModel = pageModel;

        stepModel.put(page.getId(), pageModel);
        globalModel.put(Scopes.PAGE.toContextKey(), pageModel);

        this.executionContext = new ScriptExecutionContext().addAll(globalModel);

        final GroupElement newPage = clone(page, page.getId());
        this.setGroupElementValues(newPage, page, formContentData);

        return newPage;
    }

    /**
     * Sets the element values.
     *
     * @param element
     *            the element
     * @param sourceElement
     *            the source element
     * @param sourceData
     *            the source data
     * @return the object
     */
    private Object setElementValues(final IElement<?> element, final IElement<?> sourceElement, final FormContentData sourceData) {
        if (element instanceof GroupElement) {
            return this.setGroupElementValues((GroupElement) element, (GroupElement) sourceElement, sourceData);
        } else {
            return this.setDataElementValues((DataElement) element, (DataElement) sourceElement, sourceData);
        }
    }

    /**
     * Sets the data element values.
     *
     * @param element
     *            the element
     * @param sourceElement
     *            the source element
     * @param requestData
     *            the request data
     * @return the object
     */
    private Object setDataElementValues(final DataElement element, final DataElement sourceElement, final FormContentData requestData) {
        final IValueAdapter<?> type = ValueAdapterFactory.type(this.engineContext.getElement(), element);
        try {
            return type.set(this.engineContext.getRecord(), this.engineContext.getProvider(), element, requestData);
        } catch (final IllegalArgumentException ex) {
            if (type.hasOwnUpdate()) {
                return type.set(this.engineContext.getRecord(), this.engineContext.getProvider(), element, type.get(this.engineContext.getProvider(), sourceElement));
            } else {
                throw ex;
            }
        }
    }

    /**
     * Sets the group element values.
     *
     * @param element
     *            the element
     * @param sourceElement
     *            the source element
     * @param sourceData
     *            the source data
     * @return the map
     */
    private Map<String, Object> setGroupElementValues(final GroupElement element, final GroupElement sourceElement, final FormContentData sourceData) {
        final Collection<String> childrenIds = new HashSet<>();
        final Collection<String> childrenToKeep = new HashSet<>();

        final List<IElement<?>> sourceChildren = Optional.ofNullable(sourceElement.getData()).orElse(Collections.emptyList());
        element.setData(new ArrayList<>());
        for (int childIndex = 0; childIndex < sourceChildren.size(); childIndex++) {
            final IElement<?> child = sourceChildren.get(childIndex);
            final ElementPath childPath = ElementPath.create(child.getPath());
            if (childrenIds.contains(childPath.getPath())) {
                if (childrenToKeep.contains(childPath.getPath())) {
                    element.getData().add(child);
                }
                continue;
            } else {
                childrenIds.add(childPath.getPath());
            }

            final FormContentData childrenDataSource = sourceData.withPath(childPath.getId());

            if (!this.isDisplayed(child)) {
                element.getData().add(this.setHiddenGroupElementValues(child, childPath));
            } else if (child.isRepeatable()) {
                element.getData().addAll(this.setRepeatableGroupElementValues(child, childPath, childrenDataSource, childrenToKeep));
            } else if (childrenDataSource.asMap().isEmpty() && !this.clearMissingValues) {
                element.getData().add(child);
            } else {
                element.getData().add(this.setSingleGroupElementValues(child, childPath, childrenDataSource));
            }
        }

        return this.groupModel;

    }

    private IElement<?> setHiddenGroupElementValues(final IElement<?> child, final ElementPath childPath) {
        final IElement<?> newChildElement = this.clear(child, childPath);
        final Map<String, Object> childModel = this.modelExtractor.extract(newChildElement);
        this.groupModel.put(newChildElement.getId(), child instanceof DataElement ? childModel.get(child.getId()) : childModel);
        return newChildElement;
    }

    private Collection<IElement<?>> setRepeatableGroupElementValues(final IElement<?> child, final ElementPath path, final FormContentData dataSource, final Collection<String> childrenToKeep) {
        final List<FormContentData> values = dataSource.asList();
        final Collection<Object> childrenModel = new ArrayList<>();
        this.groupModel.put(path.getId(), childrenModel);
        if (CollectionUtils.isEmpty(values)) {
            if (this.clearMissingValues) {
                return Collections.singleton(this.clear(child, path));
            } else {
                childrenToKeep.add(path.getPath());
                return Collections.singleton(child);
            }
        } else {
            final Collection<IElement<?>> newChildren = new ArrayList<>();
            for (int idx = 0; idx < values.size(); idx++) {
                final FormContentData value = values.get(idx);
                final String newChildId = String.format("%s[%d]", path.getId(), idx);
                final IElement<?> newChild = clone(child, newChildId);
                newChildren.add(newChild);

                final IElement<?> childModel = this.clear(child, newChildId);
                if (newChild instanceof GroupElement) {
                    this.goDeeper((store, model) -> childrenModel.add(model), () -> this.setElementValues(newChild, childModel, value));
                } else {
                    childrenModel.add(this.setElementValues(newChild, childModel, value));
                }
            }
            return newChildren;
        }
    }

    private IElement<?> setSingleGroupElementValues(final IElement<?> child, final ElementPath childPath, final FormContentData dataSource) {
        final IElement<?> newChild = clone(child, childPath.getId());

        if (newChild instanceof GroupElement) {
            this.goDeeper((store, model) -> store.put(childPath.getId(), model), () -> this.setElementValues(newChild, child, dataSource));
        } else {
            this.groupModel.put(childPath.getId(), this.setElementValues(newChild, child, dataSource));
        }

        return newChild;
    }

    /**
     * Go deeper.
     *
     * @param store
     *            the store
     * @param supplier
     *            the supplier
     * @return the object
     */
    private Object goDeeper(final BiConsumer<Map<String, Object>, Map<String, Object>> store, final Supplier<? extends Object> supplier) {
        final Map<String, Object> localGroupModel = this.groupModel;
        this.groupModel = new HashMap<>();

        store.accept(localGroupModel, this.groupModel);

        try {
            return supplier.get();
        } finally {
            this.groupModel = localGroupModel;
        }
    }

    /**
     * Clear.
     *
     * @param element
     *            the element
     * @param path
     *            the path
     * @return the i element
     */
    private IElement<?> clear(final IElement<?> element, final ElementPath path) {
        final String newId = element.isRepeatable() ? String.format("%s[0]", path.getId()) : path.getId();
        return this.clear(element, newId);
    }

    /**
     * Clear.
     *
     * @param element
     *            the element
     * @param newId
     *            the new id
     * @return the i element
     */
    private IElement<?> clear(final IElement<?> element, final String newId) {
        final IElement<?> newElement = this.engineContext.getRecord().elements().clear(this.engineContext, element, null);
        newElement.setId(newId);
        newElement.setPath(Optional.ofNullable(newElement.getParentPath()).map(str -> str + '.' + newElement.getId()).orElse(newId));
        return newElement;
    }

    /**
     * Checks if is displayed.
     *
     * @param element
     *            the element
     * @return true, if is displayed
     */
    private boolean isDisplayed(final IElement<?> element) {
        final String displayCondition = element.getDisplayCondition();
        if (StringUtils.isEmpty(displayCondition)) {
            return true;
        }

        final NashScriptEngine scriptEngine = this.engineContext.getRecord().getScriptEngine();
        final ScriptExpression expr = scriptEngine.parse(displayCondition, true);

        return scriptEngine.eval(expr, this.executionContext.add(Scopes.GROUP.toContextKey(), this.groupModel), Boolean.class);
    }

    /**
     * Index of.
     *
     * @param <T>
     *            the generic type
     * @param lst
     *            the lst
     * @param predicate
     *            the predicate
     * @return the int
     */
    private static <T> int indexOf(final List<T> lst, final Predicate<T> predicate) {
        for (int idx = 0; idx < lst.size(); idx++) {
            if (predicate.test(lst.get(idx))) {
                return idx;
            }
        }
        return -1;
    }

    /**
     * Clone.
     *
     * @param <R>
     *            the generic type
     * @param source
     *            the source
     * @param newId
     *            the new id
     * @return the r
     */
    private static <R extends IElement<?>> R clone(final R source, final String newId) {
        final R newElement = CoreUtil.cast(DOZER.map(source, source.getClass()));
        newElement.setId(newId);
        newElement.setPath(Optional.ofNullable(newElement.getParentPath()).map(str -> str + '.' + newId).orElse(newId));
        return newElement;
    }

}
