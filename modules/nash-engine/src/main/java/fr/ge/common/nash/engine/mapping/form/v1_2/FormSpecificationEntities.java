/**
 * 
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * @author ijijon
 *
 */
@XmlRootElement(name = "entities")
@XmlAccessorType(XmlAccessType.NONE)
public class FormSpecificationEntities implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The roles. */
    @XmlElement(name = "entity")
    private List<FormSpecificationEntityRoles> entities = null;

    /**
     * Accesseur sur l'attribut {@link #entities}.
     *
     * @return List<FormSpecificationRole> entities
     */
    public List<FormSpecificationEntityRoles> getEntities() {
        return entities;
    }

    /**
     * Mutateur sur l'attribut {@link #entities}.
     *
     * @param entities
     *            la nouvelle valeur de l'attribut entities
     */
    public void setEntities(List<FormSpecificationEntityRoles> entities) {
        this.entities = entities;
    }

}
