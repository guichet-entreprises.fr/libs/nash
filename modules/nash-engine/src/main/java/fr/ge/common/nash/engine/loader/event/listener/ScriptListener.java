/**
 *
 */
package fr.ge.common.nash.engine.loader.event.listener;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.bean.EventTypeEnum;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.ScriptEvent;
import fr.ge.common.nash.engine.loader.event.Subscribe;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;

/**
 * The script listener used to trigger events for a record.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ScriptListener {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptListener.class);

    @Subscribe
    public void onScriptEvent(final ScriptEvent event) {
        final EventTypeEnum type = event.getType();

        if (null == type) {
            return;
        }

        LOGGER.debug("Looking for {} record script trigger", type);

        final EngineContext<SpecificationLoader> context = event.getLoader().buildEngineContext();
        final Map<String, Object> model = context.buildModel();
        final ScriptExecutionContext ctx = context.buildExpressionContext(model);

        Optional.ofNullable(event.getLoader().description()) //
                .map(desc -> null == desc.getEvents() ? null : desc.getEvents().getElements()) //
                .orElse(new ArrayList<>()) //
                .stream() //
                .filter(eventType -> type.getName().equals(eventType.getType())) //
                .forEach(e -> {
                    final String src = Optional.ofNullable(e.getScript()).filter(StringUtils::isNotEmpty).orElse("<inline>");
                    final String expr = Optional.ofNullable(e.getScript()) //
                            .filter(StringUtils::isNotEmpty) //
                            .map(event.getLoader().getProvider()::asBytes) //
                            .map(bytes -> new String(bytes, StandardCharsets.UTF_8)) //
                            .orElse(e.getValue());

                    if (StringUtils.isNotEmpty(expr)) {
                        LOGGER.debug("Executing {}/{} record script trigger", e.getType(), src);
                        event.getLoader().getScriptEngine().eval(expr, true, ctx);
                    }
                });
    }

}
