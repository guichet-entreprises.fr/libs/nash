/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class AbstractNamedElement.
 *
 * @author Christian Cougourdan
 */
@XmlTransient
@XmlAccessorType(XmlAccessType.NONE)
public abstract class AbstractNamedElement extends AbstractIdentifiedElement {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The label. */
    @XmlAttribute
    private String label;

    /** The description. */
    @XmlElement
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String description;

    /** The help. */
    @XmlElement
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String help;

    /** The warnings. */
    @XmlElement(name = "warning")
    private List<WarningElement> warnings;

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel() {
        return StringUtils.isEmpty(this.label) ? null : this.label;
    }

    /**
     * Sets the label.
     *
     * @param label
     *            the label to set
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the help.
     *
     * @return the help
     */
    public String getHelp() {
        return this.help;
    }

    /**
     * Sets the help.
     *
     * @param help
     *            the help to set
     */
    public void setHelp(final String help) {
        this.help = help;
    }

    /**
     * Gets the warnings.
     *
     * @return the warnings
     */
    public List<WarningElement> getWarnings() {
        return this.warnings;
    }

    /**
     * Sets the warnings.
     *
     * @param warnings
     *            the warnings to set
     */
    public void setWarnings(final List<WarningElement> warnings) {
        this.warnings = Optional.ofNullable(warnings).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format( //
                "%s, \"label\": %s, \"description\": %s, \"help\": %s", //
                super.toString(), //
                null == this.label ? null : '"' + this.label + '"', //
                null == this.description ? null : '"' + this.description + '"', //
                null == this.help ? null : '"' + this.help + '"' //
        );
    }

}
