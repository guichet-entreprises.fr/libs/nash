/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.util;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import fr.ge.common.nash.engine.mapping.form.v1_2.data.ConftypeElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;

/**
 * The type declaration.
 */
public final class TypeDeclaration {

    private static final Logger LOGGER = LoggerFactory.getLogger(TypeDeclaration.class);

    /** Name group. */
    private static final int NAME_GROUP = 1;

    /** Option string group. */
    private static final int OPTION_STRING_GROUP = 2;

    /** The Constant PATTERN. */
    private static final Pattern PATTERN = Pattern.compile("([^(]+)(?:[(](.*)[)])?");

    private static final ObjectReader OBJECT_READER = new ObjectMapper() //
            .configure(Feature.ALLOW_SINGLE_QUOTES, true) //
            .configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true) //
            .reader();

    /** The name. */
    private final String name;

    /** The option string. */
    private String optionString;

    /** The extra options. */
    private Map<String, Object> extraOptions;

    private Map<String, Object> options;

    /**
     * Instantiates a new type declaration.
     *
     * @param name
     *            the name
     * @param optionString
     *            the option string
     */
    private TypeDeclaration(final String name, final String optionString) {
        this.name = name;
        this.optionString = optionString;
    }

    /**
     * From string.
     *
     * @param asString
     *            the as string
     * @return the type declaration
     */
    public static TypeDeclaration fromString(final String asString) {
        if (StringUtils.isEmpty(asString)) {
            return null;
        }

        final Matcher m = PATTERN.matcher(asString);
        if (m.matches()) {
            return new TypeDeclaration(m.group(NAME_GROUP), m.group(OPTION_STRING_GROUP));
        } else {
            return new TypeDeclaration(asString, null);
        }
    }

    /**
     * From element.
     *
     * @param dataElement
     *            the data element
     * @return the type declaration
     */
    public static TypeDeclaration fromElement(final DataElement dataElement) {
        if (null == dataElement) {
            return null;
        }

        final TypeDeclaration type = fromString(dataElement.getType());
        if (null != type) {
            // load confType parameters
            final Map<String, Object> params = new HashMap<>();

            Optional.ofNullable(dataElement.getConftype()) //
                    .map(ConftypeElement::getParameters) //
                    .orElse(Collections.emptyList()) //
                    .forEach(param -> params.put(param.getName(), clean(param.getValue())));

            type.setExtraOptions(params);
        }

        return type;
    }

    private static final Pattern PATTERN_PREFIX = Pattern.compile("^(?:[ \t]*\n)*([ \t]*(?=[^ \t]))", Pattern.MULTILINE);

    private static String clean(final String src) {
        final Matcher m = PATTERN_PREFIX.matcher(src);
        if (m.find()) {
            final String prefix = m.group(1);
            return Pattern.compile("^" + prefix, Pattern.MULTILINE).matcher(src.trim()).replaceAll("");
        } else {
            return src;
        }
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the option string.
     *
     * @return the optionString
     */
    public String getOptionString() {
        return this.optionString;
    }

    /**
     * Sets the option string.
     *
     * @param option
     *            the new option string
     */
    public void setOptionString(final String option) {
        this.optionString = option;
        this.options = null;
    }

    /**
     * Gets the extra options.
     *
     * @return the extraOptions
     */
    public Map<String, Object> getExtraOptions() {
        return this.extraOptions;
    }

    /**
     * Sets the extra options.
     *
     * @param extraOptions
     *            the extraOptions to set
     */
    public void setExtraOptions(final Map<String, Object> extraOptions) {
        this.extraOptions = Optional.ofNullable(extraOptions).filter(MapUtils::isNotEmpty).map(HashMap::new).orElse(null);
        this.options = null;
    }

    public Map<String, Object> getOptions() {
        if (null == this.options) {
            final Map<String, Object> opts = new HashMap<>();

            if (StringUtils.isNotEmpty(this.optionString)) {
                try {
                    opts.putAll(OBJECT_READER.forType(Map.class).readValue("{" + this.optionString + "}"));
                } catch (final IOException ex) {
                    LOGGER.warn("Unable to unmarshal options string {{}}", this.optionString);
                }
            }

            if (null != this.extraOptions) {
                opts.putAll(this.extraOptions);
            }

            this.options = opts;
        }

        return this.options;
    }

}
