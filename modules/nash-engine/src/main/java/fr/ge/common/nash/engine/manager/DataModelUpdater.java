/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.exception.TechnicalException;

/**
 *
 * @author Christian Cougourdan
 */
public class DataModelUpdater {

    /** The dozer. */
    private static final Mapper DOZER = DozerBeanMapperBuilder.create().withMappingBuilder(new BeanMappingBuilder() {

        @Override
        protected void configure() {
            this.mapping(GroupElement.class, GroupElement.class) //
                    .exclude("data") //
                    .exclude("path");
            this.mapping(DataElement.class, DataElement.class) //
                    .exclude("value") //
                    .exclude("path");
        }

    }).build();

    private final EngineContext<FormSpecificationData> context;

    public DataModelUpdater(final EngineContext<FormSpecificationData> context) {
        this.context = context;
    }

    public FormSpecificationData bind(final String absolutePath, final DataBinding binding) throws FunctionalException {
        final IElement<?> node = SpecificationLoader.find(this.context.getElement(), absolutePath);

        if (null == node) {
            throw new FunctionalException(String.format("Data set value : element %s not found", absolutePath));
        }

        if (node instanceof GroupElement) {
            this.bind((GroupElement) node, binding.asMap());
        }

        return this.context.getElement();
    }

    private void bind(final GroupElement root, final Map<String, ? extends DataBinding> bindingMap) {
        final List<IElement<?>> elements = root.getData();
        for (int idx = 0; idx < elements.size(); idx++) {
            final IElement<?> elm = elements.get(idx);
            final ElementPath path = ElementPath.create(elm.getId());
            final DataBinding data = bindingMap.get(path.getId());
            if (null != data) {
                if (elm.isRepeatable()) {
                    /*
                     * Repeatable element, check if data to bind are correct and remove all
                     * occurrences except first one
                     */
                    if (!data.isArray()) {
                        throw new TechnicalException("Expecting array object");
                    }

                    final IElement<?> cleanElm = this.context.getRecord().elements().clear(elm);

                    while (idx < elements.size() && path.getId().equals(ElementPath.create(elements.get(idx).getId()).getId())) {
                        elements.remove(idx);
                    }

                    final List<IElement<?>> newElements = new ArrayList<>();
                    final List<? extends DataBinding> bindings = data.asList();
                    for (int elmIdx = 0; elmIdx < bindings.size(); elmIdx++) {
                        final IElement<?> newElm = clone(cleanElm, String.format("%s[%d]", path.getId(), elmIdx));
                        this.bind(newElm, bindings.get(elmIdx));
                        newElements.add(newElm);
                    }

                    if (newElements.isEmpty()) {
                        elements.add(idx, cleanElm);
                    } else {
                        elements.addAll(idx, newElements);
                        idx += newElements.size() - 1;
                    }
                } else {
                    this.bind(elm, data);
                }
            }
        }
    }

    private void bind(final IElement<?> elm, final DataBinding binding) {
        if (elm instanceof GroupElement) {
            this.bind((GroupElement) elm, binding.asMap());
        } else {
            final DataElement dataElement = (DataElement) elm;
            final IValueAdapter<?> type = ValueAdapterFactory.type(this.context.getElement(), dataElement);
            try {
                type.set(this.context.getRecord(), this.context.getProvider(), dataElement, binding);
            } catch (final IllegalArgumentException ex) {
                throw new RuntimeException("Unable to register element " + elm.getPath() + " value");
            }
        }
    }

    private static IElement<?> clone(final IElement<?> src, final String newId) {
        return clone(src, elm -> {
            elm.setId(newId);
            elm.setPath(elm.getParentPath() + '.' + newId);
        });
    }

    private static IElement<?> clone(final IElement<?> src, final Consumer<IElement<?>> update) {
        final IElement<?> newElm = DOZER.map(src, src.getClass());
        update.accept(newElm);

        if (newElm instanceof GroupElement) {
            final List<IElement<?>> children = new ArrayList<>();
            for (final IElement<?> sub : ((GroupElement) src).getData()) {
                children.add(clone(sub, elm -> elm.setPath(newElm.getPath() + '.' + elm.getId())));
            }
            ((GroupElement) newElm).setData(children);
        } else {
            ((DataElement) newElm).setValue((ValueElement) src.getValue());
        }

        return newElm;
    }

}
