package fr.ge.common.nash.engine.mapping.form.v1_2.description;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * "Steps" element.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "steps")
@XmlAccessorType(XmlAccessType.NONE)
public class StepsElement implements IXml {

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "step")
    private List<StepElement> elements;

    /**
     * Constructor.
     *
     * @param elements
     *            init values
     */
    public StepsElement(final List<StepElement> elements) {
        this.elements = elements;
    }

    /**
     * Gets the elements.
     *
     * @return the elements
     */
    public List<StepElement> getElements() {
        return this.elements;
    }

    /**
     * Sets the elements.
     *
     * @param elements
     *            the elements to set
     */
    public void setElements(final List<StepElement> elements) {
        this.elements = Optional.ofNullable(elements).map(ArrayList::new).orElse(null);
    }

}
