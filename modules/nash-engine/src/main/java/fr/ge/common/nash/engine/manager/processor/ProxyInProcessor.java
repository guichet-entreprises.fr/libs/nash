/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.loader.SpecificationProcesses;
import fr.ge.common.nash.engine.manager.processor.bridge.IBridge;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.util.ElementUtil;

/**
 * The Class ProxyInProcessor.
 *
 * @author Christian Cougourdan
 */
public class ProxyInProcessor extends AbstractDataProcessor<ProxyInProcessor> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ProxyInProcessor.class);

    @Override
    protected IBridge getBridge() {
        return null;
    }

    @Override
    protected boolean validateConditions() {
        final FormSpecificationData proxyResult = this.getInputResourceValue();
        if (null == proxyResult) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String getInputResourceName() {
        final FormSpecificationData spec = SpecificationProcesses.load(this.getContext(), this.getProcess().getInput(), FormSpecificationData.class);

        if (null == spec) {
            LOGGER.info("proxy parameters file not found");
            return null;
        }

        DataElement dataElement;
        try {
            dataElement = ElementUtil.assertDataElement("proxy result file property not found", spec, "params.resultFileName");
        } catch (final AssertionError error) {
            LOGGER.info(error.getMessage());
            return null;
        }

        final IValueAdapter<?> type = ValueAdapterFactory.type(spec, dataElement);
        if (null == type) {
            LOGGER.info("proxy result file property has no type : unable to retrieve its value");
            return null;
        }

        final Object proxyResultResourceName = type.get(dataElement);
        if (null == proxyResultResourceName) {
            LOGGER.info("proxy result file property not specified");
            return null;
        } else if (!(proxyResultResourceName instanceof String)) {
            LOGGER.info("proxy result file property not readable : bad value type");
            return null;
        }

        return (String) proxyResultResourceName;
    }

    protected FormSpecificationData getInputResourceValue() {
        final String proxyResultResourceName = this.getInputResourceName();
        if (null == proxyResultResourceName) {
            return null;
        }

        final FormSpecificationData proxyResult = SpecificationProcesses.load(this.getContext(), proxyResultResourceName, FormSpecificationData.class);
        if (null == proxyResult) {
            LOGGER.info("proxy result file property [{}] not found", proxyResultResourceName);
            return null;
        }

        return proxyResult;
    }

}
