/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The Class ValueAdapterInfo.
 *
 * @author Christian Cougourdan
 */
public class ValueAdapterInfo {

    /** The name. */
    private final String name;

    /** The description. */
    private final String description;

    /** The options. */
    private final List<ValueAdapterOptionInfo> options;

    /**
     * Instantiates a new value adapter info.
     *
     * @param name
     *            the name
     * @param description
     *            the description
     * @param options
     *            the options
     */
    public ValueAdapterInfo(final String name, final String description, final ValueAdapterOptionInfo... options) {
        this(name, description, Arrays.asList(options));
    }

    /**
     * Instantiates a new value adapter info.
     *
     * @param name
     *            the name
     * @param description
     *            the description
     * @param options
     *            the options
     */
    public ValueAdapterInfo(final String name, final String description, final List<ValueAdapterOptionInfo> options) {
        this.name = name;
        this.description = description;
        this.options = Optional.ofNullable(options).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

    /**
     * From object.
     *
     * @param valueAdapter
     *            the value adapter
     * @return the value adapter info
     */
    public static ValueAdapterInfo fromObject(final IValueAdapter<?> valueAdapter) {
        final List<ValueAdapterOptionInfo> options = new ArrayList<>();
        for (Class<?> clazz = valueAdapter.getClass(); IValueAdapter.class.isAssignableFrom(clazz); clazz = clazz.getSuperclass()) {
            options.addAll( //
                    Arrays.stream(clazz.getDeclaredFields()) //
                            .filter(f -> null != f.getAnnotation(Option.class)) //
                            .map(ValueAdapterOptionInfo::fromField) //
                            .collect(Collectors.toList()) //
            );

        }

        return new ValueAdapterInfo(valueAdapter.name(), null, options);
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Gets the options.
     *
     * @return the options
     */
    public List<ValueAdapterOptionInfo> getOptions() {
        return this.options;
    }

    /**
     * The Class ValueAdapterOptionInfo.
     */
    public static class ValueAdapterOptionInfo {

        /** The name. */
        private final String name;

        /** The type. */
        private final Class<?> type;

        /** The description. */
        private final String description;

        /**
         * Instantiates a new value adapter option info.
         *
         * @param name
         *            the name
         * @param type
         *            the type
         * @param description
         *            the description
         */
        public ValueAdapterOptionInfo(final String name, final Class<?> type, final String description) {
            this.name = name;
            this.type = type;
            this.description = description;
        }

        /**
         * From field.
         *
         * @param field
         *            the field
         * @return the value adapter option info
         */
        public static ValueAdapterOptionInfo fromField(final Field field) {
            final Option option = field.getAnnotation(Option.class);

            return new ValueAdapterOptionInfo(field.getName(), field.getType(), null == option ? null : option.description());
        }

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return this.name;
        }

        /**
         * Gets the type.
         *
         * @return the type
         */
        public Class<?> getType() {
            return this.type;
        }

        /**
         * Gets the description.
         *
         * @return the description
         */
        public String getDescription() {
            return this.description;
        }

    }

}
