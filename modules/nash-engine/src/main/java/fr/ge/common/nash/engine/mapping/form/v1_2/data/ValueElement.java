/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections4.CollectionUtils;

import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.DateValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.FileValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ITypedValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;

/**
 * The Class ValueElement.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "value")
@XmlAccessorType(XmlAccessType.NONE)
public class ValueElement {

    /** The content. */
    @XmlElementRefs({ //
            @XmlElementRef(type = FileValueElement.class, name = "file"), //
            @XmlElementRef(type = TextValueElement.class, name = "text"), //
            @XmlElementRef(type = DateValueElement.class, name = "date"), //
            @XmlElementRef(type = ListValueElement.class, name = "list") //
    })
    private List<ITypedValueElement> dataContent;

    /** The string content. */
    @XmlMixed
    private List<String> stringContent;

    /**
     * Instantiates a new value element.
     */
    public ValueElement() {
        // Nothing to do
    }

    /**
     * Instantiates a new value element.
     *
     * @param content
     *            the content
     */
    public ValueElement(final String content) {
        this.setContent(content);
    }

    /**
     * Instantiates a new value element.
     *
     * @param content
     *            the content
     */
    public ValueElement(final ITypedValueElement content) {
        this.setContent(content);
    }

    /**
     * Instantie un nouveau value element.
     *
     * @param content
     *            content
     */
    public ValueElement(final List<ITypedValueElement> content) {
        this.setContent(CollectionUtils.isEmpty(content) ? null : content);
    }

    /**
     * Gets the content.
     *
     * @return the content
     */
    public Object getContent() {
        final Object content = Optional.ofNullable(this.dataContent).map(lst -> lst.stream().findFirst().orElse(null)).orElse(null);
        if (null == content) {
            return Optional.ofNullable(this.stringContent).map(l -> (Object) l.stream().collect(Collectors.joining(""))).orElse(null);
        } else {
            return content;
        }
    }

    /**
     * Sets the content.
     *
     * @param content
     *            the content to set
     */
    public final void setContent(final String content) {
        this.stringContent = Arrays.asList(content);
        this.dataContent = null;
    }

    /**
     * Setter on attribute {@link #stringContent} and {@link #dataContent}.
     *
     * @param content
     *            the new value of attribute content
     */
    public void setContent(final ITypedValueElement content) {
        this.stringContent = null;
        this.dataContent = Arrays.asList(content);
    }

    /**
     * Sets the content.
     *
     * @param content
     *            the new content
     */
    public final void setContent(final List<ITypedValueElement> content) {
        this.stringContent = null;
        this.dataContent = Arrays.asList(new ListValueElement(content));
    }

    /**
     * After unmarshal.
     *
     * @param unmarshaller
     *            unmarshaller
     * @param parent
     *            parent
     */
    public void afterUnmarshal(final Unmarshaller unmarshaller, final Object parent) {
        if (!CollectionUtils.isEmpty(this.dataContent)) {
            this.stringContent = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final String contentAsJson;
        if (this.dataContent == null) {
            if (this.stringContent == null) {
                contentAsJson = "null";
            } else {
                contentAsJson = '"' + this.stringContent.stream().collect(Collectors.joining()) + '"';
            }
        } else {
            contentAsJson = this.dataContent.toString();
        }

        return contentAsJson;
    }

}
