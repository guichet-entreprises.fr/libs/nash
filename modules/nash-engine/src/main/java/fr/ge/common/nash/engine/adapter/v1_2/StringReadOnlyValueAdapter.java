/**
 * 
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;

/**
 * The Class StringReadOnlyValueAdapter.
 *
 * @author bsadil
 */
public class StringReadOnlyValueAdapter extends AbstractValueAdapter<String> {

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {

        return "StringReadOnly";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This data type allows to display value on read only.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return StringReadOnlyValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String fromString(final String value) {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        return (String) value;
    }

}
