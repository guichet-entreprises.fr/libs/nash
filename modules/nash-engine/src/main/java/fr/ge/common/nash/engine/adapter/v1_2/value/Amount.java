/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2.value;

/**
 * Amount.
 */
public class Amount {

    /** Amount. */
    private String amount;

    /** Formated amount. */
    private String monetary;

    /** Currency. */
    private String currency;

    /**
     * Constructor.
     */
    public Amount() {
        // Nothing to do
    }

    /**
     * Constructor.
     *
     * @param amount
     *            the amount value
     * @param monetary
     *            the formated amount value
     * @param currency
     *            the currency value
     */
    public Amount(final String amount, final String monetary, final String currency) {
        this.amount = amount;
        this.monetary = monetary;
        this.currency = currency;
    }

    /**
     * Constructor.
     *
     * @param amount
     *            the amount value
     * @param monetary
     *            the formated amount value
     */
    public Amount(final String monetary, final String currency) {
        this.monetary = monetary;
        this.currency = currency;
    }

    /**
     * Accesseur sur l'attribut {@link #amount}.
     *
     * @return String amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Mutateur sur l'attribut {@link #amount}.
     *
     * @param amount
     *            la nouvelle valeur de l'attribut amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Accesseur sur l'attribut {@link #monetary}.
     *
     * @return String monetary
     */
    public String getMonetary() {
        return monetary;
    }

    /**
     * Mutateur sur l'attribut {@link #monetary}.
     *
     * @param monetary
     *            la nouvelle valeur de l'attribut monetary
     */
    public void setMonetary(String monetary) {
        this.monetary = monetary;
    }

    /**
     * Accesseur sur l'attribut {@link #currency}.
     *
     * @return String currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Mutateur sur l'attribut {@link #currency}.
     *
     * @param currency
     *            la nouvelle valeur de l'attribut currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
