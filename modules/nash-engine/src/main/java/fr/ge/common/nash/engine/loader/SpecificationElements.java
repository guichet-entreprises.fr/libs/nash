/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.FieldsMappingOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DefaultElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.utils.CoreUtil;

/**
 * Record elements manager.
 *
 * @author Christian Cougourdan
 */
public class SpecificationElements {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpecificationElements.class);

    /** The Constant PATTERN_MULTI_INDEX_FINAL. */
    private static final Pattern PATTERN_MULTI_INDEX_FINAL = Pattern.compile("\\[([^\\]]*)\\]$");

    private static final Mapper DOZER = DozerBeanMapperBuilder.create().withMappingBuilder(new BeanMappingBuilder() {

        @Override
        protected void configure() {
            this.mapping(GroupElement.class, GroupElement.class) //
                    .fields("data", "data", //
                            FieldsMappingOptions.hintA(DataElement.class, GroupElement.class), //
                            FieldsMappingOptions.hintB(DataElement.class, GroupElement.class)) //
                    .exclude("path");
            this.mapping(DataElement.class, DataElement.class) //
                    .exclude("path");
        }

    }).build();

    /** The loader. */
    private final SpecificationLoader loader;

    /**
     * Instantiates a new specification elements.
     *
     * @param loader
     *            the loader
     */
    SpecificationElements(final SpecificationLoader loader) {
        this.loader = loader;
    }

    /**
     * Clear specified element, ie remove all value and reduce multiple occurrence
     * elements to one.
     *
     * @param element
     *            element to clear
     * @return cleared element
     */
    public IElement<?> clear(final IElement<?> element) {
        return this.clear(null, (DataElement) null, element, null);
    }

    /**
     * Clear specified element, ie remove all value and reduce multiple occurrence
     * elements to one.
     *
     * @param spec
     *            {@link FormSpecificationData}, used for default attributes
     * @param element
     *            element to clear
     * @param model
     *            script evaluation context
     * @return cleared element
     */
    public IElement<?> clear(final FormSpecificationData spec, final IElement<?> element, final Map<String, Object> model) {
        return this.clear( //
                Optional.ofNullable(spec).map(FormSpecificationData::getResourceName).map(this.loader::provider).orElse(null), //
                Optional.ofNullable(spec).map(FormSpecificationData::getDefault).map(DefaultElement::getData).orElse(null), //
                element, //
                model //
        );
    }

    /**
     * Clear specified element, ie remove all value and reduce multiple occurrence
     * elements to one.
     *
     * @param engineContext
     *            {@link EngineContext}, used for default attributes and provider
     * @param element
     *            element to clear
     * @param model
     *            script evaluation context
     * @return cleared element
     */
    public IElement<?> clear(final EngineContext<FormSpecificationData> engineContext, final IElement<?> element, final Map<String, Object> model) {
        return this.clear( //
                Optional.ofNullable(engineContext).map(EngineContext::getProvider).orElse(null), //
                Optional.ofNullable(engineContext).map(EngineContext::getElement).map(FormSpecificationData::getDefault).map(DefaultElement::getData).orElse(null), //
                element, //
                model //
        );
    }

    /**
     * Build a clean (empty) element from specified one.
     *
     * @param provider
     *            provider
     * @param defaultDataElement
     *            step default attributes elements
     * @param element
     *            element to clear
     * @param model
     *            model values used to obtain occurrences values.
     * @return cleared element
     */
    private IElement<?> clear(final IProvider provider, final DataElement defaultDataElement, final IElement<?> element, final Map<String, Object> model) {
        if (element instanceof DataElement) {

            final DataElement dataElement = (DataElement) element;
            dataElement.setId(this.cleanElementPath(element.getId()));
            dataElement.setPath(this.cleanElementPath(element.getPath()));
            final String typeAsString = Optional.ofNullable(dataElement.getType()).orElse(null == defaultDataElement ? null : defaultDataElement.getType());
            if (null == typeAsString) {
                dataElement.setValue(null);
            } else {
                final IValueAdapter<?> valueAdapter = ValueAdapterFactory.type(this.loader, typeAsString);
                valueAdapter.set(this.loader, provider, dataElement, valueAdapter.getDefaultValue(dataElement));
            }

        } else if (element instanceof GroupElement) {

            final GroupElement groupElement = (GroupElement) element;
            groupElement.setId(this.cleanElementPath(element.getId()));
            final List<IElement<?>> newSubElements = this.clearSubElements(provider, defaultDataElement, groupElement, model);
            groupElement.setData(newSubElements);

        }

        return element;
    }

    /**
     * Build clean (empty) elements from specified parent one.
     *
     * @param provider
     *            provider
     * @param defaultDataElement
     *            step default attributes elements
     * @param groupElement
     *            parent group of elements to clear
     * @param model
     *            model values used to obtain occurrences values.
     * @return cleared elements collection
     */
    private List<IElement<?>> clearSubElements(final IProvider provider, final DataElement defaultDataElement, final GroupElement groupElement, final Map<String, Object> model) {
        final List<IElement<?>> newSubElements = new ArrayList<>();
        final List<String> processedElementIds = new ArrayList<>();

        for (final IElement<?> subElement : groupElement.getData()) {
            final String cleanId = this.cleanElementPath(subElement.getId());
            if (!processedElementIds.contains(cleanId)) {
                processedElementIds.add(cleanId);
                final IElement<?> clearedSubElement = this.clear(provider, defaultDataElement, subElement, model);
                int minOccurs = 1;
                if (clearedSubElement.isRepeatable() && !StringUtils.isEmpty(clearedSubElement.getMinOccurs())) {
                    minOccurs = this.loader.getScriptEngine().eval(clearedSubElement.getMinOccurs(), false, new ScriptExecutionContext().addAll(model), int.class);
                }

                if (minOccurs < 1) {
                    minOccurs = 1;
                }

                final PathToken token = PathToken.build(clearedSubElement.getId());
                if (minOccurs > 1) {
                    for (int idx = 0; idx < minOccurs; idx++) {
                        final IElement<?> newSubElement = this.duplicate(token.toString(idx), clearedSubElement);
                        newSubElements.add(newSubElement);
                    }
                } else {
                    clearedSubElement.setId(token.toString(0));
                    newSubElements.add(clearedSubElement);
                }
            }
        }

        return newSubElements;
    }

    /**
     * Duplicate specified element and register new ID.
     *
     * @param newId
     *            overrided ID
     * @param src
     *            element to duplicate
     * @return new element
     */
    public <R extends IElement<?>> R duplicate(final String newId, final R src) {
        final R dst = CoreUtil.cast(DOZER.map(src, src.getClass()));
        dst.setId(newId);
        return dst;
    }

    /**
     * Remove index for multiple occurrence ID. If no index is specified, return
     * unmodified ID.
     *
     * @param id
     *            ID to clean
     * @return cleaned ID
     */
    public String cleanElementPath(final String id) {
        return PATTERN_MULTI_INDEX_FINAL.matcher(id).replaceFirst("[]");
    }

    /**
     * Search for element by its path in specified root elements list.
     *
     * @param src
     *            root elements
     * @param absolutePath
     *            element absolute path
     * @return found element
     */
    public static IElement<?> find(final List<? extends IElement<?>> src, final String absolutePath) {
        if (null == src) {
            return null;
        }

        final PathToken token = PathToken.build(absolutePath);
        if (null == token) {
            return null;
        }

        final IElement<?> found = src.stream().filter(elm -> token.matches(elm.getId())).findFirst().orElse(null);

        if (null == found || null == token.tail) {
            // if the absolute path is like "generated" or "cerfa14004", try to
            // find and return the element which id matches at the root of src
            // list
            return found;
        } else {
            // if the absolute path is like "generated.cerfa14004", try to find
            // and return the element which id matches in "generated" data sub
            // list of src list
            return find(found.getData(), token.tail);
        }
    }

    // /**
    // * Find.
    // *
    // * @param src
    // * the src
    // * @param absolutePath
    // * the absolute path
    // * @return the i element
    // */
    // public static IElement<?> setGroupIndex(final List<? extends IElement<?>>
    // src, final String absolutePath) {
    // if (null == src) {
    // return null;
    // }
    //
    // final PathToken token = PathToken.build(absolutePath);
    // if (null == Strin) {
    // return null;
    // }
    //
    // final IElement<?> found = src.stream().filter(elm ->
    // token.matches(elm.getId())).findFirst().orElse(null);
    //
    // if (null == found || null == token.tail) {
    // // if the absolute path is like "generated" or "cerfa14004", try to
    // // find and return the element which id matches at the root of src
    // // list
    // return found;
    // } else {
    // // if the absolute path is like "generated.cerfa14004", try to find
    // // and return the element which id matches in "generated" data sub
    // // list of src list
    // return find(found.getData(), token.tail);
    // }
    // }

    /**
     * The Class PathToken.
     */
    private static class PathToken {

        /** The Constant PATTERN_PATH_EXPLODE. */
        private static final Pattern PATTERN_PATH_EXPLODE = Pattern.compile("([^./_\\[]+)(?:\\[([^\\]]*)\\])?(?:\\.(.*))?");

        /** The id. */
        private final String id;

        /** The index. */
        private final String index;

        /** The tail. */
        private final String tail;

        /**
         * Instantiates a new path token.
         *
         * @param id
         *            the id
         * @param index
         *            the index
         * @param tail
         *            the tail
         */
        private PathToken(final String id, final String index, final String tail) {
            this.id = id;
            this.index = null == index ? null : "".equals(index) ? "0" : index;
            this.tail = tail;
        }

        /**
         * Builds the.
         *
         * @param path
         *            the path
         * @return the path token
         */
        public static PathToken build(final String path) {
            final Matcher m = PATTERN_PATH_EXPLODE.matcher(path);

            if (!m.matches()) {
                LOGGER.info("Path doesn't match correct pattern : {}", path);
                return null;
            }

            return new PathToken(m.group(1), m.group(2), m.group(3));
        }

        /**
         * Matches.
         *
         * @param other
         *            the other
         * @return true, if successful
         */
        public boolean matches(final String other) {
            return this.matches(build(other));
        }

        /**
         * Matches.
         *
         * @param other
         *            the other
         * @return true, if successful
         */
        public boolean matches(final PathToken other) {
            if (null != other && this.id.equals(other.id)) {
                if (null == other.index) {
                    return null == this.index;
                } else if ("0".equals(other.index)) {
                    return null == this.index || "0".equals(this.index);
                } else {
                    return other.index.equals(this.index);
                }
            } else {
                return false;
            }
        }

        public String toString(final int newIndex) {
            final StringBuilder path = new StringBuilder(this.id);
            if (null != this.index) {
                path.append('[').append(newIndex).append(']');
            }
            if (null != this.tail) {
                path.append('.').append(this.tail);
            }
            return path.toString();
        }

    }

}
