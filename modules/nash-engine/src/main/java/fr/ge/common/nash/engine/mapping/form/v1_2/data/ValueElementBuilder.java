/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.DateValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.FileValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ITypedValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;

/**
 * Value element builder.
 *
 * @author Christian Cougourdan
 */
public class ValueElementBuilder {

    /** elements. */
    private final List<ITypedValueElement> elements = new ArrayList<>();

    /**
     * Default constructor.
     */
    public ValueElementBuilder() {
        // Nothing to do
    }

    /**
     * Build the {@link ValueElement} instance.
     *
     * @return {@code null} if no elements are presents.
     */
    public ValueElement build() {
        return Optional.ofNullable(this.elements).filter(lst -> !lst.isEmpty()).map(ValueElement::new).orElse(null);
    }

    /**
     * Add a new {@link DateValueElement}, only if id and value are not null and
     * not empty.
     *
     * @param id
     *            date ID
     * @param value
     *            date value
     * @return builder instance
     */
    public ValueElementBuilder addDateValue(final String id, final String value) {
        return this.addDateValue(id, null, value);
    }

    /**
     * Add a new {@link DateValueElement}, only if id and value are not null and
     * not empty.
     *
     * @param id
     *            date ID
     * @param label
     *            human readable date
     * @param value
     *            date value
     * @return builder instance
     */
    public ValueElementBuilder addDateValue(final String id, final String label, final String value) {
        if (!StringUtils.isBlank(id) || !StringUtils.isBlank(value)) {
            this.elements.add(new DateValueElement(StringUtils.isBlank(id) ? null : id, StringUtils.isBlank(label) ? null : label, StringUtils.isBlank(value) ? null : value));
        }
        return this;
    }

    /**
     * Add a new {@link TextValueElement}, only if id and value are not null and
     * not empty.
     *
     * @param id
     *            text ID
     * @param value
     *            text value
     * @return builder instance
     */
    public ValueElementBuilder addTextValue(final String id, final String value) {
        if (!StringUtils.isBlank(id) || !StringUtils.isBlank(value)) {
            this.elements.add(new TextValueElement(StringUtils.isBlank(id) ? null : id, StringUtils.isBlank(value) ? null : value));
        }
        return this;
    }

    /**
     * Add a new {@link FileValueElement}, only if id and value are not null and
     * not empty.
     *
     * @param id
     *            file ID
     * @param value
     *            file label
     * @return builder instance
     */
    public ValueElementBuilder addFileValue(final String id, final String value) {
        if (!StringUtils.isBlank(id) || !StringUtils.isBlank(value)) {
            this.elements.add(new FileValueElement(StringUtils.isBlank(id) ? null : id, StringUtils.isBlank(value) ? null : value));
        }
        return this;
    }

}
