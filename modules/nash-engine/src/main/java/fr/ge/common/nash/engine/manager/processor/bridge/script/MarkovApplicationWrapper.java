/**
 * 
 */
package fr.ge.common.nash.engine.manager.processor.bridge.script;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceRequest;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceResponse;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class MarkovApplicationWrapper extends ApplicationWrapper {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MarkovApplicationWrapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public IProvider load(final String baseUrl, final String code) {
        final ServiceResponse response = new ServiceRequest(StringUtils.join(baseUrl, "/v1/queue/", code)).get();
        if (Status.OK.getStatusCode() == response.getStatus()) {
            return new ZipProvider(response.asBytes());
        }

        throw new TechnicalException(String.format("Load markov record %s unsuccessfully", code));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServiceResponse uploadTo(final String baseUrl, final IProvider provider, final List<String> acls) {
        final Map<String, Object> content = new HashMap<>();
        content.put("file", provider.asBytes());
        final ServiceResponse response = new ServiceRequest(StringUtils.join(baseUrl, "/v1/queue/AWAIT")) //
                .dataType("form") //
                .param("overwrite", true) //
                .post(content);
        if (Status.OK.getStatusCode() == response.getStatus()) {
            LOGGER.info("Upload record to markov AWAIT queue successfully");
            return response;
        }

        throw new TechnicalException("Upload record to markov AWAIT queue unsuccessfully");
    }

}
