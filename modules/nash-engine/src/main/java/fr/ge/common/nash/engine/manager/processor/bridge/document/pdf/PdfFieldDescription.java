/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor.bridge.document.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 * PDF field description.
 * 
 * @author Christian Cougourdan
 */
public class PdfFieldDescription {

    /** The name. */
    private final String name;

    /** The type. */
    private final String type;

    /** The page. */
    private int page = -1;

    /** The description. */
    private String description;

    /** The value. */
    private Object value;

    /** The required. */
    private boolean required;

    /** The multiselect. */
    private boolean multi;

    /** The choices. */
    private List<PdfFieldChoice> choices;

    /** The bounding box. */
    private PdfBoundingBox boundingBox;

    /**
     * Instantiates a new pdf field description.
     *
     * @param name
     *            the name
     * @param type
     *            the type
     */
    public PdfFieldDescription(String name, String type) {
        this.name = name;
        this.type = type;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the description
     * @return the pdf field description
     */
    public PdfFieldDescription setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Object getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    public PdfFieldDescription setValue(Object value) {
        this.value = value;
        return this;
    }

    /**
     * Checks if is required.
     *
     * @return true, if is required
     */
    public boolean isRequired() {
        return this.required;
    }

    /**
     * Sets the required.
     *
     * @param required
     *            the required
     * @return the pdf field description
     */
    public PdfFieldDescription setRequired(boolean required) {
        this.required = required;
        return this;
    }

    /**
     * Checks if is multi.
     *
     * @return true, if is multi
     */
    public boolean isMulti() {
        return this.multi;
    }

    /**
     * Sets the multi.
     *
     * @param multi
     *            the multi
     * @return the pdf field description
     */
    public PdfFieldDescription setMulti(boolean multi) {
        this.multi = multi;
        return this;
    }

    /**
     * Gets the page.
     *
     * @return the page
     */
    public int getPage() {
        return this.page;
    }

    /**
     * Sets the page.
     *
     * @param page
     *            the page
     * @return the pdf field description
     */
    public PdfFieldDescription setPage(int page) {
        this.page = page;
        return this;
    }

    /**
     * Gets the bounding box.
     *
     * @return the bounding box
     */
    public PdfBoundingBox getBoundingBox() {
        return this.boundingBox;
    }

    /**
     * Sets the bounding box.
     *
     * @param boundingBox
     *            the bounding box
     * @return the pdf field description
     */
    public PdfFieldDescription setBoundingBox(PdfBoundingBox boundingBox) {
        this.boundingBox = boundingBox;
        return this;
    }

    /**
     * Gets the choices.
     *
     * @return the choices
     */
    public List<PdfFieldChoice> getChoices() {
        return this.choices;
    }

    /**
     * Adds the choice.
     *
     * @param choice
     *            the choice
     * @return the pdf field description
     */
    public PdfFieldDescription addChoice(PdfFieldChoice choice) {
        if (null == this.choices) {
            this.choices = new ArrayList<>();
        }
        this.choices.add(choice);
        return this;
    }

}
