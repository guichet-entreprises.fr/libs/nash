/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2.value;

import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Class DateRange.
 */
public class DateRange {

    /** Date format. */
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    /** The Constant FORMATTER. */
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern(DATE_FORMAT);

    /** from. */
    private Calendar from;

    /** to. */
    private Calendar to;

    /**
     * Instantie un nouveau date range.
     */
    public DateRange() {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        if (this.getFrom() == null || this.getTo() == null) {
            return StringUtils.EMPTY;
        }

        return String.format("%s - %s", new DateTime(this.getFrom()).toString(FORMATTER), new DateTime(this.getTo()).toString(FORMATTER));
    }

    /**
     * Instantie un nouveau date range.
     *
     * @param from
     *            from
     * @param to
     *            to
     */
    public DateRange(final Calendar from, final Calendar to) {
        this.from = from;
        this.to = to;
    }

    /**
     * Getter on attribute {@link #from}.
     *
     * @return the from
     */
    public Calendar getFrom() {
        return this.from;
    }

    /**
     * Setter on attribute {@link #from}.
     *
     * @param from
     *            the from to set
     */
    public void setFrom(final Calendar from) {
        this.from = from;
    }

    /**
     * Getter on attribute {@link #to}.
     *
     * @return the to
     */
    public Calendar getTo() {
        return this.to;
    }

    /**
     * Setter on attribute {@link #to}.
     *
     * @param to
     *            the to to set
     */
    public void setTo(final Calendar to) {
        this.to = to;
    }

}
