/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.script.Bindings;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_2.value.ReferentialOption;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.SpecificationPages;
import fr.ge.common.nash.engine.manager.processor.bridge.IBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.script.DataWrapper;
import fr.ge.common.nash.engine.manager.processor.result.DataProcessResult;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ConfTypeParamElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ConftypeElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ReferentialElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.WarningElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialTextElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.support.dozer.FormSpecificationMappingBuilder;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.nash.engine.util.StylesheetUtil;
import fr.ge.common.nash.engine.validation.Errors;
import fr.ge.common.nash.engine.validation.FormSpecificationDataValidator;
import fr.ge.common.nash.engine.validation.FormSpecificationValidator;
import fr.ge.common.support.i18n.MessageFormatter;

/**
 * The Class AbstractDataProcessor.
 *
 * @author Christian Cougourdan
 * @param <R>
 *            the generic type
 */
public abstract class AbstractDataProcessor<R extends AbstractDataProcessor<R>> extends AbstractProcessor<FormSpecificationData, R> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDataProcessor.class);

    /** The dozer. */
    private static final Mapper DOZER = DozerBeanMapperBuilder.create() //
            .withMappingBuilder( //
                    new FormSpecificationMappingBuilder() //
                            .setDataAttributeExclusion("displayCondition", "confType", "value", "trigger") //
                            .setGroupAttributeExclusion("displayCondition", "data", "trigger") //
            ) //
            .build();

    private static final String BASENAME = String.format("po/%s.ValidationMessages", FormSpecificationValidator.class.getPackage().getName());

    private static final Map<Predicate<Bindings>, Function<Bindings, Object>> DATA_VALUE_GETTERS;

    static {
        final Map<Predicate<Bindings>, Function<Bindings, Object>> map = new LinkedHashMap<>();

        map.put( //
                bindings -> bindings.values().stream().allMatch(elm -> elm instanceof FileValueAdapter.Item), //
                bindings -> bindings.values().stream() //
                        .map(FileValueAdapter.Item.class::cast) //
                        .map(item -> FileValueAdapter.createItem(-1, item.getLabel(), item.getMimeType(), () -> item.getContent())) //
                        .collect(Collectors.toList()) //
        );

        map.put( //
                bindings -> null != bindings.get("list"), //
                bindings -> {
                    final Bindings listValues = (Bindings) bindings.get("list");
                    final Bindings textValues = (Bindings) listValues.get("text");
                    final List<ReferentialTextElement> referentialTextElements = textValues.values().stream().map(d -> DOZER.map(d, ReferentialTextElement.class)).collect(Collectors.toList());
                    final ValueElementBuilder builder = new ValueElementBuilder();
                    referentialTextElements.forEach(opt -> builder.addTextValue(opt.getId(), opt.getLabel()));

                    return builder.build();
                } //
        );

        map.put( //
                bindings -> null != bindings.get("text"), //
                bindings -> {
                    final Bindings textValues = (Bindings) bindings.get("text");
                    final List<Object> listTextValue = (List<Object>) textValues.values();
                    return listTextValue.stream().findFirst() //
                            .map(textValue -> DOZER.map(textValue, ReferentialOption.class)) //
                            .map(referentialOption -> new ValueElement(new TextValueElement(referentialOption.getId(), referentialOption.getLabel()))) //
                            .orElse(null);
                } //
        );

        map.put(bindings -> true, Bindings::values);

        DATA_VALUE_GETTERS = Collections.unmodifiableMap(map);
    }

    @Override
    protected IProcessResult<FormSpecificationData> bind(final Object value) {
        FormSpecificationData data = null;
        if (null != value) {
            if (value instanceof Bindings) {
                LOGGER.debug("Converting JS bindings to form specification data");
                final Bindings src = (Bindings) value;

                data = DOZER.map(src, FormSpecificationData.class);

                this.buildWarnings(data, src);

                final Bindings groups = (Bindings) src.get("groups");

                if (null != groups) {
                    data.setGroups(groups.values().stream().map(grp -> this.buildGroup("", (Bindings) grp)).collect(Collectors.toList()));
                }
            } else if (value instanceof FormSpecificationData) {
                data = (FormSpecificationData) value;
            }
        }

        if (null == data) {
            LOGGER.debug("No data returned, using alternate one");
            data = this.alternateData();
        }

        final String outputResourceName = this.getProcess().getOutput();
        if (null != data && StringUtils.isNotEmpty(outputResourceName)) {
            // -->MINE-296 : validate the data before creating it
            LOGGER.debug("Validating form specification data to '{}'", outputResourceName);
            final MessageFormatter messagesFormatter = NashMessageReader.getReader(BASENAME, LocaleContextHolder.getLocale()).getFormatter();
            final FormSpecificationDataValidator validator = new FormSpecificationDataValidator(this.getContext().getProvider(), messagesFormatter);

            final Errors errors = validator.validate(data, data.getVersion());
            if (errors.hasErrors()) {
                throw new TechnicalException(errors.toString());
            }
            // <--

            LOGGER.debug("Writing form specification data to '{}'", outputResourceName);
            final FormSpecificationData previousData = this.getContext().getProvider().asBean(outputResourceName, FormSpecificationData.class);
            data = merge(previousData, data);
            if (StringUtils.isEmpty(data.getId())) {
                data.setId(this.getProcess().getId());
            }
            this.saveDataResource(outputResourceName, data);
        }

        final Map<String, DataWrapper> loadData = Optional.ofNullable(this.getBridge()) //
                .map(IBridge::getLoadedData) //
                .orElse(new HashMap<String, DataWrapper>());
        loadData.entrySet().stream().forEach(wrapper -> { //
            this.saveDataResource(wrapper.getKey(), wrapper.getValue().getData()); //
        });

        return new DataProcessResult(data);
    }

    private void saveDataResource(final String resourcePath, final FormSpecificationData bean) {
        final IProvider provider = this.getContext().getProvider();
        final String absoluteResourcePath = provider.getAbsolutePath(resourcePath);

        provider.save(resourcePath, "text/xml", JaxbFactoryImpl.instance().asByteArray(bean, StylesheetUtil.resolve(absoluteResourcePath, bean.getClass())));

        LOGGER.debug("Look for necessary template building for resource '{}'", absoluteResourcePath);

        final SpecificationPages pages = this.getContext().getRecord().pages();
        final List<StepElement> steps = this.getContext().getRecord().stepsMgr().findAll();
        for (int idx = 0; idx < steps.size(); idx++) {
            final StepElement step = steps.get(idx);
            if (absoluteResourcePath.equals(step.getData())) {
                LOGGER.debug("Build templates for resource '{}' in step #{}", absoluteResourcePath, idx);
                pages.create(this.getContext().getRecord().buildEngineContext(step));
            }
        }
    }

    /**
     * Alternate data.
     *
     * @return the form specification data
     */
    protected FormSpecificationData alternateData() {
        return null;
    }

    /**
     * Builds the.
     *
     * @param path
     *            the path
     * @param src
     *            the src
     * @return the i element
     */
    protected IElement<?> build(final String path, final Bindings src) {
        if (null == src || null == src.get("_type")) {
            return null;
        }

        IElement<?> elm;

        switch ((String) src.get("_type")) {
        case "group":
            elm = this.buildGroup(path, src);
            break;

        case "data":
            elm = this.buildData(path, src);
            break;

        default:
            elm = null;
            break;
        }

        return elm;
    }

    /**
     * Builds the group.
     *
     * @param path
     *            the path
     * @param src
     *            the src
     * @return the group element
     */
    protected GroupElement buildGroup(final String path, final Bindings src) {
        if (null == src) {
            return null;
        } else {
            final GroupElement groupElement = DOZER.map(src, GroupElement.class);

            final String newPath = String.format("%s%s.", path, groupElement.getId());
            groupElement.setPath(newPath);

            final Bindings datas = (Bindings) src.get("data");
            if (null != datas) {
                groupElement.setData(datas.values().stream().map(d -> this.build(newPath, (Bindings) d)).collect(Collectors.toList()));
            }

            this.updateRulesValue(groupElement);
            this.buildWarnings(groupElement, src);

            return groupElement;
        }
    }

    /**
     * Builds the data.
     *
     * @param path
     *            the path
     * @param src
     *            the src
     * @return the data element
     */
    protected DataElement buildData(final String path, final Bindings src) {
        if (null == src) {
            return null;
        } else {
            final Object value = src.get("value");
            src.remove("value");

            final DataElement dataElement = DOZER.map(src, DataElement.class);
            dataElement.setPath(path + dataElement.getId());

            final Object conftype = src.get("conftype");
            if (null != conftype) {
                final Bindings confTypeBindings = (Bindings) conftype;
                dataElement.setConftype(new ConftypeElement());
                dataElement.getConftype().setReferentialElements(confTypeBindings.values().stream().map(d -> this.buildReferentielElement(d)).collect(Collectors.toList()));
                if (null != confTypeBindings.get("param")) {
                    final Bindings params = (Bindings) confTypeBindings.get("param");
                    dataElement.getConftype().setParameters(params.values().stream().map(d -> this.buildConfTypeParamElement(d)).collect(Collectors.toList()));
                }
            }

            if (null != value) {
                final IValueAdapter<?> dataType = ValueAdapterFactory.type(this.getContext().getRecord(), dataElement);
                // TODO manage dataType not found or not specified
                final SpecificationLoader loader = this.getContext().getRecord();
                if (value instanceof Bindings) {
                    final Bindings bindings = (Bindings) value;
                    final Object transformedValue = DATA_VALUE_GETTERS.entrySet().stream() //
                            .filter(entry -> entry.getKey().test(bindings)) //
                            .findFirst() //
                            .map(entry -> entry.getValue().apply(bindings)) //
                            .orElse(null);

                    dataType.set(loader, this.getContext().getProvider(), dataElement, transformedValue);
                } else {
                    dataType.set(loader, this.getContext().getProvider(), dataElement, value);
                }
            }

            this.updateRulesValue(dataElement);
            this.buildWarnings(dataElement, src);

            final Object triggerValue = src.get("trigger");
            if (null != triggerValue) {
                dataElement.setTrigger((String) triggerValue);
            }
            return dataElement;
        }
    }

    /**
     * Builds the referential elements.
     *
     * @param src
     *            the source object.
     * @return a referential element
     */
    protected ReferentialElement buildReferentielElement(final Object src) {
        if (null == src) {
            return null;
        } else {
            final ReferentialElement referentialElement = DOZER.map(src, ReferentialElement.class);
            final Object text = ((Bindings) src).get("text");
            if (null != text) {
                final Bindings textValues = (Bindings) text;
                referentialElement.setReferentialTextElements(textValues.values().stream().map(d -> DOZER.map(d, ReferentialTextElement.class)).collect(Collectors.toList()));
            }

            return referentialElement;
        }
    }

    /**
     * Merge.
     *
     * @param previous
     *            the previous
     * @param processData
     *            the process data
     * @return the form specification data
     */
    protected static FormSpecificationData merge(final FormSpecificationData previous, final FormSpecificationData processData) {
        if (null == previous) {
            return processData;
        } else if (null == processData) {
            return previous;
        } else {
            return merge(previous, processData.getGroups());
        }
    }

    /**
     * Merge.
     *
     * @param previous
     *            the previous
     * @param newGroupElements
     *            the new group elements
     * @return the form specification data
     */
    private static FormSpecificationData merge(final FormSpecificationData previous, final List<GroupElement> newGroupElements) {
        if (CollectionUtils.isEmpty(previous.getGroups())) {
            previous.setGroups(newGroupElements);
        } else {
            final List<GroupElement> oldGroupElements = previous.getGroups();

            final Map<String, GroupElement> oldGroupElementById = new HashMap<>();
            oldGroupElements.forEach(g -> oldGroupElementById.put(g.getId(), g));

            final List<GroupElement> updated = newGroupElements.stream().map(newGroupElement -> {
                final GroupElement oldGroupElement = oldGroupElementById.get(newGroupElement.getId());
                if (null != oldGroupElement) {
                    newGroupElement.setData(merge(oldGroupElement.getData(), newGroupElement.getData()));
                }
                return newGroupElement;
            }).collect(Collectors.toList());

            previous.setGroups(updated);
        }

        return previous;
    }

    /**
     * Merge.
     *
     * @param oldElements
     *            the old elements
     * @param newElements
     *            the new elements
     * @return the list
     */
    private static List<IElement<?>> merge(final List<IElement<?>> oldElements, final List<IElement<?>> newElements) {
        if (null == oldElements || null == newElements) {
            return newElements;
        }

        final Map<String, IElement<?>> newElementById = new HashMap<>();
        newElements.forEach(d -> newElementById.put(d.getId(), d));

        final List<IElement<?>> updated = oldElements.stream().filter(d -> newElementById.containsKey(d.getId())) //
                .map(oldElement -> {
                    final IElement<?> newElement = newElementById.remove(oldElement.getId());

                    // -->If the structure has changed but the value of the new
                    // element is null : return the old element with the new
                    // structure
                    if (null != oldElement.getValue() && null == newElement.getValue()) {
                        if (oldElement instanceof DataElement) {
                            final DataElement dataElement = (DataElement) oldElement;
                            dataElement.setLabel(newElement.getLabel());
                            dataElement.setDescription(newElement.getDescription());
                            dataElement.setHelp(newElement.getHelp());
                            return dataElement;
                        } else if (oldElement instanceof GroupElement) {
                            final GroupElement groupElement = (GroupElement) oldElement;
                            groupElement.setLabel(newElement.getLabel());
                            groupElement.setDescription(newElement.getDescription());
                            groupElement.setHelp(newElement.getHelp());
                            return groupElement;
                        }
                    }

                    final boolean updateNeeded = !new EqualsBuilder().append(newElement.getLabel(), oldElement.getLabel()) //
                            .append(newElement.getDescription(), oldElement.getDescription()) //
                            .append(newElement.getHelp(), oldElement.getHelp()) //
                            .append(newElement.getValue(), oldElement.getValue()) //
                            .isEquals();

                    return updateNeeded ? newElement : oldElement;
                }) //
                .collect(Collectors.toList());

        updated.addAll(newElementById.values());

        return updated;
    }

    /**
     * Builds the warning elements.
     *
     * @param element
     *            The element (data or group)
     * @param src
     *            The source
     */
    private void buildWarnings(final Object element, final Bindings src) {
        final Bindings warnings = (Bindings) src.get("warnings");
        if (null != warnings) {
            final List<WarningElement> warningList = warnings.values().stream().map(warning -> DOZER.map(warning, WarningElement.class)).collect(Collectors.toList());
            if (element instanceof GroupElement) {
                final GroupElement groupElement = (GroupElement) element;
                groupElement.setWarnings(warningList);
            } else if (element instanceof DataElement) {
                final DataElement dataElement = (DataElement) element;
                dataElement.setWarnings(warningList);
            } else if (element instanceof FormSpecificationData) {
                final FormSpecificationData fsd = (FormSpecificationData) element;
                fsd.setWarnings(warningList);
            }
        }
    }

    /**
     * Update rule value for an element.
     *
     * @param element
     *            The element (data or group)
     */
    private void updateRulesValue(final IElement<?> element) {
        if (StringUtils.isNotEmpty(element.getRuleAsAttr()) && element.getRuleAsAttr().contains("_$")) {
            element.setRuleAsAttr(element.getRuleAsAttr().replace("_$", "$"));
        }
        if (StringUtils.isNotEmpty(element.getRuleAsTag()) && element.getRuleAsTag().contains("_$")) {
            element.setRuleAsTag(element.getRuleAsTag().replace("_$", "$"));
        }
    }

    /**
     * Builds the referential elements.
     *
     * @param src
     *            the source object.
     * @return a referential element
     */
    protected ConfTypeParamElement buildConfTypeParamElement(final Object src) {
        ConfTypeParamElement confTypeParamElement = null;
        if (null != src) {
            final Bindings param = (Bindings) src;
            confTypeParamElement = new ConfTypeParamElement();
            confTypeParamElement.setName((String) param.get("name"));
            confTypeParamElement.setValue((String) param.get("value"));
        }
        return confTypeParamElement;
    }
}
