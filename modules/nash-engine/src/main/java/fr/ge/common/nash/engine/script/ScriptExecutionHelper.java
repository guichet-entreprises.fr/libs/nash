/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script;

import java.util.Base64;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author Christian Cougourdan
 */
public final class ScriptExecutionHelper {

    /**
     * Checks if specified object is a {@link List} instance.
     *
     * @param obj
     *            the obj
     * @return true, if list instance
     */
    public boolean isList(final Object obj) {
        return obj instanceof List;
    }

    /**
     * convert String to byte
     *
     * @param content
     *            data encoded in Base64
     * @return byte[]
     */
    public byte[] convertBase64ToByte(final String content) {
        if (content != null) {
            return Base64.getDecoder().decode(content);
        }
        return null;
    }

    /**
     * convert byte to String
     *
     * @param content
     *            byte
     * @return String base 64
     */
    public String convertByteToBase64(final byte[] content) {
        if (content != null) {
            return Base64.getEncoder().encodeToString(content);
        }
        return null;
    }

    public String convertToJson(final Object object) {
        if (object != null) {
            final ObjectMapper mapper = new ObjectMapper();
            String json;
            try {
                json = mapper.writeValueAsString(object);
                return json;
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
                return null;
            }

        }
        return null;
    }

}
