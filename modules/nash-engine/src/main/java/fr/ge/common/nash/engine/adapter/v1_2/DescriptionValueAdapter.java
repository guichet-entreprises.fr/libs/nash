package fr.ge.common.nash.engine.adapter.v1_2;

import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;

/**
 * Widget able to display value as markdown content, without label attribute.
 *
 * @author Christian Cougourdan
 */
public class DescriptionValueAdapter extends AbstractValueAdapter<String> {

    @Override
    public String name() {
        return "Description";
    }

    @Override
    public String description() {
        return "Allow to display value as markdown content, without label attribute.\n\n* use \"&#13;\" for line feed.";
    }

    @Override
    public String template() {
        return this.getClass().getName().replace('.', '/') + "Resources/template";
    }

    @Override
    public String fromString(final String value) {
        return value;
    }

    @Override
    public String toString(final Object value) {
        return null == value ? null : value.toString();
    }

    @Override
    protected ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        if (value instanceof FormContentData) {
            throw new IllegalArgumentException();
        } else {
            return super.toValueElement(loader, dataElement, value);
        }
    }

    @Override
    public boolean hasOwnUpdate() {
        return true;
    }

}
