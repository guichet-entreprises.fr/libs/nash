/**
 * 
 */
package fr.ge.common.nash.engine.manager.processor.bridge.script;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceRequest;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceResponse;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class StorageApplicationWrapper extends ApplicationWrapper {

    /**
     * {@inheritDoc}
     */
    @Override
    public IProvider load(final String baseUrl, final String code) {
        final ServiceResponse response = new ServiceRequest(StringUtils.join(baseUrl, String.format("/v1/Record/code/%s/download", code))).get();
        if (Status.OK.getStatusCode() == response.getStatus()) {
            return new ZipProvider(response.asBytes());
        }

        throw new TechnicalException(String.format("Load storage record %s unsuccessfully", code));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServiceResponse uploadTo(final String baseUrl, final IProvider provider, final List<String> acls) {
        throw new TechnicalException("Not implemented");
    }

}
