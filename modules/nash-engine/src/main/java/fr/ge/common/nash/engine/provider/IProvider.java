/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.provider;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.mapping.IFormSpecificationResource;
import fr.ge.common.nash.engine.util.StylesheetUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * The Interface ISpecificationProvider.
 *
 * @author Christian Cougourdan
 */
public interface IProvider {

    /**
     * Load.
     *
     * @param name
     *            the name
     * @return the file entry
     */
    FileEntry load(final String name);

    /**
     * As bytes.
     *
     * @return the byte[]
     */
    default byte[] asBytes() {
        throw new TechnicalException("Not implemented ...");
    }

    /**
     * Load.
     *
     * @param name
     *            the name
     * @return the byte[]
     */
    default byte[] asBytes(final String name) {
        return Optional.ofNullable(this.load(name)).map(FileEntry::asBytes).orElse(new byte[] {});
    }

    /**
     * Load.
     *
     * @param <T>
     *            the generic type
     * @param name
     *            the name
     * @param clazz
     *            expected class type
     * @return bean as T
     */
    default <T> T asBean(final String name, final Class<T> clazz) {
        final byte[] asBytes = asBytes(name);

        if (ArrayUtils.isNotEmpty(asBytes)) {
            final T obj = JaxbFactoryImpl.instance().unmarshal(asBytes, clazz);
            if (obj instanceof IFormSpecificationResource) {
                ((IFormSpecificationResource) obj).setResourceName(getAbsolutePath(name));
            }
            return obj;
        } else {
            return null;
        }
    }

    /**
     * Save.
     *
     * @param content
     *            the content
     */
    default void save(final byte[] content) {
        throw new TechnicalException("Not implemented ...");
    }

    /**
     * Save.
     *
     * @param name
     *            the name
     * @param content
     *            the content
     */
    default void save(final String name, final byte[] content) {
        this.save(name, "application/octet-stream", content);
    }

    /**
     * Save.
     *
     * @param <T>
     *            the generic type
     * @param name
     *            the name
     * @param bean
     *            the bean
     */
    default <T> void save(final String name, final T bean) {
        this.save(name, JaxbFactoryImpl.instance().asByteArray(bean, StylesheetUtil.resolve(getAbsolutePath(name), bean.getClass())));
    }

    /**
     * Save.
     *
     * @param name
     *            the name
     * @param mimetype
     *            the mimetype
     * @param content
     *            the content
     */
    default void save(final String name, final String mimetype, final byte[] content) {
        throw new TechnicalException("Not implemented ...");
    }

    /**
     * Removes the.
     *
     * @param name
     *            the name
     * @return true, if successful
     */
    default boolean remove(final String name) {
        throw new TechnicalException("Not implemented ...");
    }

    /**
     * Returns absolute resource path.
     *
     * @param name
     *            resource name
     * @return absolute resource path
     */
    default String getAbsolutePath(final String name) {
        return name;
    }

    /**
     * Returns all resources as absolute path.
     *
     * @param stepFolder
     *            The step folder to find resources
     * @return
     */
    default List<String> resources(final String stepFolder) {
        throw new TechnicalException("Not implemented ...");
    }
}
