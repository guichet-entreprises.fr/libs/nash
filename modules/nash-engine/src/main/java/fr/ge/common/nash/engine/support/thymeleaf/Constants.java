package fr.ge.common.nash.engine.support.thymeleaf;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;

public final class Constants {

    public static final String MODEL_CODE = "code";

    /** Specification loader model constant. */
    public static final String MODEL_LOADER = "loader";

    /** Current record data specification as {@link FormSpecificationData}. */
    public static final String MODEL_STEP = "step";

    /** Current record group as {@link GroupElement}. */
    public static final String MODEL_PAGE = "page";

    /** Current step index model constant. */
    public static final String MODEL_STEP_INDEX = "idxStep";

    /** Current page index model constant. */
    public static final String MODEL_PAGE_INDEX = "idxPage";

    /** Previous page index model constant. */
    public static final String MODEL_PAGE_PREVIOUS_INDEX = "idxPreviousPage";

    /** "save-quit" constant for form submission buttons. */
    public static final String MODEL_BUTTON_SAVE = "btnSaveQuit";

    /** "next" constant for form submission buttons. */
    public static final String MODEL_BUTTON_NEXT = "btnNextPage";

    /** "finish" constant for form submission buttons. */
    public static final String MODEL_BUTTON_FINISH = "btnNextStep";

    /** "finalize" constant for form submission buttons. */
    public static final String MODEL_BUTTON_FINALIZE = "btnFinalize";

    /** "previous" constant for form submission buttons. */
    public static final String MODEL_BUTTON_PREVIOUS = "btnPrevious";

    public static final String MODEL_DATA = "data";

    public static final String MODEL_DATA_TYPE = "type";

    public static final String MODEL_DATA_PATH = "path";

    public static final String MODEL_CHILDREN = "children";

    public static final String MODEL_TEMPLATE_NAME = "templateName";

    public static final String MODEL_JSON_VALUE = "jsonValue";

    private Constants() {
        // Nothing to do
    }

}
