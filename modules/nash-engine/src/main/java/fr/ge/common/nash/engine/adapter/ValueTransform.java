package fr.ge.common.nash.engine.adapter;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vladsch.flexmark.util.collection.Consumer;

import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.AbstractIdentifiedValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.DateValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.exception.TechnicalException;

/**
 *
 * @author Christian Cougourdan
 */
public final class ValueTransform {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValueTransform.class);

    private static Map<Class<?>, ValueMapper<?>> cache = new HashMap<>();

    private ValueTransform() {
        // Nothing to do
    }

    private static <R> ValueMapper<R> getMapper(final Class<R> key) {
        ValueMapper<R> mapper = CoreUtil.cast(cache.get(key));
        if (null == mapper) {
            cache.put(key, mapper = ValueMapper.build(key));
        }
        return mapper;
    }

    public static ValueElement from(final Class<?> ref, final Object src) {
        if (src instanceof FormContentData) {
            return fromData(ref, (FormContentData) src);
        } else if (src instanceof Map) {
            return fromMap(ref, (Map) src);
        } else {
            return fromObject(src);
        }
    }

    public static ValueElement fromObject(final Object src) {
        final ValueMapper<?> mapper = getMapper(src.getClass());
        final ValueElementBuilder builder = new ValueElementBuilder();

        mapper.get(src, (name, value) -> {
            if (null != value) {
                builder.addTextValue(name, value.asString());
            }
        });

        return builder.build();
    }

    public static <S> ValueElement fromData(final Class<S> ref, final FormContentData src) {
        final ValueMapper<S> mapper = getMapper(ref);
        final ValueElementBuilder builder = new ValueElementBuilder();

        mapper.walk(name -> {
            final String value = src.withPath(name).asString();
            if (null != value && 0 != value.length()) {
                builder.addTextValue(name, value);
            }
        });

        return builder.build();
    }

    public static <S> ValueElement fromMap(final Class<S> ref, final Map<String, Object> src) {
        final ValueMapper<S> mapper = getMapper(ref);
        final ValueElementBuilder builder = new ValueElementBuilder();

        mapper.walk(name -> {
            final Object value = src.get(name);
            if (null != value) {
                final ValueWrapper wrapper = new ValueWrapper(value);
                builder.addTextValue(name, wrapper.asString());
            }
        });

        return builder.build();
    }

    public static <R> R to(final Class<R> ref, final ValueElement value) {
        final ListValueElement lst = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .orElse(null);

        if (null == lst) {
            return null;
        }

        final ValueMapper<R> mapper = getMapper(ref);
        final Map<String, ? extends AbstractIdentifiedValueElement> map = lst.asMap(AbstractIdentifiedValueElement.class);
        final R obj = mapper.create();

        mapper.set(obj, name -> {
            final AbstractIdentifiedValueElement elm = map.get(name);
            if (elm instanceof TextValueElement) {
                return ((TextValueElement) elm).getValue();
            } else if (elm instanceof DateValueElement) {
                return ((DateValueElement) elm).getValue();
            } else {
                return null;
            }
        });

        return obj;
    }

    private static class ValueMapper<T> {

        private final Class<T> cls;

        private final Map<String, Method> getters = new LinkedHashMap<>();

        private final Map<String, Method> setters = new LinkedHashMap<>();

        private final Collection<String> properties = new ArrayList<>();

        private ValueMapper(final Class<T> cls) {
            this.cls = cls;
        }

        public static <R> ValueMapper<R> build(final Class<R> cls) {
            final ValueMapper<R> mapper = new ValueMapper<>(cls);

            final Map<String, Map<String, Method>> accessors = new HashMap<>();
            accessors.put("get", new HashMap<>());
            accessors.put("is", new HashMap<>());
            accessors.put("set", new HashMap<>());

            final Pattern re = Pattern.compile("(get|is|set)([A-Z].*)");
            for (final Method method : cls.getDeclaredMethods()) {
                final Matcher m = re.matcher(method.getName());
                if (m.matches()) {
                    final String name = m.group(2);
                    accessors.get(m.group(1)).put(name.substring(0, 1).toLowerCase() + name.substring(1), method);
                }
            }

            for (final Field fld : cls.getDeclaredFields()) {
                final String name = fld.getName();

                mapper.properties.add(name);

                if (accessors.get("is").containsKey(name)) {
                    mapper.getters.put(name, accessors.get("is").get(name));
                } else {
                    mapper.getters.put(name, accessors.get("get").get(name));
                }

                mapper.setters.put(name, accessors.get("set").get(name));
            }

            return mapper;
        }

        public T create() {
            try {
                return this.cls.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                LOGGER.warn("Unable to instanciate class {} : {}", this.cls.getSimpleName(), ex.getMessage());
                throw new TechnicalException(ex.getMessage());
            }
        }

        public void get(final Object src, final BiConsumer<String, ValueWrapper> fn) {
            for (final Entry<String, Method> entry : this.getters.entrySet()) {
                Object value = null;
                try {
                    value = null == entry.getValue() ? null : entry.getValue().invoke(src);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOGGER.warn("unable to access property {} : {}", entry.getKey(), ex.getMessage());
                }

                fn.accept(entry.getKey(), null == value ? null : new ValueWrapper(value));
            }
        }

        public void walk(final Consumer<String> fn) {
            for (final Entry<String, Method> entry : this.getters.entrySet()) {
                fn.accept(entry.getKey());
            }
        }

        public void set(final Object dst, final Function<String, Object> fn) {
            for (final Entry<String, Method> entry : this.setters.entrySet()) {
                final Object value = fn.apply(entry.getKey());
                final Method method = entry.getValue();

                try {
                    if (null != value) {
                        method.invoke(dst, value.toString());
                    }
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOGGER.warn("unable to set property {} : {}", entry.getKey(), ex.getMessage());
                }
            }
        }

    }

    private static class ValueWrapper {

        private final Object value;

        public ValueWrapper(final Object value) {
            this.value = value;
        }

        public String asString() {
            if (null == this.value) {
                return null;
            } else if (this.value instanceof Boolean) {
                return ((Boolean) this.value).booleanValue() ? "yes" : "no";
            } else {
                return this.value.toString();
            }
        }

    }

}
