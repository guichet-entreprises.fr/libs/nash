/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor.bridge.document.pdf;

import java.util.Optional;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;

/**
 * The Class PdfWidgetBox.
 * 
 * @author Christian Cougourdan
 */
public class PdfBoundingBox {

    /** The lower left X. */
    private final float lowerLeftX;

    /** The lower left Y. */
    private final float lowerLeftY;

    /** The upper right X. */
    private final float upperRightX;

    /** The upper right Y. */
    private final float upperRightY;

    /**
     * Instantiates a new pdf widget box.
     *
     * @param lowerLeftX
     *            the lower left X
     * @param lowerLeftY
     *            the lower left Y
     * @param upperRightX
     *            the upper right X
     * @param upperRightY
     *            the upper right Y
     */
    private PdfBoundingBox(float lowerLeftX, float lowerLeftY, float upperRightX, float upperRightY) {
        this.lowerLeftX = lowerLeftX;
        this.lowerLeftY = lowerLeftY;
        this.upperRightX = upperRightX;
        this.upperRightY = upperRightY;
    }

    /**
     * Builds the.
     *
     * @param lowerLeftX
     *            the lower left X
     * @param lowerLeftY
     *            the lower left Y
     * @param upperRightX
     *            the upper right X
     * @param upperRightY
     *            the upper right Y
     * @return the pdf widget box
     */
    public static PdfBoundingBox build(float lowerLeftX, float lowerLeftY, float upperRightX, float upperRightY) {
        return new PdfBoundingBox(lowerLeftX, lowerLeftY, upperRightX, upperRightY);
    }

    /**
     * Builds the.
     *
     * @param widget
     *            the widget
     * @return the pdf widget box
     */
    public static PdfBoundingBox build(PDAnnotationWidget widget) {
        return build( //
                widget.getRectangle(), //
                Optional.ofNullable(widget.getPage()).map(PDPage::getBBox).orElse(null) //
        );
    }

    /**
     * Builds the.
     *
     * @param widget
     *            the widget
     * @param page
     *            the page
     * @return the pdf widget box
     */
    public static PdfBoundingBox build(PDRectangle widget, PDRectangle page) {
        if (null == page) {
            return new PdfBoundingBox( //
                    widget.getLowerLeftX(), //
                    widget.getLowerLeftY(), //
                    widget.getUpperRightX(), //
                    widget.getUpperRightY() //
            );
        } else {
            return new PdfBoundingBox( //
                    widget.getLowerLeftX() / page.getUpperRightX(), //
                    widget.getLowerLeftY() / page.getUpperRightY(), //
                    widget.getUpperRightX() / page.getUpperRightX(), //
                    widget.getUpperRightY() / page.getUpperRightY() //
            );
        }
    }

    /**
     * Gets the lower left X.
     *
     * @return the lower left X
     */
    public float getLowerLeftX() {
        return this.lowerLeftX;
    }

    /**
     * Gets the lower left Y.
     *
     * @return the lower left Y
     */
    public float getLowerLeftY() {
        return this.lowerLeftY;
    }

    /**
     * Gets the upper right X.
     *
     * @return the upper right X
     */
    public float getUpperRightX() {
        return this.upperRightX;
    }

    /**
     * Gets the upper right Y.
     *
     * @return the upper right Y
     */
    public float getUpperRightY() {
        return this.upperRightY;
    }

}
