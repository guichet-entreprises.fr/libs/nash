/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.validation;

import java.util.Arrays;
import java.util.Collection;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.validation.commons.IFunctionalValidator;
import fr.ge.common.nash.engine.validation.data.DefaultValueFunctionalValidator;
import fr.ge.common.nash.engine.validation.data.DoubleIfFunctionalValidator;
import fr.ge.common.nash.engine.validation.data.DuplicateIdFunctionalValidator;
import fr.ge.common.nash.engine.validation.data.GroupConditionalDisplayFunctionalValidator;
import fr.ge.common.nash.engine.validation.data.NullTypeFunctionalValidator;
import fr.ge.common.nash.engine.validation.data.UnknownTypeFunctionalValidator;
import fr.ge.common.support.i18n.MessageFormatter;

/**
 * XML data specification validation.
 *
 * @author atuffrea
 * @author Christian Cougourdan
 */
public class FormSpecificationDataValidator extends AbstractValidator {

    /** functional validators. */
    private final Collection<IFunctionalValidator<FormSpecificationData>> FUNCTIONAL_VALIDATORS = Arrays.asList( //
            new DoubleIfFunctionalValidator(), //
            new DuplicateIdFunctionalValidator(), //
            new NullTypeFunctionalValidator(), //
            new UnknownTypeFunctionalValidator(), //
            new DefaultValueFunctionalValidator(), //
            new GroupConditionalDisplayFunctionalValidator() //
    );

    /**
     * Constructor.
     */
    public FormSpecificationDataValidator() {
        // Nothing to do.
    }

    /**
     * Constructor.
     *
     * @param provider
     *            the provider
     * @param messageFormatter
     *            the message formatter
     */
    public FormSpecificationDataValidator(final IProvider provider, final MessageFormatter messageFormatter) {
        super(provider, messageFormatter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final String resourceName, final String version) {
        final FormSpecificationData specificationData = this.provider.asBean(resourceName, FormSpecificationData.class);
        return this.validate(specificationData, version);
    }

    /**
     * Validates functionally a specification data.
     * 
     * @param specificationData
     *            the data
     * @param version
     *            the version
     * @return the errors
     */
    public Errors validate(final FormSpecificationData specificationData, final String version) {
        final Errors errors = new Errors();
        this.FUNCTIONAL_VALIDATORS.forEach(validator -> errors.merge(specificationData.getResourceName(), validator.validate(this.messageFormatter, specificationData, version)));
        return errors;
    }
}
