/**
 * 
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.roles;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * @author bsadil
 *
 */
@XmlRootElement(name = "role")
@XmlAccessorType(XmlAccessType.NONE)
public class RoleElement implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The role. */
    @XmlValue
    private String role;

    /**
     * 
     */
    public RoleElement() {
        super();
        // Nothing to do
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param message
     *            the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }
}
