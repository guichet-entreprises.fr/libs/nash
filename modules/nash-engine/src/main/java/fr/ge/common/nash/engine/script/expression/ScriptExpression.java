/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script.expression;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The Class ScriptExpression.
 *
 * @author Christian Cougourdan
 */
public class ScriptExpression {

    /** The expression. */
    private final String expression;

    /** The text. */
    private String text;

    /** The dependencies. */
    private Collection<String> dependencies = Collections.emptyList();

    /** The dependencies by scope. */
    private Map<String, Collection<String>> dependenciesByScope;

    /**
     * Instantiates a new script expression.
     *
     * @param expression
     *            the expression
     */
    public ScriptExpression(final String expression) {
        this.expression = expression;
    }

    /**
     * Sets the text.
     *
     * @param text
     *            the text
     * @return the script expression
     */
    public ScriptExpression setText(final String text) {
        this.text = text;
        return this;
    }

    /**
     * Sets the dependencies.
     *
     * @param dependencies
     *            the dependencies
     * @return the script expression
     */
    public ScriptExpression setDependencies(final Map<String, Collection<String>> dependencies) {
        this.dependenciesByScope = dependencies;
        this.dependencies = this.dependenciesByScope.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
        return this;
    }

    /**
     * Gets the expression.
     *
     * @return the expression
     */
    public String getExpression() {
        return this.expression;
    }

    /**
     * Gets the text.
     *
     * @return the text
     */
    public String getText() {
        return this.text;
    }

    /**
     * Gets the dependencies.
     *
     * @return the dependencies
     */
    public Collection<String> getDependencies() {
        return this.dependencies;
    }

}
