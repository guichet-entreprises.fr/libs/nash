/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationReferential;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialExternalElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialExternalUrlElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialTextElement;

/**
 * {@link SpecificationLoader} mock.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public final class SpecificationLoaderMock extends SpecificationLoader {

    /** Step position. */
    private static final int STEP_POSITION = 42;

    /** The version. */
    private final String version;

    /**
     * Constructor.
     */
    private SpecificationLoaderMock() {
        this(null);
    }

    /**
     * Constructor.
     *
     * @param version
     *            the version
     */
    private SpecificationLoaderMock(final String version) {
        super(new Configuration(new Properties()), null);
        this.version = null == version ? "latest" : version;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormSpecificationDescription description() {
        final FormSpecificationDescription description = new FormSpecificationDescription();
        description.setVersion(this.version);
        description.setTitle("Test");
        description.setDescription("Test");
        description.setLang("en");
        description.setTranslations(new TranslationsElement());

        final StepElement step = new StepElement();
        step.setStatus(StepStatusEnum.TO_DO.getStatus());
        step.setUser("ge");

        description.setSteps(new StepsElement(Arrays.asList(step)));

        return description;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormSpecificationReferential referential(final String id) {
        final ReferentialTextElement value1 = new ReferentialTextElement("ba", "Banana");
        final ReferentialTextElement value2 = new ReferentialTextElement("or", "Orange");
        final ReferentialTextElement value3 = new ReferentialTextElement("peac", "Peach");
        final ReferentialTextElement value4 = new ReferentialTextElement("ma", "Mango");
        final ReferentialTextElement value5 = new ReferentialTextElement("pear", "Pear");
        final ReferentialTextElement value6 = new ReferentialTextElement("pu", "Pumpkin");
        final List<ReferentialTextElement> texts = new ArrayList<>();
        texts.addAll(Arrays.asList(value1, value2, value3, value4, value5, value6));

        final ReferentialExternalUrlElement externalUrl = new ReferentialExternalUrlElement();
        externalUrl.setInputVarName("");
        externalUrl.setValue("types/byversion/datatable?" //
                + "order[0].column=0&order[0].dir=asc&start=0&length=5&version=latest&name={{searchTerms}}");
        final ReferentialExternalElement external = new ReferentialExternalElement();
        external.setUrl(externalUrl);
        external.setBase("data");
        external.setId("");
        external.setLabel("");

        final FormSpecificationReferential referential = new FormSpecificationReferential();
        referential.setReferentialTextElements(texts);
        referential.setReferentialExternalElement(external);
        return referential;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream translationFile(final String lang) {
        if ("fr".equals(lang)) {
            return this.getClass().getResourceAsStream(this.getClass().getSimpleName() + ".po");
        } else {
            return null;
        }
    }

    @Override
    public <T> void save(final String resourcePath, final T bean) {

    }

    /**
     * Create a new specification loader mock.
     *
     * @return loader instance
     */
    public static SpecificationLoader create() {
        return new SpecificationLoaderMock();
    }

    /**
     * Create a new specification loader mock.
     *
     * @param version
     *            version
     * @return loader instance
     */
    public static SpecificationLoader create(final String version) {
        return new SpecificationLoaderMock("1.2");
    }

}
