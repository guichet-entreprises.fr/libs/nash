package fr.ge.common.nash.engine.adapter.v1_2.value;

import org.apache.commons.lang3.StringUtils;

/**
 * Togglz class.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class TogglzValue {

    private String on;

    private String off;

    private String value;

    /**
     * @return the labelOn
     */
    public String getOn() {
        return this.on;
    }

    /**
     * @param on
     *            the labelOn to set
     * @return self
     */
    public TogglzValue setOn(final String on) {
        this.on = on;
        return this;
    }

    /**
     * @return the off
     */
    public String getOff() {
        return this.off;
    }

    /**
     * @param off
     *            the off to set
     * @return self
     */
    public TogglzValue setOff(final String off) {
        this.off = off;
        return this;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return StringUtils.isNotEmpty(this.value) ? this.value : "no";
    }

    /**
     * @param value
     *            the value to set
     * @return self
     */
    public TogglzValue setValue(final String value) {
        this.value = value;
        return this;
    }

}
