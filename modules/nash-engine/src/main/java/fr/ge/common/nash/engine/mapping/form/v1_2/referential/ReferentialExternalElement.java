/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.referential;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * Referential "external" element.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "external")
@XmlAccessorType(XmlAccessType.NONE)
public class ReferentialExternalElement implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The URL of the referential. */
    @XmlElement(name = "url")
    private ReferentialExternalUrlElement url;

    /** The base path for the id and label. */
    @XmlElement(name = "base")
    private String base;

    /** The id path within the result. */
    @XmlElement(name = "id")
    private String id;

    /** The label path within the result. */
    @XmlElement(name = "label")
    private String label;

    /**
     * Constructor.
     */
    public ReferentialExternalElement() {
        // Nothing to do.
    }

    /**
     * Constructor.
     *
     * @param url
     *            the URL
     * @param base
     *            the base
     * @param id
     *            the id
     * @param label
     *            the label
     */
    public ReferentialExternalElement(final ReferentialExternalUrlElement url, final String base, final String id, final String label) {
        this.url = url;
        this.base = base;
        this.id = id;
        this.label = label;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public ReferentialExternalUrlElement getUrl() {
        return this.url;
    }

    /**
     * Sets the url.
     *
     * @param url
     *            the new url
     */
    public void setUrl(final ReferentialExternalUrlElement url) {
        this.url = url;
    }

    /**
     * Gets the base.
     *
     * @return the base
     */
    public String getBase() {
        return this.base;
    }

    /**
     * Sets the base.
     *
     * @param base
     *            the new base
     */
    public void setBase(final String base) {
        this.base = base;
    }

    /**
     * Getter on attribute {@link #id}.
     *
     * @return String id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Setter on attribute {@link #id}.
     *
     * @param id
     *            the new value of attribute id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Getter on attribute {@link #label}.
     *
     * @return String label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Setter on attribute {@link #label}.
     *
     * @param label
     *            the new value of attribute label
     */
    public void setLabel(final String label) {
        this.label = label;
    }

}
