/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script.expression.translator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;

import fr.ge.common.utils.CoreUtil;

/**
 *
 * @author Christian Cougourdan
 */
public class VariableScopeTranslator implements Function<String, String> {

    public static enum Scopes {
        RECORD, PAGE, STEP, GROUP, INPUT;

        public String toContextKey() {
            return "_" + this.toString();
        }

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }

    }

    private static final Pattern PATTERN = Pattern.compile("(?<!\\w)\\$(([a-zA-Z_]\\w+)(?:[.]([a-zA-Z_]\\w+(?:[.][a-zA-Z_]\\\\w+)*))?)");

    private static final Map<String, String> TRANSLATE;

    static {
        final Map<String, String> m = new HashMap<>();

        m.put("form", String.format("_%s.$3", Scopes.STEP));
        m.put("current", String.format("_%s.$3", Scopes.PAGE));

        TRANSLATE = Collections.unmodifiableMap(m);
    }

    private final String defaultPrefix;

    public VariableScopeTranslator() {
        this(Scopes.RECORD);
    }

    public VariableScopeTranslator(final Scopes defaultScope) {
        this.defaultPrefix = String.format("_%s.$1", defaultScope);
    }

    @Override
    public String apply(final String src) {
        return CoreUtil.searchAndReplace(src, PATTERN, matcher -> Optional.ofNullable(TRANSLATE.get(matcher.group(2))).orElse(this.defaultPrefix));
    }

}
