package fr.ge.common.nash.engine.mapping.form.v1_2.description;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Represents a Yield element.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "yield")
@XmlAccessorType(XmlAccessType.NONE)
public class YieldElement implements IFlowElement {

    private static final long serialVersionUID = 1L;

    @XmlAttribute(name = "to", required = true)
    private String to;

    @XmlAttribute(name = "extract-steps")
    private String extractSteps;

    @XmlAttribute(name = "copy-steps")
    private String copySteps;

    @XmlAttribute(name = "extract-files")
    private String extractFiles;

    @XmlAttribute(name = "copy-files")
    private String copyFiles;

    /**
     * @return the to
     */
    public String getTo() {
        return this.to;
    }

    /**
     * @param to
     *            the to to set
     */
    public void setTo(final String to) {
        this.to = to;
    }

    /**
     * @return the extractSteps
     */
    public String getExtractSteps() {
        return this.extractSteps;
    }

    /**
     * @param extractSteps
     *            the extractSteps to set
     */
    public void setExtractSteps(final String extractSteps) {
        this.extractSteps = extractSteps;
    }

    /**
     * @return the copySteps
     */
    public String getCopySteps() {
        return this.copySteps;
    }

    /**
     * @param copySteps
     *            the copySteps to set
     */
    public void setCopySteps(final String copySteps) {
        this.copySteps = copySteps;
    }

    /**
     * @return the extractFiles
     */
    public String getExtractFiles() {
        return this.extractFiles;
    }

    /**
     * @param extractFiles
     *            the extractFiles to set
     */
    public void setExtractFiles(final String extractFiles) {
        this.extractFiles = extractFiles;
    }

    /**
     * @return the copyFiles
     */
    public String getCopyFiles() {
        return this.copyFiles;
    }

    /**
     * @param copyFiles
     *            the copyFiles to set
     */
    public void setCopyFiles(final String copyFiles) {
        this.copyFiles = copyFiles;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
                .append(this.to) //
                .append(this.copySteps) //
                .append(this.extractSteps) //
                .append(this.copyFiles) //
                .append(this.extractFiles) //
                .build();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        } else if (obj instanceof YieldElement) {
            final YieldElement other = (YieldElement) obj;
            new EqualsBuilder() //
                    .append(this.to, other.to) //
                    .append(this.extractSteps, other.extractSteps) //
                    .append(this.copySteps, other.copySteps) //
                    .append(this.extractFiles, other.extractFiles) //
                    .append(this.copyFiles, other.copyFiles) //
                    .isEquals();
        }

        return false;
    }

}
