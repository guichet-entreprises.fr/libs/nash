/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;
import fr.ge.common.nash.engine.adapter.Option;
import fr.ge.common.nash.engine.adapter.v1_2.value.Phone;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.utils.CoreUtil;

/**
 * The Phone value adapter.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class PhoneValueAdapter extends AbstractValueAdapter<Phone> {

    /** La constante CONVERTERS. */
    private static final Map<Class<?>, Function<Object, ValueElement>> CONVERTERS;

    static {
        final Map<Class<?>, Function<Object, ValueElement>> map = new HashMap<>();

        map.put(Phone.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final Phone phone = (Phone) value;

            if (!StringUtils.isEmpty(phone.getCountry())) {
                builder.addTextValue("country", phone.getCountry());
            }

            if (!StringUtils.isEmpty(phone.getE164())) {
                builder.addTextValue("e164", phone.getE164());
            }

            if (!StringUtils.isEmpty(phone.getInternational())) {
                builder.addTextValue("international", phone.getInternational());
            }

            return builder.build();
        });

        map.put(FormContentData.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final FormContentData data = (FormContentData) value;

            if (Arrays.stream(new String[] { "country", "e164", "international" }).anyMatch(key -> StringUtils.isEmpty(data.withPath(key).asString()))) {
                return null;
            }

            Arrays.stream(new String[] { "country", "e164", "international" }) //
                    .filter(key -> StringUtils.isNotEmpty(data.withPath(key).asString())) //
                    .forEach(key -> builder.addTextValue(key, data.withPath(key).asString()));

            return builder.build();
        });

        map.put(HashMap.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final Map<String, String> data = CoreUtil.cast(value);

            Arrays.stream(new String[] { "country", "e164", "international" }) //
                    .filter(key -> data.containsKey(key)) //
                    .forEach(key -> {
                        builder.addTextValue(key, data.get(key));
                    });

            return builder.build();
        });

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    /** Initial country. */
    @Option(description = "Initial country selected in the flags component. Ex. : 'fr'")
    private String initialCountry;

    /** Display an example. */
    @Option(description = "Must an example be displayed ?")
    private boolean displayExample;

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {
        return "Phone";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This data type allows to answer questions by typing a phone number.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return PhoneValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Phone fromValueElement(final ValueElement value) {
        final ListValueElement lst = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .orElse(null);

        if (null == lst) {
            return null;
        }

        final Map<String, ? extends TextValueElement> map = lst.asMap(TextValueElement.class);

        final Phone phone = new Phone( //
                Optional.ofNullable(map.get("country")).map(TextValueElement::getValue).orElse(null), //
                Optional.ofNullable(map.get("e164")).map(TextValueElement::getValue).orElse(null), //
                Optional.ofNullable(map.get("international")).map(TextValueElement::getValue).orElse(null) //
        );

        if (StringUtils.isEmpty(phone.getCountry()) && StringUtils.isEmpty(phone.getE164()) && StringUtils.isEmpty(phone.getInternational())) {
            return null;
        } else {
            return phone;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        final Function<Object, ValueElement> converter = this.findConverter(value);
        if (null == converter) {
            return null;
        } else {
            return converter.apply(value);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        if (!(value instanceof Phone)) {
            return null;
        }

        final Phone phone = (Phone) value;
        return phone.toString();
    }

    /**
     * Getter on attribute {@link #initialCountry}.
     *
     * @return String initialCountry
     */
    public String getInitialCountry() {
        return this.initialCountry;
    }

    /**
     * Setter on attribute {@link #initialCountry}.
     *
     * @param initialCountry
     *            the new value of attribute initialCountry
     */
    public void setInitialCountry(final String initialCountry) {
        this.initialCountry = initialCountry;
    }

    /**
     * Getter on attribute {@link #displayExample}.
     *
     * @return boolean displayExample
     */
    public boolean isDisplayExample() {
        return this.displayExample;
    }

    /**
     * Setter on attribute {@link #displayExample}.
     *
     * @param displayExample
     *            the new value of attribute displayExample
     */
    public void setDisplayExample(final boolean displayExample) {
        this.displayExample = displayExample;
    }

    /**
     * Find converter.
     *
     * @param src
     *            src
     * @return function
     */
    private Function<Object, ValueElement> findConverter(final Object src) {
        for (final Map.Entry<Class<?>, Function<Object, ValueElement>> entry : CONVERTERS.entrySet()) {
            if (entry.getKey().isInstance(src)) {
                return entry.getValue();
            }
        }

        return null;
    }

}
