/**
 *
 */
package fr.ge.common.nash.engine.manager.processor.bridge.script;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;

import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.CoreUtil;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class StepWrapper {

    /** The dozer. */
    private final Mapper dozer = DozerBeanMapperBuilder.create().build();

    /** The context. */
    private final EngineContext<?> context;

    /** The step. */
    private final StepElement step;

    /** The step folder as pattern. **/
    private static final Pattern STEP_FOLDER = Pattern.compile("(.+?[\\/]).*", Pattern.CASE_INSENSITIVE);

    /**
     * Constructeur de la classe.
     *
     * @param context
     * @param find
     */
    public StepWrapper(final EngineContext<?> context, final StepElement step) {
        this.context = context;
        this.step = step;
    }

    public StepWrapper duplicate() {

        this.context.getRecord().stepsMgr().findResources(this.step);
        final StepElement stepCopy = this.dozer.map(this.step, StepElement.class);
        stepCopy.setStatus(null);

        stepCopy.setData(this.removePrefix(stepCopy.getData()));
        stepCopy.setPreprocess(this.removePrefix(stepCopy.getPreprocess()));
        stepCopy.setPostprocess(this.removePrefix(stepCopy.getPostprocess()));

        final String originalStepFolder = Optional.of(CoreUtil.coalesce(this.step.getData(), this.step.getPreprocess(), this.step.getPostprocess())) //
                .filter(d -> StringUtils.isNotEmpty(d)) //
                .map(d -> {
                    final Matcher matcher = STEP_FOLDER.matcher(d);
                    if (matcher.matches()) {
                        return matcher.group(1);
                    }
                    return null;
                }) //
                .orElse(null);

        final String stepFolder = this.context.getRecord().stepsMgr().addStep(stepCopy);

        new ZipProvider(this.context.getProvider().asBytes()) //
                .resources(originalStepFolder + "**/*.*") //
                .forEach(resourcePath -> {
                    final String destinationPath = "/" + resourcePath.replace(originalStepFolder, stepFolder + "/");
                    this.context.getProvider().save(destinationPath, this.context.getProvider().load(resourcePath).asBytes());
                });

        return this;
    }

    private String removePrefix(final String path) {
        final String[] lst = path.split("/", 2);
        return lst.length > 1 ? lst[1] : path;
    }

}
