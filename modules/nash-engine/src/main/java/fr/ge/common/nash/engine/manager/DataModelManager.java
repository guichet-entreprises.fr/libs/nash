/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import fr.ge.common.nash.engine.manager.extractor.IDataModelExtractor;
import fr.ge.common.nash.engine.manager.extractor.IndexedDataModelExtractor;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.manager.parser.ElementParser;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.util.TypeDeclaration;

/**
 * The Class DataModelManager.
 *
 * @author Christian Cougourdan
 */
public class DataModelManager {

    /** The extractor. */
    private final IDataModelExtractor extractor;

    /** The data specification. */
    private final EngineContext<FormSpecificationData> engineContext;

    /**
     * Default constructor.
     *
     * @param globalScope
     *            the global scope
     * @param extractor
     *            data model extractor
     * @param formSpecification
     *            form specification data object
     */
    public DataModelManager(final EngineContext<FormSpecificationData> engineContext, final IDataModelExtractor extractor) {
        this.extractor = extractor;
        this.engineContext = engineContext;
    }

    /**
     * Constructor using {@link RecursiveDataModelExtractor} as default model
     * extractor.
     *
     * @param globalScope
     *            the global scope
     * @param provider
     *            the provider
     * @param formSpecification
     *            form specification data object
     */
    public DataModelManager(final EngineContext<FormSpecificationData> engineContext) {
        this(engineContext, RecursiveDataModelExtractor.create(engineContext.getProvider()));
    }

    /**
     * Extract data model from form specification.
     *
     * @return data model as Map
     */
    public Map<String, Object> extract() {
        return this.extractor.extract(this.engineContext.getElement());
    }

    /**
     * Extract data model from form specification as a recursive map object.
     *
     * @return data model as Map
     */
    public Map<String, Object> extractRecursive() {
        return this.extractor.extract(this.engineContext.getElement());
    }

    /**
     * Extract data model from form specification as a map of indexed data element.
     *
     * @return data model as Map
     */
    public Map<String, Object> extractIndexed() {
        return new IndexedDataModelExtractor().extract(this.engineContext.getElement());
    }

    /**
     * Retrieve data element types as string, referenced by data id path.
     *
     * @param specification
     *            specification
     * @param groupElement
     *            the group element
     * @return the map
     */
    public static Map<String, TypeDeclaration> types(final FormSpecificationData specification, final GroupElement groupElement) {
        final Map<String, TypeDeclaration> map = new LinkedHashMap<>();

        ElementParser.create((path, elm, spec) -> {
            final TypeDeclaration type = TypeDeclaration.fromElement((DataElement) elm);
            if (null != type) {
                map.put(elm.getPath().replaceAll("\\[[^\\]]+\\]", "[]"), type);
            }
            return path;
        }).parse("", groupElement, specification);

        return map;
    }

    /**
     * Retrieve data elements.
     *
     * @param specification
     *            specification
     * @param groupElement
     *            the group element
     * @return the map
     */
    public static Map<String, DataElement> data(final FormSpecificationData specification, final GroupElement groupElement) {
        final Map<String, DataElement> dataElements = new HashMap<>();

        ElementParser.create((path, elm, spec) -> {
            dataElements.put(path + elm.getId(), (DataElement) elm);
            return path;
        }).parse("", groupElement, specification);

        return dataElements;
    }

    /**
     * Update form specification data from HTTP request.
     *
     * @param loader
     *            the specification loader
     * @param request
     *            HTTP request containing form values
     * @return updated form data
     */
    public FormSpecificationData updateFromRequest(final HttpServletRequest request) {
        // final DataModelUpdater updater = new DataModelUpdater(this.engineContext,
        // request, this.extractor);
        // updater.update();
        new DataModelRequestUpdater(this.engineContext).update(request);

        return this.engineContext.getElement();
    }

}
