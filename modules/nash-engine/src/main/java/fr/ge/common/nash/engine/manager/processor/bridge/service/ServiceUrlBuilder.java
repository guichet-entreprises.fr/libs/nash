/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.bean.Configuration;

/**
 * Service URL builder.
 * 
 * @author jpauchet
 *
 */
public class ServiceUrlBuilder {

    /**
     * Builds the url.
     *
     * @param configuration
     *            the configuration
     * @param url
     *            the url
     * @param params
     *            the params
     * @return the string
     */
    public static String buildUrl(final Configuration configuration, final String url, final Object... params) {
        final List<Object> tokens = new ArrayList<>(Arrays.asList(params));

        return searchAndReplace(url, "(\\$)?\\{([^}]+)\\}", m -> {
            if ("$".equals(m.group(1))) {
                final String key = m.group(2);
                return Optional.ofNullable(configuration).map(cfg -> cfg.get(key)) //
                        .map(String.class::cast) //
                        .orElse(null);
            } else {
                try {
                    return tokens.isEmpty() ? null : URLEncoder.encode("" + tokens.remove(0), StandardCharsets.UTF_8.name()).replace("+", "%20").replace("%2F", "/");
                } catch (final UnsupportedEncodingException ex) {
                    throw new TechnicalException("Unsupported encoding 'UTF-8'", ex);
                }

            }
        });
    }

    /**
     * Search and replace.
     *
     * @param source
     *            the source
     * @param regex
     *            the regex
     * @param resolver
     *            the resolver
     * @return the string
     */
    private static String searchAndReplace(final String source, final String regex, final Function<Matcher, Object> resolver) {
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(source);
        final StringBuffer buffer = new StringBuffer();

        while (matcher.find()) {
            final Object replacement = resolver.apply(matcher);
            if (null == replacement) {
                matcher.appendReplacement(buffer, "$0");
            } else {
                matcher.appendReplacement(buffer, replacement.toString());
            }
        }

        matcher.appendTail(buffer);

        return buffer.toString();
    }

}
