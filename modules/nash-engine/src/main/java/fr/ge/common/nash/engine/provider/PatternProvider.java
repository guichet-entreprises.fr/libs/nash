/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.provider;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import fr.ge.common.utils.bean.FileEntry;

/**
 * The Class PatternSpecificationProvider.
 *
 * @author Christian Cougourdan
 */
public class PatternProvider implements IProvider {

    /** The provider. */
    private final IProvider provider;

    /** The pattern. */
    private final String pattern;

    /**
     * Instantiates a new pattern specification provider.
     *
     * @param provider
     *            the provider
     * @param pattern
     *            the pattern
     */
    public PatternProvider(final IProvider provider, final String pattern) {
        this.provider = provider;
        this.pattern = pattern;
    }

    /**
     * Load.
     *
     * @param name
     *            the name
     * @return the file entry
     */
    @Override
    public FileEntry load(final String name) {
        return this.provider.load(this.getRelativePath(name));
    }

    /**
     * As bytes.
     *
     * @param name
     *            the name
     * @return the byte[]
     */
    @Override
    public byte[] asBytes(final String name) {
        return this.provider.asBytes(this.getRelativePath(name));
    }

    /**
     * As bean.
     *
     * @param <T>
     *            the generic type
     * @param name
     *            the name
     * @param clazz
     *            the clazz
     * @return the t
     */
    @Override
    public <T> T asBean(final String name, final Class<T> clazz) {
        return this.provider.asBean(this.getRelativePath(name), clazz);
    }

    /**
     * Save.
     *
     * @param name
     *            the name
     * @param content
     *            the content
     */
    @Override
    public void save(final String name, final byte[] content) {
        this.provider.save(this.getRelativePath(name), content);
    }

    /**
     * Save.
     *
     * @param name
     *            the name
     * @param mimetype
     *            the mimetype
     * @param content
     *            the content
     */
    @Override
    public void save(final String name, final String mimetype, final byte[] content) {
        this.provider.save(this.getRelativePath(name), mimetype, content);
    }

    /**
     * Removes the.
     *
     * @param name
     *            the name
     * @return true, if successful
     */
    @Override
    public boolean remove(final String name) {
        return this.provider.remove(this.getRelativePath(name));
    }

    /**
     * Get path relative to its parent provider.
     *
     * @param name
     *            path to convert
     * @return relative resource path
     */
    private String getRelativePath(final String name) {
        if (null == name) {
            return null;
        } else if ('/' == name.charAt(0)) {
            return name;
        } else {
            return String.format(this.pattern, name);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAbsolutePath(final String name) {
        return this.provider.getAbsolutePath(this.getRelativePath(name));
    }

    /**
     * Gets a provider from a source provider, given a pattern.
     *
     * @param srcProvider
     *            the source provider
     * @param resourcePath
     *            the resource path
     * @return the provider.
     */
    public static IProvider from(final IProvider srcProvider, final String resourcePath) {
        final String resourceParentPath = Optional.ofNullable(resourcePath) //
                .map(Paths::get) //
                .map(Path::getParent) //
                .map(prefix -> prefix + "/%s") //
                .orElse("%s");

        return new PatternProvider(srcProvider, resourceParentPath);
    }

}
