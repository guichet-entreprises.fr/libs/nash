/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.ClasspathUtil;
import fr.ge.common.nash.engine.bean.Version;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DefaultElement;
import fr.ge.common.nash.engine.util.TypeDeclaration;
import fr.ge.common.nash.engine.util.ValueAdapterResourceTypeEnum;

/**
 * The Class ValueAdapterFactory.
 *
 * @author Christian Cougourdan
 */
public final class ValueAdapterFactory {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ValueAdapterFactory.class);

    /** Default value adapter version. */
    private static final String DEFAULT_VALUE_ADAPTER_VERSION = "1.0";

    /** Latest value adapter version. */
    private static final String LATEST_VALUE_ADAPTER_VERSION = "latest";

    /** Package info. */
    private static final String PACKAGE_INFO = ".package-info";

    /** Inherited types. */
    private static final Map<Version, InheritTypes> INHERITED_TYPES = new HashMap<>();

    /** Value adapter versions. */
    private static final Map<String, Version> VALUE_ADAPTER_VERSIONS = new HashMap<>();

    /** La constante VALUE_ADAPTER_RESOURCES. */
    private static final Map<Class<? extends IValueAdapter<?>>, Map<ValueAdapterResourceTypeEnum, byte[]>> VALUE_ADAPTER_RESOURCES = new HashMap<>();

    /** Versioned types. */
    private static final Map<String, Map<String, IValueAdapter<?>>> VERSIONED_TYPES = loadVersionedTypesNoException();

    private static boolean useCache = false;

    /**
     * Default private constructor.
     */
    private ValueAdapterFactory() {
        // Nothing to do
    }

    /**
     * Loads versioned types.
     *
     * @return versioned types as unmodifiable {@link Map}
     */
    private static Map<String, Map<String, IValueAdapter<?>>> loadVersionedTypesNoException() {
        try {
            return loadVersionedTypes();
        } catch (final TechnicalException ex) {
            LOGGER.error("Loading versioned value adapters : failed !", ex);
            return null;
        }
    }

    /**
     * Loads versioned types.
     *
     * @return versioned types as unmodifiable {@link Map}
     */
    private static Map<String, Map<String, IValueAdapter<?>>> loadVersionedTypes() {
        final Map<Version, Map<String, IValueAdapter<?>>> typesByVersions = new HashMap<>();

        final List<String> candidates = ClasspathUtil.getClassesFromPackage(IValueAdapter.class.getPackage().getName() + ".v", IValueAdapter.class);

        for (final String candidate : candidates) {
            final Version version = extractAdapterVersion(candidate);

            if (candidate.endsWith(PACKAGE_INFO)) {
                // scan all package-info with their annotations containing
                // inheritance data, for each package of the value adapters
                // hierarchy
                gatherPackagesInformation(version, candidate, typesByVersions);
            } else {
                final IValueAdapter<?> valueAdapter = buildValueAdapter(candidate);
                if (null == valueAdapter) {
                    continue;
                }

                loadValueAdapterResources(valueAdapter);

                // if the version of the current value adapter doesn't exist yet
                // in the top-level map (versions), add it...
                final Map<String, IValueAdapter<?>> typesByNames = retrieveValueAdapterForVersion(version, typesByVersions);

                // ... and add the type name to the second-level map (types)
                final String valueAdapterName = valueAdapter.name().toLowerCase(Locale.getDefault());
                if (typesByNames.containsKey(valueAdapterName)) {
                    throw new TechnicalException("Several value adapters have the same name : " + valueAdapterName);
                }
                typesByNames.put(valueAdapterName, valueAdapter);
            }
        }

        // for each version of the top-level map (versions), copy all the types
        // from previous versions that are not yet referenced in the
        // second-level map (types), for inheritance
        final Version version = mergeOlderTypes(typesByVersions);

        // convert the top-level map (versions) from object keys to dots version
        // string keys
        final Map<String, Map<String, IValueAdapter<?>>> typesByStringVersions = new HashMap<>();
        for (final Entry<Version, Map<String, IValueAdapter<?>>> entry : typesByVersions.entrySet()) {
            typesByStringVersions.put(entry.getKey().toString(), entry.getValue());
            VALUE_ADAPTER_VERSIONS.put(entry.getKey().toString(), entry.getKey());
        }

        // add a "special" version that corresponds to the latest available
        // version, containing all its types
        if (null == version) {
            typesByStringVersions.put(LATEST_VALUE_ADAPTER_VERSION, typesByStringVersions.get(DEFAULT_VALUE_ADAPTER_VERSION));
        } else {
            typesByStringVersions.put(LATEST_VALUE_ADAPTER_VERSION, typesByStringVersions.get(version.toString()));
        }

        return typesByStringVersions;
    }

    /**
     * Load value adapter resources.
     *
     * @param valueAdapter
     *            value adapter
     */
    @SuppressWarnings("unchecked")
    private static void loadValueAdapterResources(final IValueAdapter<?> valueAdapter) {
        final Map<ValueAdapterResourceTypeEnum, byte[]> resources = new HashMap<>();

        for (final ValueAdapterResourceTypeEnum resourceType : ValueAdapterResourceTypeEnum.values()) {
            final byte[] asBytes = loadValueAdapterResources(valueAdapter, resourceType);
            if (!ArrayUtils.isEmpty(asBytes)) {
                resources.put(resourceType, asBytes);
            }
        }

        VALUE_ADAPTER_RESOURCES.put((Class<? extends IValueAdapter<?>>) valueAdapter.getClass(), resources);
    }

    /**
     * Load value adapter resources.
     *
     * @param valueAdapter
     *            value adapter
     * @param resourceType
     *            resource type
     * @return byte[]
     */
    private static byte[] loadValueAdapterResources(final IValueAdapter<?> valueAdapter, final ValueAdapterResourceTypeEnum resourceType) {
        final List<InputStream> resourceStreams = new ArrayList<>();

        for (Class<?> clazz = valueAdapter.getClass(); resourceStreams.isEmpty() && null != clazz; clazz = resourceType.isInherit() ? clazz.getSuperclass() : null) {
            resourceStreams.addAll(getResourceStreams(resourceType, clazz));
        }

        if (CollectionUtils.isEmpty(resourceStreams)) {
            return null;
        }

        if (resourceType.getPrefix() != null) {
            resourceStreams.add(0, new ByteArrayInputStream(resourceType.getPrefix().getBytes(StandardCharsets.UTF_8)));
        }

        if (resourceType.getSuffix() != null) {
            resourceStreams.add(new ByteArrayInputStream(resourceType.getSuffix().getBytes(StandardCharsets.UTF_8)));
        }

        try {
            return IOUtils.toByteArray(new SequenceInputStream(new Vector<>(resourceStreams).elements()));
        } catch (final IOException ex) {
            LOGGER.warn("Unable to retrieve '{}' resource for '{}'", resourceType, valueAdapter.name(), ex);
        }

        return null;
    }

    /**
     * Getter on attribute {@link #resource streams}.
     *
     * @param resourceType
     *            resource type
     * @param clazz
     *            clazz
     * @return resource streams
     */
    private static List<InputStream> getResourceStreams(final ValueAdapterResourceTypeEnum resourceType, final Class<?> clazz) {
        final String path = clazz.getSimpleName() + "Resources/";
        final List<InputStream> resources = new ArrayList<>();

        InputStream resource = clazz.getResourceAsStream(path + resourceType.getName() + resourceType.getExtension());

        Optional.ofNullable(resource).ifPresent(rsc -> resources.add(rsc));

        if (resourceType.isMultiple()) {
            if (null == resource) {
                resource = clazz.getResourceAsStream(path + resourceType.getName() + '1' + resourceType.getExtension());
                Optional.ofNullable(resource).ifPresent(rsc -> resources.add(rsc));
            }

            for (int index = 2; null != (resource = clazz.getResourceAsStream(path + resourceType.getName() + index + resourceType.getExtension())); index++) {
                resources.add(resource);
            }
        }

        return resources;
    }

    /**
     * Extract adapter version.
     *
     * @param valueAdapterCandidate
     *            the value adapter candidate
     * @return the value adapter version
     */
    static Version extractAdapterVersion(final String valueAdapterCandidate) {
        final Pattern versionPattern = Pattern.compile(String.format("%s[.](v[0-9]+(?:_[0-9]+)*)[.].*", IValueAdapter.class.getPackage().getName().replaceAll("[.]", "[.]")));

        final Matcher m = versionPattern.matcher(valueAdapterCandidate);
        if (m.matches()) {
            return new Version(m.group(1));
        } else {
            return null;
        }
    }

    /**
     * Builds the value adapter.
     *
     * @param valueAdapterClassName
     *            the value adapter class name
     * @return the i value adapter
     */
    private static IValueAdapter<?> buildValueAdapter(final String valueAdapterClassName) {
        try {
            final Class<?> clazz = Class.forName(valueAdapterClassName);
            if (IValueAdapter.class.isAssignableFrom(clazz)) {
                try {
                    return (IValueAdapter<?>) clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException | IllegalArgumentException ex) {
                    LOGGER.warn("Loading {} : unable to instanciate", valueAdapterClassName, ex);
                }
            }
        } catch (final ClassNotFoundException ex) {
            LOGGER.warn("Class {} not found", valueAdapterClassName, ex);
        }

        return null;
    }

    /**
     * Gather packages information.
     *
     * @param version
     *            the version
     * @param packageName
     *            the package name
     * @param typesByVersions
     *            the types by versions
     */
    private static void gatherPackagesInformation(final Version version, final String packageName, final Map<Version, Map<String, IValueAdapter<?>>> typesByVersions) {
        if (version != null) {
            try {
                Class.forName(packageName);
                final Package myPackage = Package.getPackage(packageName.replace(PACKAGE_INFO, StringUtils.EMPTY));
                InheritTypes inheritTypes = null;
                if (myPackage != null) {
                    inheritTypes = myPackage.getAnnotation(InheritTypes.class);
                }
                INHERITED_TYPES.put(version, inheritTypes);
                retrieveValueAdapterForVersion(version, typesByVersions);
            } catch (final ClassNotFoundException e) {
                LOGGER.warn("Class {} not found", packageName, e);
            }
        }
    }

    /**
     * Add version if missing.
     *
     * @param version
     *            the version
     * @param typesByVersions
     *            the types by versions
     * @return the types by names
     */
    private static Map<String, IValueAdapter<?>> retrieveValueAdapterForVersion(final Version version, final Map<Version, Map<String, IValueAdapter<?>>> typesByVersions) {
        Map<String, IValueAdapter<?>> typesByNames = typesByVersions.get(version);
        if (null == typesByNames) {
            typesByNames = new HashMap<>();
            typesByVersions.put(version, typesByNames);
        }
        return typesByNames;
    }

    /**
     * Merge older types.
     *
     * @param typesByVersions
     *            the types by versions
     * @return the value adapter version
     */
    private static Version mergeOlderTypes(final Map<Version, Map<String, IValueAdapter<?>>> typesByVersions) {
        final Set<Version> orderedVersions = new TreeSet<>(typesByVersions.keySet());

        // iterate from the lowest version to the latest version
        final Iterator<Version> orderedVersionsIterator = orderedVersions.iterator();
        Version version = orderedVersionsIterator.next();
        while (orderedVersionsIterator.hasNext()) {
            version = orderedVersionsIterator.next();
            // get the list of types of the current version
            final Map<String, IValueAdapter<?>> typesByName = typesByVersions.get(version);
            // copy each type from the optional inherited version, if the types
            // are not exceptions in the version inheritance and don't exist yet
            // in the current version
            final InheritTypes inheritedTypes = INHERITED_TYPES.get(version);
            Version inheritedVersion = null;
            List<String> inheritanceExceptions = Collections.emptyList();
            if (inheritedTypes != null) {
                if (inheritedTypes.version() != null) {
                    inheritedVersion = new Version(inheritedTypes.version());
                }
                if (inheritedTypes.except() != null) {
                    inheritanceExceptions = Arrays.asList(inheritedTypes.except());
                }
            }
            if (inheritedVersion != null && typesByVersions.get(inheritedVersion) != null) {
                for (final Entry<String, IValueAdapter<?>> entry : typesByVersions.get(inheritedVersion).entrySet()) {
                    if (!inheritanceExceptions.stream().anyMatch(entry.getKey()::equalsIgnoreCase)) {
                        typesByName.putIfAbsent(entry.getKey(), entry.getValue());
                    }
                }
            }
        }

        return version;
    }

    /**
     * Retrieve data type object corresponding to specific data element.
     *
     * @param spec
     *            form specification linked to {@link DataElement}
     * @param dataElement
     *            data element
     * @return corresponding data type
     */
    public static IValueAdapter<?> type(final FormSpecificationData spec, final DataElement dataElement) {
        return type( //
                Optional.ofNullable(spec) //
                        .map(FormSpecificationData::getVersion) //
                        .orElse(LATEST_VALUE_ADAPTER_VERSION), //
                Optional.ofNullable(dataElement.getType()) //
                        .orElse( //
                                Optional.ofNullable(spec) //
                                        .map(FormSpecificationData::getDefault) //
                                        .map(DefaultElement::getData) //
                                        .map(DataElement::getType) //
                                        .orElse(null) //
                        ) //
        );
    }

    /**
     * Retrieve data type object corresponding to specific data element.
     *
     * @param loader
     *            specification loader
     * @param dataElement
     *            data element
     * @return corresponding data type
     */
    public static IValueAdapter<?> type(final SpecificationLoader loader, final DataElement dataElement) {
        return type( //
                getVersion(loader), //
                Optional.ofNullable(dataElement) //
                        .map(DataElement::getType) //
                        .orElse(null) //
        );
    }

    /**
     * Finds an implementation of a value adapter with a specification loader and a
     * type name.
     *
     * @param loader
     *            the loader
     * @param typeAsString
     *            the type name
     * @return the implementation
     */
    public static IValueAdapter<?> type(final SpecificationLoader loader, final String typeAsString) {
        return type( //
                getVersion(loader), //
                typeAsString //
        );
    }

    /**
     * Finds an implementation of a value adapter with a version and a type name.
     *
     * @param version
     *            the version
     * @param typeAsString
     *            the type name
     * @return the implementation
     */
    public static IValueAdapter<?> type(final String version, final String typeAsString) {
        IValueAdapter<?> dataType = null;

        if (VERSIONED_TYPES == null) {
            LOGGER.error("Could not find a value adapter because versioned types are not initialized.");
            return null;
        }
        final TypeDeclaration typeDeclaration = TypeDeclaration.fromString(typeAsString);
        final Map<String, IValueAdapter<?>> versionTypes = VERSIONED_TYPES.get(version);
        if (typeDeclaration != null && versionTypes != null) {
            dataType = versionTypes.get(typeDeclaration.getName().toLowerCase(Locale.getDefault()));
        }
        if (dataType != null && typeDeclaration != null && StringUtils.isNotEmpty(typeDeclaration.getOptionString())) {
            final ObjectMapper mapper = new ObjectMapper() //
                    .configure(Feature.ALLOW_SINGLE_QUOTES, true) //
                    .configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
            try {
                dataType = mapper.readValue('{' + typeDeclaration.getOptionString() + '}', dataType.getClass());
            } catch (final IOException ex) {
                final String msg = String.format("Unable to parse option string {%s} and apply it to %s", typeDeclaration.getOptionString(), dataType.getClass().getName());
                LOGGER.warn(msg);
                throw new TechnicalException(msg, ex);
            }
        }

        return dataType;
    }

    /**
     * Gets a value adapter resource.
     *
     * @param resourceType
     *            the type of the resource
     * @param valueAdapterClass
     *            the value adapter
     * @return the resource
     */
    public static byte[] getValueAdapterResource(final ValueAdapterResourceTypeEnum resourceType, @SuppressWarnings("rawtypes") final Class<? extends IValueAdapter> valueAdapterClass) {
        final Map<ValueAdapterResourceTypeEnum, byte[]> resourceByType = VALUE_ADAPTER_RESOURCES.get(valueAdapterClass);

        if (null == resourceByType) {
            return null;
        }

        if (false == useCache) {
            try {
                LOGGER.debug("Try to load {} {} resource without using cache", valueAdapterClass.getSimpleName(), resourceType.getName());
                return loadValueAdapterResources(valueAdapterClass.newInstance(), resourceType);
            } catch (InstantiationException | IllegalAccessException e) {
                LOGGER.info("Unable to load {} class", valueAdapterClass.getSimpleName());
            }
        }

        return resourceByType.get(resourceType);
    }

    /**
     * Has resource.
     *
     * @param valueAdapter
     *            value adapter
     * @param resourceType
     *            resource type
     * @return true, si ça fonctionne
     */
    public static boolean hasResource(@SuppressWarnings("rawtypes") final Class<? extends IValueAdapter> valueAdapter, final ValueAdapterResourceTypeEnum resourceType) {
        return null != getValueAdapterResource(resourceType, valueAdapter);
    }

    /**
     * Getter on attribute {@link #LATEST_VALUE_ADAPTER_VERSION}.
     *
     * @return String latestValueAdapterVersion
     */
    public static String getLatestValueAdapterVersion() {
        return LATEST_VALUE_ADAPTER_VERSION;
    }

    /**
     * Getter on attribute {@link #VALUE_ADAPTER_VERSIONS}.
     *
     * @return Map&lt;String,Version&gt; valueAdapterVersions
     */
    public static Map<String, Version> getValueAdapterVersions() {
        return VALUE_ADAPTER_VERSIONS;
    }

    /**
     * Getter on attribute {@link #VERSIONED_TYPES}.
     *
     * @return Map&lt;String,Map&lt;String,IValueAdapter&lt;?&gt;&gt;&gt;
     *         VERSIONED_TYPES
     */
    public static Map<String, Map<String, IValueAdapter<?>>> getVersionedTypes() {
        return VERSIONED_TYPES;
    }

    /**
     * Gets the version.
     *
     * @param loader
     *            the loader
     * @return the version
     */
    private static String getVersion(final SpecificationLoader loader) {
        return Optional.ofNullable(loader) //
                .map(SpecificationLoader::description) //
                .map(FormSpecificationDescription::getVersion) //
                .orElse(DEFAULT_VALUE_ADAPTER_VERSION);
    }

}
