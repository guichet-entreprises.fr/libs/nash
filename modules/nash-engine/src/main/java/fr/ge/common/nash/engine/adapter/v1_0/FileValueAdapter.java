/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_0;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;
import fr.ge.common.nash.engine.adapter.Option;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.FileValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.HashUtil;

/**
 * The File value adapter.
 *
 * @author Christian Cougourdan
 */
public class FileValueAdapter extends AbstractValueAdapter<List<Item>> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileValueAdapter.class);

    /** Item format. */
    private static final String ITEM_FORMAT = "%d:%s";

    /** The Constant ITEM_PATTERN. */
    private static final Pattern ITEM_PATTERN = Pattern.compile("[ ]*([0-9]+):(.+)[ ]*");

    @Option(description = "Authorized file extensions")
    private String ext = this.generateExtensions(".jpg, .png, .jpeg, .pdf");

    @Option(description = "Maximum documents to be uploaded")
    private Integer split = 10;

    @Option(description = "Maximum size allowed per upload (in Mo)")
    private Integer size = 2;

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {
        return "File";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This data type allows to select a file to send.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return FileValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * @return the ext
     */
    public String getExt() {
        return this.ext;
    }

    /**
     * @param ext
     *            the ext to set
     */
    public void setExt(final String ext) {
        this.ext = ext;
    }

    /**
     * @return the maxDocuments
     */
    public Integer getSplit() {
        return this.split;
    }

    /**
     * @param split
     *            the split to set
     */
    public void setSplit(final Integer split) {
        this.split = split;
    }

    /**
     * @return the size
     */
    public Integer getSize() {
        return this.size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final Integer size) {
        this.size = size;
    }

    public String generateExtensions(final String ext) {
        return new StringBuilder(ext) //
                .append(", ") //
                .append(ext.toUpperCase()) //
                .toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Item> get(final DataElement dataElement) {
        return Optional.ofNullable(dataElement) //
                .map(DataElement::getValue) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .map(ListValueElement::getElements) //
                .orElse(new ArrayList<>()) //
                .stream() //
                .filter(v -> v instanceof FileValueElement) //
                .map(FileValueElement.class::cast) //
                .map(v -> createItem(Integer.parseInt(v.getRef()), v.getLabel())) //
                .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Item> get(final IProvider provider, final DataElement dataElement) {
        return Optional.ofNullable(dataElement) //
                .map(DataElement::getValue) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .map(ListValueElement::getElements) //
                .orElse(new ArrayList<>()) //
                .stream() //
                .filter(v -> v instanceof FileValueElement) //
                .map(FileValueElement.class::cast) //
                .map(v -> {
                    final String relativePath = this.itemRelativePath(dataElement, v);
                    if (null == provider) {
                        return createItem(Integer.parseInt(v.getRef()), v.getLabel(), null, () -> null);
                    } else {
                        return createItem(Integer.parseInt(v.getRef()), v.getLabel(), null, () -> provider.asBytes(relativePath)) //
                                .setAbsolutePath(provider.getAbsolutePath(relativePath));
                    }
                }) //
                .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] getAsBytes(final IProvider provider, final DataElement dataElement) {
        InputStream filesAsStream = null;
        byte[] filesAsBytes = null;
        final List<Item> files = this.get(provider, dataElement);
        for (final Item file : files) {
            final InputStream fileAsStream = new ByteArrayInputStream(file.getContent());
            if (filesAsStream == null) {
                filesAsStream = fileAsStream;
            } else {
                filesAsStream = new SequenceInputStream(filesAsStream, fileAsStream);
            }
        }
        if (filesAsStream == null) {
            filesAsStream = new ByteArrayInputStream("".getBytes());
        }
        try {
            filesAsBytes = IOUtils.toByteArray(filesAsStream);
        } catch (final IOException e) {
            LOGGER.debug("Error while converting files to bytes", e);
        }
        return filesAsBytes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Item> set(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        throw new TechnicalException("Provider needed");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Item> set(final SpecificationLoader loader, final IProvider provider, final DataElement dataElement, final Object value) {
        List<Item> storedItems = null;
        if (value instanceof FormContentData) {
            return this.set(loader, provider, dataElement, this.checkAndConvert(((FormContentData) value).asList()));
        } else if (value instanceof DataBinding) {
            return this.set(loader, provider, dataElement, this.checkAndConvert(((DataBinding) value).asList()));
        } else if (value instanceof List && CollectionUtils.isNotEmpty((List<?>) value)) {
            final List<Object> values = CoreUtil.cast(value);

            if (values.stream().allMatch(elm -> elm instanceof Item)) {
                int nextId = Optional.ofNullable(values) //
                        .map(lst -> lst.stream().filter(item -> null != item) //
                                .map(Item.class::cast) //
                                .mapToInt(Item::getId) //
                                .filter(id -> id >= 0) //
                                .max() //
                                .orElse(0) //
                        ).orElse(0);

                storedItems = values.stream() //
                        .filter(item -> null != item) //
                        .map(Item.class::cast) //
                        .collect(Collectors.toList());

                final Predicate<Item> newOrNotPresent = ((Predicate<Item>) item -> item.getId() < 0) //
                        .or(item -> null != provider && ArrayUtils.isEmpty(provider.asBytes(this.itemRelativePath(dataElement, item))));

                final List<Item> itemsToPersist = storedItems.stream().filter(newOrNotPresent).collect(Collectors.toList());
                for (final Item item : itemsToPersist) {
                    if (item.getId() < 0) {
                        item.setId(++nextId);
                    }
                    provider.save(this.itemRelativePath(dataElement, item), item.getMimeType(), item.getContent());
                }
            } else {
                return this.set(loader, provider, dataElement, this.checkAndConvert(values));
            }
        }

        this.clean(provider, dataElement, storedItems);

        if (CollectionUtils.isEmpty(storedItems)) {
            dataElement.setValue(null);
        } else {
            dataElement.setValue( //
                    new ValueElement( //
                            new ListValueElement( //
                                    storedItems.stream() //
                                            .map(item -> new FileValueElement(Integer.toString(item.getId()), item.getLabel())) //
                                            .collect(Collectors.toList()) //
                            ) //
                    ) //
            );

        }
        return this.get(provider, dataElement);
    }

    private List<Item> checkAndConvert(final List<? extends Object> values) {
        List<Item> storedItems = null;
        if (values.stream().allMatch(FormContentData.class::isInstance)) {
            storedItems = values.stream() //
                    .map(FormContentData.class::cast) //
                    .map(FileValueAdapter::createItem) //
                    .collect(Collectors.toList());
        } else if (values.stream().allMatch(DataBinding.class::isInstance)) {
            storedItems = values.stream() //
                    .map(DataBinding.class::cast) //
                    .map(FileValueAdapter::createItem) //
                    .collect(Collectors.toList());
        } else {
            storedItems = values.stream() //
                    .filter(item -> null != item) //
                    .map(obj -> createItemFromMap(CoreUtil.cast(obj))) //
                    .collect(Collectors.toList());
        }

        if (values.stream().anyMatch(item -> null == item)) {
            throw new IllegalArgumentException("One or more values are incomplete");
        } else {
            return storedItems;
        }
    }

    /**
     * Clean.
     *
     * @param provider
     *            the provider
     * @param dataElement
     *            the data element
     * @param newItems
     *            the new items
     */
    protected void clean(final IProvider provider, final DataElement dataElement, final List<Item> newItems) {
        final List<Item> previousItems = this.get(provider, dataElement);
        for (final Item item : previousItems) {
            if (null == newItems || !newItems.contains(item)) {
                provider.remove(this.itemRelativePath(dataElement, item));
            }
        }
    }

    /**
     * Item relative path.
     *
     * @param dataElement
     *            the data element
     * @param item
     *            the item
     * @return the string
     */
    public String itemRelativePath(final DataElement dataElement, final Item item) {
        return this.itemRelativePath(dataElement, Integer.toString(item.getId()), item.getLabel());
    }

    /**
     * Item relative path.
     *
     * @param dataElement
     *            the data element
     * @param fileValueElement
     *            the file value element
     * @return the string
     */
    public String itemRelativePath(final DataElement dataElement, final FileValueElement fileValueElement) {
        return this.itemRelativePath(dataElement, fileValueElement.getRef(), fileValueElement.getLabel());
    }

    /**
     * Item relative path.
     *
     * @param dataElement
     *            the data element
     * @param ref
     *            the ref
     * @param label
     *            the label
     * @return the string
     */
    public String itemRelativePath(final DataElement dataElement, final String ref, final String label) {
        final Pattern patternItemPath = Pattern.compile("(.+/)([^/]+)");
        final Matcher m = patternItemPath.matcher(label);
        if (m.matches()) {
            return String.format("%s%s-%s-%s", m.group(1), dataElement.getPath(), ref, m.group(2));
        } else {
            return String.format("%s%s-%s-%s", this.externalPath(), dataElement.getPath(), ref, label);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String externalPath() {
        return "uploaded/";
    }

    @Override
    public boolean hasOwnUpdate() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        return value == null ? null : value.toString();
    }

    /**
     * Creates the item.
     *
     * @param id
     *            the id
     * @param label
     *            the label
     * @return the item
     */
    public static Item createItem(final int id, final String label) {
        return createItem(id, label, null, () -> null);
    }

    /**
     * Creates the item.
     *
     * @param id
     *            the id
     * @param label
     *            the label
     * @param mimeType
     *            the mime type
     * @param content
     *            the content
     * @return the item
     */
    public static Item createItem(final int id, final String label, final String mimeType, final byte[] content) {
        return createItem(id, label, mimeType, () -> content);
    }

    /**
     * Creates the item.
     *
     * @param id
     *            the id
     * @param label
     *            the label
     * @param mimeType
     *            the mime type
     * @param supplier
     *            the supplier
     * @return the item
     */
    public static Item createItem(final int id, final String label, final String mimeType, final Supplier<byte[]> supplier) {
        return new Item(id, label, mimeType, supplier);
    }

    /**
     * Creates the item.
     *
     * @param src
     *            the src
     * @return the item
     */
    public static Item createItem(final String src) {
        final Matcher m = ITEM_PATTERN.matcher(src);
        if (m.matches()) {
            return createItem(Integer.parseInt(m.group(Item.ID_GROUP)), m.group(Item.LABEL_GROUP), null, () -> null);
        } else {
            return createItem(-1, src, null, () -> null);
        }
    }

    /**
     * Creates the item.
     *
     * @param src
     *            the src
     * @return the item
     */
    public static Item createItemFromMap(final Map<String, Object> src) {
        final String[] keys = { "label", "mimeType", "content" };
        if (src.keySet().containsAll(Arrays.asList(keys))) {
            final byte[] content = convertBase64ToByte((String) src.get("content"));
            final String id = (String) src.get("id");
            return createItem(null == id ? -1 : Integer.parseInt(id), (String) src.get("label"), (String) src.get("mimeType"), content);
        } else {
            return null;
        }
    }

    public static Item createItem(final FormContentData src) {
        final Integer id = src.asInt("id");
        final String label = src.asString("label");
        final String mimeType = src.asString("mimeType");
        final String content = src.asString("content");

        if (null != label && null != mimeType && null != content) {
            return createItem(null == id ? -1 : id, label, mimeType, convertBase64ToByte(content));
        } else {
            return null;
        }
    }

    public static Item createItem(final DataBinding src) {
        final Integer id = src.withPath("id").asInt();
        final String label = src.withPath("label").asString();
        final String mimeType = src.withPath("mimeType").asString();
        final String content = src.withPath("content").asString();

        if (null != label && null != mimeType && null != content) {
            return createItem(null == id ? -1 : id, label, mimeType, convertBase64ToByte(content));
        } else {
            return null;
        }
    }

    /**
     * convert String to byte
     *
     * @param content
     *            data encoded in Base64
     * @return byte[]
     */
    public static byte[] convertBase64ToByte(final String content) {
        if (content != null) {
            return Base64.getDecoder().decode(content);
        }
        return null;
    }

    /**
     * The Class Item.
     */
    public static class Item {

        /** Id group. */
        private static final int ID_GROUP = 1;

        /** Label group. */
        private static final int LABEL_GROUP = 2;

        /** The id. */
        private int id;

        /** The label. */
        private final String label;

        /** The mime type. */
        private final String mimeType;

        /** The content supplier. */
        private final Supplier<byte[]> contentSupplier;

        /** The content. */
        private byte[] content;

        /** The hash. */
        private String hash;

        private String absolutePath;

        /** La constante FILE_NAME_PATTERN. */
        private static final Pattern FILE_NAME_PATTERN = Pattern.compile(".*\\.([^.]+)");

        /** La constante MIME_TYPES. */
        private static final Map<String, String> EXTENSIONS;

        static {
            final Map<String, String> map = new HashMap<>();
            map.put("jpeg", "jpg");

            EXTENSIONS = Collections.unmodifiableMap(map);
        }

        /**
         * Instantiates a new item.
         *
         * @param id
         *            the id
         * @param label
         *            the label
         * @param mimeType
         *            the mime type
         * @param supplier
         *            the supplier
         */
        protected Item(final int id, final String label, final String mimeType, final Supplier<byte[]> supplier) {
            this.id = id;
            this.label = label;
            this.mimeType = mimeType;
            this.contentSupplier = supplier;
        }

        /**
         * Gets the id.
         *
         * @return the id
         */
        public int getId() {
            return this.id;
        }

        /**
         * Sets the id.
         *
         * @param id
         *            the new id
         */
        private Item setId(final int id) {
            this.id = id;
            return this;
        }

        /**
         * Gets the label.
         *
         * @return the label
         */
        public String getLabel() {
            return this.renameLabel(this.label);
        }

        /**
         * Gets the mime type.
         *
         * @return the mimeType
         */
        public String getMimeType() {
            if (this.mimeType == null) {
                return null;
            }
            return this.mimeType;
        }

        /**
         * Function to convert files with upper case extensions to lower case
         * extensions
         *
         * @param label
         * @return
         */
        public String renameLabel(final String label) {
            String target = label;
            final Matcher matcher = FILE_NAME_PATTERN.matcher(label);
            if (matcher.matches()) {
                String ext = matcher.group(1);
                if (null != EXTENSIONS.get(ext.toLowerCase())) {
                    ext = EXTENSIONS.get(ext.toLowerCase());
                }
                target = label.replaceFirst(Pattern.quote("." + matcher.group(1)) + "$", Matcher.quoteReplacement("." + ext.toLowerCase()));
            }
            return target;
        }

        /**
         * Gets the content.
         *
         * @return the content
         */
        @JsonIgnore
        public byte[] getContent() {
            if (null == this.content) {
                this.content = Optional.ofNullable(this.contentSupplier).map(s -> s.get()).orElse(null);
            }
            return Optional.ofNullable(this.content).map(bytes -> bytes.clone()).orElse(null);
        }

        @JsonIgnore
        public String getContentAsString() {
            return Optional.ofNullable(this.getContent()).map(bytes -> new String(bytes, StandardCharsets.UTF_8)).orElse(null);
        }

        /**
         * Gets the hash.
         *
         * @return the hash
         */
        @JsonIgnore
        public String getHash() {
            if (null == this.hash) {
                this.hash = HashUtil.hashAsString(this.getContent());
            }

            return this.hash;
        }

        @JsonIgnore
        public String getAbsolutePath() {
            return this.absolutePath;
        }

        public Item setAbsolutePath(final String path) {
            this.absolutePath = path;
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString() {
            return String.format(ITEM_FORMAT, this.id, this.label);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder() //
                    .append(this.id) //
                    .append(this.label) //
                    .toHashCode();
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            } else if (obj == null || this.getClass() != obj.getClass()) {
                return false;
            } else {
                final Item other = (Item) obj;
                return new EqualsBuilder() //
                        .append(this.id, other.id) //
                        .append(this.label, other.label) //
                        .isEquals();
            }
        }
    }

}
