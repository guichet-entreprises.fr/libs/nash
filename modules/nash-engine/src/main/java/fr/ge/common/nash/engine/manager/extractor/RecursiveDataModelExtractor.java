/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.extractor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.manager.parser.ElementParser;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class RecursiveDataModelExtractor.
 *
 * @author Christian Cougourdan
 */
public final class RecursiveDataModelExtractor implements IDataModelExtractor {

    /** The parser. */
    private final ElementParser<Map<String, Object>> parser = new ElementParser<>();

    private final IProvider provider;

    private boolean allowOverride = false;

    /**
     * Instantiates a new recursive data model extractor.
     */
    private RecursiveDataModelExtractor() {
        this(null);
    }

    /**
     * Instantiates a new recursive data model extractor.
     *
     * @param provider
     */
    private RecursiveDataModelExtractor(final IProvider provider) {
        this.provider = provider;
        this.parser.register(GroupElement.class, (model, elm, spec) -> this.subModel(model, elm));
        this.parser.register(DataElement.class, (model, elm, spec) -> this.extract(model, elm, spec));
    }

    public static RecursiveDataModelExtractor create(final IProvider provider) {
        return new RecursiveDataModelExtractor(provider);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Object> extract(final FormSpecificationData formSpecification) {

        if (null == formSpecification) {
            return Collections.emptyMap();
        }

        final Map<String, Object> model = this.extract(formSpecification, new HashMap<>());

        final String specId = formSpecification.getId();
        if (StringUtils.isEmpty(specId)) {
            return model;
        } else {
            return new HashMap<>(Collections.singletonMap(specId, model));
        }
    }

    public Map<String, Object> extract(final FormSpecificationData spec, final Map<String, Object> model) {
        if (null != spec) {
            this.parser.parse(model, spec);
        }
        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Object> extract(final IElement<?> element) {
        final Map<String, Object> model = new HashMap<>();

        this.parser.parse(model, element);

        return model;
    }

    public Map<String, Object> extract(final IElement<?> element, final Map<String, Object> model) {
        this.parser.parse(model, element);

        return model;
    }

    /**
     * Extract.
     *
     * @param model
     *            the model
     * @param element
     *            the element
     * @param spec
     *            the specification
     * @return the map
     */
    protected Map<String, Object> extract(final Map<String, Object> model, final IElement<?> element, final FormSpecificationData spec) {
        final DataElement dataElement = (DataElement) element;
        final String key = dataElement.getId();
        final Object value = model.get(key);

        if (null != value) {
            if (value instanceof Map) {
                throw new TechnicalException("Extracting data value : invalid path");
            } else if (!this.allowOverride) {
                throw new TechnicalException("Extracting data value : duplicate path");
            }
        }

        final IValueAdapter<?> type = ValueAdapterFactory.type(spec, dataElement);
        if (type == null) {
            model.put(key, null);
        } else {
            final String keySimplified = this.getKeyFromMultipleElement(key);
            final Object newValue = type.get(this.provider, dataElement);
            if (key.equals(keySimplified)) {
                model.put(key, newValue);
            } else {
                // -->This is a multiple data. We need to put a list of
                // values
                List<Object> dataValues = new ArrayList<Object>();
                if (null != model.get(keySimplified)) {
                    dataValues = CoreUtil.cast(model.get(keySimplified));
                }
                if (null == newValue) {
                    dataValues.add(StringUtils.EMPTY);
                } else {
                    dataValues.add(newValue);
                }
                model.put(keySimplified, dataValues);
            }
        }

        return model;
    }

    /**
     * Sub model.
     *
     * @param model
     *            the model
     * @param element
     *            the element
     * @return the map
     */
    @SuppressWarnings("unchecked")
    protected Map<String, Object> subModel(final Map<String, Object> model, final IElement<?> element) {
        final String key = element.getId();
        // -->Getting real key name for multiple element
        final String keySimplified = this.getKeyFromMultipleElement(key);
        Object newModelAsObject = model.get(key);
        Map<String, Object> newModel;

        if (key.equals(keySimplified)) {
            if (null == newModelAsObject) {
                newModel = new HashMap<>();
                model.put(keySimplified, newModel);
            } else {
                newModel = (Map<String, Object>) newModelAsObject;
            }
        } else {
            /*
             * Cardinality activated !!!! Add new model into model map
             */
            newModelAsObject = model.get(keySimplified);
            final List<Object> lst = null == newModelAsObject ? new ArrayList<Object>() : CoreUtil.cast(newModelAsObject);
            newModel = new HashMap<>();
            lst.add(newModel);
            model.put(keySimplified, lst);
        }

        return newModel;
    }

    /**
     * Return the real data name for a multiple element.
     *
     * @param key
     *            the element identifier
     * @return the real key name
     */
    private String getKeyFromMultipleElement(final String key) {
        final Pattern pattern = Pattern.compile("\\[.*?\\]");
        final Matcher matcher = pattern.matcher(key);
        return matcher.replaceAll("");
    }

    /**
     * @param allowOverride
     *            the allowOverride to set
     */
    public RecursiveDataModelExtractor setAllowOverride(final boolean allowOverride) {
        this.allowOverride = allowOverride;
        return this;
    }

}
