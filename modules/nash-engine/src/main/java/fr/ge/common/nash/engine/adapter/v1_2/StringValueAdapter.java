/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import fr.ge.common.nash.engine.adapter.Option;

/**
 * The String value adapter.
 *
 * @author Christian Cougourdan
 */
public class StringValueAdapter extends fr.ge.common.nash.engine.adapter.v1_0.StringValueAdapter {

    /** min. */
    @Option(description = "Value minimum size")
    private int min = 0;

    /** max. */
    @Option(description = "Value maximum size")
    private int max = -1;

    /** Pattern. */
    @Option(description = "Regular expression")
    private String pattern;

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return StringValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * Getter on attribute {@link #min}.
     *
     * @return the min
     */
    public int getMin() {
        return this.min;
    }

    /**
     * Setter on attribute {@link #min}.
     *
     * @param min
     *            the min to set
     */
    public void setMin(final int min) {
        this.min = min;
    }

    /**
     * Getter on attribute {@link #max}.
     *
     * @return the max
     */
    public int getMax() {
        return this.max;
    }

    /**
     * Setter on attribute {@link #max}.
     *
     * @param max
     *            the max to set
     */
    public void setMax(final int max) {
        this.max = max;
    }

    /**
     * Accesseur sur l'attribut {@link #pattern}.
     *
     * @return String pattern
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * Mutateur sur l'attribut {@link #pattern}.
     *
     * @param pattern
     *            la nouvelle valeur de l'attribut pattern
     */
    public void setPattern(final String pattern) {
        this.pattern = pattern;
    }
}
