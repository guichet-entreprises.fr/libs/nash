/**
 * 
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.meta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * @author bsadil
 *
 */
@XmlRootElement(name = "meta")
@XmlAccessorType(XmlAccessType.NONE)
public class MetaElement implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The name. */
    @XmlAttribute
    private String name;

    /** The label. */
    @XmlValue
    private String value;

    /**
     * 
     */
    public MetaElement() {
        super();
        // Nothing to do
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public MetaElement setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param message
     *            the value to set
     */
    public MetaElement setValue(String value) {
        this.value = value;
        return this;
    }
}
