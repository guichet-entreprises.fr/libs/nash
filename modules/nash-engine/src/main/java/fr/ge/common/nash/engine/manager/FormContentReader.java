/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.CoreUtil;

/**
 * HTTP request reader.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class FormContentReader {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FormContentReader.class);

    /** The Constant JSON_GETTERS. */
    private static final Map<Predicate<Object>, BiFunction<String, Object, Map<String, String[]>>> JSON_GETTERS;

    static {
        final Map<Predicate<Object>, BiFunction<String, Object, Map<String, String[]>>> map = new LinkedHashMap<>();

        map.put( //
                Map.class::isInstance, //
                (path, value) -> readJson(path, CoreUtil.cast(value)) //
        );

        map.put( //
                List.class::isInstance, //
                (path, value) -> {
                    final Map<String, String[]> valueByPath = new HashMap<>();

                    final List<Object> lst = CoreUtil.cast(value);
                    if (lst.stream().allMatch(Collection.class::isInstance)) {
                        for (int idx = 0; idx < lst.size(); idx++) {
                            readJsonValue(String.format("%s[%d]", path, idx), lst.get(idx)).ifPresent(valueByPath::putAll);
                        }
                    } else if (lst.stream().allMatch(Map.class::isInstance)) {
                        final Collection<String> keys = Arrays.asList("id", "label");
                        if (lst.stream().allMatch(o -> {
                            final Map<String, Object> m = CoreUtil.cast(o);
                            return m.keySet().containsAll(keys) && m.size() == 2;
                        })) {
                            /*
                             * TODO had to remove this after team discussion
                             */
                            final Map<String, Object> newValue = new HashMap<>();
                            lst.forEach(o -> {
                                final Map<String, Object> m = CoreUtil.cast(o);
                                newValue.put((String) m.get("id"), m.get("label"));
                            });

                            valueByPath.putAll(readJson(path, newValue));
                        } else {
                            for (int idx = 0; idx < lst.size(); idx++) {
                                readJsonValue(String.format("%s[%d]", path, idx), lst.get(idx)).ifPresent(valueByPath::putAll);
                            }
                        }
                    } else {
                        valueByPath.put(path, lst.stream().map(String::valueOf).collect(Collectors.toList()).toArray(new String[] {}));
                    }

                    return valueByPath;
                } //
        );

        map.put( //
                String.class::isInstance, //
                (path, value) -> Collections.singletonMap(path, new String[] { (String) value }) //
        );

        map.put( //
                ((Predicate<Object>) Long.class::isInstance).or(Boolean.class::isInstance).or(Arrays.class::isInstance), //
                (path, value) -> Collections.singletonMap(path, new String[] { String.valueOf(value) }) //
        );

        map.put( //
                Integer.class::isInstance, //
                (path, value) -> Collections.singletonMap(path, new String[] { String.valueOf(value) }) //
        );

        JSON_GETTERS = Collections.unmodifiableMap(map);
    }

    /**
     * Constructor.
     */
    public FormContentReader() {
        // Nothing to do.
    }

    /**
     * Reads the HTTP request data.
     *
     * @param request
     *            HTTP servlet request
     * @param excludes
     *            request parameters to exclude
     * @return the HTTP request data
     */
    public static FormContentData read(final HttpServletRequest request, final String... excludes) {
        final Map<String, String[]> params = new LinkedHashMap<>(request.getParameterMap());
        if (ArrayUtils.isNotEmpty(excludes)) {
            Arrays.stream(excludes).forEach(params::remove);
        }
        return new FormContentData(request.getParameterMap());
    }

    /**
     * Reads parameters from map.
     *
     * @param requestParams
     *            the request params
     * @return the Form Content Data
     */
    public static FormContentData read(final Map<String, String[]> requestParams) {
        return new FormContentData(requestParams);
    }

    /**
     * Reads the JSON data.
     *
     * @param json
     *            a JSON object
     * @return the JSON data
     */
    @SuppressWarnings("unchecked")
    public static FormContentData read(final byte[] json) {
        try {
            final Map<String, Object> jsonAsMap = new ObjectMapper().readValue(json, HashMap.class);
            return new FormContentData(readJson(StringUtils.EMPTY, jsonAsMap));
        } catch (final IOException ex) {
            LOGGER.warn("Unable to read source JSON", ex);
        }
        return new FormContentData(Collections.emptyMap());
    }

    /**
     * Reads the JSON data recursively.
     *
     * @param basePath
     *            the base path
     * @param root
     *            the root
     * @return the map
     */
    private static Map<String, String[]> readJson(final String basePath, final Map<String, Object> root) {
        final Map<String, String[]> valueByPath = new TreeMap<>();

        for (final Entry<String, Object> entry : root.entrySet()) {
            String path = (StringUtils.isEmpty(basePath) ? "" : basePath + '.') + entry.getKey();
            final Object value = entry.getValue();

            readJsonValue(path, value).ifPresent(valueByPath::putAll);
        }

        return valueByPath;
    }

    private static Optional<Map<String, String[]>> readJsonValue(final String path, final Object value) {
        return JSON_GETTERS.entrySet().stream() //
                .filter(getter -> getter.getKey().test(value)) //
                .findFirst() //
                .map(getter -> getter.getValue().apply(path, value));
    }

    public static FormContentData read(final String currentPath, final Map<String, Object> value) {
        return new FormContentData(readJson(currentPath, value));
    }
}
