package fr.ge.common.nash.engine.mapping.form.v1_2.description;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * Represent a step or a yield element.
 *
 * @author Christian Cougourdan
 */
public interface IFlowElement extends IXml {

}
