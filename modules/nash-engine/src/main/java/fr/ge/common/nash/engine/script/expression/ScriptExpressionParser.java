/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script.expression;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import fr.ge.common.nash.engine.script.expression.translator.BooleanOperatorsTranslator;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;

/**
 * Nash script expressions parser.
 *
 * @author Christian Cougourdan
 */
@Component
public class ScriptExpressionParser {

    /** The translators. */
    private final Function<String, String> translators;

    /** The local scoped translators. */
    private final Function<String, String> localScopedTranslators;

    /** The Constant DEPENDENCIES_PATTERN. */
    private static final Pattern DEPENDENCIES_PATTERN = Pattern
            .compile("(?<!\\w)_(" + Arrays.stream(Scopes.values()).map(Scopes::toString).collect(Collectors.joining("|")) + ")([.][a-zA-Z_]\\w+(?:[.][a-zA-Z_]\\w+)*)?");

    /**
     * Instantiates a new script expression parser.
     */
    public ScriptExpressionParser() {
        final Function<String, String> booleanOperatorsTranslator = new BooleanOperatorsTranslator();

        this.translators = new VariableScopeTranslator().andThen(booleanOperatorsTranslator);
        this.localScopedTranslators = new VariableScopeTranslator(Scopes.PAGE).andThen(booleanOperatorsTranslator);
    }

    /**
     * Parses the specified expression without local scope.
     *
     * @param expr
     *            the expr
     * @return the script expression
     */
    public ScriptExpression parse(final String expr) {
        return this.parse(expr, false);
    }

    /**
     * Parses the specified expression.
     *
     * @param expression
     *            the expression
     * @param withLocalScope
     *            the with local scope
     * @return the script expression
     */
    public ScriptExpression parse(final String expression, final boolean withLocalScope) {
        final String translatedExpression = withLocalScope ? this.localScopedTranslators.apply(expression) : this.translators.apply(expression);

        return new ScriptExpression(translatedExpression) //
                .setText(expression) //
                .setDependencies(this.extractDependencies(translatedExpression));
    }

    /**
     * Extract dependencies.
     *
     * @param expression
     *            the expression
     * @return the map
     */
    private Map<String, Collection<String>> extractDependencies(final String expression) {
        final Map<String, Collection<String>> dependenciesByScope = new HashMap<>();

        final Matcher matcher = DEPENDENCIES_PATTERN.matcher(expression);
        while (matcher.find()) {
            Collection<String> dependencies = dependenciesByScope.get(matcher.group(1));
            if (null == dependencies) {
                dependenciesByScope.put(matcher.group(1), dependencies = new HashSet<>());
            }
            dependencies.add(matcher.group());
        }

        return dependenciesByScope;
    }

}
