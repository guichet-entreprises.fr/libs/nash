/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.support.thymeleaf;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.messageresolver.AbstractMessageResolver;
import org.thymeleaf.util.Validate;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.support.i18n.MessageReader;

/**
 * Nash message resolver, looking for message inside records.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class NashMessageResolver extends AbstractMessageResolver {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(NashMessageResolver.class);

    /** Message key template locale. */
    private static final String MSG_KEY_TEMPLATE_LOCALE = "message with key \"{}\" for template \"{}\" and locale \"{}\".";

    /** Could not resolve message. */
    private static final String COULD_NOT_RESOLVE_MSG = "[THYMELEAF][{}] Could not resolve " + MSG_KEY_TEMPLATE_LOCALE;

    /** Resolving message. */
    private static final String RESOLVING_MSG = "[THYMELEAF][{}] Resolving " + MSG_KEY_TEMPLATE_LOCALE;

    /** Resolved message. */
    private static final String RESOLVED_MSG = "[THYMELEAF][{}] Resolved " + MSG_KEY_TEMPLATE_LOCALE + " with the ";

    /** Resolved message with specification. */
    private static final String RESOLVED_MSG_SPEC = RESOLVED_MSG + "specification messages.";

    /** Resolved message with type. */
    private static final String RESOLVED_MSG_TYPE = RESOLVED_MSG + "type messages.";

    /** Resolved message with UI. */
    private static final String RESOLVED_MSG_UI = RESOLVED_MSG + "UI messages.";

    /** Specification loader. */
    private static final String SPEC_LOADER = "loader";

    /** Type. */
    private static final String TYPE = "type";

    private final Map<TypeCacheKey, MessageReader> cacheTypeMessageReaders = new HashMap<>();

    /**
     * Constructor.
     */
    public NashMessageResolver() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createAbsentMessageRepresentation(final ITemplateContext context, final Class<?> origin, final String key, //
            final Object[] messageParameters) {
        return NashMessageReader.getReader().getFormatter().format(key, messageParameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String resolveMessage(final ITemplateContext context, final Class<?> origin, final String key, //
            final Object[] messageParameters) {
        Validate.notNull(context, "Arguments cannot be null");
        Validate.notNull(context.getLocale(), "Locale in context cannot be null");
        Validate.notNull(key, "Message key cannot be null");

        final String templateName = context.getTemplateData().getTemplate();
        final Locale locale = context.getLocale();

        final Object[] logArgs = new Object[] { TemplateEngine.threadIndex(), key, templateName, locale };
        LOGGER.trace(RESOLVING_MSG, logArgs);

        // Search in the type translations
        String resolvedMessage = this.translateByType(locale, key, context);

        // Search in the specification translations
        if (resolvedMessage == null || key.equals(resolvedMessage)) {
            resolvedMessage = this.translateBySpec(locale, key, context);
            LOGGER.trace("Translate {} with spec and locale {} : {}", key, locale, resolvedMessage);
        } else {
            LOGGER.trace(RESOLVED_MSG_TYPE, logArgs);
        }

        // Search in the message source translations
        if (resolvedMessage == null || key.equals(resolvedMessage)) {
            resolvedMessage = NashMessageReader.getReader().getFormatter().format(key, messageParameters);
        } else {
            LOGGER.trace(RESOLVED_MSG_SPEC, logArgs);
        }

        // Return the resolved message
        if (resolvedMessage == null) {
            LOGGER.trace(COULD_NOT_RESOLVE_MSG, logArgs);
            return null;
        } else {
            LOGGER.trace(RESOLVED_MSG_UI, logArgs);
            return resolvedMessage;
        }
    }

    /**
     * Translate by type.
     *
     * @param locale
     *            the locale
     * @param key
     *            the key
     * @param context
     *            the context
     * @return the translation
     */
    private String translateByType(final Locale locale, final String key, final ITemplateContext context) {
        final Object evaluationRootType = context.getVariable(TYPE);
        IValueAdapter<?> type = null;
        if (evaluationRootType instanceof IValueAdapter) {
            type = (IValueAdapter<?>) evaluationRootType;
        }
        if (type != null) {
            final TypeCacheKey cacheKey = new TypeCacheKey(type, locale);
            MessageReader reader = this.cacheTypeMessageReaders.get(cacheKey);
            if (null == reader) {
                this.cacheTypeMessageReaders.put(cacheKey, reader = NashMessageReader.getReader(type, locale));
            }
            return reader.getFormatter().formatNullIfMissing(key);
        } else {
            return null;
        }
    }

    /**
     * Translate by specification.
     *
     * @param locale
     *            the locale
     * @param key
     *            the key
     * @param context
     *            the context
     * @return the translation
     */
    private String translateBySpec(final Locale locale, final String key, final ITemplateContext context) {
        String resolvedMessage = null;
        final Object evaluationRootSpecLoader = context.getVariable(SPEC_LOADER);
        SpecificationLoader specLoader = null;
        if (evaluationRootSpecLoader instanceof SpecificationLoader) {
            specLoader = (SpecificationLoader) evaluationRootSpecLoader;
        }
        if (specLoader != null) {
            resolvedMessage = NashMessageReader.getReader(specLoader).getFormatter().formatNullIfMissing(key);
        }
        return resolvedMessage;
    }

    private static class TypeCacheKey {

        private final Class<?> typeClass;

        private final Locale locale;

        public TypeCacheKey(final IValueAdapter<?> type, final Locale locale) {
            this.typeClass = type.getClass();
            this.locale = locale;
        }

        @Override
        public boolean equals(final Object obj) {
            if (!(obj instanceof TypeCacheKey)) {
                return false;
            }

            final TypeCacheKey other = (TypeCacheKey) obj;
            return new EqualsBuilder() //
                    .append(this.typeClass, other.typeClass) //
                    .append(this.locale, other.locale) //
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder() //
                    .append(this.typeClass) //
                    .append(this.locale) //
                    .toHashCode();
        }

    }

}
