/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.validation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.loader.LocalEntityResolver;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.nash.engine.validation.Error.Level;
import fr.ge.common.support.i18n.MessageFormatter;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class FormSpecificationValidator.
 *
 * @author Christian Cougourdan
 */
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FormSpecificationValidator {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FormSpecificationValidator.class);

    private static final String BASENAME = String.format("po/%s.ValidationMessages", FormSpecificationValidator.class.getPackage().getName());

    /** The data validator. */
    private AbstractValidator dataValidator;

    /** The description validator. */
    private AbstractValidator descriptionValidator;

    /** The process validator. */
    private AbstractValidator processValidator;

    /** The locale. */
    private final Locale locale;

    /** The provider. */
    private final IProvider provider;

    /** The message formatter. */
    private final MessageFormatter messageFormatter;

    /**
     * Instantiates a new form specification validator.
     *
     * @param locale
     *            the locale
     * @param provider
     *            the provider
     */
    public FormSpecificationValidator(final Locale locale, final IProvider provider) {
        this.locale = locale;
        this.provider = provider;
        this.messageFormatter = NashMessageReader.getReader(BASENAME, locale).getFormatter();
        this.dataValidator = new FormSpecificationDataValidator(this.provider, this.messageFormatter);
        this.descriptionValidator = new FormSpecificationDescriptionValidator(this.provider, this.messageFormatter);
        this.processValidator = new FormSpecificationProcessValidator(this.provider, this.messageFormatter);
    }

    /**
     * Validate.
     *
     * @return the errors
     */
    public Errors validate() {
        final Errors errors = new Errors();
        return this.validate(errors);
    }

    /**
     * Validate.
     *
     * @param errors
     *            the errors
     * @return the errors
     */
    public Errors validate(final Errors errors) {
        this.validate("description.xml", errors, this.descriptionValidator, null);
        if (errors.hasErrors()) {
            return errors;
        }

        String version = ValueAdapterFactory.getLatestValueAdapterVersion();
        final FormSpecificationDescription formSpecificationElement = this.provider.asBean("description.xml", FormSpecificationDescription.class);
        if (formSpecificationElement != null) {
            version = formSpecificationElement.getVersion();
        }

        final SpecificationLoader loader = SpecificationLoader.create(this.provider);
        for (final StepElement step : loader.stepsMgr().findAll()) {
            if (step.getPreprocess() == null) {
                if (step.getData() == null) {
                    errors.add(new Error(this.messageFormatter.format("No preprocess or data file specified for step '{0}'", step.getId())));
                } else {
                    this.validate(step.getData(), errors, this.dataValidator, version);
                }
            } else {
                this.validate(step.getPreprocess(), errors, this.processValidator, version);
            }

            if (null != step.getPostprocess()) {
                this.validate(step.getPostprocess(), errors, this.processValidator, version);
            }
        }

        return errors;
    }

    /**
     * Validate.
     *
     * @param resourceName
     *            the resource name
     * @param errors
     *            the errors
     * @param validator
     *            the validator
     * @param version
     *            the version
     * @return the errors
     */
    public Errors validate(final String resourceName, final Errors errors, final AbstractValidator validator, final String version) {
        LOGGER.debug("Validating resource '{}'", resourceName);
        final byte[] resourceAsBytes = this.provider.asBytes(resourceName);
        if (ArrayUtils.isEmpty(resourceAsBytes)) {
            errors.add( //
                    new Error( //
                            Level.ERROR, //
                            this.messageFormatter.format( //
                                    "Missing file '{0}'", //
                                    resourceName //
                            ) //
                    ) //
            );
        } else {
            try {
                errors.merge(resourceName, this.validateXml(resourceAsBytes));
                errors.merge(resourceName, validator.validate(resourceName, version));
            } catch (final TechnicalException ex) {
                final SAXParseException saxException = CoreUtil.findException(ex, SAXParseException.class);
                String msg = "unexpected error";
                if (null != saxException) {
                    msg = String.format("[%d,%d] %s", saxException.getLineNumber(), saxException.getColumnNumber(), saxException.getMessage());
                }
                LOGGER.warn("Validation error on resource '{}' : {}", resourceName, msg, ex);
                errors.add(resourceName, new Error(Level.FATAL, msg));
            }
        }
        return errors;
    }

    /**
     * Validate xml.
     *
     * @param content
     *            the content
     * @return the errors
     */
    Errors validateXml(final byte[] content) {
        final Errors errors = new Errors();

        final DocumentBuilder documentBuilder = this.createDocumentBuilder(errors);

        try (InputStream in = new ByteArrayInputStream(content)) {
            documentBuilder.parse(in);
        } catch (final SAXParseException ex) {
            LOGGER.debug("Parse error : {}", ex.getMessage());
            LOGGER.trace(StringUtils.EMPTY, ex);
        } catch (final SAXException ex) {
            errors.add(new Error(Level.FATAL, ex.getMessage()));
            LOGGER.trace(StringUtils.EMPTY, ex);
        } catch (final IOException ex) {
            throw new TechnicalException("Error while loading XML document", ex);
        }

        return errors;
    }

    /**
     * Creates the document builder.
     *
     * @param errors
     *            the errors
     * @return the document builder
     */
    private DocumentBuilder createDocumentBuilder(final Errors errors) {
        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(true);
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage", "http://www.w3.org/2001/XMLSchema");
        documentBuilderFactory.setAttribute("http://apache.org/xml/properties/locale", this.locale);

        final LocalEntityResolver localEntityResolver = new LocalEntityResolver();

        final DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (final ParserConfigurationException ex) {
            throw new TechnicalException("Unable to create document builder", ex);
        }

        documentBuilder.setEntityResolver(localEntityResolver);
        documentBuilder.setErrorHandler(new ErrorHandler() {

            @Override
            public void warning(final SAXParseException exception) throws SAXException {
                errors.add(new Error(Level.WARN, this.build(exception)));
            }

            @Override
            public void error(final SAXParseException exception) throws SAXException {
                errors.add(new Error(Level.ERROR, this.build(exception)));
            }

            @Override
            public void fatalError(final SAXParseException exception) throws SAXException {
                errors.add(new Error(Level.FATAL, this.build(exception)));
            }

            private String build(final SAXParseException ex) {
                return String.format( //
                        "[%d,%d] %s", //
                        ex.getLineNumber(), //
                        ex.getColumnNumber(), //
                        ex.getMessage() //
                );
            }
        });

        return documentBuilder;
    }

    /**
     * Setter on attribute {@link #dataValidator}.
     *
     * @param dataValidator
     *            the new value of attribute dataValidator
     */
    public void setDataValidator(final AbstractValidator dataValidator) {
        this.dataValidator = dataValidator;
    }

    /**
     * Setter on attribute {@link #descriptionValidator}.
     *
     * @param descriptionValidator
     *            the new value of attribute descriptionValidator
     */
    public void setDescriptionValidator(final AbstractValidator descriptionValidator) {
        this.descriptionValidator = descriptionValidator;
    }

    /**
     * Setter on attribute {@link #processValidator}.
     *
     * @param processValidator
     *            the new value of attribute processValidator
     */
    public void setProcessValidator(final AbstractValidator processValidator) {
        this.processValidator = processValidator;
    }

}
