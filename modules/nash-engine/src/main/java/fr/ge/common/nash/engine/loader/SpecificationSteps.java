/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.loader;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.IFlowElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.YieldElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;

/**
 * Record steps manager.
 *
 * @author Christian Cougourdan
 */
public class SpecificationSteps {

    /** The loader. */
    private final SpecificationLoader loader;

    /**
     * Instantiates a new specification steps.
     *
     * @param loader
     *            the loader
     */
    SpecificationSteps(final SpecificationLoader loader) {
        this.loader = loader;
    }

    /**
     * Retrieve steps list.
     *
     * @return list of step element
     */
    public List<StepElement> findAll() {
        return Optional.ofNullable(this.loader.description()).map(FormSpecificationDescription::getSteps).map(StepsElement::getElements).orElse(new ArrayList<>());
    }

    /**
     * Retrieve specific step, identify by its index in steps list.
     *
     * @param stepIndex
     *            step index to retrieve
     * @return corresponding step element
     */
    public StepElement find(final int stepIndex) {
        final List<StepElement> steps = this.findAll();
        if (stepIndex >= 0 && stepIndex < steps.size()) {
            return steps.get(stepIndex);
        } else {
            return null;
        }
    }

    /**
     * Retrieve specific step, identify by its ID.
     *
     * @param id
     *            step ID to retrieve
     * @return corresponding step element
     */
    public StepElement find(final String id) {
        return this.findFirst(step -> step.getId().equals(id));
    }

    /**
     * Search for the first step element corresponding to specified predicate.
     *
     * @param predicate
     *            search predicate
     * @return found step element
     */
    public StepElement findFirst(final Predicate<StepElement> predicate) {
        return this.findAll().stream() //
                .filter(predicate) //
                .findFirst() //
                .orElse(null);
    }

    /**
     * Search for the last step element corresponding to specified predicate.
     *
     * @param predicate
     *            search predicate
     * @return found step element
     */
    public StepElement findLast(final Predicate<StepElement> predicate) {
        return this.findAll().stream() //
                .filter(predicate) //
                .reduce((prev, elm) -> elm) //
                .orElse(null);
    }

    /**
     * Search for the first step element for specified engine user having one of
     * specified status.
     *
     * @param user
     *            step engine user
     * @param status
     *            required status
     * @return found step element
     */
    public StepElement findFirst(final String user, final StepStatusEnum... status) {
        Predicate<StepElement> predicate = step -> Arrays.stream(status).map(StepStatusEnum::getStatus).anyMatch(code -> code.equals(step.getStatus()));
        if (null != user) {
            predicate = predicate.and(step -> user.equals(step.getUser()));
        }
        return this.findFirst(predicate);
    }

    /**
     * Search for the last step element for specified engine user having one of
     * specified status.
     *
     * @param user
     *            step engine user
     * @param status
     *            required status
     * @return found step element
     */
    public StepElement findLast(final String user, final StepStatusEnum... status) {
        Predicate<StepElement> predicate = step -> Arrays.stream(status).map(StepStatusEnum::getStatus).anyMatch(code -> code.equals(step.getStatus()));
        if (null != user) {
            predicate = predicate.and(step -> user.equals(step.getUser()));
        }
        return this.findLast(predicate);
    }

    /**
     * Retrieve active step.
     *
     * @return step element
     */
    public StepElement current() {
        final List<StepElement> steps = this.findAll();

        return steps.stream() //
                .filter(s -> StepStatusEnum.ERROR.getStatus().equals(s.getStatus()) || //
                        StepStatusEnum.TO_DO.getStatus().equals(s.getStatus()) || //
                        StepStatusEnum.IN_PROGRESS.getStatus().equals(s.getStatus()) //
                ) //
                .findFirst() //
                .orElse(steps.get(steps.size() - 1));
    }

    /**
     * Check for redirection script in post processes.
     *
     * @param step
     *            the step
     * @return true, if successful
     */
    public boolean hasRedirectionScript(final StepElement step) {
        final List<IProcess> processes = this.loader.processes().findPostprocesses(step).getProcesses();
        return hasRedirectionScript(processes);
    }

    /** The Constant REDIRECTION_PREDICATE. */
    private static final Predicate<IProcess> REDIRECTION_PREDICATE = ((Predicate<IProcess>) process -> process instanceof ProcessElement) //
            .and(process -> "redirect".equals(((ProcessElement) process).getType())) //
            .or(process -> hasRedirectionScript(process.getChildren()));

    /**
     * Checks for redirection script.
     *
     * @param processes
     *            the processes
     * @return true, if successful
     */
    private static boolean hasRedirectionScript(final List<IProcess> processes) {
        if (null == processes) {
            return false;
        }

        return processes.stream().anyMatch(REDIRECTION_PREDICATE);
    }

    /**
     * Retrieve steps list filtering by the corresponding engine user type.
     *
     * @param engineUserType
     *            The engine user type
     * @return
     */
    public List<StepElement> findAll(final String engineUserType) {
        return this.findAll().stream().filter(s -> engineUserType.equals(s.getUser())).collect(Collectors.toList());
    }

    /**
     * Add a sealing request for a step.
     *
     * @param stepIndex
     *            the step index
     */
    public void requestSealing(final int stepIndex) {
        this.requestStatusFromStep(stepIndex, StepStatusEnum.SEALED);
    }

    /**
     * Ends a step at a specific index.
     *
     * @param stepIndex
     *            the step index
     */
    public void requestDone(final int stepIndex) {
        this.requestStatusFromStep(stepIndex, StepStatusEnum.DONE);
    }

    /**
     * Update status from a step index.
     *
     * @param stepIndex
     *            the step index
     * @param nextStatus
     *            the next status
     */
    private void requestStatusFromStep(final int stepIndex, final StepStatusEnum nextStatus) {
        final int currentPosition = this.current().getPosition();
        if (stepIndex <= currentPosition) {
            // -->Update all previous steps from 0 to step index
            this.loader.updateStepStatus(stepIndex, nextStatus, this.loader.description().getAuthor());
            this.loader.flush();
        } else {
            throw new TechnicalException("Cannot update status to " + nextStatus.getStatus() + " for the step position '" + stepIndex + "' different than current position '" + currentPosition + "'");
        }
    }

    public Collection<String> findResources(final int stepIndex) {
        return this.findResources(this.find(stepIndex));
    }

    public Collection<String> findResources(final String stepId) {
        return this.findResources(this.find(stepId));
    }

    public Collection<String> findResources(final StepElement step) {
        if (null == step) {
            return Collections.emptyList();
        }

        final Path parent = Paths.get(step.getData()).getParent();
        if (null == parent) {
            // step has no folder
            final Collection<String> resources = new LinkedHashSet<>();
            resources.add(step.getData());
            resources.addAll(this.findResourcesFromProcess(step.getPreprocess()));
            resources.addAll(this.findResourcesFromProcess(step.getPostprocess()));
            return resources;
        } else {
            // list all resources present in step folder
            return this.loader.getProvider().resources(parent.toString() + "/*");
        }

    }

    private Collection<? extends String> findResourcesFromProcess(final String processResourcePath) {
        if (null == processResourcePath) {
            return Collections.emptyList();
        }

        final Collection<String> resources = new LinkedHashSet<>();
        resources.add(processResourcePath);

        final FormSpecificationProcess processes = this.loader.getProvider().asBean(processResourcePath, FormSpecificationProcess.class);
        for (final IProcess process : processes.getProcesses()) {
            resources.addAll(this.findResourcesFromProcess(process));
        }

        return resources;
    }

    private Collection<? extends String> findResourcesFromProcess(final IProcess process) {
        final Collection<String> resources = new LinkedHashSet<>();

        if (null != process.getInput()) {
            resources.addAll(splittedResourceList(process.getInput()));
        }

        if (process instanceof ProcessElement) {
            final ProcessElement elm = (ProcessElement) process;
            if (null != elm.getOutput()) {
                resources.add(elm.getOutput());
            }
            if (null != elm.getScript()) {
                resources.add(elm.getScript());
            }
        } else if (null != process.getChildren()) {
            for (final IProcess child : process.getChildren()) {
                resources.addAll(this.findResourcesFromProcess(child));
            }
        }

        return resources;
    }

    public static Collection<String> splittedResourceList(final String resourceList) {
        return Arrays.asList(resourceList.split("[ ]*,[ ]*"));
    }

    public void cleanFromId(final String id) {
        final List<IFlowElement> lst = this.loader.description().getFlow();
        for (int i = 0; i < lst.size(); i++) {
            final IFlowElement item = lst.get(i);
            if (item instanceof StepElement) {
                final StepElement step = (StepElement) item;
                if (id.equals(step.getId())) {
                    this.loader.description().setFlow(lst.subList(0, i));
                    return;
                }
            }
        }
    }

    public String addStep(final StepElement newStep) {
        final FormSpecificationDescription description = this.loader.description();

        if (StringUtils.isBlank(newStep.getStatus())) {
            newStep.setStatus(StepStatusEnum.TO_DO.getStatus());
        }

        final List<StepElement> steps = description.getSteps().getElements();
        final String newStepId = newStep.getId();
        final long idx = steps.stream() //
                .map(StepElement::getId) //
                .filter(Pattern.compile("^" + newStepId + "(?:|_0*[1-9][0-9]*)$").asPredicate()) //
                .count() //
                + 1;

        newStep.setId(String.format("%s_%04d", newStepId, idx));

        this.loader.stepsMgr().cleanFromId(newStep.getId());

        final String newStepFolder = String.format("%s-%04d", newStepId, idx);
        newStep.setData(this.addPrefix(newStepFolder, newStep.getData()));
        newStep.setPreprocess(this.addPrefix(newStepFolder, newStep.getPreprocess()));
        newStep.setPostprocess(this.addPrefix(newStepFolder, newStep.getPostprocess()));

        final List<IFlowElement> flow = description.getFlow();
        flow.add(newStep);
        description.setFlow(flow);

        this.loader.flush();

        return newStepFolder;
    }

    private String addPrefix(final String prefix, final String path) {
        if (null == path) {
            return null;
        } else if (path.startsWith("/")) {
            return path.substring(1);
        } else {
            return prefix + "/" + path;
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(SpecificationSteps.class);

    /**
     * Find active step, ie the first one which is not DONE or SEALED and assignated
     * to specified "user".
     *
     * @param engineUser
     *            engine user
     * @return active step
     */
    public StepElement getActiveStep(final String engineUser) {
        final StepElement step = this.current();
        if (null == step) {
            LOGGER.debug("No active step");
        } else if (null == step.getStatus()) {
            LOGGER.debug("Active step {} has no status, set to {}", step.getId(), StepStatusEnum.TO_DO);
            step.setStatus(StepStatusEnum.TO_DO.getStatus());
            return step;
        } else {
            final StepStatusEnum status = StepStatusEnum.fromCode(step.getStatus());
            if (null == status) {
                LOGGER.warn("Active step {} has unknown status : {}", step.getStatus());
            } else if (status.isClosed()) {
                LOGGER.debug("Active step {} already done", step.getId());
            } else if (null != engineUser && !engineUser.equals(step.getUser())) {
                LOGGER.debug("Active step {} does not match engine user \"{}\" : {}", step.getId(), engineUser, step.getUser());
            } else {
                return step;
            }
        }

        return null;
    }

    /**
     * Return all {@link YieldElement} following specified step.
     *
     * @param step
     *            offset step
     * @return list of following {@link YieldElement}
     */
    public List<YieldElement> findFollowingYield(final StepElement step) {
        final FormSpecificationDescription desc = this.loader.description();
        final List<IFlowElement> flow = desc.getFlow();
        final int offset = flow.indexOf(step);

        final List<YieldElement> yields = new ArrayList<>();
        for (int idx = offset + 1; idx < flow.size() && flow.get(idx) instanceof YieldElement; idx++) {
            yields.add((YieldElement) flow.get(idx));
        }

        return yields;
    }

    /**
     * Update step positions.
     */
    public void updatePositions() {
        final StepsElement steps = this.loader.description().getSteps();
        if (null != steps && null != steps.getElements()) {
            final List<StepElement> elms = steps.getElements();
            for (int idx = 0; idx < elms.size(); idx++) {
                elms.get(idx).setPosition(idx);
            }
        }
    }

}
