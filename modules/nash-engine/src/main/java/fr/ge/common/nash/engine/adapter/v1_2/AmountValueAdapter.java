/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.function.TriFunction;
import fr.ge.common.nash.engine.adapter.AbstractReferentialValueAdapter;
import fr.ge.common.nash.engine.adapter.Option;
import fr.ge.common.nash.engine.adapter.v1_2.value.Amount;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.utils.CoreUtil;

/**
 * The Amount value adapter.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class AmountValueAdapter extends AbstractReferentialValueAdapter<Amount> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AmountValueAdapter.class);

    /** Converters. */
    private static final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> CONVERTERS;

    static {
        final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> map = new HashMap<>();

        map.put(Amount.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final Amount amount = (Amount) value;

            if (amount.getAmount() != null) {
                builder.addTextValue("amount", amount.getAmount());
            }

            if (!StringUtils.isEmpty(amount.getMonetary())) {
                builder.addTextValue("monetary", amount.getMonetary());
            }

            if (!StringUtils.isEmpty(amount.getCurrency())) {
                builder.addTextValue("currency", amount.getCurrency());
            }

            return builder.build();
        });

        map.put(FormContentData.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final FormContentData data = (FormContentData) value;

            Arrays.stream(new String[] { "amount", "monetary", "currency" }) //
                    .filter(key -> StringUtils.isNotEmpty(data.withPath(key).asString())) //
                    .forEach(key -> builder.addTextValue(key, data.withPath(key).asString()));

            return builder.build();
        });

        map.put(DataBinding.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final DataBinding data = (DataBinding) value;

            Arrays.stream(new String[] { "amount", "monetary", "currency" }) //
                    .filter(key -> StringUtils.isNotEmpty(data.withPath(key).asString())) //
                    .forEach(key -> builder.addTextValue(key, data.withPath(key).asString()));

            return builder.build();
        });

        map.put(HashMap.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final Map<String, String> data = CoreUtil.cast(value);

            Arrays.stream(new String[] { "amount", "monetary", "currency" }) //
                    .filter(key -> data.containsKey(key)) //
                    .forEach(key -> {
                        builder.addTextValue(key, data.get(key));
                    });

            return builder.build();
        });

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    /** Minimum fraction digits. */
    @Option(description = "Minimum number of digits after the decimal point")
    private int minFractionDigits = 2;

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {
        return "Amount";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This data type allows to answer questions by typing an amount.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return AmountValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Amount fromValueElement(final ValueElement value) {
        final ListValueElement lst = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .orElse(null);

        if (null == lst) {
            return null;
        }

        final Map<String, ? extends TextValueElement> map = lst.asMap(TextValueElement.class);

        final Amount amount = new Amount( //
                Optional.ofNullable(map.get("amount")).map(TextValueElement::getValue).orElse(null), //
                Optional.ofNullable(map.get("monetary")).map(TextValueElement::getValue).orElse(null), //
                Optional.ofNullable(map.get("currency")).map(TextValueElement::getValue).orElse(null) //
        );

        if (StringUtils.isEmpty(amount.getAmount()) && StringUtils.isEmpty(amount.getMonetary()) && StringUtils.isEmpty(amount.getCurrency())) {
            return null;
        } else {
            return amount;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        if (!(value instanceof Amount)) {
            return null;
        }

        final Amount amount = (Amount) value;
        return amount.toString();
    }

    /**
     * Accesseur sur l'attribut {@link #minFractionDigits}.
     *
     * @return String minFractionDigits
     */
    public int getMinFractionDigits() {
        return this.minFractionDigits;
    }

    /**
     * Mutateur sur l'attribut {@link #minFractionDigits}.
     *
     * @param currency
     *            la nouvelle valeur de l'attribut minFractionDigits
     */
    public void setMinFractionDigits(final int minFractionDigits) {
        this.minFractionDigits = minFractionDigits;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TriFunction<SpecificationLoader, DataElement, Object, ValueElement> findConverter(final SpecificationLoader loader, final DataElement dataElement, final Object src) {
        for (final Map.Entry<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> entry : CONVERTERS.entrySet()) {
            if (entry.getKey().isInstance(src)) {
                return entry.getValue();
            }
        }

        return null;
    }
}
