/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.function.TriFunction;
import fr.ge.common.nash.engine.adapter.AbstractMultipleReferentialValueAdapter;
import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;
import fr.ge.common.nash.engine.adapter.Option;
import fr.ge.common.nash.engine.adapter.v1_2.value.ReferentialOption;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.nash.engine.referential.ReferentialReader;
import fr.ge.common.utils.CoreUtil;

/**
 * The FileReadOnly value adapter.
 *
 * @author Christian Cougourdan
 */
public class PreviewFileValueAdapter extends AbstractValueAdapter<List<ReferentialOption>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMultipleReferentialValueAdapter.class);

    /** Converters. */
    private static final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> CONVERTERS;

    static {
        final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> map = new HashMap<>();

        map.put(List.class, (loader, dataElement, value) -> {
            LOGGER.info("list is {}", value);
            final ValueElementBuilder builder = new ValueElementBuilder();

            Map<String, String> labels = null;

            for (final Object item : (List<?>) value) {
                if (item instanceof ReferentialOption) {
                    Optional.of(item).map(ReferentialOption.class::cast).ifPresent(opt -> builder.addTextValue(opt.getId(), opt.getLabel()));
                } else if (item instanceof String) {
                    if (null == labels) {
                        labels = new ReferentialReader().findLabels(loader, dataElement);
                    }
                    builder.addTextValue((String) item, labels.get(item));
                } else if (item instanceof HashMap) {
                    Optional.of(item).map(HashMap.class::cast).ifPresent(opt -> builder.addTextValue((String) opt.get("id"), (String) opt.get("label")));
                }
            }

            return builder.build();
        });

        map.put(FormContentData.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            LOGGER.info("FormContentData is {}", value);
            final FormContentData data = (FormContentData) value;
            final List<FormContentData> files = data.asList();
            if (!files.isEmpty()) {
                for (final FormContentData elm : files) {
                    builder.addTextValue(elm.withPath("id").asString(), elm.withPath("label").asString());
                }
            } else if (!data.asMap().isEmpty()) {
                final List<FormContentData> labels = data.withPath("label").asList();
                final List<FormContentData> ids = data.withPath("id").asList();
                for (int i = 0; i < ids.size(); i++) {
                    builder.addTextValue(ids.get(i).asString(), labels.get(i).asString());
                }
            }

            return builder.build();
        });

        map.put(DataBinding.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final DataBinding data = (DataBinding) value;
            if (data.isArray()) {
                for (final DataBinding elm : data.asList()) {
                    builder.addTextValue(elm.withPath("id").asString(), elm.withPath("label").asString());
                }
            } else if (data.isMap()) {
                builder.addTextValue(data.withPath("id").asString(), data.withPath("label").asString());
            }

            return builder.build();
        });

        map.put(HashMap.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();
            LOGGER.info("HashMap is {}", value);
            final Map<String, String> data = CoreUtil.cast(value);
            for (final Entry<String, String> entry : data.entrySet()) {
                final String strDataId = entry.getKey();
                final String strDataLabel = entry.getValue();
                if (StringUtils.isEmpty(strDataId) || StringUtils.isEmpty(strDataLabel)) {
                    return null;
                } else {
                    builder.addTextValue(strDataId, strDataLabel);
                }
            }
            return builder.build();

        });

        map.put(Map.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            LOGGER.info("Map is {}", value);
            final Map<String, String> data = CoreUtil.cast(value);
            for (final Entry<String, String> entry : data.entrySet()) {
                final String strDataId = entry.getKey();
                final String strDataLabel = entry.getValue();
                if (StringUtils.isEmpty(strDataId) || StringUtils.isEmpty(strDataLabel)) {
                    return null;
                } else {
                    builder.addTextValue(strDataId, strDataLabel);
                }
            }
            return builder.build();

        });

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ReferentialOption> fromValueElement(final ValueElement value) {
        final ListValueElement lst = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .orElse(null);

        if (null == lst) {
            return null;
        }

        return lst.getElements().stream() //
                .map(TextValueElement.class::cast) //
                .filter(elm -> !StringUtils.isEmpty(elm.getId())) //
                .map(elm -> new ReferentialOption(elm.getId(), elm.getValue())) //
                .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    protected TriFunction<SpecificationLoader, DataElement, Object, ValueElement> findConverter(final SpecificationLoader loader, //
            final DataElement dataElement, final Object src) {
        for (final Map.Entry<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> entry : CONVERTERS.entrySet()) {
            if (entry.getKey().isInstance(src)) {
                return entry.getValue();
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        final TriFunction<SpecificationLoader, DataElement, Object, ValueElement> converter = this.findConverter(loader, dataElement, //
                value);
        if (null == converter) {
            return null;
        } else {
            return converter.apply(loader, dataElement, value);
        }
    }

    /** Referential. */
    @Option(description = "active download of file")
    private boolean downloadUrl;

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {
        return "PreviewFile";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This data type allows to view a sent file.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return PreviewFileValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String externalPath() {
        return "generated/";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    @Override
    public boolean hasOwnUpdate() {
        return true;
    }

    /**
     * @return the downloadUrl
     */
    public boolean isDownloadUrl() {
        return this.downloadUrl;
    }

    /**
     * @param downloadUrl
     *            the downloadUrl to set
     */
    public void setDownloadUrl(final boolean downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

}
