/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.core.function.TriFunction;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.FormContentReader;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ITypedValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.nash.engine.referential.ReferentialReader;

/**
 * A referential value adapter.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 * @param <T>
 *            the generic type
 */
public abstract class AbstractReferentialValueAdapter<T> extends AbstractValueAdapter<T> {

    /** Referential. */
    @Option(description = "Referential identifier")
    private String ref = "";

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        final TriFunction<SpecificationLoader, DataElement, Object, ValueElement> converter = this.findConverter(loader, dataElement, value);
        if (null == converter) {
            return null;
        } else {
            return converter.apply(loader, dataElement, value);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    /**
     * Finds a converter.
     *
     * @param loader
     *            the specification loader
     * @param dataElement
     *            the data element
     * @param src
     *            the source object to convert from
     * @return a function to convert to a value element
     */
    protected abstract TriFunction<SpecificationLoader, DataElement, Object, ValueElement> findConverter( //
            final SpecificationLoader loader, final DataElement dataElement, final Object src);

    /**
     * {@inheritDoc}
     */
    @Override
    public T set(final SpecificationLoader loader, final DataElement dataElement, final String key, final HttpServletRequest request) {
        final FormContentData data = FormContentReader.read(request).withPath(key);
        return this.set(loader, null, dataElement, data);
    }

    /**
     * Getter on attribute {@link #ref}.
     *
     * @return String ref
     */
    public String getRef() {
        return this.ref;
    }

    /**
     * Setter on attribute {@link #ref}.
     *
     * @param ref
     *            the new value of attribute ref
     */
    public void setRef(final String ref) {
        this.ref = ref;
    }

    protected static List<ITypedValueElement> transcode(final SpecificationLoader loader, final DataElement dataElement, final FormContentData source) {
        final List<ITypedValueElement> opts = new ArrayList<>();
        final List<FormContentData> elms = Optional.of(source.asList()).filter(CollectionUtils::isNotEmpty).orElseGet(() -> Arrays.asList(source));
        final Map<String, String> labels = Optional.ofNullable(new ReferentialReader().findLabels(loader, dataElement)).orElseGet(HashMap::new);

        for (final FormContentData data : elms) {
            final Map<String, String[]> dataAsMap = data.asMap();
            String optId = null;
            String optLabel = null;
            if (dataAsMap.containsKey("")) {
                optId = dataAsMap.get("")[0];
                optLabel = Optional.ofNullable(labels.get(optId)).filter(StringUtils::isNotEmpty).orElse(optId);
            } else if (dataAsMap.containsKey("id")) {
                optId = dataAsMap.get("id")[0];
                optLabel = Optional.ofNullable(labels.get(optId)) //
                        .filter(StringUtils::isNotEmpty) //
                        .orElse( //
                                Optional.ofNullable(dataAsMap.get("label")) //
                                        .filter(ArrayUtils::isNotEmpty) //
                                        .map(values -> values[0]) //
                                        .orElse(optId) //
                        );
            } else {
                final Iterator<Entry<String, String[]>> it = dataAsMap.entrySet().iterator();
                if (it.hasNext()) {
                    final Entry<String, String[]> entry = it.next();
                    optId = entry.getKey();
                    optLabel = Optional.ofNullable(labels.get(optId)) //
                            .filter(StringUtils::isNotEmpty) //
                            .orElse( //
                                    Optional.ofNullable(entry.getValue()) //
                                            .filter(ArrayUtils::isNotEmpty) //
                                            .map(values -> values[0]) //
                                            .orElse(optId) //
                            );
                }
            }

            if (StringUtils.isNotEmpty(optId)) {
                opts.add(new TextValueElement(optId, optLabel));
            }
        }

        return opts;
    }

}
