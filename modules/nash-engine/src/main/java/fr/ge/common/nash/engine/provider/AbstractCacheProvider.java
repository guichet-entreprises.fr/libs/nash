package fr.ge.common.nash.engine.provider;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 *
 * @author Christian Cougourdan
 */
public abstract class AbstractCacheProvider implements IProvider {

    private final Map<String, FileEntry> cacheFileEntries = new HashMap<>();

    private final Map<String, Map<Class<?>, Object>> cacheBeans = new HashMap<>();

    @Override
    public FileEntry load(final String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }

        final String absolutePath = this.getAbsolutePath(name);
        FileEntry entry = this.cacheFileEntries.get(absolutePath);

        if (null == entry) {
            entry = this.loadWithoutCache(absolutePath);

            this.cacheFileEntries.put(absolutePath, entry);
        }

        return entry;
    }

    @Override
    public <T> T asBean(final String name, final Class<T> clazz) {
        final String absolutePath = this.getAbsolutePath(name);

        Map<Class<?>, Object> cacheByClass = this.cacheBeans.get(absolutePath);
        if (null == cacheByClass) {
            this.cacheBeans.put(absolutePath, cacheByClass = new HashMap<>());
        } else if (cacheByClass.containsKey(clazz)) {
            return CoreUtil.cast(cacheByClass.get(clazz));
        }

        final T bean = this.asBeanWithoutCache(absolutePath, clazz);
        cacheByClass.put(clazz, bean);

        return bean;
    }

    @Override
    public boolean remove(final String name) {
        return IProvider.super.remove(name);
    }

    @Override
    public void save(final String name, final String mimetype, final byte[] content) {
        IProvider.super.save(name, mimetype, content);
    }

    protected abstract FileEntry loadWithoutCache(String absoluteResourcePath);

    protected abstract <T> T asBeanWithoutCache(String name, Class<T> expected);

    protected abstract void removeWithoutCache(String absoluteResourcePath);

    protected abstract void saveWithoutCache(String name, String mimetype, byte[] content);

}
