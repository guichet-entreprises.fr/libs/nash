/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.util;

/**
 * Value adapter resource type enum.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public enum ValueAdapterResourceTypeEnum {

    /** The preview. */
    PREVIEW("preview", ".png", true, false, null, null),

    /** The script. */
    SCRIPT("script", ".js", false, true, "", ""),

    /** The style. */
    STYLE("style", ".css", false, true, null, null),

    /** The translation. */
    TRANSLATION("translation", ".po", false, false, null, null);

    /** The name. */
    private String name;

    /** The extension. */
    private String extension;

    /** Inherit ?. */
    private boolean inherit;

    /** Multiple ?. */
    private boolean multiple;

    /** Prefix. */
    private String prefix;

    /** Suffix. */
    private String suffix;

    /**
     *
     * Constructor.
     *
     * @param name
     *            the name
     * @param extension
     *            the extension
     * @param inherit
     *            inherit ?
     * @param multiple
     *            multiple ?
     * @param prefix
     *            the prefix
     * @param suffix
     *            the suffix
     */
    ValueAdapterResourceTypeEnum(final String name, final String extension, final boolean inherit, final boolean multiple, final String prefix, final String suffix) {
        this.name = name;
        this.extension = extension;
        this.inherit = inherit;
        this.multiple = multiple;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    /**
     * Getter on attribute {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter on attribute {@link #extension}.
     *
     * @return String extension
     */
    public String getExtension() {
        return this.extension;
    }

    /**
     * Getter on attribute {@link #inherit}.
     *
     * @return boolean inherit
     */
    public boolean isInherit() {
        return this.inherit;
    }

    /**
     * Getter on attribute {@link #multiple}.
     *
     * @return boolean multiple
     */
    public boolean isMultiple() {
        return this.multiple;
    }

    /**
     * Getter on attribute {@link #prefix}.
     *
     * @return String prefix
     */
    public String getPrefix() {
        return this.prefix;
    }

    /**
     * Getter on attribute {@link #suffix}.
     *
     * @return String suffix
     */
    public String getSuffix() {
        return this.suffix;
    }

}
