package fr.ge.common.nash.engine.loader.event;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;

/**
 * Event relative to record status property change.
 *
 * @author Christian Cougourdan
 */
public class RecordStepStatusChangeEvent extends Event {

    private final StepElement step;

    private final StepStatusEnum oldStatus;

    private final StepStatusEnum newStatus;

    /**
     * Default constructor.
     *
     * @param loader
     *            relative record loader
     */
    public RecordStepStatusChangeEvent(final SpecificationLoader loader, final StepElement step, final StepStatusEnum oldStatus, final StepStatusEnum newStatus) {
        super(loader);
        this.step = step;
        this.oldStatus = oldStatus;
        this.newStatus = newStatus;
    }

    /**
     * @return the step
     */
    public StepElement getStep() {
        return this.step;
    }

    /**
     * @return the oldStatus
     */
    public StepStatusEnum getOldStatus() {
        return this.oldStatus;
    }

    /**
     * @return the newStatus
     */
    public StepStatusEnum getNewStatus() {
        return this.newStatus;
    }

}
