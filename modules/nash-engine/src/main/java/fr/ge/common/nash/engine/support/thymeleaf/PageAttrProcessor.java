/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.support.thymeleaf;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.context.IEngineContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateInputException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.processor.AbstractStandardFragmentInsertionTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.ButtonElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;

/**
 * The Class PageAttrProcessor.
 *
 * @author Christian Cougourdan
 */
public class PageAttrProcessor extends AbstractStandardFragmentInsertionTagProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(PageAttrProcessor.class);

    /** The Constant ATTR_NAME. */
    public static final String ATTR_NAME = "page";

    /** The Constant EXPECTED_PARAMETER_CLASS. */
    private static final Map<String, Class<?>> EXPECTED_PARAMETER_CLASS;

    /** The Constant STEP_ELEMENT_BUTTON. */
    private static final Map<String, Function<StepElement, ButtonElement>> SUBMIT_BUTTONS;

    static {
        final Map<String, Class<?>> map = new HashMap<>();

        map.put(Constants.MODEL_STEP, FormSpecificationData.class);
        map.put(Constants.MODEL_PAGE, GroupElement.class);

        EXPECTED_PARAMETER_CLASS = Collections.unmodifiableMap(map);

        final Map<String, Function<StepElement, ButtonElement>> mapButton = new HashMap<>();

        mapButton.put(Constants.MODEL_BUTTON_PREVIOUS, StepElement::getPreviousButton);
        mapButton.put(Constants.MODEL_BUTTON_SAVE, StepElement::getSaveQuit);
        mapButton.put(Constants.MODEL_BUTTON_FINALIZE, StepElement::getFinalize);
        mapButton.put(Constants.MODEL_BUTTON_FINISH, StepElement::getNextStep);
        mapButton.put(Constants.MODEL_BUTTON_NEXT, StepElement::getNextPage);

        SUBMIT_BUTTONS = Collections.unmodifiableMap(mapButton);
    }

    /**
     * Default constructor.
     *
     * @param dialectPrefix
     *            dialect prefix
     */
    public PageAttrProcessor(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, ATTR_NAME, 0, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag, final AttributeName attributeName, final String attributeValue,
            final IElementTagStructureHandler structureHandler) {

        final Map<String, Object> localVariables = this.localVariables(context, attributeValue);
        ((IEngineContext) context).setVariables(localVariables);

        super.doProcess(context, tag, attributeName, "page", structureHandler);
    }

    /**
     * {@inheritDoc}
     */
    protected Map<String, Object> localVariables(final ITemplateContext context, final String attributeValue) {
        final Map<String, Object> model = FragmentUtil.parseAssignation(context, attributeValue, EXPECTED_PARAMETER_CLASS);

        if (!model.containsKey(Constants.MODEL_STEP)) {
            throw new TemplateInputException("Form specification not defined");
        }

        if (!model.containsKey(Constants.MODEL_PAGE)) {
            throw new TemplateInputException("Form group element not defined");
        }

        final FormSpecificationData spec = (FormSpecificationData) model.get(Constants.MODEL_STEP);
        final GroupElement groupElement = (GroupElement) model.get(Constants.MODEL_PAGE);
        if (spec.getGroups() != null) {
            final int page = spec.getGroups().indexOf(groupElement);
            model.put(Constants.MODEL_PAGE_INDEX, page);
            if (!model.containsKey(Constants.MODEL_PAGE_PREVIOUS_INDEX)) {
                model.put(Constants.MODEL_PAGE_PREVIOUS_INDEX, page - 1);
            }
        }

        model.put(Constants.MODEL_CHILDREN, FragmentUtil.getFilteredChildren(groupElement));

        // -->Displaying or hiding submit buttons
        final SpecificationLoader loader = (SpecificationLoader) context.getVariable(Constants.MODEL_LOADER);
        final Integer stepIndex = (Integer) context.getVariable(Constants.MODEL_STEP_INDEX);
        final StepElement step = Optional.ofNullable(loader) //
                .map(SpecificationLoader::stepsMgr) //
                .map(steps -> steps.find(stepIndex)) //
                .orElse(null);

        if (null != step) {
            SUBMIT_BUTTONS.forEach((buttonId, stepElement) -> {
                final ButtonElement buttonElement = stepElement.apply(step);
                if (null != buttonElement) {
                    model.put(buttonId, buttonElement);
                }
                LOGGER.debug("Adding submit button '{}' to model", buttonId);
            });
        }

        return model;
    }

}
