/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.referential;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.engine.adapter.AbstractReferentialValueAdapter;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.DataModelManager;
import fr.ge.common.nash.engine.mapping.form.v1_2.AbstractReferentialElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialTextElement;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.support.i18n.MessageFormatter;

/**
 * Referential reader.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public final class ReferentialReader {

    /** Target field. */
    private static final String TARGET_FIELD = StringUtils.EMPTY;

    /** Fields referential elements. */
    private Map<String, List<AbstractReferentialElement>> fieldsReferentials;

    /** Fields processed referentials data. */
    private Map<String, FieldProcessedReferentialsData> fieldsProcessedReferentialsData;

    /**
     * Constructor.
     */
    public ReferentialReader() {
        // Nothing to do.
    }

    /**
     * Reads a data group referentials.
     *
     * @param parser
     *            the parser
     * @param specLoader
     *            the specification loader
     * @param spec
     *            the specification
     * @param group
     *            the group
     * @return the processed referentials data
     */
    public Map<String, FieldProcessedReferentialsData> read(final SpecificationLoader specLoader, final FormSpecificationData spec, final GroupElement group) {
        this.gather(specLoader, spec, group).merge().translate(specLoader);
        return this.fieldsProcessedReferentialsData;
    }

    /**
     * Finds a data element option label.
     *
     * @param specLoader
     *            the specification loader
     * @param dataElement
     *            the data element
     * @return the referential reader
     */
    public Map<String, String> findLabels(final SpecificationLoader specLoader, final DataElement dataElement) {
        return this.gather(specLoader, dataElement).merge().labelAsMap();
    }

    /**
     * Gather all available referentials.
     *
     * @param specLoader
     *            the specification loader
     * @param spec
     *            the specification
     * @param group
     *            the group
     * @return the referential reader
     */
    private ReferentialReader gather(final SpecificationLoader specLoader, final FormSpecificationData spec, final GroupElement group) {
        this.fieldsReferentials = new HashMap<>();
        for (final Entry<String, DataElement> entry : DataModelManager.data(spec, group).entrySet()) {
            final String fieldName = entry.getKey();
            final DataElement dataElement = entry.getValue();
            this.gatherDataElement(specLoader, fieldName, dataElement);
        }
        return this;
    }

    /**
     * Gather all available referentials.
     *
     * @param specLoader
     *            the specification loader
     * @param dataElement
     *            the data element
     * @return the referential reader
     */
    private ReferentialReader gather(final SpecificationLoader specLoader, final DataElement dataElement) {
        this.fieldsReferentials = new HashMap<>();
        this.gatherDataElement(specLoader, TARGET_FIELD, dataElement);
        return this;
    }

    /**
     * Gather all available referentials for a data element.
     *
     * @param specLoader
     *            the specification loader
     * @param fieldName
     *            the field name
     * @param dataElement
     *            the data element
     */
    private void gatherDataElement(final SpecificationLoader specLoader, final String fieldName, final DataElement dataElement) {
        IValueAdapter<?> valueAdapter = null;
        if (dataElement != null) {
            valueAdapter = ValueAdapterFactory.type(specLoader, dataElement.getType());
        }
        if (valueAdapter instanceof AbstractReferentialValueAdapter) {
            final List<AbstractReferentialElement> fieldReferentials = new ArrayList<>();
            final AbstractReferentialValueAdapter<?> refValueAdapter = (AbstractReferentialValueAdapter<?>) valueAdapter;
            // get the referentials externalized in a dedicated file
            if (refValueAdapter.getRef() != null && specLoader != null) {
                final AbstractReferentialElement fieldReferentialElement = specLoader.referential(refValueAdapter.getRef());
                if (fieldReferentialElement != null) {
                    fieldReferentials.add(fieldReferentialElement);
                }
            }
            // get the field specific referentials
            if (dataElement.getConftype() != null && dataElement.getConftype().getReferentialElements() != null) {
                fieldReferentials.addAll(dataElement.getConftype().getReferentialElements());
            }
            // add the field to the list of all fields with referentials
            this.fieldsReferentials.put(fieldName, fieldReferentials);
        }
    }

    /**
     * Merges the referentials texts and display conditions.
     *
     * @return the referential reader
     */
    private ReferentialReader merge() {
        this.fieldsProcessedReferentialsData = new HashMap<>();
        for (final Entry<String, List<AbstractReferentialElement>> entry : this.fieldsReferentials.entrySet()) {
            final String fieldName = entry.getKey();
            this.fieldsProcessedReferentialsData.put(fieldName, new FieldProcessedReferentialsData());
            final List<AbstractReferentialElement> fieldReferentials = entry.getValue();
            final Map<String, ReferentialTextElement> fieldTextsValues = new HashMap<>();
            final List<ReferentialTextElement> fieldTextsOrdered = new ArrayList<>();
            for (final AbstractReferentialElement fieldReferential : fieldReferentials) {
                if (fieldReferential.getReferentialTextElements() != null) {
                    for (final ReferentialTextElement text : fieldReferential.getReferentialTextElements()) {
                        final ReferentialTextElement alreadyFoundText = fieldTextsValues.get(text.getId());
                        if (alreadyFoundText == null) {
                            fieldTextsOrdered.add(text);
                            this.merge(fieldReferential, null, text);
                            fieldTextsValues.put(text.getId(), text);
                        } else {
                            this.merge(fieldReferential, text, alreadyFoundText);
                            fieldTextsValues.put(alreadyFoundText.getId(), alreadyFoundText);
                        }
                    }
                }
                if (fieldReferential.getReferentialExternalElement() != null) {
                    this.fieldsProcessedReferentialsData.get(fieldName).setExternal(fieldReferential.getReferentialExternalElement());
                }
            }
            this.fieldsProcessedReferentialsData.get(fieldName).setTexts(fieldTextsOrdered);
        }
        return this;
    }

    /**
     * Merges a text with a previously already found text.
     *
     * @param fieldReferential
     *            the field referential
     * @param textMergeFrom
     *            the text to merge from
     * @param textMergeTo
     *            the text to merge to
     */
    private void merge(final AbstractReferentialElement fieldReferential, final ReferentialTextElement textMergeFrom, final ReferentialTextElement textMergeTo) {
        if (!StringUtils.isEmpty(textMergeTo.getRuleAsAttr())) {
            textMergeTo.addDisplayCondition(textMergeTo.getRuleAsAttr());
        }

        if (textMergeFrom != null) {
            // if there are several occurrences of the text, get
            // the first label which contains characters
            if (StringUtils.isBlank(textMergeTo.getLabel()) && StringUtils.isNotBlank(textMergeFrom.getLabel())) {
                textMergeTo.setLabel(textMergeFrom.getLabel());
            }
            if (!StringUtils.isEmpty(textMergeFrom.getRuleAsAttr())) {
                textMergeTo.addDisplayCondition(textMergeFrom.getRuleAsAttr());
            }
        }

        if (fieldReferential.getDisplayCondition() != null) {
            textMergeTo.addDisplayCondition(fieldReferential.getDisplayCondition());
        }
    }

    /**
     * Translate the referentials texts labels.
     *
     * @param specLoader
     *            the specification loader
     * @return the referential reader
     */
    private ReferentialReader translate(final SpecificationLoader specLoader) {
        if (null == specLoader) {
            return this;
        }

        final MessageFormatter messageFormatter = NashMessageReader.getReader(specLoader).getFormatter();
        for (final FieldProcessedReferentialsData fieldProcessedReferentialsData : this.fieldsProcessedReferentialsData.values()) {
            for (final ReferentialTextElement text : fieldProcessedReferentialsData.getTexts()) {
                if (text.getLabel() != null) {
                    final String translation = messageFormatter.formatNullIfMissing(text.getLabel());
                    text.setTranslation(translation);
                }
            }
        }

        return this;
    }

    /**
     * Converts the referentials texts as a map of labels.
     *
     * @return the map
     */
    private Map<String, String> labelAsMap() {
        final Map<String, String> labels = new HashMap<>();
        List<ReferentialTextElement> fieldTexts = null;
        if (this.fieldsProcessedReferentialsData.get(TARGET_FIELD) != null) {
            fieldTexts = this.fieldsProcessedReferentialsData.get(TARGET_FIELD).getTexts();
        }
        if (fieldTexts != null) {
            for (final ReferentialTextElement textElement : fieldTexts) {
                labels.put(textElement.getId(), textElement.getLabel());
            }
        }
        return labels;
    }

}
