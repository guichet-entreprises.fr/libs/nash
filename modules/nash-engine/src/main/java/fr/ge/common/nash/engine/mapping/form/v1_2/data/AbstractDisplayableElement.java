/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.mapping.form.v1_2.data;

import java.util.Optional;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class AbstractDisplayableElement.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
@XmlTransient
@XmlAccessorType(XmlAccessType.NONE)
public abstract class AbstractDisplayableElement<T extends AbstractDisplayableElement<T>> extends AbstractNamedElement implements IElement<AbstractDisplayableElement<?>> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The path. */
    @XmlTransient
    private String path;

    /** The parent path. */
    @XmlTransient
    private String parentPath;

    /** The min occurences. */
    @XmlAttribute(name = "min-occurs")
    private String minOccurs;

    /** The max occurences. */
    @XmlAttribute(name = "max-occurs")
    private String maxOccurs;

    /** The path. */
    @XmlTransient
    private String trigger;

    /**
     * Gets the path.
     *
     * @return the path
     */
    @Override
    public String getPath() {
        if (null == this.path) {
            return Optional.ofNullable(this.parentPath).map(str -> str + '.' + this.getId()).orElse(this.getId());
        } else {
            return this.path;
        }
    }

    /**
     * Gets the parent path.
     *
     * @return the parent path
     */
    @Override
    public String getParentPath() {
        return this.parentPath;
    }

    /**
     * Is the element repeatable ?
     *
     * @return a boolean
     */
    @Override
    public boolean isRepeatable() {
        return StringUtils.isNotEmpty(this.getMinOccurs()) || StringUtils.isNotEmpty(this.getMaxOccurs());
    }

    /**
     * Sets the path.
     *
     * @param path
     *            the new path
     * @return the t
     */
    @Override
    @SuppressWarnings("unchecked")
    public T setPath(final String path) {
        this.path = path;
        return (T) this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public T setParentPath(final String parentPath) {
        this.parentPath = parentPath;
        return (T) this;
    }

    /**
     * Before unmarshal.
     *
     * @param unmarshaller
     *            the unmarshaller
     * @param parent
     *            the parent
     */
    public void beforeUnmarshal(final Unmarshaller unmarshaller, final Object parent) {
        if (parent instanceof IElement<?>) {
            this.parentPath = ((IElement<?>) parent).getPath();
        }
    }

    /**
     * After unmarshal.
     *
     * @param unmarshaller
     *            the unmarshaller
     * @param parent
     *            the parent
     */
    public void afterUnmarshal(final Unmarshaller unmarshaller, final Object parent) {
        this.path = Optional.ofNullable(this.parentPath).map(str -> str + '.' + this.getId()).orElse(this.getId());
    }

    /**
     * Accesseur sur l'attribut {@link #minOccurs}.
     *
     * @return String minOccurs
     */
    @Override
    public String getMinOccurs() {
        return this.minOccurs;
    }

    /**
     * Mutateur sur l'attribut {@link #minOccurs}.
     *
     * @param minOccurs
     *            la nouvelle valeur de l'attribut minOccurs
     */
    public void setMinOccurs(String minOccurs) {
        this.minOccurs = minOccurs;
    }

    /**
     * Accesseur sur l'attribut {@link #maxOccurs}.
     *
     * @return String maxOccurs
     */
    @Override
    public String getMaxOccurs() {
        return this.maxOccurs;
    }

    /**
     * Mutateur sur l'attribut {@link #maxOccurs}.
     *
     * @param maxOccurs
     *            la nouvelle valeur de l'attribut maxOccurs
     */
    public void setMaxOccurs(String maxOccurs) {
        this.maxOccurs = maxOccurs;
    }
}
