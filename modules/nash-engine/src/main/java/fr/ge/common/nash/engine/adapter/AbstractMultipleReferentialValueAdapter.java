/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.function.TriFunction;
import fr.ge.common.nash.engine.adapter.v1_2.value.ReferentialOption;
import fr.ge.common.nash.engine.adapter.v1_2.value.ReferentialOptions;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ITypedValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.nash.engine.referential.ReferentialReader;
import fr.ge.common.utils.CoreUtil;

/**
 * A referential value adapter.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public abstract class AbstractMultipleReferentialValueAdapter extends AbstractReferentialValueAdapter<List<ReferentialOption>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMultipleReferentialValueAdapter.class);
    /** Converters. */
    private static final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> CONVERTERS;

    static {
        final Map<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> map = new HashMap<>();

        map.put(ReferentialOptions.class, (loader, dataElement, value) -> {

            LOGGER.info("ReferentialOptions is {}", value);
            final ValueElementBuilder builder = new ValueElementBuilder();

            final ReferentialOptions options = (ReferentialOptions) value;

            LOGGER.info("Referential Options is {}", options);
            for (final ReferentialOption option : options.getOptions()) {
                builder.addTextValue(option.getId(), option.getLabel());
            }

            return builder.build();
        });

        map.put(List.class, (loader, dataElement, value) -> {
            LOGGER.info("list is {}", value);
            final ValueElementBuilder builder = new ValueElementBuilder();

            Map<String, String> labels = null;

            for (final Object item : (List<?>) value) {
                if (item instanceof ReferentialOption) {
                    Optional.of(item).map(ReferentialOption.class::cast).ifPresent(opt -> builder.addTextValue(opt.getId(), opt.getLabel()));
                } else if (item instanceof String) {
                    if (null == labels) {
                        labels = new ReferentialReader().findLabels(loader, dataElement);
                    }
                    builder.addTextValue((String) item, labels.get(item));
                } else if (item instanceof HashMap) {
                    Optional.of(item).map(HashMap.class::cast).ifPresent(opt -> builder.addTextValue((String) opt.get("id"), (String) opt.get("label")));
                }
            }

            return builder.build();
        });

        map.put(FormContentData.class, (loader, dataElement, value) -> {
            LOGGER.info("FormContentData is {}", value);
            final List<ITypedValueElement> values = transcode(loader, dataElement, (FormContentData) value);

            if (values.isEmpty()) {
                return null;
            } else {
                return new ValueElement(values);
            }
        });

        map.put(DataBinding.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final Map<String, String> labels = Optional.ofNullable(new ReferentialReader().findLabels(loader, dataElement)).orElseGet(HashMap::new);

            LOGGER.debug("DataBinding is {}", value);
            final DataBinding data = (DataBinding) value;
            if (data.isMap()) {
                for (final Entry<String, DataBinding> entry : data.asMap().entrySet()) {
                    builder.addTextValue(entry.getKey(), labels.getOrDefault(entry.getValue(), entry.getKey()));
                }
            } else {
                final List<DataBinding> elms = data.isArray() ? data.asList() : Arrays.asList(data);
                for (final DataBinding elm : elms) {
                    String optId = null;
                    String optLabel = null;
                    if (elm.isMap()) {
                        if (elm.asMap().containsKey("id")) {
                            optId = elm.withPath("id").asString();
                        } else {
                            final Iterator<Entry<String, DataBinding>> it = elm.asMap().entrySet().iterator();
                            if (it.hasNext()) {
                                final Entry<String, DataBinding> entry = it.next();
                                optId = entry.getKey();
                                optLabel = entry.getValue().asString();
                            }
                        }
                    } else {
                        optId = elm.asString();
                        optLabel = null;
                    }

                    if (null != optId) {
                        builder.addTextValue(optId, null == optLabel ? labels.getOrDefault(optId, optId) : optLabel);
                    }
                }
            }

            return builder.build();
        });

        map.put(HashMap.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();
            LOGGER.info("HashMap is {}", value);
            final Map<String, String> data = CoreUtil.cast(value);
            for (final Entry<String, String> entry : data.entrySet()) {
                final String strDataId = entry.getKey();
                final String strDataLabel = entry.getValue();
                if (StringUtils.isEmpty(strDataId) || StringUtils.isEmpty(strDataLabel)) {
                    return null;
                } else {
                    builder.addTextValue(strDataId, strDataLabel);
                }
            }
            return builder.build();

        });

        map.put(Map.class, (loader, dataElement, value) -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            LOGGER.info("Map is {}", value);
            final Map<String, String> data = CoreUtil.cast(value);
            for (final Entry<String, String> entry : data.entrySet()) {
                final String strDataId = entry.getKey();
                final String strDataLabel = entry.getValue();
                if (StringUtils.isEmpty(strDataId) || StringUtils.isEmpty(strDataLabel)) {
                    return null;
                } else {
                    builder.addTextValue(strDataId, strDataLabel);
                }
            }
            return builder.build();

        });

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ReferentialOption> fromValueElement(final ValueElement value) {
        final ListValueElement lst = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .orElse(null);

        if (null == lst) {
            return null;
        }

        return lst.getElements().stream() //
                .map(TextValueElement.class::cast) //
                .filter(elm -> !StringUtils.isEmpty(elm.getId())) //
                .map(elm -> new ReferentialOption(elm.getId(), elm.getValue())) //
                .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TriFunction<SpecificationLoader, DataElement, Object, ValueElement> findConverter(final SpecificationLoader loader, //
            final DataElement dataElement, final Object src) {
        for (final Map.Entry<Class<?>, TriFunction<SpecificationLoader, DataElement, Object, ValueElement>> entry : CONVERTERS.entrySet()) {
            if (entry.getKey().isInstance(src)) {
                return entry.getValue();
            }
        }

        return null;
    }

}
