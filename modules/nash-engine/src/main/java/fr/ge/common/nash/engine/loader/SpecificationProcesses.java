/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.loader;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.manager.ProcessModelManager;
import fr.ge.common.nash.engine.manager.processor.IProcessResult;
import fr.ge.common.nash.engine.manager.processor.result.EmptyProcessResult;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;

/**
 * Record processes manager.
 *
 * @author Christian Cougourdan
 */
public class SpecificationProcesses {

    /** The loader. */
    private final SpecificationLoader loader;

    /**
     * Instantiates a new specification processes.
     *
     * @param loader
     *            the loader
     */
    SpecificationProcesses(final SpecificationLoader loader) {
        this.loader = loader;
    }

    /**
     * Find pre-processes from a step element.
     *
     * @param step
     *            the step
     * @return the form specification process
     */
    public FormSpecificationProcess findPreprocesses(final StepElement step) {
        return this.findProcesses(step, StepElement::getPreprocess, "pre");
    }

    /**
     * Find post-processes from a step element.
     *
     * @param step
     *            the step
     * @return the form specification process
     */
    public FormSpecificationProcess findPostprocesses(final StepElement step) {
        return this.findProcesses(step, StepElement::getPostprocess, "post");
    }

    /**
     * Find processes.
     *
     * @param step
     *            the step
     * @param resourceNameGetter
     *            the resource name getter, ie {@link StepElement#getPreprocess()}
     *            or {@link StepElement#getPostprocess()}
     * @param processesType
     *            the processes type, ie "pre" or "post"
     * @return the form specification process
     */
    public FormSpecificationProcess findProcesses(final StepElement step, final Function<StepElement, String> resourceNameGetter, final String processesType) {
        if (null == step) {
            return null;
        }

        final String processesResourceName = resourceNameGetter.apply(step);
        if (StringUtils.isEmpty(processesResourceName)) {
            return null;
        }

        final FormSpecificationProcess processes = this.loader.getProvider().asBean(processesResourceName, FormSpecificationProcess.class);

        if (null == processes) {
            throw new TechnicalException(String.format("Process file \"%s\" not found", processesResourceName));
        }

        processes.setType(processesType);
        processes.setResourceName(processesResourceName);

        return processes;
    }

    /**
     * Execute pre processes from specified step element.
     *
     * @param stepElement
     *            step origin
     * @return the process result
     */
    public IProcessResult<?> preExecute(final StepElement stepElement) {
        return this.execute(stepElement, this::findPreprocesses, step -> Collections.emptyMap(), null);
    }

    /**
     * Execute post processes from specified step element.
     *
     * @param stepElement
     *            step element
     * @return the process result
     */
    public IProcessResult<?> postExecute(final StepElement stepElement) {
        return this.execute(stepElement, this::findPostprocesses, this.loader::extract, null);
    }

    /**
     * Execute pre processes from specified step element, starting at identified
     * process.
     *
     * @param stepElement
     *            the step element
     * @param processId
     *            the process id
     * @return the process result
     */
    public IProcessResult<?> preExecuteFrom(final StepElement stepElement, final String processId) {
        return this.execute(stepElement, this::findPreprocesses, this.loader::extract, lst -> this.subProcessListFrom(lst, processId));
    }

    /**
     * Execute post processes from specified step element, starting at identified
     * process.
     *
     * @param stepElement
     *            the step element
     * @param processId
     *            the process id
     * @return the i process result
     */
    public IProcessResult<?> postExecuteFrom(final StepElement stepElement, final String processId) {
        return this.execute(stepElement, this::findPostprocesses, this.loader::extract, lst -> this.subProcessListFrom(lst, processId));
    }

    /**
     * Execute pre processes from specified step element, starting at identified
     * process.
     *
     * @param stepElement
     *            the step element
     * @param processId
     *            the process id
     * @return the process result
     */
    public IProcessResult<?> preExecuteAfter(final StepElement stepElement, final String processId) {
        return this.execute(stepElement, this::findPreprocesses, this.loader::extract, lst -> this.subProcessListAfter(lst, processId));
    }

    /**
     * Execute post processes from specified step element, starting at identified
     * process.
     *
     * @param stepElement
     *            the step element
     * @param processId
     *            the process id
     * @return the i process result
     */
    public IProcessResult<?> postExecuteAfter(final StepElement stepElement, final String processId) {
        return this.execute(stepElement, this::findPostprocesses, this.loader::extract, lst -> this.subProcessListAfter(lst, processId));
    }

    /**
     * Execute processes from a specific step and phase.
     *
     * @param stepElement
     *            step element
     * @param resourceGetter
     *            the resource getter
     * @param additionalModelExtractor
     *            the additional model extractor
     * @param processId
     *            the from process id
     * @return the process result
     */
    private IProcessResult<?> execute(final StepElement stepElement, final Function<StepElement, FormSpecificationProcess> resourceGetter,
            final Function<StepElement, Map<String, Object>> additionalModelExtractor, final Function<List<IProcess>, List<IProcess>> processId) {

        final EngineContext<?> descContext = EngineContext.build(this.loader);
        final EngineContext<StepElement> stepContext = descContext.target(stepElement);
        final FormSpecificationProcess processes = resourceGetter.apply(stepElement);

        if (null == processes) {
            return null;
        }

        final Map<String, Object> model = this.loader.extractUntil(stepElement);
        if (null != additionalModelExtractor) {
            model.putAll(additionalModelExtractor.apply(stepElement));
        }

        final IProvider stepProvider = this.loader.provider(processes.getResourceName());
        final EngineContext<FormSpecificationProcess> processesContext = stepContext.target(processes).withProvider(stepProvider);

        final ProcessModelManager mgr = ProcessModelManager.create(processesContext);

        List<IProcess> processElements = processes.getProcesses();
        if (null != processId) {
            processElements = processId.apply(processElements);
        }

        if (CollectionUtils.isEmpty(processElements)) {
            return new EmptyProcessResult();
        }

        try {
            return mgr.execute(processElements, model);
        } catch (final ExpressionException ex) {
            throw new ExpressionException("Processes execution error", ex);
        }
    }

    /**
     * Sub process list.
     *
     * @param processElements
     *            the process elements
     * @param processId
     *            the from process id
     * @return the list
     */
    private List<IProcess> subProcessListFrom(final List<IProcess> processElements, final String processId) {
        if (null == processElements) {
            return Collections.emptyList();
        } else if (null == processId || processElements.isEmpty()) {
            return processElements;
        }

        final int idx = IntStream.range(0, processElements.size()).filter(processIdx -> {
            final IProcess process = processElements.get(processIdx);
            return process instanceof ProcessElement && processId.equals(((ProcessElement) process).getId());
        }).findFirst().orElse(-1);

        if (idx >= 0) {
            return processElements.subList(idx, processElements.size());
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Sub process list after.
     *
     * @param processElements
     *            the process elements
     * @param processId
     *            the process id
     * @return the list
     */
    private List<IProcess> subProcessListAfter(final List<IProcess> processElements, final String processId) {
        final List<IProcess> sublist = this.subProcessListFrom(processElements, processId);
        if (sublist.isEmpty()) {
            return sublist;
        } else {
            return sublist.subList(1, sublist.size());
        }
    }

    /**
     * Resolve a resource identified by its name or a process ID (preceding by a
     * sharp). If process ID is used, context has to concern a process element.
     *
     * @param context
     *            the execution context
     * @param resourceName
     *            the resource name
     * @return the resolved resource name
     */
    public static String resolveResourceName(final EngineContext<?> context, final String resourceName) {
        if (StringUtils.isEmpty(resourceName)) {
            return null;
        }

        final String resolvedResourceNames = Arrays.stream(resourceName.split("[ ]*,[ ]*")) //
                .map(path -> {
                    if (path.startsWith("#")) {
                        final FormSpecificationProcess processes = context.getParent(FormSpecificationProcess.class).getElement();
                        final String targetedProcessId = path.substring(1);

                        /*
                         * Looking for identified process element
                         */
                        final Optional<ProcessElement> processElement = processes.getProcesses().stream() //
                                .filter(ProcessElement.class::isInstance) //
                                .map(ProcessElement.class::cast) //
                                .filter(prc -> targetedProcessId.equals(prc.getId())) //
                                .findFirst();

                        /*
                         * Retrieve output attribute of found process element
                         */
                        return processElement //
                                .map(ProcessElement::getOutput) //
                                .map(src -> resolveResourceName(context, src)) //
                                .orElse(null);
                    } else {
                        return path;
                    }
                }) //
                .filter(StringUtils::isNotEmpty) //
                .collect(Collectors.joining(", "));

        return Optional.of(resolvedResourceNames).filter(StringUtils::isNotEmpty).orElse(null);
    }

    /**
     * Load a resource identified by its name or a process ID (preceding by a
     * sharp). If process ID is used, context has to concern a process element.
     *
     * @param <R>
     *            the generic type representing the returned value type
     * @param context
     *            the execution context
     * @param resourceName
     *            the resource name
     * @param expected
     *            the expected value type
     * @return the unmarshalled resource
     */
    public static <R> R load(final EngineContext<?> context, final String resourceName, final Class<R> expected) {
        final String resolvedResourceName = resolveResourceName(context, resourceName);
        if (null == resolvedResourceName) {
            return null;
        } else {
            return context.getProvider().asBean(resolvedResourceName, expected);
        }
    }

}
