/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.transition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.engine.mapping.IXml;
import fr.ge.common.nash.engine.mapping.transition.description.ReferenceElement;

/**
 * Transition description object containing minimal information about a
 * transition.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "description")
@XmlAccessorType(XmlAccessType.NONE)
public class TransitionDescription implements IXml {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The reference. */
    @XmlElement(name = "reference-id")
    private ReferenceElement reference;

    /** The title. */
    @XmlElement(name = "title")
    private String title;

    /** The action. */
    @XmlElement(name = "detail")
    private String detail;

    /**
     * Getter on attribute {@link #reference}.
     *
     * @return ReferenceElement reference
     */
    public ReferenceElement getReference() {
        return this.reference;
    }

    /**
     * Setter on attribute {@link #reference}.
     *
     * @param reference
     *            the new value of attribute reference
     */
    public void setReference(final ReferenceElement reference) {
        this.reference = reference;
    }

    /**
     * Getter on attribute {@link #title}.
     *
     * @return String title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Setter on attribute {@link #title}.
     *
     * @param title
     *            the new value of attribute title
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Getter on attribute {@link #detail}.
     *
     * @return String detail
     */
    public String getDetail() {
        return this.detail;
    }

    /**
     * Setter on attribute {@link #detail}.
     *
     * @param detail
     *            the new value of attribute detail
     */
    public void setDetail(final String detail) {
        this.detail = detail;
    }

}
