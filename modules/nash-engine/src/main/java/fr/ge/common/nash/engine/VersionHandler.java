/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class VersionHandler.
 *
 * @author atuffrea
 */
public final class VersionHandler {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(VersionHandler.class);

    /** The Constant NAMESPACES. */
    private static final String NAMESPACES = "namespace.properties";

    /** The Constant TEMPLATE_NS_PATTERN. */
    private static final String TEMPLATE_NS_PATTERN = "<${elementName}[^>]+xmlns\\s*=\\s*\"([^\"]+)\"[^>]*>";

    /** The Constant VERSION_PATTERN. */
    private static final Pattern VERSION_PATTERN = Pattern.compile(".*/([0-9]+(?:\\.[0-9]+)*)/.*");

    /**
     * Map a namespace to the package containing its implementation.
     */
    private static final Map<String, String> VERSIONS_PACKAGE;

    static {
        final Map<String, String> m = new HashMap<>();
        final Properties namespaces = new Properties();
        try {
            namespaces.load(VersionHandler.class.getResourceAsStream('/' + NAMESPACES));
        } catch (final IOException e) {
            LOGGER.error("Version package could not be found.", e);
        }

        namespaces.forEach((key, value) -> m.put((String) key, (String) value));
        VERSIONS_PACKAGE = Collections.unmodifiableMap(m);
    }

    /**
     * Default private constructor.
     */
    private VersionHandler() {
        // Nothing to do
    }

    /**
     * Version package.
     *
     * @param xml
     *            the xml
     * @param rootElement
     *            the root element
     * @return the string
     */
    public static String versionPackage(final byte[] xml, final String rootElement) {
        final String namespace = VersionHandler.getNamespace(xml, rootElement);
        if (null == namespace) {
            return null;
        }

        return VERSIONS_PACKAGE.get(namespace);
    }

    /**
     * Get the XSD version of the element in xml file.
     * <p>
     * To find the XSD version, the attribute <code>xmlns</code> must be
     * provided respecting the regexp <code>.*\/([0-9]+(?:\\.[0-9]+)*)/.*</code>
     * . <br>
     * The returned value is the sequence of digit separated by dots as a string
     *
     * @param beanAsBytes
     *            the xml file
     * @param elementName
     *            element which for the XSD version is returned
     * @return the XSD version or null if not found
     */
    public static String getVersion(final byte[] beanAsBytes, final String elementName) {
        final String xml = new String(beanAsBytes, StandardCharsets.UTF_8);
        final Pattern xmlnsPattern = getNsPattern(elementName);
        final Matcher m = xmlnsPattern.matcher(xml);

        if (!m.find()) {
            return null;
        }

        final String ns = m.group(1);
        if (null == ns) {
            return null;
        }

        final Matcher vM = VERSION_PATTERN.matcher(ns);
        if (!vM.find()) {
            return null;
        }

        return vM.group(1);
    }

    /**
     * Look for the "xmlns" attibute in the element named
     * <code>elementName</code> in the <code>beanAsByte</code>.
     *
     * @param beanAsBytes
     *            the xml file
     * @param elementName
     *            element that should contain "xmlns" attribute
     * @return Get the xml's namespace or null if not found
     */
    public static String getNamespace(final byte[] beanAsBytes, final String elementName) {
        final String xml = new String(beanAsBytes, StandardCharsets.UTF_8);
        final Pattern xmlnsPattern = getNsPattern(elementName);
        final Matcher m = xmlnsPattern.matcher(xml);

        if (!m.find()) {
            return null;
        }

        return m.group(1);
    }

    /**
     * Build the pattern matching the <code>elementName</code> containing the
     * attribute "xmlns".
     *
     * @param elementName
     *            name of the searched element
     * @return the pattern
     */
    private static Pattern getNsPattern(final String elementName) {
        final Map<String, String> m = new HashMap<>();
        m.put("elementName", elementName);

        return Pattern.compile(StrSubstitutor.replace(TEMPLATE_NS_PATTERN, m));
    }

}
