/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.bean.ProcessStatusEnum;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.SpecificationProcesses;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.manager.processor.AbstractProcessor;
import fr.ge.common.nash.engine.manager.processor.AttachmentProcessor;
import fr.ge.common.nash.engine.manager.processor.IProcessResult;
import fr.ge.common.nash.engine.manager.processor.PdfProcessor;
import fr.ge.common.nash.engine.manager.processor.ProxyInProcessor;
import fr.ge.common.nash.engine.manager.processor.ProxyOutProcessor;
import fr.ge.common.nash.engine.manager.processor.RedirectProcessor;
import fr.ge.common.nash.engine.manager.processor.ScriptProcessor;
import fr.ge.common.nash.engine.manager.processor.TransitionProcessor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.NashScriptEngine;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;

/**
 * The Class ProcessModelManager.
 *
 * @author Christian Cougourdan
 */
public class ProcessModelManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessModelManager.class);

    /** The Constant PROCESSORS. */
    private static final Map<String, Class<? extends AbstractProcessor<?, ?>>> PROCESSORS;

    static {
        final Map<String, Class<? extends AbstractProcessor<?, ?>>> m = new HashMap<>();

        m.put("attachment", AttachmentProcessor.class);
        m.put("pdf", PdfProcessor.class);
        m.put("script", ScriptProcessor.class);
        m.put("transition", TransitionProcessor.class);
        m.put("redirect", RedirectProcessor.class);
        m.put("proxy.out", ProxyOutProcessor.class);
        m.put("proxy.in", ProxyInProcessor.class);

        PROCESSORS = Collections.unmodifiableMap(m);
    }

    /** The context. */
    private final EngineContext<FormSpecificationProcess> context;

    /**
     * Instantiate a new process model manager.
     *
     * @param context
     *            the context
     */
    public ProcessModelManager(final EngineContext<FormSpecificationProcess> context) {
        this.context = context;
    }

    /**
     * Process.
     *
     * @param model
     *            the model
     * @return the form specification data
     */
    public IProcessResult<?> execute(final Map<String, Object> model) {
        return this.execute(this.context.getElement().getProcesses(), model);
    }

    /**
     * Runs processes.
     *
     * @param processes
     *            the processes
     * @param model
     *            the model to build the context from (previous steps data)
     * @return the i process result
     */
    public IProcessResult<?> execute(final List<IProcess> processes, final Map<String, Object> model) {
        try {
            return this.executeInternal(processes, model);
        } finally {
            this.context.getProvider().save('/' + this.context.getElement().getResourceName(), this.context.getElement());
        }
    }

    private IProcessResult<?> executeInternal(final List<IProcess> processes, final Map<String, Object> model) {
        IProcessResult<?> lastResult = null;

        for (final IProcess process : processes) {
            final NashScriptEngine engine = this.context.getRecord().getScriptEngine();
            final ScriptExecutionContext ctx = this.context.target(process).buildExpressionContext(Collections.singletonMap(Scopes.RECORD.toContextKey(), model));
            this.addInputsToContext(process, ctx);
            if (StringUtils.isBlank(process.getCondition()) || engine.eval(process.getCondition(), ctx, Boolean.class)) {
                if (process instanceof ProcessElement) {
                    final ProcessElement processElement = (ProcessElement) process;
                    this.cleanProcess(process);
                    try {
                        lastResult = this.execute(processElement, ctx);
                    } finally {
                        if (StringUtils.isNotEmpty(processElement.getScript())) {
                            processElement.setValue(null);
                        }
                        processElement.setStatus(ProcessStatusEnum.DONE.getStatus());
                    }
                } else if (process.getChildren() != null) {
                    lastResult = this.executeInternal(process.getChildren(), model);
                }
            }

            if (null != lastResult && lastResult.isInterrupting()) {
                break;
            }
        }

        return lastResult;
    }

    private void addInputsToContext(final IProcess process, final ScriptExecutionContext ctx) {
        this.addInputsToContext(this.context.target(process), process.getInput(), ctx);
    }

    /**
     * Adds input data to the context.
     *
     * @param process
     *            the process
     * @param ctx
     *            the context
     * @param inputs
     *            the inputs
     */
    private void addInputsToContext(final EngineContext<IProcess> engineContext, final String originalInputResourceName, final ScriptExecutionContext ctx) {
        final String inputResourceNames = SpecificationProcesses.resolveResourceName(engineContext, originalInputResourceName);
        if (StringUtils.isNotEmpty(inputResourceNames)) {
            final Map<String, Object> innerInputModel = new HashMap<>();
            final String[] splittedInputResourceName = inputResourceNames.split("[ ]*,[ ]*");
            final String rootId = Arrays.stream(splittedInputResourceName) //
                    .reduce(null, (id, inputResourceName) -> {
                        final FormSpecificationData inputData = this.load(inputResourceName);
                        RecursiveDataModelExtractor.create(engineContext.relativeProvider(inputResourceName)) //
                                .setAllowOverride(true) //
                                .extract(inputData, innerInputModel);

                        return null == id && null != inputData ? inputData.getId() : id;
                    });

            final Map<String, Object> inputModel = new HashMap<>();
            if (null != rootId) {
                inputModel.put(rootId, innerInputModel);
            }

            ctx.merge(Scopes.RECORD.toContextKey(), inputModel);
            ctx.add(Scopes.INPUT.toContextKey(), innerInputModel);
            ctx.addExtra( //
                    "_INPUT_NAME_", //
                    Arrays.stream(splittedInputResourceName) //
                            .map(engineContext.getProvider()::getAbsolutePath) //
                            .collect(Collectors.joining(", ")) //
            );

            /*
             * Retro compatibility
             */
            ctx.add("_INPUT_", innerInputModel);
        }
    }

    /**
     * Load.
     *
     * @param resourceName
     *            resource name
     * @return form specification data
     */
    private FormSpecificationData load(final String resourceName) {
        if (StringUtils.isEmpty(resourceName)) {
            return null;
        } else {
            return this.context.getProvider().asBean(resourceName, FormSpecificationData.class);
        }
    }

    /**
     * Process.
     *
     * @param process
     *            the process
     * @param model
     *            the model
     * @return the list
     */
    private IProcessResult<?> execute(final ProcessElement process, final ScriptExecutionContext ctx) {
        final Class<? extends AbstractProcessor<?, ?>> processor = PROCESSORS.get(process.getType().toLowerCase(Locale.ROOT));
        if (null == processor) {
            throw new TechnicalException(String.format("No processor found for type \"%s\"", process.getType()));
        } else {
            final EngineContext<IProcess> processEngineContext = this.context.target(process);
            try {
                final Constructor<? extends AbstractProcessor<?, ?>> constructor = processor.getConstructor();
                final AbstractProcessor<?, ?> p = constructor.newInstance().setContext(processEngineContext);
                final String replacementInputResourceName = p.getInputResourceName();
                if (null != replacementInputResourceName) {
                    this.addInputsToContext(processEngineContext, replacementInputResourceName, ctx);
                }
                return p.execute(ctx);
            } catch (NoSuchMethodException | SecurityException ex) {
                throw new TechnicalException("Compliant constructor not found", ex);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                throw new TechnicalException("Error occured while instanciate processor class", ex);
            }
        }
    }

    /**
     * Creates a process model manager.
     *
     * @param loader
     *            the loader
     * @param process
     *            the process
     * @param stepProvider
     *            the step provider
     * @return the process model manager
     */
    public static ProcessModelManager create(final SpecificationLoader loader, final FormSpecificationProcess process, final IProvider stepProvider) {
        final EngineContext<FormSpecificationProcess> context = EngineContext.build(loader).target(process).withProvider(stepProvider);
        return new ProcessModelManager(context);
    }

    /**
     * Creates the.
     *
     * @param context
     *            the context
     * @return the process model manager
     */
    public static ProcessModelManager create(final EngineContext<FormSpecificationProcess> context) {
        return new ProcessModelManager(context);
    }

    /**
     * Clean process.
     *
     * @param process
     *            the process
     */
    private void cleanProcess(final IProcess process) {
        if (process instanceof ProcessElement) {
            final ProcessElement processElement = (ProcessElement) process;
            if (StringUtils.isEmpty(processElement.getValue())) {
                if (StringUtils.isEmpty(processElement.getScript())) {
                    LOGGER.debug("process #{} has no attached script", processElement.getId());
                } else {
                    final byte[] scriptAsBytes = this.context.getProvider().asBytes(processElement.getScript());
                    if (ArrayUtils.isNotEmpty(scriptAsBytes)) {
                        processElement.setValue(new String(scriptAsBytes, StandardCharsets.UTF_8));
                    }
                }
            }
        }
    }

}