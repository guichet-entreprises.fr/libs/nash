/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader.template;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.script.EngineContext;

/**
 * Extract enhanced model tree from data description.
 *
 * @author Christian Cougourdan
 */
public class TemplateModelExtractor {

    private final EngineContext<FormSpecificationData> engineContext;

    /**
     * Instanciate extractor using a step data engine context.
     *
     * @param engineContext
     *            step data engine context
     */
    public TemplateModelExtractor(final EngineContext<FormSpecificationData> engineContext) {
        this.engineContext = engineContext;
    }

    /**
     * Extract from a specific element.
     *
     * @param elm
     *            element to extract
     * @return parsed tree root element wrapper
     */
    public NodeWrapper extract(final IElement<?> elm) {
        if (elm instanceof DataElement) {
            return this.extract((DataElement) elm);
        } else {
            return this.extract((GroupElement) elm);
        }
    }

    private GroupElementWrapper extract(final GroupElement grp) {
        final GroupElementWrapper wrapper = new GroupElementWrapper(grp);

        for (final IElement<?> elm : grp.getData()) {
            elm.setParentPath(wrapper.getElm().getPath());
            wrapper.add(this.extract(elm));
        }

        return wrapper;
    }

    private DataElementWrapper extract(final DataElement elm) {
        final IValueAdapter<?> adapter = ValueAdapterFactory.type(this.engineContext.getElement(), elm);
        return new DataElementWrapper(elm) //
                .setAdapter(adapter) //
                .setValue(adapter.get(this.engineContext.getProvider(), elm)) //
        ;
    }

}
