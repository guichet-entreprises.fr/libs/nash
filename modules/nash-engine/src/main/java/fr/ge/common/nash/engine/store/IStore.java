package fr.ge.common.nash.engine.store;

import java.util.Collection;

import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.bean.roles.EntityRole;

/**
 * Represents a final store, like nash module, storage, or file system.
 *
 * @author Christian Cougourdan
 */
public interface IStore {

    /**
     * Send a record to the store.
     *
     * @param provider
     *            record provider
     * @param roles
     *            entity roles
     * @return new record provider linked to store
     */
    IProvider upload(IProvider provider, Collection<EntityRole> roles);

}
