package fr.ge.common.nash.engine.loader.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.exception.TechnicalException;

/**
 * @author Christian Cougourdan
 */
public class EventBus {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventBus.class);

    private final Map<Class<?>, Collection<Invocation>> invocations = new ConcurrentHashMap<>();

    public void post(final Object... events) throws TechnicalException {
        for (final Object event : events) {
            if (this.invocations.containsKey(event.getClass())) {
                for (final Invocation invocation : this.invocations.get(event.getClass())) {
                    invocation.invoke(event);
                }
            }
        }
    }

    public void register(final Object subscriber) {
        for (Class<?> type = subscriber.getClass(); null != type; type = type.getSuperclass()) {
            final Collection<Method> methods = this.findSubscriptionMethods(type);
            for (final Method method : methods) {
                final Class<?> parameterType = method.getParameterTypes()[0];
                if (!this.invocations.containsKey(parameterType)) {
                    this.invocations.put(parameterType, new LinkedList<>());
                }
                this.invocations.get(parameterType).add(new Invocation(method, subscriber));
            }
        }
    }

    private Collection<Method> findSubscriptionMethods(final Class<?> type) {
        final Collection<Method> methods = Arrays.stream(type.getDeclaredMethods()) //
                .filter(method -> method.isAnnotationPresent(Subscribe.class)) //
                .collect(Collectors.toList());

        for (final Method method : methods) {
            final int nb = method.getParameterCount();
            if (0 == nb) {
                throw new IllegalArgumentException("@Subscribe method does not have any parameter");
            } else if (nb > 1) {
                throw new IllegalArgumentException("@Subscribe method has more than one parameter");
            }
        }

        return methods;
    }

    /**
     * Represents a method invocation on a targeted object.
     *
     * @author Christian Cougourdan
     */
    private static class Invocation {

        private final Method method;

        private final Object target;

        /**
         * Create invocation.
         *
         * @param method
         *            method
         * @param target
         *            targeted object
         */
        public Invocation(final Method method, final Object target) {
            this.method = method;
            this.target = target;
        }

        /**
         * Invoke method on targeted object using provided value as unique
         * method parameter.
         *
         * @param value
         *            unique method parameter
         */
        public void invoke(final Object value) {
            try {
                this.method.invoke(this.target, value);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOGGER.warn("Invoking method {} of class {} : {}", this.method.getName(), this.method.getDeclaringClass().getSimpleName(), ex.getMessage(), ex);
                if (ex.getCause() instanceof TechnicalException) {
                    throw new TechnicalException(ex);
                }
            }
        }

    }

}
