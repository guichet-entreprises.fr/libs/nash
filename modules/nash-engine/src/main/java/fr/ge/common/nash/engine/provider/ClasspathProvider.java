/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.provider;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.utils.bean.FileEntry;

/**
 * Classpath provider.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class ClasspathProvider implements IProvider {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ClasspathProvider.class);

    /** The class to search from. */
    private final Class<?> clazz;

    /** The path of the resource. */
    private final String path;

    /**
     * Constructor.
     *
     * @param clazz
     *            the class
     * @param path
     *            the path
     */
    public ClasspathProvider(final Class<?> clazz, final String path) {
        this.clazz = clazz;
        this.path = path;
    }

    /**
     * Finds a file in the classpath.
     *
     * @param subPath
     *            the sub path within the provider path
     * @return the file as bytes
     */
    private byte[] find(final String subPath) {
        final InputStream resource = this.clazz.getResourceAsStream(this.path + subPath);
        byte[] resourceAsBytes = null;
        if (resource != null) {
            try {
                resourceAsBytes = IOUtils.toByteArray(resource);
            } catch (final IOException e) {
                LOGGER.info(StringUtils.EMPTY, e);
            }
        }
        return resourceAsBytes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FileEntry load(final String name) {
        final byte[] resourceAsBytes = this.find(this.getAbsoluteClasspathResource(name));
        if (null == resourceAsBytes) {
            return null;
        } else {
            return new FileEntry(name).asBytes(resourceAsBytes);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] asBytes() {
        return this.find(StringUtils.EMPTY);
    }

    /**
     * Gets the absolute classpath resource.
     *
     * @param name
     *            the name
     * @return the absolute classpath resource
     */
    private String getAbsoluteClasspathResource(final String name) {
        if (name != null && name.startsWith("/")) {
            return name;
        } else {
            return "/" + name;
        }
    }

}
