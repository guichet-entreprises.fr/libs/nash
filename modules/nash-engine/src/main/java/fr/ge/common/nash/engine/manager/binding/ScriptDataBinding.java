/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.binding;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.script.Bindings;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Script value wrapper.
 *
 * @author Christian Cougourdan
 */
public class ScriptDataBinding implements DataBinding {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptDataBinding.class);

    private static final Pattern HEAD = Pattern.compile("^(?:([^.\\[]+)(?:\\[([0-9]*)\\])?)(?:\\.(.+))?");

    private static final int HEAD_GRP_ROOT_KEY = 1;
    private static final int HEAD_GRP_TAIL = 3;

    private Class<?> scriptObjectMirrorClass;

    private Method isArrayMethod;

    private final Object source;

    /* cache for inner values, retrieved by withPath method */
    private final Map<String, DataBinding> subValues = new HashMap<>();

    /* cache for source value as map */
    private Map<String, DataBinding> sourceAsMap;

    public ScriptDataBinding(final Object source) {
        this.source = source;
    }

    @Override
    public boolean isArray() {
        if (this.source instanceof Collection) {
            return true;
        }

        if (null == this.scriptObjectMirrorClass) {
            try {
                this.scriptObjectMirrorClass = Class.forName("jdk.nashorn.api.scripting.ScriptObjectMirror");
            } catch (final ClassNotFoundException e) {
                LOGGER.warn("Nashorn classe [ScriptObjectMirror] not present");
                return false;
            }
        }

        if (this.scriptObjectMirrorClass.isInstance(this.source)) {
            if (null == this.isArrayMethod) {
                try {
                    this.isArrayMethod = this.scriptObjectMirrorClass.getMethod("isArray");
                } catch (NoSuchMethodException | SecurityException e) {
                    LOGGER.warn("Nashorn classe [ScriptObjectMirror] has no method [isArray]");
                    return false;
                }
            }

            try {
                return (boolean) this.isArrayMethod.invoke(this.source);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                LOGGER.warn("[ScriptObjectMirror.isArray(...)] invocation error");
            }
        }

        return false;
    }

    @Override
    public boolean isMap() {
        return this.source instanceof Map && !this.isArray();
    }

    @Override
    public List<DataBinding> asList() {
        if (this.source instanceof Collection) {
            return ((Collection<?>) this.source).stream().map(ScriptDataBinding::new).collect(Collectors.toList());
        } else if (this.isArray() && this.source instanceof Bindings) {
            return ((Bindings) this.source).values().stream().map(ScriptDataBinding::new).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    @Override
    public Map<String, DataBinding> asMap() {
        if (null == this.sourceAsMap) {
            if (this.isMap()) {
                final Map<String, DataBinding> map = new HashMap<>();
                for (final Entry<String, Object> entry : ((Map<String, Object>) this.source).entrySet()) {
                    map.put(entry.getKey(), new ScriptDataBinding(entry.getValue()));
                }
                this.sourceAsMap = map;
            } else {
                this.sourceAsMap = Collections.emptyMap();
            }
        }

        return this.sourceAsMap;
    }

    @Override
    public String asString() {
        return null == this.source ? null : this.source.toString();
    }

    @Override
    public Integer asInt() {
        if (this.source instanceof Number) {
            return ((Number) this.source).intValue();
        } else if (null != this.source) {
            final String asString = this.source.toString();
            try {
                return Integer.parseInt(asString);
            } catch (final NumberFormatException ex) {
                LOGGER.info("Unable to parse '{}' as integer ({})", asString, ex.getMessage());
            }
        }
        return null;
    }

    @Override
    public DataBinding withPath(final String path) {
        if (!this.isMap()) {
            return null;
        }

        if (this.subValues.containsKey(path)) {
            return this.subValues.get(path);
        }

        final Matcher m = HEAD.matcher(path);
        if (m.matches()) {
            final String key = m.group(HEAD_GRP_ROOT_KEY);
            final String tail = m.group(HEAD_GRP_TAIL);
            final ScriptDataBinding binding = new ScriptDataBinding(((Map<String, Object>) this.source).get(key));
            this.subValues.put(key, binding);
            if (StringUtils.isNotEmpty(tail)) {
                final DataBinding subBinding = binding.withPath(tail);
                this.subValues.put(path, subBinding);
                return subBinding;
            } else {
                return binding;
            }
        }

        return null;
    }

}
