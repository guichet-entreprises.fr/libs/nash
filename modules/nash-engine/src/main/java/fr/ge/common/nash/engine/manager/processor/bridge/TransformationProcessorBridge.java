/**
 *
 */
package fr.ge.common.nash.engine.manager.processor.bridge;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class TransformationProcessorBridge.
 *
 * @author bsadil
 */
public class TransformationProcessorBridge extends AbstractBridge {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TransformationProcessorBridge.class);

    /** The factory. */
    private final TransformerFactory factory;

    /**
     * Instantiates a new processor.
     *
     * @param context
     *            the context
     */
    public TransformationProcessorBridge(final EngineContext<?> context) {
        super(context);
        this.factory = TransformerFactory.newInstance();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "xml";
    }

    /**
     * transform xml file to another xml with xslt.
     *
     * @param srcFilePath
     *            the source file path
     * @param xsl
     *            the xsl
     * @param output
     *            the output
     * @throws TransformerException
     *             the transformer exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void transform(final String srcFilePath, final String xsl, final String output) throws TransformerException, IOException {

        final Source xslt = new StreamSource(new ByteArrayInputStream(this.getProvider().asBytes(xsl)));
        final Transformer transformer = this.factory.newTransformer(xslt);
        final Source text = new StreamSource(new ByteArrayInputStream(this.getProvider().asBytes(srcFilePath)));

        final IProvider provider = this.getProvider();
        transformer.setURIResolver((href, base) -> new StreamSource(new ByteArrayInputStream(provider.asBytes(href))));

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            final StreamResult streamResult = new StreamResult(out);
            transformer.transform(text, streamResult);

            final byte[] arr = out.toByteArray();
            this.getProvider().save(output, arr);
        }

    }

    /**
     * Extract data from XML content.
     * 
     * @param xmlFilePath
     *            the XML file path
     * @param expression
     *            the XPath expression
     * @return the extracted data
     * @throws TechnicalException
     */
    public JsonNode extract(final String xmlFilePath, final String expression) throws TechnicalException {
        if (StringUtils.isEmpty(xmlFilePath) || StringUtils.isEmpty(expression)) {
            return null;
        }

        try {
            final IProvider provider = this.getProvider();
            final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            final DocumentBuilder builder = builderFactory.newDocumentBuilder();
            final Document xmlDocument = builder.parse(new ByteArrayInputStream(provider.asBytes(xmlFilePath)));
            final XPath xPath = XPathFactory.newInstance().newXPath();
            final NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

            if (null == nodeList) {
                return null;
            }

            final Document virtualDocument = builder.newDocument();
            final Element root = virtualDocument.createElement("root");
            virtualDocument.appendChild(root);
            for (int index = 0; index < nodeList.getLength(); index++) {
                final Node child = nodeList.item(index);
                virtualDocument.adoptNode(child);
                root.appendChild(child);
            }

            final ObjectMapper xmlMapper = new XmlMapper();
            final StringWriter writer = new StringWriter();
            factory.newTransformer().transform(new DOMSource(root), new StreamResult(writer));

            return xmlMapper.readTree(writer.toString());
        } catch (ParserConfigurationException | SAXException | XPathExpressionException | TransformerException | IOException e) {
            LOGGER.error("Cannot extract data from xml file", e);
            throw new TechnicalException(e);
        }
    }
}
