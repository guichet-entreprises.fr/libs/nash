/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Interface IElement.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public interface IElement<T extends IElement<T>> extends IConditionalDisplayElement, Serializable {

    /**
     * Gets the id.
     *
     * @return the id
     */
    String getId();

    /**
     * Sets the id.
     *
     * @param id
     *            the id
     */
    void setId(String id);

    /**
     * Gets the path.
     *
     * @return the path
     */
    String getPath();

    /**
     * Sets the path.
     *
     * @param path
     *            the path
     * @return the i element
     */
    T setPath(String path);

    /**
     * Gets the parent path.
     *
     * @return the parent path
     */
    String getParentPath();

    /**
     * Sets the parent path.
     *
     * @param parentPath
     *            the parent path
     * @return the t
     */
    T setParentPath(String parentPath);

    /**
     * Gets the label.
     *
     * @return the label
     */
    String getLabel();

    /**
     * Gets the description.
     *
     * @return the description
     */
    String getDescription();

    /**
     * Gets the help.
     *
     * @return the help
     */
    String getHelp();

    /**
     * Gets the value.
     *
     * @return the value
     */
    Object getValue();

    /**
     * Gets the min occurs.
     *
     * @return the min occurs
     */
    String getMinOccurs();

    /**
     * Gets the max occurs.
     *
     * @return the max occurs
     */
    String getMaxOccurs();

    /**
     * Return true if element is a multiple occurrences one, ie having
     * attributes minOccurs and/or maxOccurs filled.
     * 
     * @return true if element is a multiple occurrences one
     */
    boolean isRepeatable();

    /**
     * Gets the trigger as tag.
     * 
     * @return the trigger script
     */
    String getTriggerAsTag();

    /**
     * Gets the trigger as attribute.
     * 
     * @return the trigger script
     */
    String getTriggerAsAttr();

    /**
     * Sets the trigger as tag.
     * 
     * @param triggerAsTag
     * @return the element
     */
    void setTriggerAsTag(final String triggerAsTag);

    /**
     * Sets the trigger as attribute.
     * 
     * @param triggerAsTag
     * @return the element
     */
    void setTriggerAsAttr(final String triggerAsAttr);

    /**
     * Gets the datas.
     *
     * @return the datas
     */
    default List<IElement<?>> getData() {
        return new ArrayList<>();
    }

    /**
     * Gets the trigger.
     * 
     * @return the trigger script
     */
    String getTrigger();

    /**
     * Sets the trigger.
     * 
     * @param trigger
     * @return the element
     */
    void setTrigger(final String trigger);
}
