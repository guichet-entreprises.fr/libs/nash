/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data.value;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.core.exception.TechnicalException;

/**
 * The Class ListValueElement.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "list")
@XmlAccessorType(XmlAccessType.NONE)
public class ListValueElement extends AbstractIdentifiedValueElement implements IWrappedValueElement {

    /** The elements. */
    @XmlElementRefs({ //
            @XmlElementRef(type = FileValueElement.class, name = "file"), //
            @XmlElementRef(type = TextValueElement.class, name = "text"), //
            @XmlElementRef(type = DateValueElement.class, name = "date"), //
            @XmlElementRef(type = ListValueElement.class, name = "list") //
    })
    private List<ITypedValueElement> elements;

    /**
     * Instantiates a new list value element.
     */
    public ListValueElement() {
        // Nothing to do
    }

    /**
     * Instantiates a new list value element.
     *
     * @param elements
     *            the elements
     */
    public ListValueElement(final ITypedValueElement... elements) {
        this.elements = Optional.ofNullable(elements).filter(lst -> lst.length > 0).map(lst -> Arrays.asList(lst)).orElse(null);
    }

    /**
     * Instantiates a new list value element.
     *
     * @param elements
     *            the elements
     */
    public ListValueElement(final List<ITypedValueElement> elements) {
        this.elements = Optional.ofNullable(elements).filter(lst -> !lst.isEmpty()).orElse(null);
    }

    /**
     * Gets the elements.
     *
     * @return the elements
     */
    public List<ITypedValueElement> getElements() {
        return this.elements;
    }

    /**
     * Sets the elements.
     *
     * @param elements
     *            the elements to set
     */
    public void setElements(final List<ITypedValueElement> elements) {
        this.elements = Optional.ofNullable(elements).filter(lst -> !lst.isEmpty()).orElse(null);
    }

    /**
     * Return elements list as a map, using {@link AbstractIdentifiedValueElement}.
     * Only {@link AbstractIdentifiedValueElement} element are returned.
     *
     * @return elements list as {@link Map}
     */
    public Map<String, ITypedValueElement> asMap() {
        return this.asMap(ITypedValueElement.class);
    }

    /**
     * Return elements list as a map, using {@link AbstractIdentifiedValueElement}.
     * Only element of specified type are returned.
     *
     * @param <T>
     *            the type of filtered elements
     * @param clazz
     *            class of filtered elements
     * @return elements list as {@link Map}
     */
    public <T> Map<String, T> asMap(final Class<T> clazz) {
        final Map<String, T> map = new HashMap<>();

        if (!ITypedValueElement.class.isAssignableFrom(clazz)) {
            throw new TechnicalException("Transcoding list to map : incorrect filtered class " + clazz);
        }

        if (null != this.elements) {
            this.elements.stream() //
                    .filter(elm -> null != elm) //
                    .filter(elm -> clazz.isAssignableFrom(elm.getClass())) //
                    .map(clazz::cast) //
                    .forEach(elm -> map.put(((ITypedValueElement) elm).getId(), elm));
        }

        return map;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        String str;
        if (null == this.elements) {
            str = "null";
        } else if (this.elements.isEmpty()) {
            str = "[]";
        } else {
            str = String.format("[ %s ]", this.elements.stream().map(Object::toString).collect(Collectors.joining(", ")));
        }

        return String.format("{ %s, \"value\": %s }", super.toString(), str);
    }

}
