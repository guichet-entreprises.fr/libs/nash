package fr.ge.common.nash.engine.support.thymeleaf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.exceptions.TemplateInputException;
import org.thymeleaf.standard.expression.Assignation;
import org.thymeleaf.standard.expression.AssignationSequence;
import org.thymeleaf.standard.expression.AssignationUtils;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;

/**
 *
 * @author Christian Cougourdan
 */
public final class FragmentUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FragmentUtil.class);

    private FragmentUtil() {
        // Nothing to do
    }

    /**
     * Parses the value.
     *
     * @param <T>
     *            the generic type
     * @param context
     *            context
     * @param attributeValue
     *            the attribute value
     * @param expectedClass
     *            the expected class
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public static <T> T parseValue(final ITemplateContext context, final String attributeValue, final Class<T> expectedClass) {
        final IEngineConfiguration configuration = context.getConfiguration();
        final IStandardExpressionParser expressionParser = StandardExpressions.getExpressionParser(configuration);

        final IStandardExpression expression = expressionParser.parseExpression(context, attributeValue);

        final Object obj = expression.execute(context);

        if (null != obj && expectedClass.isAssignableFrom(obj.getClass())) {
            return (T) obj;
        } else {
            return null;
        }
    }

    /**
     * Parses the value.
     *
     * @param context
     *            context
     * @param attributeValue
     *            the attribute value
     * @return the object
     */
    public static Object parseValue(final ITemplateContext context, final String attributeValue) {
        return parseValue(context, attributeValue, Object.class);
    }

    /**
     * Parses the assignation.
     *
     * @param context
     *            context
     * @param attributeValue
     *            the attribute value
     * @param expected
     *            the expected
     * @return the map
     */
    public static Map<String, Object> parseAssignation(final ITemplateContext context, final String attributeValue, final Map<String, Class<?>> expected) {
        final Map<String, Object> model = new HashMap<>();

        final AssignationSequence parametersAsSeq = AssignationUtils.parseAssignationSequence(context, attributeValue, false);
        for (final Assignation assignation : parametersAsSeq) {
            final String key = (String) assignation.getLeft().execute(context);
            final Object value = assignation.getRight().execute(context);
            final Class<?> expectedClass = expected.get(key);

            if (value == null) {
                throw new TemplateInputException("Expecting value for parameter " + key);
            } else if (null == expectedClass || expectedClass.isAssignableFrom(value.getClass())) {
                model.put(key, value);
            } else {
                throw new TemplateInputException("Unexpected \"" + key + "\" parameter type : " + value.getClass().getName());
            }
        }

        return model;
    }

    /**
     * Walk through group element children and filter duplicate ones.
     *
     * @param element
     *            parent group element
     * @return filtered children list
     */
    public static Collection<IElement<?>> getFilteredChildren(final GroupElement element) {
        /*
         * Create a list of children elements without repeated ones
         */
        final Collection<String> known = new ArrayList<>();
        final Collection<IElement<?>> children = new ArrayList<>();

        for (final IElement<?> elm : element.getData()) {
            final String key = ElementPath.create(elm.getId()).getId();
            if (!known.contains(key)) {
                known.add(key);
                children.add(elm);
            }
        }

        return children;
    }

    public static String getDataTemplate(final Object obj, final IValueAdapter<?> dataType) {
        if (null == obj) {
            throw new TechnicalException("no data presents");
        } else if (obj instanceof DataElement) {
            final DataElement dataElement = (DataElement) obj;
            if (null == dataType) {
                LOGGER.warn("No data element type specified for '" + dataElement.getId() + "'");
                return "no-type";
            } else {
                return dataType.template() + "-v3";
            }
        } else if (obj instanceof GroupElement) {
            return "group";
        } else {
            throw new TechnicalException("unknown element of type " + obj.getClass().getName());
        }
    }

}
