/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.support.thymeleaf;

import java.util.Arrays;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.Fragment;
import org.thymeleaf.standard.expression.FragmentExpression;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.NoOpToken;
import org.thymeleaf.standard.expression.StandardExpressionExecutionContext;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;

import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.options.MutableDataHolder;
import com.vladsch.flexmark.util.options.MutableDataSet;

/**
 * Markdown dialect attribute which allow to insert markdown data as html.
 *
 * @author Christian Cougourdan
 */
public class MarkdownAttrProcessor extends AbstractAttributeTagProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(MarkdownAttrProcessor.class);

    public static final int PRECEDENCE = 1000;

    public static final String ATTR_NAME = "md";

    private static final MutableDataHolder OPTIONS = new MutableDataSet() //
            .set(HtmlRenderer.INDENT_SIZE, 4) //
            .set(HtmlRenderer.PERCENT_ENCODE_URLS, true) //
            .set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create()));

    /** The parser. */
    private static final Parser parser = Parser.builder(OPTIONS).build();

    /** The renderer. */
    private static final HtmlRenderer renderer = HtmlRenderer.builder(OPTIONS).build();

    public MarkdownAttrProcessor(final TemplateMode templateMode, final String dialectPrefix) {
        super(templateMode, dialectPrefix, null, false, ATTR_NAME, true, PRECEDENCE, true);
    }

    @Override
    protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag, final AttributeName attributeName, final String attributeValue,
            final IElementTagStructureHandler structureHandler) {
        final IEngineConfiguration configuration = context.getConfiguration();
        final IStandardExpressionParser expressionParser = StandardExpressions.getExpressionParser(configuration);
        final IStandardExpression expression = expressionParser.parseExpression(context, attributeValue);

        final Object expressionResult;
        if (null == expression) {
            return;
        } else {
            if (expression instanceof FragmentExpression) {
                LOGGER.warn("Unable to parse fragment expression : {}", expression);
                return;
            } else {
                expressionResult = expression.execute(context, StandardExpressionExecutionContext.RESTRICTED);
            }
        }

        if (expressionResult == NoOpToken.VALUE) {
            return;
        }

        if (expressionResult instanceof Fragment) {
            LOGGER.warn("Unable to process fragment : {}", expression);
            return;
        }

        final String stringToInsert = Optional.ofNullable(expressionResult).map(Object::toString).map(this::convertMarkdownToHtml).orElse("");
        structureHandler.setBody(stringToInsert, false);
    }

    /**
     * Converts markdown to HTML.
     *
     * @param src
     *            the source
     * @return the rendering
     */
    private String convertMarkdownToHtml(final String src) {
        if (null == src) {
            return null;
        } else {
            return renderer.render(parser.parse(src));
        }
    }

}
