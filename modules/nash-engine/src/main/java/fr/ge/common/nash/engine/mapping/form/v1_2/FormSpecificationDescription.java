/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import fr.ge.common.nash.engine.mapping.IXml;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.EventsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.IFlowElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.ReferenceElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.ReferentialsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.YieldElement;

/**
 * Form description object containing minimal information about form
 * specification.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "description")
@XmlAccessorType(XmlAccessType.NONE)
public class FormSpecificationDescription implements IXml {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The language. */
    @XmlAttribute(name = "lang")
    private String lang;

    /** The form uid. */
    @XmlElement(name = "form-uid")
    private String formUid;

    /** The record uid. */
    @XmlElement(name = "record-uid")
    private String recordUid;

    @XmlElement(name = "origin")
    private String origin;

    /** The author. */
    @XmlElement(name = "author")
    private String author;

    /** The record stat. */
    @XmlElement(name = "status")
    private String status;

    /** The reference. */
    @XmlElement(name = "reference-id")
    private ReferenceElement reference;

    /** The title. */
    @XmlElement(name = "title")
    private String title;

    /** The action. */
    @XmlElement(name = "action")
    private String action;

    /** The version. */
    @XmlAttribute(required = true)
    private String version;

    /** The description. */
    @XmlElement(name = "description")
    private String description;

    /** The referentials. */
    @XmlElement(name = "referentials")
    private ReferentialsElement referentials;

    /** The translations. */
    @XmlElement(name = "translations")
    private TranslationsElement translations;

    /** The history path. */
    @XmlElement(name = "history")
    private String historyPath;

    /** The steps. */
    @XmlElementWrapper(name = "steps")
    @XmlElements({ //
            @XmlElement(name = "step", type = StepElement.class), //
            @XmlElement(name = "yield", type = YieldElement.class) //
    })
    private List<? extends IFlowElement> flow;

    @XmlTransient
    private StepsElement steps;

    /** The anything. */
    // @XmlAnyElement(lax = true)
    private List<Serializable> anything;

    /** The steps. */
    @XmlElement(name = "events")
    private EventsElement events;

    /** The color. */
    @XmlAttribute(name = "color")
    private String color;

    /** The icon. */
    @XmlAttribute(name = "icon")
    private String icon;

    /**
     * Getter on attribute {@link #lang}.
     *
     * @return String lang
     */
    public String getLang() {
        return this.lang;
    }

    /**
     * Setter on attribute {@link #lang}.
     *
     * @param lang
     *            the new value of attribute lang
     */
    public void setLang(final String lang) {
        this.lang = lang;
    }

    /**
     * Gets the form uid.
     *
     * @return the formUid
     */
    public String getFormUid() {
        return this.formUid;
    }

    /**
     * Sets the form uid.
     *
     * @param formUid
     *            the formUid to set
     */
    public void setFormUid(final String formUid) {
        this.formUid = formUid;
    }

    /**
     * Gets the record uid.
     *
     * @return the recordUid
     */
    public String getRecordUid() {
        return this.recordUid;
    }

    /**
     * Sets the record uid.
     *
     * @param recordUid
     *            the recordUid to set
     */
    public void setRecordUid(final String recordUid) {
        this.recordUid = recordUid;
    }

    /**
     * @return the origin
     */
    public String getOrigin() {
        return this.origin;
    }

    /**
     * @param origin
     *            the origin to set
     */
    public void setOrigin(final String origin) {
        this.origin = origin;
    }

    /**
     * Getter on attribute {@link #author}.
     *
     * @return String author
     */
    public String getAuthor() {
        return this.author;
    }

    /**
     * Setter on attribute {@link #author}.
     *
     * @param author
     *            the new value of attribute author
     */
    public void setAuthor(final String author) {
        this.author = author;
    }

    /**
     * Gets the reference.
     *
     * @return the reference
     */
    public ReferenceElement getReference() {
        return this.reference;
    }

    /**
     * Sets the reference.
     *
     * @param reference
     *            the reference to set
     */
    public void setReference(final ReferenceElement reference) {
        this.reference = reference;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Sets the title.
     *
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    public String getAction() {
        return this.action;
    }

    /**
     * Sets the action.
     *
     * @param action
     *            the action to set
     */
    public void setAction(final String action) {
        this.action = action;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version to set
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Getter on attribute {@link #referentials}.
     *
     * @return ReferentialsElement referentials
     */
    public ReferentialsElement getReferentials() {
        return this.referentials;
    }

    /**
     * Setter on attribute {@link #referentials}.
     *
     * @param referentials
     *            the new value of attribute referentials
     */
    public void setReferentials(final ReferentialsElement referentials) {
        this.referentials = referentials;
    }

    /**
     * Getter on attribute {@link #translations}.
     *
     * @return TranslationsElement translations
     */
    public TranslationsElement getTranslations() {
        return this.translations;
    }

    /**
     * Setter on attribute {@link #translations}.
     *
     * @param translations
     *            the new value of attribute translations
     */
    public void setTranslations(final TranslationsElement translations) {
        this.translations = translations;
    }

    /**
     * Getter on attribute {@link #historyPath}.
     *
     * @return String historyPath
     */
    public String getHistoryPath() {
        return this.historyPath;
    }

    /**
     * Setter on attribute {@link #historyPath}.
     *
     * @param historyPath
     *            the new value of attribute historyPath
     */
    public void setHistoryPath(final String historyPath) {
        this.historyPath = historyPath;
    }

    /**
     * Gets the steps.
     *
     * @return the steps
     */
    public StepsElement getSteps() {
        if (null == this.steps && null != this.flow) {
            this.steps = new StepsElement(this.flow.stream() //
                    .filter(StepElement.class::isInstance) //
                    .map(StepElement.class::cast) //
                    .collect(Collectors.toList()) //
            );

        }

        return this.steps;
    }

    /**
     * Sets the steps.
     *
     * @param steps
     *            the new steps
     */
    public void setSteps(final StepsElement steps) {
        this.setFlow(Optional.ofNullable(steps).map(StepsElement::getElements).orElse(null));
    }

    /**
     * @return the flow
     */
    public List<IFlowElement> getFlow() {
        return Optional.ofNullable(this.flow).map(ArrayList<IFlowElement>::new).orElse(null);
    }

    /**
     * @param flow
     *            the flow to set
     */
    public void setFlow(final List<? extends IFlowElement> flow) {
        this.flow = Optional.ofNullable(flow).map(ArrayList::new).orElse(null);
        this.steps = null;
        this.updateStepPositions();
    }

    /**
     * Gets the anything.
     *
     * @return the anything
     */
    public List<Serializable> getAnything() {
        return this.anything;
    }

    /**
     * Sets the anything.
     *
     * @param anything
     *            the anything to set
     */
    public void setAnything(final List<Serializable> anything) {
        this.anything = Optional.ofNullable(anything).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

    /**
     * Gets the status.
     *
     * @return the recordStatus
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Sets the record status.
     *
     * @param status
     *            the record status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Accesseur sur l'attribut {@link #events}.
     *
     * @return EventsElement events
     */
    public EventsElement getEvents() {
        return this.events;
    }

    /**
     * Mutateur sur l'attribut {@link #events}.
     *
     * @param events
     *            la nouvelle valeur de l'attribut events
     */
    public void setEvents(final EventsElement events) {
        if (null == events) {
            this.events = new EventsElement();
        } else {
            this.events = events;
        }
    }

    /**
     * Accesseur sur l'attribut {@link #color}.
     *
     * @return String color
     */
    public String getColor() {
        return this.color;
    }

    /**
     * Mutateur sur l'attribut {@link #color}.
     *
     * @param color
     *            la nouvelle valeur de l'attribut color
     */
    public void setColor(final String color) {
        this.color = color;
    }

    /**
     * Accesseur sur l'attribut {@link #icon}.
     *
     * @return String icon
     */
    public String getIcon() {
        return this.icon;
    }

    /**
     * Mutateur sur l'attribut {@link #icon}.
     *
     * @param icon
     *            la nouvelle valeur de l'attribut icon
     */
    public void setIcon(final String icon) {
        this.icon = icon;
    }

    /**
     * After unmarshal.
     *
     * @param unmarshaller
     *            the unmarshaller
     * @param parent
     *            the parent
     */
    public void afterUnmarshal(final Unmarshaller unmarshaller, final Object parent) {
        this.updateStepPositions();
    }

    private void updateStepPositions() {
        if (null != this.flow) {
            int idx = 0;
            for (final IFlowElement elm : this.flow) {
                if (elm instanceof StepElement) {
                    ((StepElement) elm).setPosition(idx++);
                }
            }
        }
    }

}
