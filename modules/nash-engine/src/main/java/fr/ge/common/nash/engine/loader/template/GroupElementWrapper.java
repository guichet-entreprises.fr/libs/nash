/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader.template;

import java.util.HashMap;
import java.util.Map;

import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;

/**
 * Group element wrapper.
 *
 * @author Christian Cougourdan
 */
public class GroupElementWrapper extends NodeWrapper {

    private final Map<String, NodeWrapper> children = new HashMap<>();

    GroupElementWrapper(final GroupElement elm) {
        super(elm);
    }

    /**
     * Add a child to this group.
     *
     * @param child
     *            new child element wrapper
     * @return this instance
     */
    public GroupElementWrapper add(final NodeWrapper child) {
        final String key = child.getPath().getId();
        if (child.getElm().isRepeatable()) {
            RepeatableElementWrapper repeatable = (RepeatableElementWrapper) this.children.get(key);
            if (null == repeatable) {
                child.setPath(child.getPath().withIndex(0));
                repeatable = new RepeatableElementWrapper(child);
                this.children.put(key, repeatable);
            } else {
                child.setPath(child.getPath().withIndex(repeatable.size()));
                repeatable.add(child);
            }
        } else {
            this.children.put(key, child);
        }
        return this;
    }

    /**
     * Retrieve all group child element wrapper.
     *
     * @return the children
     */
    public Map<String, NodeWrapper> getChildren() {
        return this.children;
    }

}
