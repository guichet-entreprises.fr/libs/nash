/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data.value;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * The Class FileValueElement.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "file")
@XmlAccessorType(XmlAccessType.NONE)
public class FileValueElement extends AbstractIdentifiedValueElement implements ITypedValueElement {

    /** The ref. */
    @XmlAttribute(name = "ref")
    private String ref;

    /** The label. */
    @XmlValue
    private String label;

    /**
     * Instantiates a new file value element.
     */
    public FileValueElement() {
        this(null, null);
    }

    /**
     * Instantiates a new file value element.
     *
     * @param ref
     *            the ref
     * @param label
     *            the label
     */
    public FileValueElement(final String ref, final String label) {
        this.ref = ref;
        this.label = label;
    }

    /**
     * Gets the ref.
     *
     * @return the ref
     */
    public String getRef() {
        return this.ref;
    }

    /**
     * Sets the ref.
     *
     * @param ref
     *            the ref to set
     */
    public void setRef(final String ref) {
        this.ref = ref;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Sets the label.
     *
     * @param label
     *            the label to set
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format( //
                "{ %s, \"ref\": %s, \"label\": %s }", //
                super.toString(), //
                null == this.ref ? null : '"' + this.ref + '"', //
                null == this.label ? null : '"' + this.label + '"' //
        );
    }

}
