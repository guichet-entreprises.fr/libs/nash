/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import fr.ge.common.nash.engine.mapping.IFormSpecificationResource;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.AbstractIdentifiedElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessesIfElement;

/**
 * The Class FormSpecificationProcess.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "processes")
@XmlAccessorType(XmlAccessType.NONE)
public class FormSpecificationProcess extends AbstractIdentifiedElement implements IFormSpecificationResource {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The processes or "if" elements. */
    @XmlElements({ //
            @XmlElement(name = "if", type = ProcessesIfElement.class), //
            @XmlElement(name = "process", type = ProcessElement.class) //
    })
    private List<IProcess> processes;

    /** The anything. */
    @XmlAnyElement(lax = true)
    private List<Serializable> anything;

    /** The type. */
    @XmlTransient
    private String type;

    /** The resource name. */
    @XmlTransient
    private String resourceName;

    /**
     * Getter on attribute {@link #processes}.
     *
     * @return List&lt;IProcess&gt; processes
     */
    public List<IProcess> getProcesses() {
        return this.processes;
    }

    /**
     * Setter on attribute {@link #processes}.
     *
     * @param processes
     *            the new value of attribute processes
     */
    public void setProcesses(final List<IProcess> processes) {
        this.processes = Optional.ofNullable(processes).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

    /**
     * Gets the anything.
     *
     * @return the anything
     */
    public List<Serializable> getAnything() {
        return this.anything;
    }

    /**
     * Sets the anything.
     *
     * @param anything
     *            the anything to set
     */
    public void setAnything(final List<Serializable> anything) {
        this.anything = Optional.ofNullable(anything).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the resource name.
     *
     * @return the resource name
     */
    @Override
    public String getResourceName() {
        return this.resourceName;
    }

    /**
     * Sets the resource name.
     *
     * @param resourceName
     *            the new resource name
     */
    public void setResourceName(final String resourceName) {
        this.resourceName = resourceName;
    }

}
