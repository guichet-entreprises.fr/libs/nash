package fr.ge.common.nash.engine.store;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.bean.roles.EntityRole;

/**
 * Void store, no persistence just a hole.
 *
 * @author Christian Cougourdan
 */
public class VoidStore implements IStore {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoidStore.class);

    @Override
    public IProvider upload(final IProvider provider, final Collection<EntityRole> roles) {
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        LOGGER.info("Send record {} (origin {}) to void", desc.getRecordUid(), desc.getOrigin());
        return null;
    }

}
