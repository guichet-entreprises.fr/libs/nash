/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.converter.BooleanConverter;
import fr.ge.common.nash.engine.converter.IConverter;
import fr.ge.common.nash.engine.converter.ObjectConverter;

/**
 *
 * @author Christian Cougourdan
 */
public class ScriptValueConversionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptValueConversionService.class);

    /** The Constant CONVERTERS. */
    private static final List<IConverter> CONVERTERS;

    static {
        final List<IConverter> lst = new ArrayList<>();
        lst.add(new BooleanConverter());
        CONVERTERS = Collections.unmodifiableList(lst);
    }

    /** The convertersCache. */
    private static final Map<Class<?>, IConverter> convertersCache = new HashMap<>();

    /**
     * Get value converter.
     *
     * @param srcClass
     *            the src class
     * @param dstClass
     *            the dst class
     * @return the converter
     */
    private static IConverter converter(final Class<?> srcClass, final Class<?> dstClass) {
        IConverter converter = convertersCache.get(dstClass);
        if (null == converter) {
            converter = CONVERTERS.stream().filter(c -> c.accept(srcClass, dstClass)).findFirst().orElse(new ObjectConverter());
            convertersCache.put(dstClass, converter);
        }
        return converter;
    }

    /**
     * Convert specified value to expected class instance.
     *
     * @param value
     *            value to convert
     * @param expectedClass
     *            Expected class
     * @return converted value
     */
    public static <R> R convert(final Object value, final Class<R> expectedClass) {
        return converter(null == value ? Object.class : value.getClass(), expectedClass).convert(value, expectedClass);
    }

    /**
     * Check if specified value is a javascript array.
     *
     * @param value
     *            value to test
     * @return true if value is a javascript array.
     */
    public static boolean isArray(final Object value) {
        try {
            final Class<?> cls = Class.forName("jdk.nashorn.api.scripting.ScriptObjectMirror");
            if (cls.isInstance(value)) {
                final Object flag = cls.getMethod("isArray").invoke(value);
                return null != flag && flag.equals(true);
            }
        } catch (final ClassNotFoundException e) {
            LOGGER.warn("Nashorn classe [ScriptObjectMirror] not present");
        } catch (NoSuchMethodException | SecurityException e) {
            LOGGER.warn("Nashorn classe [ScriptObjectMirror] has no method [isArray]");
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            LOGGER.warn("[ScriptObjectMirror.isArray(...)] invocation error");
        }

        return false;
    }

}
