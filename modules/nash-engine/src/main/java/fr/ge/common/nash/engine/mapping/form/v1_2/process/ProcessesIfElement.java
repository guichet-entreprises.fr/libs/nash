/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * The processes "if" element.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "if")
@XmlAccessorType(XmlAccessType.NONE)
public class ProcessesIfElement implements IXml, IProcess {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The test. */
    @XmlAttribute(required = true)
    private String test;

    /** The input. */
    @XmlAttribute
    private String input;

    /** The process. */
    @XmlElements({ //
            @XmlElement(name = "if", type = ProcessesIfElement.class), //
            @XmlElement(name = "process", type = ProcessElement.class) //
    })
    private List<IProcess> processes;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IProcess> getChildren() {
        return this.processes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCondition() {
        return this.test;
    }

    /**
     * Getter on attribute {@link #test}.
     *
     * @return String test
     */
    public String getTest() {
        return this.test;
    }

    /**
     * Setter on attribute {@link #test}.
     *
     * @param test
     *            the new value of attribute test
     */
    public void setTest(final String test) {
        this.test = test;
    }

    /**
     * Getter on attribute {@link #input}.
     *
     * @return String input
     */
    @Override
    public String getInput() {
        return this.input;
    }

    /**
     * Setter on attribute {@link #input}.
     *
     * @param input
     *            the new value of attribute input
     */
    public void setInput(final String input) {
        this.input = input;
    }

    /**
     * Gets the processes.
     *
     * @return the processes
     */
    public List<IProcess> getProcesses() {
        return this.processes;
    }

    /**
     * Sets the processes.
     *
     * @param processes
     *            the processes to set
     */
    public void setProcesses(final List<IProcess> processes) {
        this.processes = Optional.ofNullable(processes).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

}
