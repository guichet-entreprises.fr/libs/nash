/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Optional;
import java.util.Properties;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.ge.common.nash.core.exception.TechnicalException;

/**
 * The Class LocalEntityResolver.
 *
 * @author Christian Cougourdan
 */
public class LocalEntityResolver implements EntityResolver {

    /** The local schemas. */
    private Properties localSchemas;

    /**
     * Instantiates a new local entity resolver.
     */
    public LocalEntityResolver() {
        this.init("/META-INF/nash.schemas");
    }

    /**
     * Inits the.
     *
     * @param pathname
     *            the pathname
     */
    public final void init(final String pathname) {
        final Properties props = new Properties();

        try {
            final InputStream schemaResourceAsStream = this.getClass().getResourceAsStream(pathname);
            if (null == schemaResourceAsStream) {
                throw new TechnicalException(String.format("Try loading schemas from \"%s\" : resource not found", pathname));
            }
            props.load(schemaResourceAsStream);
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Error while loading schemas from \"%s\"", pathname), ex);
        }

        this.localSchemas = props;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputSource resolveEntity(final String publicId, final String systemId) throws SAXException, IOException {
        final URL url = Optional.ofNullable(this.localSchemas.getProperty(systemId)).map(p -> this.getClass().getResource('/' + p)).orElse(new URL(systemId));
        if ("file".equals(url.getProtocol()) || "jar".equals(url.getProtocol())) {
            return new InputSource(url.toString());
        } else {
            throw new SAXException(String.format("Unknown schema URL : \"%s\"", systemId));
        }
    }

}
