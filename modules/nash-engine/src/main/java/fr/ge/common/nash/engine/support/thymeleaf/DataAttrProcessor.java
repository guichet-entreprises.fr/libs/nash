/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.support.thymeleaf;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.thymeleaf.context.IEngineContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateInputException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.processor.AbstractStandardFragmentInsertionTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;

/**
 * The Class DataAttrProcessor.
 *
 * @author Christian Cougourdan
 */
public class DataAttrProcessor extends AbstractStandardFragmentInsertionTagProcessor {

    /** The Constant ELEMENT_NAME. */
    public static final String ATTR_NAME = "data";

    /**
     * Default constructor.
     *
     * @param dialectPrefix
     *            dialect prefix
     */
    public DataAttrProcessor(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, ATTR_NAME, 0, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag, final AttributeName attributeName, final String attributeValue,
            final IElementTagStructureHandler structureHandler) {

        final Map<String, Object> localVariables = this.localVariables(context, attributeValue);
        final String templateName = this.fragment(localVariables);

        ((IEngineContext) context).setVariables(localVariables);

        super.doProcess(context, tag, attributeName, templateName, structureHandler);
    }

    /**
     * {@inheritDoc}
     */
    protected Map<String, Object> localVariables(final ITemplateContext context, final String attributeValue) {
        // handle exceptions
        final Object obj = FragmentUtil.parseValue(context, attributeValue);

        Optional.ofNullable(obj).orElseThrow(() -> new TemplateInputException("Form data element not defined"));
        Optional.ofNullable(obj).filter(IElement.class::isInstance).orElseThrow(() -> new TemplateInputException("Bad type of form data element"));

        final IElement<?> element = (IElement<?>) obj;
        final ElementPath path = Optional.ofNullable(element.getPath()) //
                .map(ElementPath::create) //
                .orElseThrow(() -> new TemplateInputException("Form data element path is invalid"));

        final Map<String, Object> model = new HashMap<>();

        // convert the the raw context information
        // final FormSpecificationData spec =
        // Optional.ofNullable(context.getVariable(Constants.MODEL_SPEC_DATA)) //
        // .filter(FormSpecificationData.class::isInstance) //
        // .map(FormSpecificationData.class::cast) //
        // .orElse(null);

        // final boolean isFragment =
        // Optional.ofNullable(context.getVariable(Constants.MODEL_FRAGMENT)) //
        // .filter(Boolean.class::isInstance) //
        // .map(Boolean.class::cast) //
        // .orElse(false)
        ;

        // final String contextTarget =
        // Optional.ofNullable(context.getVariable(Constants.MODEL_TARGET)) //
        // .map(Object::toString) //
        // .orElse(StringUtils.EMPTY);

        // is it a fragment context and is the current element the root ?
        // final boolean isFragmentRoot = isFragment &&
        // element.getPath().replaceAll("\\[[^\\]]+\\]",
        // "[]").equals(contextTarget.replaceAll("\\[[^\\]]+\\]", "[]"));
        // model.put(Constants.MODEL_FRAGMENT, isFragment);
        // model.put(Constants.MODEL_FRAGMENT_ROOT, isFragmentRoot);

        model.put(Constants.MODEL_DATA, obj);
        model.put(Constants.MODEL_DATA_PATH, path);

        if (element instanceof DataElement) {
            model.putAll(this.dataElementLocalVariables(context, (FormSpecificationData) context.getVariable(Constants.MODEL_STEP), (DataElement) element));
        } else {
            model.putAll(this.groupElementLocalVariables(context, (FormSpecificationData) context.getVariable(Constants.MODEL_STEP), (GroupElement) element));
        }

        return model;
    }

    private Map<String, Object> dataElementLocalVariables(final ITemplateContext context, final FormSpecificationData spec, final DataElement element) {
        final Map<String, Object> model = new HashMap<>();

        final String typeAsString = element.getType();
        if (null != typeAsString) {
            final IValueAdapter<?> type = ValueAdapterFactory.type(spec, element);
            model.put(Constants.MODEL_DATA_TYPE, type);
        }

        return model;
    }

    private Map<String, Object> groupElementLocalVariables(final ITemplateContext context, final FormSpecificationData spec, final GroupElement element) {
        final Map<String, Object> model = new HashMap<>();

        model.put("children", FragmentUtil.getFilteredChildren(element));

        return model;
    }

    /**
     * Template.
     *
     * @param localVariables
     *            the local variables
     * @return the string
     */
    protected String fragment(final Map<String, Object> localVariables) {
        String templateName = null;

        final Object obj = localVariables.get(Constants.MODEL_DATA);

        if (null == obj) {
            throw new TechnicalException("no data presents");
        } else if (obj instanceof IElement) {
            final IElement<?> elm = (IElement<?>) obj;
            if (elm.isRepeatable()) {
                templateName = "many";
            } else {
                templateName = "one";
            }

            final IValueAdapter<?> dataType = (IValueAdapter<?>) localVariables.get(Constants.MODEL_DATA_TYPE);

            localVariables.put(Constants.MODEL_TEMPLATE_NAME, FragmentUtil.getDataTemplate(obj, dataType));
        } else {
            throw new TechnicalException("unknown element of type " + obj.getClass().getName());
        }

        return templateName;
    }

}
