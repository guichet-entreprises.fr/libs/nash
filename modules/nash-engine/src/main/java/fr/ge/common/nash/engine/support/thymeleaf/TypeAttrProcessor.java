package fr.ge.common.nash.engine.support.thymeleaf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.IEngineContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.Fragment;
import org.thymeleaf.standard.expression.FragmentExpression;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.NoOpToken;
import org.thymeleaf.standard.expression.StandardExpressionExecutionContext;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;

/**
 *
 * @author Christian Cougourdan
 */
public class TypeAttrProcessor extends AbstractAttributeTagProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TypeAttrProcessor.class);

    private static final String ATTR_NAME = "type";

    private static final int PRECEDENCE = 1000;

    public TypeAttrProcessor(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, null, false, ATTR_NAME, true, PRECEDENCE, true);
    }

    @Override
    protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag, final AttributeName attributeName, final String attributeValue,
            final IElementTagStructureHandler structureHandler) {

        final IEngineConfiguration configuration = context.getConfiguration();
        final IStandardExpressionParser expressionParser = StandardExpressions.getExpressionParser(configuration);
        final IStandardExpression expression = expressionParser.parseExpression(context, attributeValue);

        final Object expressionResult;
        if (null == expression) {
            return;
        } else {
            if (expression instanceof FragmentExpression) {
                LOGGER.warn("Unable to parse fragment expression : {}", expression);
                return;
            } else {
                expressionResult = expression.execute(context, StandardExpressionExecutionContext.RESTRICTED);
            }
        }

        if (expressionResult == NoOpToken.VALUE) {
            return;
        }

        if (expressionResult != null) {
            if (expressionResult instanceof Fragment) {
                LOGGER.warn("Unable to process fragment : {}", expression);
            } else {
                ((IEngineContext) context).setVariable(Constants.MODEL_DATA_TYPE, expressionResult);
            }
        }
    }

}
