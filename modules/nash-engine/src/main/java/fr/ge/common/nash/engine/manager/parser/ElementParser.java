/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;

/**
 * Handle form specification parsing.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public class ElementParser<T> {

    /** The actions. */
    private final Map<Class<? extends IElement<?>>, IElementAction<T>> actions = new HashMap<>();

    /**
     * Register.
     *
     * @param key
     *            the key
     * @param fn
     *            the fn
     */
    public ElementParser<T> register(final Class<? extends IElement<?>> key, final IElementAction<T> fn) {
        this.actions.put(key, fn);
        return this;
    }

    /**
     * Parses the.
     *
     * @param model
     *            the model
     * @param formSpecificationData
     *            the form specification data
     */
    public void parse(final T model, final FormSpecificationData formSpecificationData) {
        if (null != formSpecificationData.getGroups()) {
            for (final GroupElement groupElement : formSpecificationData.getGroups()) {
                this.parseInternal(model, groupElement, formSpecificationData);
            }
        }
    }

    /**
     * Creates a new ElementParser of String using specified DataElement callback
     * function. The GroupElement callback function just return current element
     * path.
     *
     * @param dataElementFunction
     *            the data element callback function
     * @return the element parser
     */
    public static ElementParser<String> create(final IElementAction<String> dataElementFunction) {
        return create((path, elm, spec) -> path + elm.getId() + '.', dataElementFunction);
    }

    /**
     * Creates a new ElementParser of String using specified GroupElement and
     * DataElement callback function.
     *
     * @param groupElementFunction
     *            the data element callback function
     * @param dataElementFunction
     *            the data element callback function
     * @return the element parser
     */
    public static ElementParser<String> create(final IElementAction<String> groupElementFunction, final IElementAction<String> dataElementFunction) {
        final ElementParser<String> parser = new ElementParser<>();

        parser.register(GroupElement.class, groupElementFunction);
        parser.register(DataElement.class, dataElementFunction);

        return parser;
    }

    /**
     * Parses the.
     *
     * @param model
     *            the model
     * @param element
     *            the element
     */
    public void parse(final T model, final IElement<?> element) {
        this.parse(model, element, null);
    }

    /**
     * Parses the.
     *
     * @param model
     *            the model
     * @param element
     *            the element
     * @param spec
     *            spec
     */
    public void parse(final T model, final IElement<?> element, final FormSpecificationData spec) {
        if (null != element && null != element.getData()) {
            element.getData().forEach(elm -> this.parseInternal(model, elm, spec));
        }
    }

    /**
     * Parses the internal.
     *
     * @param model
     *            the model
     * @param element
     *            the element
     * @param formSpecificationData
     *            form specification data
     */
    private void parseInternal(final T model, final IElement<?> element, final FormSpecificationData formSpecificationData) {
        final T newModel = Optional.ofNullable(this.actions.get(element.getClass())).map(action -> action.apply(model, element, formSpecificationData)).orElse(model);

        if (null != element.getData()) {
            for (final IElement<?> data : element.getData()) {
                this.parseInternal(newModel, data, formSpecificationData);
            }
        }
    }

}
