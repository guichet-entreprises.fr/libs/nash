/**
 *
 */
package fr.ge.common.nash.engine.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationHistory;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationReferential;

/**
 * Class StylesheetUtil.
 *
 * @author bsadil
 */
public final class StylesheetUtil {

    /** La constante STYLESHEET_BY_TYPES. */
    private static final Map<Class<?>, String> STYLESHEET_BY_TYPES;

    static {
        final Map<Class<?>, String> m = new HashMap<>();

        m.put(FormSpecificationDescription.class, "description.xslt");
        m.put(FormSpecificationData.class, "data.css");
        m.put(FormSpecificationProcess.class, "process.xslt");
        m.put(FormSpecificationReferential.class, "referential.css");
        m.put(FormSpecificationHistory.class, "history.css");

        STYLESHEET_BY_TYPES = Collections.unmodifiableMap(m);
    }

    /**
     * Instantie un nouveau stylesheet util.
     */
    private StylesheetUtil() {
        // Nothing to do
    }

    /**
     * Resolve.
     *
     * @param resourcePath
     *            resource path
     * @param clazz
     *            clazz
     * @return string
     */
    public static String resolve(final String resourcePath, final Class<?> clazz) {
        final String stylesheet = STYLESHEET_BY_TYPES.get(clazz);

        if (null == stylesheet) {
            return null;
        }

        final StringBuilder builder = new StringBuilder();
        final String[] exploded = resourcePath.split("/");
        for (int i = 1; i < exploded.length; i++) {
            builder.append("../");
        }

        return builder.append("css/").append(stylesheet).toString();
    }

    /**
     * Stylesheets.
     *
     * @return the list
     */
    public static List<String> stylesheets() {
        return Stream.concat( //
                STYLESHEET_BY_TYPES.values().stream(), //
                Stream.of("common.css", "types.css", "common.xslt") //
        ).map(path -> "css/" + path).collect(Collectors.toList());
    }

}
