/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge;

import java.util.Map;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceRequest;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.ResourceUtil;

/**
 * The Class ServiceProcessorBridge.
 *
 * @author Christian Cougourdan
 */
public class ServiceProcessorBridge extends AbstractBridge {

    /**
     * Instantiates a new service processor bridge.
     *
     * @param context
     *            the context
     */
    public ServiceProcessorBridge(final EngineContext<?> context) {
        super(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "service";
    }

    /**
     * Request.
     *
     * @param url
     *            the url
     * @param params
     *            the params
     * @return the service request
     */
    public ServiceRequest request(final String url, final Object... params) {
        final Configuration cfg = this.getConfiguration();
        final Map<String, String> cfgAsMap = cfg.asMap();
        final String processedUrl = ResourceUtil.buildUrl(url, cfgAsMap, params);
        return new ServiceRequest(processedUrl);
    }

}
