/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * The Class FormSpecificationProvider.
 *
 * @author Christian Cougourdan
 */
public class ZipProvider implements IProvider {

    /** The as bytes. */
    private byte[] resourceAsBytes;

    private final Map<String, byte[]> updatedFiles = new HashMap<>();

    private final Map<String, FileEntry> cache = new HashMap<>();

    /** Regular expression to find file into zip. */
    private static final Map<String, String> REGEX_FILES;

    static {
        final Map<String, String> map = new LinkedHashMap<>();
        map.put("\\/(\\*{2})", "\\/(.*)");
        map.put("\\/(\\*{1})", "\\/([^\\/][a-zA-Z0-9\\-\\.]+)?");
        map.put("(\\*{1}$)", "([^\\/][a-zA-Z0-9\\-\\.]+)?");

        REGEX_FILES = Collections.unmodifiableMap(map);
    }

    /**
     * Initialize provider with specification as zip compressed file.
     *
     * @param zipContentAsBytes
     *            zip data as bytes
     */
    public ZipProvider(final byte[] zipContentAsBytes) {
        this.resourceAsBytes = zipContentAsBytes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FileEntry load(final String name) {
        if (null == name) {
            return null;
        } else {
            final String relativePath = this.getRelativePath(name);
            if (this.updatedFiles.containsKey(relativePath)) {
                return new FileEntry(relativePath).asBytes(this.updatedFiles.get(relativePath));
            } else if (this.cache.containsKey(relativePath)) {
                return this.cache.get(relativePath);
            } else {
                final FileEntry entry = ZipUtil.entry(relativePath, this.resourceAsBytes);
                this.cache.put(relativePath, entry);
                return entry;
            }
        }
    }

    /**
     * Save.
     *
     * @param name
     *            the name
     * @param mimetype
     *            the mimetype
     * @param content
     *            the content
     */
    @Override
    public void save(final String name, final String mimetype, final byte[] content) {
        final String relativePath = this.getRelativePath(name);
        this.updatedFiles.put(relativePath, content);
        this.cache.remove(relativePath);
    }

    @Override
    public boolean remove(final String name) {
        this.save(name, null, null);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] asBytes() {
        this.resourceAsBytes = ZipUtil.entries(this.updatedFiles, this.resourceAsBytes);
        this.updatedFiles.clear();

        return this.resourceAsBytes;
    }

    /**
     * Gets the relative path.
     *
     * @param name
     *            the name
     * @return the relative path
     */
    private String getRelativePath(final String name) {
        if (name != null && name.startsWith("/")) {
            return name.substring(1);
        } else {
            return name;
        }
    }

    @Override
    public List<String> resources(final String input) {
        if (StringUtils.isEmpty(input)) {
            return new ArrayList<>();
        }

        final String pattern = input.indexOf("*") < 0 ? input + "/**/*.*" : input;

//        final String regexResources = REGEX_FILES.keySet().stream().reduce(input, (acc, e) -> acc.replaceAll(e, REGEX_FILES.get(e)));
//        final Pattern pattern = Pattern.compile(regexResources, Pattern.CASE_INSENSITIVE);
        return Optional.of(ZipUtil.entries(this.asBytes())) //
                .orElse(new ArrayList<>()) //
                .stream() //
                .filter(ResourceUtil.filterAsPredicate(pattern)) //
                .collect(Collectors.toList());
    }
}
