/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import fr.ge.common.nash.engine.mapping.IFormSpecificationResource;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.AbstractNamedElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DefaultElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.types.TypeElement;

/**
 * The Class FormSpecificationData.
 *
 * @author atuffrea
 */
@XmlRootElement(name = "form")
@XmlAccessorType(XmlAccessType.NONE)
public class FormSpecificationData extends AbstractNamedElement implements IFormSpecificationResource {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3L;

    /** The resource name. */
    @XmlTransient
    private String resourceName;

    /** The def. */
    @XmlElement(name = "default")
    private DefaultElement def;

    /** The group. */
    @XmlElement(name = "group")
    private List<GroupElement> groups;

    /** The types. */
    @XmlElement(name = "type")
    private List<TypeElement> types;

    /**
     * Default constructor.
     */
    public FormSpecificationData() {
        super();
    }

    /**
     * Default constructor specifying corresponding resource name.
     *
     * @param resourceName
     *            resource name
     */
    public FormSpecificationData(final String resourceName) {
        super();
        this.resourceName = resourceName;
    }

    /**
     * Gets the resource name.
     *
     * @return the resourceName
     */
    @Override
    public String getResourceName() {
        return this.resourceName;
    }

    /**
     * Sets the resource name.
     *
     * @param resourceName
     *            the resourceName to set
     */
    public void setResourceName(final String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     * Gets the default.
     *
     * @return the def
     */
    public DefaultElement getDefault() {
        return this.def;
    }

    /**
     * Sets the default.
     *
     * @param def
     *            the def to set
     */
    public void setDefault(final DefaultElement def) {
        this.def = def;
    }

    /**
     * Gets the group.
     *
     * @return the group
     */
    public List<GroupElement> getGroups() {
        return this.groups;
    }

    /**
     * Sets the group.
     *
     * @param groups
     *            the groups to set
     */
    public void setGroups(final List<GroupElement> groups) {
        if (null == groups) {
            this.groups = null;
        } else {
            this.groups = new ArrayList<>(groups);
        }
    }

    /**
     * Getter on attribute {@link #types}.
     *
     * @return the types
     */
    public List<TypeElement> getTypes() {
        return this.types;
    }

    /**
     * Setter on attribute {@link #types}.
     *
     * @param types
     *            the types to set
     */
    public void setTypes(final List<TypeElement> types) {
        this.types = Optional.ofNullable(types).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return "1.2";
    }

    /**
     * Sets the version.
     *
     * @param value
     *            the new version
     */
    public void setVersion(final String value) {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format( //
                "{ %s, \"default\": %s, \"groups\": %s }", //
                super.toString(), //
                this.def, //
                Optional.ofNullable(this.groups).map(l -> l.stream().map(GroupElement::toString).collect(Collectors.joining(","))).map(s -> '[' + s + ']').orElse("null") //
        );
    }

}
