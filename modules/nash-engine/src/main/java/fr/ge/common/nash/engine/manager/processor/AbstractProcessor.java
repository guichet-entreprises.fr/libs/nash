/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import java.util.Map;

import fr.ge.common.nash.engine.manager.processor.bridge.IBridge;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.NashScriptEngine;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.utils.CoreUtil;

/**
 * A processor to execute processes : preprocess and postprocess items.
 *
 * @author jpauchet
 * @param <T>
 *            the type of the output of the processor
 * @param <R>
 *            the generic type
 */
public abstract class AbstractProcessor<T, R extends AbstractProcessor<T, R>> {

    /** The Constant PATTERN_SCRIPT. */
    private static final String PATTERN_SCRIPT = "(function () { %s })(); ";

    /** The context. */
    private EngineContext<?> context;

    /**
     * Gets the processor specific bridge.
     *
     * @return the bridge
     */
    protected abstract IBridge getBridge();

    /**
     * Instantiates a new abstract processor.
     */
    public AbstractProcessor() {
    }

    /**
     * Initializes the processor and defines a specific bridge if necessary.
     *
     * @return the r
     */
    protected R init() {
        return CoreUtil.cast(this);
    }

    /**
     * Execute.
     *
     * @param model
     *            the model
     * @return the i process result
     */
    public IProcessResult<T> execute(final Map<String, Object> model) {
        return this.execute(this.context.buildExpressionContext(model));
    }

    public final IProcessResult<T> execute(final ScriptExecutionContext ctx) {
        this.init();

        if (this.validateConditions()) {
            final String script = this.getScript();
            final Object rawValue = this.execute(script, ctx);

            final Object value = this.afterExecute(rawValue);

            return this.bind(value);
        } else {
            return null;
        }
    }

    public String getInputResourceName() {
        return null;
    }

    /**
     * Gets the script.
     *
     * @return the script
     */
    protected String getScript() {
        return String.format(PATTERN_SCRIPT, this.getProcess().getValue());
    }

    /**
     * Validate conditions.
     *
     * @return true, if successful
     */
    protected boolean validateConditions() {
        // Nothing to do yet
        return true;
    }

    /**
     * Execute.
     *
     * @param script
     *            the script
     * @param model
     *            the model
     * @return the object
     */
    protected Object execute(final String script, final ScriptExecutionContext ctx) {
        final IBridge bridge = this.getBridge();
        if (null != bridge) {
            ctx.addBridge(bridge);
            ctx.addExtra("_" + bridge.getName(), bridge);
        }

        final NashScriptEngine scriptEngine = CoreUtil.time("Retrieve script engine", () -> this.context.getRecord().getScriptEngine());
        CoreUtil.time("Prepare execution environment", () -> scriptEngine.eval("root.init(this, nash)", ctx));
        return CoreUtil.time("Execute process script", () -> scriptEngine.eval(script, ctx));
    }

    /**
     * After execute.
     *
     * @param value
     *            the value
     * @return the object
     */
    protected Object afterExecute(final Object value) {
        // Nothing to do yet
        return value;
    }

    /**
     * Bind.
     *
     * @param value
     *            the value
     * @return the i process result
     */
    protected abstract IProcessResult<T> bind(Object value);

    /**
     * Sets the context.
     *
     * @param context
     *            the context
     * @return the r
     */
    public R setContext(final EngineContext<?> context) {
        this.context = context;
        return CoreUtil.cast(this);
    }

    /**
     * Gets the context.
     *
     * @return the context
     */
    public EngineContext<?> getContext() {
        return this.context;
    }

    /**
     * Gets the process.
     *
     * @return the process
     */
    public ProcessElement getProcess() {
        return CoreUtil.cast(this.context.getElement());
    }

}
