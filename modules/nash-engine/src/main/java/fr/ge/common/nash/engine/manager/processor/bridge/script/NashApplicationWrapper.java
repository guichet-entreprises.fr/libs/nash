/**
 * 
 */
package fr.ge.common.nash.engine.manager.processor.bridge.script;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceRequest;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceResponse;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class NashApplicationWrapper extends ApplicationWrapper {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(NashApplicationWrapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public IProvider load(final String baseUrl, final String code) {
        final ServiceResponse response = new ServiceRequest(StringUtils.join(baseUrl, String.format("/v1/Record/code/%s/file", code))).get();
        if (Status.OK.getStatusCode() == response.getStatus()) {
            return new ZipProvider(response.asBytes());
        }

        throw new TechnicalException(String.format("Load record %s unsuccessfully", code));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServiceResponse uploadTo(final String baseUrl, final IProvider provider, final List<String> acls) {
        final ServiceRequest request = new ServiceRequest(StringUtils.join(baseUrl, "/v1/Record")) //
                .dataType("application/zip") //
                .accept("json");

        final SpecificationLoader loader = SpecificationLoader.create(provider);

        // -->Reset record identifier to generate a new one
        loader.description().setRecordUid(null);
        loader.flush();
        // <--

        final String[] roles = acls.toArray(new String[acls.size()]);

        request //
                .param("author", loader.description().getAuthor()) //
                .param("roles", roles) //
        ;

        final ServiceResponse response = request.post(provider.asBytes());

        if (Status.CREATED.getStatusCode() == response.getStatus()) {
            LOGGER.info("Upload record to nash successfully");
            return response;
        }

        throw new TechnicalException("Upload record unsuccessfully");
    }
}
