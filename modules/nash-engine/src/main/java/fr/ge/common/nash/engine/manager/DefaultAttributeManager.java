/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import java.util.Optional;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DefaultElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;

/**
 * On group element, attributes that are processed are :
 * <ul>
 * <li>label</li>
 * <li>description</li>
 * </ul>
 *
 * On data element, attributes that are processed are :
 * <ul>
 * <li>mandatory</li>
 * <li>type</li>
 * </ul>
 * .
 *
 * @author Christian Cougourdan
 */
public class DefaultAttributeManager {

    /** The form specification. */
    private final FormSpecificationData _formSpecification;

    /** The default element. */
    private final DefaultElement _defaultElement;

    /**
     * Default constructor. Initialize form default attributes.
     *
     * @param defaultElement
     *            form default attributes
     */
    public DefaultAttributeManager(final DefaultElement defaultElement) {
        this._formSpecification = null;
        this._defaultElement = defaultElement;
    }

    /**
     * Default constructor. Initialize form specification.
     *
     * @param formSpecification
     *            form specification
     */
    public DefaultAttributeManager(final FormSpecificationData formSpecification) {
        this._formSpecification = formSpecification;
        this._defaultElement = Optional.ofNullable(formSpecification).map(s -> s.getDefault()).orElse(null);
    }

    /**
     * Retrieve default form element from specified one or form specification on
     * instantiation.
     *
     * @return default form element
     */
    public DefaultElement defaultElement() {
        return Optional.ofNullable(this._defaultElement).orElse(Optional.ofNullable(this._formSpecification).map(s -> s.getDefault()).orElse(null));
    }

    /**
     * Register default attributes if not yet specified on specified data element.
     *
     * @param element
     *            the element
     * @return the i element
     */
    public IElement<?> register(final IElement<?> element) {
        if (element instanceof DataElement) {
            final DataElement dataElement = (DataElement) element;
            final DataElement def = Optional.ofNullable(this.defaultElement()).map(d -> d.getData()).orElse(null);

            if (null == dataElement.getMandatory()) {
                dataElement.setMandatory(null == def || null == def.getMandatory() ? Boolean.FALSE : def.getMandatory());
            }

            if (null == dataElement.getType()) {
                dataElement.setType(null == def || null == def.getType() ? null : def.getType());
            }

            final String id = dataElement.getId();
            dataElement.setId(null == id ? null : id.replaceAll("\\[\\]", "\\[0\\]"));
        } else if (element instanceof GroupElement) {
            return this.register((GroupElement) element);
        }

        return element;
    }

    /**
     * Register recursively default attributes if not yet specified on specified
     * group element.
     *
     * @param groupElement
     *            the group element
     * @return the group element
     */
    public GroupElement register(final GroupElement groupElement) {
        final GroupElement def = Optional.ofNullable(this.defaultElement()).map(d -> d.getGroup()).orElse(null);

        if (null != def) {
            if (null == groupElement.getDescription()) {
                groupElement.setDescription(def.getDescription());
            }

            if (null == groupElement.getLabel()) {
                groupElement.setLabel(def.getLabel());
            }
        }

        if (null != groupElement.getData()) {
            groupElement.getData().forEach(this::register);
        }

        final String id = groupElement.getId();
        groupElement.setId(null == id ? null : id.replaceAll("\\[\\]", "\\[0\\]"));

        return groupElement;
    }

    /**
     * Register.
     *
     * @param idx
     *            the idx
     * @return the group element
     */
    public GroupElement register(final int idx) {
        if (null == this._formSpecification) {
            throw new IllegalStateException("Unable to register default attributes : no form specification registered");
        } else if (this._formSpecification.getGroups() != null && (idx < 0 || idx >= this._formSpecification.getGroups().size())) {
            throw new IllegalStateException("Unable to register default attributes : group index [" + idx + "] is out of bound");
        }

        if (null == this._formSpecification.getGroups()) {
            final GroupElement group = new GroupElement();
            group.setId("emptyGroup");
            return group;
        } else {
            return this.register(this._formSpecification.getGroups().get(idx));
        }
    }

}
