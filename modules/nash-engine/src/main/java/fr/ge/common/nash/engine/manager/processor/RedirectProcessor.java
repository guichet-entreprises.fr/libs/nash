/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor;

import javax.script.Bindings;

import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;

import fr.ge.common.nash.engine.manager.processor.bridge.IBridge;
import fr.ge.common.nash.engine.manager.processor.result.RedirectProcessBean;
import fr.ge.common.nash.engine.manager.processor.result.RedirectProcessResult;

/**
 * The Class RedirectProcessor.
 *
 * @author Christian Cougourdan
 */
public class RedirectProcessor extends AbstractProcessor<RedirectProcessBean, RedirectProcessor> {

    /** The Constant PATTERN_SCRIPT. */
    private static final String PATTERN_SCRIPT = "_bridge = nash.instance; (function () { %s })(); ";

    /** The dozer. */
    private final Mapper dozer = DozerBeanMapperBuilder.create().build();

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScript() {
        return String.format(PATTERN_SCRIPT, this.getProcess().getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IBridge getBridge() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IProcessResult<RedirectProcessBean> bind(final Object value) {
        RedirectProcessBean bean = null;
        if (null != value) {
            if (value instanceof Bindings) {
                bean = this.dozer.map((Bindings) value, RedirectProcessBean.class);
            } else if (value instanceof RedirectProcessBean) {
                bean = (RedirectProcessBean) value;
            }
        }
        return new RedirectProcessResult(bean);
    }

}
