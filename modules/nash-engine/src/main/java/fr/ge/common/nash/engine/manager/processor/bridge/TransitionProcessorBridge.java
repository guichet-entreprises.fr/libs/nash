/**
 *
 */
package fr.ge.common.nash.engine.manager.processor.bridge;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.script.Bindings;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapperBuilder;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.PatternProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * Transition processor bridge.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class TransitionProcessorBridge extends AbstractBridge {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TransitionProcessorBridge.class);

    /** Bridge name. */
    private static final String BRIDGE_NAME = "transition";

    /** The dozer. */
    private final Mapper dozer = DozerBeanMapperBuilder.create().build();

    private final TransitionWrapper wrapper;

    /**
     * Instantiates a new processor.
     *
     * @param context
     *            the context
     */
    public TransitionProcessorBridge(final EngineContext<?> context) {
        super(context);

        this.wrapper = new TransitionWrapper(this.getContext(), this.getContext(), null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return BRIDGE_NAME;
    }

    /**
     * Adds a step.
     *
     * @param bindings
     *            the bindings
     * @throws FunctionalException
     *             the functional exception
     */
    public TransitionWrapper createStep(final Bindings bindings) throws FunctionalException {
        StepElement step = null;
        if (null == bindings) {
            step = new StepElement();
        } else {
            step = this.dozer.map(bindings, StepElement.class);
        }

        final String overrideUser = Optional.ofNullable(this.getElement()) //
                .filter(ProcessElement.class::isInstance) //
                .map(ProcessElement.class::cast) //
                .map(ProcessElement::getWithUser) //
                .orElse(null);

        if (StringUtils.isNotEmpty(overrideUser)) {
            step.setUser(overrideUser);
        }

        String stepFolder = getRecord().stepsMgr().addStep(step);

        return new TransitionWrapper( //
                this.getContext(), //
                this.getContext().withProvider(new PatternProvider(this.getBaseProvider(), stepFolder + "/%s")), //
                overrideUser //
        );
    }

    /**
     * Copies a file from the transition to the specification.
     *
     * @param srcFilePath
     *            the source file path
     * @param dstFolderPath
     *            the destination folder path
     */
    public void copy(final String srcFilePath, final String dstFolderPath) {
        this.wrapper.copy(srcFilePath, dstFolderPath);
    }

    /**
     * Creates a process file.
     *
     * @param resourcePath
     *            the resource path
     * @param values
     *            the values
     * @return the form specification process
     */
    public FormSpecificationProcess createProcessFile(final String resourcePath, final Bindings values) {
        final FormSpecificationProcess processes = new FormSpecificationProcess();
        processes.setId((String) values.get("id"));

        final Bindings processList = (Bindings) values.get("process");
        processes.setProcesses(processList.values().stream() //
                .map(elm -> this.createProcess((Bindings) elm)) //
                .collect(Collectors.toList()));

        this.getBaseProvider().save(resourcePath, processes);

        return processes;
    }

    /**
     * Creates a process.
     *
     * @param values
     *            the values
     * @return the process element
     */
    public ProcessElement createProcess(final Bindings values) {
        return this.dozer.map(values, ProcessElement.class);
    }

    public static class TransitionWrapper {

        private final EngineContext<?> baseContext;

        private final EngineContext<?> stepContext;

        private final String withUser;

        private TransitionWrapper(final EngineContext<?> baseContext, final EngineContext<?> stepContext, final String withUser) {
            this.baseContext = baseContext;
            this.stepContext = stepContext;
            this.withUser = withUser;
        }

        public void copy(final String srcFilePath) {
            this.copy(srcFilePath, null);
        }

        public void copy(final String srcFilePath, final String dstFolderPath) {
            final Deque<String> srcPaths = new LinkedList<>();
            srcPaths.addAll(Arrays.asList(srcFilePath.split("[ ]*,[ *]")));

            if (srcPaths.isEmpty()) {
                return;
            }

            final String dstFilePath = this.buildDestinationPath(srcPaths.peek(), dstFolderPath);
            if (srcPaths.size() > 1) {
                /*
                 * Do a merge of all inputs
                 */
                final FormSpecificationData root = srcPaths.stream() //
                        .map(this::load) //
                        .filter(spec -> spec != null) //
                        .reduce(null, this::merge) //
                ;

                this.stepContext.getProvider().save(dstFilePath, root);
            } else {
                /*
                 * Just copy resource
                 */
                final FileEntry entry = this.find(srcPaths.peek());
                if (null != entry) {
                    this.stepContext.getProvider().save(dstFilePath, entry.asBytes());
                    if (StringUtils.isNotEmpty(this.withUser)) {
                        try {
                            final FormSpecificationProcess process = JaxbFactoryImpl.instance().unmarshal(entry.asBytes(), FormSpecificationProcess.class);
                            if (null != process && CollectionUtils.isNotEmpty(process.getProcesses())) {
                                for (final IProcess p : process.getProcesses()) {
                                    if (p instanceof ProcessElement) {
                                        ProcessElement pe = (ProcessElement) p;
                                        if (pe.getType().equals(BRIDGE_NAME)) {
                                            pe.setWithUser(this.withUser);
                                            LOGGER.debug("Update with-user attribute for process '{}'", pe.getId());
                                        }
                                    }
                                }
                                this.stepContext.getProvider().save(dstFilePath, process);
                            }
                        } catch (Exception e) {
                            // -->Do nothing here
                        }
                    }
                }
            }

        }

        private FormSpecificationData load(final String resourcePath) {
            return Optional.of(resourcePath) //
                    .map(this::find) //
                    .map(FileEntry::asBytes) //
                    .map(bytes -> JaxbFactoryImpl.instance().unmarshal(bytes, FormSpecificationData.class)) //
                    .orElse(null);
        }

        private FormSpecificationData merge(final FormSpecificationData spec, final FormSpecificationData toMerge) {
            if (null == spec) {
                return toMerge;
            } else if (null == toMerge) {
                return spec;
            }

            final Map<String, Integer> indexes = new HashMap<>();
            for (int idx = 0; idx < spec.getGroups().size(); idx++) {
                indexes.put(spec.getGroups().get(idx).getId(), idx);
            }

            for (final GroupElement grp : toMerge.getGroups()) {
                final int idx = Optional.ofNullable(grp.getId()) //
                        .map(indexes::get) //
                        .orElse(-1) //
                ;

                final GroupElement dstGrp = Optional.ofNullable(grp.getId()) //
                        .map(indexes::get) //
                        .map(spec.getGroups()::get) //
                        .orElse(null) //
                ;

                if (null == dstGrp) {
                    spec.getGroups().add(grp);
                } else {
                    spec.getGroups().set(idx, (GroupElement) this.merge(dstGrp, grp));
                }
            }
            return spec;
        }

        private IElement<?> merge(final IElement<?> dst, final IElement<?> src) {
            if (src instanceof DataElement || dst instanceof DataElement) {
                return src;
            }

            final List<IElement<?>> children = dst.getData();

            final Map<String, Integer> indexes = new HashMap<>();
            for (int idx = 0; idx < children.size(); idx++) {
                indexes.put(children.get(idx).getId(), idx);
            }

            for (final IElement<?> elm : src.getData()) {
                final int idx = Optional.ofNullable(elm.getId()) //
                        .map(indexes::get) //
                        .orElse(-1) //
                ;

                if (idx < 0) {
                    children.add(elm);
                } else {
                    children.set(idx, this.merge(children.get(idx), elm));
                }
            }

            return dst;
        }

        private FileEntry find(final String srcFilePath) {
            return CoreUtil.coalesce( //
                    () -> this.baseContext.getScriptProvider().load(srcFilePath), //
                    () -> this.stepContext.getBaseProvider().load(srcFilePath), //
                    () -> this.stepContext.getProvider().load(srcFilePath) //
            );
        }

        private String buildDestinationPath(final String resourcePath, final String dstFolderPath) {
            String dstFilePath = Paths.get(resourcePath).toFile().getName();

            if (null != dstFolderPath) {
                if (dstFolderPath.endsWith("/")) {
                    dstFilePath = dstFolderPath + dstFilePath;
                } else {
                    dstFilePath = dstFolderPath;
                }
            }

            return dstFilePath;
        }

    }

}
