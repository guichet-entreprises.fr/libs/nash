/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.support.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.util.ValueAdapterResourceTypeEnum;
import fr.ge.common.support.i18n.MessageReader;

/**
 * Message reader.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public final class NashMessageReader extends MessageReader {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(NashMessageReader.class);

    /**
     * Constructor.
     *
     * @param resourceBundle
     *            the resource bundle
     */
    private NashMessageReader(final ResourceBundle resourceBundle) {
        super(resourceBundle);
    }

    /**
     * Gets the bundle.
     *
     * @param specLoader
     *            the specification loader
     * @param locale
     *            the locale
     * @return the bundle
     */
    private static ResourceBundle getSpecBundle(final SpecificationLoader specLoader, final Locale locale) {
        final Locale bundleLocale = locale == null ? LocaleContextHolder.getLocale() : locale;
        if (bundleLocale == null) {
            LOGGER.debug("No bundle found for specification='{}', locale='null'", specLoader.description().getFormUid());
            return null;
        } else {
            return specLoader.i18n().getBundle(bundleLocale.getLanguage());
        }
    }

    /**
     * Gets the value adapter base name.
     *
     * @param valueAdapter
     *            the value adapter
     * @return the base name
     */
    private static String getValueAdapterBaseName(final IValueAdapter<?> valueAdapter) {
        String baseName = null;
        if (valueAdapter != null) {
            baseName = valueAdapter.getClass().getName().replaceAll("[.]", "/") //
                    + "Resources/" //
                    + ValueAdapterResourceTypeEnum.TRANSLATION.getName();
        }
        return baseName;
    }

    /**
     * Gets the message reader.
     *
     * @param valueAdapter
     *            the value adapter
     * @return the message reader
     */
    public static MessageReader getReader(final IValueAdapter<?> valueAdapter) {
        return getReader(valueAdapter, null);
    }

    /**
     * Gets the message reader.
     *
     * @param valueAdapter
     *            the value adapter
     * @param locale
     *            the locale
     * @return the message reader
     */
    public static MessageReader getReader(final IValueAdapter<?> valueAdapter, final Locale locale) {
        return getReader(getValueAdapterBaseName(valueAdapter), locale);
    }

    /**
     * Gets the message reader.
     *
     * @param specLoader
     *            the specification loader
     * @return the message reader
     */
    public static MessageReader getReader(final SpecificationLoader specLoader) {
        return getReader(specLoader, null);
    }

    /**
     * Gets the message reader.
     *
     * @param specLoader
     *            the specification loader
     * @param locale
     *            the locale
     * @return the message reader
     */
    public static MessageReader getReader(final SpecificationLoader specLoader, final Locale locale) {
        return new NashMessageReader(getSpecBundle(specLoader, locale));
    }

}
