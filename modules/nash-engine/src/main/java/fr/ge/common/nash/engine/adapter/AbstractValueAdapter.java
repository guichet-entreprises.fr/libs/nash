/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.adapter;

import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.FormContentReader;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.provider.IProvider;

/**
 * Value adapter.
 *
 * @author Christian Cougourdan
 *
 * @param <T>
 *            the data type of the value adapter
 */
public abstract class AbstractValueAdapter<T> implements IValueAdapter<T> {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractValueAdapter.class);

    @Override
    public boolean hasOwnUpdate() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getDefaultValue() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getDefaultValue(final DataElement elm) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String externalPath() {
        return "";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T fromString(final String value) {
        throw new TechnicalException("Not implemented");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        throw new TechnicalException("Not implemented");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(final DataElement dataElement) {
        return this.get(null, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(final IProvider provider, final DataElement dataElement) {
        return Optional.ofNullable(dataElement) //
                .map(DataElement::getValue) //
                .map(this::fromValueElement) //
                .orElse(this.getDefaultValue(dataElement));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] getAsBytes(final IProvider provider, final DataElement dataElement) {
        return Optional.ofNullable(dataElement) //
                .map(DataElement::toString) //
                .map(String::getBytes) //
                .orElse(new byte[0]);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#clone()
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T set(final DataElement dataElement, final Object value) {
        return this.set(null, null, dataElement, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T set(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        return this.set(loader, null, dataElement, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T set(final SpecificationLoader loader, final IProvider provider, final DataElement dataElement, final Object value) {
        if (value instanceof ValueElement) {
            dataElement.setValue((ValueElement) value);
        } else {
            dataElement.setValue(this.toValueElement(loader, dataElement, value));
        }
        return this.get(provider, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T set(final SpecificationLoader loader, final DataElement dataElement, final String key, final HttpServletRequest request) {
        final FormContentData data = FormContentReader.read(request).withPath(key);
        try {
            final String dataAsString = data.asString();
            if (dataAsString == null) {
                return this.set(loader, null, dataElement, data);
            } else {
                return this.set(loader, null, dataElement, dataAsString);
            }
        } catch (final TechnicalException ex) {
            LOGGER.debug("Registering value to XML : {}", ex.getMessage());
            LOGGER.trace(StringUtils.EMPTY, ex);
            return this.set(loader, null, dataElement, data);
        }
    }

    /**
     * Extract value from {@link ValueElement}.
     *
     * @param value
     *            the value element
     * @return corresponding typed value
     */
    protected T fromValueElement(final ValueElement value) {
        return Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(String.class::isInstance) //
                .map(String.class::cast) //
                .map(this::fromString) //
                .orElse(null); //
    }

    /**
     * Convert value to {@link ValueElement}.
     *
     * @param loader
     *            the specification loader
     * @param dataElement
     *            the data element
     * @param value
     *            typed value to convert
     * @return corresponding value
     */
    protected ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        String storedValue = null;
        if (value instanceof FormContentData) {
            final Map<String, String[]> contentData = ((FormContentData) value).asMap();
            if (contentData.containsKey("val") && contentData.size() == 1) {
                storedValue = this.toString(((FormContentData) value).withPath("val").asString());
            } else {
                storedValue = this.toString(((FormContentData) value).asString());
            }
        } else if (value instanceof DataBinding) {
            final DataBinding binding = (DataBinding) value;
            if (binding.isMap() && binding.asMap().containsKey("val") && binding.asMap().size() == 1) {
                storedValue = this.toString(binding.withPath("val").asString());
            } else {
                storedValue = this.toString(binding.asString());
            }
        } else {
            storedValue = this.toString(value);
        }
        return null == storedValue ? null : new ValueElement(storedValue);
    }

}
