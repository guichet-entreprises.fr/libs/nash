/**
 *
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.engine.mapping.IXml;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;

/**
 * Record meta data manager.
 *
 * @author Adil Bsibiss
 */
@XmlRootElement(name = "metas")
@XmlAccessorType(XmlAccessType.NONE)
public class FormSpecificationMeta implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The metas. */
    @XmlElement(name = "meta")
    private List<MetaElement> metas = new ArrayList<>();

    /**
     * Getter on attribute {@link #metas}.
     *
     * @return List&lt;MetaElement&gt; events
     */
    public List<MetaElement> getMetas() {
        return this.metas;
    }

    /**
     * Setter on attribute {@link #metas}.
     *
     * @param metas
     *            the new value of attribute metas
     */
    public void setMetas(final List<MetaElement> metas) {
        this.metas = Optional.ofNullable(metas).map(lst -> new ArrayList<>(lst)).orElse(null);
    }

}
