/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge.script;

import java.io.IOException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.bridge.ServiceProcessorBridge;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.EngineContext;

/**
 * @author Christian Cougourdan
 */
public class ServiceWrapper {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceWrapper.class);

    private static final Pattern PATTERN_RESOURCE_URI = Pattern.compile("^(?:([^:]+):)?(.*)$");

    private final EngineContext<?> context;

    private String srcUrl;

    private String dstUrl;

    private ApplicationWrapper sourceWrapper;

    public ServiceWrapper(final EngineContext<?> context) {
        this.context = context;
    }

    public RecordWrapper createRecord(final String resourceCode) {
        final byte[] resourceBytes = this.retrieveRecordModel(resourceCode);

        final IProvider provider = new ZipProvider(resourceBytes);
        final SpecificationLoader loader = this.context.getEngine().loader(provider);
        final FormSpecificationDescription desc = loader.description();
        desc.setAuthor(this.context.getRecord().description().getAuthor());

        final String sourceUid = this.context.getRecord().description().getRecordUid();
        LOGGER.info("Setting sourceUid {} to RecordWrapper", sourceUid);
        final EngineContext<SpecificationLoader> ctx = loader.buildEngineContext().withScriptProvider(this.context.getScriptProvider());
        return new RecordWrapper(ctx).setBaseUrl(Optional.ofNullable(this.dstUrl).orElse(this.srcUrl)).setSourceUid(sourceUid);
    }

    private byte[] retrieveRecordModel(final String resourceCode) {
        final Matcher m = PATTERN_RESOURCE_URI.matcher(resourceCode);
        if (m.matches()) {
            final String resourceType = m.group(1);
            final String resourceName = m.group(2);

            if ("local".equals(resourceType)) {
                return this.retrieveRecordModelFromLocal(resourceName);
            } else if ("ref".equals(resourceType)) {
                return this.retrieveRecordModelFromReference(resourceName);
            } else if (StringUtils.isEmpty(resourceType)) {
                return this.retrieveRecordModeFromCode(resourceName);
            }
        }

        throw new TechnicalException(String.format("Unsupport record reference format : %s", resourceCode));
    }

    /**
     * @param resourceName
     * @return
     */
    private byte[] retrieveRecordModelFromReference(final String resourceReference) {
        try (ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.context)) {
            return bridge //
                    .request(this.srcUrl + "/v1/Specification/ref/{reference}", resourceReference) //
                    .get() //
                    .asBytes();
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to retrieve model [%s] from [%s]", resourceReference, ex.getMessage()));
        }
    }

    /**
     * @param resourceName
     * @return
     */
    private byte[] retrieveRecordModelFromLocal(final String resourceName) {
        final byte[] resourceAsBytes = this.context.getScriptProvider().asBytes(resourceName + ".zip");
        if (ArrayUtils.isEmpty(resourceAsBytes)) {
            throw new TechnicalException(String.format("Unable to retrieve model [%s] from [local]", resourceName));
        }
        return resourceAsBytes;
    }

    /**
     * @param resourceName
     */
    private byte[] retrieveRecordModeFromCode(final String resourceCode) {
        try (ServiceProcessorBridge bridge = new ServiceProcessorBridge(this.context)) {
            return bridge //
                    .request(this.srcUrl + "/v1/Specification/code/{code}/file", resourceCode) //
                    .get() //
                    .asBytes();
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Creating record [%s] from [%s]", ex.getMessage(), this.context.getRecord().description().getRecordUid()));
        }
    }

    public ServiceWrapper from(final String srcUrl) {
        this.srcUrl = srcUrl;
        return this;
    }

    public ServiceWrapper to(final String dstUrl) {
        this.dstUrl = dstUrl;
        return this;
    }

    public ServiceWrapper wrap(final ApplicationWrapper sourceWrapper) {
        this.sourceWrapper = sourceWrapper;
        return this;
    }

    public RecordWrapper load(final String code) {
        if (null == this.sourceWrapper) {
            throw new TechnicalException("Cannot find source wrapper");
        }

        try {
            return new RecordWrapper(this.context.getEngine().loader(this.sourceWrapper.load(this.srcUrl, code)).buildEngineContext());
        } catch (final SecurityException ex) {
            throw new TechnicalException("Compliant constructor not found", ex);
        } catch (final IllegalArgumentException ex) {
            throw new TechnicalException("Error occured while instanciate processor class", ex);
        }

    }

}
