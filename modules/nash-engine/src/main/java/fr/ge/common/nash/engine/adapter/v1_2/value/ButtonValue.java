package fr.ge.common.nash.engine.adapter.v1_2.value;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Christian Cougourdan
 */
public class ButtonValue {

    private String label;

    private String color;

    private String icon;

    private String size;

    private String value;

    /**
     * @return the label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * @param label
     *            the label to set
     * @return self
     */
    public ButtonValue setLabel(final String label) {
        this.label = label;
        return this;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return this.color;
    }

    /**
     * @param color
     *            the color to set
     * @return self
     */
    public ButtonValue setColor(final String color) {
        this.color = color;
        return this;
    }

    /**
     * @return the icon
     */
    public String getIcon() {
        return this.icon;
    }

    /**
     * @param icon
     *            the icon to set
     * @return self
     */
    public ButtonValue setIcon(final String icon) {
        this.icon = icon;
        return this;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return this.size;
    }

    /**
     * @param size
     *            the size to set
     * @return self
     */
    public ButtonValue setSize(final String size) {
        this.size = size;
        return this;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return StringUtils.isNotEmpty(this.value) ? this.value : "no";
    }

    /**
     * @param value
     *            the value to set
     * @return self
     */
    public ButtonValue setValue(final String value) {
        this.value = value;
        return this;
    }

}
