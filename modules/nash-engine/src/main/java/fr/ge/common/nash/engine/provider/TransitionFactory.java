/**
 *
 */
package fr.ge.common.nash.engine.provider;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.Engine;

/**
 * A factory for creating Transition objects.
 *
 * @author bsadil
 */
public final class TransitionFactory {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TransitionFactory.class);

    /** Package info. */

    private static final Map<String, TransitionProvider> TRANSITION_RESOURCES = loadAllTransitions();

    /** File. */
    private static final String FILE = "file";

    /** JAR. */
    private static final String JAR = "jar";

    /** JAR extension. */
    private static final String JAR_EXTENSION = ".jar";

    /** the base path of all Transitions. */
    private static final String BASE_PATH = Engine.class.getPackage().getName().replace('.', '/') + "/transition";

    /**
     * the constructot of TransitionFactory Class.
     */
    public TransitionFactory() {
        super();
        // Nothing to do
    }

    /**
     * Load all transitionProvider.
     *
     * @return Map&lt;taransitionName,TransitionSources&gt;
     */
    public static Map<String, TransitionProvider> loadAllTransitions() {

        LOGGER.info("load all transitions of form-manager");

        final Map<String, TransitionProvider> transitions = new HashMap<>();

        try {
            final Collection<String> candidates = TransitionFactory.getClassesFromPackageJarProtocol( //
                    Engine.class.getPackage().getName().replace('.', '/') + "/transition/", //
                    Thread.currentThread().getContextClassLoader().getResource(Engine.class.getPackage().getName().replace('.', '/') + "/transition/") //
            );

            for (final String candidate : candidates) {
                LOGGER.info("add transition {}  to list of all transition to display", candidate);

                final TransitionProvider transition = new TransitionProvider(TransitionFactory.class, candidate);
                transitions.put(candidate, transition);
            }
        } catch (final IOException e) {
            LOGGER.error("Error when loading all transitions {}", e);
        }

        LOGGER.info("the size of transitions list is {}", transitions.keySet().size());
        return transitions;
    }

    /**
     * Gets the classes of a package, provided a class from which the jar is
     * retrieved, for the jar protocol.
     *
     * @param packageToScan
     *            the prefix to scan with, like "fr.my.prefix.to.scan"
     * @param jarClassUrl
     *            the JAR class URL
     * @return the classes
     * @throws IOException
     *             an IO exception
     */
    private static Collection<String> getClassesFromPackageJarProtocol(final String packageToScan, final URL jarClassUrl) throws IOException {
        Collection<String> packageClasses = new ArrayList<>();

        if (JAR.equals(jarClassUrl.getProtocol())) {
            final JarURLConnection urlConnection = (JarURLConnection) jarClassUrl.openConnection();
            final JarFile jar = urlConnection.getJarFile();
            final Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                final JarEntry entry = entries.nextElement();
                if (entry.isDirectory() && entry.getName().startsWith(packageToScan)) {
                    final String className = entry.getName();
                    final String name = StringUtils.substringBetween(className, packageToScan, "/");
                    if (!packageClasses.contains(name) && null != name) {
                        packageClasses.add(name);
                    }

                }
            }
        } else if (FILE.equals(jarClassUrl.getProtocol())) {
            packageClasses = getClassesFromPackageFileProtocol(packageToScan);
        }
        return packageClasses;
    }

    /**
     * Gets the classes of a package, provided a class from which the jar is
     * retrieved, for the jar protocol.
     *
     * @param packageToScan
     *            the prefix to scan with, like "fr.my.prefix.to.scan"
     * @return the classes
     * @throws MalformedURLException
     *             a malformed URL exception
     */
    private static Collection<String> getClassesFromPackageFileProtocol(final String packageToScan) throws MalformedURLException {
        final Collection<String> packageClasses = new HashSet<>();
        final URL[] classpathEntries = ((URLClassLoader) Thread.currentThread().getContextClassLoader()).getURLs();

        for (final URL classpathEntry : classpathEntries) {
            if (!classpathEntry.getPath().endsWith(JAR_EXTENSION)) {
                final String classpathEntryWithPackage = classpathEntry + packageToScan;
                final File file = new File(new URL(classpathEntryWithPackage).getPath());
                if (file.isDirectory() && file.getAbsolutePath().contains(StringUtils.substringBefore(packageToScan, "/"))) {
                    final File[] files = file.listFiles(f -> f.isDirectory());
                    if (null != files) {
                        Arrays.stream(files) //
                                .map(File::getName) //
                                .filter(elm -> null != elm) //
                                .forEach(packageClasses::add);
                    }
                }
            }
        }
        return packageClasses;
    }

}
