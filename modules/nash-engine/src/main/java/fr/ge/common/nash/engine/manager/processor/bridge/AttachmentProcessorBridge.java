/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.loader.SpecificationProcesses;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.types.TypeElement;
import fr.ge.common.nash.engine.script.EngineContext;

/**
 * The Class AttachmentProcessBridge.
 */
public class AttachmentProcessorBridge extends AbstractBridge {

    /** The pattern. */
    private final Pattern pattern = Pattern.compile("[{]{2}[$]([^}]+)[}]{2}");

    /** The type repo. */
    private final Map<String, TypeElement> typeRepo = new HashMap<>();

    /** The data elements. */
    private final List<IElement<?>> dataElements = new ArrayList<>();

    /**
     * Instantiates a new attachment process bridge.
     *
     * @param context
     *            the context
     */
    public AttachmentProcessorBridge(final EngineContext<?> context) {
        super(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "attachment";
    }

    /**
     * Gets the type.
     *
     * @param ref
     *            the ref
     * @return the type
     */
    private TypeElement getType(final String ref) {
        if (MapUtils.isEmpty(this.typeRepo)) {
            if (!(this.getElement() instanceof ProcessElement)) {
                throw new TechnicalException("Retrieving type element : only process element supported");
            }

            final String typesResourcePath = SpecificationProcesses.resolveResourceName(this.getContext(), ((ProcessElement) this.getElement()).getInput());
            if (StringUtils.isEmpty(typesResourcePath)) {
                throw new TechnicalException("Retrieving type element : process's input attribute mandatory");
            }

            final FormSpecificationData types = this.getProvider().asBean(typesResourcePath, FormSpecificationData.class);
            if (null == types) {
                throw new TechnicalException(String.format("Retrieving type element : file [%s] not loadable", typesResourcePath));
            }

            if (null != types.getTypes()) {
                types.getTypes().forEach(t -> this.typeRepo.put(t.getId(), t));
            }
        }

        return this.typeRepo.get(ref);
    }

    /**
     * Attachment.
     *
     * @param ref
     *            the ref
     * @param id
     *            the id
     * @param model
     *            the model
     */
    public void attachment(final String ref, final String id, final Map<String, Object> model) {
        final TypeElement typeElement = this.getType(ref);

        if (null == typeElement) {
            return;
        }

        // Process mandatory value
        boolean mandatory = true;
        if (model != null) {
            final Object param = model.get("mandatory");
            if (param != null) {
                mandatory = Boolean.valueOf(param.toString());
            }
        }

        // Create data element
        final DataElement dataElement = new DataElement();
        dataElement.setLabel(this.replace(typeElement.getLabel(), model));
        dataElement.setDescription(this.replace(typeElement.getDescription(), model));
        dataElement.setHelp(this.replace(typeElement.getHelp(), model));
        dataElement.setType("File");
        dataElement.setMandatory(mandatory);

        dataElement.setId(id);

        this.dataElements.add(dataElement);
    }

    /**
     * Gets the data elements.
     *
     * @return the dataElements
     */
    public List<IElement<?>> getDataElements() {
        return this.dataElements;
    }

    /**
     * Replace.
     *
     * @param src
     *            the src
     * @param model
     *            the model
     * @return the string
     */
    protected String replace(final String src, final Map<String, Object> model) {
        if (StringUtils.isEmpty(src)) {
            return null;
        }

        final StringBuffer buffer = new StringBuffer();
        final Matcher m = this.pattern.matcher(src);
        while (m.find()) {
            final Object value = model.get(m.group(1));
            m.appendReplacement(buffer, null == value ? "" : value.toString());
        }
        m.appendTail(buffer);

        return buffer.toString();
    }

}
