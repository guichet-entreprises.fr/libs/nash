/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.process;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * The Class ProcessElement.
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "process")
@XmlAccessorType(XmlAccessType.NONE)
public class ProcessElement implements IXml, IProcess {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @XmlAttribute(required = true)
    private String id;

    /** The type. */
    @XmlAttribute(required = true)
    private String type;

    /** The input. */
    @XmlAttribute
    private String input;

    /** The output. */
    @XmlAttribute
    private String output;

    /** The script. */
    @XmlAttribute
    private String script;

    /** The ref. */
    @XmlAttribute
    private String ref;

    /** The rule as attr. */
    @XmlAttribute(name = "if")
    private String ruleAsAttr;

    /** The value. */
    @XmlValue
    private String value;

    /** The status. */
    @XmlAttribute(name = "status")
    private String status;

    /** The status. */
    @XmlAttribute(name = "with-user")
    private String withUser;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCondition() {
        return this.ruleAsAttr;
    }

    /**
     * {@inheritDoc}
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the input.
     *
     * @return the input
     */
    @Override
    public String getInput() {
        return this.input;
    }

    /**
     * Sets the input.
     *
     * @param input
     *            the input to set
     */
    public void setInput(final String input) {
        this.input = input;
    }

    /**
     * Gets the output.
     *
     * @return the ouput
     */
    public String getOutput() {
        return this.output;
    }

    /**
     * Sets the output.
     *
     * @param output
     *            the ouput to set
     */
    public void setOutput(final String output) {
        this.output = output;
    }

    /**
     * Gets the script.
     *
     * @return the script
     */
    public String getScript() {
        return this.script;
    }

    /**
     * Sets the script.
     *
     * @param script
     *            the script to set
     */
    public void setScript(final String script) {
        this.script = script;
    }

    /**
     * Getter on attribute {@link #ref}.
     *
     * @return String ref
     */
    public String getRef() {
        return this.ref;
    }

    /**
     * Setter on attribute {@link #ref}.
     *
     * @param ref
     *            the new value of attribute ref
     */
    public void setRef(final String ref) {
        this.ref = ref;
    }

    /**
     * Getter on attribute {@link #ruleAsAttr}.
     *
     * @return String ruleAsAttr
     */
    public String getRuleAsAttr() {
        return this.ruleAsAttr;
    }

    /**
     * Setter on attribute {@link #ruleAsAttr}.
     *
     * @param ruleAsAttr
     *            the new value of attribute ruleAsAttr
     */
    public void setRuleAsAttr(final String ruleAsAttr) {
        this.ruleAsAttr = ruleAsAttr;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Accesseur sur l'attribut {@link #withUser}.
     *
     * @return String withUser
     */
    public String getWithUser() {
        return withUser;
    }

    /**
     * Mutateur sur l'attribut {@link #withUser}.
     *
     * @param withUser
     *            la nouvelle valeur de l'attribut withUser
     */
    public void setWithUser(final String withUser) {
        this.withUser = withUser;
    }
}
