/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;
import fr.ge.common.nash.engine.adapter.Option;

/**
 * The Box Value Adapter.
 *
 * @author Ali AKKOU
 */

public class BoxValueAdapter extends AbstractValueAdapter<String> {

    /** Icon. */
    @Option(description = "icon")
    private String icon;

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {
        return "Box";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This field type allows selecting a choice from the suggestions. It has a special option for adding an icon from Font Awesome.</br>" //
                + "<b>Example : </b> To use the icon 'fa fa-cog' enter 'cog'.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return BoxValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String fromString(final String value) {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        return (String) value;
    }

    /**
     * Getter on attribute {@link #icon}.
     *
     * @return the icon
     */
    public String getIcon() {
        return this.icon;
    }

    /**
     * Setter on attribute {@link #icon}.
     *
     * @param icon
     *            the icon to set
     */
    public void setIcon(final String icon) {
        this.icon = icon;
    }

}