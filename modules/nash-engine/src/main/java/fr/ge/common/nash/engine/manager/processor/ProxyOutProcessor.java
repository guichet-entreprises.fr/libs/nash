/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor;

import java.util.Optional;
import java.util.function.Predicate;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.bridge.HangoutProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.IBridge;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceRequest;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceResponse;
import fr.ge.common.nash.engine.manager.processor.result.RedirectProcessBean;
import fr.ge.common.nash.engine.manager.processor.result.RedirectProcessResult;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.utils.HashUtil;

/**
 * Processor for proxies (using RedirectProcessor utilities).
 *
 * @author aolubi
 *
 */
public class ProxyOutProcessor extends AbstractProcessor<RedirectProcessBean, ProxyOutProcessor> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ProxyOutProcessor.class);

    /** The bridge. */
    private HangoutProcessorBridge bridge;

    /**
     * Check if output file is present and correct
     */
    private final Predicate<FormSpecificationData> isOutputValid = spec -> {
        if (StringUtils.isEmpty(spec.getId())) {
            LOGGER.info("Validate execute conditions : specification has no ID");
            return false;
        } else {
            LOGGER.info("Validate execute conditions : specification has ID");
            return true;
        }
    };

    /**
     * Check if input file has changed
     */
    private final Predicate<FormSpecificationData> isInputHasChanged = spec -> {
        // get the hash for the input
        // final FormSpecificationData inputFsd =
        // SpecificationProcesses.load(this.getContext(),
        // this.getProcess().getInput(), FormSpecificationData.class);
        // final String inputHash = HashUtil.hashAsString(this.parse(inputFsd));

        final String inputHash = HashUtil.hashAsString(this.getContext().getProvider().asBytes(this.getProcess().getInput()));
        // TODO use a parse function instead of
        // this.getContext().getProvider().asBytes(this.getProcess().getInput())

        // get the hash for the existing output (param file)
        final String paramFileHash = this.getSpecDataValue(spec, "params.hash");

        if (inputHash.equals(paramFileHash)) {
            LOGGER.info("Validate execute conditions : input data has not changed");
            return false;
        } else {
            LOGGER.info("Validate execute conditions : input data had changed");
            return true;
        }
    };

    /**
     * Check if proxy result file is present
     */
    private final Predicate<FormSpecificationData> isProxyResultMissing = spec -> {
        final byte[] resultAsBytes = Optional.ofNullable(this.getSpecDataValue(spec, "params.resultFileName")) //
                .filter(StringUtils::isNotEmpty) //
                .map(resourceName -> this.getContext().getProvider().asBytes(resourceName)) //
                .orElse(null);
        if (ArrayUtils.isEmpty(resultAsBytes)) {
            LOGGER.info("Validate execute conditions : proxy result file is not present");
            return true;
        } else {
            LOGGER.info("Validate execute conditions : proxy result file is present");
            return false;
        }
    };

    /**
     * Check if proxy token is valid
     */
    private final Predicate<FormSpecificationData> isProxyTokenValid = spec -> {
        if (Optional.ofNullable(this.getSpecDataValue(spec, "params.urlValidation")) //
                .filter(StringUtils::isNotEmpty) //
                .map(ServiceRequest::new) //
                .map(ServiceRequest::get) //
                .filter(response -> Status.OK.getStatusCode() == response.getStatus()) //
                .map(ServiceResponse::asString) //
                .map(BooleanUtils::toBoolean) //
                .orElse(false)) {

            LOGGER.info("Validate execute conditions : proxy session token is valid");
            return true;
        } else {
            LOGGER.info("Validate execute conditions : proxy session token is invalid");
            return false;
        }
    };

    /**
     * {@inheritDoc}
     */
    @Override
    protected ProxyOutProcessor init() {
        this.bridge = new HangoutProcessorBridge(this.getContext());
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IBridge getBridge() {
        return this.bridge;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean validateConditions() {
        final String outputResourceName = this.getProcess().getOutput();

        final FormSpecificationData outputData = Optional.ofNullable(outputResourceName) //
                .filter(StringUtils::isNotEmpty) //
                .map(resourceName -> this.getContext().getProvider().asBean(resourceName, FormSpecificationData.class)) //
                .orElse(null);

        if (null == outputData) {
            LOGGER.info("Validate conditions : no output specification file");
        }

        if (null == outputData || //
                (this.isOutputValid.and(this.isProxyResultMissing).and(this.isInputHasChanged).and(this.isProxyTokenValid.negate()).test(outputData)) || //
                (this.isOutputValid.and(this.isProxyResultMissing).and(this.isInputHasChanged.negate()).and(this.isProxyTokenValid.negate()).test(outputData)) //
        ) {
            this.getContext().getProvider().remove(outputResourceName);
            LOGGER.info("Removing output file {}", outputResourceName);
            LOGGER.info("Validate conditions : true");
            return true;
        }

        if (this.isOutputValid.and(this.isProxyResultMissing).and(this.isInputHasChanged.negate()).and(this.isProxyTokenValid).test(outputData) || //
                this.isOutputValid.and(this.isProxyResultMissing).and(this.isInputHasChanged).and(this.isProxyTokenValid).test(outputData)) {
            LOGGER.info("Validate conditions : true");
            return true;
        }

        LOGGER.info("Validate conditions : false");
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object afterExecute(final Object value) {
        if (null == value) {
            LOGGER.warn("Expecting specification data object, having null one");
        } else if (value instanceof FormSpecificationData) {
            final FormSpecificationData paramFileFsd = (FormSpecificationData) value;

            // Calculate hash from input.xml get the hash for the input
            // final FormSpecificationData inputFsd =
            // SpecificationProcesses.load(this.getContext(),
            // this.getProcess().getInput(), FormSpecificationData.class);
            // final String inputDataHashValue =
            // HashUtil.hashAsString(this.parse(inputFsd));

            final String inputDataHashValue = HashUtil.hashAsString(this.getContext().getProvider().asBytes(this.getProcess().getInput()));
            // TODO use a parse function instead of
            // this.getContext().getProvider().asBytes(this.getProcess().getInput())

            // Update param file adding hash input value
            final DataElement data = new DataElement();
            data.setId("hash");
            data.setType("String");
            data.setValue(new ValueElement(inputDataHashValue));
            if (CollectionUtils.isNotEmpty(paramFileFsd.getGroups()) && CollectionUtils.isNotEmpty(paramFileFsd.getGroups().get(0).getData())) {
                paramFileFsd.getGroups().get(0).getData().add(data);
            }
            this.getContext().getProvider().save(this.getProcess().getOutput(), paramFileFsd);
        } else {
            LOGGER.warn("Expecting specification data object, having {}", value.getClass().getSimpleName());
        }

        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IProcessResult<RedirectProcessBean> bind(final Object value) {
        RedirectProcessBean bean = null;

        if (null == value) {
            LOGGER.warn("Expecting specification data object, having null one");
        } else if (value instanceof FormSpecificationData) {
            final FormSpecificationData fsd = (FormSpecificationData) value;

            final DataElement dataElement = (DataElement) SpecificationLoader.find(fsd, "params.urlRedirection");
            final String url = (String) ValueAdapterFactory.type(fsd, dataElement).get(dataElement);

            if (StringUtils.isNotEmpty(url)) {
                bean = new RedirectProcessBean().setUrl(url).setMessage("");
            }
        } else {
            LOGGER.warn("Expecting specification data object, having {}", value.getClass().getSimpleName());
        }

        return new RedirectProcessResult(bean);
    }

    /**
     * Gets the value of a specification data.
     *
     * @param formSpecificationData
     *            the specification
     * @param path
     *            the path to search
     * @return the value
     */
    private String getSpecDataValue(final FormSpecificationData formSpecificationData, final String path) {
        final IElement<?> element = SpecificationLoader.find(formSpecificationData, path);
        String value = null;
        if (element instanceof DataElement) {
            final DataElement dataElement = (DataElement) element;
            if (dataElement.getValue() != null && dataElement.getValue().getContent() != null) {
                value = dataElement.getValue().getContent().toString();
            }
        }
        return value;
    }

    /**
     * Sets the bridge.
     *
     * @param bridge
     *            the new bridge
     */
    public void setBridge(final HangoutProcessorBridge bridge) {
        this.bridge = bridge;
    }

}
