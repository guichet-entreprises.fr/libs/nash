/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import fr.ge.common.nash.engine.adapter.AbstractValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.DateValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_2.value.DateRange;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.binding.DataBinding;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElementBuilder;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.DateValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.utils.CoreUtil;

/**
 * The DateRange value adapter.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class DateRangeValueAdapter extends AbstractValueAdapter<DateRange> {

    /** The Constant FORMATTER. */
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("dd/MM/yyyy");

    /** La constante CONVERTERS. */
    private static final Map<Class<?>, Function<Object, ValueElement>> CONVERTERS;

    static {
        final Map<Class<?>, Function<Object, ValueElement>> map = new HashMap<>();

        map.put(DateRange.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final DateRange period = (DateRange) value;

            if (null != period.getFrom()) {
                builder.addDateValue("from", new DateTime(period.getFrom()).toString(FORMATTER));
            }

            if (null != period.getTo()) {
                builder.addDateValue("to", new DateTime(period.getTo()).toString(FORMATTER));
            }

            return builder.build();
        });

        map.put(FormContentData.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final FormContentData data = (FormContentData) value;

            Arrays.stream(new String[] { "from", "to" }).forEach(key -> {
                Optional.ofNullable(data.withPath(key).asString()) //
                        .filter(str -> !"".equals(str)) //
                        .map(str -> DateValueAdapter.parse(str)) //
                        .ifPresent(date -> {
                            builder.addDateValue(key, date.toString(FORMATTER));
                        });
            });

            return builder.build();
        });

        map.put(DataBinding.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final DataBinding data = (DataBinding) value;

            Arrays.stream(new String[] { "from", "to" }).forEach(key -> {
                Optional.ofNullable(data.withPath(key).asString()) //
                        .filter(str -> !"".equals(str)) //
                        .map(str -> DateValueAdapter.parse(str)) //
                        .ifPresent(date -> {
                            builder.addDateValue(key, date.toString(FORMATTER));
                        });
            });

            return builder.build();
        });

        map.put(HashMap.class, value -> {
            final ValueElementBuilder builder = new ValueElementBuilder();

            final Map<String, String> data = CoreUtil.cast(value);

            Arrays.stream(new String[] { "from", "to" }).forEach(key -> {
                Optional.ofNullable(String.valueOf(data.get(key))) //
                        .filter(str -> !"".equals(str)) //
                        .map(str -> DateValueAdapter.parse(str)) //
                        .ifPresent(date -> {
                            builder.addDateValue(key, date.toString(FORMATTER));
                        });
            });

            return builder.build();
        });

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    /**
     * Default constructor.
     */
    public DateRangeValueAdapter() {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String name() {
        return "DateRange";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String description() {
        return "This data type allows to answer questions by selecting a start date and an end date, in a double calendar.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String template() {
        return DateRangeValueAdapter.class.getName().replace('.', '/') + "Resources/template";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DateRange fromValueElement(final ValueElement value) {
        final ListValueElement lst = Optional.ofNullable(value) //
                .map(ValueElement::getContent) //
                .filter(v -> v instanceof ListValueElement) //
                .map(ListValueElement.class::cast) //
                .orElse(null);

        if (null == lst) {
            return null;
        }

        final Map<String, ? extends DateValueElement> map = lst.asMap(DateValueElement.class);

        final DateRange period = new DateRange();

        if (map.containsKey("from")) {
            Optional.ofNullable(DateValueAdapter.parse(map.get("from").getValue())).map(dt -> dt.toCalendar(Locale.getDefault())).orElse(null);
            period.setFrom(DateValueAdapter.parseAsCalendar(map.get("from").getValue()));
        }

        if (map.containsKey("to")) {
            period.setTo(DateValueAdapter.parseAsCalendar(map.get("to").getValue()));
        }

        return null == period.getFrom() && null == period.getTo() ? null : period;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueElement toValueElement(final SpecificationLoader loader, final DataElement dataElement, final Object value) {
        final Function<Object, ValueElement> converter = this.findConverter(value);
        if (null == converter) {
            return null;
        } else {
            return converter.apply(value);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(final Object value) {
        if (value instanceof DateRange) {
            return value.toString();
        } else if (value instanceof Calendar) {
            return new DateTime(value).toString(FORMATTER);
        } else {
            return null;
        }
    }

    /**
     * Find converter.
     *
     * @param src
     *            src
     * @return function
     */
    private Function<Object, ValueElement> findConverter(final Object src) {
        for (final Map.Entry<Class<?>, Function<Object, ValueElement>> entry : CONVERTERS.entrySet()) {
            if (entry.getKey().isInstance(src)) {
                return entry.getValue();
            }
        }

        return null;
    }

}
