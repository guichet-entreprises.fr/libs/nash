/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script;

import javax.annotation.PostConstruct;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.engine.script.expression.ScriptExpression;
import fr.ge.common.nash.engine.script.expression.ScriptExpressionParser;

/**
 *
 * @author Christian Cougourdan
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NashScriptEngine {

    @Autowired
    private ScriptExpressionParser parser;

    @Autowired
    private ScriptExpressionExecutorFactory executorFactory;

    private ScriptExpressionExecutor executor;

    @PostConstruct
    public void init() {
        this.executor = this.executorFactory.getScriptExpressionExecutor();
    }

    /**
     * Parse script expression without local scope, ie all root variables attached
     * to '_record' object.
     *
     * @param stringExpression
     *            script expression as string
     * @return {@link ScriptExpression}
     */
    public ScriptExpression parse(final String stringExpression) {
        return this.parse(stringExpression, false);
    }

    /**
     * Parse script expression. Defining true to local scope attaches all root
     * variables to '_page' object, otherwise to '_record' object.
     *
     * @param stringExpression
     *            script expression as string
     * @param withLocalScope
     *            true if using local scope
     * @return {@link ScriptExpression}
     */
    public ScriptExpression parse(final String stringExpression, final boolean withLocalScope) {
        return this.parser.parse(stringExpression, withLocalScope);
    }

    /**
     * Evaluate script expression.
     *
     * @param expr
     *            script expression
     * @param context
     *            evaluation context
     * @return script evaluation result
     * @throws ScriptException
     */
    public Object eval(final ScriptExpression expr, final ScriptExecutionContext context) {
        return this.executor.eval(expr, context);
    }

    /**
     * Evaluate script expression.
     *
     * @param expr
     *            script expression
     * @param context
     *            evaluation context
     * @param expectedClass
     *            expected value type
     * @throws ScriptException
     */
    public <R> R eval(final ScriptExpression expr, final ScriptExecutionContext context, final Class<R> expectedClass) {
        return ScriptValueConversionService.convert(this.eval(expr, context), expectedClass);
    }

    public Object eval(final String expr, final ScriptExecutionContext context) {
        return this.eval(expr, false, context);
    }

    public <R> R eval(final String expr, final ScriptExecutionContext context, final Class<R> expectedClass) {
        return ScriptValueConversionService.convert(this.eval(expr, false, context), expectedClass);
    }

    public Object eval(final String expr, final boolean withLocalScope, final ScriptExecutionContext context) {
        return this.executor.eval(this.parse(expr, withLocalScope), context);
    }

    public <R> R eval(final String expr, final boolean withLocalScope, final ScriptExecutionContext context, final Class<R> expectedClass) {
        return ScriptValueConversionService.convert(this.eval(expr, withLocalScope, context), expectedClass);
    }

}
