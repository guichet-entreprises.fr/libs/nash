/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter;

import javax.servlet.http.HttpServletRequest;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.provider.IProvider;

/**
 * Data type interface.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public interface IValueAdapter<T> extends Cloneable {

    /**
     * Gets the name used to reference the value adapter.
     *
     * @return the name
     */
    String name();

    /**
     * Gets the description of the value adapter.
     *
     * @return the description
     */
    String description();

    /**
     * Return HTML template resource for specific data type.
     *
     * @return HTML template resource
     */
    String template();

    /**
     * Convert value from string to this data type. The value is human readable,
     * coming from user input.
     *
     * @param value
     *            string value to convert
     * @return corresponding typed value
     */
    T fromString(final String value);

    /**
     * Convert value from this data type to human readable string, in order to
     * display into views.
     *
     * @param value
     *            typed value to convert
     * @return corresponding string value
     */
    String toString(final Object value);

    /**
     * Extract value from {@link DataElement}.
     *
     * @param dataElement
     *            the data element
     * @return corresponding typed value
     */
    T get(final DataElement dataElement);

    /**
     * Extract value from {@link DataElement}.
     *
     * @param provider
     *            the provider
     * @param dataElement
     *            the data element
     * @return corresponding typed value
     */
    T get(final IProvider provider, final DataElement dataElement);

    /**
     * Gets a {@link DataElement} value as bytes.
     *
     * @param provider
     *            the provider
     * @param dataElement
     *            the data element
     * @return the value as bytes
     */
    byte[] getAsBytes(final IProvider provider, final DataElement dataElement);

    /**
     * Register value to XML {@link DataElement}.
     *
     * @param dataElement
     *            the data element to update
     * @param value
     *            typed value to convert
     * @return corresponding value
     */
    T set(final DataElement dataElement, final Object value);

    /**
     * Register value to XML {@link DataElement}.
     *
     * @param loader
     *            the specification loader
     * @param dataElement
     *            the data element to update
     * @param value
     *            typed value to convert
     * @return corresponding value
     */
    T set(final SpecificationLoader loader, final DataElement dataElement, final Object value);

    /**
     * Register value to XML {@link DataElement}.
     *
     * @param loader
     *            the specification loader
     * @param provider
     *            the provider
     * @param dataElement
     *            the data element to update
     * @param value
     *            typed value to convert
     * @return corresponding value
     */
    T set(final SpecificationLoader loader, final IProvider provider, final DataElement dataElement, final Object value);

    /**
     * Register value to XML {@link DataElement} from HTTP request.
     *
     * @param loader
     *            the specification loader
     * @param dataElement
     *            the data element to update
     * @param key
     *            root key of the value in request
     * @param request
     *            input HTTP request
     * @return object
     */
    T set(final SpecificationLoader loader, final DataElement dataElement, final String key, final HttpServletRequest request);

    /**
     * Gets the default value.
     *
     * @return default value
     */
    T getDefaultValue();

    /**
     * Gets the default value relative to specified data element.
     *
     * @param elm
     *            data element attached to this value adapter
     * @return default value
     */
    T getDefaultValue(DataElement elm);

    /**
     * External path.
     *
     * @return the string
     */
    String externalPath();

    /**
     * Return true if data are updated by this adapter and have to not be managed by
     * engine.
     *
     * @return true or false
     */
    boolean hasOwnUpdate();

}
