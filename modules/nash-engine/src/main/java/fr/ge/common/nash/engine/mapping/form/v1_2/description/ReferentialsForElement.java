/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.description;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import fr.ge.common.nash.engine.mapping.IXml;

/**
 * Referentials "for" element.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "referential")
@XmlAccessorType(XmlAccessType.NONE)
public class ReferentialsForElement implements IXml {

    /** UID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @XmlAttribute(name = "id")
    private String id;

    /** The path. */
    @XmlValue
    private String path;

    /**
     * Getter on attribute {@link #id}.
     *
     * @return String id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Setter on attribute {@link #id}.
     *
     * @param id
     *            the new value of attribute id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Getter on attribute {@link #path}.
     *
     * @return String path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Setter on attribute {@link #path}.
     *
     * @param path
     *            the new value of attribute path
     */
    public void setPath(final String path) {
        this.path = path;
    }

}
