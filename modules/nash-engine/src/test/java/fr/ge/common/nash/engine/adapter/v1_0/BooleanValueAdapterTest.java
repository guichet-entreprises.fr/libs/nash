/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_0;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

import org.junit.Test;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;

/**
 * Class BooleanValueAdapterTest.
 *
 * @author Christian Cougourdan
 */
public class BooleanValueAdapterTest extends AbstractValueAdapterTest<Boolean> {

    /**
     * Test from string null or empty.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromStringNullOrEmpty() throws Exception {
        final BooleanValueAdapter adapter = new BooleanValueAdapter();

        assertFalse(adapter.fromString(""));
        assertFalse(adapter.fromString(null));
    }

    /**
     * Test from string true false.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromStringTrueFalse() throws Exception {
        final BooleanValueAdapter adapter = new BooleanValueAdapter();

        assertTrue(adapter.fromString("true"));
        assertFalse(adapter.fromString("false"));
    }

    /**
     * Test from string yes no.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromStringYesNo() throws Exception {
        final BooleanValueAdapter adapter = new BooleanValueAdapter();

        assertTrue(adapter.fromString("yes"));
        assertFalse(adapter.fromString("no"));
    }

    /**
     * Test from string numbers.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromStringNumbers() throws Exception {
        final BooleanValueAdapter adapter = new BooleanValueAdapter();

        assertTrue(adapter.fromString("1"));
        assertFalse(adapter.fromString("0"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IValueAdapter<Boolean> type() {
        return new BooleanValueAdapter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        return Collections.singletonMap("", new String[] { "true" });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequest(final Boolean actual, final DataElement dataElement) {
        assertEquals(Boolean.TRUE, actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "yes"), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected void assertFromHttpRequestNone(final Boolean actual, final DataElement dataElement) {
        this.assertFromHttpRequestFalse(actual, dataElement);
    }

    @Test
    public final void testFromHttpRequestYesValue() throws Exception {
        this.executeTestFromHttpRequest(() -> Collections.singletonMap("", new String[] { "yes" }), this::assertFromHttpRequest);
    }

    @Test
    public final void testFromHttpRequestOneValue() throws Exception {
        this.executeTestFromHttpRequest(() -> Collections.singletonMap("", new String[] { "1" }), this::assertFromHttpRequest);
    }

    /**
     * {@inheritDoc}
     */
    protected void assertFromHttpRequestFalse(final Object actual, final DataElement dataElement) {
        assertEquals(Boolean.FALSE, actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "no"), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Test
    public final void testFromHttpRequestFalseValue() throws Exception {
        this.executeTestFromHttpRequest(() -> Collections.singletonMap("", new String[] { "false" }), this::assertFromHttpRequestFalse);
    }

    @Test
    public final void testFromHttpRequestNoValue() throws Exception {
        this.executeTestFromHttpRequest(() -> Collections.singletonMap("", new String[] { "no" }), this::assertFromHttpRequestFalse);
    }

    @Test
    public final void testFromHttpRequestZeroValue() throws Exception {
        this.executeTestFromHttpRequest(() -> Collections.singletonMap("", new String[] { "0" }), this::assertFromHttpRequestFalse);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        return Collections.singletonMap("", new String[] { "" });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequestEmpty(final Boolean actual, final DataElement dataElement) {
        assertEquals(Boolean.FALSE, actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "no"), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Boolean buildFromObject() {
        return Boolean.TRUE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObject(final Boolean actual, final DataElement dataElement) {
        assertEquals(Boolean.TRUE, actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "yes"), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Boolean buildFromObjectEmpty() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObjectEmpty(final Boolean actual, final DataElement dataElement) {
        assertEquals(Boolean.FALSE, actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "no"), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected void assertFromObjectNone(final Boolean actual, final DataElement dataElement) {
        this.assertFromHttpRequestFalse(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXml() {
        return String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "yes").getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXml(final Boolean actual, final DataElement dataElement) {
        assertEquals(Boolean.TRUE, actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXmlEmpty() {
        return String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "").getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlEmpty(final Boolean actual, final DataElement dataElement) {
        assertEquals(Boolean.FALSE, actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlNone(final Boolean actual, final DataElement dataElement) {
        assertEquals(Boolean.FALSE, actual);
    }

}
