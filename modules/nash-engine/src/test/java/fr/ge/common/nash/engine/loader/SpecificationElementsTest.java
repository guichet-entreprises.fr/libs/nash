/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.test.builder.DataElementBuilder;
import fr.ge.common.nash.engine.test.builder.GroupElementBuilder;
import fr.ge.common.nash.test.AbstractBeanResourceTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class SpecificationElementsTest extends AbstractBeanResourceTest {

    private SpecificationLoader loader;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void setUp() throws Exception {
        final Configuration configuration = new Configuration(new Properties());
        final IProvider provider = mock(IProvider.class);
        this.loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        final FormSpecificationDescription desc = new FormSpecificationDescription();
        desc.setVersion("1.2");

        when(provider.asBean(eq("description.xml"), eq(FormSpecificationDescription.class))).thenReturn(desc);
    }

    @Test
    public void testClearSimpleDataElement() throws Exception {
        final DataElement dataElement = new DataElementBuilder("fld01").type("String").value("fld01 value").build();

        final IElement<?> actual = this.loader.elements().clear(dataElement);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo(dataElement.getId())), //
                        hasProperty("type", equalTo(dataElement.getType())), //
                        hasProperty("value", nullValue()) //
                ) //
        );
    }

    @Test
    public void testClearMultiOccurrenceDataElement() throws Exception {
        final DataElement dataElement = new DataElementBuilder("fld01[12]").type("String").value("fld01 value").minOccurs("0").build();

        final IElement<?> actual = this.loader.elements().clear(dataElement);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("fld01[]")), //
                        hasProperty("type", equalTo(dataElement.getType())), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", equalTo(dataElement.getMinOccurs())) //
                ) //
        );
    }

    @Test
    public void testClearNoIndexMultiOccurrenceDataElement() throws Exception {
        final DataElement dataElement = new DataElementBuilder("fld01[]").type("String").value("fld01 value").minOccurs("0").build();

        final IElement<?> actual = this.loader.elements().clear(dataElement);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("fld01[]")), //
                        hasProperty("type", equalTo(dataElement.getType())), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", equalTo(dataElement.getMinOccurs())) //
                ) //
        );
    }

    @Test
    public void testClearSimpleGroupElement() throws Exception {
        final GroupElement element = new GroupElementBuilder("grp01").datas( //
                new DataElementBuilder("fld01").type("String").value("fld01 value").build(), //
                new DataElementBuilder("fld02").type("String").value("fld02 value").build() //
        ).build();

        final IElement<?> actual = this.loader.elements().clear(element);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo(element.getId())), //
                        hasProperty("minOccurs", equalTo(element.getMinOccurs())) //
                ) //
        );

        assertThat(actual.getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("fld02")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );
    }

    @Test
    public void testClearMultiOccurrenceGroupElement() throws Exception {
        final GroupElement element = new GroupElementBuilder("grp01[3]").datas( //
                new DataElementBuilder("fld01").type("String").value("fld01 value").build(), //
                new DataElementBuilder("fld02").type("String").value("fld02 value").build() //
        ).minOccurs("1").build();

        final IElement<?> actual = this.loader.elements().clear(element);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("grp01[]")), //
                        hasProperty("minOccurs", equalTo(element.getMinOccurs())) //
                ) //
        );

        assertThat(actual.getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("fld02")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );
    }

    @Test
    public void testClearNoIndexMultiOccurrenceGroupElement() throws Exception {
        final GroupElement element = new GroupElementBuilder("grp01[]").datas( //
                new DataElementBuilder("fld01").type("String").value("fld01 value").build(), //
                new DataElementBuilder("fld02").type("String").value("fld02 value").build() //
        ).minOccurs("1").build();

        final IElement<?> actual = this.loader.elements().clear(element);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("grp01[]")), //
                        hasProperty("minOccurs", equalTo(element.getMinOccurs())) //
                ) //
        );

        assertThat(actual.getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("fld02")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );
    }

    @Test
    public void testClearMultiOccurrenceTree() throws Exception {
        final GroupElement element = new GroupElementBuilder("grp01").datas( //
                new DataElementBuilder("fld01").type("String").value("fld01 value").build(), //
                new DataElementBuilder("fld02").type("String").value("fld02 value").build(), //
                new GroupElementBuilder("grp02[0]").datas( //
                        new DataElementBuilder("fld02x01").type("String").value("fld02x01 (a) value").build(), //
                        new DataElementBuilder("fld02x02[0]").type("String").minOccurs("0").value("fld02x02 (a.0) value").build(), //
                        new DataElementBuilder("fld02x02[1]").type("String").minOccurs("0").value("fld02x02 (a.1) value").build(), //
                        new DataElementBuilder("fld02x02[2]").type("String").minOccurs("0").value("fld02x02 (a.2) value").build() //
                ).minOccurs("1").build(), //
                new GroupElementBuilder("grp02[1]").datas( //
                        new DataElementBuilder("fld02x01").type("String").value("fld02x01 (b) value").build(), //
                        new DataElementBuilder("fld02x02[0]").type("String").minOccurs("0").value("fld02x02 (b) value").build() //
                ).minOccurs("1").build(), //
                new DataElementBuilder("fld03").type("String").value("fld03 value").build() //
        ).build();

        final IElement<?> actual = this.loader.elements().clear(element);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("grp01")), //
                        hasProperty("data", hasSize(4)), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("fld02")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(2), //
                allOf( //
                        hasProperty("id", equalTo("grp02[0]")), //
                        hasProperty("data", hasSize(2)), //
                        hasProperty("minOccurs", equalTo("1")) //
                ) //
        );

        assertThat(actual.getData().get(2).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld02x01")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(2).getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("fld02x02[0]")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", equalTo("0")) //
                ) //
        );

        assertThat(actual.getData().get(3), //
                allOf( //
                        hasProperty("id", equalTo("fld03")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );
    }

    @Test
    public void testClearMultiOccurrenceDataElementWithMinOccurs() throws Exception {
        final GroupElement element = new GroupElementBuilder("grp01").datas( //
                new DataElementBuilder("fld01").type("String").value("fld01 value").build(), //
                new DataElementBuilder("fld02[0]").type("String").value("fld02.0 value").minOccurs("2").build(), //
                new DataElementBuilder("fld02[1]").type("String").value("fld02.1 value").minOccurs("2").build(), //
                new DataElementBuilder("fld02[2]").type("String").value("fld02.2 value").minOccurs("2").build(), //
                new DataElementBuilder("fld02[3]").type("String").value("fld02.3 value").minOccurs("2").build() //
        ).build();

        final IElement<?> actual = this.loader.elements().clear(element);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("grp01")), //
                        hasProperty("data", hasSize(3)) //
                ) //
        );

        assertThat(actual.getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("fld02[0]")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", equalTo("2")) //
                ) //
        );

        assertThat(actual.getData().get(2), //
                allOf( //
                        hasProperty("id", equalTo("fld02[1]")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", equalTo("2")) //
                ) //
        );
    }

    @Test
    public void testClearMultiOccurrenceGroupElementWithMinOccurs() throws Exception {
        final GroupElement element = new GroupElementBuilder("grp01").datas( //
                new DataElementBuilder("fld01").type("String").value("fld01 value").build(), //
                new GroupElementBuilder("grp02[]").minOccurs("2").datas( //
                        new DataElementBuilder("fld02[0]").type("String").value("fld02.0 value").build(), //
                        new DataElementBuilder("fld02[1]").type("String").value("fld02.1 value").build() //
                ).build() //
        ).build();

        final IElement<?> actual = this.loader.elements().clear(element);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("grp01")), //
                        hasProperty("data", hasSize(3)) //
                ) //
        );

        assertThat(actual.getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("type", equalTo("String")), //
                        hasProperty("value", nullValue()), //
                        hasProperty("minOccurs", nullValue()) //
                ) //
        );

        assertThat(actual.getData().get(1), //
                allOf( //
                        instanceOf(GroupElement.class), //
                        hasProperty("id", equalTo("grp02[0]")), //
                        hasProperty("data", hasSize(1)) //
                ) //
        );

        assertThat(actual.getData().get(2), //
                allOf( //
                        instanceOf(GroupElement.class), //
                        hasProperty("id", equalTo("grp02[1]")), //
                        hasProperty("data", hasSize(1)) //
                ) //
        );
    }

    @Test
    public void testFindRoot() throws Exception {
        final FormSpecificationData spec = this.resourceAsBean("find.xml", FormSpecificationData.class);

        final IElement<?> actual = SpecificationElements.find(spec.getGroups(), "grp01");
        assertThat(actual, //
                allOf( //
                        instanceOf(GroupElement.class), //
                        hasProperty("id", equalTo("grp01")), //
                        hasProperty("data", hasSize(8)) //
                ) //
        );
    }

    @Test
    public void testFindData() throws Exception {
        final FormSpecificationData spec = this.resourceAsBean("find.xml", FormSpecificationData.class);

        final IElement<?> actual = SpecificationElements.find(spec.getGroups(), "grp01.fld03");
        assertThat(actual, //
                allOf( //
                        instanceOf(DataElement.class), //
                        hasProperty("id", equalTo("fld03")), //
                        hasProperty("path", equalTo("grp01.fld03")), //
                        hasProperty("value", hasProperty("content", equalTo("Fld03 value"))) //
                ) //
        );
    }

    @Test
    public void testFindMultiGroupNoIndex() throws Exception {
        final FormSpecificationData spec = this.resourceAsBean("find.xml", FormSpecificationData.class);

        final IElement<?> actual = SpecificationElements.find(spec.getGroups(), "grp01.grp01x02");
        assertThat(actual, //
                allOf( //
                        instanceOf(GroupElement.class), //
                        hasProperty("id", equalTo("grp01x02[0]")), //
                        hasProperty("path", equalTo("grp01.grp01x02[0]")), //
                        hasProperty("data", hasSize(4)) //
                ) //
        );
    }

    @Test
    public void testFindMultiGroupEmptyIndex() throws Exception {
        final FormSpecificationData spec = this.resourceAsBean("find.xml", FormSpecificationData.class);

        final IElement<?> actual = SpecificationElements.find(spec.getGroups(), "grp01.grp01x02[]");
        assertThat(actual, //
                allOf( //
                        instanceOf(GroupElement.class), //
                        hasProperty("id", equalTo("grp01x02[0]")), //
                        hasProperty("path", equalTo("grp01.grp01x02[0]")), //
                        hasProperty("data", hasSize(4)) //
                ) //
        );
    }

    @Test
    public void testFindMultiGroupFilledIndex() throws Exception {
        final FormSpecificationData spec = this.resourceAsBean("find.xml", FormSpecificationData.class);

        final IElement<?> actual = SpecificationElements.find(spec.getGroups(), "grp01.grp01x02[1]");
        assertThat(actual, //
                allOf( //
                        instanceOf(GroupElement.class), //
                        hasProperty("id", equalTo("grp01x02[1]")), //
                        hasProperty("path", equalTo("grp01.grp01x02[1]")), //
                        hasProperty("data", hasSize(3)) //
                ) //
        );
    }

}
