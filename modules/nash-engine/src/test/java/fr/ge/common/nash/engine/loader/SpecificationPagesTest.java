package fr.ge.common.nash.engine.loader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.IContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.messageresolver.AbstractMessageResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.template.NodeWrapper;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.support.thymeleaf.Constants;
import fr.ge.common.nash.engine.support.thymeleaf.NashDialect;
import fr.ge.common.nash.engine.support.thymeleaf.NashMessageResolver;
import fr.ge.common.nash.engine.test.AbstractBeanResourceTest;
import fr.ge.common.utils.CoreUtil;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class SpecificationPagesTest extends AbstractBeanResourceTest {

    public static final Logger LOGGER = LoggerFactory.getLogger(SpecificationPagesTest.class);

    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.asBytes("description.xml")).thenReturn(this.resourceAsBytes("description.xml"));
        when(this.provider.asBean(any(), any())).thenCallRealMethod();
        when(this.provider.getAbsolutePath(any())).thenCallRealMethod();
    }

    protected void writeResource(final String name, final String content) {
        final String resourceName = this.resourceName(name);
        final String resourcePath = String.format("src/test/resources/%s/%s", this.getClass().getPackage().getName().replace('.', '/'), resourceName);
        try {
            Files.write(Paths.get(resourcePath), content.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (final IOException ex) {
            LOGGER.warn("Resource {} not writable", resourceName, ex);
        }
    }

    private void test(final String root, final String widgetName, final int pageNum) {
        final String label = (null == widgetName ? root : widgetName);
        final String prefix = null == widgetName ? "" : widgetName + '/';
        when(this.provider.asBytes("step01.xml")).thenReturn(this.resourceAsBytes(root + "/" + prefix + "step01.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, new Configuration(new Properties()), this.provider);
        final StepElement step = loader.stepsMgr().find(0);
        final SpecificationPages page = new SpecificationPages(loader);

        CoreUtil.time("Find " + label + " template (first time)", () -> page.find(step, pageNum));

        final String actual = CoreUtil.time("Find " + label + " template", () -> page.find(step, pageNum));
        // this.writeResource(root + "/" + prefix + "expected-template.html", actual);
        assertEquals(this.resourceAsString(root + "/" + prefix + "expected-template.html"), actual);

        final NodeWrapper node = CoreUtil.time("Load " + label + " template model", () -> page.buildTemplateModel(step, pageNum));

        final Map<String, Object> model = new HashMap<>();
        model.put(Constants.MODEL_LOADER, loader);
        model.put(Constants.MODEL_STEP, loader.data(step));
        model.put(Constants.MODEL_STEP_INDEX, 0);
        model.put(Constants.MODEL_PAGE, node);

        final String html = CoreUtil.time("Use found " + label + " template", () -> this.getTemplateEngine().process(actual, this.buildTemplateContext(model)));
        // this.writeResource(root + "/" + prefix + "expected.html", html);
        assertEquals(this.resourceAsString(root + "/" + prefix + "expected.html"), html);
    }

    private void testV2(final String widgetName) throws Exception {
        this.test("v1.2", widgetName, 0);
    }

    @Test
    public void testSimple() throws Exception {
        this.test("simple", null, 1);
    }

    @Test
    public void testV1() throws Exception {
        this.test("v1.0", null, 0);
    }

    @Test
    public void testAmount() throws Exception {
        this.testV2("Amount");
    }

    @Test
    public void testBIC() throws Exception {
        this.testV2("BIC");
    }

    @Test
    public void testBooleanCheckBox() throws Exception {
        this.testV2("BooleanCheckBox");
    }

    @Test
    public void testBooleanComboBox() throws Exception {
        this.testV2("BooleanComboBox");
    }

    @Test
    public void testBox() throws Exception {
        this.testV2("Box");
    }

    @Test
    public void testCheckBoxes() throws Exception {
        this.testV2("CheckBoxes");
    }

    @Test
    public void testComboBoxExternal() throws Exception {
        this.testV2("ComboBoxExternal");
    }

    @Test
    public void testComboBoxMultiple() throws Exception {
        this.testV2("ComboBoxMultiple");
    }

    @Test
    public void testComboBoxSelectExternal() throws Exception {
        this.testV2("ComboBoxSelectExternal");
    }

    @Test
    public void testComboBox() throws Exception {
        this.testV2("ComboBox");
    }

    @Test
    public void testDateBirthDay() throws Exception {
        this.testV2("DateBirthDay");
    }

    @Test
    public void testDateFuture() throws Exception {
        this.testV2("DateFuture");
    }

    @Test
    public void testDatePast() throws Exception {
        this.testV2("DatePast");
    }

    @Test
    public void testDateRange() throws Exception {
        this.testV2("DateRange");
    }

    @Test
    public void testDescription() throws Exception {
        this.testV2("Description");
    }

    @Test
    public void testEmail() throws Exception {
        this.testV2("Email");
    }

    @Test
    public void testIBAN() throws Exception {
        this.testV2("IBAN");
    }

    @Test
    public void testNIR() throws Exception {
        this.testV2("NIR");
    }

    @Test
    public void testOfficialJournalEu() throws Exception {
        this.testV2("OfficialJournalEu");
    }

    @Test
    public void testPasswordBase64() throws Exception {
        this.testV2("PasswordBase64");
    }

    @Test
    public void testPasswordBCrypt() throws Exception {
        this.testV2("PasswordBCrypt");
    }

    @Test
    public void testPatternString() throws Exception {
        this.testV2("PatternString");
    }

    @Test
    public void testPercentage() throws Exception {
        this.testV2("Percentage");
    }

    @Test
    public void testPhone() throws Exception {
        this.testV2("Phone");
    }

    @Test
    public void testPreviewFile() throws Exception {
        this.testV2("PreviewFile");
    }

    @Test
    public void testRadiosBox() throws Exception {
        this.testV2("RadiosBox");
    }

    @Test
    public void testRadios() throws Exception {
        this.testV2("Radios");
    }

    @Test
    public void testRating() throws Exception {
        this.testV2("Rating");
    }

    @Test
    public void testRemoteContent() throws Exception {
        this.testV2("RemoteContent");
    }

    @Test
    public void testRIB() throws Exception {
        this.testV2("RIB");
    }

    @Test
    public void testSIRENApiEntreprise() throws Exception {
        this.testV2("SIRENApiEntreprise");
    }

    @Test
    public void testSIREN() throws Exception {
        this.testV2("SIREN");
    }

    @Test
    public void testSIRETApiEntreprise() throws Exception {
        this.testV2("SIRETApiEntreprise");
    }

    @Test
    public void testSIRET() throws Exception {
        this.testV2("SIRET");
    }

    @Test
    public void testStringReadOnly() throws Exception {
        this.testV2("StringReadOnly");
    }

    @Test
    public void testString() throws Exception {
        this.testV2("String");
    }

    @Test
    public void testTextReadOnly() throws Exception {
        this.testV2("TextReadOnly");
    }

    @Test
    public void testText() throws Exception {
        this.testV2("Text");
    }

    @Test
    public void testURLBase64() throws Exception {
        this.testV2("URLBase64");
    }

    @Test
    public void testYear() throws Exception {
        this.testV2("Year");
    }

    @Test
    public void testV2() throws Exception {
        this.test("v1.2", null, 0);
    }

    @Test
    public void testLarge() throws Exception {
        this.test("large", null, 5);
    }

    @Test
    public void testBuildTemplateModel() throws Exception {
        when(this.provider.asBytes("step01.xml")).thenReturn(this.resourceAsBytes("large/step01.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, new Configuration(new Properties()), this.provider);
        final StepElement step = loader.stepsMgr().find(0);
        final EngineContext<FormSpecificationData> ctx = loader.buildEngineContext(step.getPosition());
        final SpecificationPages pages = new SpecificationPages(loader);

        final Supplier<Object> fn = () -> {
            final Map<String, NodeWrapper> map = new HashMap<>();
            for (int i = 0; i < ctx.getElement().getGroups().size(); i++) {
                final NodeWrapper wrapper = pages.buildTemplateModel(step, i);
                map.put(wrapper.getPath().getId(), wrapper);
            }
            return map;
        };

        CoreUtil.time("Build template model (first time)", fn);
        final Object actual = CoreUtil.time("Build template model", fn);

        assertNotNull(actual);
    }

    @Test
    public void testRepeatableData() throws Exception {
        this.test("repeatable", "data", 0);
    }

    @Test
    public void testRepeatableGroup() throws Exception {
        this.test("repeatable", "group", 0);
    }

    @Test
    public void testRepeatableBoth() throws Exception {
        this.test("repeatable", "both", 0);
    }

    @Test
    public void testUiBuild() throws Exception {
        when(this.provider.asBytes("step01.xml")).thenReturn(this.resourceAsBytes("v1.2/step01.xml"));

        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final MockServletContext servletContext = new MockServletContext();

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, new Configuration(new Properties()), this.provider);
        final String tpl = CoreUtil.time("", () -> loader.pages().build(request, response, servletContext, loader.stepsMgr().find(0), 0, null));

        // this.writeResource("v1.2/expected-direct.html", tpl);
        assertEquals(this.resourceAsString("v1.2/expected-direct.html"), tpl);
    }

    @Test
    public void testBuildDataFragment() throws Exception {
        this.testBuildFragment("repeatable/data", "page01.fld03");
    }

    @Test
    public void testBuildGroupFragment() throws Exception {
        this.testBuildFragment("repeatable/group", "page01.grp03");
    }

    @Test
    public void testBuildBothFragment() throws Exception {
        this.testBuildFragment("repeatable/both", "page01.grp03");
    }

    private void testBuildFragment(final String basePath, final String elementPath) throws Exception {
        final int idxPage = 0;
        when(this.provider.asBytes("step01.xml")).thenReturn(this.resourceAsBytes(basePath + "/step01.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, new Configuration(new Properties()), this.provider);
        final EngineContext<FormSpecificationData> engineContext = loader.buildEngineContext(0);
        final IElement<?> root = SpecificationLoader.find(engineContext.getElement(), elementPath);

        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final MockServletContext servletContext = new MockServletContext();

        final String actual = loader.pages().buildFragment(request, response, servletContext, engineContext, idxPage, root, null);
        assertEquals(this.resourceAsString(basePath + "/expected-fragment.html"), actual);
    }

    private TemplateEngine getTemplateEngine() {
        final StringTemplateResolver templateResolver = new StringTemplateResolver();
        templateResolver.setOrder(1);
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCacheable(false);

        final TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolvers(new HashSet<>(Arrays.asList(templateResolver)));
        templateEngine.setAdditionalDialects(Collections.singleton(new NashDialect()));
        templateEngine.setMessageResolver(new NashMessageResolver());

        return templateEngine;
    }

    private IContext buildTemplateContext(final Map<String, Object> src) {
        final Map<String, Object> model = new HashMap<>();
        model.put("code", "2019-05-REC-ORD-42");
        model.put("idxStep", 0);
        model.putAll(src);

        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final MockServletContext servletContext = new MockServletContext();

        return new WebContext(request, response, servletContext, Locale.getDefault(), model);
    }

    private class TestMessageResolver extends AbstractMessageResolver {

        @Override
        public String resolveMessage(final ITemplateContext context, final Class<?> origin, final String key, final Object[] messageParameters) {
            return String.format("[%s] %s", context.getLocale(), MessageFormat.format(key, messageParameters));
        }

        @Override
        public String createAbsentMessageRepresentation(final ITemplateContext context, final Class<?> origin, final String key, final Object[] messageParameters) {
            return String.format("??%s_%s??", MessageFormat.format(key, messageParameters), context.getLocale());
        }

    }

}
