/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.binding;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.test.AbstractTest;

/**
 *
 * @author Christian Cougourdan
 */
public class ObjectDataBindingTest extends AbstractTest {

    @Test
    public void testSimple() throws Exception {
        final Map<String, Object> jsonAsMap = CoreUtil.cast(new ObjectMapper().readValue(this.resourceAsBytes("data.json"), HashMap.class));
        final DataBinding binding = new ObjectDataBinding(jsonAsMap);

        assertThat(binding, notNullValue());
        assertThat(binding.withPath("step01"), notNullValue());
        assertThat(binding.withPath("step01.grp01.grp01x1"), equalTo(binding.withPath("step01").withPath("grp01").withPath("grp01x1")));
        assertThat(binding.withPath("step01.grp01.grp01x2"), equalTo(binding.withPath("step01").withPath("grp01").withPath("grp01x2")));

        final DataBinding grp01 = binding.withPath("step01.grp01");
        assertThat(grp01.withPath("fld01").asString(), equalTo("{val=yes}"));
        assertThat(grp01.withPath("fld01").asInt(), nullValue());
        assertThat(grp01.withPath("fld01.val").asString(), equalTo("yes"));
        assertThat(grp01.withPath("fld02.val").asString(), equalTo("yes"));
        assertThat(grp01.withPath("fld03.val").asString(), equalTo("42"));
        assertThat(grp01.withPath("fld03.val").asInt(), equalTo(42));
        assertThat(grp01.withPath("fld04.val").asString(), equalTo("13001977100018"));
        assertThat(grp01.withPath("fld05").asString(), equalTo("batman@gotham-city.us"));
        assertThat(grp01.withPath("fld06.val1").asString(), equalTo("Value #1"));
        assertThat(grp01.withPath("fld07.val2").asString(), equalTo("Value #2"));
        assertThat(grp01.withPath("fld08.val2").asString(), equalTo("Value #2"));
        assertThat(grp01.withPath("fld09").asString(), equalTo("30/05/2016"));

        final Map<String, DataBinding> fld10 = grp01.withPath("fld10").asMap();
        assertThat(fld10.get("from").asString(), equalTo("30/05/2018"));
        assertThat(fld10.get("to").asString(), equalTo("30/05/2019"));

        assertThat(grp01.withPath("grp01x1").asMap(), nullValue());

        final List<DataBinding> grp01x1 = grp01.withPath("grp01x1").asList();
        assertThat(grp01x1, hasSize(2));
        assertThat(grp01x1.get(0).withPath("fld01").asString(), equalTo("New Value #01.0"));
        assertThat(grp01x1.get(1).withPath("fld01").asString(), equalTo("New Value #01.1"));

        assertThat(grp01.withPath("grp01x2").asMap(), nullValue());

        final List<DataBinding> grp01x2 = grp01.withPath("grp01x2").asList();
        assertThat(grp01x2, hasSize(2));
        assertThat(grp01x2.get(0).withPath("fld01").asString(), equalTo("New Value #01.0"));
        assertThat(grp01x2.get(1).withPath("fld01").asString(), equalTo("New Value #01.1"));

        assertThat(binding.isArray(), equalTo(false));
        assertThat(binding.isMap(), equalTo(true));

        assertThat(grp01.withPath("fld05").isArray(), equalTo(false));
        assertThat(grp01.withPath("fld05").isMap(), equalTo(false));

        assertThat(grp01.withPath("grp01x1").isArray(), equalTo(true));
        assertThat(grp01.withPath("grp01x1").isMap(), equalTo(false));

        assertThat(grp01.withPath("grp01x2").isArray(), equalTo(true));
        assertThat(grp01.withPath("grp01x2").isMap(), equalTo(false));
    }

}
