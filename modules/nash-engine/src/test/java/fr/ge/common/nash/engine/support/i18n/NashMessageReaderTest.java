/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.support.i18n;

import java.util.Locale;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.adapter.v42_1.FooBarValueAdapter;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.common.support.i18n.MessageFormatter;

/**
 * Tests {@link NashMessageReader}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class NashMessageReaderTest extends AbstractTest {

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        System.out.println("###### testFormat ###### Locale : " + Locale.getDefault());
        System.out.println("###### testFormat ###### TimeZone : " + TimeZone.getDefault().getID());
    }

    /**
     * Tests
     * {@link NashMessageReader#getReader(fr.ge.common.nash.engine.adapter.IValueAdapter, Locale)}
     * .
     */
    @Test
    public void testValueAdapterFormat() {
        final MessageFormatter formatter = NashMessageReader.getReader(new FooBarValueAdapter(), Locale.FRENCH).getFormatter();
        Assert.assertEquals("Ce type de données ne fait rien.", formatter.format("This data type does nothing."));
        Assert.assertEquals("Ce type de données ne fait rien.", formatter.formatNullIfMissing("This data type does nothing."));
    }

    /**
     * Tests
     * {@link NashMessageReader#getReader(fr.ge.common.nash.engine.adapter.IValueAdapter, Locale)}
     * .
     */
    @Test
    public void testValueAdapterNullLocale() {
        final MessageFormatter formatter = NashMessageReader.getReader(new FooBarValueAdapter(), null).getFormatter();
        Assert.assertEquals("Ce type de données ne fait rien.", formatter.format("This data type does nothing."));
        Assert.assertEquals("Ce type de données ne fait rien.", formatter.formatNullIfMissing("This data type does nothing."));
    }

    /**
     * Tests
     * {@link NashMessageReader#getReader(fr.ge.common.nash.engine.adapter.IValueAdapter, Locale)}
     * .
     */
    @Test
    public void testValueAdapterFormatNoBundle() {
        final MessageFormatter formatter = NashMessageReader.getReader(new FooBarValueAdapter(), Locale.ITALIAN).getFormatter();
        Assert.assertEquals("Ce type de données ne fait rien.", formatter.format("This data type does nothing."));
        Assert.assertEquals("Ce type de données ne fait rien.", formatter.formatNullIfMissing("This data type does nothing."));
    }

    /**
     * Tests
     * {@link NashMessageReader#getReader(fr.ge.common.nash.engine.adapter.IValueAdapter, Locale)}
     * .
     */
    @Test
    public void testValueAdapterFormatNoMessage() {
        final MessageFormatter formatter = NashMessageReader.getReader(new FooBarValueAdapter(), Locale.FRENCH).getFormatter();
        Assert.assertEquals("message that does not exist", formatter.format("message that does not exist"));
        Assert.assertEquals(null, formatter.formatNullIfMissing("message that does not exist"));
    }

    /**
     * Tests {@link NashMessageReader#getReader(SpecificationLoader, Locale)}.
     */
    @Test
    public void testSpecLoaderFormat() {
        final byte[] resourceAsBytes = this.resourceAsBytes("specLoaderFormatNoDefault.zip");
        final IProvider provider = new ZipProvider(resourceAsBytes);
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        MessageFormatter formatter = NashMessageReader.getReader(loader, Locale.FRENCH).getFormatter();
        Assert.assertEquals("message qui existe", formatter.format("message that exists"));
        Assert.assertEquals("message qui existe", formatter.formatNullIfMissing("message that exists"));

        formatter = NashMessageReader.getReader(loader, new Locale("es", "ES")).getFormatter();
        Assert.assertEquals("mensaje que existe", formatter.format("message that exists"));
        Assert.assertEquals("mensaje que existe", formatter.formatNullIfMissing("message that exists"));
    }

    /**
     * Tests {@link NashMessageReader#getReader(SpecificationLoader, Locale)}.
     */
    @Test
    public void testSpecLoaderFormatNullLocale() {
        final byte[] resourceAsBytes = this.resourceAsBytes("specLoaderFormatNoDefault.zip");
        final IProvider provider = new ZipProvider(resourceAsBytes);
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final MessageFormatter formatter = NashMessageReader.getReader(loader, null).getFormatter();
        Assert.assertEquals("message qui existe", formatter.format("message that exists"));
        Assert.assertEquals("message qui existe", formatter.formatNullIfMissing("message that exists"));
    }

    /**
     * Tests {@link NashMessageReader#getReader(SpecificationLoader, Locale)}.
     */
    @Test
    public void testSpecLoaderFormatNoBundleNoDefault() {
        final byte[] resourceAsBytes = this.resourceAsBytes("specLoaderFormatNoDefault.zip");
        final IProvider provider = new ZipProvider(resourceAsBytes);
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final MessageFormatter formatter = NashMessageReader.getReader(loader, Locale.ITALIAN).getFormatter();
        Assert.assertEquals("message that exists", formatter.format("message that exists"));
        Assert.assertEquals(null, formatter.formatNullIfMissing("message that exists"));
    }

    /**
     * Tests {@link NashMessageReader#getReader(SpecificationLoader, Locale)}.
     */
    @Test
    public void testSpecLoaderFormatNoBundleDefault() {
        final byte[] resourceAsBytes = this.resourceAsBytes("specLoaderFormatDefault.zip");
        final IProvider provider = new ZipProvider(resourceAsBytes);
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final MessageFormatter formatter = NashMessageReader.getReader(loader, Locale.ITALIAN).getFormatter();
        Assert.assertEquals("message qui existe", formatter.format("message that exists"));
        Assert.assertEquals("message qui existe", formatter.formatNullIfMissing("message that exists"));
    }

    /**
     * Tests {@link NashMessageReader#getReader(SpecificationLoader, Locale)}.
     */
    @Test
    public void testSpecLoaderFormatNoMessage() {
        final byte[] resourceAsBytes = this.resourceAsBytes("specLoaderFormatNoDefault.zip");
        final IProvider provider = new ZipProvider(resourceAsBytes);
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final MessageFormatter formatter = NashMessageReader.getReader(loader, Locale.FRENCH).getFormatter();
        Assert.assertEquals("message that does not exist", formatter.format("message that does not exist"));
        Assert.assertEquals(null, formatter.formatNullIfMissing("message that does not exist"));
    }

}
