/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.extractor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

import fr.ge.common.nash.engine.manager.extractor.IndexedDataModelExtractor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.test.builder.DataElementBuilder;
import fr.ge.common.nash.engine.test.builder.FormSpecificationDataBuilder;
import fr.ge.common.nash.engine.test.builder.GroupElementBuilder;

/**
 * Class IndexedDataModelExtractorTest.
 *
 * @author Christian Cougourdan
 */
public class IndexedDataModelExtractorTest {

    /** extractor. */
    private final IndexedDataModelExtractor extractor = new IndexedDataModelExtractor();

    /**
     * Test no group from specification.
     */
    @Test
    public void testNoGroupFromSpecification() {
        final FormSpecificationData formSpecification = new FormSpecificationDataBuilder("form01").build();

        final Map<String, Object> model = this.extractor.extract(formSpecification);

        assertNotNull(model);
        assertTrue(model.isEmpty());
    }

    /**
     * Test no data from specification.
     */
    @Test
    public void testNoDataFromSpecification() {
        final FormSpecificationData formSpecification = new FormSpecificationDataBuilder("form01").group(new GroupElementBuilder("group01").build()).build();

        final Map<String, Object> model = this.extractor.extract(formSpecification);

        assertNotNull(model);
        assertTrue(model.isEmpty());
    }

    /**
     * Test no data from group.
     */
    @Test
    public void testNoDataFromGroup() {
        final GroupElement groupElement = new GroupElementBuilder().build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertTrue(model.isEmpty());
    }

    /**
     * Test simple path.
     */
    @Test
    public void testSimplePath() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas(new DataElementBuilder("lastname").value("test value").build()).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertEquals(1, model.keySet().size());
        this.assertValue("lastname", model, "test value");
    }

    // @Test
    // public void testComplexPath() {
    // final DataElement dataElement = new
    // DataElementBuilder("personal.address.city").value("city
    // value").build();
    //
    // final Map<String, Object> model = new HashMap<>();
    // extractor.extract(model, "", dataElement);
    //
    // assertEquals(1, model.keySet().size());
    // assertValue("personal.address.city", model, "city value");
    // }

    /**
     * Test multiple path.
     */
    @Test
    public void testMultiplePath() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new DataElementBuilder("personal.address").value("address value").build(), //
                new DataElementBuilder("personal.address.city").value("city value").build() //
        ).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertEquals(2, model.keySet().size());
        this.assertValue("personal.address", model, "address value");
        this.assertValue("personal.address.city", model, "city value");
    }

    /**
     * Test from group.
     */
    @Test
    public void testFromGroup() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new DataElementBuilder("firstname").type("String").value("John").build(), //
                new DataElementBuilder("lastname").type("String").value("Doe").build(), //
                new DataElementBuilder("alias").type("String").build() //
        ).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertEquals(3, model.keySet().size());
        this.assertValue("firstname", model, "John");
        this.assertValue("lastname", model, "Doe");
        this.assertValue("alias", model, null);
    }

    /**
     * Test from spec.
     */
    @Test
    public void testFromSpec() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new DataElementBuilder("firstname").type("String").value("John").build(), //
                new DataElementBuilder("lastname").type("String").value("Doe").build(), //
                new DataElementBuilder("alias").type("String").build() //
        ).build();
        final FormSpecificationData formSpecification = new FormSpecificationDataBuilder("form01").group(groupElement).build();

        final Map<String, Object> model = this.extractor.extract(formSpecification);

        assertNotNull(model);
        assertEquals(3, model.keySet().size());
        this.assertValue("group01.firstname", model, "John");
        this.assertValue("group01.lastname", model, "Doe");
        this.assertValue("group01.alias", model, null);
    }

    /**
     * Assert value.
     *
     * @param name
     *            name
     * @param model
     *            model
     * @param expected
     *            expected
     */
    public void assertValue(final String name, final Map<String, Object> model, final Object expected) {
        final Object actual = model.get(name);
        assertNotNull("Attribute \"" + name + "\" expected", actual);

        if (null == expected) {
            assertThat(actual, hasProperty("value", nullValue()));
        } else {
            assertThat(actual, //
                    hasProperty("value", //
                            hasProperty("content", //
                                    equalTo(expected) //
                            ) //
                    ) //
            );
        }
    }

}
