/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script.expression;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

public class ScriptExpressionParserTest {

    /** parser. */
    private ScriptExpressionParser parser;

    /**
     * Set up.
     */
    @Before
    public void setUp() {
        this.parser = new ScriptExpressionParser();
    }

    /**
     * Test operator and.
     */
    @Test
    public void testOperatorAnd() {
        final ScriptExpression expr = this.parser.parse("true and false");
        assertEquals("true && false", expr.getExpression());
    }

    /**
     * Test operator or.
     */
    @Test
    public void testOperatorOr() {
        ScriptExpression expr = this.parser.parse("true or false");
        assertEquals("true || false", expr.getExpression());

        expr = this.parser.parse("trueor and false");
        assertEquals("trueor && false", expr.getExpression());
    }

    /**
     * Test operator not.
     */
    @Test
    public void testOperatorNot() {
        ScriptExpression expr = this.parser.parse("true or not false");
        assertEquals("true || ! false", expr.getExpression());

        expr = this.parser.parse("truenot or false");
        assertEquals("truenot || false", expr.getExpression());

        expr = this.parser.parse("not false");
        assertEquals("! false", expr.getExpression());

        expr = this.parser.parse("not(false)");
        assertEquals("!(false)", expr.getExpression());
    }

    /**
     * Test global variable.
     */
    @Test
    public void testGlobalVariable() {
        final ScriptExpression expr = this.parser.parse("$form.address.city");
        assertEquals("_step.address.city", expr.getExpression());
    }

    /**
     * Test local variable.
     */
    @Test
    public void testLocalVariable() {
        final ScriptExpression expr = this.parser.parse("$current.address.city");
        assertEquals("_page.address.city", expr.getExpression());
    }

    /**
     * Test simple expression.
     */
    @Test
    public void testSimpleExpression() {
        final ScriptExpression expr = this.parser.parse("$address.city");
        assertEquals("_record.address.city", expr.getExpression());
    }

    /**
     * Test variable with default scope.
     */
    @Test
    public void testVariableWithLocalScope() {
        final ScriptExpression expr = this.parser.parse("$address.city", true);
        assertEquals("_page.address.city", expr.getExpression());
    }

    /**
     * Test method.
     */
    @Test
    public void testMethod() {
        final ScriptExpression expr = this.parser.parse("$form.address.isOk()");
        assertEquals("_step.address.isOk()", expr.getExpression());
    }

    /**
     * Test dependencies.
     */
    @Test
    public void testDependencies() {
        final ScriptExpression expr = this.parser.parse("$address.city != $form.address.zip");
        final Collection<String> dependencies = expr.getDependencies();

        assertNotNull(dependencies);
        assertEquals(2, dependencies.size());
        assertTrue(dependencies.contains("_record.address.city"));
        assertTrue(dependencies.contains("_step.address.zip"));
    }

    // @Test
    // public void testParseError() {
    // final IParser parser = new LocalExpressionParser(new IParser() {
    //
    // @Override
    // public ScriptExpression parseExpression(final String expressionString) {
    // throw new ParseException(null, expressionString, "error");
    // }
    //
    // @Override
    // public void init() {
    // // Nothing to do
    // }
    // });
    // parser.init();
    //
    // final String originalExpr = "$address.city xxx $form.address.zip";
    // final String expandedExpr = "error";
    //
    // try {
    // parser.parse(originalExpr);
    // fail("ParseException expected");
    // } catch (final ParseException ex) {
    // assertEquals(String.format("Error while parsing expression : \"%s\" (was
    // expanded to \"%s\").", originalExpr, expandedExpr), ex.getMessage());
    // assertEquals(originalExpr, ex.getOriginalExpression());
    // assertEquals(expandedExpr, ex.getExpression());
    // }
    //
    // }

}
