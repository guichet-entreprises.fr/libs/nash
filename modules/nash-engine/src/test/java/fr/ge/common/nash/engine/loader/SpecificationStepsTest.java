package fr.ge.common.nash.engine.loader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * 
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class SpecificationStepsTest extends AbstractTest {

    @Autowired
    private Engine engine;

    @Test
    public void testVoid() throws Exception {
        IProvider provider = new ZipProvider(resourceAsBytes("spec.zip"));
        SpecificationLoader loader = engine.loader(provider);

        Collection<String> resources = loader.stepsMgr().findResources(42);
        assertThat(resources, hasSize(0));
    }

    @Test
    public void testSimpleByIndex() throws Exception {
        IProvider provider = new ZipProvider(resourceAsBytes("spec.zip"));
        SpecificationLoader loader = engine.loader(provider);

        Collection<String> resources = loader.stepsMgr().findResources(0);
        assertThat(resources, contains(equalTo("step01.xml")));
    }

    public void testSimpleById() throws Exception {
        IProvider provider = new ZipProvider(resourceAsBytes("spec.zip"));
        SpecificationLoader loader = engine.loader(provider);

        Collection<String> resources = loader.stepsMgr().findResources("step02");
        assertThat(resources, contains(equalTo("step02/data.xml")));
    }

    public void testComplexInFolder() throws Exception {
        IProvider provider = new ZipProvider(resourceAsBytes("spec.zip"));
        SpecificationLoader loader = engine.loader(provider);

        Collection<String> expected = Arrays.asList( //
                "step03/post.xml", //
                "step03/pre.xml", //
                "step03/pre01.js", //
                "step03/pre02.js", //
                "step03/pre03.js", //
                "step03/pre04.js" //
        );

        Collection<String> resources = loader.stepsMgr().findResources("step03");
        assertThat(resources, hasSize(expected.size()));
        for (String exp : expected) {
            assertThat(String.format("%s is missing in step resource list", exp), resources.contains(exp));
        }
    }

    @Test
    public void testComplexAtRoot() throws Exception {
        IProvider provider = new ZipProvider(resourceAsBytes("spec.zip"));
        SpecificationLoader loader = engine.loader(provider);

        Collection<String> expected = Arrays.asList( //
                "step04.xml", //
                "pre04.xml", //
                "pre04-1.xml", //
                "pre04-2.xml", //
                "pre04-3.xml", //
                "test.xml", //
                "pre04-2.js", //
                "step04-pre-2.xml", //
                "pre04-3.js", //
                "step04-pre-3.xml", //
                "pre04-4.js", //
                "pre04-4.xml", //
                "step04-pre-4.xml", //
                "post04.xml", //
                "step04-out.xml", //
                "post04-1.xml", //
                "post04-2.xml", //
                "post04-3.xml", //
                "post04-2.js", //
                "step04-post-2.xml", //
                "post04-3.js", //
                "step04-post-3.xml", //
                "post04-4.js", //
                "post04-4.xml", //
                "step04-post-4.xml" //
        );

        Collection<String> resources = loader.stepsMgr().findResources("step04");
        assertThat(resources, hasSize(expected.size()));
        for (String exp : expected) {
            assertThat(String.format("%s is missing in step resource list", exp), resources.contains(exp));
        }
    }

}
