/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.adapter.v1_2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hamcrest.Matcher;
import org.springframework.mock.web.MockHttpServletRequest;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;

/**
 * @author Christian Cougourdan
 *
 */
public class PasswordBase64ValueAdapterTest extends AbstractValueAdapterTest<String> {

    private static final String RAW_PASSWORD = "t3st p455w0rd";

    protected void assertAsTypeSpecificObject(final String actual) {
        assertEquals(Base64.getEncoder().encodeToString(RAW_PASSWORD.getBytes()), actual);
    }

    protected void assertAsValueElement(final Object actual) {
        this.assertAsTypeSpecificObject((String) actual);
    }

    protected void assertAsString(final String actual) {
        assertEquals(RAW_PASSWORD, actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IValueAdapter<String> type() {
        return new PasswordBase64ValueAdapter();
    }

    /**
     * {@inheritDoc}
     */
    protected String valueAsTypeSpecificObject() {
        return RAW_PASSWORD;
    }

    /**
     * {@inheritDoc}
     */
    protected Matcher<? super String> expectedAsTypeSpecificObject() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    protected ValueElement valueAsValueElement() {
        return new ValueElement(Base64.getEncoder().encodeToString(RAW_PASSWORD.getBytes()));
    }

    /**
     * {@inheritDoc}
     */
    protected Matcher<? super Object> expectedAsValueElement() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    protected HttpServletRequest valueAsHttpRequest() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("", new String[] { RAW_PASSWORD });
        return request;
    }

    /**
     * {@inheritDoc}
     */
    protected String expectedAsString() {
        return RAW_PASSWORD;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        return Collections.singletonMap("", new String[] { RAW_PASSWORD });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequest(final String actual, final DataElement dataElement) {
        assertEquals(Base64.getEncoder().encodeToString(RAW_PASSWORD.getBytes()), actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        return Collections.singletonMap("", new String[] { "" });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequestEmpty(final String actual, final DataElement dataElement) {
        assertEquals(Base64.getEncoder().encodeToString("".getBytes()), actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String buildFromObject() {
        return RAW_PASSWORD;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObject(final String actual, final DataElement dataElement) {
        assertEquals(Base64.getEncoder().encodeToString(RAW_PASSWORD.getBytes()), actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String buildFromObjectEmpty() {
        return "";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObjectEmpty(final String actual, final DataElement dataElement) {
        assertEquals(Base64.getEncoder().encodeToString("".getBytes()), actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXml() {
        return String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), Base64.getEncoder().encodeToString(RAW_PASSWORD.getBytes())).getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXml(final String actual, final DataElement dataElement) {
        assertEquals(Base64.getEncoder().encodeToString(RAW_PASSWORD.getBytes()), actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXmlEmpty() {
        return String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), Base64.getEncoder().encodeToString("".getBytes())).getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlEmpty(final String actual, final DataElement dataElement) {
        assertEquals(null, actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlNone(final String actual, final DataElement dataElement) {
        assertNull(actual);
    }

}
