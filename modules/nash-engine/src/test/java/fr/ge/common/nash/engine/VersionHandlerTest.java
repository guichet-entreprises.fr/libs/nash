/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import fr.ge.common.nash.engine.VersionHandler;

/**
 * Class VersionHandlerTest.
 *
 * @author atuffrea
 */
public class VersionHandlerTest {

    /** The Constant XML_SOURCE_OK. */
    private static final String XML_SOURCE_OK = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<description xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" xsi:schemaLocation=\"http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd\">\n    <title>Test Title</title>\n</description>\n";

    /** The Constant XML_SOURCE_WITH_INVALID_NS. */
    private static final String XML_SOURCE_WITH_INVALID_NS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<description xmlns=\"http://formmanager/no_version/description\" version=\"1.0\">\n    <title>Test Title</title>\n</description>\n";

    /** The Constant XML_SOURCE_WITH_VALID_NS. */
    private static final String XML_SOURCE_WITH_VALID_NS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<description test=\"test\" xmlns=\"http://formmanager/1.2.2/description\" anotherTest=\"anotherTest\">\n    <title>Test Title</title>\n</description>\n";

    /** The Constant XML_SOURCE_WITH_NO_NS_IN_ROOT. */
    private static final String XML_SOURCE_WITH_NO_NS_IN_ROOT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<description>\n    <title xmlns=\"http://formmanager/1.2.2/description\">Test Title</title>\n</description>\n";

    /**
     * Test version.
     */
    @Test
    public void testVersion() {
        assertNull(VersionHandler.versionPackage("<test xmlns=\"http://www.ge.fr/schema/unknown/form-manager\"></test>".getBytes(), "test"));
        assertEquals("fr/gouv/ge/form/engine/v1_1", VersionHandler.versionPackage("<test xmlns=\"http://www.ge.fr/schema/1.1/form-manager\"></test>".getBytes(), "test"));
        assertEquals("fr/gouv/ge/form/engine/v1_2", VersionHandler.versionPackage("<test xmlns=\"http://www.ge.fr/schema/1.2/form-manager\"></test>".getBytes(), "test"));
    }

    /**
     * Test get version null01.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetVersionNull01() throws Exception {
        assertNull(VersionHandler.getVersion(XML_SOURCE_OK.getBytes(), "description"));
    }

    /**
     * Test get version null02.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetVersionNull02() throws Exception {
        assertNull(VersionHandler.getVersion(XML_SOURCE_WITH_INVALID_NS.getBytes(), "description"));
    }

    /**
     * Test get version null03.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetVersionNull03() throws Exception {
        assertNull(VersionHandler.getVersion(XML_SOURCE_WITH_NO_NS_IN_ROOT.getBytes(), "description"));
    }

    /**
     * Test get version ok.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetVersionOK() throws Exception {
        assertEquals("1.2.2", VersionHandler.getVersion(XML_SOURCE_WITH_VALID_NS.getBytes(), "description"));
    }

    /**
     * Test name space pattern.
     *
     * @throws IllegalArgumentException
     *             the illegal argument exception
     * @throws IllegalAccessException
     *             the illegal access exception
     * @throws NoSuchFieldException
     *             the no such field exception
     * @throws SecurityException
     *             the security exception
     * @throws NoSuchMethodException
     *             the no such method exception
     * @throws InvocationTargetException
     *             the invocation target exception
     */
    @Test
    public void testNameSpacePattern() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException, NoSuchMethodException, InvocationTargetException {
        assertFalse(this.getPattern("x").matcher("").find());
        assertFalse(this.getPattern("x").matcher(" ").find());
        assertFalse(this.getPattern("x").matcher("<xmlns=\"test\"/>").find());
        assertFalse(this.getPattern("x").matcher("< xmlns=\"test\"/>").find());
        assertFalse(this.getPattern("x").matcher("<x xmlns\"test\"/>").find());
        assertFalse(this.getPattern("x").matcher("<x xmlns=test\"/>").find());
        assertFalse(this.getPattern("x").matcher("<x xmlns=\"test/>").find());
        assertFalse(this.getPattern("x").matcher("<x =\"test\"/>").find());
        assertFalse(this.getPattern("x").matcher("<x any=\"test\"/>").find());

        final Matcher m = this.getPattern("x").matcher("<x xmlns=\"test\"/>");
        assertTrue(m.find());
        assertEquals("test", m.group(1));
        assertTrue(this.getPattern("x").matcher(" any <x xmlns = \" test \" /> any ").find());
    }

    /**
     * Test get ns pattern.
     *
     * @throws IllegalAccessException
     *             the illegal access exception
     * @throws IllegalArgumentException
     *             the illegal argument exception
     * @throws InvocationTargetException
     *             the invocation target exception
     * @throws NoSuchMethodException
     *             the no such method exception
     * @throws SecurityException
     *             the security exception
     */
    @Test
    public void testGetNsPattern() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        assertEquals("<description[^>]+xmlns\\s*=\\s*\"([^\"]+)\"[^>]*>", this.getPattern("description").pattern());

    }

    /**
     * Gets the pattern.
     *
     * @param elementName
     *            the element name
     * @return the pattern used by JaxbFactoryImpl to get xml nameSpace.
     * @throws NoSuchMethodException
     *             the no such method exception
     * @throws IllegalAccessException
     *             the illegal access exception
     * @throws InvocationTargetException
     *             the invocation target exception
     */
    private Pattern getPattern(final String elementName) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        final Method m = VersionHandler.class.getDeclaredMethod("getNsPattern", String.class);
        m.setAccessible(true);
        final Pattern pattern = (Pattern) m.invoke(null, elementName);
        return pattern;
    }

}
