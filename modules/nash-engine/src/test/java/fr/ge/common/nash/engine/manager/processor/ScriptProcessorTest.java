/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.bridge.ServiceProcessorBridgeTest.ITestService;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.test.builder.ProcessElementBuilder;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class ScriptProcessorTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class ScriptProcessorTest extends AbstractTest {

    /** The Constant ENDPOINT. */
    protected static final String ENDPOINT = "http://localhost:8888/test";

    /** The server. */
    protected Server server;

    /** The service. */
    protected ITestService service;

    /** The context. */
    protected Configuration cfg;

    /** provider. */
    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.asBytes("description.xml")).thenReturn(this.resourceAsBytes("description.xml"));
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
        when(this.provider.getAbsolutePath("data-generated.xml")).thenReturn("data-generated.xml");

        this.service = mock(ITestService.class);

        final Properties properties = new Properties();
        properties.put("ws.external.url", ENDPOINT);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.service);
        serverFactory.setProvider(jsonProvider);
        this.server = serverFactory.create();

        this.cfg = new Configuration(properties);
    }

    @After
    public void tearDown() throws Exception {
        this.server.stop();
        this.server.destroy();
    }

    /**
     * Test meta.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMeta() throws Exception {
        final FormSpecificationData actual = this.process("meta.js");

        assertThat(actual, //
                allOf( //
                        hasProperty("description", nullValue()), //
                        hasProperty("help", nullValue()), //
                        hasProperty("default", nullValue()), //
                        hasProperty("groups", hasSize(2)) //
                ) //
        );
        assertThat(actual, notNullValue());
    }

    /**
     * Process.
     *
     * @param scriptFilename
     *            script filename
     * @return form specification data
     */
    private FormSpecificationData process(final String scriptFilename) {
        final ProcessElement processElement = new ProcessElementBuilder() //
                .id("review") //
                .type("script") //
                .output("data-generated.xml") //
                .value(this.resourceAsString(scriptFilename)) //
                .build();

        final Map<String, Object> model = new HashMap<>();
        model.put("lastname", "Doe");
        model.put("firstname", "John");

        final ScriptProcessor processor = new ScriptProcessor() //
                .setContext(this.applicationContext.getBean(SpecificationLoader.class, this.cfg, this.provider).buildEngineContext().target(processElement));

        return processor.execute(model).getContent();
    }

    /**
     * Test embedded font.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    @Ignore
    public void testEmbeddedFont() throws Exception {
        final String basePath = "D:/Users/ccougour/Downloads/2017-06-SHS-FAT-38/3-review/models";
        // final PDDocument doc = PDDocument.load(Paths.get(basePath +
        // "/GC-EAU_LAI_CS_NOV_16ab GE.pdf").toFile());
        final PDDocument doc = PDDocument.load(Paths.get(basePath + "/GC-EAU_LAI_CS_NOV_16ab GE.pdf").toFile());
        final PDPageTree pages = doc.getPages();
        PDFont font = null;
        for (final PDPage page : pages) {
            final PDResources resources = page.getResources();
            for (final COSName fontName : resources.getFontNames()) {
                font = resources.getFont(fontName);
                System.out.println(font.getName() + " --> " + font.isEmbedded());
            }
        }

        // final PDPage page = doc.getPage(0);
        // final PDPageContentStream stream = new PDPageContentStream(doc, page,
        // PDPageContentStream.AppendMode.APPEND, false);
        // stream.beginText();
        // stream.setFont(PDType1Font.HELVETICA, 12);
        // stream.showText("Hello");
        // stream.showText("هيهههيغيعغي");
        // stream.endText();
        // stream.close();

        // final PDFont customFont = PDTrueTypeFont.load(doc,
        // Paths.get(basePath).resolve("WildernessTypeface-Regular.ttf").toFile(),
        // WinAnsiEncoding.INSTANCE);
        // final String fontPath = "NotoNaskhArabic-Regular.ttf";
        // final PDFont customFont = PDTrueTypeFont.load(doc,
        // Paths.get(basePath).resolve(fontPath).toFile(),
        // WinAnsiEncoding.INSTANCE);
        // final PDFont customFont = PDType0Font.load(doc,
        // Paths.get(basePath).resolve(fontPath).toFile());

        final PDAcroForm form = doc.getDocumentCatalog().getAcroForm();
        // final COSName fontName = form.getDefaultResources().add(customFont);
        // form.getField("NOM_EXPED").setValue("Ou pas");

        final PDField field = form.getField("ADR_EXPED");
        // field.getCOSObject().setString(COSName.DA, "/" + fontName.getName() +
        // " 10 Tf 0 g");
        field.setValue("عادل");
        // field.setValue("Christian & Adil & Mina");

        doc.save(basePath + "/saved.pdf");
    }

    /**
     * Test conftype.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testConftype() throws Exception {
        final FormSpecificationData actual = this.process("conftype.js");
        assertThat(actual, //
                allOf( //
                        hasProperty("description", nullValue()), //
                        hasProperty("help", nullValue()), //
                        hasProperty("default", nullValue()), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );
        assertThat(actual, notNullValue());
        final GroupElement groupActual = actual.getGroups().get(0);
        assertThat(groupActual, notNullValue());
        assertThat(groupActual.getData().size(), equalTo(1));

        final DataElement actualDataElement = (DataElement) groupActual.getData().get(0);
        assertThat(actualDataElement.getConftype(), notNullValue());
        assertThat(actualDataElement.getConftype().getReferentialElements(), notNullValue());
        assertThat(actualDataElement.getConftype().getReferentialElements().size(), equalTo(1));
        assertThat(actualDataElement.getConftype().getReferentialElements().get(0).getReferentialTextElements().size(), equalTo(3));
    }

    /**
     * Test trigger.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testTrigger() throws Exception {
        final FormSpecificationData actual = this.process("trigger.js");
        assertThat(actual, //
                allOf( //
                        hasProperty("description", nullValue()), //
                        hasProperty("help", nullValue()), //
                        hasProperty("default", nullValue()), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );
        assertThat(actual, notNullValue());
        final GroupElement groupActual = actual.getGroups().get(0);
        assertThat(groupActual, notNullValue());
        assertThat(groupActual.getData().size(), equalTo(1));

        final DataElement actualDataElement = (DataElement) groupActual.getData().get(0);
        assertThat(actualDataElement.getTrigger(), notNullValue());
    }

    /**
     * Test trigger.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPushingBoxWithoutAnyConfig() throws Exception {
        final Properties properties = new Properties();
        properties.put("pushingbox.devid", "test");
        properties.put("pushingbox.baseUrl", "test");
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("pushingbox.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);
        final StepElement step = loader.stepsMgr().find(0);
        final IProcessResult<?> postResult = loader.processes().postExecute(step);

        final FormSpecificationData spec = (FormSpecificationData) postResult.getContent();
        assertNotNull(spec);
        assertNotNull(spec.getGroups());
        assertThat(spec.getGroups(), hasSize(1));
        assertNotNull(spec.getGroups().get(0));
        assertThat(spec.getGroups().get(0).getData(), hasSize(1));

        final DataElement dataElement = (DataElement) spec.getGroups().get(0).getData().get(0);
        assertEquals("no", dataElement.getValue().getContent());
    }

    /**
     * Test trigger.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPushingBoxWithConfig() throws Exception {
        final Properties properties = new Properties();
        properties.put("pushingbox.devid", "test");
        properties.put("pushingbox.baseUrl", "test");
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("pushingbox.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);
        final StepElement step = loader.stepsMgr().find(0);
        final IProcessResult<?> postResult = loader.processes().postExecute(step);

        final FormSpecificationData spec = (FormSpecificationData) postResult.getContent();
        assertNotNull(spec);
        assertNotNull(spec.getGroups());
        assertThat(spec.getGroups(), hasSize(1));
        assertNotNull(spec.getGroups().get(0));
        assertThat(spec.getGroups().get(0).getData(), hasSize(1));

        final DataElement dataElement = (DataElement) spec.getGroups().get(0).getData().get(0);
        assertEquals("no", dataElement.getValue().getContent());
    }

    @Test
    public void testService() throws Exception {
        when(this.service.download()).thenReturn(Response.serverError().build());

        try {
            this.process("service.js");
            fail("ServiceException expected");
        } catch (final Exception ex) {
            LoggerFactory.getLogger(this.getClass()).warn("Exception", ex);
        }
    }

    /**
     * Test trigger.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testConftypeParam() throws Exception {
        final FormSpecificationData actual = this.process("conftype-param.js");
        assertThat(actual, //
                allOf( //
                        hasProperty("description", nullValue()), //
                        hasProperty("help", nullValue()), //
                        hasProperty("default", nullValue()), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );
        assertThat(actual, notNullValue());
        final GroupElement groupActual = actual.getGroups().get(0);
        assertThat(groupActual, notNullValue());
        assertThat(groupActual.getData().size(), equalTo(1));

        final DataElement actualDataElement = (DataElement) groupActual.getData().get(0);
        assertThat(actualDataElement.getConftype(), notNullValue());
        assertThat(actualDataElement.getConftype().getParameters(), notNullValue());
    }

    @Test
    public void testTemplates() throws Exception {
        final FormSpecificationData actual = this.process("templates.js");

        assertThat(actual, //
                allOf( //
                        hasProperty("description", nullValue()), //
                        hasProperty("help", nullValue()), //
                        hasProperty("default", nullValue()), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

    }

}
