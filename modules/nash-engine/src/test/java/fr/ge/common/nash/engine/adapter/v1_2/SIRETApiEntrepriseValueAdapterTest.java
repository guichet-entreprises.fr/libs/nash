/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import fr.ge.common.nash.engine.adapter.v1_2.value.ExternalData;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.nash.engine.test.builder.DataElementBuilder;

/**
 * Tests {@link SIRETApiEntrepriseValueAdapter}.
 *
 * @author jpauchet
 */
public class SIRETApiEntrepriseValueAdapterTest {

    /** Type. */
    private final SIRETApiEntrepriseValueAdapter type = new SIRETApiEntrepriseValueAdapter();

    /**
     * Test from xml bad format.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromXmlBadFormat() throws Exception {
        final DataElement dataElement = new DataElementBuilder().value("oups").build();
        final ExternalData actual = this.type.get(null, dataElement);
        assertNull(actual);
    }

    /**
     * Test from xml null.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromXmlNull() throws Exception {
        final DataElement dataElement = new DataElementBuilder().value((String) null).build();
        final ExternalData actual = this.type.get(null, dataElement);
        assertNull(actual);
    }

    /**
     * Test from xml empty.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromXmlEmpty() throws Exception {
        final DataElement dataElement = new DataElementBuilder().value("").build();
        final ExternalData actual = this.type.get(null, dataElement);
        assertNull(actual);
    }

    /**
     * Test from xml.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromXml() throws Exception {
        final DataElement dataElement = new DataElementBuilder().value( //
                new ListValueElement( //
                        new TextValueElement("val", "12345678912345"), //
                        new TextValueElement("data", "{'data':null}") //
                ) //
        ).build();

        final ExternalData actual = this.type.get(null, dataElement);

        assertThat(actual.getVal(), equalTo("12345678912345"));
        assertThat(actual.getData(), equalTo("{'data':null}"));
        assertEquals(actual.toString(), "12345678912345");
    }

    /**
     * Test to xml.
     */
    @Test
    public void testToXml() {
        final ExternalData value = new ExternalData( //
                "12345678912345", //
                "{'data':null}" //
        );

        final DataElement dataElement = new DataElement();

        final Object actual = this.type.set(dataElement, value);

        assertThat(actual, //
                allOf( //
                        hasProperty("val", equalTo("12345678912345")), //
                        hasProperty("data", equalTo("{'data':null}")) //
                ) //
        );
    }

    /**
     * Test to xml null.
     */
    @Test
    public void testToXmlNull() {
        final DataElement dataElement = new DataElement();

        final Object actual = this.type.set(dataElement, null);

        assertThat(actual, nullValue());
    }

    /**
     * Test to xml null from.
     */
    @Test
    public void testToXmlNullFrom() {
        final ExternalData value = new ExternalData( //
                null, //
                "{'data':null}" //
        );

        final DataElement dataElement = new DataElement();

        final Object actual = this.type.set(dataElement, value);

        assertThat(actual, hasProperty("data", equalTo("{'data':null}")));
    }

    /**
     * Test to xml null to.
     */
    @Test
    public void testToXmlNullTo() {
        final ExternalData value = new ExternalData( //
                "12345678912345", //
                null //
        );

        final DataElement dataElement = new DataElement();

        final Object actual = this.type.set(dataElement, value);

        assertThat(actual, hasProperty("val", equalTo("12345678912345")));
    }

    /**
     * Test to value element from object.
     */
    @Test
    public void testToValueElementFromObject() {
        final ExternalData value = new ExternalData( //
                "12345678912345", //
                "{'data':null}" //
        );

        final ValueElement actual = this.type.toValueElement(null, null, value);

        assertThat(((ListValueElement) actual.getContent()).getElements(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("val")), //
                                        hasProperty("value", equalTo("12345678912345")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("data")), //
                                        hasProperty("value", equalTo("{'data':null}")) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test to value element from data.
     */
    @Test
    public void testToValueElementFromData() {
        final Map<String, String[]> map = new HashMap<>();
        map.put("val", new String[] { "12345678912345" });
        map.put("data", new String[] { "{'data':null}" });

        final ValueElement actual = this.type.toValueElement(null, null, new FormContentData(map));

        assertThat(actual, //
                hasProperty("content", //
                        hasProperty("elements", //
                                contains( //
                                        Arrays.asList( //
                                                allOf( //
                                                        hasProperty("id", equalTo("val")), //
                                                        hasProperty("value", equalTo("12345678912345")) //
                                                ), //
                                                allOf( //
                                                        hasProperty("id", equalTo("data")), //
                                                        hasProperty("value", equalTo("{'data':null}")) //
                                                ) //
                                        ) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test to value element from empty data.
     */
    @Test
    public void testToValueElementFromEmptyData() {
        final Map<String, String[]> map = new HashMap<>();
        map.put("val", new String[] { "" });
        map.put("data", new String[] { "" });

        final ValueElement actual = this.type.toValueElement(null, null, new FormContentData(map));

        assertThat(actual, nullValue());
    }

}
