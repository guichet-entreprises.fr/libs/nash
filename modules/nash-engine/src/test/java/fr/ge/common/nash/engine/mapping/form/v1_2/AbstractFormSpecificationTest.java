/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import static org.hamcrest.MatcherAssert.assertThat;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.loader.LocalEntityResolver;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class AbstractFormSpecificationTest.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            type générique
 */
public abstract class AbstractFormSpecificationTest<T> extends AbstractTest {

    /** logger. */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Test validate.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testValidate() throws Exception {
        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(true);
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage", "http://www.w3.org/2001/XMLSchema");

        final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setEntityResolver(new LocalEntityResolver());

        documentBuilder.parse(this.getClass().getResourceAsStream(this.resourceName("input.xml")));
    }

    /**
     * Test load.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoad() throws Exception {
        final byte[] asBytes = this.resourceAsBytes("input.xml");

        final T actual = JaxbFactoryImpl.instance().unmarshal(asBytes, this.getSpecClass());

        assertThat(actual, this.matcher());
    }

    /**
     * Test write.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testWrite() throws Exception {
        final String expected = this.resourceAsString("expected.xml");
        final byte[] asBytes = this.resourceAsBytes("input.xml");

        final T bean = JaxbFactoryImpl.instance().unmarshal(asBytes, this.getSpecClass());

        try {
            bean.getClass().getMethod("setAnything", List.class).invoke(bean, Collections.emptyList());
        } catch (final Exception ex) {
            this.logger.info("No [anything] property", ex);
        }

        final byte[] actual = JaxbFactoryImpl.instance().asByteArray(bean, this.getCssReference());

        Assert.assertEquals(expected, new String(actual, StandardCharsets.UTF_8));
    }

    /**
     * Getter on the spec class.
     *
     * @return spec class
     */
    protected abstract Class<T> getSpecClass();

    /**
     * Getter on the css reference.
     *
     * @return css reference
     */
    protected abstract String getCssReference();

    /**
     * Matcher.
     *
     * @return matcher
     */
    protected abstract Matcher<T> matcher();

}
