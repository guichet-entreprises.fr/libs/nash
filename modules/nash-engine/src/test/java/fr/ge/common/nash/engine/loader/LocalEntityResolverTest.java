/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.loader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.loader.LocalEntityResolver;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;

/**
 * Class LocalEntityResolverTest.
 *
 * @author Christian Cougourdan
 */
public class LocalEntityResolverTest {

    /**
     * Test existing xsd.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testExistingXsd() throws Exception {
        final EntityResolver entityResolver = new LocalEntityResolver();
        final InputSource source = entityResolver.resolveEntity(null, "http://www.ge.fr/schema/form-manager-1.2.xsd");

        assertNotNull(source);
        assertEquals(Thread.currentThread().getContextClassLoader().getResource(FormSpecificationDescription.class.getPackage().getName().replace('.', '/') + "/form-manager-1.2.xsd").toString(),
                source.getSystemId());
    }

    /**
     * Test unknown xsd.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUnknownXsd() throws Exception {
        final EntityResolver entityResolver = new LocalEntityResolver();
        try {
            entityResolver.resolveEntity(null, "http://www.ge.fr/schema/unknown-1.2.xsd");
            fail("A SAXException must be thrown on unknown XSD");
        } catch (final SAXException ex) {
            assertEquals("Unknown schema URL : \"http://www.ge.fr/schema/unknown-1.2.xsd\"", ex.getMessage());
        } catch (final Exception ex) {
            fail("A SAXException must be thrown on unknown XSD");
        }
    }

    /**
     * Test load bad schemas.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadBadSchemas() throws Exception {
        final LocalEntityResolver entityResolver = new LocalEntityResolver();
        try {
            entityResolver.init("/META-INF/unknown.schemas");
            fail("A TechnicalException must be thrown on schemas file");
        } catch (final TechnicalException ex) {
            assertEquals("Try loading schemas from \"/META-INF/unknown.schemas\" : resource not found", ex.getMessage());
        } catch (final Exception ex) {
            fail("A TechnicalException must be thrown on schemas file, not \"" + ex.getClass().getName() + "\"");
        }
    }

}
