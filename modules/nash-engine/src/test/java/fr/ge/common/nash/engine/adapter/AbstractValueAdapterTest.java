/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.SpecificationLoaderMock;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.ResourceUtil;

/**
 * @author Christian Cougourdan
 */
public abstract class AbstractValueAdapterTest<T> {

    protected static final String DATA_ELEMENT_EMPTY_AS_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" //
            + "<data xmlns=\"http://www.ge.fr/schema/1.2/form-manager\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"grp01.fld01\" label=\"...\" type=\"%s\" xsi:schemaLocation=\"http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd\"/>";

    protected static final String DATA_ELEMENT_FILLED_AS_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" //
            + "<data xmlns=\"http://www.ge.fr/schema/1.2/form-manager\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"grp01.fld01\" label=\"...\" type=\"%s\" xsi:schemaLocation=\"http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd\">\n" //
            + "    <value>%s</value>\n" //
            + "</data>";

    protected abstract String version();

    abstract protected IValueAdapter<T> type();

    @Test
    public void testHasDescription() throws Exception {
        assertTrue(StringUtils.isNotEmpty(this.type().description()));
    }

    @Test
    public void testHasTemplate() throws Exception {
        final String templateName = this.type().template();
        final String resourceAsString = ResourceUtil.resourceAsString(String.format("/%s.html", templateName), this.type().getClass());
        assertTrue(StringUtils.isNotEmpty(resourceAsString));
    }

    protected abstract Map<String, String[]> buildHttpRequestModel();

    protected abstract void assertFromHttpRequest(final T actual, final DataElement dataElement);

    @Ignore
    @Test
    public final void testFromHttpRequest() throws Exception {
        this.executeTestFromHttpRequest(this::buildHttpRequestModel, this::assertFromHttpRequest);
    }

    protected abstract Map<String, String[]> buildHttpRequestModelEmpty();

    protected abstract void assertFromHttpRequestEmpty(final T actual, final DataElement dataElement);

    @Test
    public final void testFromHttpRequestEmpty() throws Exception {
        this.executeTestFromHttpRequest(this::buildHttpRequestModelEmpty, this::assertFromHttpRequestEmpty);
    }

    protected Map<String, String[]> buildHttpRequestModelNone() {
        return Collections.emptyMap();
    }

    protected void assertFromHttpRequestNone(final T actual, final DataElement dataElement) {
        assertNull(actual);
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Test
    public final void testFromHttpRequestNone() throws Exception {
        this.executeTestFromHttpRequest(this::buildHttpRequestModelNone, this::assertFromHttpRequestNone);
    }

    protected final void executeTestFromHttpRequest(final Supplier<Map<String, String[]>> model, final BiConsumer<T, DataElement> assertion) {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        model.get().forEach((k, v) -> request.addParameter("grp01.fld01" + (StringUtils.isEmpty(k) ? "" : "." + k), v));

        final DataElement dataElement = this.buildDataElement();
        final T actual = this.type().set(this.buildLoader(), dataElement, "grp01.fld01", request);

        assertion.accept(actual, dataElement);
    }

    protected abstract T buildFromObject();

    protected abstract void assertFromObject(final T actual, final DataElement dataElement);

    @Ignore
    @Test
    public void testFromObject() throws Exception {
        final DataElement dataElement = this.buildDataElement();
        final T actual = this.type().set(this.buildLoader(), null, dataElement, this.buildFromObject());
        this.assertFromObject(actual, dataElement);
    }

    protected abstract T buildFromObjectEmpty();

    protected abstract void assertFromObjectEmpty(final T actual, final DataElement dataElement);

    @Test
    public void testFromObjectEmpty() throws Exception {
        final DataElement dataElement = this.buildDataElement();
        final T actual = this.type().set(this.buildLoader(), null, dataElement, this.buildFromObjectEmpty());
        this.assertFromObjectEmpty(actual, dataElement);
    }

    protected T buildFromObjectNone() {
        return null;
    }

    protected void assertFromObjectNone(final T actual, final DataElement dataElement) {
        assertNull(actual);
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Test
    public void testFromObjectNone() throws Exception {
        final DataElement dataElement = this.buildDataElement();
        final T actual = CoreUtil.cast(this.type().set(this.buildLoader(), null, dataElement, this.buildFromObjectNone()));
        this.assertFromObjectNone(actual, dataElement);
    }

    protected DataElement buildDataElement() {
        return JaxbFactoryImpl.instance().unmarshal(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()).getBytes(StandardCharsets.UTF_8), DataElement.class);
    }

    protected SpecificationLoader buildLoader() {
        return SpecificationLoaderMock.create(this.version());
    }

    protected abstract byte[] buildFromXml();

    protected abstract void assertFromXml(final T actual, final DataElement dataElement);

    @Test
    public void testFromXml() throws Exception {
        final DataElement dataElement = JaxbFactoryImpl.instance().unmarshal(this.buildFromXml(), DataElement.class);
        final T actual = this.type().get(null, dataElement);
        this.assertFromXml(actual, dataElement);
    }

    protected abstract byte[] buildFromXmlEmpty();

    protected abstract void assertFromXmlEmpty(final T actual, final DataElement dataElement);

    @Test
    public void testFromXmlEmpty() throws Exception {
        final DataElement dataElement = JaxbFactoryImpl.instance().unmarshal(this.buildFromXmlEmpty(), DataElement.class);
        final T actual = this.type().get(null, dataElement);
        this.assertFromXmlEmpty(actual, dataElement);
    }

    protected byte[] buildFromXmlNone() {
        return String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()).getBytes(StandardCharsets.UTF_8);
    }

    protected abstract void assertFromXmlNone(final T actual, final DataElement dataElement);

    @Test
    public void testFromXmlNone() throws Exception {
        final DataElement dataElement = JaxbFactoryImpl.instance().unmarshal(this.buildFromXmlNone(), DataElement.class);
        final T actual = this.type().get(null, dataElement);
        this.assertFromXmlNone(actual, dataElement);
    }

}
