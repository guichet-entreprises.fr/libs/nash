/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.AbstractLoaderTest;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.util.StylesheetUtil;

/**
 * Class RecordStepControllerCardinalityTest.
 *
 * @author aolubi
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class DataModelManagerCardinalityTest extends AbstractLoaderTest {

    /**
     * Test save cardinality one to one.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSaveCardinalityOneToOne() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");
        request.setParameter("group01data01", "Doe");
        request.setParameter("group01data02[0]", "Luke");
        request.setParameter("group01data02[1]", "David");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-updated.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

    @Test
    public void testSaveCardinalityFromEmpty() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");
        request.setParameter("group01data01", "Doe");
        request.setParameter("group01data02[0]", "Luke");
        request.setParameter("group01data02[1]", "David");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data-empty.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-updated.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

    @Test
    public void testSaveCardinalityAddOne() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");
        request.setParameter("group01data01", "Doe");
        request.setParameter("group01data02[0]", "Luke");
        request.setParameter("group01data02[1]", "Anakin");
        request.setParameter("group01data02[2]", "Obiwan");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-updated-three.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

    @Test
    public void testSaveCardinalityRemoveDataElement() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");
        request.setParameter("group01data01", "Doe");
        request.setParameter("group01data02[0]", "Luke");
        request.setParameter("subGroup01[0].group01data01", "John");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data-multiple-dataelement.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-updated-one.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

    @Test
    public void testSaveCardinalityToDataEmpty() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");
        request.setParameter("group01data01", "Doe");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-updated-empty.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

    @Test
    public void testSaveCardinalityUpdateSubGroup() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");
        request.setParameter("group01data01", "Doe");
        request.setParameter("subGroup01[0].subGroup01Item01", "James");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data-subgroup.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-updated-subgroup.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

    @Test
    public void testSaveCardinalityAddSubGroup() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");
        request.setParameter("group01data01", "Doe");
        request.setParameter("subGroup01[0].subGroup01Item01", "David");
        request.setParameter("subGroup01[1].subGroup01Item01", "Kevin");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data-subgroup.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-updated-new-subgroup.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

    @Test
    public void testSaveCardinalityRemoveGroupElement() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");
        request.setParameter("enfant[0].nom", "Smith");
        request.setParameter("enfant[0].prenom[0]", "Louis");
        request.setParameter("enfant[0].prenom[1]", "Olivier");
        request.setParameter("enfant[0].adresse[0].numeroVoie", "1");
        request.setParameter("enfant[0].adresse[0].typeVoie", "rue");
        request.setParameter("enfant[0].adresse[0].nomVoie", "Diderot");
        request.setParameter("enfant[0].adresse[1].numeroVoie", "1");
        request.setParameter("enfant[0].adresse[1].typeVoie", "boulevard");
        request.setParameter("enfant[0].adresse[1].nomVoie", "de la République");
        request.setParameter("enfant[0].telephone[0]", "0123456789");
        request.setParameter("enfant[0].telephone[1]", "0623456789");
        request.setParameter("enfant[0].profession[0].nomProfession", "Agriculteur");
        request.setParameter("enfant[0].profession[0].adresseProfession[0].numeroVoie", "2");
        request.setParameter("enfant[0].profession[0].adresseProfession[0].typeVoie", "rue");
        request.setParameter("enfant[0].profession[0].adresseProfession[0].nomVoie", "Diderot");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data-multiple-groupelement.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-updated-multiple-groupelement.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

    @Test
    public void testSaveCardinalityToGroupElementEmpty() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("success", "next");
        request.setParameter("group.id", "group01");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("spec/1-data/data-multiple-groupelement.xml")).updateFromRequest(request);

        assertEquals( //
                this.resourceAsString("spec/1-data/data-empty-multiple-groupelement.xml"), //
                new String(JaxbFactoryImpl.instance().asByteArray(actual, StylesheetUtil.resolve("1-data/data.xml", FormSpecificationData.class))) //
        );
    }

}
