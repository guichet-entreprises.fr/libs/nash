/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class UtilProcessorBridgeTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class UtilProcessorBridgeTest extends AbstractTest {

    @Autowired
    private Engine engine;

    private final IProvider provider = mock(IProvider.class);

    private SpecificationLoader loader;

    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        this.loader = this.engine.loader(this.provider);
    }

    /**
     * Test resource from path.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResourceFromPath() throws Exception {
        final String resourcePath = "/3-review/generated/generated.form-1-document.pdf";
        final String expectedResourceAsString = "PDF Test File Content";
        when(this.provider.load(resourcePath)).thenReturn(new FileEntry(resourcePath).asBytes(expectedResourceAsString.getBytes(StandardCharsets.UTF_8)));

        try (UtilProcessorBridge bridge = new UtilProcessorBridge(EngineContext.build(this.loader))) {
            final Item actual = bridge.resourceFromPath(resourcePath);
            assertNotNull(actual);
            assertEquals("application/pdf", actual.getMimeType());
            assertEquals(expectedResourceAsString, new String(actual.getContent(), StandardCharsets.UTF_8));
        }
    }

    /**
     * Test resource from binary with byet array input.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResourceFromBinary() throws Exception {
        final String resourceName = "document.pdf";

        try (UtilProcessorBridge bridge = new UtilProcessorBridge(EngineContext.build(this.loader))) {
            final Item actual = bridge.resourceFromBinary(resourceName, this.resourceAsBytes("document.pdf"));
            assertNotNull(actual);
            assertNotNull(actual.getContent());
            assertEquals("application/pdf", actual.getMimeType());
        }
    }

    /**
     * Test resource from binary with string input.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResourceFromBinaryWithString() throws Exception {
        final String resourceName = "document.pdf";

        try (UtilProcessorBridge bridge = new UtilProcessorBridge(EngineContext.build(this.loader))) {
            final String documentAsString = Base64.getEncoder().encodeToString(this.resourceAsBytes("document.pdf"));
            final Item actual = bridge.resourceFromBinary(resourceName, documentAsString);
            assertNotNull(actual);
            assertNotNull(actual.getContent());
            assertEquals("application/pdf", actual.getMimeType());
        }
    }

    /**
     * Test resource from binary with integer input.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResourceFromBinaryWithInt() throws Exception {
        final String resourceName = "document.pdf";

        try (UtilProcessorBridge bridge = new UtilProcessorBridge(EngineContext.build(this.loader))) {
            final String documentAsString = Base64.getEncoder().encodeToString(this.resourceAsBytes("document.pdf"));
            bridge.resourceFromBinary(resourceName, documentAsString);
        } catch (final TechnicalException ex) {
            assertEquals("Cannot read value from binary", ex.getMessage());
        }
    }

    /**
     * Test resource from binary with null input.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResourceFromBinaryWithNull() throws Exception {
        final String resourceName = "document.pdf";

        try (UtilProcessorBridge bridge = new UtilProcessorBridge(EngineContext.build(this.loader))) {
            final Item actual = bridge.resourceFromBinary(resourceName, null);
            assertNull(actual);
        }
    }

    /**
     * Test converting string in Base64 to byte array
     *
     * @throws IOException
     */
    @Test
    public void testConvertBase64ToByte() throws IOException {
        try (UtilProcessorBridge bridge = new UtilProcessorBridge(EngineContext.build(this.loader))) {
            final String documentAsString = Base64.getEncoder().encodeToString(this.resourceAsBytes("document.pdf"));
            final byte[] ressourceAsByteArray = bridge.convertBase64ToByte(documentAsString);
            assertTrue(Arrays.equals(ressourceAsByteArray, this.resourceAsBytes("document.pdf")));
        }

    }

    /**
     * Test converting string in Base64 to byte array, null input case
     *
     * @throws IOException
     */
    @Test
    public void testConvertBase64ToByteNull() throws IOException {
        final IProvider provider = mock(IProvider.class);

        try (UtilProcessorBridge bridge = new UtilProcessorBridge(EngineContext.build(this.loader))) {
            final byte[] ressourceAsByteArray = bridge.convertBase64ToByte(null);
            assertNull(ressourceAsByteArray);
        }

    }
}
