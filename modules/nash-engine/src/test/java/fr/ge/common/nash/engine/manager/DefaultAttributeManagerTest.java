/**
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DefaultElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.test.builder.DataElementBuilder;
import fr.ge.common.nash.engine.test.builder.DefaultElementBuilder;
import fr.ge.common.nash.engine.test.builder.FormSpecificationDataBuilder;
import fr.ge.common.nash.engine.test.builder.GroupElementBuilder;

/**
 * Class DefaultAttributeManagerTest.
 *
 * @author Christian Cougourdan
 */
public class DefaultAttributeManagerTest {

    /**
     * Test construct with default.
     */
    @Test
    public void testConstructWithDefault() {
        final DefaultElement defaultElement = new DefaultElementBuilder().build();
        final DefaultAttributeManager mgr = new DefaultAttributeManager(defaultElement);

        assertNotNull(mgr.defaultElement());
    }

    /**
     * Test construct with null default.
     */
    @Test
    public void testConstructWithNullDefault() {
        final DefaultAttributeManager mgr = new DefaultAttributeManager((DefaultElement) null);

        assertNull(mgr.defaultElement());
    }

    /**
     * Test construct with null specification.
     */
    @Test
    public void testConstructWithNullSpecification() {
        final DefaultAttributeManager mgr = new DefaultAttributeManager((FormSpecificationData) null);

        assertNull(mgr.defaultElement());
    }

    /**
     * Test construct with specification.
     */
    @Test
    public void testConstructWithSpecification() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01").def(new DefaultElementBuilder().build()).build();
        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        assertNotNull(mgr.defaultElement());
    }

    /**
     * Test data default.
     */
    @Test
    public void testDataDefault() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01") //
                .def(new DefaultElementBuilder().data(new DataElementBuilder().type("Integer").mandatory(false).build()).build()) //
                .group(new GroupElementBuilder().datas(new DataElementBuilder().build()).build()) //
                .build();

        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        final GroupElement groupElement = mgr.register(0);

        assertNotNull(groupElement);
        assertNotNull(groupElement.getData());
        assertEquals(1, groupElement.getData().size());

        final DataElement dataElement = (DataElement) groupElement.getData().get(0);
        assertEquals("Integer", dataElement.getType());
        assertEquals(Boolean.FALSE, dataElement.getMandatory());
    }

    /**
     * Test data default mandatory.
     */
    @Test
    public void testDataDefaultMandatory() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01") //
                .def(new DefaultElementBuilder().data(new DataElementBuilder().mandatory(true).build()).build()) //
                .group(new GroupElementBuilder().datas(new DataElementBuilder().mandatory(false).build()).build()) //
                .build();

        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        final GroupElement groupElement = mgr.register(0);

        assertNotNull(groupElement);
        assertNotNull(groupElement.getData());
        assertEquals(1, groupElement.getData().size());

        final DataElement dataElement = (DataElement) groupElement.getData().get(0);
        assertEquals(Boolean.FALSE, dataElement.getMandatory());
    }

    /**
     * Test data default no mandatory.
     */
    @Test
    public void testDataDefaultNoMandatory() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01") //
                .def(new DefaultElementBuilder().data(new DataElementBuilder().type("Integer").build()).build()) //
                .group(new GroupElementBuilder().datas(new DataElementBuilder().type("String").mandatory(true).build()).build()) //
                .build();

        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        final GroupElement groupElement = mgr.register(0);

        assertNotNull(groupElement);
        assertNotNull(groupElement.getData());
        assertEquals(1, groupElement.getData().size());

        final DataElement dataElement = (DataElement) groupElement.getData().get(0);
        assertEquals(Boolean.TRUE, dataElement.getMandatory());
    }

    /**
     * Test group default description.
     */
    @Test
    public void testGroupDefaultDescription() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01") //
                .def(new DefaultElementBuilder().group(new GroupElementBuilder().label("default label").description("default description").build()).build()) //
                .group(new GroupElementBuilder().label("group label").build()) //
                .build();

        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        final GroupElement groupElement = mgr.register(0);

        assertNotNull(groupElement);
        assertEquals("group label", groupElement.getLabel());
        assertEquals("default description", groupElement.getDescription());
    }

    /**
     * Test group default label.
     */
    @Test
    public void testGroupDefaultLabel() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01") //
                .def(new DefaultElementBuilder().group(new GroupElementBuilder().label("default label").description("default description").build()).build()) //
                .group(new GroupElementBuilder().description("group description").build()) //
                .build();

        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        final GroupElement groupElement = mgr.register(0);

        assertNotNull(groupElement);
        assertEquals("default label", groupElement.getLabel());
        assertEquals("group description", groupElement.getDescription());
    }

    /**
     * Test index too high.
     */
    @Test
    public void testIndexTooHigh() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01").group(new GroupElementBuilder().build()).build();
        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        try {
            mgr.register(3);
            fail();
        } catch (final IllegalStateException ex) {
            assertEquals("Unable to register default attributes : group index [3] is out of bound", ex.getMessage());
        }
    }

    /**
     * Test index too low.
     */
    @Test
    public void testIndexTooLow() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01").group(new GroupElementBuilder().build()).build();
        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        try {
            mgr.register(-1);
            fail();
        } catch (final IllegalStateException ex) {
            assertEquals("Unable to register default attributes : group index [-1] is out of bound", ex.getMessage());
        }
    }

    /**
     * Tests with no group.
     */
    @Test
    public void testNoGroup() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01").build();
        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        mgr.register(0);
    }

    /**
     * Test no default.
     */
    @Test
    public void testNoDefault() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01") //
                .group(new GroupElementBuilder().datas(new DataElementBuilder().build()).build()) //
                .build();

        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        final GroupElement groupElement = mgr.register(0);

        assertNotNull(groupElement);
        assertNull(groupElement.getLabel());
        assertNull(groupElement.getDescription());
    }

    /**
     * Test no specification.
     */
    @Test
    public void testNoSpecification() {
        final DefaultAttributeManager mgr = new DefaultAttributeManager((FormSpecificationData) null);

        try {
            mgr.register(0);
            fail();
        } catch (final IllegalStateException ex) {
            assertEquals("Unable to register default attributes : no form specification registered", ex.getMessage());
        }
    }

    /**
     * Test empty group multiple.
     */
    @Test
    public void testEmptyGroupMultiple() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01") //
                .group(new GroupElementBuilder().id("group01[]").description("group description")
                        .datas(new DataElementBuilder("firstname").type("String").value("John").build(), //
                                new DataElementBuilder("lastname").type("String").value("Alex").build()) //
                        .build()) //
                .build();

        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        final GroupElement groupElement = mgr.register(0);

        assertNotNull(groupElement);
        assertEquals("group01[0]", groupElement.getId());
        assertEquals("group description", groupElement.getDescription());
    }

    /**
     * Test empty group multiple.
     */
    @Test
    public void testNonEmptyGroupMultiple() {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("form01") //
                .group(new GroupElementBuilder().id("group01[1]").description("group description")
                        .datas(new DataElementBuilder("firstname").type("String").value("John").build(), //
                                new DataElementBuilder("lastname").type("String").value("Alex").build()) //
                        .build()) //
                .build();

        final DefaultAttributeManager mgr = new DefaultAttributeManager(spec);

        final GroupElement groupElement = mgr.register(0);

        assertNotNull(groupElement);
        assertEquals("group01[1]", groupElement.getId());
        assertEquals("group description", groupElement.getDescription());
    }
}
