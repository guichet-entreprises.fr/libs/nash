/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.bean.EventTypeEnum;
import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.manager.processor.ScriptProcessor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationHistory;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationReferential;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.ReferentialsForElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsForElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.test.builder.ProcessElementBuilder;
import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class SpecificationLoaderTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class SpecificationLoaderTest extends AbstractTest {

    /** La constante DESCRIPTION. */
    private static final String DESCRIPTION = "description.xml";

    /** La constante DESCRIPTION_STEP_2. */
    private static final String DESCRIPTION_STEP_2 = "description-step-2.xml";

    /** provider. */
    private final IProvider provider = mock(IProvider.class);

    /** La constante CONTEXT. */
    private static final String CONTEXT = "context.xml";

    /** La constante DATA. */
    private static final String DATA = "data.xml";

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.getAbsolutePath(any(String.class))).thenCallRealMethod();
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
    }

    /**
     * Test load description.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadDescription() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final FormSpecificationDescription actual = loader.description();

        assertThat(actual, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("lang", equalTo("fr")), //
                                hasProperty("formUid", equalTo("2016-04-SPC-AAA-29")), //
                                hasProperty("recordUid", equalTo("2016-04-REC-AAA-20")), //
                                hasProperty("reference", //
                                        allOf( //
                                                hasProperty("code", equalTo("R&D/Test")), //
                                                hasProperty("revision", equalTo(1L)) //
                                        ) //
                                ), //
                                hasProperty("title", equalTo("R&D Test")), //
                                hasProperty("description", equalTo("R&D Test description")) //
                        ) //
                ) //
        );
    }

    /**
     * Test validate data error.
     *
     * @throws Exception
     *             the exception
     */
    @Test(expected = TechnicalException.class)
    public void testValidateDataError() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final FormSpecificationData actual = this.process("data.js");
        loader.validate(actual);
    }

    /**
     * Test validate data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testValidateData() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(ResourceUtil.resourceAsBytes(DESCRIPTION, Engine.class));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final FormSpecificationData actual = this.process("valid-data.js");
        loader.validate(actual);
    }

    /**
     * Process.
     *
     * @param scriptFilename
     *            script filename
     * @return form specification data
     */
    private FormSpecificationData process(final String scriptFilename) {
        final ProcessElement processElement = new ProcessElementBuilder() //
                .id("review") //
                .type("script") //
                .output("data-generated.xml") //
                .value(this.resourceAsString(scriptFilename)) //
                .build();

        final Map<String, Object> model = new HashMap<>();
        model.put("lastname", "Doe");
        model.put("firstname", "John");

        final ScriptProcessor processor = new ScriptProcessor() //
                .setContext(EngineContext.build(this.applicationContext.getBean(SpecificationLoader.class, null, this.provider)).target(processElement));

        return processor.execute(model).getContent();
    }

    /**
     * Test referentials.
     */
    @Test
    public void testReferentials() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final List<ReferentialsForElement> referentials = loader.referentials();
        assertThat(referentials, //
                contains( //
                        allOf( //
                                hasProperty("id", equalTo("ref01")), //
                                hasProperty("path", equalTo("referentials/ref01.xml")) //
                        ) //
                ) //
        );
    }

    /**
     * Test referential path.
     */
    @Test
    public void testReferentialPath() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final String path = loader.referentialPath("ref01");
        assertThat(path, equalTo("referentials/ref01.xml"));
    }

    /**
     * Test referential path not existing.
     */
    @Test
    public void testReferentialPathNotExisting() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final String path = loader.referentialPath("ref02");
        assertThat(path, equalTo(null));
    }

    /**
     * Test referential.
     */
    @Test
    public void testReferential() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        when(this.provider.asBytes("referentials/ref01.xml")).thenReturn(this.resourceAsBytes("referential.xml"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final FormSpecificationReferential referentialElement = loader.referential("ref01");
        assertThat(referentialElement, notNullValue());
        assertThat(referentialElement.getReferentialTextElements(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("code01")), //
                                        hasProperty("label", equalTo("Ref #01 Value")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("code02")), //
                                        hasProperty("label", equalTo("Ref #02 Value")) //
                                ) //
                        ) //
                ) //
        );
        assertThat(referentialElement.getDisplayCondition(), equalTo("1 == 1"));
    }

    /**
     * Test referential file not existing.
     */
    @Test
    public void testReferentialFileNotExisting() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final FormSpecificationReferential referentialElement = loader.referential("ref01");
        assertThat(referentialElement, nullValue());
    }

    /**
     * Test translations.
     */
    @Test
    public void testTranslations() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final List<TranslationsForElement> translations = loader.translations();
        assertThat(translations, //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("lang", equalTo("en")), //
                                        hasProperty("path", equalTo("messages/en.po")) //
                                ), //
                                allOf( //
                                        hasProperty("lang", equalTo("es")), //
                                        hasProperty("path", equalTo("messages/es.po")) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test translation path.
     */
    @Test
    public void testTranslationPath() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final String path = loader.translationPath("es");
        assertThat(path, equalTo("messages/es.po"));
    }

    /**
     * Test translation path not existing.
     */
    @Test
    public void testTranslationPathNotExisting() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final String path = loader.translationPath("zh");
        assertThat(path, equalTo(null));
    }

    /**
     * Test translation file.
     *
     * @throws IOException
     *             an {@link IOException}
     */
    @Test
    public void testTranslationFile() throws IOException {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        when(this.provider.asBytes("messages/es.po")).thenReturn("a translation file".getBytes());
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final InputStream file = loader.translationFile("es");
        final String fileContent = IOUtils.toString(file, StandardCharsets.UTF_8.name());
        assertThat(fileContent, equalTo("a translation file"));
    }

    /**
     * Test translation file not existing.
     */
    @Test
    public void testTranslationFileNotExisting() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final InputStream file = loader.translationFile("zh");
        assertThat(file, nullValue());
    }

    /**
     * Test history path.
     */
    @Test
    public void testHistoryPath() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final String path = loader.historyPath();
        assertThat(path, equalTo("customHistoryPath.xml"));
    }

    /**
     * Test history.
     *
     * @throws IOException
     *             an {@link IOException}
     */
    @Test
    public void testHistory() throws IOException {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        when(this.provider.asBytes("customHistoryPath.xml")).thenReturn(this.resourceAsBytes("history.xml"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final FormSpecificationHistory historyElement = loader.history();
        assertThat(historyElement, notNullValue());
        assertThat(historyElement.getEvents(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("message", equalTo("message 1")), //
                                hasProperty("message", equalTo("message 2")), //
                                hasProperty("message", equalTo("message 3")) //
                        ) //
                ) //
        );
    }

    /**
     * Test has redirection script.
     */
    @Test
    public void testHasRedirectionScript() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        when(this.provider.asBytes("step-03/postprocess.xml")).thenReturn(this.resourceAsBytes("redirection-postprocess.xml"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final boolean redirection = loader.stepsMgr().hasRedirectionScript(loader.stepsMgr().find("step03"));
        assertThat(redirection, equalTo(true));
    }

    /**
     * Test no redirection script.
     */
    @Test
    public void testNoRedirectionScript() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        when(this.provider.asBytes("step-03/postprocess.xml")).thenReturn(this.resourceAsBytes("no-redirection-postprocess.xml"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final boolean redirection = loader.stepsMgr().hasRedirectionScript(loader.stepsMgr().find("step03"));
        assertThat(redirection, equalTo(false));
    }

    /**
     * Test load description empty.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadDescriptionEmpty() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(new byte[] {});

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        try {
            loader.description();
            fail("No description generates TechnicalException");
        } catch (final RecordNotFoundException ex) {
            assertEquals("Specification's description file not found", ex.getMessage());
        }
    }

    /**
     * Test load description null.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadDescriptionNull() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(null);

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        try {
            loader.description();
            fail("No description generates TechnicalException");
        } catch (final RecordNotFoundException ex) {
            assertEquals("Specification's description file not found", ex.getMessage());
        }

    }

    /**
     * Test load steps.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadSteps() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final List<StepElement> steps = loader.stepsMgr().findAll();
        assertThat(steps, //
                contains( //
                        Arrays.asList( //
                                this.firstStepMatcher(), //
                                this.secondStepMatcher(), //
                                this.thirdStepMatcher(), //
                                this.fourthStepMatcher() //
                        ) //
                ) //
        );
    }

    /**
     * Load current step.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void loadCurrentStep() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement actual = loader.stepsMgr().current();

        assertThat(actual, this.firstStepMatcher());
    }

    /**
     * Load step out of bound.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void loadStepOutOfBound() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        assertThat(loader.stepsMgr().find(-1), nullValue());
        assertThat(loader.stepsMgr().find(42), nullValue());
    }

    /**
     * Load step by index.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void loadStepByIndex() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final StepElement actual = loader.stepsMgr().find(2);

        assertThat(actual, this.thirdStepMatcher());
    }

    /**
     * Load step by name ok.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void loadStepByNameOk() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final StepElement actual = loader.stepsMgr().find("step02");

        assertThat(actual, this.secondStepMatcher());
    }

    /**
     * Load step by name ko.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void loadStepByNameKo() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final StepElement actual = loader.stepsMgr().find("unknown");

        assertThat(actual, nullValue());
    }

    /**
     * Build data.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testBuildData() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        when(this.provider.asBytes("step-01/data.xml")).thenReturn(this.resourceAsBytes(CONTEXT));
        when(this.provider.asBytes("step-02/data.xml")).thenReturn(this.resourceAsBytes(DATA));
        when(this.provider.asBytes("step-03/preprocess.xml")).thenReturn(this.resourceAsBytes("preprocess.xml"));
        when(this.provider.asBytes("step-03/preprocess.js")).thenReturn(this.resourceAsBytes("preprocess.js"));
        when(this.provider.asBytes("step-03/types.xml")).thenReturn(this.resourceAsBytes("types.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final StepElement step = loader.stepsMgr().find(2);
        loader.processes().preExecute(step);

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.provider).save(eq("step-03/data-generated.xml"), eq("text/xml"), captor.capture());

        final FormSpecificationData actual = JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationData.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("attachments")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        assertThat(actual.getGroups().get(0), //
                allOf( //
                        hasProperty("id", equalTo("attachments")), //
                        hasProperty("data", hasSize(1)) //
                ) //
        );

        assertThat(actual.getGroups().get(0).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("userAttachment")), //
                        hasProperty("label", equalTo("User attachment #01 for John Doe")), //
                        hasProperty("type", equalTo("File")) //
                ) //
        );
    }

    /**
     * Build data referential.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void buildDataReferential() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));
        when(this.provider.asBytes("step-01/data.xml")).thenReturn(this.resourceAsBytes(CONTEXT));
        when(this.provider.asBytes("step-02/data.xml")).thenReturn(this.resourceAsBytes(DATA));
        when(this.provider.asBytes("step-03/preprocess.xml")).thenReturn(this.resourceAsBytes("preprocess.xml"));
        when(this.provider.asBytes("step-03/preprocess.js")).thenReturn(this.resourceAsBytes("preprocess.js"));
        when(this.provider.asBytes("step-03/types.xml")).thenReturn(this.resourceAsBytes("types.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final StepElement step = loader.stepsMgr().find(1);
        loader.processes().preExecute(step);
        final FormSpecificationData actual = loader.data(step);

        assertThat(actual.getId(), equalTo("step02"));
        assertThat(actual.getGroups(), hasSize(1));

        final GroupElement group = actual.getGroups().get(0);
        assertThat(group.getId(), equalTo("grp01"));
        assertThat(group.getData(), hasSize(4));

        final DataElement data = (DataElement) group.getData().get(group.getData().size() - 1);
        assertThat(data, hasProperty("id", equalTo("fld02")));
        assertThat(data, hasProperty("label", equalTo("Field #02 Label : list")));
        assertThat(data, hasProperty("type", equalTo("ComboBox")));
        assertThat(data.getConftype().getReferentialElements(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("displayCondition", equalTo("1 == 1")), //
                                hasProperty("displayCondition", equalTo("1 == 2")), //
                                hasProperty("displayCondition", equalTo(null)), //
                                hasProperty("displayCondition", equalTo("$fld01 == 'A'")), //
                                hasProperty("displayCondition", equalTo("$fld01 == 'A'")) //
                        ) //
                ) //
        );
    }

    /**
     * Test step index nominal.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testStepIndexNominal() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION_STEP_2));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final int actual = loader.currentStepPosition();

        assertThat(actual, equalTo(1));
    }

    /**
     * Test group condition displayed.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGroupConditionDisplayed() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("group-condition.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final int actual = loader.group(1, 1);

        assertThat(actual, equalTo(1));
    }

    /**
     * Test group condition hidden.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGroupConditionHidden() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("group-condition.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final int actual = loader.group(1, 3);

        assertThat(actual, equalTo(5));

        final FormSpecificationData bean = provider.asBean("step02.xml", FormSpecificationData.class);
        assertThat(bean.getGroups().get(0).getData(), //
                contains( //
                        hasProperty("value", //
                                hasProperty("content", equalTo("Field 01 value")) //
                        ) //
                ) //
        );

        assertThat(bean.getGroups().get(1).getData(), //
                contains( //
                        hasProperty("value", //
                                hasProperty("content", equalTo("Field 01 value")) //
                        ) //
                ) //
        );

        assertThat(bean.getGroups().get(2).getData(), //
                contains( //
                        hasProperty("value", //
                                hasProperty("content", equalTo("Field 01 value")) //
                        ) //
                ) //
        );

        assertThat(bean.getGroups().get(3).getData(), //
                contains( //
                        hasProperty("value", //
                                nullValue() //
                        ) //
                ) //
        );

        assertThat(bean.getGroups().get(4).getData(), //
                contains( //
                        hasProperty("value", //
                                nullValue() //
                        ) //
                ) //
        );

        assertThat(bean.getGroups().get(5).getData(), //
                contains( //
                        hasProperty("value", //
                                hasProperty("content", equalTo("Field 01 value")) //
                        ) //
                ) //
        );

    }

    /**
     * Test previous group condition displayed.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPreviousGroupConditionDisplayed() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("group-condition.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        loader.group(1, 3);
        final int actual = loader.previousGroup(1, 2);

        assertThat(actual, equalTo(1));
    }

    /**
     * Test previous group condition hidden.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPreviousGroupConditionHidden() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("group-condition.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        loader.group(1, 3);
        final int actual = loader.previousGroup(1, 5);

        assertThat(actual, equalTo(2));
    }

    /**
     * Test path.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPath() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("path.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final FormSpecificationData data = loader.data(0);

        assertThat(data, notNullValue());
        assertThat(data.getGroups().get(0), //
                allOf( //
                        hasProperty("id", equalTo("grp01")), //
                        hasProperty("data", hasSize(4)) //
                ) //
        );//

        assertThat(data.getGroups().get(0).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("path", equalTo("grp01.fld01")) //
                ) //
        );
        assertThat(data.getGroups().get(0).getData().get(3), //
                allOf( //
                        hasProperty("id", equalTo("fld02")), //
                        hasProperty("path", equalTo("grp01.fld02")) //
                ) //
        );

        assertThat(data.getGroups().get(0).getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("grp02")), //
                        hasProperty("path", equalTo("grp01.grp02")), //
                        hasProperty("data", hasSize(3)) //
                ) //
        );
        assertThat(data.getGroups().get(0).getData().get(1).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("path", equalTo("grp01.grp02.fld01")) //
                ) //
        );
        assertThat(data.getGroups().get(0).getData().get(1).getData().get(2), //
                allOf( //
                        hasProperty("id", equalTo("fld02")), //
                        hasProperty("path", equalTo("grp01.grp02.fld02")) //
                ) //
        );
        assertThat(data.getGroups().get(0).getData().get(1).getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("grp03")), //
                        hasProperty("path", equalTo("grp01.grp02.grp03")), //
                        hasProperty("data", hasSize(3)) //
                ) //
        );

        assertThat(data.getGroups().get(0).getData().get(1).getData().get(1).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("path", equalTo("grp01.grp02.grp03.fld01")) //
                ) //
        );
        assertThat(data.getGroups().get(0).getData().get(1).getData().get(1).getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("fld02")), //
                        hasProperty("path", equalTo("grp01.grp02.grp03.fld02")) //
                ) //
        );
        assertThat(data.getGroups().get(0).getData().get(1).getData().get(1).getData().get(2), //
                allOf( //
                        hasProperty("id", equalTo("fld03")), //
                        hasProperty("path", equalTo("grp01.grp02.grp03.fld03")) //
                ) //
        );

        assertThat(data.getGroups().get(0).getData().get(2), //
                allOf( //
                        hasProperty("id", equalTo("grp02b")), //
                        hasProperty("path", equalTo("grp01.grp02b")), //
                        hasProperty("data", hasSize(1)) //
                ) //
        );
        assertThat(data.getGroups().get(0).getData().get(2).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("path", equalTo("grp01.grp02b.fld01")) //
                ) //
        );

    }

    /**
     * Load first step to do for an engine user.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void loadFirstStepTodo() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement actual = loader.firstStepToDo("user");
        assertThat(actual, this.firstStepMatcher());

        final StepElement actualBo = loader.firstStepToDo("bo");
        assertThat(actualBo, nullValue());
    }

    /**
     * Load last step to do for an engine user.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void loadLastStepTodo() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement actual = loader.lastStepToDo("user");
        assertThat(actual, this.fourthStepMatcher());

        final StepElement actualBo = loader.firstStepToDo("bo");
        assertThat(actualBo, nullValue());
    }

    /**
     * Test last step done.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testLastStepDone() throws Exception {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement actual = loader.lastStepDone();
        assertThat(actual, nullValue());

    }

    /**
     * First step matcher.
     *
     * @return matcher
     */
    private Matcher<StepElement> firstStepMatcher() {
        return allOf( //
                hasProperty("id", equalTo("step01")), //
                hasProperty("data", equalTo("step-01/data.xml")), //
                hasProperty("preprocess", nullValue()), //
                hasProperty("postprocess", nullValue()), //
                hasProperty("icon", equalTo("question")) //
        );
    }

    /**
     * Second step matcher.
     *
     * @return matcher
     */
    private Matcher<StepElement> secondStepMatcher() {
        return allOf( //
                hasProperty("id", equalTo("step02")), //
                hasProperty("data", equalTo("step-02/data.xml")), //
                hasProperty("preprocess", nullValue()), //
                hasProperty("postprocess", nullValue()), //
                hasProperty("icon", nullValue()) //
        );
    }

    /**
     * Third step matcher.
     *
     * @return matcher
     */
    private Matcher<StepElement> thirdStepMatcher() {
        return allOf( //
                hasProperty("id", equalTo("step03")), //
                hasProperty("data", equalTo("step-03/data-generated.xml")), //
                hasProperty("preprocess", equalTo("step-03/preprocess.xml")), //
                hasProperty("postprocess", equalTo("step-03/postprocess.xml")), //
                hasProperty("icon", nullValue()) //
        );
    }

    /**
     * Fourth step matcher.
     *
     * @return matcher
     */
    private Matcher<StepElement> fourthStepMatcher() {
        return allOf( //
                hasProperty("id", equalTo("step04")), //
                hasProperty("data", equalTo("step-04/data-generated")), //
                hasProperty("preprocess", equalTo("step-04/preprocess.xml")), //
                hasProperty("postprocess", equalTo("step-04/postprocess.xml")), //
                hasProperty("icon", equalTo("like")) //
        ); //
    }

    /**
     * Test sealling the steps.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSeallingSteps() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION_STEP_2));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        loader.stepsMgr().requestSealing(1);
        final StepElement actual = loader.firstStepToDo("user");
        loader.updateStatus(actual, StepStatusEnum.SEALED, "test").flush();
        StepElement stepElementSealed = loader.stepsMgr().find("step01");
        assertThat(stepElementSealed, //
                allOf( //
                        hasProperty("id", equalTo("step01")), //
                        hasProperty("data", equalTo("step-01/data.xml")), //
                        hasProperty("preprocess", nullValue()), //
                        hasProperty("postprocess", nullValue()), // , //
                        hasProperty("icon", equalTo("question")), hasProperty("status", equalTo(StepStatusEnum.SEALED.getStatus())) //
                ) //
        );
        stepElementSealed = loader.stepsMgr().find("step02");
        assertThat(stepElementSealed, //
                allOf( //
                        hasProperty("id", equalTo("step02")), //
                        hasProperty("data", equalTo("step-02/data.xml")), //
                        hasProperty("preprocess", nullValue()), //
                        hasProperty("postprocess", nullValue()), //
                        hasProperty("status", equalTo(StepStatusEnum.SEALED.getStatus())) //
                ) //
        );
        final StepElement stepElementToDo = loader.stepsMgr().find("step03");
        assertThat(stepElementToDo, //
                allOf( //
                        hasProperty("id", equalTo("step03")), //
                        hasProperty("data", equalTo("step-03/data-generated.xml")), //
                        hasProperty("preprocess", equalTo("step-03/preprocess.xml")), //
                        hasProperty("postprocess", equalTo("step-03/postprocess.xml")), //
                        hasProperty("status", equalTo(StepStatusEnum.SEALED.getStatus())) //
                ) //
        );
    }

    /**
     * Test sealling no steps.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSeallingNoSteps() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION_STEP_2));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        loader.stepsMgr().requestSealing(1);
        final StepElement actual = loader.firstStepToDo("user");
        loader.updateStatus(actual, StepStatusEnum.SEALED, "test").flush();
        StepElement stepElementSealed = loader.stepsMgr().find("step01");
        assertThat(stepElementSealed, //
                allOf( //
                        hasProperty("id", equalTo("step01")), //
                        hasProperty("data", equalTo("step-01/data.xml")), //
                        hasProperty("preprocess", nullValue()), //
                        hasProperty("postprocess", nullValue()), //
                        hasProperty("icon", equalTo("question")), //
                        hasProperty("status", equalTo(StepStatusEnum.SEALED.getStatus())) //
                ) //
        );
        stepElementSealed = loader.stepsMgr().find("step02");
        assertThat(stepElementSealed, //
                allOf( //
                        hasProperty("id", equalTo("step02")), //
                        hasProperty("data", equalTo("step-02/data.xml")), //
                        hasProperty("preprocess", nullValue()), //
                        hasProperty("postprocess", nullValue()), //
                        hasProperty("status", equalTo(StepStatusEnum.SEALED.getStatus())) //
                ) //
        );
    }

    /**
     * Test done steps.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDoneStep() {
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes(DESCRIPTION_STEP_2));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        loader.stepsMgr().requestDone(1);
        StepElement stepElement = loader.stepsMgr().find("step01");
        assertThat(stepElement, //
                allOf( //
                        hasProperty("id", equalTo("step01")), //
                        hasProperty("data", equalTo("step-01/data.xml")), //
                        hasProperty("preprocess", nullValue()), //
                        hasProperty("postprocess", nullValue()), // , //
                        hasProperty("icon", equalTo("question")), hasProperty("status", equalTo(StepStatusEnum.DONE.getStatus())) //
                ) //
        );
        stepElement = loader.stepsMgr().find("step02");
        assertThat(stepElement, //
                allOf( //
                        hasProperty("id", equalTo("step02")), //
                        hasProperty("data", equalTo("step-02/data.xml")), //
                        hasProperty("preprocess", nullValue()), //
                        hasProperty("postprocess", nullValue()), //
                        hasProperty("status", equalTo(StepStatusEnum.DONE.getStatus())) //
                ) //
        );
        stepElement = loader.stepsMgr().find("step03");
        assertThat(stepElement, //
                allOf( //
                        hasProperty("id", equalTo("step03")), //
                        hasProperty("data", equalTo("step-03/data-generated.xml")), //
                        hasProperty("preprocess", equalTo("step-03/preprocess.xml")), //
                        hasProperty("postprocess", equalTo("step-03/postprocess.xml")), //
                        hasProperty("status", equalTo(StepStatusEnum.TO_DO.getStatus())) //
                ) //
        );
    }

    @Test
    public void testUpdateStatus() {
        final Properties properties = new Properties();
        properties.put("engine.user.type", "support");
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("updateStatus.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        final StepElement stepToUpdate = loader.stepsMgr().find(3);
        loader.updateStatus(stepToUpdate, StepStatusEnum.IN_PROGRESS, "");

        assertThat(loader.stepsMgr().find(0).getStatus(), equalTo(StepStatusEnum.DONE.getStatus()));
        assertThat(loader.stepsMgr().find(1).getStatus(), equalTo(StepStatusEnum.DONE.getStatus()));
        assertThat(loader.stepsMgr().find(2).getStatus(), equalTo(StepStatusEnum.DONE.getStatus()));
        assertThat(loader.stepsMgr().find(3).getStatus(), equalTo(StepStatusEnum.IN_PROGRESS.getStatus()));
        assertThat(loader.stepsMgr().find(4).getStatus(), equalTo(StepStatusEnum.TO_DO.getStatus()));
        assertThat(loader.stepsMgr().find(5).getStatus(), equalTo(StepStatusEnum.TO_DO.getStatus()));
        assertThat(loader.stepsMgr().find(6).getStatus(), equalTo(StepStatusEnum.TO_DO.getStatus()));
    }

    @Test
    public void testMetaFromFileItem() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example-01.txt", "uploaded/grp01.fld03-2-example-02.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });
        when(this.provider.asBytes("post01.js")).thenReturn("nash.record.meta({\"download\":_input.grp01.fld03});".getBytes(StandardCharsets.UTF_8));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        loader.processes().postExecute(loader.stepsMgr().current());

        final ArgumentCaptor<FormSpecificationMeta> captor = ArgumentCaptor.forClass(FormSpecificationMeta.class);
        verify(this.provider).save(eq("meta.xml"), captor.capture());

        final FormSpecificationMeta meta = captor.getValue();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("download")), hasProperty("value", equalTo("uploaded/grp01.fld03-1-example-01.txt"))), //
                                allOf(hasProperty("name", equalTo("download")), hasProperty("value", equalTo("uploaded/grp01.fld03-2-example-02.txt"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testMetaFromListOfFileItem() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example-01.txt", "uploaded/grp01.fld03-2-example-02.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });
        when(this.provider.asBytes("post01.js")).thenReturn("nash.record.meta([{\"download\":_input.grp01.fld03}]);".getBytes(StandardCharsets.UTF_8));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        loader.processes().postExecute(loader.stepsMgr().current());

        final ArgumentCaptor<FormSpecificationMeta> captor = ArgumentCaptor.forClass(FormSpecificationMeta.class);
        verify(this.provider).save(eq("meta.xml"), captor.capture());

        final FormSpecificationMeta meta = captor.getValue();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("download")), hasProperty("value", equalTo("uploaded/grp01.fld03-1-example-01.txt"))), //
                                allOf(hasProperty("name", equalTo("download")), hasProperty("value", equalTo("uploaded/grp01.fld03-2-example-02.txt"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testMetaFromListOfString() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });
        when(this.provider.asBytes("post01.js")).thenReturn("nash.record.meta([{\"name\":\"key01\",\"value\":\"val01\"},{\"name\":\"key02\",\"value\":\"val02\"}]);".getBytes(StandardCharsets.UTF_8));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        loader.processes().postExecute(loader.stepsMgr().current());

        final ArgumentCaptor<FormSpecificationMeta> captor = ArgumentCaptor.forClass(FormSpecificationMeta.class);
        verify(this.provider).save(eq("meta.xml"), captor.capture());

        final FormSpecificationMeta meta = captor.getValue();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("key01")), hasProperty("value", equalTo("val01"))), //
                                allOf(hasProperty("name", equalTo("key02")), hasProperty("value", equalTo("val02"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testMetaFromListOfStringMultiple() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });
        when(this.provider.asBytes("post01.js")).thenReturn("nash.record.meta([{\"name\":\"key01\",\"value\":[\"val01\",\"val02\"]}]);".getBytes(StandardCharsets.UTF_8));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        loader.processes().postExecute(loader.stepsMgr().current());

        final ArgumentCaptor<FormSpecificationMeta> captor = ArgumentCaptor.forClass(FormSpecificationMeta.class);
        verify(this.provider).save(eq("meta.xml"), captor.capture());

        final FormSpecificationMeta meta = captor.getValue();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("key01")), hasProperty("value", equalTo("val01"))), //
                                allOf(hasProperty("name", equalTo("key01")), hasProperty("value", equalTo("val02"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testMetaFromObjectString() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });
        when(this.provider.asBytes("post01.js")).thenReturn("nash.record.meta({\"key01\":[\"val01\",\"val02\"],\"key02\":\"val03\"});".getBytes(StandardCharsets.UTF_8));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        loader.processes().postExecute(loader.stepsMgr().current());

        final ArgumentCaptor<FormSpecificationMeta> captor = ArgumentCaptor.forClass(FormSpecificationMeta.class);
        verify(this.provider).save(eq("meta.xml"), captor.capture());

        final FormSpecificationMeta meta = captor.getValue();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("key01")), hasProperty("value", equalTo("val01"))), //
                                allOf(hasProperty("name", equalTo("key01")), hasProperty("value", equalTo("val02"))), //
                                allOf(hasProperty("name", equalTo("key02")), hasProperty("value", equalTo("val03"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testMetaFromObjectStringBis() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });
        when(this.provider.asBytes("post01.js")).thenReturn("nash.record.meta({\"key01\":[\"val01\",\"val02\"]});".getBytes(StandardCharsets.UTF_8));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        loader.processes().postExecute(loader.stepsMgr().current());

        final ArgumentCaptor<FormSpecificationMeta> captor = ArgumentCaptor.forClass(FormSpecificationMeta.class);
        verify(this.provider).save(eq("meta.xml"), captor.capture());

        final FormSpecificationMeta meta = captor.getValue();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("key01")), hasProperty("value", equalTo("val01"))), //
                                allOf(hasProperty("name", equalTo("key01")), hasProperty("value", equalTo("val02"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testAddDocumentMetaWithValue() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example-01.txt", "uploaded/grp01.fld03-2-example-02.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final boolean actual = loader.addDocumentMeta(0);

        assertThat(actual, equalTo(true));

        final ArgumentCaptor<FormSpecificationMeta> captor = ArgumentCaptor.forClass(FormSpecificationMeta.class);
        verify(this.provider).save(eq("meta.xml"), captor.capture());

        final FormSpecificationMeta meta = captor.getValue();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("document")), hasProperty("value", equalTo("uploaded/grp01.fld03-1-example-01.txt"))), //
                                allOf(hasProperty("name", equalTo("document")), hasProperty("value", equalTo("uploaded/grp01.fld03-2-example-02.txt"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testAddDocumentMetaWithoutValue() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example-01.txt", "uploaded/grp01.fld03-2-example-02.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final boolean actual = loader.addDocumentMeta(1);

        assertThat(actual, equalTo(false));

        verify(this.provider, never()).save(eq("meta.xml"), any());
    }

    /**
     * Test trigger events.
     */
    @Test
    public void testUpdateStatusWithEvent() {
        final Properties properties = new Properties();
        properties.put("engine.user.type", "user");
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("events.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        final StepElement stepToUpdate = loader.stepsMgr().find(0);
        loader.updateStatus(stepToUpdate, StepStatusEnum.DONE, "");

        assertThat(loader.stepsMgr().find(0).getStatus(), equalTo(StepStatusEnum.DONE.getStatus()));
        assertNotNull(loader.description().getEvents());
        assertThat(loader.description().getEvents(), notNullValue());
        assertThat(loader.description().getEvents().getElements(), hasSize(2));
        assertThat(loader.description().getEvents().getElements(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("type", equalTo(EventTypeEnum.BEFORE_UPDATE_STEP_STATUS.getName())), hasProperty("value", equalTo(StringUtils.EMPTY)),
                                        hasProperty("script", notNullValue())), //
                                allOf(hasProperty("type", equalTo(EventTypeEnum.BEFORE_UPDATE_STEP_STATUS.getName())), hasProperty("value", notNullValue()), hasProperty("script", nullValue())) //
                        ) //
                ) //
        );
    }

    @Test
    public void testRemoveMeta() throws Exception {
        Arrays.asList("description.xml", "step01.xml", "step02.xml", "post01.xml", "uploaded/grp01.fld03-1-example-01.txt", "uploaded/grp01.fld03-2-example-02.txt").forEach(name -> {
            when(this.provider.asBytes(name)).thenReturn(this.resourceAsBytes("meta/" + name));
        });

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final boolean actual = loader.addDocumentMeta(0);
        assertThat(actual, equalTo(true));

        final ArgumentCaptor<FormSpecificationMeta> captor = ArgumentCaptor.forClass(FormSpecificationMeta.class);
        verify(this.provider).save(eq("meta.xml"), captor.capture());

        FormSpecificationMeta meta = captor.getValue();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("document")), hasProperty("value", equalTo("uploaded/grp01.fld03-1-example-01.txt"))), //
                                allOf(hasProperty("name", equalTo("document")), hasProperty("value", equalTo("uploaded/grp01.fld03-2-example-02.txt"))) //
                        ) //
                ) //
        );

        final List<Map<String, Object>> bindings = new ArrayList<>();
        final Map<String, Object> document = new HashMap<>();
        document.put("name", "document");
        document.put("value", "uploaded/grp01.fld03-1-example-01.txt");
        bindings.add(document);

        loader.removeMeta(bindings);

        meta = loader.meta();
        assertThat(meta, notNullValue());
        assertThat(meta.getMetas(), //
                contains( //
                        Arrays.asList( //
                                allOf(hasProperty("name", equalTo("document")), hasProperty("value", equalTo("uploaded/grp01.fld03-2-example-02.txt"))) //
                        ) //
                ) //
        );
    }

    /**
     * Test validate data containing mandatory fields with empty values.
     *
     * @throws Exception
     *             the exception
     */
    @Test(expected = TechnicalException.class)
    public void testValidateMandatoryEmptyFields() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("empty-value-mandatory-fields.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final StepElement current = loader.stepsMgr().current();
        loader.validate(loader.data(current));
    }
}
