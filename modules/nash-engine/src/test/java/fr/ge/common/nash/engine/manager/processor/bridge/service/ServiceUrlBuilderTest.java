/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge.service;

import static org.junit.Assert.assertEquals;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Tests {@link ServiceUrlBuilder}.
 *
 * @author jpauchet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/test-context.xml" })
public class ServiceUrlBuilderTest extends AbstractTest {

    protected static final String ENDPOINT = "http://localhost:8888/test";

    private Configuration configuration;

    @Before
    public void setUp() {
        final Properties properties = new Properties();
        properties.put("ws.external.url", ENDPOINT);
        this.configuration = new Configuration(properties);
    }

    @Test
    public void testBuildUrlNominal() throws Exception {
        final String actual = ServiceUrlBuilder.buildUrl(this.configuration, "${ws.external.url}/v1/Record/{code}/use/{now}", "2017-05-REC-AAA-42", "2017-05-15T14:30:00Z");
        assertEquals(ENDPOINT + "/v1/Record/2017-05-REC-AAA-42/use/2017-05-15T14%3A30%3A00Z", actual);
    }

    @Test
    public void testBuildUrlNoConfiguration() throws Exception {
        final String actual = ServiceUrlBuilder.buildUrl(null, "${ws.external.url}/v1/Record/{code}/use/{now}", "A Simple Code", "2017-05-15T14:30:00Z");
        assertEquals("${ws.external.url}/v1/Record/A%20Simple%20Code/use/2017-05-15T14%3A30%3A00Z", actual);
    }

    @Test
    public void testBuildUrlNotEnoughParameters() throws Exception {
        final String actual = ServiceUrlBuilder.buildUrl(this.configuration, "${ws.external.url}/v1/Record/{code}/use/{now}", "2017-05-REC-AAA-42");
        assertEquals(ENDPOINT + "/v1/Record/2017-05-REC-AAA-42/use/{now}", actual);
    }

}
