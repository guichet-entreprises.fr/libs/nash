/**
 *
 */
package fr.ge.common.nash.engine.manager.processor.bridge;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class TransformationProcessorBridgeTest.
 *
 * @author bsadil
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/test-context.xml" })
public class TransformationProcessorBridgeTest extends AbstractTest {

    /**
     * Transform test.
     *
     * @throws TransformerException
     *             the transformer exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Test
    public void transformTest() throws TransformerException, IOException {

        final IProvider provider = mock(IProvider.class);
        final SpecificationLoader loader = mock(SpecificationLoader.class);

        when(provider.asBytes(eq("regent.xml"))).thenReturn(this.resourceAsBytes("regent.xml"));
        when(provider.asBytes(eq("xslt_regent.xslt"))).thenReturn(this.resourceAsBytes("xslt_regent.xslt"));

        when(provider.asBytes(eq("evenements_ref.xml"))).thenReturn(this.resourceAsBytes("evenements_ref.xml"));
        when(provider.asBytes(eq("supra_ref.xml"))).thenReturn(this.resourceAsBytes("supra_ref.xml"));
        when(provider.asBytes(eq("type_liasse_ref.xml"))).thenReturn(this.resourceAsBytes("type_liasse_ref.xml"));

        // when(provider.save(eq("xslt_regent.xslt"),)).thenReturn(this.resourceAsBytes("xslt_regent.xslt"));

        when(loader.getProvider()).thenReturn(provider);

        try (TransformationProcessorBridge bridge = new TransformationProcessorBridge(EngineContext.build(loader, provider))) {
            bridge.transform("regent.xml", "xslt_regent.xslt", "output.xml");
        }

        final ArgumentCaptor<byte[]> arg = ArgumentCaptor.forClass(byte[].class);
        verify(provider).save(eq("output.xml"), arg.capture());

    }

    /**
     * Extract data from from XML file containing simple result.
     *
     * @throws IOException
     * @throws TechnicalException
     */
    @Test
    public void testExtracSimpleData() throws TechnicalException, IOException {
        final IProvider provider = mock(IProvider.class);
        final SpecificationLoader loader = mock(SpecificationLoader.class);
        when(loader.getProvider()).thenReturn(provider);
        when(provider.asBytes(eq("extract-regent.xml"))).thenReturn(this.resourceAsBytes("extract-regent.xml"));
        try (TransformationProcessorBridge bridge = new TransformationProcessorBridge(EngineContext.build(loader, provider))) {
            final JsonNode result = bridge.extract("extract-regent.xml", "/REGENT-XML/Emetteur");
            assertNotNull(result);
            assertThat(result.get("Emetteur").asText(), equalTo("Z1611"));
        }
    }

    /**
     * Extract data from from XML file containing complex tag.
     *
     * @throws IOException
     * @throws TechnicalException
     */
    @Test
    public void testExtractComplexData() throws TechnicalException, IOException {
        final IProvider provider = mock(IProvider.class);
        final SpecificationLoader loader = mock(SpecificationLoader.class);
        when(loader.getProvider()).thenReturn(provider);
        when(provider.asBytes(eq("extract-regent.xml"))).thenReturn(this.resourceAsBytes("extract-regent.xml"));
        try (TransformationProcessorBridge bridge = new TransformationProcessorBridge(EngineContext.build(loader, provider))) {
            final JsonNode result = bridge.extract("extract-regent.xml", "/REGENT-XML/ServiceApplicatif/Liasse/Service");
            assertNotNull(result);
            assertThat(result.get("Service").get("IDF"), notNullValue());
            assertThat(result.get("Service").get("EDF"), notNullValue());
            assertThat(result.get("Service").get("ADF").get("C37"), notNullValue());
            assertThat(result.get("Service").get("IDF").get("C02"), notNullValue());
            assertThat(result.get("Service").get("IDF").get("C02").asText(), equalTo("H10000020489"));
        }
    }

    /**
     * Extract data from XML file with wrong expression.
     *
     * @throws IOException
     * @throws TechnicalException
     */
    @Test
    public void testExtractWithXPathExpression() throws TechnicalException, IOException {
        final IProvider provider = mock(IProvider.class);
        final SpecificationLoader loader = mock(SpecificationLoader.class);
        when(loader.getProvider()).thenReturn(provider);
        when(provider.asBytes(eq("extract-regent.xml"))).thenReturn(this.resourceAsBytes("extract-regent.xml"));
        try (TransformationProcessorBridge bridge = new TransformationProcessorBridge(EngineContext.build(loader, provider))) {
            final JsonNode result = bridge.extract("extract-regent.xml", "/REGENT-XML/ServiceApplicatif/Specification/AAAA");
            assertNotNull(result);
            assertThat(result.asText(), isEmptyString());
        }
    }

    /**
     * Extract data from XML file with wrong expression.
     *
     * @throws IOException
     * @throws TechnicalException
     */
    @Test
    public void testExtractWithNullExpression() throws TechnicalException, IOException {
        final IProvider provider = mock(IProvider.class);
        final SpecificationLoader loader = mock(SpecificationLoader.class);
        when(loader.getProvider()).thenReturn(provider);
        when(provider.asBytes(eq("extract-regent.xml"))).thenReturn(this.resourceAsBytes("extract-regent.xml"));
        try (TransformationProcessorBridge bridge = new TransformationProcessorBridge(EngineContext.build(loader, provider))) {
            final JsonNode result = bridge.extract("extract-regent.xml", null);
            assertNull(result);
        }
    }
}
