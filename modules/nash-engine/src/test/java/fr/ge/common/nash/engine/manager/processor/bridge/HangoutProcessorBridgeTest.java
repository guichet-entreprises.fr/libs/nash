/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor.bridge;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.script.Bindings;
import javax.script.SimpleBindings;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class HangoutProcessorBridgeTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class HangoutProcessorBridgeTest extends AbstractTest {

    /** The Constant ENDPOINT. */
    protected static final String ENDPOINT = "http://localhost:8888/test";

    /** La constante DESCRIPTION. */
    private static final String DESCRIPTION = "description.xml";

    /** The server. */
    protected Server server;

    /** The service. */
    protected ITestService service;

    /** The proxy cart service. */
    protected ICartService cartService;

    /** The context. */
    protected EngineContext<?> context;

    /** The provider. */
    protected IProvider provider;

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        this.service = mock(ITestService.class);
        this.cartService = mock(ICartService.class);
        this.provider = mock(IProvider.class);
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();

        final Properties properties = new Properties();
        properties.put("nash.web.public.url", "http://localhost/nash");
        properties.put("proxy.service.remove.proxy.file.url", "http://localhost/nash");
        properties.put("stamper.proxy.url.public", ENDPOINT);
        properties.put("stamper.proxy.create.session.url", ENDPOINT + "/v1/proxy/create");
        properties.put("stamper.proxy.validate.session.url", ENDPOINT + "/v1/proxy/validate");
        properties.put("stamper.proxy.ui.callback.url", "http://localhost/proxy/ui/callback/url");
        properties.put("stamper.proxy.service.callback.url", "http://localhost/proxy/service/callback/url");
        properties.put("payment.proxy.url.public", ENDPOINT);
        properties.put("payment.proxy.create.session.url", ENDPOINT + "/v1/proxy/create");
        properties.put("payment.proxy.validate.session.url", ENDPOINT + "/v1/proxy/validate");
        properties.put("payment.proxy.ui.callback.url", "http://localhost/proxy/ui/callback/url");
        properties.put("payment.proxy.service.callback.url", "http://localhost/proxy/service/callback/url");
        properties.put("payment.proxy.service.lock.record", "http://localhost/proxy/service/callback/url");
        properties.put("payment.proxy.service.create.cart", "http://localhost:8888/test/v1/cart/{token}/create");

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBeans(Arrays.asList(this.service, this.cartService));
        serverFactory.setProvider(jsonProvider);
        this.server = serverFactory.create();

        this.context = this.applicationContext.getBean(SpecificationLoader.class, new Configuration(properties), this.provider).buildEngineContext();
    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             the exception
     */
    @After
    public void tearDown() throws Exception {
        this.server.stop();
        this.server.destroy();
    }

    /**
     * Test build url nominal.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testBuildUrlNominal() throws Exception {
        // prepare
        final String token = "1234";
        when(this.service.create(any(), any(), any())).thenReturn(Response.ok(token).build());
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes("stamp/decrochage_proxy/" + DESCRIPTION));
        when(this.provider.asBytes("step01/postprocess.xml")).thenReturn(this.resourceAsBytes("stamp/decrochage_proxy/step01/postprocess.xml"));

        final StepElement stepElement = this.context.getRecord().stepsMgr().current();
        final FormSpecificationProcess stepPostProcess = this.context.getRecord().processes().findPostprocesses(stepElement);
        final IProvider stepProvider = this.context.getRecord().provider(stepPostProcess.getResourceName());
        final EngineContext<IProcess> processContext = this.context //
                .target(stepElement) //
                .target(stepPostProcess) //
                .withProvider(stepProvider) //
                .target(stepPostProcess.getProcesses().get(0));

        // call
        try (final HangoutProcessorBridge bridge = new HangoutProcessorBridge(processContext)) {
            // verify
            final Map<String, Object> file1 = new HashMap<>();
            file1.put("document", FileValueAdapter.createItem(1, "label1", "text/plain", "content1".getBytes()));
            file1.put("zoneId", "id1");
            final Map<String, Object> file2 = new HashMap<>();
            file2.put("document", FileValueAdapter.createItem(2, "label2", "text/plain", "content2".getBytes()));
            file2.put("zoneId", "id2");
            final Map<String, Object> params = new HashMap<>();
            params.put("files", Arrays.asList(file1, file2));
            params.put("civility", "Monsieur");
            params.put("lastName", "NOM");
            params.put("firstName", "Prénom");
            params.put("email", "prenom.nom@mail.com");
            params.put("phone", "06 00 00 00 00");
            final FormSpecificationData actual = bridge.stamp(params);
            final GroupElement group = actual.getGroups().get(0);
            assertThat(group.getId(), equalTo("params"));
            assertThat(group.getData().size(), equalTo(4));
            assertThat(group.getData().get(0).getId(), equalTo("urlRedirection"));
            assertThat(group.getData().get(0).getValue().toString(), equalTo("\"http://localhost:8888/test?token=1234\""));
            assertThat(group.getData().get(1).getId(), equalTo("urlValidation"));
            assertThat(group.getData().get(1).getValue().toString(), equalTo("\"http://localhost:8888/test/v1/proxy/validate\""));
            assertThat(group.getData().get(2).getId(), equalTo("token"));
            assertThat(group.getData().get(2).getValue().toString(), equalTo("\"1234\""));
            assertThat(group.getData().get(3).getId(), equalTo("resultFileName"));
            assertThat(group.getData().get(3).getValue().toString(), equalTo("\"stamp-calling.xml\""));
        }
        final ArgumentCaptor<List<Attachment>> attachments = ArgumentCaptor.forClass(List.class);
        verify(this.service).create(any(), any(), attachments.capture());
        assertThat(attachments.getValue().size(), equalTo(13));
    }

    @Test
    public void testHangoutPay() throws Exception {
        // prepare
        final String token = "1234";
        when(this.service.create(any(), any(), any())).thenReturn(Response.ok(token).build());
        when(this.provider.asBytes(DESCRIPTION)).thenReturn(this.resourceAsBytes("payment/decrochage_proxy/" + DESCRIPTION));
        when(this.provider.asBytes("step01/postprocess.xml")).thenReturn(this.resourceAsBytes("payment/decrochage_proxy/step01/postprocess.xml"));
        when(this.cartService.create(any(), any())).thenReturn(Response.ok().build());

        final StepElement stepElement = this.context.getRecord().stepsMgr().current();
        final FormSpecificationProcess stepPostProcess = this.context.getRecord().processes().findPostprocesses(stepElement);
        final IProvider stepProvider = this.context.getRecord().provider(stepPostProcess.getResourceName());
        final EngineContext<IProcess> processContext = this.context //
                .target(stepElement) //
                .target(stepPostProcess) //
                .withProvider(stepProvider) //
                .target(stepPostProcess.getProcesses().get(0));

        // call
        try (final HangoutProcessorBridge bridge = new HangoutProcessorBridge(processContext)) {
            // verify
            final Bindings params = new SimpleBindings();
            final List<Map<String, Object>> payments = new ArrayList<Map<String, Object>>();
            final Map<String, Object> paymentMap = new HashMap<String, Object>();
            paymentMap.put("sellerId", "1234");
            paymentMap.put("productId", "1234");
            paymentMap.put("amount", "100");
            paymentMap.put("currency", "Euros");

            final List<Map<String, Object>> lines = new ArrayList<Map<String, Object>>();
            final Bindings line = new SimpleBindings();
            line.put("description", "line description");
            line.put("reference", "INPI_produit_01");
            lines.add(line);
            paymentMap.put("lines", lines);

            payments.add(paymentMap);
            params.put("payments", payments);

            final FormSpecificationData actual = bridge.pay(params);
            final GroupElement group = actual.getGroups().get(0);
            assertThat(group.getId(), equalTo("params"));
            assertThat(group.getData().size(), equalTo(4));
            assertThat(group.getData().get(0).getId(), equalTo("urlRedirection"));
            assertThat(group.getData().get(0).getValue().toString(), equalTo("\"http://localhost:8888/test?token=1234\""));
            assertThat(group.getData().get(1).getId(), equalTo("urlValidation"));
            assertThat(group.getData().get(1).getValue().toString(), equalTo("\"http://localhost:8888/test/v1/proxy/validate\""));
            assertThat(group.getData().get(2).getId(), equalTo("token"));
            assertThat(group.getData().get(2).getValue().toString(), equalTo("\"1234\""));
            assertThat(group.getData().get(3).getId(), equalTo("resultFileName"));
            assertThat(group.getData().get(3).getValue().toString(), equalTo("\"payment-calling.xml\""));
        }
        final ArgumentCaptor<List<Attachment>> attachments = ArgumentCaptor.forClass(List.class);
        verify(this.service).create(any(), any(), attachments.capture());
        assertThat(attachments.getValue().size(), equalTo(8));
    }

    @Path("/v1/proxy")
    public static interface ITestService {

        /**
         * Creates the.
         *
         * @param serviceCallbackUrl
         *            the service callback url
         * @param uiCallbackUrl
         *            the ui callback url
         * @param attachmentList
         *            the attachment list
         * @return the response
         */
        @POST
        @Path("/create")
        @Consumes("multipart/form-data")
        Response create(@QueryParam("serviceCallbackUrl") final String serviceCallbackUrl, @QueryParam("uiCallbackUrl") final String uiCallbackUrl, final List<Attachment> attachmentList);

    }

    @Path("/v1/cart")
    public static interface ICartService {

        /**
         * Creates a cart.
         *
         * @param proxyToken
         *            : The proxy token
         * @param resourceAsBytes
         *            : The cart as byte array
         * @return Response : The response
         *
         * @throws TechnicalException
         */
        @POST
        @Path("/{token}/create")
        @Consumes(MediaType.APPLICATION_OCTET_STREAM)
        @Produces(MediaType.APPLICATION_JSON)
        Response create(@PathParam("token") final String proxyToken, final byte[] resourceAsBytes) throws TechnicalException;
    }
}
