/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matcher;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_2.value.ReferentialOption;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;

/**
 * Tests {@link ComboBoxMultipleValueAdapter}.
 */
public class ComboBoxMultipleValueAdapterTest extends AbstractValueAdapterTest<List<ReferentialOption>> {

    private static final List<ReferentialOption> REF_OPTIONS = Arrays.asList( //
            new ReferentialOption("ba", "Banana"), //
            new ReferentialOption("ma", "Mango") //
    );

    private final String dataAsString = String.format( //
            DATA_ELEMENT_FILLED_AS_XML, //
            this.type().name(), //
            String.format("\n" //
                    + "        <list>\n" //
                    + "            <text id=\"%s\">%s</text>\n" //
                    + "            <text id=\"%s\">%s</text>\n" //
                    + "        </list>\n" //
                    + "    ", //
                    REF_OPTIONS.get(0).getId(), //
                    REF_OPTIONS.get(0).getLabel(), //
                    REF_OPTIONS.get(1).getId(), //
                    REF_OPTIONS.get(1).getLabel() //
            ) //
    );

    private final String dataEmptyAsString = String.format( //
            DATA_ELEMENT_FILLED_AS_XML, //
            this.type().name(), //
            "\n" //
                    + "        <list>\n" //
                    + "            <text id=\"\"></text>\n" //
                    + "            <text id=\"\"></text>\n" //
                    + "        </list>\n" //
                    + "    " //
    );

    /**
     * {@inheritDoc}
     */
    @Override
    protected IValueAdapter<List<ReferentialOption>> type() {
        return new ComboBoxMultipleValueAdapter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        return Collections.singletonMap("", new String[] { REF_OPTIONS.get(0).getId(), REF_OPTIONS.get(1).getId() });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequest(final List<ReferentialOption> actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        return Collections.singletonMap("", new String[] { "", "" });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequestEmpty(final List<ReferentialOption> actual, final DataElement dataElement) {
        this.assertFromObjectEmpty(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ReferentialOption> buildFromObject() {
        return REF_OPTIONS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObject(final List<ReferentialOption> actual, final DataElement dataElement) {
        assertThat(actual, //
                contains( //
                        Arrays.asList( //
                                this.expectedAsReferentialOption(REF_OPTIONS.get(0).getId(), REF_OPTIONS.get(0).getLabel()), //
                                this.expectedAsReferentialOption(REF_OPTIONS.get(1).getId(), REF_OPTIONS.get(1).getLabel()) //
                        ) //
                ) //
        );
        assertEquals(this.dataAsString, JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ReferentialOption> buildFromObjectEmpty() {
        return Arrays.asList(new ReferentialOption(), new ReferentialOption());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObjectEmpty(final List<ReferentialOption> actual, final DataElement dataElement) {
        assertNull(actual);
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXml() {
        return this.dataAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXml(final List<ReferentialOption> actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXmlEmpty() {
        return this.dataEmptyAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlEmpty(final List<ReferentialOption> actual, final DataElement dataElement) {
        assertTrue(actual.isEmpty());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlNone(final List<ReferentialOption> actual, final DataElement dataElement) {
        assertNull(actual);
    }

    private Matcher<ReferentialOption> expectedAsReferentialOption(String id, String label) {
        return allOf( //
                hasProperty("id", equalTo(id)), //
                hasProperty("label", equalTo(label)) //
        );
    }

}
