/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.function.Supplier;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.AbstractLoaderTest;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.nash.engine.script.EngineContext;

/**
 * Class DataModelUpdaterTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class DataModelRequestUpdaterTest extends AbstractLoaderTest {

    /**
     * Test conditional group.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testConditionalGroup() throws Exception {
        final EngineContext<FormSpecificationData> engineContext = this.engineContext("data-with-group-condition.xml");
        final FormSpecificationData spec = engineContext.getElement();

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "group01");
        request.setParameter("field01", "false");

        // final IDataModelExtractor extractor =
        // RecursiveDataModelExtractor.create(null);

        new DataModelRequestUpdater(engineContext).update(request);

        final Matcher<Iterable<? extends GroupElement>> expectedSubGroups = ((Supplier<Matcher<Iterable<? extends GroupElement>>>) () -> contains( //
                Arrays.asList( //
                        hasProperty("value", hasProperty("content", equalTo("no"))), //
                        hasProperty("value", nullValue()) //
                ) //
        )).get();

        assertThat(spec.getGroups().get(0).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("no"))), //
                                hasProperty("data", expectedSubGroups), //
                                hasProperty("value", nullValue()) //
                        ) //
                ) //
        );
    }

    /**
     * Test conditional group.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGroupWithoutElement() throws Exception {
        final EngineContext<FormSpecificationData> engineContext = this.engineContext("group-without-element.xml");
        final FormSpecificationData spec = engineContext.getElement();

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "group01");

        // final IDataModelExtractor extractor =
        // RecursiveDataModelExtractor.create(null);

        new DataModelRequestUpdater(engineContext).update(request);

        assertThat(spec.getGroups().get(0).getData().size(), equalTo(0));
    }

    @Test
    public void testLocalContext() throws Exception {
        final EngineContext<FormSpecificationData> engineContext = this.engineContext("local-context.xml");
        final FormSpecificationData spec = engineContext.getElement();

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "group01");
        request.setParameter("field01", "false");
        request.setParameter("group02.field01", "true");
        request.setParameter("group02.field02", "A new simple value");
        request.setParameter("group02.field03", "A new simple value");
        request.setParameter("group02.field04", "A new simple value");
        request.setParameter("group02.field05", "true");
        request.setParameter("group02.group03.field01", "true");
        request.setParameter("group02.group03.field02", "A new simple value");
        request.setParameter("group02.group03.field03", "A new simple value");
        request.setParameter("group02.group03.field04", "A new simple value");
        request.setParameter("group02.group05.field01", "A new simple value");
        request.setParameter("group04[0].field01", "true");
        request.setParameter("group04[0].field02", "A new simple value");
        request.setParameter("group04[1].field01", "false");
        request.setParameter("group04[1].field02", "A new simple value");

        // final IDataModelExtractor extractor =
        // RecursiveDataModelExtractor.create(null);

        new DataModelRequestUpdater(engineContext).update(request);

        final GroupElement grp01 = spec.getGroups().get(0);
        assertThat(grp01.getData().get(0), hasProperty("value", hasProperty("content", equalTo("no"))));
        assertThat(grp01.getData().get(1), hasProperty("value", nullValue()));
        assertThat(grp01.getData().get(2), hasProperty("value", nullValue()));
        assertThat(grp01.getData().get(3), hasProperty("value", nullValue()));

        final GroupElement grp02 = (GroupElement) grp01.getData().get(4);
        assertThat(grp02.getData().get(0), hasProperty("value", hasProperty("content", equalTo("yes"))));
        assertThat(grp02.getData().get(1), hasProperty("value", nullValue()));
        assertThat(grp02.getData().get(2), hasProperty("value", nullValue()));
        assertThat(grp02.getData().get(3), hasProperty("value", hasProperty("content", equalTo("A new simple value"))));

        final GroupElement grp03 = (GroupElement) grp02.getData().get(4);
        assertThat(grp03.getData().get(0), hasProperty("value", hasProperty("content", equalTo("yes"))));
        assertThat(grp03.getData().get(1), hasProperty("value", nullValue()));
        assertThat(grp03.getData().get(2), hasProperty("value", nullValue()));
        assertThat(grp03.getData().get(3), hasProperty("value", hasProperty("content", equalTo("A new simple value"))));

        final GroupElement grp05 = (GroupElement) grp02.getData().get(6);
        assertThat(grp05.getData().get(0), hasProperty("value", hasProperty("content", equalTo("A new simple value"))));

        final GroupElement grp04a = (GroupElement) grp01.getData().get(5);
        assertThat(grp04a.getData().get(0), hasProperty("value", hasProperty("content", equalTo("yes"))));
        assertThat(grp04a.getData().get(1), hasProperty("value", hasProperty("content", equalTo("A new simple value"))));

        final GroupElement grp04b = (GroupElement) grp01.getData().get(6);
        assertThat(grp04b.getData().get(0), hasProperty("value", hasProperty("content", equalTo("no"))));
        assertThat(grp04b.getData().get(1), hasProperty("value", nullValue()));

    }

    @Test
    public void testAcheteur() throws Exception {
        final EngineContext<FormSpecificationData> engineContext = this.engineContext("aife-acheteur.xml");
        final FormSpecificationData spec = engineContext.getElement();

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "partieI");
        request.setParameter("publication.publicationFaite", "no");
        request.setParameter("publication.numeroAvisJO", "");
        request.setParameter("publication.numeroAvisRecu", "");
        request.setParameter("identite.siretAcheteur", "130+019+771+00018");
        request.setParameter("identite.identiteAcheteur", "DUME");
        request.setParameter("passationMarche.typeProcedure", "01");
        request.setParameter("passationMarche.typeMarche", "02");
        request.setParameter("passationMarche.naturetMarket", "01");
        request.setParameter("passationMarche.numeroReferenceMarche", "1234567890");
        request.setParameter("passationMarche.titre", "");
        request.setParameter("passationMarche.breveDescriptionMarche", "");
        request.setParameter("passationMarche.codecpvs[0].codeCpvComboType", "03000000-1");
        request.setParameter("passationMarche.codecpvs[1].codeCpvComboType", "03100000-2");
        request.setParameter("passationMarche.lotsApplicables[0].numeroLot", "1");
        request.setParameter("passationMarche.lotsApplicables[1].numeroLot", "2");
        request.setParameter("passationMarche.lotsApplicables[2].numeroLot", "3");
        request.setParameter("passationMarche.comboType", "LOT_ONE");
        request.setParameter("passationMarche.maxLot", "");
        request.setParameter("passationMarche.maxLotByOe", "");

        new DataModelRequestUpdater(engineContext).update(request);

        final GroupElement grp01 = spec.getGroups().get(0);
        final GroupElement passationMarche = (GroupElement) grp01.getData().get(2);
        GroupElement codecpvs = (GroupElement) passationMarche.getData().get(6);
        ValueElement valueElement = new ValueElement(new TextValueElement("03000000-1", "03000000-1"));
        assertEquals(valueElement.toString(), codecpvs.getData().get(0).getValue().toString());

        codecpvs = (GroupElement) passationMarche.getData().get(7);
        valueElement = new ValueElement(new TextValueElement("03100000-2", "03100000-2"));
        assertEquals(valueElement.toString(), codecpvs.getData().get(0).getValue().toString());

        GroupElement lotsApplicables = (GroupElement) passationMarche.getData().get(8);
        valueElement = new ValueElement("1");
        assertEquals(valueElement.toString(), lotsApplicables.getData().get(0).getValue().toString());

        lotsApplicables = (GroupElement) passationMarche.getData().get(9);
        valueElement = new ValueElement("2");
        assertEquals(valueElement.toString(), lotsApplicables.getData().get(0).getValue().toString());

        lotsApplicables = (GroupElement) passationMarche.getData().get(10);
        valueElement = new ValueElement("3");
        assertEquals(valueElement.toString(), lotsApplicables.getData().get(0).getValue().toString());

        final DataElement comboType = (DataElement) passationMarche.getData().get(11);
        assertEquals("[{ \"id\": \"LOT_ONE\", \"value\": \"LOT_ONE\" }]", comboType.getValue().toString());
    }

    @Test
    public void testRetroCompatibility() throws Exception {
        final EngineContext<FormSpecificationData> engineContext = this.engineContext("retro/data.xml");
        final DataModelRequestUpdater updater = new DataModelRequestUpdater(engineContext).setClearMissingValues(false);
        final FormContentData contextData = FormContentReader.read(this.resourceAsBytes("retro/data.json"));

        updater.update(contextData.withPath("step01"));

        final FormSpecificationData actual = engineContext.getElement();

        assertThat(actual, notNullValue());

        int idx = 0;

        final GroupElement page = actual.getGroups().get(0);

        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("Kept Field #01 Value")));
        assertThat(page.getData().get(idx++).getData().get(0).getValue(), hasProperty("content", equalTo("Kept Field #01 Value")));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("Kept Field #02.0 Value")));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("Kept Field #02.1 Value")));

        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("yes")));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("yes")));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("yes")));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("13001977100018")));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("batman@gotham-city.us")));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", hasProperty("id", equalTo("val1"))));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", hasProperty("elements", contains(hasProperty("id", equalTo("val2"))))));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", hasProperty("id", equalTo("val2"))));
        assertThat(page.getData().get(idx++).getValue(), hasProperty("content", equalTo("30/05/2016")));
        idx++;

        assertThat(page.getData().get(idx++), //
                allOf( //
                        hasProperty("id", equalTo("grp01x1[0]")), //
                        hasProperty("data", contains(hasProperty("value", hasProperty("content", equalTo("New Value #01.0"))))) //
                ) //
        );

        assertThat(page.getData().get(idx++), //
                allOf( //
                        hasProperty("id", equalTo("grp01x1[1]")), //
                        hasProperty("data", contains(hasProperty("value", hasProperty("content", equalTo("New Value #01.1"))))) //
                ) //
        );
    }

}
