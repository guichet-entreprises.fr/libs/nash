/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.test.builder.ProcessElementBuilder;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class AttachmentProcessorTest.
 *
 * @author Adil Bsibiss
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class AttachmentProcessorTest extends AbstractTest {

    /** provider. */
    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
    }

    /**
     * Test mandatory attachement.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMandatoryAttachement() throws Exception {
        when(this.provider.asBytes("types.xml")).thenReturn(this.resourceAsBytes("types.xml"));

        final FormSpecificationData actual = this.process("attachement.js");

        assertThat(actual.getGroups().get(0), hasProperty("data", hasSize(3)));
        assertThat(actual.getGroups().get(0).getData().get(0), allOf(hasProperty("id", equalTo("pj1")), hasProperty("mandatory", equalTo(false))));
        assertThat(actual.getGroups().get(0).getData().get(1), allOf(hasProperty("id", equalTo("pj2")), hasProperty("mandatory", equalTo(true))));
        assertThat(actual.getGroups().get(0).getData().get(2), allOf(hasProperty("id", equalTo("pj3")), hasProperty("mandatory", equalTo(true))));
    }

    /**
     * Process.
     *
     * @param scriptFilename
     *            script filename
     * @return form specification data
     */
    private FormSpecificationData process(final String scriptFilename) {
        final ProcessElement processElement = new ProcessElementBuilder() //
                .id("review") //
                .type("attachment") //
                .input("types.xml") //
                .value(this.resourceAsString(scriptFilename)) //
                .build();

        final Map<String, Object> model = new HashMap<>();
        model.put("lastname", "Doe");
        model.put("firstname", "John");

        final AttachmentProcessor processor = new AttachmentProcessor() //
                .setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.provider).buildEngineContext().target(processElement));

        return processor.execute(model).getContent();
    }

}
