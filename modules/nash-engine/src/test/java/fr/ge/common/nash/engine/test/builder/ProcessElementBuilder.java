/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.test.builder;

import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;

/**
 * Class ProcessElementBuilder.
 *
 * @author Christian Cougourdan
 */
public class ProcessElementBuilder {

    /** _id. */
    private String _id;

    /** _type. */
    private String _type;

    /** _input. */
    private String _input;

    /** _output. */
    private String _output;

    /** _script. */
    private String _script;

    /** _ref. */
    private String _ref;

    /** _value. */
    private String _value;

    /**
     * Build.
     *
     * @return process element
     */
    public ProcessElement build() {
        final ProcessElement elm = new ProcessElement();

        elm.setId(this._id);
        elm.setType(this._type);
        elm.setInput(this._input);
        elm.setOutput(this._output);
        elm.setScript(this._script);
        elm.setRef(this._ref);
        elm.setValue(this._value);

        return elm;
    }

    /**
     * Id.
     *
     * @param value
     *            value
     * @return process element builder
     */
    public ProcessElementBuilder id(final String value) {
        this._id = value;
        return this;
    }

    /**
     * Type.
     *
     * @param value
     *            value
     * @return process element builder
     */
    public ProcessElementBuilder type(final String value) {
        this._type = value;
        return this;
    }

    /**
     * Input.
     *
     * @param value
     *            value
     * @return process element builder
     */
    public ProcessElementBuilder input(final String value) {
        this._input = value;
        return this;
    }

    /**
     * Output.
     *
     * @param value
     *            value
     * @return process element builder
     */
    public ProcessElementBuilder output(final String value) {
        this._output = value;
        return this;
    }

    /**
     * Script.
     *
     * @param value
     *            value
     * @return process element builder
     */
    public ProcessElementBuilder script(final String value) {
        this._script = value;
        return this;
    }

    /**
     * Ref.
     *
     * @param value
     *            value
     * @return process element builder
     */
    public ProcessElementBuilder ref(final String value) {
        this._ref = value;
        return this;
    }

    /**
     * Value.
     *
     * @param value
     *            value
     * @return process element builder
     */
    public ProcessElementBuilder value(final String value) {
        this._value = value;
        return this;
    }

}
