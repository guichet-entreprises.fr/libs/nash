/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_2.value.Phone;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;

/**
 * Tests {@link PhoneValueAdapter}.
 */
public class PhoneValueAdapterTest extends AbstractValueAdapterTest<Phone> {

    private static final Phone PHONE = new Phone("fr", "0123456789", "+33 1 23 45 67 89");

    private final String phoneAsString = String.format(DATA_ELEMENT_FILLED_AS_XML, //
            this.type().name(), //
            String.format(
                    "\n" //
                            + "        <list>\n" //
                            + "            <text id=\"country\">%s</text>\n" //
                            + "            <text id=\"e164\">%s</text>\n" //
                            + "            <text id=\"international\">%s</text>\n" //
                            + "        </list>\n" //
                            + "    ", //
                    PHONE.getCountry(), //
                    PHONE.getE164(), //
                    PHONE.getInternational() //
            ) //
    );

    private final String phoneEmptyAsString = String.format(DATA_ELEMENT_FILLED_AS_XML, //
            this.type().name(), //
            "\n" //
                    + "        <list>\n" //
                    + "            <text id=\"country\"></text>\n" //
                    + "            <text id=\"e164\"></text>\n" //
                    + "            <text id=\"international\"></text>\n" //
                    + "        </list>\n" //
                    + "    " //
    );

    /**
     * {@inheritDoc}
     */
    @Override
    protected IValueAdapter<Phone> type() {
        return new PhoneValueAdapter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        final Map<String, String[]> model = new HashMap<>();

        model.put("country", new String[] { PHONE.getCountry() });
        model.put("e164", new String[] { PHONE.getE164() });
        model.put("international", new String[] { PHONE.getInternational() });

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequest(final Phone actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        final Map<String, String[]> model = new HashMap<>();

        model.put("country", new String[] { "" });
        model.put("e164", new String[] { "" });
        model.put("international", new String[] { "" });

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequestEmpty(final Phone actual, final DataElement dataElement) {
        this.assertFromObjectEmpty(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Phone buildFromObject() {
        return PHONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObject(final Phone actual, final DataElement dataElement) {
        assertThat(actual,
                allOf( //
                        hasProperty("country", equalTo(PHONE.getCountry())), //
                        hasProperty("e164", equalTo(PHONE.getE164())), //
                        hasProperty("international", equalTo(PHONE.getInternational())) //
                ) //
        );
        assertEquals(this.phoneAsString, JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Phone buildFromObjectEmpty() {
        return new Phone("", "", "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObjectEmpty(final Phone actual, final DataElement dataElement) {
        assertNull(actual);
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXml() {
        return this.phoneAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXml(final Phone actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXmlEmpty() {
        return this.phoneEmptyAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlEmpty(final Phone actual, final DataElement dataElement) {
        assertNull(actual);
        assertEquals(this.phoneEmptyAsString, JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlNone(final Phone actual, final DataElement dataElement) {
        this.assertFromObjectEmpty(actual, dataElement);
    }

}
