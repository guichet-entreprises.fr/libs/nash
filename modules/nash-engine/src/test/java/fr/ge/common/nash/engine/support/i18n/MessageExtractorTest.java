/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.support.i18n;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.ge.common.support.i18n.MessageReader;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Tests {@link MessageExtractor}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class MessageExtractorTest extends AbstractTest {

    /**
     * Setup.
     */
    @Before
    public void setup() {
        System.out.println("###### testFormat ###### Locale : " + Locale.getDefault());
        System.out.println("###### testFormat ###### TimeZone : " + TimeZone.getDefault().getID());
    }

    /**
     * Tests {@link MessageExtractor#extractAsScript(MessageReader)}.
     *
     * @throws IOException
     *             Signale qu'une exception I/O est apparue
     */
    @Test
    public void testBaseNameExtractAsScript() throws IOException {
        final MessageReader reader = NashMessageReader.getReader("po/tests", Locale.FRENCH);
        final StringBuilder sb = new StringBuilder();
        sb.append("requirejs(['lib/i18n'], function(i18n) {\n");
        sb.append("i18n.register('fr', {\n");
        sb.append("'page.engine.types.display':'Affichage',\n");
        sb.append("'HelloWorld':'BonjourMonde',\n");
        sb.append("}\n");
        sb.append(");\n");
        sb.append("});");
        Assert.assertEquals(sb.toString(), IOUtils.toString(MessageExtractor.extractAsScript(reader), StandardCharsets.UTF_8));
    }

}
