/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.transition;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;

/**
 *
 * @author ijijon
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class FindChannelEnabledTest extends AbstractTransitionTest {

    final private static String AUTHORITY_ID = "2019-03-TST-FCE-01";
    final private static String AUTHORITY_LABEL = "AUTHORITY_LABEL";
    final private static String AUTHORITY_PATH = "AUTHORITY_PATH";

    protected IService service;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public Object getService() {
        return this.service = mock(IService.class);
    }

    public static interface IService {

        @GET
        @Path("/v1/authority/{entityId}")
        @Produces(MediaType.APPLICATION_JSON)
        HashMap<String, Object> getByEntityId(@PathParam("entityId") String entityId);

        @GET
        @Path("/private/v1/services/authority/{funcId}/channel/active")
        @Produces(MediaType.APPLICATION_JSON)
        JsonNode computeActiveChannel(@PathParam("funcId") String funcId);

    }

    @Test
    public void testMetas() throws Exception {

        final ObjectNode retour = JsonNodeFactory.instance.objectNode();
        retour.put("entityId", AUTHORITY_ID);
        retour.put("label", AUTHORITY_LABEL);
        retour.put("path", AUTHORITY_PATH);

        // Mock service:
        when(this.service.computeActiveChannel(any())).thenReturn(retour);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        verify(this.service).computeActiveChannel(AUTHORITY_ID);

        final List<MetaElement> metas = loader.meta().getMetas();

        assertEquals(metas.size(), 2);
        assertEquals("authority", metas.get(0).getName());
        assertEquals(AUTHORITY_ID, metas.get(0).getValue());

        assertEquals("replay-" + AUTHORITY_ID, metas.get(1).getName());
        assertEquals("findChannelEnabled_0001", metas.get(1).getValue());

        try (OutputStream out = new FileOutputStream(Paths.get("target/resource.zip").toFile())) {
            IOUtils.write(provider.asBytes(), out);
        }
    }

    @Test
    public void testBackofficeChannel() throws Exception {

        final ObjectNode retour = JsonNodeFactory.instance.objectNode();
        retour.put("entityId", AUTHORITY_ID);
        retour.put("label", AUTHORITY_LABEL);
        retour.put("path", AUTHORITY_PATH);

        final ObjectNode backoffice = JsonNodeFactory.instance.objectNode();
        backoffice.put("state", "enabled");

        retour.put("backoffice", backoffice);

        // Mock service:
        when(this.service.computeActiveChannel(any())).thenReturn(retour);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        assertEquals("findPartnerRecord_0001", loader.stepsMgr().find(2).getId());
    }

    @Test
    public void testEmailChannel() throws Exception {

        final ObjectNode retour = JsonNodeFactory.instance.objectNode();
        retour.put("entityId", AUTHORITY_ID);
        retour.put("label", AUTHORITY_LABEL);
        retour.put("path", AUTHORITY_PATH);

        final ObjectNode email = JsonNodeFactory.instance.objectNode();
        email.put("state", "enabled");

        retour.put("email", email);

        // Mock service:
        when(this.service.computeActiveChannel(any())).thenReturn(retour);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        assertEquals("findEmailAddress_0001", loader.stepsMgr().find(2).getId());
    }

    @Test
    public void testAddressChannel() throws Exception {

        final ObjectNode retour = JsonNodeFactory.instance.objectNode();
        retour.put("entityId", AUTHORITY_ID);
        retour.put("label", AUTHORITY_LABEL);
        retour.put("path", AUTHORITY_PATH);

        final ObjectNode address = JsonNodeFactory.instance.objectNode();
        address.put("state", "enabled");

        retour.put("address", address);

        // Mock service:
        when(this.service.computeActiveChannel(any())).thenReturn(retour);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        assertEquals("findPostMailAddress_0001", loader.stepsMgr().find(2).getId());
    }

    @Test
    public void testFtpChannel() throws Exception {

        final ObjectNode retour = JsonNodeFactory.instance.objectNode();
        retour.put("entityId", AUTHORITY_ID);
        retour.put("label", AUTHORITY_LABEL);
        retour.put("path", AUTHORITY_PATH);

        final ObjectNode ftp = JsonNodeFactory.instance.objectNode();
        ftp.put("state", "enabled");

        retour.put("ftp", ftp);

        // Mock service:
        when(this.service.computeActiveChannel(any())).thenReturn(retour);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        assertEquals("findInfoEddie_0001", loader.stepsMgr().find(2).getId());
    }

    @Test
    public void testMandatoryChannel() throws Exception {
        // -->prepare
        final String AUTHORITY_ID = "AUTHORITY_ID";
        final String AUTHORITY_LABEL = "AUTHORITY_LABEL";

        final HashMap<String, Object> authority = new HashMap<String, Object>();
        authority.put("entityId", AUTHORITY_ID);
        authority.put("label", AUTHORITY_LABEL);

        final HashMap<String, Object> backoffice = new HashMap<String, Object>();
        backoffice.put("state", "enabled");
        final HashMap<String, Object> transferChannels = new HashMap<String, Object>();
        transferChannels.put("backoffice", backoffice);
        final HashMap<String, Object> details = new HashMap<String, Object>();
        details.put("transferChannels", transferChannels);
        authority.put("details", details);

        // Mock service:
        when(this.service.getByEntityId(any())).thenReturn(authority);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec-mandatory-channel.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        assertEquals("findPartnerRecord_0001", loader.stepsMgr().find(2).getId());
    }

}
