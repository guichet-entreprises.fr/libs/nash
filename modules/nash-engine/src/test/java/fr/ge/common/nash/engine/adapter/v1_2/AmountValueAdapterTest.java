/**
 * 
 */
package fr.ge.common.nash.engine.adapter.v1_2;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_2.value.Amount;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;

/**
 * @author $Author: aakkou $
 * @version $Revision: 0 $
 */
public class AmountValueAdapterTest extends AbstractValueAdapterTest<Amount> {

    private static final Amount AMOUNT = new Amount("11", "$ 11", "USD");

    private final String amountAsString = String.format(DATA_ELEMENT_FILLED_AS_XML, //
            this.type().name(), //
            String.format("\n" //
                    + "        <list>\n" //
                    + "            <text id=\"amount\">%s</text>\n" //
                    + "            <text id=\"monetary\">%s</text>\n" //
                    + "            <text id=\"currency\">%s</text>\n" //
                    + "        </list>\n" //
                    + "    ", //
                    AMOUNT.getAmount(), //
                    AMOUNT.getMonetary(), //
                    AMOUNT.getCurrency() //
            ) //
    );

    private final String amountEmptyAsString = String.format(DATA_ELEMENT_FILLED_AS_XML, //
            this.type().name(), //
            "\n" //
                    + "        <list>\n" //
                    + "            <text id=\"amount\"></text>\n" //
                    + "            <text id=\"monetary\"></text>\n" //
                    + "            <text id=\"currency\"></text>\n" //
                    + "        </list>\n" //
                    + "    " //
    );

    /**
     * {@inheritDoc}
     */
    @Override
    protected IValueAdapter<Amount> type() {
        return new AmountValueAdapter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        final Map<String, String[]> model = new HashMap<>();

        model.put("amount", new String[] { AMOUNT.getAmount() });
        model.put("monetary", new String[] { AMOUNT.getMonetary() });
        model.put("currency", new String[] { AMOUNT.getCurrency() });

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequest(final Amount actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        final Map<String, String[]> model = new HashMap<>();

        model.put("amount", new String[] { "" });
        model.put("monetary", new String[] { "" });
        model.put("currency", new String[] { "" });

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequestEmpty(final Amount actual, final DataElement dataElement) {
        this.assertFromObjectEmpty(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Amount buildFromObject() {
        return AMOUNT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObject(final Amount actual, final DataElement dataElement) {
        assertThat(actual, allOf( //
                hasProperty("amount", equalTo(AMOUNT.getAmount())), //
                hasProperty("monetary", equalTo(AMOUNT.getMonetary())), //
                hasProperty("currency", equalTo(AMOUNT.getCurrency())) //
        ) //
        );
        assertEquals(this.amountAsString, JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Amount buildFromObjectEmpty() {
        return new Amount("", "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObjectEmpty(final Amount actual, final DataElement dataElement) {
        assertNull(actual);
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXml() {
        return this.amountAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXml(final Amount actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXmlEmpty() {
        return this.amountEmptyAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlEmpty(final Amount actual, final DataElement dataElement) {
        assertNull(actual);
        assertEquals(this.amountEmptyAsString, JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlNone(final Amount actual, final DataElement dataElement) {
        this.assertFromObjectEmpty(actual, dataElement);
    }

}
