/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor.bridge;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import javax.script.Bindings;
import javax.script.SimpleBindings;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.IFlowElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class TransitionProcessorBridgeTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class TransitionProcessorBridgeTest extends AbstractTest {

    /** The process. */
    @Mock
    private ProcessElement process;

    /** The specification provider. */
    @Mock
    private IProvider specProvider;

    /** The loader. */
    private SpecificationLoader specLoader;

    /** The transition provider. */
    @Mock
    private IProvider transitionProvider;

    /** The transition processor. */
    private TransitionProcessorBridge bridge;

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.specLoader = spy(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider));

        this.bridge = new TransitionProcessorBridge(this.specLoader.buildEngineContext().withScriptProvider(this.transitionProvider));
    }

    /**
     * Test add step.
     *
     * @throws Exception
     *             an exception
     */
    @Test
    public void testAddStep() throws Exception {
        // prepare
        final StepElement previousStep = new StepElement();
        previousStep.setId("previousOne");

        final Bindings bindings = new SimpleBindings();
        bindings.put("id", "fooBarStep");
        bindings.put("label", "Foo Bar step");

        final FormSpecificationDescription descriptionBefore = new FormSpecificationDescription();
        descriptionBefore.setSteps(new StepsElement(Arrays.asList(previousStep)));
        when(this.specProvider.asBean("description.xml", FormSpecificationDescription.class)).thenReturn(descriptionBefore);

        // call
        this.bridge.createStep(bindings);

        // check
        final ArgumentCaptor<FormSpecificationDescription> description = ArgumentCaptor.forClass(FormSpecificationDescription.class);
        verify(this.specLoader).save(eq("description.xml"), description.capture());
        assertThat(description.getValue().getSteps().getElements(), //
                contains( //
                        Arrays.asList( //
                                notNullValue(), //
                                allOf( //
                                        hasProperty("id", equalTo("fooBarStep_0001")), //
                                        hasProperty("label", equalTo("Foo Bar step")) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test add existing step.
     *
     * @throws Exception
     *             an exception
     */
    @Test
    public void testAddExistingStep() throws Exception {
        // prepare
        final Bindings bindings = new SimpleBindings();
        bindings.put("id", "fooBarStep");
        bindings.put("label", "Foo Bar step");
        final FormSpecificationDescription descriptionBefore = new FormSpecificationDescription();
        final StepElement step = new StepElement();
        step.setId("fooBarStep");
        step.setLabel("Foo Bar step");
        step.setPosition(0);

        descriptionBefore.setSteps(new StepsElement(Arrays.asList(step)));
        final StepElement step2 = new StepElement();
        step2.setId("secondStep");
        step2.setLabel("Second  step");
        step2.setPosition(1);

        final List<IFlowElement> flow = descriptionBefore.getFlow();
        flow.add(step2);
        descriptionBefore.setFlow(flow);

        when(this.specProvider.asBean("description.xml", FormSpecificationDescription.class)).thenReturn(descriptionBefore);

        // call
        this.bridge.createStep(bindings);

        // check list contain 1 element and remove all step after existing step
        final ArgumentCaptor<FormSpecificationDescription> description = ArgumentCaptor.forClass(FormSpecificationDescription.class);
        verify(this.specProvider).save(eq("description.xml"), description.capture());
        assertThat(description.getValue().getSteps().getElements(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("id", equalTo("fooBarStep")), //
                                hasProperty("id", equalTo("secondStep")), //
                                allOf( //
                                        hasProperty("id", equalTo("fooBarStep_0002")), //
                                        hasProperty("label", equalTo("Foo Bar step")) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test copy.
     *
     * @throws Exception
     *             an exception
     */
    @Test
    public void testCopy() throws Exception {
        // prepare
        final String fileName = "files/preprocess.xml";
        final byte[] fileContent = "my file".getBytes();
        final FileEntry fileEntry = new FileEntry(fileName);
        fileEntry.asBytes(fileContent);
        when(this.transitionProvider.load(eq(fileName))).thenReturn(fileEntry);

        // call
        this.bridge.copy(fileName, "findReceiver/");

        // check
        verify(this.transitionProvider).load(any(String.class));
        verify(this.specProvider).save(eq("findReceiver/preprocess.xml"), eq(fileContent));
    }

}
