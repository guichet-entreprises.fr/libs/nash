/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.loader;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class SpecificationTranslationTest extends AbstractTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testSimpleAlternateLanguage() throws Exception {
        final SpecificationLoader loader = this.loader("description/simple.xml");

        assertThat(loader.i18n().getAlternateLanguage(null), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("fr"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("en"), equalTo("en"));
        assertThat(loader.i18n().getAlternateLanguage("es"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("de"), equalTo("de"));
    }

    @Test
    public void testMultipleAtlernateLanguage() throws Exception {
        final SpecificationLoader loader = this.loader("description/multiple.xml");

        assertThat(loader.i18n().getAlternateLanguage(null), equalTo("en"));
        assertThat(loader.i18n().getAlternateLanguage("fr"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("en"), equalTo("en"));
        assertThat(loader.i18n().getAlternateLanguage("es"), equalTo("en"));
        assertThat(loader.i18n().getAlternateLanguage("de"), equalTo("de"));
    }

    @Test
    public void testUnregisteredAtlernateLanguage() throws Exception {
        final SpecificationLoader loader = this.loader("description/unregistered.xml");

        assertThat(loader.i18n().getAlternateLanguage(null), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("fr"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("en"), equalTo("en"));
        assertThat(loader.i18n().getAlternateLanguage("es"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("de"), equalTo("de"));
    }

    @Test
    public void testNotFoundAtlernateLanguage() throws Exception {
        final SpecificationLoader loader = this.loader("description/notfound.xml");

        assertThat(loader.i18n().getAlternateLanguage(null), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("fr"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("en"), equalTo("en"));
        assertThat(loader.i18n().getAlternateLanguage("es"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("de"), equalTo("de"));
    }

    @Test
    public void testNoTranslationsAtlernateLanguage() throws Exception {
        final SpecificationLoader loader = this.loader("description/no-translations.xml");

        assertThat(loader.i18n().getAlternateLanguage(null), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("fr"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("en"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("es"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("de"), nullValue());
    }

    @Test
    public void testNoneAtlernateLanguage() throws Exception {
        final SpecificationLoader loader = this.loader("description/none.xml");

        assertThat(loader.i18n().getAlternateLanguage(null), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("fr"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("en"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("es"), nullValue());
        assertThat(loader.i18n().getAlternateLanguage("de"), nullValue());
    }

    private SpecificationLoader loader(final String descriptionResourceName) throws Exception {
        final IProvider provider = spy(IProvider.class);

        Arrays.asList("messages_en.po", "messages_fr.po").forEach(name -> when(provider.asBytes(name)).thenReturn(this.resourceAsBytes("description/" + name)));
        when(provider.asBytes("description.xml")).thenReturn(this.resourceAsBytes(descriptionResourceName));

        return this.applicationContext.getBean(SpecificationLoader.class, null, provider);
    }

}
