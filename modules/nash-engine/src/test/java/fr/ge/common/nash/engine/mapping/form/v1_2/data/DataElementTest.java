/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

import org.junit.Assert;
import org.junit.Test;

/**
 * Class DataElementTest.
 *
 * @author Christian Cougourdan
 */
public class DataElementTest {

    /**
     * Test to string.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testToString() throws Exception {
        final List<UseCaseParameter<? extends Object>> params = Arrays.asList( //
                new UseCaseParameter<>("id", DataElement::setId, "id01", "\"id01\""), //
                new UseCaseParameter<>("label", DataElement::setLabel, "lbl", "\"lbl\""), //
                new UseCaseParameter<>("description", DataElement::setDescription, "desc", "\"desc\""), //
                new UseCaseParameter<>("help", DataElement::setHelp, "hlp", "\"hlp\""), //
                new UseCaseParameter<>("type", DataElement::setType, "Str", "\"Str\""), //
                new UseCaseParameter<>("mandatory", DataElement::setMandatory, true, "true"), //
                new UseCaseParameter<>("ruleAsAttr", DataElement::setRuleAsAttr, "rule as attr", "\"rule as attr\""), //
                new UseCaseParameter<>("ruleAsTag", DataElement::setRuleAsTag, "rule as tag", "\"rule as tag\""), //
                new UseCaseParameter<>("triggerAsAttr", DataElement::setTriggerAsAttr, "trigger as attr", "\"trigger as attr\""), //
                new UseCaseParameter<>("triggerAsTag", DataElement::setTriggerAsTag, "trigger as tag", "\"trigger as tag\""), //
                new UseCaseParameter<>("value", DataElement::setValue, new ValueElement("simple value"), "\"simple value\"") //
        );

        List<List<UseCaseParameter<? extends Object>>> useCases = new ArrayList<>();
        for (final UseCaseParameter<? extends Object> p : params) {
            if (useCases.isEmpty()) {
                useCases.add(Arrays.asList(p));
                useCases.add(Arrays.asList(UseCaseParameter.createEmpty(p)));
            } else {
                final List<List<UseCaseParameter<? extends Object>>> newUseCases = new ArrayList<>();
                useCases.forEach(subLst -> {
                    List<UseCaseParameter<? extends Object>> newSubLst = new ArrayList<>(subLst);
                    newSubLst.add(p);
                    newUseCases.add(newSubLst);

                    newSubLst = new ArrayList<>(subLst);
                    newSubLst.add(UseCaseParameter.createEmpty(p));
                    newUseCases.add(newSubLst);
                });

                useCases = newUseCases;
            }
        }

        for (final List<UseCaseParameter<? extends Object>> useCase : useCases) {
            final DataElement elm = new DataElement();
            final String expected = String.format( //
                    "{ \"id\": %s, \"label\": %s, \"description\": %s, \"help\": %s, \"type\": %s, \"mandatory\": %s, \"if (attr)\": %s, \"if (element)\": %s, \"trigger (attr)\": %s, \"trigger (element)\": %s, \"value\": %s }", //
                    useCase.stream().map(UseCaseParameter::getExpected).toArray() //
            );
            useCase.forEach(fp -> fp.apply(elm));

            Assert.assertEquals(expected, elm.toString());
        }
    }

    /**
     * Class UseCaseParameter.
     *
     * @param <U>
     *            type générique
     */
    private static class UseCaseParameter<U> {

        /** name. */
        private final String name;

        /** setter. */
        private final BiConsumer<DataElement, U> setter;

        /** value. */
        private final U value;

        /** expected. */
        private final String expected;

        /**
         * Instantie un nouveau use case parameter.
         *
         * @param name
         *            name
         * @param setter
         *            setter
         * @param value
         *            value
         * @param expected
         *            expected
         */
        public UseCaseParameter(final String name, final BiConsumer<DataElement, U> setter, final U value, final String expected) {
            this.name = name;
            this.setter = setter;
            this.value = value;
            this.expected = expected;
        }

        /**
         * Apply.
         *
         * @param dataElement
         *            data element
         * @return data element
         */
        public DataElement apply(final DataElement dataElement) {
            this.setter.accept(dataElement, this.value);
            return dataElement;
        }

        /**
         * Getter on attribute {@link #expected}.
         *
         * @return expected
         */
        public String getExpected() {
            return this.expected;
        }

        /**
         * Create empty.
         *
         * @param <T>
         *            type générique
         * @param fp
         *            fp
         * @return use case parameter
         */
        public static <T> UseCaseParameter<T> createEmpty(final UseCaseParameter<T> fp) {
            return new UseCaseParameter<>(fp.name, fp.setter, null, "null");
        }

    }

}
