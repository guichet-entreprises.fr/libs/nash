/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_0;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.FileValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;

/**
 * Class FileValueAdapterTest.
 *
 * @author Christian Cougourdan
 */
public class FileValueAdapterTest extends AbstractValueAdapterTest<List<Item>> {

    private static final List<Item> DATA = Arrays.asList(FileValueAdapter.createItem(1, "file01.PNG"), FileValueAdapter.createItem(2, "file02.PNG"));

    private final String dataAsString = String.format(DATA_ELEMENT_FILLED_AS_XML, //
            this.type().name(), //
            String.format("\n" //
                    + "        <list>\n" //
                    + "            <file ref=\"1\">file01.PNG</file>\n" //
                    + "            <file ref=\"2\">file02.PNG</file>\n" //
                    + "        </list>\n" //
                    + "    ", //
                    DATA.get(0).getId(), //
                    DATA.get(0).getLabel(), //
                    DATA.get(1).getId(), //
                    DATA.get(1).getLabel() //
            ) //
    );

    /**
     * {@inheritDoc}
     */
    @Override
    protected IValueAdapter<List<Item>> type() {
        return new FileValueAdapter();
    }

    /**
     * {@inheritDoc}
     */
    protected List<Item> valueAsTypeSpecificObject() {
        return DATA;
    }

    /**
     * {@inheritDoc}
     */
    protected Matcher<? super List<Item>> expectedAsTypeSpecificObject() {
        return contains( //
                Arrays.asList( //
                        this.expectedAsItem(1, "file01.png"), //
                        this.expectedAsItem(2, "file02.png") //
                ) //
        );
    }

    /**
     * {@inheritDoc}
     */
    protected ValueElement valueAsValueElement() {
        return new ValueElement(new ListValueElement( //
                new FileValueElement("1", "file01.png"), //
                new FileValueElement("2", "file02.png") //
        ));
    }

    /**
     * {@inheritDoc}
     */
    protected Matcher<Object> expectedAsValueElement() {
        return hasProperty("elements", //
                contains( //
                        Arrays.asList( //
                                this.expectedAsFileValueElement("1", "file01.png"), //
                                this.expectedAsFileValueElement("2", "file02.png") //
                        ) //
                ) //
        );
    }

    /**
     * {@inheritDoc}
     */
    protected HttpServletRequest valueAsHttpRequest() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("id", new String[] { "1", "2" });
        request.setParameter("label", new String[] { "file01.png", "file02.png" });
        return request;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        final Map<String, String[]> model = new HashMap<>();

        model.put("id", new String[] { "1", "2" });
        model.put("label", new String[] { "file01.png", "file02.png" });

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequest(final List<Item> actual, final DataElement dataElement) {
        this.assertFromObjectNone(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        final Map<String, String[]> model = new HashMap<>();

        model.put("id", new String[] {});
        model.put("label", new String[] {});

        return model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequestEmpty(final List<Item> actual, final DataElement dataElement) {
        assertTrue(actual.isEmpty());
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected void assertFromHttpRequestNone(final List<Item> actual, final DataElement dataElement) {
        this.assertFromObjectNone(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Item> buildFromObject() {
        return Arrays.asList(FileValueAdapter.createItem(1, "file01.png"), FileValueAdapter.createItem(2, "file02.png"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObject(final List<Item> actual, final DataElement dataElement) {
        assertThat(actual, contains( //
                Arrays.asList( //
                        this.expectedAsItem(1, "file01.png"), //
                        this.expectedAsItem(2, "file02.png") //
                ) //
        ) //
        );
        assertEquals(this.dataAsString, JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Item> buildFromObjectEmpty() {
        return Arrays.asList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObjectEmpty(final List<Item> actual, final DataElement dataElement) {
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObjectNone(final List<Item> actual, final DataElement dataElement) {
        assertTrue(actual.isEmpty());
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXml() {
        return this.dataAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXml(final List<Item> actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXmlEmpty() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlEmpty(final List<Item> actual, final DataElement dataElement) {
        // Nothing to do
    }

    @Override
    public void testFromXmlEmpty() throws Exception {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlNone(final List<Item> actual, final DataElement dataElement) {
        this.assertFromObjectNone(actual, dataElement);
    }

    private Matcher<FileValueElement> expectedAsFileValueElement(String ref, String label) {
        return allOf( //
                instanceOf(FileValueElement.class), //
                hasProperty("ref", equalTo(ref)), //
                hasProperty("label", equalTo(label)) //
        );
    }

    private Matcher<Item> expectedAsItem(int id, String label) {
        return allOf( //
                instanceOf(FileValueAdapter.Item.class), //
                hasProperty("id", equalTo(id)), //
                hasProperty("label", equalTo(label)) //
        );
    }

    @Test
    public void toLowerCaseExtensionTest() {
        Item filePng = FileValueAdapter.createItem(1, "File.PNG");
        assertEquals("File.png", filePng.getLabel());
        assertNotEquals("File.PNG", filePng.getLabel());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testSerialize() throws Exception {
        final Item item = FileValueAdapter.createItem(12, "test.txt", "text/plain", () -> "test content".getBytes(StandardCharsets.UTF_8));
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writeValueAsString(item);

        final Map<String, Object> actual = objectMapper.readValue(json, Map.class);
        assertEquals(12, actual.get("id"));
        assertEquals("test.txt", actual.get("label"));
        assertEquals("text/plain", actual.get("mimeType"));
        assertNull(actual.get("content"));
        assertNull(actual.get("hash"));
    }

}
