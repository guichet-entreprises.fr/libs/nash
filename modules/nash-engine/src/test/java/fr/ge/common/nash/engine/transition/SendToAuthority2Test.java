package fr.ge.common.nash.engine.transition;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class SendToAuthority2Test extends AbstractTransitionTest {

    @Autowired
    private ApplicationContext applicationContext;

    protected IService service;

    @Override
    public Object getService() {
        return this.service = mock(IService.class);
    }

    @Test
    public void test() throws Exception {

        // Mock service:
        when(this.service.execute(any(), any())).thenReturn("{listAuthorities={0={role=TDR, authority=2020-01-GGM-GZD-38}}}");

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);
        this.execute(loader);

        final FormSpecificationData data = loader.data(loader.lastStepDone());

        final List<IElement<?>> elements = data.getGroups().get(0).getData().get(0).getData();
        final List<IElement<?>> elements2 = data.getGroups().get(0).getData().get(1).getData();

        assertEquals("algo", elements.get(0).getId());
        assertEquals("key", elements.get(1).getData().get(0).getId());
        assertEquals("codeCommune".toString(), "" + elements.get(1).getData().get(0).getValue().toString().replace("\"", ""));
        assertEquals("01006".toString(), "" + elements.get(1).getData().get(1).getValue().toString().replace("\"", ""));

        assertEquals("funcId", elements2.get(0).getId());
        assertEquals("{listAuthorities={0={role=TDR, authority=2020-01-GGM-GZD-38}}}".toString(), "" + elements2.get(0).getValue().toString().replace("\"", ""));

    }

    public static interface IService {

        @POST
        @Path("v1/algo/{code}/execute")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.TEXT_PLAIN)
        String execute(@PathParam("code") String code, Map<String, String> metas);

    }

}
