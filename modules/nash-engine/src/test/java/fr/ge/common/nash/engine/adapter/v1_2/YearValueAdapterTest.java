/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.adapter.v1_2;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.test.builder.DataElementBuilder;

/**
 * @author Christian Cougourdan
 *
 */
public class YearValueAdapterTest extends AbstractValueAdapterTest<Calendar> {

    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyyy");

    private final String rawValue = "2018";

    /** type. */
    private final YearValueAdapter type = new YearValueAdapter();

    /**
     * Test from xml bad format.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromXmlBadFormat() throws Exception {
        final DataElement dataElement = new DataElementBuilder().value("oups").build();
        final Calendar actual = this.type.get(null, dataElement);
        assertNull(actual);
    }

    /**
     * Test from xml null.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFromXmlNull() throws Exception {
        final DataElement dataElement = new DataElementBuilder().value((String) null).build();
        final Calendar actual = this.type.get(null, dataElement);
        assertNull(actual);
    }

    /**
     * Test from xml empty.
     *
     * @throws Exception
     *             exception
     */
    @Override
    @Test
    public void testFromXmlEmpty() throws Exception {
        final DataElement dataElement = new DataElementBuilder().value("").build();
        final Calendar actual = this.type.get(null, dataElement);
        assertNull(actual);
    }

    /**
     * Test to xml from calendar.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testToXmlFromCalendar() throws Exception {
        final Calendar now = Calendar.getInstance();
        final DataElement dataElement = new DataElement();
        final Object actual = this.type.set(dataElement, now);

        assertThat(actual, equalTo(new DateTime(now).withTimeAtStartOfDay().withDayOfYear(1).toCalendar(Locale.getDefault())));
        assertThat(dataElement, //
                hasProperty("value", //
                        hasProperty("content", equalTo(new DateTime(now).toString(FORMATTER))) //
                ) //
        );
    }

    /**
     * Test to xml null.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testToXmlNull() throws Exception {
        final DataElement dataElement = new DataElement();
        final Object actual = this.type.set(dataElement, null);
        assertNull(dataElement.getValue());
        assertNull(actual);
    }

    /**
     * Test to xml from string.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testToXmlFromString() throws Exception {
        final DataElement dataElement = new DataElement();
        final DateTime today = DateTime.now().withTimeAtStartOfDay().withDayOfYear(1);
        final Object actual = this.type.set(dataElement, today.toString(FORMATTER));

        assertThat(actual, equalTo(today.toCalendar(Locale.getDefault())));
        assertThat(dataElement.getValue(), //
                hasProperty("content", equalTo(today.toString(FORMATTER))) //
        );
    }

    @Override
    protected IValueAdapter<Calendar> type() {
        return new YearValueAdapter();
    }

    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        return Collections.singletonMap("", new String[] { this.rawValue });
    }

    @Override
    protected void assertFromHttpRequest(final Calendar actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        return Collections.singletonMap("", new String[] { "" });
    }

    @Override
    protected void assertFromHttpRequestEmpty(final Calendar actual, final DataElement dataElement) {
        this.assertFromObjectEmpty(actual, dataElement);
    }

    @Override
    protected Calendar buildFromObject() {
        return FORMATTER.parseDateTime(this.rawValue).toCalendar(Locale.getDefault());
    }

    @Override
    protected void assertFromObject(final Calendar actual, final DataElement dataElement) {
        assertEquals(this.buildFromObject(), actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), this.rawValue), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected Calendar buildFromObjectEmpty() {
        return null;
    }

    @Override
    protected void assertFromObjectEmpty(final Calendar actual, final DataElement dataElement) {
        assertNull(actual);
        assertEquals(String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected byte[] buildFromXml() {
        return String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), this.rawValue).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    protected void assertFromXml(final Calendar actual, final DataElement dataElement) {
        assertEquals(this.buildFromObject(), actual);
    }

    @Override
    protected byte[] buildFromXmlEmpty() {
        return String.format(DATA_ELEMENT_EMPTY_AS_XML, this.type().name()).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    protected void assertFromXmlEmpty(final Calendar actual, final DataElement dataElement) {
        assertNull(actual);
    }

    @Override
    protected void assertFromXmlNone(final Calendar actual, final DataElement dataElement) {
        assertNull(actual);
    }

}
