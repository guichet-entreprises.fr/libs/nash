/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor.bridge.document;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.junit.Test;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.manager.processor.bridge.document.pdf.PdfFieldDescription;
import fr.ge.common.utils.test.AbstractTest;

public class PdfDocumentTest extends AbstractTest {

    private final FileValueAdapter fileValueAdapter = new FileValueAdapter();

    @Test
    public void testGetFields() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.pdf");
        try (final PdfDocument doc = new PdfDocument(resourceAsBytes)) {
            final Map<String, PdfFieldDescription> actual = doc.getFields();
            assertThat(actual.get("TextField"), //
                    allOf( //
                            hasProperty("type", equalTo("text")), //
                            hasProperty("description", equalTo("Text input field")), //
                            hasProperty("value", equalTo("Default value for mandatory TextField")), //
                            hasProperty("required", equalTo(true)), //
                            hasProperty("multi", equalTo(false)), //
                            hasProperty("choices", nullValue()) //
                    ) //
            );
            assertThat(actual.get("PasswordField"), //
                    allOf( //
                            hasProperty("type", equalTo("password")), //
                            hasProperty("description", equalTo("Password input field")), //
                            hasProperty("value", nullValue()), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(false)), //
                            hasProperty("choices", nullValue()) //
                    ) //
            );
            assertThat(actual.get("MultiTextField"), //
                    allOf( //
                            hasProperty("type", equalTo("text")), //
                            hasProperty("description", equalTo("Multiline text input field")), //
                            hasProperty("value", equalTo("Default value for\rMulti-line TextField")), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(true)), //
                            hasProperty("choices", nullValue()) //
                    ) //
            );
            assertThat(actual.get("ComboBox"), //
                    allOf( //
                            hasProperty("type", equalTo("combo")), //
                            hasProperty("description", equalTo("Choice list using combo box")), //
                            hasProperty("value", equalTo("ComboBoxElm02")), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(false)), //
                            hasProperty("choices", //
                                    contains( //
                                            Arrays.asList( //
                                                    allOf( //
                                                            hasProperty("code", equalTo("ComboBoxElm01")), //
                                                            hasProperty("value", equalTo("Combo Box Elm 01")) //
                                                    ), //
                                                    allOf( //
                                                            hasProperty("code", equalTo("ComboBoxElm02")), //
                                                            hasProperty("value", equalTo("Combo Box Elm 02")) //
                                                    ), //
                                                    allOf( //
                                                            hasProperty("code", equalTo("ComboBoxElm03")), //
                                                            hasProperty("value", equalTo("Combo Box Elm 03")) //
                                                    ) //
                                            ) //
                                    ) //
                            ) //
                    ) //
            );
            assertThat(actual.get("ListBox"), //
                    allOf( //
                            hasProperty("type", equalTo("list")), //
                            hasProperty("description", equalTo("Choice list using list box")), //
                            hasProperty("value", contains("ListBoxElm01", "ListBoxElm03")), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(true)), //
                            hasProperty("choices", //
                                    contains( //
                                            Arrays.asList( //
                                                    allOf( //
                                                            hasProperty("code", equalTo("ListBoxElm01")), //
                                                            hasProperty("value", equalTo("List Box Elm 01")) //
                                                    ), //
                                                    allOf( //
                                                            hasProperty("code", equalTo("ListBoxElm02")), //
                                                            hasProperty("value", equalTo("List Box Elm 02")) //
                                                    ), //
                                                    allOf( //
                                                            hasProperty("code", equalTo("ListBoxElm03")), //
                                                            hasProperty("value", equalTo("List Box Elm 03")) //
                                                    ) //
                                            ) //
                                    ) //
                            ) //
                    ) //
            );
            assertThat(actual.get("CheckBox"), //
                    allOf( //
                            hasProperty("type", equalTo("checkbox")), //
                            hasProperty("description", equalTo("Check boxes input field")), //
                            hasProperty("value", equalTo("NoValue")), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(true)), //
                            hasProperty("choices", //
                                    contains( //
                                            Arrays.asList( //
                                                    allOf( //
                                                            hasProperty("code", equalTo("YesValue")), //
                                                            hasProperty("value", nullValue()) //
                                                    ), //
                                                    allOf( //
                                                            hasProperty("code", equalTo("NoValue")), //
                                                            hasProperty("value", nullValue()) //
                                                    ) //
                                            ) //
                                    ) //
                            ) //
                    ) //
            );
            assertThat(actual.get("PushButton"), //
                    allOf( //
                            hasProperty("type", equalTo("button")), //
                            hasProperty("description", equalTo("Push button input field")), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(false)), //
                            hasProperty("choices", nullValue()) //
                    ) //
            );
            assertThat(actual.get("RadioButton"), //
                    allOf( //
                            hasProperty("type", equalTo("radio")), //
                            hasProperty("description", equalTo("Radio button input field")), //
                            hasProperty("value", equalTo("RadioButtonElm01")), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(false)), //
                            hasProperty("choices", //
                                    contains( //
                                            Arrays.asList( //
                                                    allOf( //
                                                            hasProperty("code", equalTo("RadioButtonElm01")), //
                                                            hasProperty("value", nullValue()) //
                                                    ), //
                                                    allOf( //
                                                            hasProperty("code", equalTo("RadioButtonElm02")), //
                                                            hasProperty("value", nullValue()) //
                                                    ) //
                                            ) //
                                    ) //
                            ) //
                    ) //
            );
            assertThat(actual.get("Signature"), //
                    allOf( //
                            hasProperty("type", equalTo("signature")), //
                            hasProperty("description", equalTo("Document signature input field")), //
                            hasProperty("value", nullValue()), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(false)), //
                            hasProperty("choices", nullValue()) //
                    ) //
            );
            assertThat(actual.get("BarCode"), //
                    allOf( //
                            hasProperty("type", equalTo("text")), //
                            hasProperty("description", equalTo("Barcode input field")), //
                            hasProperty("required", equalTo(false)), //
                            hasProperty("multi", equalTo(true)), //
                            hasProperty("choices", nullValue()) //
                    ) //
            );
        }
    }

    /**
     * Adding no blank page.
     * 
     * @throws Exception
     */
    @Test
    public void testSaveWithNoBlankPage() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("one-page.pdf");
        try (final PdfDocument docFinal = new PdfDocument(resourceAsBytes)) {
            docFinal.append(true, new PdfDocument(resourceAsBytes).save("append-one-page.pdf"));
            final Item itemFinal = docFinal.save("result.pdf");
            PDDocument pdDocument = PDDocument.load(itemFinal.getContent());
            assertEquals(pdDocument.getNumberOfPages(), 4);
        }
    }

    /**
     * Adding one blank page for the first and last appended files which has an
     * even number of pages. There is no need to have blank page at the end the
     * document
     * 
     * @throws Exception
     */
    @Test
    public void testSaveAddingOneBlankPage() throws Exception {

        final byte[] resourceAsBytes = this.resourceAsBytes("one-page.pdf");

        try (final PdfDocument docFinal = new PdfDocument(resourceAsBytes)) {
            docFinal.append(true, new PdfDocument(resourceAsBytes).save("append-one-page.pdf"));
            docFinal.append(true, new PdfDocument(resourceAsBytes).save("append-one-page.pdf"));
            final Item itemFinal = docFinal.save("result.pdf");
            PDDocument pdDocument = PDDocument.load(itemFinal.getContent());
            assertEquals(pdDocument.getNumberOfPages(), 6);
        }
    }

    /**
     * Adding one blank page for the first appended file except the last one.
     * 
     * @throws Exception
     */
    @Test
    public void testAddingOneBlankPageExceptLastResource() throws Exception {

        final byte[] resourceAsBytes = this.resourceAsBytes("one-page.pdf");
        final byte[] resourceTwoPagesAsBytes = this.resourceAsBytes("two-pages.pdf");

        try (final PdfDocument docFinal = new PdfDocument(resourceAsBytes)) {
            docFinal.append(true, new PdfDocument(resourceAsBytes).save("append-one-page.pdf"));
            docFinal.append(true, new PdfDocument(resourceTwoPagesAsBytes).save("append-two-pages.pdf"));
            docFinal.append(true, new PdfDocument(resourceAsBytes).save("append-one-page.pdf"));
            final Item itemFinal = docFinal.save("result.pdf");
            PDDocument pdDocument = PDDocument.load(itemFinal.getContent());
            assertEquals(pdDocument.getNumberOfPages(), 8);
        }
    }

    /**
     * The method <b>apply</b> sets fields to readOnly after they are set. Here
     * we test 2 things : values set to fields after appl
     * 
     * @throws Exception
     */
    @Test
    public void testApply() throws Exception {

        final byte[] resourceAsBytes = this.resourceAsBytes("simple.pdf");

        try (final PdfDocument doc = new PdfDocument(resourceAsBytes)) {
            Map<String, Object> hm = new HashMap<String, Object>();
            String value = "test";
            hm.put("TextField", value);
            doc.apply(hm);
            final Item itemFinal = doc.save("result.pdf");
            PDDocument pdDocument = PDDocument.load(itemFinal.getContent());
            final PDAcroForm form = pdDocument.getDocumentCatalog().getAcroForm();

            form.getFields().forEach((field) -> {
                if (field instanceof PDTextField) {
                    try {
                        field.setValue(value);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (field instanceof PDCheckBox) {
                    try {
                        ((PDCheckBox) field).check();
                    } catch (final IOException ex) {
                        throw new TechnicalException(String.format("Error while setting [%s] checkbox value [%s] : %s", field.getFullyQualifiedName(), value, ex.getMessage()), ex);
                    }
                }
            });
            form.getFields().forEach((field) -> {
                if (field instanceof PDTextField) {
                    assertEquals(value, field.getValueAsString());
                }
                if (field instanceof PDCheckBox) {
                    assertTrue(((PDCheckBox) field).isChecked());
                }
            });
            if (null != form) {
                form.getFields().forEach((field) -> {
                    assertTrue(field.isReadOnly());
                });
            }

        }
    }

    /**
     * Test appending images (jpg, jpeg and png)
     * 
     * @throws Exception
     */
    @Test
    public void testSaveAddingImages() throws Exception {

        final byte[] onePageAsBytes = this.resourceAsBytes("one-page.pdf");

        final Item jpg = fileValueAdapter.createItem(0, "image.jpg", "jpg", this.resourceAsBytes("image-jpg.jpg"));
        final Item jpeg = fileValueAdapter.createItem(1, "image.jpeg", "jpeg", this.resourceAsBytes("image-jpeg.jpeg"));
        final Item png = fileValueAdapter.createItem(2, "image.png", "png", this.resourceAsBytes("image-png.png"));

        final Item JPG = fileValueAdapter.createItem(3, "image.JPG", "JPG", this.resourceAsBytes("image.JPG"));
        final Item JPEG = fileValueAdapter.createItem(4, "image.JPEG", "JPEG", this.resourceAsBytes("image.JPEG"));
        final Item PNG = fileValueAdapter.createItem(5, "image.PNG", "PNG", this.resourceAsBytes("image.PNG"));

        try (final PdfDocument docFinal = new PdfDocument(onePageAsBytes)) {
            docFinal //
                    .append(jpg) //
                    .append(jpeg) //
                    .append(png) //
                    .append(JPG) //
                    .append(JPEG) //
                    .append(PNG);
            final Item itemFinal = docFinal.save("result.pdf");
            PDDocument pdDocument = PDDocument.load(itemFinal.getContent());
            assertEquals(pdDocument.getNumberOfPages(), 7);
        }
    }

    /**
     * The method <b>apply</b> sets fields to readOnly after they are set. Here
     * we test 2 things : values set to fields after appl
     * 
     * @throws Exception
     */

    /**
     * Create a PDF from scratch applying same template twice.
     * 
     * @throws Exception
     */
    @Test
    public void testApplySameDocument() throws Exception {
        try (final PdfDocument document = new PdfDocument()) {

            final byte[] resourceAsBytes = this.resourceAsBytes("read-jqpa.pdf");

            for (int i = 0; i < 2; i++) {
                try (final PdfDocument doc = new PdfDocument(resourceAsBytes)) {
                    final Map<String, Object> hm = new HashMap<String, Object>();
                    hm.put("entreprise_siren", "test");
                    document.append(doc.apply(hm).save("result.pdf"));
                }
            }

            document.save("final.pdf");
            final Map<String, PdfFieldDescription> actual = document.getFields();
            assertThat(actual, notNullValue());
        }
    }

    /**
     * Create a PDF from scratch applying multiple times same template.
     * 
     * @throws Exception
     */
    @Test
    public void testApplySamePDF() throws Exception {
        try (final PdfDocument document = new PdfDocument(this.resourceAsBytes("simple.pdf"))) {

            final byte[] resourceAsBytes = this.resourceAsBytes("cerfa_11686-07_TNS.pdf");

            for (int i = 0; i < 2; i++) {
                try (final PdfDocument doc = new PdfDocument(resourceAsBytes)) {
                    final Map<String, Object> hm = new HashMap<String, Object>();
                    hm.put("signature_TNS", "Signature");
                    document.append(doc.apply(hm).save("result.pdf"));
                }
            }

            document.save("final.pdf");
            final Map<String, PdfFieldDescription> actual = document.getFields();
            assertThat(actual, notNullValue());
            assertThat(document.getNumberOfPages(), equalTo(3));
        }
    }

    /**
     * Test apply with hyphen character
     * 
     * @throws Exception
     */
    @Test
    public void testApplyWithHyphen() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.pdf");
        final String fieldName = "TextField";
        final String hyphenValue = "livrai­son à vélo de repas, plis et colis ;";
        final String expectedValue = "livraison à vélo de repas, plis et colis ;";

        try (final PdfDocument doc = new PdfDocument(resourceAsBytes)) {
            final Map<String, Object> hm = new HashMap<String, Object>();
            hm.put(fieldName, hyphenValue);
            doc.apply(hm);
            final Item itemFinal = doc.save("result.pdf");
            final PDDocument pdDocument = PDDocument.load(itemFinal.getContent());
            final PDAcroForm form = pdDocument.getDocumentCatalog().getAcroForm();
            form.getFields().forEach((field) -> {
                if (field instanceof PDTextField && field.getPartialName().equals(fieldName)) {
                    assertEquals(expectedValue, ((PDTextField) field).getValue());
                }
            });
        }
    }

    /**
     * Create a PDF from scratch applying multiple times same template.
     * 
     * @throws Exception
     */
    @Test
    public void testApplyDistinctTemplateWithSameField() throws Exception {
        final Map<String, Object> hm = new HashMap<String, Object>();
        hm.put("modifSituationpersonnelle", false);
        hm.put("modifEtablissement", false);
        hm.put("modifTransfert", false);
        hm.put("modifAutre", true);
        hm.put("p2_professionLiberale", true);
        hm.put("p2_artisteAuteur", false);
        hm.put("siren", "676767676");
        hm.put("microentrepreneurOui", false);
        hm.put("microentrepreneurNon", true);
        hm.put("numeroSS", "0000000000000");
        hm.put("numeroSSCle", "00");
        hm.put("personneLiee_personnePhysique_nomNaissance", "frh");
        hm.put("personneLiee_personnePhysique_nomUsage", "");
        hm.put("personneLiee_personnePhysique_prenom1", "sreyhrt");
        hm.put("personneLiee_personnePhysique_dateNaissance", "17062004");
        hm.put("personneLiee_personnePhysique_lieuNaissanceDepartement", "");
        hm.put("personneLiee_personnePhysique_lieuNaissanceCommune", "seryres");
        hm.put("personneLiee_personnePhysique_lieuNaissancePays", "ANTIGUA - ET - BARBUDA");
        hm.put("eirl_immatriculation", false);
        hm.put("eirl_modification", false);
        hm.put("eirl_affectationRetraitPatrimoine", false);
        hm.put("modifConcerneActivite", false);
        hm.put("modifConcerneTransfert", false);
        hm.put("modifConcerneOuverture", false);
        hm.put("modifConcerneFermeture", false);
        hm.put("modifConcerneAutre", true);
        hm.put("modifDateEtablissementOuvertModifier", "11062020");
        hm.put("modifNouvelleAdresseEtablissement_voieAdresse", "gsrty");
        hm.put("modifNouvelleAdresseEtablissement_cpAdresse", "78000");
        hm.put("modifNouvelleAdresseEtablissement_communeAdresse", "VERSAILLES");
        hm.put("modifCategarieEtablissementOuvert_effectifPresenceOui", false);
        hm.put("modifCategarieEtablissementOuvert_effectifPresenceNon", true);
        hm.put("modifCategarieEtablissementModifier_principal", true);
        hm.put("modifDateObservations", "11062020");
        hm.put("observations", "Modification de la date de dÃ©but d 'activitÃ© / ");
        hm.put("adresseCorrespondanceCadre_coche", true);
        hm.put("adresseCorrespondanceCadre_numero", "12");
        hm.put("adressesCorrespondanceAutre", false);
        hm.put("telephone1", "");
        hm.put("telephone2", "+33 1 01 01 01 01");
        hm.put("courriel", "");
        hm.put("diffusionInformation", false);
        hm.put("nonDiffusionInformation", true);
        hm.put("signataireDeclarant", true);
        hm.put("signatureDate", "11062020");
        hm.put("signatureLieu", "fqzet");
        hm.put("estEIRL_oui", false);
        hm.put("estEIRL_non", true);
        hm.put("signature", "");
        hm.put("formulaire_dependance_P0PL", false);
        hm.put("formulaire_dependance_P0PLME", false);
        hm.put("formulaire_dependance_P2PL", true);
        hm.put("formulaire_dependance_P4PL", false);
        hm.put("formulaire_dependance_AC0", false);
        hm.put("formulaire_dependance_AC2", false);
        hm.put("formulaire_dependance_AC4", false);
        hm.put("declaration_initiale", false);
        hm.put("declaration_modification", false);
        hm.put("eirl_siren", "676767676");
        hm.put("eirl_nomNaissance", "frh");
        hm.put("eirl_nomUsage", "");
        hm.put("eirl_prenom", "sreyhr");

        try (final PdfDocument document = new PdfDocument(this.resourceAsBytes("tpl1.pdf")).apply(hm)) {

            try (final PdfDocument doc = new PdfDocument(this.resourceAsBytes("tpl2.pdf"))) {
                document.append(doc.apply(hm).save("result.pdf"));
            }

            document.save("final.pdf");
            final Map<String, PdfFieldDescription> actual = document.getFields();
            assertThat(actual, notNullValue());
            assertThat(document.getNumberOfPages(), equalTo(4));
        }
    }
}
