/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2.data.value;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;

/**
 * Class ListValueElementTest.
 *
 * @author Christian Cougourdan
 */
public class ListValueElementTest {

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        final ListValueElement elm = new ListValueElement();
        elm.setElements(Arrays.asList(new TextValueElement("first"), new TextValueElement("second")));

        final String actual = elm.toString();

        assertThat(actual, equalTo("{ \"id\": null, \"value\": [ { \"id\": null, \"value\": \"first\" }, { \"id\": null, \"value\": \"second\" } ] }"));
    }

    /**
     * Test to string empty.
     */
    @Test
    public void testToStringEmpty() {
        final ListValueElement elm = new ListValueElement(new ArrayList<>());

        final String actual = elm.toString();

        assertThat(actual, equalTo("{ \"id\": null, \"value\": null }"));
    }

    /**
     * Test to string null.
     */
    @Test
    public void testToStringNull() {
        final ListValueElement elm = new ListValueElement();

        final String actual = elm.toString();

        assertThat(actual, equalTo("{ \"id\": null, \"value\": null }"));
    }

}
