package fr.ge.common.nash.engine.manager.processor.bridge;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Properties;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.test.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class TrackerProcessorBridgeTest extends AbstractTest {

    /** The Constant ENDPOINT. */
    protected static final String ENDPOINT = "http://localhost:8888/test";

    /** The server. */
    protected Server server;

    /** The service. */
    protected ITestService service;

    /** The context. */
    protected EngineContext<?> context;

    /** provider. */
    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        this.service = mock(ITestService.class);

        final Properties properties = new Properties();
        properties.put("tracker.baseUrl", ENDPOINT);
        properties.put("tracker.author", "nash");

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.service);
        serverFactory.setProvider(jsonProvider);
        this.server = serverFactory.create();

        reset(this.provider);
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
        when(this.provider.asBytes("description.xml")).thenReturn(this.resourceAsBytes("description.xml"));

        final Configuration cfg = new Configuration(properties);
        this.context = this.applicationContext.getBean(SpecificationLoader.class, cfg, this.provider).buildEngineContext();
    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             the exception
     */
    @After
    public void tearDown() throws Exception {
        this.server.stop();
        this.server.destroy();
    }

    /**
     * The Interface ITestService.
     */
    @Path("/v1/uid")
    public static interface ITestService {

        @POST
        @Path("/{uid}/ref/{ref}")
        String addReference(@PathParam("uid") String uid, @PathParam("ref") String ref, @QueryParam("author") String author);

        @POST
        @Path("/{ref}/msg")
        String addMessage(@PathParam("ref") String ref, @QueryParam("content") String content, @QueryParam("author") String author);
    }

    /**
     * Test link.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testLink() throws Exception {
        when(this.service.addReference(any(), any(), any())).thenReturn("");

        try (TrackerProcessorBridge bridge = new TrackerProcessorBridge(this.context)) {
            bridge.link("456");
        }

        verify(this.service).addReference(any(), any(), any());
    }

    /**
     * Test posting a message.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostMessage() throws Exception {
        when(this.service.addMessage(any(), any(), any())).thenReturn("");

        try (TrackerProcessorBridge bridge = new TrackerProcessorBridge(this.context)) {
            bridge.post("This is a new message");
        }

        verify(this.service).addMessage(any(), any(), any());
    }

    /**
     * Test posting a message.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testLinkPostMessage() throws Exception {
        when(this.service.addReference(any(), any(), any())).thenReturn("");
        when(this.service.addMessage(any(), any(), any())).thenReturn("");

        try (TrackerProcessorBridge bridge = new TrackerProcessorBridge(this.context)) {
            bridge.post("ENVOI-MAIL-IN", "This is a new message");
        }

        verify(this.service).addMessage(any(), any(), any());
    }

    /**
     * Test posting a message with missing configuration.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostMessageMisssingPropertiesConfiguration() throws Exception {
        final Configuration cfg = new Configuration(new Properties());
        final SpecificationLoader loader = mock(SpecificationLoader.class);
        when(loader.getConfiguration()).thenReturn(cfg);
        final EngineContext<SpecificationLoader> context = EngineContext.build(loader);
        final FormSpecificationDescription description = new FormSpecificationDescription();
        description.setAuthor("author");
        when(loader.description()).thenReturn(description);
        when(this.service.addMessage(any(), any(), any())).thenReturn("");

        try (TrackerProcessorBridge bridge = new TrackerProcessorBridge(context)) {
            bridge.post("This is a new message");
        }

        verify(this.service, never()).addMessage(any(), any(), any());
    }
}
