package fr.ge.common.nash.engine.transition;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/application-context-test.xml", "/spring/test-context.xml" })
public class FindEmailAddressTest extends AbstractTransitionTest {

    protected IService service;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public Object getService() {
        return this.service = mock(IService.class);
    }

    @Test
    public void test() throws Exception {

        final String AUTHORITY_ID = "AUTHORITY_ID";
        final String AUTHORITY_LABEL = "AUTHORITY_LABEL";

        // Prepare authority mock :
        final HashMap<String, Object> authority = new HashMap<String, Object>();
        authority.put("entityId", AUTHORITY_ID);
        authority.put("label", AUTHORITY_LABEL);

        final HashMap<String, Object> email = new HashMap<String, Object>();
        email.put("emailAddress", "findemailaddress@mail.com");
        email.put("state", "enabled");
        final HashMap<String, Object> transferChannels = new HashMap<String, Object>();
        transferChannels.put("email", email);
        final HashMap<String, Object> details = new HashMap<String, Object>();
        details.put("transferChannels", transferChannels);
        authority.put("details", details);

        // Mock service:
        when(this.service.getByEntityId(any())).thenReturn(authority);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        final FormSpecificationData data = loader.data(loader.lastStepDone());

        final GroupElement output = (GroupElement) data.getGroups().get(0).getData().get(1);

        assertEquals("output", output.getId());
        assertEquals("\"findemailaddress@mail.com\"", output.getData().get(0).getValue().toString());

    }

    public static interface IService {

        @GET
        @Path("/v1/authority/{entityId}")
        @Produces(MediaType.APPLICATION_JSON)
        HashMap<String, Object> getByEntityId(@PathParam("entityId") String entityId);

    }
}
