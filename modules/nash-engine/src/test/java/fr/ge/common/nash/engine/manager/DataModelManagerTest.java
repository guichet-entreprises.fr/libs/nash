/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.AbstractLoaderTest;
import fr.ge.common.nash.engine.manager.extractor.IDataModelExtractor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.script.NashScriptEngine;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.test.hamcrest.IsMapWithSize;
import fr.ge.common.nash.engine.util.TypeDeclaration;
import fr.ge.common.utils.CoreUtil;

/**
 * Class DataModelManagerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/application-context-test.xml", "/spring/test-context.xml" })
public class DataModelManagerTest extends AbstractLoaderTest {

    /**
     * Test update.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUpdate() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "group01");
        request.setParameter("field01", "John");
        request.setParameter("field02", "51");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("data.xml"), null).updateFromRequest(request);
        final List<? extends IElement<?>> lst = actual.getGroups().get(0).getData();

        assertNotNull(actual);
        assertEquals("John", ((DataElement) lst.get(0)).getValue().getContent());
        assertEquals("51", ((DataElement) lst.get(1)).getValue().getContent());
        assertEquals(null, ((DataElement) lst.get(2)).getValue());
        assertEquals("no", ((DataElement) lst.get(3)).getValue().getContent());
    }

    /**
     * Test update with condition.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUpdateWithCondition() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "group01");
        request.setParameter("field01", "yes");
        request.setParameter("field02", "yes");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("data-with-condition.xml"), null).updateFromRequest(request);
        final List<? extends IElement<?>> lst = actual.getGroups().get(0).getData();

        assertNotNull(actual);
        assertEquals("yes", ((DataElement) lst.get(0)).getValue().getContent());
        assertEquals("no", ((DataElement) lst.get(1)).getValue().getContent());
    }

    /**
     * Test update recursive.
     */
    @Test
    public void testUpdateRecursive() throws Exception {

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "group02");
        request.setParameter("field11", "value11");
        request.setParameter("group03.field31", "value31");
        request.setParameter("group03.field32", "42");
        request.setParameter("group03.group04.field41", "value42");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("data.xml"), null).updateFromRequest(request);

        assertThat(actual.getGroups(), //
                contains( //
                        Arrays.asList( //
                                isA(GroupElement.class), //
                                isA(GroupElement.class), //
                                isA(GroupElement.class) //
                        ) //
                ) //
        );

        assertThat(actual.getGroups().get(1).getData().get(5).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("value31"))), //
                                hasProperty("value", hasProperty("content", equalTo("42"))), //
                                hasProperty("data", hasSize(1)), //
                                hasProperty("data", hasSize(1)) //
                        ) //
                ) //
        );

        assertThat(actual.getGroups().get(1).getData().get(5).getData().get(2).getData(), //
                contains( //
                        hasProperty("value", //
                                hasProperty("content", equalTo("value42")) //
                        ) //
                ) //
        );

        assertThat(actual.getGroups().get(1).getData().get(5).getData().get(3).getData(), //
                contains( //
                        hasProperty("value", //
                                nullValue()) //
                ) //
        );

        assertThat(actual.getGroups().get(1).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("value11"))), //
                                hasProperty("value", nullValue()), //
                                hasProperty("value", hasProperty("content", equalTo("no"))), //
                                hasProperty("value", nullValue()), //
                                hasProperty("value", nullValue()), //
                                notNullValue() //
                        ) //
                ) //
        );

    }

    /**
     * Test update double condition.
     *
     * @throws Exception
     *             exception
     */
    @Test
    @Ignore
    public void testUpdateDoubleCondition() throws Exception {

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "group01");

        try {
            new DataModelManager(this.engineContext("data-double-condition.xml"), null).updateFromRequest(request);
            fail("TechnicalException expected");
        } catch (final TechnicalException ex) {
            assertEquals("Error on field 'field01' : dependencies on next field(s) found", ex.getMessage());
        }
    }

    /**
     * Test update without form id.
     */
    @Test
    public void testUpdateWithoutFormId() throws Exception {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("group.id")).thenReturn(null);

        try {
            new DataModelManager(this.engineContext("data.xml"), null).updateFromRequest(request);
            fail("TechnicalException expected");
        } catch (final TechnicalException ex) {
            assertEquals("Update form record from request : no group ID specified", ex.getMessage());
        }
    }

    /**
     * Test update sub field conditions.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUpdateSubFieldConditions() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("group.id", "group03");
        request.setParameter("field31.country", "fr");
        request.setParameter("field31.e164", "+33612345678");
        request.setParameter("field31.international", "+33 6 12 34 56 78");

        final FormSpecificationData actual = new DataModelManager(this.engineContext("data.xml"), null).updateFromRequest(request);

        assertThat(((ListValueElement) ((ValueElement) actual.getGroups().get(2).getData().get(0).getValue()).getContent()).getElements(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("country")), //
                                        hasProperty("value", equalTo("fr")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("e164")), //
                                        hasProperty("value", equalTo("+33612345678")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("international")), //
                                        hasProperty("value", equalTo("+33 6 12 34 56 78")) //
                                ) //
                        ) //
                ) //
        );

        assertThat(actual.getGroups().get(2).getData().get(1), //
                hasProperty("value", nullValue()) //
        );
    }

    /**
     * Test specific extraction.
     */
    @Test
    public void testSpecificExtraction() throws Exception {
        final FormSpecificationData spec = this.resourceAsBean("data.xml", FormSpecificationData.class);

        final IDataModelExtractor extractor = mock(IDataModelExtractor.class);
        final DataModelManager mgr = new DataModelManager(this.engineContext("data.xml"), extractor);

        mgr.extract();

        verify(extractor).extract(spec);
    }

    /**
     * Test recursive extraction.
     */
    @Test
    public void testRecursiveExtraction() throws Exception {
        final Map<String, Object> model = new DataModelManager(this.engineContext("data.xml")).extractRecursive();

        assertThat(model, //
                allOf( //
                        IsMapWithSize.hasSize(1), //
                        hasKey("form01") //
                ) //
        );

        final Map<String, Object> form01 = CoreUtil.cast(model.get("form01"));
        assertThat(form01, //
                allOf( //
                        IsMapWithSize.hasSize(3), //
                        hasKey("group01"), //
                        hasKey("group02"), //
                        hasKey("group03") //
                ) //
        );

        final Map<String, Object> group01 = CoreUtil.cast(form01.get("group01"));
        assertThat(group01, //
                allOf( //
                        IsMapWithSize.hasSize(4), //
                        hasEntry("field01", (Object) "Field 01 value"), //
                        hasEntry("field02", (Object) 42), //
                        hasEntry("field03", (Object) null), //
                        hasEntry("field04", (Object) true) //
                ) //
        );

        final Map<String, Object> group02 = CoreUtil.cast(form01.get("group02"));
        assertThat(group02, //
                allOf( //
                        IsMapWithSize.hasSize(6), //
                        allOf( //
                                hasEntry("field11", (Object) "Field 11 value"), //
                                hasEntry("field12", (Object) 51), //
                                hasEntry("field13", (Object) false), //
                                hasEntry("field14", (Object) "Field 14 value"), //
                                hasEntry("field15", (Object) null), //
                                hasKey("group03") //
                        ) //
                ));

        final Map<String, Object> group03 = CoreUtil.cast(group02.get("group03"));
        assertThat(group03, //
                allOf( //
                        IsMapWithSize.hasSize(4), //
                        hasEntry("field31", (Object) null), //
                        hasEntry("field32", (Object) null) //
                ) //
        );

        final Map<String, Object> group04 = CoreUtil.cast(group03.get("group04"));
        assertThat(group04, //
                allOf( //
                        IsMapWithSize.hasSize(1), //
                        hasEntry("field41", (Object) null) //
                ) //
        );

        final Map<String, Object> group05 = CoreUtil.cast(group03.get("group05"));
        assertThat(group05, //
                allOf( //
                        IsMapWithSize.hasSize(1), //
                        hasEntry("field51", (Object) "Field 51 value") //
                ) //
        );
    }

    /**
     * Test evaluate.
     */
    @Test
    public void testEvaluate() throws Exception {
        final EngineContext<FormSpecificationData> stepEngineContext = this.engineContext("data-eval.xml");
        final EngineContext<GroupElement> engineContext = stepEngineContext.target(stepEngineContext.getElement().getGroups().get(0));
        final NashScriptEngine engine = engineContext.getRecord().getScriptEngine();
        final ScriptExecutionContext ctx = engineContext.buildExpressionContext();

        assertEquals("Doe", engine.eval("$lastname", true, ctx));
        assertEquals("Doe", engine.eval("$current.lastname", true, ctx));
        assertEquals("Doe", engine.eval("$form.group01.lastname", true, ctx));
        assertEquals(true, engine.eval("$lastname == 'Doe'", true, ctx));
        assertEquals(false, engine.eval("$lastname == 'Not Doe'", true, ctx));
        assertEquals(false, engine.eval("$lastname != 'Doe'", true, ctx));
        assertEquals(false, engine.eval("not($lastname == 'Doe')", true, ctx));
        assertEquals(true, engine.eval("$lastname == 'Doe' and $firstname != 'Doe'", true, ctx));
        assertEquals("John Doe", engine.eval("$firstname + ' ' + $lastname", true, ctx));
        assertEquals(42, engine.eval("6 * 7", true, ctx));
        assertEquals(42, ((Number) engine.eval("6 * $count", true, ctx)).intValue());

        assertTrue(engine.eval("$lastname == 'Doe'", true, ctx, Boolean.class));
        assertTrue(engine.eval("1", true, ctx, Boolean.class));
        assertFalse(engine.eval("0", true, ctx, Boolean.class));

        assertTrue(engine.eval("$major", true, ctx, Boolean.class));
        assertFalse(engine.eval("$died", true, ctx, Boolean.class));
        assertFalse(engine.eval("$unknown", true, ctx, Boolean.class));
    }

    /**
     * Test types.
     */
    @Test
    public void testTypes() {
        final FormSpecificationData spec = this.resourceAsBean("data-types.xml", FormSpecificationData.class);

        final Map<String, TypeDeclaration> actual = DataModelManager.types(spec, spec.getGroups().get(0));

        assertThat(actual.size(), equalTo(3));
        assertThat(actual.get("grp01.fld01"), //
                allOf( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("String")), //
                                hasProperty("optionString", nullValue()), //
                                hasProperty("extraOptions", nullValue()) //
                        ) //
                ) //
        );
        assertThat(actual.get("grp01.fld02"), //
                allOf( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("String")), //
                                hasProperty("optionString", equalTo("min: 3, max: 12")), //
                                hasProperty("extraOptions", nullValue()) //
                        ) //
                ) //
        );
        assertThat(actual.get("grp01.fld03"), //
                allOf( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("String")), //
                                hasProperty("optionString", equalTo("min: 3, max: 12")), //
                                hasProperty("extraOptions", //
                                        allOf( //
                                                hasEntry("placeholder", "Foo placeholder"), //
                                                hasEntry("pattern", "XXXX-XX-XXX-XXX-XX") //
                                        ) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test {@link DataModelManager#data(FormSpecificationData, GroupElement)}.
     */
    @Test
    public void testData() {
        final FormSpecificationData spec = this.resourceAsBean("data.xml", FormSpecificationData.class);

        final Map<String, DataElement> actual = DataModelManager.data(spec, spec.getGroups().get(0));

        assertThat(actual, IsMapWithSize.hasSize(4));
        assertThat(actual.get("field01"), allOf(hasProperty("id", equalTo("field01")), hasProperty("type", equalTo("String"))));
        assertThat(actual.get("field02"), allOf(hasProperty("id", equalTo("field02")), hasProperty("type", equalTo("Integer"))));
        assertThat(actual.get("field03"), allOf(hasProperty("id", equalTo("field03")), hasProperty("type", equalTo("String"))));
        assertThat(actual.get("field04"), allOf(hasProperty("id", equalTo("field04")), hasProperty("type", equalTo("Boolean"))));
    }

}
