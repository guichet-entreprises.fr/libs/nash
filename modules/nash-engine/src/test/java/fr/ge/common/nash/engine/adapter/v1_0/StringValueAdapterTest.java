/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.adapter.v1_0;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;

/**
 * @author Christian Cougourdan
 *
 */
public class StringValueAdapterTest extends AbstractValueAdapterTest<String> {

    private final String value = "field 01 value";

    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        return Collections.singletonMap("", new String[] { this.value });
    }

    @Override
    protected void assertFromHttpRequest(final String actual, final DataElement dataElement) {
        assertEquals(this.value, actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), this.value), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        return Collections.singletonMap("", new String[] { "" });
    }

    @Override
    protected void assertFromHttpRequestEmpty(final String actual, final DataElement dataElement) {
        assertEquals("", actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), ""), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected String buildFromObject() {
        return this.value;
    }

    @Override
    protected void assertFromObject(final String actual, final DataElement dataElement) {
        assertEquals(this.value, actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), this.value), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected String buildFromObjectEmpty() {
        return "";
    }

    @Override
    protected void assertFromObjectEmpty(final String actual, final DataElement dataElement) {
        assertEquals("", actual);
        assertEquals(String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), ""), JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    @Override
    protected byte[] buildFromXml() {
        return String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), this.value).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    protected void assertFromXml(final String actual, final DataElement dataElement) {
        assertEquals(this.value, actual);
    }

    @Override
    protected byte[] buildFromXmlEmpty() {
        return String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "").getBytes(StandardCharsets.UTF_8);
    }

    @Override
    protected void assertFromXmlEmpty(final String actual, final DataElement dataElement) {
        assertNull(actual);
    }

    @Override
    protected void assertFromXmlNone(final String actual, final DataElement dataElement) {
        assertNull(actual);
    }

    @Override
    protected IValueAdapter<String> type() {
        return new StringValueAdapter();
    }

}
