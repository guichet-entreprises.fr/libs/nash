package fr.ge.common.nash.engine.transition;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class PreparePostMailTest extends AbstractTransitionTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void test() throws Exception {

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        this.execute(loader);
        this.execute(loader);

        final FormSpecificationData data = loader.data(loader.lastStepDone());
        final GroupElement groupElement = data.getGroups().get(0);

        final List<IElement<?>> elements = data.getGroups().get(0).getData().get(0).getData();
        final String entryDataType = ((DataElement) elements.get(0)).getType();

        assertEquals("request", groupElement.getId());
        assertEquals("content", groupElement.getData().get(0).getId());
        assertEquals("entry", groupElement.getData().get(0).getData().get(0).getId());
        assertEquals("PreviewFile", entryDataType);

        assertEquals("mergePdf_0001", loader.stepsMgr().current().getId());

    }

}
