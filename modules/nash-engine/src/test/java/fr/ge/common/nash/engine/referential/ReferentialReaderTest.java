/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.referential;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Tests {@link ReferentialReader}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class ReferentialReaderTest extends AbstractTest {

    /** object mapper. */
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testReadRef() throws Exception {
        this.testRead("specRef");
    }

    @Test
    public void testReadInline() throws Exception {
        this.testRead("specInline");
    }

    @Test
    public void testReadRefInline() throws Exception {
        this.testRead("specRefInline");
    }

    @Test
    public void testReadComplexDisplayCondition() throws Exception {
        this.testRead("specComplexDisplayCondition");
    }

    @Test
    public void testReadExternal() throws Exception {
        this.testRead("specExternal");
    }

    @Test
    public void testFindLabelsRef() {
        final Map<String, String> labels = this.testFindLabels("specRef");
        assertThat(labels, notNullValue());
        assertThat(labels.get("ref11"), equalTo("ref1 - text1"));
        assertThat(labels.get("ref12"), equalTo("ref1 - text2"));
    }

    @Test
    public void testFindLabelsInline() {
        final Map<String, String> labels = this.testFindLabels("specInline");
        assertThat(labels, notNullValue());
        assertThat(labels.get("code11"), equalTo("Valeur 1-1"));
        assertThat(labels.get("code13"), equalTo("Valeur 1-3"));
        assertThat(labels.get("code15"), equalTo("Valeur 1-5"));
        assertThat(labels.get("code17"), equalTo("Valeur 1-7"));
    }

    @Test
    public void testFindLabelsRefInline() {
        final Map<String, String> labels = this.testFindLabels("specRefInline");
        assertThat(labels, notNullValue());
        assertThat(labels.get("ref11"), equalTo("ref1 - text1"));
        assertThat(labels.get("code11"), equalTo("Valeur 1-1"));
        assertThat(labels.get("code13"), equalTo("Valeur 1-3"));
        assertThat(labels.get("code15"), equalTo("Valeur 1-5"));
        assertThat(labels.get("code17"), equalTo("Valeur 1-7"));
    }

    @Test
    public void testFindLabelsComplexDisplayCondition() {
        final Map<String, String> labels = this.testFindLabels("specComplexDisplayCondition");
        assertThat(labels, notNullValue());
        assertThat(labels.get("code11"), equalTo("Valeur 1-1"));
        assertThat(labels.get("code12"), equalTo("Valeur 1-2"));
        assertThat(labels.get("ref11"), equalTo("ref1 - text1"));
        assertThat(labels.get("ref12"), equalTo("ref1 - text2"));
    }

    public void testRead(final String testCase) throws Exception {
        // prepare call
        final IProvider provider = new ZipProvider(this.resourceAsBytes(testCase + ".zip"));
        final SpecificationLoader specLoader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);
        final FormSpecificationData spec = specLoader.data(0);
        final GroupElement group = spec.getGroups().get(0);

        final String expectedAsString = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this.objectMapper.readValue(this.resourceAsString(testCase + ".json"), Object.class));

        // call
        final Map<String, FieldProcessedReferentialsData> actual = new ReferentialReader().read(specLoader, spec, group);
        final String actualAsString = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(actual);

        assertEquals(expectedAsString, actualAsString);

        // checks
        // assertThat(expected, notNullValue());
        // assertThat(expected, equalTo(content));
    }

    public Map<String, String> testFindLabels(final String testCase) {
        // prepare call
        final IProvider provider = new ZipProvider(this.resourceAsBytes(testCase + ".zip"));
        final SpecificationLoader specLoader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);
        final FormSpecificationData spec = specLoader.data(0);
        final GroupElement group = spec.getGroups().get(0);
        final DataElement dataElement = (DataElement) group.getData().get(0);

        // call
        final Map<String, String> labels = new ReferentialReader().findLabels(specLoader, dataElement);
        return labels;
    }

}
