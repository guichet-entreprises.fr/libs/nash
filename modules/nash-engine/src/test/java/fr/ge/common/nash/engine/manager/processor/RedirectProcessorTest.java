/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.ProcessModelManager;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class RedirectProcessorTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class RedirectProcessorTest extends AbstractTest {

    /** The provider. */
    private final IProvider provider = mock(IProvider.class);

    /** The ticker. */
    private final Ticker ticker = mock(Ticker.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider, this.ticker);
    }

    /**
     * Test nominal.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNominal() throws Exception {
        when(this.provider.asBytes("redirect.js")).thenReturn("$test.go('redirect'); return {\"message\":\"a test redirection\",\"url\":\"local://test/redirect\"};".getBytes(StandardCharsets.UTF_8));
        when(this.provider.asBytes("script.js")).thenReturn("$test.go('script'); return null;".getBytes(StandardCharsets.UTF_8));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final FormSpecificationProcess process = new FormSpecificationProcess();
        process.setId("proc");
        process.setProcesses( //
                Arrays.asList( //
                        this.buildProcess("p1", "redirect", "redirect.js"), //
                        this.buildProcess("p2", "script", "script.js") //
                ) //
        );

        final IProcessResult<?> actual = ProcessModelManager.create(loader, process, this.provider).execute(Collections.singletonMap("test", this.ticker));

        assertThat(actual, notNullValue());
        assertThat(actual.isInterrupting(), equalTo(true));
        assertThat(actual.getContent(), //
                allOf( //
                        hasProperty("message", equalTo("a test redirection")), //
                        hasProperty("url", equalTo("local://test/redirect")) //
                ) //
        );

        verify(this.ticker, times(1)).go("redirect");
        verify(this.ticker, never()).go("script");
    }

    /**
     * Test no redirect.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNoRedirect() throws Exception {
        when(this.provider.asBytes("redirect.js")).thenReturn("$test.go('redirect'); return null;".getBytes(StandardCharsets.UTF_8));
        when(this.provider.asBytes("script.js")).thenReturn("$test.go('script'); return null;".getBytes(StandardCharsets.UTF_8));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final FormSpecificationProcess process = new FormSpecificationProcess();
        process.setId("proc");
        process.setProcesses( //
                Arrays.asList( //
                        this.buildProcess("p1", "redirect", "redirect.js"), //
                        this.buildProcess("p2", "script", "script.js") //
                ) //
        );

        final IProcessResult<?> actual = ProcessModelManager.create(loader, process, this.provider).execute(Collections.singletonMap("test", this.ticker));

        assertThat(actual, notNullValue());
        assertThat(actual.isInterrupting(), equalTo(false));
        assertThat(actual.getContent(), nullValue());

        verify(this.ticker, times(1)).go("redirect");
        verify(this.ticker, times(1)).go("script");
    }

    /**
     * Builds the process.
     *
     * @param id
     *            the id
     * @param type
     *            the type
     * @param script
     *            the script
     * @return the i process
     */
    private IProcess buildProcess(final String id, final String type, final String script) {
        final ProcessElement process = new ProcessElement();
        process.setId(id);
        process.setType(type);
        process.setScript(script);

        return process;
    }

    /**
     * The Interface Ticker.
     */
    private static interface Ticker {

        /**
         * Go.
         *
         * @param code
         *            the code
         */
        void go(String code);

    }
}
