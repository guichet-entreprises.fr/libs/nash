/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.ProcessModelManager;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.test.AbstractBeanResourceTest;
import fr.ge.common.utils.bean.FileEntry;

/**
 * The Class TransitionProcessorTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class TransitionProcessorTest extends AbstractBeanResourceTest {

    /** provider. */
    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
        when(this.provider.getAbsolutePath(any(String.class))).thenCallRealMethod();
        when(this.provider.asBytes(any(String.class))).thenCallRealMethod();

        // final byte[] resourceAsBytes = asBytes(name);
        // if (null == resourceAsBytes) {
        // return null;
        // } else {
        // return new FileEntry(name, "application/octetstream") //
        // .lastModified(Calendar.getInstance()) //
        // .asBytes(resourceAsBytes) //
        // ;
        // }
    }

    @Test
    public void testProcess() throws Exception {
        Arrays.asList("description.xml", "data.xml").forEach(name -> when(this.provider.load(name)).thenReturn( //
                new FileEntry(name, "text/xml") //
                        .lastModified(Calendar.getInstance()) //
                        .asBytes(this.resourceAsBytes("spec/" + name)) //
        ));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final ProcessModelManager mgr = new ProcessModelManager( //
                loader.buildEngineContext() //
                        .target(this.buildProcess("nextOne", "fooBar@1.0")) //
                        .withProvider(this.provider) //
        );

        mgr.execute(null);

        assertThat(loader.description().getSteps().getElements(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("id", equalTo("other")), //
                                hasProperty("id", equalTo("fooBarStep")), //
                                hasProperty("id", equalTo("fooBarStep_0002")), //
                                allOf( //
                                        hasProperty("id", equalTo("fooBarStep_0003")), //
                                        hasProperty("data", equalTo("fooBarStep-0003/display.xml")) //
                                ) //
                        ) //
                ) //
        );

        final FormSpecificationData display = this.retrieveSavedFile("fooBarStep-0003/display.xml");
        assertThat(display, //
                allOf( //
                        hasProperty("id", equalTo("pre01Display")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        final FormSpecificationData input = this.retrieveSavedFile("fooBarStep-0003/input.xml");
        assertThat(input, //
                allOf( //
                        hasProperty("id", equalTo("step01")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

    }

    @Test
    public void testProcessMultipleInput() throws Exception {
        Arrays.asList("description.xml", "data.xml", "data-bis.xml").forEach(name -> when(this.provider.load(name)).thenReturn( //
                new FileEntry(name, "text/xml") //
                        .lastModified(Calendar.getInstance()) //
                        .asBytes(this.resourceAsBytes("spec/" + name) //
                        ) //
        ));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final ProcessModelManager mgr = new ProcessModelManager( //
                loader.buildEngineContext() //
                        .target(this.buildProcess("nextOne", "fooBar@1.0", "data.xml", "data-bis.xml")) //
                        .withProvider(this.provider) //
        );

        mgr.execute(null);

        final FormSpecificationData input = this.retrieveSavedFile("fooBarStep-0003/input.xml");
        assertThat(input, //
                allOf( //
                        hasProperty("id", equalTo("step01")), //
                        hasProperty("groups", hasSize(2)) //
                ) //
        );

        assertThat(input.getGroups(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("id", equalTo("grp01")), //
                                hasProperty("id", equalTo("grp03")) //
                        ) //
                ) //
        );

        assertThat(input.getGroups().get(0).getData().get(2), //
                hasProperty("id", equalTo("grp02")) //
        );

        assertThat(input.getGroups().get(0).getData().get(2).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("id", equalTo("fld03")), //
                                hasProperty("id", equalTo("fld04")) //
                        ) //
                ) //
        );

        assertThat(input.getGroups().get(0).getData().get(2).getData().get(0), //
                hasProperty("value", hasProperty("content", equalTo("Value #03 bis"))) //
        );
    }

    /**
     * Builds the process.
     *
     * @param id
     *            the id
     * @param ref
     *            the ref
     * @return the process element
     */
    private FormSpecificationProcess buildProcess(final String id, final String ref, final String... additionalInputs) {
        final ProcessElement pre = new ProcessElement();
        pre.setId(id);
        pre.setRef(ref);
        pre.setType("transition");
        pre.setInput(Optional.of(additionalInputs) //
                .filter(ArrayUtils::isNotEmpty) //
                .map(Arrays::stream) //
                .map(stream -> stream.collect(Collectors.joining(", "))) //
                .orElse("data.xml") //
        );

        final FormSpecificationProcess procs = new FormSpecificationProcess();
        procs.setId("pre01");
        procs.setProcesses(Arrays.asList(pre));

        return procs;
    }

    private FormSpecificationData retrieveSavedFile(final String resourceName) {
        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.provider).save(eq(resourceName), captor.capture());
        return JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationData.class);
    }

}
