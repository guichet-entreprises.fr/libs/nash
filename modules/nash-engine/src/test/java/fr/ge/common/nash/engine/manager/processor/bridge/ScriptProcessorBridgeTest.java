/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.manager.processor.bridge;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.IProcessResult;
import fr.ge.common.nash.engine.manager.processor.bridge.script.RecordWrapper;
import fr.ge.common.nash.engine.manager.processor.bridge.script.StepWrapper;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class ScriptProcessorBridgeTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/application-context-test.xml", "/spring/test-context.xml" })
public class ScriptProcessorBridgeTest extends AbstractTest {

    /** The Constant ENDPOINT. */
    protected static final String ENDPOINT = "http://localhost:8888/test";

    /** The server. */
    protected Server server;

    /** The service. */
    protected INashService service;

    /** The context. */
    protected EngineContext<?> context;

    /** The specification provider. */
    private IProvider specProvider;

    /** The loader. */
    private SpecificationLoader specLoader;

    /** The script processor. */
    private ScriptProcessorBridge bridge;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private Engine engine;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        this.service = mock(INashService.class);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.service);
        serverFactory.setProvider(jsonProvider);
        // serverFactory.setTransportId(LocalTransportFactory.TRANSPORT_ID);

        this.server = serverFactory.create();
        this.specProvider = mock(IProvider.class);
        this.specLoader = spy(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider));
        this.bridge = new ScriptProcessorBridge(EngineContext.build(this.specLoader, this.specProvider));
    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             the exception
     */
    @After
    public void tearDown() throws Exception {
        this.server.stop();
        this.server.destroy();
    }

    /**
     * Test profiler.
     */
    @Test
    public void testProfiler() {
        this.engine.getEngineProperties().asMap().put("ws.external.url", ENDPOINT);

        when(this.service.downloadSpecification(any())).thenReturn(Response.ok(this.resourceAsBytes("profiler/dst.zip")).build());
        when(this.service.uploadRecord(any(), any())).thenReturn("/v1/Record/code/2017-09-REC-AAA-02");

        final IProvider provider = new ZipProvider(this.resourceAsBytes("profiler/src.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);

        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);
        loader.validate(loader.data(step));
        final IProcessResult<?> postResult = loader.processes().postExecute(step);

        assertThat(postResult.getContent(), //
                allOf( //
                        hasProperty("url", equalTo("2017-09-REC-AAA-02")), //
                        hasProperty("message", equalTo("All is ok")) //
                ) //
        );

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.service).uploadRecord(captor.capture(), any());

        assertThat(captor.getValue(), notNullValue());

        final SpecificationLoader actual = this.engine.loader(new ZipProvider(captor.getValue()));

        assertThat(actual.description(), hasProperty("formUid", equalTo("2017-09-SPC-AAA-02")));
        assertThat(actual.description(), hasProperty("reference", hasProperty("code", equalTo("R&D/Test/Profiler Result"))));
        assertThat(new String(actual.getProvider().asBytes("step01/data-added.xml"), StandardCharsets.UTF_8), equalTo(this.resourceAsString("profiler/src/step01/data.xml")));
        assertThat(new String(actual.getProvider().asBytes("step02/data-01.xml"), StandardCharsets.UTF_8), equalTo(this.resourceAsString("profiler/src/step01/uploaded/grp01.fld03-1-document.txt")));

        final FormSpecificationData data = actual.data(0);
        assertThat(data.getGroups().get(0), //
                allOf( //
                        hasProperty("id", equalTo("grp01")), //
                        hasProperty("data", hasSize(2)) //
                ) //
        );

        assertThat(data.getGroups().get(0).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("fld01")), //
                        hasProperty("value", hasProperty("content", equalTo("new value"))) //
                ) //
        );

        assertThat(data.getGroups().get(0).getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("fld02")), //
                        hasProperty("value", hasProperty("content", equalTo("Value 02"))) //
                ) //
        );

    }

    /**
     * The Interface INashService.
     */
    public static interface INashService {

        /**
         * Download specification.
         *
         * @param code
         *            the code
         * @return the response
         */
        @GET
        @Path("/v1/Specification/code/{code}/file")
        @Produces("application/zip")
        Response downloadSpecification(@PathParam("code") String code);

        /**
         * Upload record.
         *
         * @param resourceAsBytes
         *            the resource as bytes
         * @param author
         *            the author
         * @return the string
         */
        @POST
        @Path("/v1/Record")
        @Consumes("application/zip")
        String uploadRecord(byte[] resourceAsBytes, @QueryParam("author") String author);

        /**
         * Downloads an item.
         *
         * @param code
         *            the item identifier
         * @return the response
         */
        @GET
        @Path("/v1/Record/code/{code}/file")
        @Produces("application/zip")
        Response download(@PathParam("code") String code);

        /**
         * Upload record file in a specific queue.
         *
         * @param queueCode
         *            destination queue
         * @param overwritePreviousOne
         *            true to remove previous corresponding record (same UID)
         * @param attachment
         *            the attachment
         * @return
         */
        @POST
        @Path("/v1/queue/{queueCode}")
        @Consumes(MediaType.MULTIPART_FORM_DATA)
        Response upload(@PathParam("queueCode") String queueCode, @QueryParam("overwrite") @DefaultValue("true") boolean overwritePreviousOne, Attachment attachment);
    }

    @Test
    @Ignore
    public void testWrap() {
        final Properties properties = new Properties();
        properties.put("ws.external.url", ENDPOINT);
        properties.put("forms.company.creation.public.url", "http://www.google.fr");
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("recordwrap/src.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);
        loader.validate(loader.data(step));
        final IProcessResult<?> postResult = loader.processes().postExecute(step);

        assertThat(postResult.getContent(), //
                allOf( //
                        hasProperty("url", equalTo("http://www.google.fr")), //
                        hasProperty("message", nullValue()) //
                ) //
        );

        final FormSpecificationData spec = loader.getProvider().asBean("/2-finish/data.xml", FormSpecificationData.class);
        assertNotNull(spec);
        assertThat(spec.getGroups(), hasSize(1));
        assertNotNull(spec.getGroups().get(0));
        assertThat(spec.getGroups().get(0).getData(), hasSize(1));
        final GroupElement group = (GroupElement) spec.getGroups().get(0).getData().get(0);
        final DataElement data = (DataElement) group.getData().get(0);
        assertEquals("Vous souhaitez déclarer la création de votre entreprise individuelle ou d'une société", data.getValue().getContent());
    }

    @Test
    public void testBindPreviewFile() {
        final Properties properties = new Properties();
        properties.put("ws.external.url", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("bind-preview/src.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);
        loader.validate(loader.data(step));

        final FormSpecificationData spec = loader.getProvider().asBean("/1-data/data.xml", FormSpecificationData.class);
        assertNotNull(spec);
        assertThat(spec.getGroups(), hasSize(1));
        assertNotNull(spec.getGroups().get(0));
        assertThat(spec.getGroups().get(0).getData(), hasSize(1));
        final GroupElement group = (GroupElement) spec.getGroups().get(0).getData().get(0);
        final DataElement dataString = (DataElement) group.getData().get(0);
        assertEquals("/C1000A1001L166274D20180522H095028TDMTDUPGUEN.zip", dataString.getValue().getContent());

        final DataElement dataPreviewFile = (DataElement) group.getData().get(1);

        final ValueElement valueElement = new ValueElement(new ListValueElement( //
                new TextValueElement("file1", "/file1.xml"), //
                new TextValueElement("file2", "/file2.xml"), //
                new TextValueElement("file3", "/file3.xml") //
        ));
        assertEquals(valueElement.toString(), dataPreviewFile.getValue().toString());
    }

    /**
     * Test a zip file appending input files.
     *
     * @throws Exception
     *             an exception
     */
    @Test
    public void testCreateZipFromInputFiles() throws Exception {
        // prepare
        String fileName = "files/preprocess.xml";
        byte[] fileContent = "my file 1".getBytes();
        FileEntry fileEntry = new FileEntry(fileName);
        fileEntry.asBytes(fileContent);
        when(this.specProvider.load(eq(fileName))).thenReturn(fileEntry);

        fileName = "files/postprocess.xml";
        fileContent = "my file 2".getBytes();
        fileEntry = new FileEntry(fileName);
        fileEntry.asBytes(fileContent);
        when(this.specProvider.load(eq(fileName))).thenReturn(fileEntry);

        when(this.specProvider.getAbsolutePath(eq("record.zip"))).thenReturn("1-data/record.zip");

        // call
        final Item zipItem = this.bridge.zip() //
                .add("file1.xml", "files/preprocess.xml") //
                .add("attachment/file2.xml", "files/postprocess.xml") //
                .save("record.zip");

        assertNotNull(zipItem);
        assertNotNull(zipItem.getContent());
        assertThat(zipItem.getAbsolutePath(), equalTo("1-data/record.zip"));

        // check
        verify(this.specProvider, Mockito.times(2)).load(any(String.class));
        verify(this.specProvider).save(any(String.class), any(byte[].class));
    }

    @Test
    public void testBindSimple() {
        final Properties properties = new Properties();
        final Configuration configuration = new Configuration(properties);

        final IProvider p = name -> null;

        final IProvider provider = mock(IProvider.class);
        when(provider.asBean(any(), any())).thenCallRealMethod();
        when(provider.getAbsolutePath(any())).thenCallRealMethod();

        Arrays.asList("description.xml", "step01.xml", "pre01.xml").forEach(resourceName -> when(provider.asBytes(resourceName)).thenReturn(this.resourceAsBytes("bind/src/" + resourceName)));
        when(provider.asBytes("pre01.js")).thenReturn(this.resourceAsBytes("bind/src/pre01-grp01.js"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);

        final ArgumentCaptor<byte[]> bytesCaptor = ArgumentCaptor.forClass(byte[].class);

        verify(provider).save(eq("step01.xml"), eq("text/xml"), bytesCaptor.capture());
        final FormSpecificationData actual = JaxbFactoryImpl.instance().unmarshal(bytesCaptor.getValue(), FormSpecificationData.class);

        assertThat(actual, notNullValue());

        this.assertBindGroup02(actual);
        this.assertBindGroup03(actual);

        // Group #01
        assertThat(actual.getGroups().get(0).getData().get(0).getData(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("fld01")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #01 (#01) new value"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld02")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #02 (#01) value"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld03[0]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #03.0 (#01) new value"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld03[1]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #03.1 (#01) new value"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld04[0]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #04.0 (#01) new value"))) //
                                ) //
                        ) //
                ) //
        );
    }

    @Test
    public void testBindEmptyMultiGroup() {
        final Properties properties = new Properties();
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = mock(IProvider.class);
        when(provider.asBean(any(), any())).thenCallRealMethod();
        when(provider.getAbsolutePath(any())).thenCallRealMethod();

        Arrays.asList("description.xml", "step01.xml", "pre01.xml").forEach(resourceName -> when(provider.asBytes(resourceName)).thenReturn(this.resourceAsBytes("bind/src/" + resourceName)));
        when(provider.asBytes("pre01.js")).thenReturn(this.resourceAsBytes("bind/src/pre01-grp02.js"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);

        final ArgumentCaptor<byte[]> bytesCaptor = ArgumentCaptor.forClass(byte[].class);

        verify(provider).save(eq("step01.xml"), eq("text/xml"), bytesCaptor.capture());
        final FormSpecificationData actual = JaxbFactoryImpl.instance().unmarshal(bytesCaptor.getValue(), FormSpecificationData.class);

        assertThat(actual, notNullValue());

        this.assertBindGroup01(actual);
        this.assertBindGroup03(actual);

        // Group #02
        assertThat(actual.getGroups().get(0).getData().get(1), hasProperty("id", equalTo("grp02[0]")));
        assertThat(actual.getGroups().get(0).getData().get(1).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("Field #01 (#02.0) new value"))), //
                                hasProperty("value", hasProperty("content", equalTo("Field #02 (#02.0) new value"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testBindExistingMultiGroup() {
        final Properties properties = new Properties();
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = mock(IProvider.class);
        when(provider.asBean(any(), any())).thenCallRealMethod();
        when(provider.getAbsolutePath(any())).thenCallRealMethod();

        Arrays.asList("description.xml", "step01.xml", "pre01.xml").forEach(resourceName -> when(provider.asBytes(resourceName)).thenReturn(this.resourceAsBytes("bind/src/" + resourceName)));
        when(provider.asBytes("pre01.js")).thenReturn(this.resourceAsBytes("bind/src/pre01-grp03.js"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);

        final ArgumentCaptor<byte[]> bytesCaptor = ArgumentCaptor.forClass(byte[].class);

        verify(provider).save(eq("step01.xml"), eq("text/xml"), bytesCaptor.capture());
        final FormSpecificationData actual = JaxbFactoryImpl.instance().unmarshal(bytesCaptor.getValue(), FormSpecificationData.class);

        assertThat(actual, notNullValue());

        this.assertBindGroup01(actual);
        this.assertBindGroup02(actual);

        // Group #03x1
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(0), hasProperty("id", equalTo("grp03x1")));
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(0).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("Field #01 (#03x1) new value"))), //
                                hasProperty("value", hasProperty("content", equalTo("Field #02 (#03x1) value"))) //
                        ) //
                ) //
        );

        // Group #03x2.0
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(1), hasProperty("id", equalTo("grp03x2[0]")));
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(1).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("Field #01 (#03x2.0) new value"))), //
                                hasProperty("value", hasProperty("content", equalTo("Field #02 (#03x2.0) new value"))) //
                        ) //
                ) //
        );

        // Group #03x2.1
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(2), hasProperty("id", equalTo("grp03x2[1]")));
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(2).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("Field #01 (#03x2.1) new value"))), //
                                hasProperty("value", nullValue()) //
                        ) //
                ) //
        );
    }

    private void assertBindGroup01(final FormSpecificationData actual) {
        // Group #01
        assertThat(actual.getGroups().get(0).getData().get(0).getData(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("fld01")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #01 (#01) value"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld02")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #02 (#01) value"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld03[]")), //
                                        hasProperty("value", nullValue()) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld04[0]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #04.0 (#01) value"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld04[1]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #04.1 (#01) value"))) //
                                ) //
                        ) //
                ) //
        );
    }

    private void assertBindGroup02(final FormSpecificationData actual) {
        // Group #02
        assertThat(actual.getGroups().get(0).getData().get(1), hasProperty("id", equalTo("grp02[]")));
        assertThat(actual.getGroups().get(0).getData().get(1).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", nullValue()), //
                                hasProperty("value", nullValue()) //
                        ) //
                ) //
        );
    }

    private void assertBindGroup03(final FormSpecificationData actual) {
        // Group #03x1
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(0), hasProperty("id", equalTo("grp03x1")));
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(0).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("Field #01 (#03x1) value"))), //
                                hasProperty("value", hasProperty("content", equalTo("Field #02 (#03x1) value"))) //
                        ) //
                ) //
        );

        // Group #03x2.0
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(1), hasProperty("id", equalTo("grp03x2[0]")));
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(1).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("Field #01 (#03x2.0) value"))), //
                                hasProperty("value", hasProperty("content", equalTo("Field #02 (#03x2.0) value"))) //
                        ) //
                ) //
        );

        // Group #03x2.1
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(2), hasProperty("id", equalTo("grp03x2[1]")));
        assertThat(actual.getGroups().get(0).getData().get(2).getData().get(2).getData(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("value", hasProperty("content", equalTo("Field #01 (#03x2.1) value"))), //
                                hasProperty("value", hasProperty("content", equalTo("Field #02 (#03x2.1) value"))) //
                        ) //
                ) //
        );
    }

    @Test
    public void testBindReferentials() {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("bind-ref/src.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);

        final FormSpecificationData actual = JaxbFactoryImpl.instance().unmarshal(provider.asBytes("step01.xml"), FormSpecificationData.class);

        assertThat(actual, notNullValue());
        final GroupElement grp01 = (GroupElement) actual.getGroups().get(0).getData().get(0);
        assertThat(grp01, notNullValue());

        final DataElement checkBoxes = (DataElement) grp01.getData().get(0);
        assertThat(checkBoxes, notNullValue());
        assertThat(checkBoxes.getValue(), notNullValue());

        final DataElement urlBase64 = (DataElement) grp01.getData().get(1);
        assertThat(urlBase64, notNullValue());
        assertThat(urlBase64.getValue(), notNullValue());
    }

    
    public void testDuplicateStep() {
        final Properties properties = new Properties();
        properties.put("ws.external.url", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        when(this.service.downloadSpecification(any())).thenReturn(Response.ok(this.resourceAsBytes("step-duplicate/dst.zip")).build());
        when(this.service.uploadRecord(any(), any())).thenReturn("/v1/Record/code/2017-09-REC-AAA-02");

        final IProvider provider = new ZipProvider(this.resourceAsBytes("step-duplicate/src.zip"));
        assertThat(provider.resources("2-finish/data*.*"), containsInAnyOrder("2-finish/data.xml"));
        assertThat(provider.resources("2-finish/**/*.*"), containsInAnyOrder(//
                "2-finish/uploaded/document.txt", //
                "2-finish/data.xml", //
                "2-finish/preprocess.xml", //
                "2-finish/preprocess.js") //
        );
        assertThat(provider.resources("2-finish/*.xml"), containsInAnyOrder("2-finish/data.xml", "2-finish/preprocess.xml"));
        assertThat(provider.resources("2-finish/uploaded/*"), containsInAnyOrder("2-finish/uploaded/document.txt"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);
        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);
        loader.validate(loader.data(step));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.service).uploadRecord(captor.capture(), any());

        final byte[] recordAsBytes = captor.getValue();
        assertThat(recordAsBytes, notNullValue());

        final List<String> resources = new ZipProvider(recordAsBytes).resources("archived-0002/**/*.*");
        assertThat(resources, hasSize(3));
        assertThat(resources, containsInAnyOrder(//
                "archived-0002/data.xml", //
                "archived-0002/preprocess.xml", //
                "archived-0002/preprocess.js") //
        );
    }

    @Test(expected = TechnicalException.class)
    public void testStepNotFound() {
        final Map<String, String> properties = new HashMap<>();
        properties.put("ws.external.url", ENDPOINT);
        this.engine.getEngineProperties().asMap().putAll(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("step-duplicate/src.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);
        final EngineContext<SpecificationLoader> ctx = loader.buildEngineContext();

        final RecordWrapper recordWrapper = new RecordWrapper(ctx);
        final StepWrapper stepWrapper = recordWrapper.step("archived");
        assertThat(stepWrapper, notNullValue());
    }

    @Test
    public void testLoadAndUploadRecord() {
        final Map<String, String> properties = new HashMap<>();
        properties.put("providers.alias.forms", ENDPOINT);
        properties.put("providers.alias.markov", ENDPOINT);
        this.engine.getEngineProperties().asMap().putAll(properties);

        when(this.service.uploadRecord(any(), any())).thenReturn("/v1/Record/code/2017-09-REC-AAA-02");
        when(this.service.download(any())).thenReturn(Response.ok(this.resourceAsBytes("load-record/dst.zip")).build());
        when(this.service.upload(eq("AWAIT"), eq(true), any())).thenReturn(Response.ok().build());

        final IProvider provider = new ZipProvider(this.resourceAsBytes("load-record/src.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);
        final StepElement step = loader.stepsMgr().current();
        loader.processes().preExecute(step);
        loader.validate(loader.data(step));
    }
}
