/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.isEmptyOrNullString;

import java.util.Arrays;

import org.hamcrest.Matcher;

/**
 * Class FormSpecificationDescriptionTest.
 *
 * @author Christian Cougourdan
 */
public class FormSpecificationDescriptionTest extends AbstractFormSpecificationTest<FormSpecificationDescription> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<FormSpecificationDescription> getSpecClass() {
        return FormSpecificationDescription.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getCssReference() {
        return "css/description.css";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Matcher<FormSpecificationDescription> matcher() {
        final Matcher<Iterable<?>> stepsMatcher = hasProperty("elements", //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("context")), //
                                        hasProperty("data", equalTo("0-context/data.xml")), //
                                        hasProperty("icon", equalTo("question")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("data")), //
                                        hasProperty("data", equalTo("1-data/data.xml")) //
                                ) //
                        ) //
                ) //
        );

        return allOf( //
                hasProperty("formUid", equalTo("2016-03-AAA-AAA-99")), //
                hasProperty("recordUid", isEmptyOrNullString()), //
                hasProperty("title", equalTo("Cerfa 14004*02")), //
                hasProperty("description", equalTo("Déclaration en mairie des meublés de tourisme")), //
                hasProperty("reference", //
                        allOf( //
                                hasProperty("code", equalTo("cerfa14004")), //
                                hasProperty("revision", equalTo(1L)) //
                        ) //
                ), //
                hasProperty("steps", stepsMatcher) //
        );
    }

}
