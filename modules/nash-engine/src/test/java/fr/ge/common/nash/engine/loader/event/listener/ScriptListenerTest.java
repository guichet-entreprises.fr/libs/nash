package fr.ge.common.nash.engine.loader.event.listener;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.bean.EventTypeEnum;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.ScriptEvent;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Script listener tests.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class ScriptListenerTest extends AbstractTest {

    /** The event. */
    @Mock
    private ScriptEvent event;

    /** The listener. */
    @InjectMocks
    private ScriptListener listener;

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnEvent() {
        // mock
        final IProvider provider = new ZipProvider(this.resourceAsBytes("events.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        when(this.event.getType()).thenReturn(EventTypeEnum.BEFORE_UPDATE_STEP_STATUS);
        when(this.event.getLoader()).thenReturn(loader);

        // call
        this.listener.onScriptEvent(this.event);
    }

}
