/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.transition;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/application-context-test.xml", "/spring/test-context.xml" })
public class SendPartnerRecordTest extends AbstractTransitionTest {

    /** The Constant ENDPOINT. */
    protected static final String ENDPOINT = "http://localhost:8888/test";

    /** The server. */
    protected Server server;

    protected INashService service;

    @Autowired
    private Engine engine;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    @Before
    public void setUp() {

        this.service = mock(INashService.class);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.service);
        serverFactory.setProvider(jsonProvider);

        this.server = serverFactory.create();
    }

    @Override
    @After
    public void tearDown() throws Exception {
        this.server.stop();
        this.server.destroy();
    }

    @Test
    // Test is valid for sendPartnerRecord-1.0
    public void testProfiler() throws Exception {

        final Map<String, String> properties = new HashMap<>();
        properties.put("nashBo.baseUrl", ENDPOINT);
        properties.put("tracker.baseUrl", ENDPOINT);
        this.engine.getEngineProperties().asMap().putAll(properties);

        when(this.service.downloadSpecification(any())).thenReturn(Response.ok(this.resourceAsBytes("spec.zip")).build());
        when(this.service.upload(any(), any(), any())).thenReturn(Response.ok("/v1/Record/code/2017-09-REC-AAA-02").build());
        when(this.service.link(any(), any())).thenReturn("test");

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);

        this.execute(loader);
        this.execute(loader);

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.service).upload(captor.capture(), any(), any());

        assertThat(captor.getValue(), notNullValue());

        final SpecificationLoader actual = this.engine.loader(new ZipProvider(captor.getValue()));

        assertThat(actual.description(), hasProperty("reference", hasProperty("code", equalTo("SCN/GP/Dossier Partenaire"))));

        final FormSpecificationDescription description = actual.description();
        assertThat(description.getSteps().getElements(), hasSize(2));
    }

    @Test
    // Test is valid for sendPartnerRecord-1.1
    public void testMetas() throws Exception {

        final Map<String, String> properties = new HashMap<>();
        properties.put("nashBo.baseUrl", ENDPOINT);
        this.engine.getEngineProperties().asMap().putAll(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec2.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);

        this.execute(loader);
        this.execute(loader);

        try (OutputStream out = new FileOutputStream(Paths.get("target/resource.zip").toFile())) {
            IOUtils.write(provider.asBytes(), out);
        }

        assertThat(loader.meta().getMetas(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("denomination")), //
                                hasProperty("name", equalTo("author")), //
                                hasProperty("name", equalTo("title")), //
                                hasProperty("name", equalTo("uid")) //
                        ) //
                ) //
        );

        final ArgumentCaptor<byte[]> resourceAsBytes = ArgumentCaptor.forClass(byte[].class);
        verify(this.service).upload(resourceAsBytes.capture(), any(), any());

        final IProvider partnerProvider = new ZipProvider(resourceAsBytes.getValue());
        final SpecificationLoader partnerLoader = this.engine.loader(partnerProvider);

        assertThat(partnerLoader.meta().getMetas(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("denomination")), //
                                hasProperty("name", equalTo("author")), //
                                hasProperty("name", equalTo("title")), //
                                allOf( //
                                        hasProperty("name", equalTo("document")), //
                                        hasProperty("value", equalTo("1-summary/generated/view.informations.attachment-1-C1000A1001L000302D20190418H145543TPIJTES004PGUEN.pdf")) //
                                ), //
                                allOf( //
                                        hasProperty("name", equalTo("document")), //
                                        hasProperty("value", equalTo("1-summary/generated/view.informations.attachment-2-C1000A1001L000302D20190418H145543TPIJTES003PGUEN.pdf")) //
                                ), //
                                allOf( //
                                        hasProperty("name", equalTo("document")), //
                                        hasProperty("value", equalTo("1-summary/generated/view.informations.attachment-3-C1000A1001L000302D20190418H145545TXMLTCS001PGUEN.xml")) //
                                ) //
                        ) //
                ) //
        );

    }

    public static interface INashService {

        @GET
        @Path("/v1/Specification/code/{code}/file")
        @Produces("application/zip")
        Response downloadSpecification(@PathParam("code") String code);

        @POST
        @Path("/v1/Record")
        @Consumes({ "application/zip", MediaType.APPLICATION_OCTET_STREAM })
        @Produces(MediaType.APPLICATION_JSON)
        Response upload(byte[] resourceAsBytes, @QueryParam("author") @DefaultValue("") String author, @QueryParam("roles") @DefaultValue("*:*") List<SearchQueryFilter> roles);

        @POST
        @Path("/v1/uid/{uid}/ref/{ref}")
        String link(@PathParam("uid") String newReference, @PathParam("ref") String sourceReference);
    }
}
