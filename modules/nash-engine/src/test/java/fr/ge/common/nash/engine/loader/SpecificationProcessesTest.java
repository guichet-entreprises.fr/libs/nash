/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.loader;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.manager.processor.IProcessResult;
import fr.ge.common.nash.engine.manager.processor.result.DataProcessResult;
import fr.ge.common.nash.engine.manager.processor.result.EmptyProcessResult;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class SpecificationProcessesTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class SpecificationProcessesTest extends AbstractTest {

    /** The provider. */
    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.getAbsolutePath(any(String.class))).thenCallRealMethod();
        when(this.provider.asBean(any(), any())).thenCallRealMethod();
        when(this.provider.asBytes(eq("description.xml"))).thenReturn(this.resourceAsBytes("spec/description.xml"));
        when(this.provider.asBytes(eq("step01/data.xml"))).thenReturn(this.resourceAsBytes("spec/step01/data.xml"));
        when(this.provider.asBytes(eq("step02/preprocess.xml"))).thenReturn(this.resourceAsBytes("spec/step02/preprocess.xml"));
        when(this.provider.asBytes(eq("step02/postprocess.xml"))).thenReturn(this.resourceAsBytes("spec/step02/postprocess.xml"));
    }

    /**
     * Test find preprocess.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFindPreprocess() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");
        stepElement.setPostprocess("step02/postprocess.xml");

        final FormSpecificationProcess actual = loader.processes().findPreprocesses(stepElement);
        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("step02pre")), //
                        hasProperty("type", equalTo("pre")), //
                        hasProperty("resourceName", equalTo("step02/preprocess.xml")) //
                ) //
        );
    }

    /**
     * Test find postprocess.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFindPostprocess() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");
        stepElement.setPostprocess("step02/postprocess.xml");

        final FormSpecificationProcess actual = loader.processes().findPostprocesses(stepElement);
        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("step02post")), //
                        hasProperty("type", equalTo("post")), //
                        hasProperty("resourceName", equalTo("step02/postprocess.xml")) //
                ) //
        );
    }

    /**
     * Test find process.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFindProcess() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");
        stepElement.setPostprocess("step02/postprocess.xml");

        final FormSpecificationProcess actual = loader.processes().findProcesses(stepElement, StepElement::getPreprocess, "pre");
        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("step02pre")), //
                        hasProperty("type", equalTo("pre")), //
                        hasProperty("resourceName", equalTo("step02/preprocess.xml")) //
                ) //
        );
    }

    /**
     * Test find process no step.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFindProcessNoStep() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final FormSpecificationProcess actual = loader.processes().findProcesses(null, StepElement::getPreprocess, "pre");
        assertThat(actual, nullValue());
    }

    /**
     * Test find process no process resource name.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFindProcessNoProcessResourceName() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");

        final FormSpecificationProcess actual = loader.processes().findProcesses(stepElement, StepElement::getPreprocess, "pre");
        assertThat(actual, nullValue());
    }

    /**
     * Test find process empty process resource name.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFindProcessEmptyProcessResourceName() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("");

        final FormSpecificationProcess actual = loader.processes().findProcesses(stepElement, StepElement::getPreprocess, "pre");
        assertThat(actual, nullValue());
    }

    /**
     * Test find process not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFindProcessNotFound() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess-notfound.xml");

        try {
            loader.processes().findProcesses(stepElement, StepElement::getPreprocess, "pre");
            fail("Technical exception expected");
        } catch (final TechnicalException ex) {
            assertThat(ex, hasProperty("message", equalTo("Process file \"step02/preprocess-notfound.xml\" not found")));
        }
    }

    /**
     * Test pre execute.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPreExecute() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");

        final IProcessResult<?> actual = loader.processes().preExecute(stepElement);

        assertThat(actual, instanceOf(DataProcessResult.class));
        assertThat(actual.getContent(), //
                allOf( //
                        hasProperty("id", equalTo("step02x03")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        verify(this.provider).save(eq("step02/data-pre01.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-pre02.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-pre03.xml"), eq("text/xml"), any());
    }

    /**
     * Test pre execute no step.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPreExecuteNoStep() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final IProcessResult<?> actual = loader.processes().preExecute(null);

        assertThat(actual, nullValue());
    }

    /**
     * Test pre execute process error.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPreExecuteProcessError() throws Exception {
        when(this.provider.asBytes(eq("step02/preprocess.xml"))).thenReturn(this.resourceAsBytes("spec/step02/preprocess-error.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");

        try {
            loader.processes().preExecute(stepElement);
            fail("Expression exception expected");
        } catch (final ExpressionException ex) {
            assertEquals("<eval>:3:21 Expected ; but found simple\n return a simple syntax error !!!\n ^ in <eval> at line number 3 at column number 21",
                    ex.getMessage().replace("\r", "").replaceAll("[ ]{2,}", " "));
        }
    }

    /**
     * Test pre execute no processes.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPreExecuteNoProcesses() throws Exception {
        when(this.provider.asBytes(eq("step02/preprocess.xml"))).thenReturn(this.resourceAsBytes("spec/step02/preprocess-empty.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");

        final IProcessResult<?> actual = loader.processes().preExecute(stepElement);

        assertThat(actual, instanceOf(EmptyProcessResult.class));
    }

    /**
     * Test pre execute from specific process id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPreExecuteFromSpecificProcessId() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");

        final IProcessResult<?> actual = loader.processes().preExecuteFrom(stepElement, "prc02");

        assertThat(actual, instanceOf(DataProcessResult.class));
        assertThat(actual.getContent(), //
                allOf( //
                        hasProperty("id", equalTo("step02x03")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        verify(this.provider, times(0)).save(eq("step02/data-pre01.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-pre02.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-pre03.xml"), eq("text/xml"), any());
    }

    /**
     * Test pre execute from specific process id not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPreExecuteFromSpecificProcessIdNotFound() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");

        final IProcessResult<?> actual = loader.processes().preExecuteFrom(stepElement, "prc42NotFound");

        assertThat(actual, instanceOf(EmptyProcessResult.class));

        verify(this.provider, times(0)).save(eq("step02/data-pre01.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-pre02.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-pre03.xml"), eq("text/xml"), any());
    }

    /**
     * Test pre execute after specific process id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPreExecuteAfterSpecificProcessId() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");

        final IProcessResult<?> actual = loader.processes().preExecuteAfter(stepElement, "prc02");

        assertThat(actual, instanceOf(DataProcessResult.class));
        assertThat(actual.getContent(), //
                allOf( //
                        hasProperty("id", equalTo("step02x03")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        verify(this.provider, times(0)).save(eq("step02/data-pre01.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-pre02.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-pre03.xml"), eq("text/xml"), any());
    }

    /**
     * Test pre execute after specific process id not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPreExecuteAfterSpecificProcessIdNotFound() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPreprocess("step02/preprocess.xml");

        final IProcessResult<?> actual = loader.processes().preExecuteAfter(stepElement, "prc42NotFound");

        assertThat(actual, instanceOf(EmptyProcessResult.class));

        verify(this.provider, times(0)).save(eq("step02/data-pre01.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-pre02.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-pre03.xml"), eq("text/xml"), any());
    }

    /**
     * Test post execute.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostExecute() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPostprocess("step02/postprocess.xml");

        final IProcessResult<?> actual = loader.processes().postExecute(stepElement);

        assertThat(actual, instanceOf(DataProcessResult.class));
        assertThat(actual.getContent(), //
                allOf( //
                        hasProperty("id", equalTo("step02x03")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        verify(this.provider).save(eq("step02/data-post01.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-post02.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-post03.xml"), eq("text/xml"), any());
    }

    /**
     * Test post execute no step.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostExecuteNoStep() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final IProcessResult<?> actual = loader.processes().postExecute(null);

        assertThat(actual, nullValue());
    }

    /**
     * Test post execute process error.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostExecuteProcessError() throws Exception {
        when(this.provider.asBytes(eq("step02/postprocess.xml"))).thenReturn(this.resourceAsBytes("spec/step02/preprocess-error.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPostprocess("step02/postprocess.xml");

        try {
            loader.processes().postExecute(stepElement);
            fail("Expression exception expected");
        } catch (final ExpressionException ex) {
            assertEquals("<eval>:3:21 Expected ; but found simple\n return a simple syntax error !!!\n ^ in <eval> at line number 3 at column number 21",
                    ex.getMessage().replace("\r", "").replaceAll("[ ]{2,}", " "));
        }
    }

    /**
     * Test post execute no processes.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostExecuteNoProcesses() throws Exception {
        when(this.provider.asBytes(eq("step02/postprocess.xml"))).thenReturn(this.resourceAsBytes("spec/step02/preprocess-empty.xml"));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPostprocess("step02/postprocess.xml");

        final IProcessResult<?> actual = loader.processes().postExecute(stepElement);

        assertThat(actual, instanceOf(EmptyProcessResult.class));
    }

    /**
     * Test post execute from specific process id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostExecuteFromSpecificProcessId() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPostprocess("step02/postprocess.xml");

        final IProcessResult<?> actual = loader.processes().postExecuteFrom(stepElement, "prc02");

        assertThat(actual, instanceOf(DataProcessResult.class));
        assertThat(actual.getContent(), //
                allOf( //
                        hasProperty("id", equalTo("step02x03")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        verify(this.provider, times(0)).save(eq("step02/data-post01.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-post02.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-post03.xml"), eq("text/xml"), any());
    }

    /**
     * Test post execute from specific process id not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostExecuteFromSpecificProcessIdNotFound() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPostprocess("step02/postprocess.xml");

        final IProcessResult<?> actual = loader.processes().postExecuteFrom(stepElement, "prc42NotFound");

        assertThat(actual, instanceOf(EmptyProcessResult.class));

        verify(this.provider, times(0)).save(eq("step02/data-post01.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-post02.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-post03.xml"), eq("text/xml"), any());
    }

    /**
     * Test post execute after specific process id.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostExecuteAfterSpecificProcessId() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPostprocess("step02/postprocess.xml");

        final IProcessResult<?> actual = loader.processes().postExecuteAfter(stepElement, "prc02");

        assertThat(actual, instanceOf(DataProcessResult.class));
        assertThat(actual.getContent(), //
                allOf( //
                        hasProperty("id", equalTo("step02x03")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        verify(this.provider, times(0)).save(eq("step02/data-post01.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-post02.xml"), eq("text/xml"), any());
        verify(this.provider).save(eq("step02/data-post03.xml"), eq("text/xml"), any());
    }

    /**
     * Test post execute after specific process id not found.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPostExecuteAfterSpecificProcessIdNotFound() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);

        final StepElement stepElement = new StepElement();
        stepElement.setId("step02");
        stepElement.setPostprocess("step02/postprocess.xml");

        final IProcessResult<?> actual = loader.processes().postExecuteAfter(stepElement, "prc42NotFound");

        assertThat(actual, instanceOf(EmptyProcessResult.class));

        verify(this.provider, times(0)).save(eq("step02/data-post01.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-post02.xml"), eq("text/xml"), any());
        verify(this.provider, times(0)).save(eq("step02/data-post03.xml"), eq("text/xml"), any());
    }

    /**
     * Test resolve resource name.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResolveResourceName() throws Exception {
        final String resourceName = "step02/data.xml";

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader);

        final String actual = SpecificationProcesses.resolveResourceName(context, resourceName);
        assertThat(actual, equalTo(resourceName));
    }

    /**
     * Test resolve resource name null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResolveResourceNameNull() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader);

        final String actual = SpecificationProcesses.resolveResourceName(context, null);
        assertThat(actual, nullValue());
    }

    /**
     * Test resolve resource name empty.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResolveResourceNameEmpty() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader);

        final String actual = SpecificationProcesses.resolveResourceName(context, "");
        assertThat(actual, nullValue());
    }

    /**
     * Test resolve resource name by known ID.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResolveResourceNameByKnownID() throws Exception {
        final String resourceName = "#prc01";
        final FormSpecificationProcess processes = JaxbFactoryImpl.instance().unmarshal(this.resourceAsBytes("spec/step02/preprocess-resolve.xml"), FormSpecificationProcess.class);

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader) //
                .target(processes).withProvider(loader.provider("step02/preprocess-resolve.xml")) //
                .target(processes.getProcesses().get(1));

        final String actual = SpecificationProcesses.resolveResourceName(context, resourceName);
        assertThat(actual, equalTo("data-pre01.xml"));
    }

    /**
     * Test resolve resource name by unknown ID.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResolveResourceNameByUnknownID() throws Exception {
        final String resourceName = "#prc12";
        final FormSpecificationProcess processes = JaxbFactoryImpl.instance().unmarshal(this.resourceAsBytes("spec/step02/preprocess-resolve.xml"), FormSpecificationProcess.class);

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader) //
                .target(processes).withProvider(loader.provider("step02/preprocess-resolve.xml")) //
                .target(processes.getProcesses().get(1));

        final String actual = SpecificationProcesses.resolveResourceName(context, resourceName);
        assertThat(actual, nullValue());
    }

    @Test
    public void testResolveMultipleResourceName() throws Exception {
        final String resourceName = "#prc01, input.xml, other.xml";
        final FormSpecificationProcess processes = JaxbFactoryImpl.instance().unmarshal(this.resourceAsBytes("spec/step02/preprocess-resolve.xml"), FormSpecificationProcess.class);

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader) //
                .target(processes).withProvider(loader.provider("step02/preprocess-resolve.xml")) //
                .target(processes.getProcesses().get(1));

        final String actual = SpecificationProcesses.resolveResourceName(context, resourceName);
        assertThat(actual, equalTo("data-pre01.xml, input.xml, other.xml"));
    }

    /**
     * Test load.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testLoad() throws Exception {
        final String resourceName = "step02/data-pre01.xml";

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader);

        SpecificationProcesses.load(context, resourceName, FormSpecificationData.class);
        verify(this.provider).asBytes(resourceName);
    }

    /**
     * Test load null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testLoadNull() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader);

        SpecificationProcesses.load(context, null, FormSpecificationData.class);
        verify(this.provider, times(0)).asBytes(any());
    }

    /**
     * Test load empty.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testLoadEmpty() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader);

        SpecificationProcesses.load(context, null, FormSpecificationData.class);
        verify(this.provider, times(0)).asBytes(any());
    }

    /**
     * Test load known reference.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testLoadKnownReference() throws Exception {
        final String resourceName = "#prc01";
        final FormSpecificationProcess processes = JaxbFactoryImpl.instance().unmarshal(this.resourceAsBytes("spec/step02/preprocess-resolve.xml"), FormSpecificationProcess.class);

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader) //
                .target(processes).withProvider(loader.provider("step02/preprocess-resolve.xml")) //
                .target(processes.getProcesses().get(1));

        SpecificationProcesses.load(context, resourceName, FormSpecificationData.class);
        verify(this.provider).asBytes("step02/data-pre01.xml");
    }

    /**
     * Test load unknown reference.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testLoadUnknownReference() throws Exception {
        final String resourceName = "#prc12";
        final FormSpecificationProcess processes = JaxbFactoryImpl.instance().unmarshal(this.resourceAsBytes("spec/step02/preprocess-resolve.xml"), FormSpecificationProcess.class);

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final EngineContext<?> context = EngineContext.build(loader) //
                .target(processes).withProvider(loader.provider("step02/preprocess-resolve.xml")) //
                .target(processes.getProcesses().get(1));

        SpecificationProcesses.load(context, resourceName, FormSpecificationData.class);
        verify(this.provider, times(0)).asBytes(any());
    }

}
