/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.ProcessModelManager;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.test.AbstractBeanResourceTest;

/**
 * The Class ProxyInProcessorTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class ProxyInProcessorTest extends AbstractBeanResourceTest {

    /** The provider. */
    @Mock
    private IProvider provider;

    /** The processor. */
    private ProxyInProcessor processor;

    /** The context. */
    protected EngineContext<?> context;

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.provider = mock(IProvider.class);

        when(this.provider.getAbsolutePath(any(String.class))).thenCallRealMethod();
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
        when(this.provider.asBytes(eq("description.xml"))).thenReturn(this.resourceAsBytes("data/description.xml"));

        final Properties properties = new Properties();
        this.context = this.applicationContext.getBean(SpecificationLoader.class, new Configuration(properties), this.provider).buildEngineContext();

        this.processor = new ProxyInProcessor().setContext(this.context);
    }

    /**
     * Inits the.
     *
     * @throws Exception
     *             the exception
     */
    private void init() throws Exception {
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final FormSpecificationProcess processes = this.resourceAsBean("data/step02/preprocess.xml", FormSpecificationProcess.class);
        final ProcessElement process = (ProcessElement) processes.getProcesses().get(1);
        process.setValue(this.resourceAsString("data/step02/hangin.js"));

        this.processor.setContext(this.context.target(processes).withProvider(loader.provider("step02/preprocess.xml")).target(process));
        this.processor.init();
    }

    /**
     * Call.
     *
     * @return true, if successful
     * @throws Exception
     *             the exception
     */
    private boolean call() throws Exception {
        this.init();
        return this.processor.validateConditions();
    }

    /**
     * Test no parameters file.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNoParametersFile() throws Exception {
        final boolean actual = this.call();

        assertThat(actual, equalTo(false));

        verify(this.provider).asBytes(eq("step02/proxy-calling.xml"));
        verify(this.provider, times(0)).asBytes(eq("step02/proxy-result.xml"));
    }

    /**
     * Test no result file.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNoResultFile() throws Exception {
        when(this.provider.asBytes(eq("step02/proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling.xml"));

        final boolean actual = this.call();

        assertThat(actual, equalTo(false));

        verify(this.provider).asBytes(eq("step02/proxy-calling.xml"));
        verify(this.provider).asBytes(eq("step02/proxy-result.xml"));
    }

    /**
     * Test nominal.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNominal() throws Exception {
        when(this.provider.asBytes(eq("step02/proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling.xml"));
        when(this.provider.asBytes(eq("step02/proxy-result.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-result.xml"));

        final boolean actual = this.call();

        assertThat(actual, equalTo(true));

        verify(this.provider).asBytes(eq("step02/proxy-calling.xml"));
        verify(this.provider).asBytes(eq("step02/proxy-result.xml"));
    }

    /**
     * Test no result filename property.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNoResultFilenameProperty() throws Exception {
        when(this.provider.asBytes(eq("step02/proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling-property-not-present.xml"));

        final boolean actual = this.call();

        assertThat(actual, equalTo(false));

        verify(this.provider).asBytes(eq("step02/proxy-calling.xml"));
        verify(this.provider, times(0)).asBytes(eq("step02/proxy-result.xml"));
    }

    /**
     * Test result filename property as group.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResultFilenamePropertyAsGroup() throws Exception {
        when(this.provider.asBytes(eq("step02/proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling-property-as-group.xml"));

        final boolean actual = this.call();

        assertThat(actual, equalTo(false));

        verify(this.provider).asBytes(eq("step02/proxy-calling.xml"));
        verify(this.provider, times(0)).asBytes(eq("step02/proxy-result.xml"));
    }

    /**
     * Test no result filename property type.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNoResultFilenamePropertyType() throws Exception {
        when(this.provider.asBytes(eq("step02/proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling-property-no-type.xml"));

        final boolean actual = this.call();

        assertThat(actual, equalTo(false));

        verify(this.provider).asBytes(eq("step02/proxy-calling.xml"));
        verify(this.provider, times(0)).asBytes(eq("step02/proxy-result.xml"));
    }

    /**
     * Test bad result filename property type.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testBadResultFilenamePropertyType() throws Exception {
        when(this.provider.asBytes(eq("step02/proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling-property-bad-type.xml"));

        final boolean actual = this.call();

        assertThat(actual, equalTo(false));

        verify(this.provider).asBytes(eq("step02/proxy-calling.xml"));
        verify(this.provider, times(0)).asBytes(eq("step02/proxy-result.xml"));
    }

    /**
     * Test no result filename property value.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testNoResultFilenamePropertyValue() throws Exception {
        when(this.provider.asBytes(eq("step02/proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling-property-no-value.xml"));

        final boolean actual = this.call();

        assertThat(actual, equalTo(false));

        verify(this.provider).asBytes(eq("step02/proxy-calling.xml"));
        verify(this.provider, times(0)).asBytes(eq("step02/proxy-result.xml"));
    }

    /**
     * Test execute.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testExecute() throws Exception {
        Arrays.asList("hangout.js", "hangin.js", "proxy-calling.xml", "proxy-result.xml") //
                .forEach(name -> when(this.provider.asBytes(eq("step02/" + name))).thenReturn(this.resourceAsBytes("data/step02/" + name)));

        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, this.provider);
        final FormSpecificationProcess processes = this.resourceAsBean("data/step02/pre-hangin.xml", FormSpecificationProcess.class);

        final ProcessModelManager mgr = new ProcessModelManager(EngineContext.build(loader).target(processes).withProvider(loader.provider("step02/pre-hangin.xml")));

        final IProcessResult<?> actual = mgr.execute(null);

        assertThat(actual, notNullValue());
    }

    // private FormSpecificationData retrieveSavedFile(final String
    // resourceName) {
    // final ArgumentCaptor<byte[]> captor =
    // ArgumentCaptor.forClass(byte[].class);
    // verify(this.provider).save(eq(resourceName), any(), captor.capture());
    // return JaxbFactoryImpl.instance().unmarshal(captor.getValue(),
    // FormSpecificationData.class);
    // }

}
