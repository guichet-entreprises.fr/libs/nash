/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class FormSpecificationValidatorTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/application-context-test.xml", "/spring/validator-context-test.xml" })
public class FormSpecificationValidatorTest extends AbstractTest {

    /**
     * Test nominal.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testNominal() throws Exception {
        final Errors actual = this.validate("spec.zip");

        assertThat(actual.hasErrors(), equalTo(false));
    }

    /**
     * Test id with square brackets.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testIdSquareBrackets() throws Exception {
        final Errors actual = this.validate("id-square-brackets.zip");

        assertThat(actual.hasErrors(), equalTo(false));
    }

    /**
     * Test with a wrong id.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testIdWrong() throws Exception {
        final Errors actual = this.validate("id-wrong.zip");

        assertThat(actual.getObjectErrors("1-data/data.xml"), //
                contains( //
                        Arrays.asList( //
                                hasProperty("message", equalTo("[52,35] La valeur de l'attribut \"id\" associé à un type d'élément \"group\" ne doit pas contenir le caractère '<'.")) //
                        ) //
                ) //
        );
    }

    /**
     * Test missing id and mandatory.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testMissingIdAndMandatory() throws Exception {
        final Errors actual = this.validate("missingIdAndMandatory.zip");

        assertThat(actual.getGlobalErrors(), //
                contains( //
                        hasProperty("message", equalTo("Le fichier '0-context/context.xml' est manquant")) //
                ) //
        );

        assertThat(actual.getObjectErrors("1-data/data.xml"), //
                contains( //
                        Arrays.asList( //
                                hasProperty("message", equalTo("[37,170] cvc-complex-type.4 : L'attribut 'id' doit figurer dans l'élément 'form'.")), //
                                hasProperty("message", equalTo("[66,33] cvc-complex-type.4 : L'attribut 'id' doit figurer dans l'élément 'data'.")) //
                        ) //
                ) //
        );
    }

    /**
     * Test step file not specified.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testStepFileNotSpecified() throws Exception {
        final Errors actual = this.validate("stepFileNotSpecified.zip");

        assertThat(actual.getGlobalErrors(), //
                contains( //
                        hasProperty("message", equalTo("Aucun fichier de pré-traitement ou de données spécifié pour l'étape 'context'")) //
                ) //
        );

        assertThat(actual.getObjectErrors().isEmpty(), equalTo(true));
    }

    @Test
    public void testYield() throws Exception {
        final Errors actual = this.validate("yield.zip");
        assertThat(actual.getGlobalErrors(), hasSize(0));
        assertThat(actual.getObjectErrors("description.xml"), nullValue());
    }

    /**
     * Test bad zip format.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testBadZipFormat() throws Exception {
        final Errors actual = this.validate("xxx".getBytes(StandardCharsets.UTF_8));

        assertThat(actual.getGlobalErrors(), //
                contains( //
                        hasProperty("message", equalTo("Le fichier 'description.xml' est manquant")) //
                ) //
        );
    }

    /**
     * Test double if.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDoubleIf() throws Exception {
        final Errors actual = this.validate("specDoubleIf.zip");

        assertThat(actual.getGlobalErrors().isEmpty(), equalTo(true));

        assertThat(actual.getObjectErrors("1-data/data.xml"), //
                contains( //
                        Arrays.asList( //
                                hasProperty("message", equalTo("L'attribut 'if' du champ 'cerfa14004.classification.classificationLevelTer' est renseigné de 2 manières différentes")) //
                        ) //
                ) //
        );
    }

    /**
     * Test id duplicates.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testIdDuplicates() throws Exception {
        final Errors actual = this.validate("specIdDuplicates.zip");

        assertThat(actual.getGlobalErrors().isEmpty(), equalTo(true));

        assertThat(actual.getObjectErrors("1-data/data.xml"), //
                contains( //
                        Arrays.asList( //
                                hasProperty("message", equalTo("L'id 'cerfa14004.classification.classificationLevelBis' n'est pas unique")) //
                        ) //
                ) //
        );
    }

    /**
     * Test null type.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testNullType() throws Exception {
        final Errors actual = this.validate("specNullType.zip");

        assertThat(actual.getGlobalErrors().isEmpty(), equalTo(true));

        assertThat(actual.getObjectErrors("1-data/data.xml"), //
                contains( //
                        Arrays.asList( //
                                hasProperty("message", equalTo("Le champ 'cerfa14004.classification.classificationLevelBis' n'a pas de type")) //
                        ) //
                ) //
        );
    }

    /**
     * Test missing type.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testMissingType() throws Exception {
        final Errors actual = this.validate("specMissingType.zip");

        assertThat(actual.getGlobalErrors().isEmpty(), equalTo(true));

        assertThat(actual.getObjectErrors("1-data/data.xml"), //
                contains( //
                        Arrays.asList( //
                                hasProperty("message", equalTo("Le type 'ThisTypeDoesNotExist' du champ 'cerfa14004.classification.classificationLevelBis' n'existe pas en version '1.2'")) //
                        ) //
                ) //
        );
    }

    /**
     * Test invalid default value.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testInvalidDefaultValue() throws Exception {
        final Errors actual = this.validate("specInvalidDefaultValue.zip");

        assertThat(actual.getGlobalErrors().isEmpty(), equalTo(true));

        assertThat(actual.getObjectErrors("1-data/data.xml"), //
                contains( //
                        Arrays.asList( //
                                hasProperty("message", equalTo("La valeur par défaut du champ 'cerfa14004.classification.classificationLevelBis' de type 'Integer' n'est pas valide en version '1.2'")) //
                        ) //
                ) //
        );
    }

    /**
     * Test phone value type.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPhoneValueType() throws Exception {
        final Errors actual = this.validate("phoneValueType.zip");

        assertThat(actual.hasErrors(), equalTo(false));
    }

    /**
     * Test empty value.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEmptyValue() throws Exception {
        final Errors actual = this.validate("emptyValue.zip");

        assertThat(actual.hasErrors(), equalTo(false));
    }

    /**
     * Test entity.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testEntity() throws Exception {
        final Errors actual = this.validate("entity.zip");
        assertThat(actual.getObjectErrors("description.xml"), //
                contains( //
                        allOf( //
                                hasProperty("level", equalTo(Error.Level.FATAL)), //
                                hasProperty("message", equalTo("[38,33] La référence à l'entité \"token\" doit se terminer par le délimiteur ';'.")) //
                        ) //
                ) //
        );
    }

    /**
     * Test group conditional display.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGroupConditionalDisplay() throws Exception {
        final Errors actual = this.validate("group-display.zip");

        final Error[] errors = actual.getObjectErrors("1-data/data.xml").toArray(new Error[] {});
        assertThat(errors.length, equalTo(2));
        assertThat(errors[0], //
                allOf( //
                        hasProperty("level", equalTo(Error.Level.ERROR)), //
                        hasProperty("message", equalTo("Le premier groupe de premier niveau ne doit pas avoir de condition d'affichage")) //
                ) //
        );
        assertThat(errors[1], //
                allOf( //
                        hasProperty("level", equalTo(Error.Level.ERROR)), //
                        hasProperty("message", equalTo("Le dernier groupe de premier niveau ne doit pas avoir de condition d'affichage")) //
                ) //
        );
    }

    /**
     * Test one group conditional display.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testOneGroupConditionalDisplay() throws Exception {
        final Errors actual = this.validate("one-group-display.zip");

        assertThat(actual.getObjectErrors("1-data/data.xml"), //
                contains( //
                        allOf( //
                                hasProperty("level", equalTo(Error.Level.ERROR)), //
                                hasProperty("message", equalTo("Le premier groupe de premier niveau ne doit pas avoir de condition d'affichage")) //
                        ) //
                ) //
        );
    }

    /**
     * Validate.
     *
     * @param resourceName
     *            resource name
     * @return errors
     */
    private Errors validate(final String resourceName) {
        return this.validate(this.resourceAsBytes(resourceName));
    }

    /**
     * Validate.
     *
     * @param resourceAsBytes
     *            resource as bytes
     * @return errors
     */
    private Errors validate(final byte[] resourceAsBytes) {
        final IProvider provider = new ZipProvider(resourceAsBytes);
        final FormSpecificationValidator validator = new FormSpecificationValidator(Locale.FRANCE, provider);

        return validator.validate();
    }

}
