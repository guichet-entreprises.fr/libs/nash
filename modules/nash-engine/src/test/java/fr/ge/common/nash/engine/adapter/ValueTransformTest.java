package fr.ge.common.nash.engine.adapter;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import fr.ge.common.nash.engine.adapter.v1_2.value.ButtonValue;
import fr.ge.common.nash.engine.adapter.v1_2.value.Phone;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.TextValueElement;

public class ValueTransformTest {

    @Test
    public void testFromObject() throws Exception {
        final ValueElement actual = ValueTransform.fromObject(new ButtonValue().setLabel("Test Label").setIcon("cogs").setSize("xl"));

        assertThat(actual.getContent(), instanceOf(ListValueElement.class));

        assertThat(((ListValueElement) actual.getContent()).getElements(), //
                containsInAnyOrder( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("label")), //
                                        hasProperty("value", equalTo("Test Label")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("icon")), //
                                        hasProperty("value", equalTo("cogs")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("size")), //
                                        hasProperty("value", equalTo("xl")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("value")), //
                                        hasProperty("value", equalTo("no")) //
                                ) //
                        ) //
                ) //
        );
    }

    @Test
    public void testFromMap() throws Exception {
        final Map<String, Object> model = new HashMap<>();
        model.put("label", "Test Label");
        model.put("color", "#abcdef");
        model.put("icon", "cogs");
        model.put("size", null);
        model.put("nothing", "Unknown property");
        model.put("country", "fr");
        model.put("e164", "+33 6 12 34 56 78");
        model.put("international", "+33612345678");

        this.testFrom(model);
    }

    @Test
    public void testFromContentData() throws Exception {
        final Map<String, String[]> model = new HashMap<>();
        model.put("label", new String[] { "Test Label" });
        model.put("color", new String[] { "#abcdef" });
        model.put("icon", new String[] { "cogs" });
        model.put("nothing", new String[] { "Unknown property" });
        model.put("country", new String[] { "fr" });
        model.put("e164", new String[] { "+33 6 12 34 56 78" });
        model.put("international", new String[] { "+33612345678" });

        this.testFrom(new FormContentData(model));
    }

    private void testFrom(final Object model) throws Exception {
        final ValueElement btn = ValueTransform.from(ButtonValue.class, model);

        assertThat(btn.getContent(), instanceOf(ListValueElement.class));

        assertThat(((ListValueElement) btn.getContent()).getElements(), //
                containsInAnyOrder( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("label")), //
                                        hasProperty("value", equalTo("Test Label")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("color")), //
                                        hasProperty("value", equalTo("#abcdef")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("icon")), //
                                        hasProperty("value", equalTo("cogs")) //
                                ) //
                        ) //
                ) //
        );

        final ValueElement phone = ValueTransform.from(Phone.class, model);

        assertThat(phone.getContent(), instanceOf(ListValueElement.class));

        assertThat(((ListValueElement) phone.getContent()).getElements(), //
                containsInAnyOrder( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("country")), //
                                        hasProperty("value", equalTo("fr")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("e164")), //
                                        hasProperty("value", equalTo("+33 6 12 34 56 78")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("international")), //
                                        hasProperty("value", equalTo("+33612345678")) //
                                ) //
                        ) //
                ) //
        );
    }

    @Test
    public void testTo() throws Exception {
        final ValueElement elm = new ValueElement();
        elm.setContent( //
                new ListValueElement( //
                        new TextValueElement("label", "Test Label"), //
                        new TextValueElement("color", "#abcdef"), //
                        new TextValueElement("size", "xl") //
                ) //
        );

        final ButtonValue actual = ValueTransform.to(ButtonValue.class, elm);

        assertThat(actual, //
                allOf( //
                        hasProperty("label", equalTo("Test Label")), //
                        hasProperty("color", equalTo("#abcdef")), //
                        hasProperty("icon", nullValue()), //
                        hasProperty("size", equalTo("xl")) //
                ) //
        );
    }
}
