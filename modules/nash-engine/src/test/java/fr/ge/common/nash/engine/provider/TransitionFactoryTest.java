/**
 *
 */
package fr.ge.common.nash.engine.provider;

import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;

/**
 * @author bsadil
 *
 */
public class TransitionFactoryTest {

    /**
     * Test load all transition.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLoadAllTransitions() throws Exception {

        final Map<String, TransitionProvider> transitions = TransitionFactory.loadAllTransitions();
        assertNotNull(transitions);
        assertNotNull(transitions.keySet());

    }

}
