/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter.v1_0;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;

/**
 * Class IntegerValueAdapterTest.
 *
 * @author Christian Cougourdan
 */
public class IntegerValueAdapterTest extends AbstractValueAdapterTest<Integer> {

    private static final Integer DATA = 42;

    private final String dataAsString = String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), DATA);

    private final String dataEmptyAsString = String.format(DATA_ELEMENT_FILLED_AS_XML, this.type().name(), "");

    /**
     * {@inheritDoc}
     */
    @Override
    protected IValueAdapter<Integer> type() {
        return new IntegerValueAdapter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModel() {
        return Collections.singletonMap("", new String[] { DATA.toString() });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequest(final Integer actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, String[]> buildHttpRequestModelEmpty() {
        return Collections.singletonMap("", new String[] {});
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromHttpRequestEmpty(final Integer actual, final DataElement dataElement) {
        this.assertFromObjectEmpty(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer buildFromObject() {
        return DATA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObject(final Integer actual, final DataElement dataElement) {
        assertEquals(DATA, actual);
        assertEquals(this.dataAsString, JaxbFactoryImpl.instance().asString(dataElement).trim());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer buildFromObjectEmpty() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromObjectEmpty(final Integer actual, final DataElement dataElement) {
        this.assertFromObjectNone(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXml() {
        return this.dataAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXml(final Integer actual, final DataElement dataElement) {
        this.assertFromObject(actual, dataElement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected byte[] buildFromXmlEmpty() {
        return this.dataEmptyAsString.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlEmpty(final Integer actual, final DataElement dataElement) {
        assertNull(actual);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void assertFromXmlNone(final Integer actual, final DataElement dataElement) {
        this.assertFromObjectNone(actual, dataElement);
    }

}
