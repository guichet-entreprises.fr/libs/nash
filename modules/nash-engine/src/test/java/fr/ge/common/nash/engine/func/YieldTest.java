package fr.ge.common.nash.engine.func;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.listener.YieldListener;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.store.IStore;
import fr.ge.common.utils.test.AbstractTest;

/**
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class YieldTest extends AbstractTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(YieldTest.class);

    @Autowired
    private Engine engine;

    private Map<String, IProvider> stored;

    @Before
    public void setUp() throws Exception {
        final YieldListener listener = new YieldListener();

        final Map<String, IStore> stores = new HashMap<>();
        for (final String name : new String[] { "fs1", "fs2", "fs3" }) {
            stores.put(name, this.buildStore(name));
        }
        listener.setStores(stores);

        Engine.setEventListeners(Arrays.asList(listener));
        this.stored = new HashMap<>();
    }

    private IStore buildStore(final String name) {
        return (provider, roles) -> {
            LOGGER.debug("[{}] store record", name);
            this.stored.put(name, provider);
            return null;
        };
    }

    @Test
    public void testPreserveYieldElement() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);
        final int nb = loader.description().getFlow().size();

        final boolean actual = loader.executeActiveStep("user");

        assertThat(actual, equalTo(true));
        assertThat(loader.description().getFlow().size(), equalTo(nb + 1));

        assertThat(this.stored.keySet(), hasSize(0));
    }

    
    public void testCopyStep() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);
        final int nb = loader.description().getFlow().size();

        loader.stepsMgr().find(0).setStatus(StepStatusEnum.DONE.getStatus());

        final boolean actual = loader.executeActiveStep("user");

        assertThat(actual, equalTo(true));

        final IProvider storedProvider = this.stored.get("fs1");
        assertThat(storedProvider, notNullValue());

        final FormSpecificationDescription desc = storedProvider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        assertThat(desc, notNullValue());
        assertThat(desc.getFlow(), contains(hasProperty("id", equalTo("step01"))));
        assertThat(storedProvider.resources("**/*.*"), containsInAnyOrder( //
                new String[] { //
                        "description.xml", //
                        "step01/data.xml", //
                        "step01/post.xml", //
                        "step02/data.xml", //
                        "step04/data.xml" //
                } //
        ));
    }

    
    public void testDoubleExtract() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);
        final int nb = loader.description().getFlow().size();

        for (int idx = 0; idx <= 2; idx++) {
            loader.executeActiveStep("user");
        }

        assertThat(loader.description().getFlow().size(), equalTo(2));

        /*
         * Check fs1 store
         */
        IProvider storedProvider = this.stored.get("fs1");
        assertThat(storedProvider, notNullValue());

        FormSpecificationDescription desc = storedProvider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        assertThat(desc, notNullValue());
        assertThat(desc.getFlow(), contains(hasProperty("id", equalTo("step01"))));
        assertThat(storedProvider.resources("**/*.*"), containsInAnyOrder( //
                new String[] { //
                        "description.xml", //
                        "step01/data.xml", //
                        "step01/post.xml", //
                        "step02/data.xml", //
                        "step04/data.xml" //
                } //
        ));

        /*
         * Check fs2 store
         */
        storedProvider = this.stored.get("fs2");
        assertThat(storedProvider, notNullValue());

        desc = storedProvider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        assertThat(desc, notNullValue());
        assertThat(desc.getFlow(), contains(hasProperty("id", equalTo("step01"))));
        assertThat(storedProvider.resources("**/*.*"), containsInAnyOrder( //
                new String[] { //
                        "description.xml", //
                        "step01/data.xml", //
                        "step01/post.xml" //
                } //
        ));

        /*
         * Check fs3 store
         */
        storedProvider = this.stored.get("fs3");
        assertThat(storedProvider, notNullValue());

        desc = storedProvider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        assertThat(desc, notNullValue());
        assertThat(desc.getFlow(), contains( //
                Arrays.asList( //
                        hasProperty("id", equalTo("step01")), //
                        hasProperty("id", equalTo("step02")), //
                        hasProperty("id", equalTo("step03")) //
                ) //
        ));
        assertThat(storedProvider.resources("**/*.*"), containsInAnyOrder( //
                new String[] { //
                        "description.xml", //
                        "step01/data.xml", //
                        "step01/post.xml", //
                        "step02/data.xml", //
                        "step03/data.xml" //
                } //
        ));
    }

}
