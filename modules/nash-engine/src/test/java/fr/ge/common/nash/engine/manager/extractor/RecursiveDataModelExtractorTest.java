/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.extractor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.test.builder.DataElementBuilder;
import fr.ge.common.nash.engine.test.builder.FormSpecificationDataBuilder;
import fr.ge.common.nash.engine.test.builder.GroupElementBuilder;

/**
 * Class RecursiveDataModelExtractorTest.
 */
public class RecursiveDataModelExtractorTest {

    /** extractor. */
    private final RecursiveDataModelExtractor extractor = RecursiveDataModelExtractor.create(null);

    /**
     * Test no group from specification.
     */
    @Test
    public void testNoGroupFromSpecification() {
        final FormSpecificationData formSpecification = new FormSpecificationDataBuilder("form01").build();

        final Map<String, Object> model = this.extractor.extract(formSpecification);

        assertNotNull(model);
        assertEquals(1, model.size());
        assertNotNull(model.get("form01"));
    }

    /**
     * Test no data from specification.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testNoDataFromSpecification() {
        final FormSpecificationData formSpecification = new FormSpecificationDataBuilder("form01").group(new GroupElementBuilder("group01").build()).build();

        final Map<String, Object> model = this.extractor.extract(formSpecification);

        assertNotNull(model);
        assertEquals(1, model.size());

        Map<String, Object> subModel = (Map<String, Object>) model.get("form01");
        assertNotNull(subModel);
        assertEquals(1, subModel.size());

        subModel = (Map<String, Object>) subModel.get("group01");
        assertNotNull(subModel);
        assertTrue(subModel.isEmpty());
    }

    /**
     * Test no data from group.
     */
    @Test
    public void testNoDataFromGroup() {
        final GroupElement groupElement = new GroupElementBuilder("group01").build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertTrue(model.isEmpty());
    }

    /**
     * Test from group.
     */
    @Test
    public void testFromGroup() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new DataElementBuilder("firstname").type("String").value("John").build(), //
                new DataElementBuilder("lastname").type("String").value("Doe").build(), //
                new DataElementBuilder("alias").type("String").build() //
        ).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertEquals(3, model.size());
        assertEquals("John", model.get("firstname"));
        assertEquals("Doe", model.get("lastname"));
        assertTrue(model.containsKey("alias"));
        assertNull(model.get("alias"));
    }

    /**
     * Test complex from group.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testComplexFromGroup() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new GroupElementBuilder("personal").datas( //
                        new GroupElementBuilder("address").datas( //
                                new DataElementBuilder("city").value("city value").type("String").build(), //
                                new DataElementBuilder("zip").value("zip value").type("String").build() //
                        ).build()).build()).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertEquals(1, model.size());

        Map<String, Object> subModel = (Map<String, Object>) model.get("personal");
        assertNotNull(subModel);
        assertEquals(1, subModel.size());

        subModel = (Map<String, Object>) subModel.get("address");
        assertNotNull(subModel);
        assertEquals(2, subModel.size());
        assertEquals("city value", subModel.get("city"));
        assertEquals("zip value", subModel.get("zip"));
    }

    /**
     * Test extract with multiple data.
     */
    @Test
    public void testExtractWithMultipleField() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new DataElementBuilder("firstname[0]").type("String").value("John").build(), //
                new DataElementBuilder("firstname[1]").type("String").value("Alex").build(), //
                new DataElementBuilder("firstname[2]").type("String").value("Eric").build(), //
                new DataElementBuilder("lastname").type("String").value("Doe").build(), //
                new DataElementBuilder("alias").type("String").build() //
        ).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertEquals(3, model.size());
        assertEquals(Arrays.asList("John", "Alex", "Eric"), model.get("firstname"));
        assertTrue(model.containsKey("alias"));
        assertNull(model.get("alias"));
    }

    /**
     * Test extract with multiple groups.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testExtractWithMultipleGroups() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new GroupElementBuilder("personal[0]").datas( //
                        new GroupElementBuilder("address").datas( //
                                new DataElementBuilder("city").value("city value").type("String").build(), //
                                new DataElementBuilder("zip").value("zip value").type("String").build() //
                        ).build()).build(), //
                new GroupElementBuilder("personal[1]").datas( //
                        new GroupElementBuilder("address").datas( //
                                new DataElementBuilder("city").value("city value1").type("String").build(), //
                                new DataElementBuilder("zip").value("zip value1").type("String").build() //
                        ).build()).build()).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertEquals(1, model.size());

        List<Object> subModel = (List<Object>) model.get("personal");
        assertNotNull(subModel);
        assertEquals(2, subModel.size());
    }

    /**
     * Test extract with multiple groups and multiple data.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testExtractWithMultipleGroupsAndMultipleData() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new GroupElementBuilder("personal[0]").datas( //
                        new DataElementBuilder("firstname[0]").type("String").value("John").build(), //
                        new DataElementBuilder("firstname[1]").type("String").value("Alex").build(), //
                        new DataElementBuilder("firstname[2]").type("String").value("Eric").build(), //
                        new GroupElementBuilder("address").datas( //
                                new DataElementBuilder("city").value("city value").type("String").build(), //
                                new DataElementBuilder("zip").value("zip value").type("String").build()
                        //
                        ).build()).build(), //
                new GroupElementBuilder("personal[1]").datas( //
                        new DataElementBuilder("firstname[0]").type("String").value("Michael").build(), //
                        new DataElementBuilder("firstname[1]").type("String").value("Philip").build(), //
                        new DataElementBuilder("firstname[2]").type("String").value("Donald").build(), //
                        new GroupElementBuilder("address").datas( //
                                new DataElementBuilder("city").value("city value1").type("String").build(), //
                                new DataElementBuilder("zip").value("zip value1").type("String").build()
                        //
                        ).build()).build()).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertEquals(1, model.size());

        List<Object> subModel = (List<Object>) model.get("personal");
        assertNotNull(subModel);
        assertEquals(2, subModel.size());
    }

    /**
     * Test extract with empty multiple data.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testExtractWithEmptyMultipleData() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new DataElementBuilder("firstname[]").type("String").build(), //
                new DataElementBuilder("lastname").type("String").value("Doe").build(), //
                new DataElementBuilder("alias").type("String").build() //
        ).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertEquals(3, model.size());

        List<Object> subModel = (List<Object>) model.get("firstname");
        assertEquals(1, subModel.size());
    }

    /**
     * Test no data from specification.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testFromSpecificationWithMultipleData() {
        final FormSpecificationData formSpecification = new FormSpecificationDataBuilder("form01").group(new GroupElementBuilder("group01").datas( //
                new DataElementBuilder("firstname[0]").type("String").value("John").build(), //
                new DataElementBuilder("firstname[1]").type("String").value("Alex").build(), //
                new DataElementBuilder("firstname[2]").type("String").value("Eric").build(), //
                new DataElementBuilder("lastname").type("String").value("Doe").build(), //
                new DataElementBuilder("alias").type("String").build() //
        ).build()).build();

        final Map<String, Object> model = this.extractor.extract(formSpecification);

        assertNotNull(model);
        assertEquals(1, model.size());

        Map<String, Object> subModel = (Map<String, Object>) model.get("form01");
        assertNotNull(subModel);
        assertEquals(1, subModel.size());

        subModel = (Map<String, Object>) subModel.get("group01");
        assertNotNull(subModel);
        assertEquals(3, subModel.size());

        assertEquals(Arrays.asList("John", "Alex", "Eric"), subModel.get("firstname"));
    }

    /**
     * Test with empty multiple group.
     */
    @Test
    public void testFromEmptyMultipleGroup() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new GroupElementBuilder("personal[]").datas( //
                        new DataElementBuilder("firstname[0]").type("String").value("John").build(), //
                        new DataElementBuilder("firstname[1]").type("String").value("Alex").build(), //
                        new DataElementBuilder("firstname[2]").type("String").value("Eric").build(), //
                        new GroupElementBuilder("address").datas( //
                                new DataElementBuilder("city").value("city value").type("String").build(), //
                                new DataElementBuilder("zip").value("zip value").type("String").build()
                        //
                        ).build()).build()).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);
        assertNotNull(model);
        assertEquals(1, model.size());

        List<Object> subModel = (List<Object>) model.get("personal");
        assertNotNull(subModel);
        assertEquals(1, subModel.size());
    }

    /**
     * Test extract with empty multiple data.
     */
    @Test(expected = TechnicalException.class)
    public void testExtractWithDuplicateField() {
        final GroupElement groupElement = new GroupElementBuilder("group01").datas( //
                new DataElementBuilder("lastname").type("String").value("Doe").build(), //
                new DataElementBuilder("lastname").type("String").value("Doe1").build(), //
                new DataElementBuilder("alias").type("String").build() //
        ).build();

        final Map<String, Object> model = this.extractor.extract(groupElement);

        assertNotNull(model);
        assertEquals(3, model.size());

        List<Object> subModel = (List<Object>) model.get("firstname");
        assertEquals(1, subModel.size());
    }
}
