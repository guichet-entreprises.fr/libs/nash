/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.test.AbstractTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class EngineContextTest extends AbstractTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testBuildRecordModel() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final Map<String, Object> model = loader.buildEngineContext().buildModel();
        final Map<String, Object> recordModel = CoreUtil.cast(model.get(Scopes.RECORD.toContextKey()));

        assertThat(model.size(), equalTo(1));
        assertThat(recordModel, notNullValue());

        final Map<String, Object> step01Model = CoreUtil.cast(recordModel.get("step01"));
        final Map<String, Object> step02Model = CoreUtil.cast(recordModel.get("step02"));

        assertThat(recordModel.size(), equalTo(2));
        assertThat(step01Model, notNullValue());
        assertThat(step02Model, notNullValue());
    }

    @Test
    public void testBuildStepModel() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);

        final Map<String, Object> model = loader.buildEngineContext(1).buildModel();
        final Map<String, Object> recordModel = CoreUtil.cast(model.get(Scopes.RECORD.toContextKey()));
        final Map<String, Object> stepModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));

        assertThat(model.size(), equalTo(2));
        assertThat(recordModel, notNullValue());
        assertThat(recordModel.size(), equalTo(2));
        assertThat(stepModel, notNullValue());
        assertThat(stepModel, equalTo(recordModel.get("step02")));
    }

    @Test
    public void testBuildStepModelFromData() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);
        final FormSpecificationData step = loader.data(1);

        final Map<String, Object> model = loader.buildEngineContext(step).buildModel();
        final Map<String, Object> recordModel = CoreUtil.cast(model.get(Scopes.RECORD.toContextKey()));
        final Map<String, Object> stepModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));

        assertThat(model.size(), equalTo(2));
        assertThat(recordModel, notNullValue());
        assertThat(recordModel.size(), equalTo(1));
        assertThat(stepModel, notNullValue());
        assertThat(stepModel, equalTo(recordModel.get("step02")));
    }

    @Test
    public void testBuildPageElementModel() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);
        final FormSpecificationData step = loader.data(1);
        final GroupElement page = step.getGroups().get(0);

        final Map<String, Object> model = loader.buildEngineContext(1).target(page).buildModel();
        final Map<String, Object> recordModel = CoreUtil.cast(model.get(Scopes.RECORD.toContextKey()));
        final Map<String, Object> stepModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));
        final Map<String, Object> pageModel = CoreUtil.cast(model.get(Scopes.PAGE.toContextKey()));
        final Map<String, Object> groupModel = CoreUtil.cast(model.get(Scopes.GROUP.toContextKey()));

        assertThat(model.size(), equalTo(4));

        assertThat(recordModel, notNullValue());
        assertThat(recordModel.size(), equalTo(2));

        assertThat(stepModel, equalTo(recordModel.get("step02")));
        assertThat(pageModel, equalTo(stepModel.get("grp01x02")));
        assertThat(groupModel, equalTo(pageModel));
    }

    @Test
    public void testBuildFirstLevelDataElementModel() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);
        final FormSpecificationData step = loader.data(1);
        final GroupElement page = step.getGroups().get(0);
        final DataElement data = (DataElement) page.getData().get(1);

        final Map<String, Object> model = loader.buildEngineContext(1).target(data).buildModel();
        final Map<String, Object> recordModel = CoreUtil.cast(model.get(Scopes.RECORD.toContextKey()));
        final Map<String, Object> stepModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));
        final Map<String, Object> pageModel = CoreUtil.cast(model.get(Scopes.PAGE.toContextKey()));
        final Map<String, Object> groupModel = CoreUtil.cast(model.get(Scopes.GROUP.toContextKey()));

        assertThat(model.size(), equalTo(4));

        assertThat(recordModel, notNullValue());
        assertThat(recordModel.size(), equalTo(2));

        assertThat(stepModel, equalTo(recordModel.get("step02")));
        assertThat(pageModel, equalTo(stepModel.get("grp01x02")));
        assertThat(groupModel, equalTo(pageModel));
    }

    @Test
    public void testBuildInDepthDataElementModel() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);
        final FormSpecificationData step = loader.data(1);
        final GroupElement page = step.getGroups().get(0);
        final GroupElement grp02 = (GroupElement) page.getData().get(3);
        final GroupElement grp03 = (GroupElement) grp02.getData().get(1);
        final DataElement data = (DataElement) grp03.getData().get(0);

        final Map<String, Object> model = loader.buildEngineContext(1).target(data).buildModel();
        final Map<String, Object> recordModel = CoreUtil.cast(model.get(Scopes.RECORD.toContextKey()));
        final Map<String, Object> stepModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));
        final Map<String, Object> pageModel = CoreUtil.cast(model.get(Scopes.PAGE.toContextKey()));
        final Map<String, Object> groupModel = CoreUtil.cast(model.get(Scopes.GROUP.toContextKey()));

        final Map<String, Object> subPageModel = CoreUtil.cast(pageModel.get("grp02x02"));

        assertThat(model.size(), equalTo(4));

        assertThat(recordModel, notNullValue());
        assertThat(recordModel.size(), equalTo(2));

        assertThat(stepModel, equalTo(recordModel.get("step02")));
        assertThat(pageModel, equalTo(stepModel.get("grp01x02")));
        assertThat(groupModel, equalTo(subPageModel.get("grp03x02")));
    }

    @Test
    public void testBuildInDepthRepeatableDataElementModel() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, null, provider);
        final FormSpecificationData step = loader.data(1);
        final GroupElement page = step.getGroups().get(0);
        final GroupElement grp02 = (GroupElement) page.getData().get(3);
        final GroupElement grp03 = (GroupElement) grp02.getData().get(3);
        final DataElement data = (DataElement) grp03.getData().get(0);

        final Map<String, Object> model = loader.buildEngineContext(1).target(data).buildModel();
        final Map<String, Object> recordModel = CoreUtil.cast(model.get(Scopes.RECORD.toContextKey()));
        final Map<String, Object> stepModel = CoreUtil.cast(model.get(Scopes.STEP.toContextKey()));
        final Map<String, Object> pageModel = CoreUtil.cast(model.get(Scopes.PAGE.toContextKey()));
        final Map<String, Object> groupModel = CoreUtil.cast(model.get(Scopes.GROUP.toContextKey()));

        final Map<String, Object> subPageModel = CoreUtil.cast(pageModel.get("grp02x02"));

        assertThat(model.size(), equalTo(4));

        assertThat(recordModel, notNullValue());
        assertThat(recordModel.size(), equalTo(2));

        assertThat(stepModel, equalTo(recordModel.get("step02")));
        assertThat(pageModel, equalTo(stepModel.get("grp01x02")));
        assertThat(groupModel, equalTo(subPageModel.get("grp03x02")));
    }

}
