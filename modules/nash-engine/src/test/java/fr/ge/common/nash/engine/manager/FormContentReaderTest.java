/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import fr.ge.common.nash.core.exception.TechnicalException;

/**
 * Tests {@link FormContentReader}. "g" is a group, "f" a field and "a" an
 * attribute.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class FormContentReaderTest {

    /**
     * Tests {@link FormContentReader#read(byte[])}.
     */
    @Test
    public void testReadJson() {
        final byte[] jsonAsBytesSimple = "{\"step01\":{\"grp01\":{\"fld01\":\"42\",\"fld02\":\"test\"},\"grp02\":[{\"id\":\"key01\",\"label\":\"label 01\"},{\"id\":\"key02\",\"label\":\"label 02\"}],\"grp03\":[\"test 03a\",\"test 03b\"]}}"
                .getBytes(StandardCharsets.UTF_8);

        final byte[] jsonAsBytes = "{\"step01\":{\"grp01\":{\"fld01\":\"42\",\"fld02\":\"test\"},\"grp02\":[{\"id\":\"key01\",\"label\":\"label 01\"},{\"id\":\"key02\",\"label\":\"label 02\"}],\"grp03\":[\"test 03a\",\"test 03b\"],\"grp04\":[{\"key\":\"name 04a\",\"value\":\"test 04a\"},{\"key\":\"name 04b\",\"value\":\"test 04b\"}]}}"
                .getBytes(StandardCharsets.UTF_8);

        final FormContentData data = FormContentReader.read(jsonAsBytes);
        assertThat(data.asMap().size(), equalTo(9));
        assertThat(data.withPath("step01.grp01.fld01").asString(), equalTo("42"));
        assertThat(data.withPath("step01.grp01.fld02").asString(), equalTo("test"));
        assertThat(data.withPath("step01.grp02.key01").asString(), equalTo("label 01"));
        assertThat(data.withPath("step01.grp02.key02").asString(), equalTo("label 02"));
        assertThat(data.withPath("step01.grp03").asList().get(0).asString(), equalTo("test 03a"));
        assertThat(data.withPath("step01.grp03").asList().get(1).asString(), equalTo("test 03b"));
        assertThat(data.withPath("step01.grp04").asList().get(0).withPath("key").asString(), equalTo("name 04a"));
        assertThat(data.withPath("step01.grp04").asList().get(0).withPath("value").asString(), equalTo("test 04a"));
        assertThat(data.withPath("step01.grp04").asList().get(1).withPath("key").asString(), equalTo("name 04b"));
        assertThat(data.withPath("step01.grp04").asList().get(1).withPath("value").asString(), equalTo("test 04b"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsString() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1A");
        request.addParameter("g1.fB", "1B");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fA").asString(), equalTo("1A"));
        assertThat(data.withPath("g1.fB").asString(), equalTo("1B"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsStringShortcut() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1A");
        request.addParameter("g1.fB", "1B");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.asString("g1.fA"), equalTo("1A"));
        assertThat(data.asString("g1.fB"), equalTo("1B"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsStringMultipleFieldNoElement() {
        final MockHttpServletRequest request = new MockHttpServletRequest();

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fA").asString(), equalTo(null));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsStringMultipleFieldOneElement() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", new String[] { "1A" });

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fA").asString(), equalTo("1A"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test(expected = TechnicalException.class)
    public void testWithPathAsStringMultipleFieldSeveralElement() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", new String[] { "1A", "1B" });

        final FormContentData data = FormContentReader.read(request);
        data.withPath("g1.fA").asString();
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test(expected = TechnicalException.class)
    public void testWithPathAsStringArray() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA[0]", "1A0");
        request.addParameter("g1.fA[1]", "1A1");

        final FormContentData data = FormContentReader.read(request);
        data.withPath("g1.fA").asString();
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsStringGroup() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1A");
        request.addParameter("g1.fB", "1B");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1").asString(), equalTo(null));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsStringUnknown() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1A");
        request.addParameter("g1.fB", "1B");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("xxx").asString(), equalTo(null));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsStringDeep() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.g2.fC", "12C");
        request.addParameter("g1.g2.g3.fD", "123D");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.g2.fC").asString(), equalTo("12C"));
        assertThat(data.withPath("g1.g2.g3.fD").asString(), equalTo("123D"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsStringDoublePath() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.g2.fC", "12C");
        request.addParameter("g1.g2.g3.fD", "123D");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.g2").withPath("fC").asString(), equalTo("12C"));
        assertThat(data.withPath("g1.g2").withPath("g3.fD").asString(), equalTo("123D"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsInt() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1");
        request.addParameter("g1.fB", "2");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fA").asInt(), equalTo(1));
        assertThat(data.withPath("g1.fB").asInt(), equalTo(2));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsIntShortcut() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1");
        request.addParameter("g1.fB", "2");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.asInt("g1.fA"), equalTo(1));
        assertThat(data.asInt("g1.fB"), equalTo(2));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test(expected = TechnicalException.class)
    public void testWithPathAsIntMultipleFieldSeveralElement() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", new String[] { "1", "2" });

        final FormContentData data = FormContentReader.read(request);
        data.withPath("g1.fA").asString();
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsIntWrongFormat() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1A");
        request.addParameter("g1.fB", "2B");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fA").asInt(), equalTo(null));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsIntEmpty() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "");
        request.addParameter("g1.fB", "");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fA").asInt(), equalTo(null));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsBoolean() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "true");
        request.addParameter("g1.fB", "false");
        request.addParameter("g1.fC", "t");
        request.addParameter("g1.fD", "f");
        request.addParameter("g1.fE", "yes");
        request.addParameter("g1.fF", "no");
        request.addParameter("g1.fG", "y");
        request.addParameter("g1.fH", "n");
        request.addParameter("g1.fI", "on");
        request.addParameter("g1.fJ", "off");
        request.addParameter("g1.fK", "1");
        request.addParameter("g1.fL", "0");
        request.addParameter("g1.fM", "");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fA").asBoolean(), equalTo(true));
        assertThat(data.withPath("g1.fB").asBoolean(), equalTo(false));
        assertThat(data.withPath("g1.fC").asBoolean(), equalTo(true));
        assertThat(data.withPath("g1.fD").asBoolean(), equalTo(false));
        assertThat(data.withPath("g1.fE").asBoolean(), equalTo(true));
        assertThat(data.withPath("g1.fF").asBoolean(), equalTo(false));
        assertThat(data.withPath("g1.fG").asBoolean(), equalTo(true));
        assertThat(data.withPath("g1.fH").asBoolean(), equalTo(false));
        assertThat(data.withPath("g1.fI").asBoolean(), equalTo(true));
        assertThat(data.withPath("g1.fJ").asBoolean(), equalTo(false));
        assertThat(data.withPath("g1.fK").asBoolean(), equalTo(true));
        assertThat(data.withPath("g1.fL").asBoolean(), equalTo(false));
        assertThat(data.withPath("g1.fM").asBoolean(), equalTo(false));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsBooleanShortcut() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "true");
        request.addParameter("g1.fB", "false");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.asBoolean("g1.fA"), equalTo(true));
        assertThat(data.asBoolean("g1.fB"), equalTo(false));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test(expected = TechnicalException.class)
    public void testWithPathAsBooleanMultipleFieldSeveralElement() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", new String[] { "1", "2" });

        final FormContentData data = FormContentReader.read(request);
        data.withPath("g1.fA").asBoolean();
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsBooleanWrongFormat() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "trueX");
        request.addParameter("g1.fB", "falseX");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.asBoolean("g1.fA"), equalTo(false));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsList() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fE[0]", "1E0");
        request.addParameter("g1.fE.1", "1E1");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fE").asList().get(0).asString(), equalTo("1E0"));
        assertThat(data.withPath("g1.fE").asList().get(1).asString(), equalTo("1E1"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsListShortcut() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fE[0]", "1E0");
        request.addParameter("g1.fE.1", "1E1");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.asList("g1.fE").get(0).asString(), equalTo("1E0"));
        assertThat(data.asList("g1.fE").get(1).asString(), equalTo("1E1"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsListMultipleField() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fE", new String[] { "1E0", "1E1" });

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fE").asList().get(0).asString(), equalTo("1E0"));
        assertThat(data.withPath("g1.fE").asList().get(1).asString(), equalTo("1E1"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsListObjects() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fF[0].aV", "1F0V");
        request.addParameter("g1.fF[0].aW", "1F0W");
        request.addParameter("g1.fF.1.aX", "1F0X");
        request.addParameter("g1.fF.1.aY", "1F0Y");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fF").asList().get(0).withPath("aV").asString(), equalTo("1F0V"));
        assertThat(data.withPath("g1.fF").asList().get(0).withPath("aW").asString(), equalTo("1F0W"));
        assertThat(data.withPath("g1.fF").asList().get(1).withPath("aX").asString(), equalTo("1F0X"));
        assertThat(data.withPath("g1.fF").asList().get(1).withPath("aY").asString(), equalTo("1F0Y"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsListDoubleList() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fG[0][0]", "1G00");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fG").asList().get(0).asList().get(0).asString(), equalTo("1G00"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsListDoubleListObjects() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fH[0][0].aV", "1H00V");
        request.addParameter("g1.fH[0][0].aW", "1H00W");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fH").asList().get(0).asList().get(0).withPath("aV").asString(), equalTo("1H00V"));
        assertThat(data.withPath("g1.fH").asList().get(0).asList().get(0).withPath("aW").asString(), equalTo("1H00W"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsMap() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1A");
        request.addParameter("g1.fB", "1B");
        request.addParameter("g1.g2.fC", "12C");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1").asMap().keySet(), hasItems("fA", "fB", "g2.fC"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsMapShortcut() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fA", "1A");
        request.addParameter("g1.fB", "1B");
        request.addParameter("g1.g2.fC", "12C");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.asMap("g1").keySet(), hasItems("fA", "fB", "g2.fC"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testWithPathAsMapDeep() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("g1.fH[0][0].aV", "1H00V");
        request.addParameter("g1.fH[0][0].aW", "1H00W");

        final FormContentData data = FormContentReader.read(request);
        assertThat(data.withPath("g1.fH").asList().get(0).asList().get(0).asMap().get("aV")[0], equalTo("1H00V"));
        assertThat(data.withPath("g1.fH").asList().get(0).asList().get(0).asMap().get("aW")[0], equalTo("1H00W"));
    }

    /**
     * Tests {@link FormContentReader#read(javax.servlet.http.HttpServletRequest)}.
     */
    @Test
    public void testChildren() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("field01", "1");
        request.addParameter("leader[0].firstname", "Jo");
        request.addParameter("leader[1].firstname", "Ade");
        request.addParameter("grp1.adm[0].firstname", "2");
        request.addParameter("grp1.adm[1].firstname", "3");

        final List<FormContentData> data = FormContentReader.read(request).children();

        assertThat(data.size(), equalTo(4));
        assertThat(data.get(0).getCurrentPath(), equalTo("field01"));
        assertThat(data.get(0).asString(), equalTo("1"));
        assertThat(data.get(0).children().size(), equalTo(0));

        assertThat(data.get(1).getCurrentPath(), equalTo("leader[0]"));
        assertThat(data.get(1).asString(), equalTo(null));
        assertThat(data.get(1).children().size(), equalTo(1));
        assertThat(data.get(1).children().get(0).getCurrentPath(), equalTo("leader[0].firstname"));
        assertThat(data.get(1).children().get(0).asString(), equalTo("Jo"));

        assertThat(data.get(2).getCurrentPath(), equalTo("leader[1]"));
        assertThat(data.get(2).asString(), equalTo(null));
        assertThat(data.get(2).children().size(), equalTo(1));
        assertThat(data.get(2).children().get(0).getCurrentPath(), equalTo("leader[1].firstname"));
        assertThat(data.get(2).children().get(0).asString(), equalTo("Ade"));

        assertThat(data.get(3).getCurrentPath(), equalTo("grp1"));
        assertThat(data.get(3).children().size(), equalTo(2));
        assertThat(data.get(3).children().get(0).getCurrentPath(), equalTo("grp1.adm[0]"));
        assertThat(data.get(3).children().get(0).asString(), equalTo(null));
        assertThat(data.get(3).children().get(0).children().size(), equalTo(1));
        assertThat(data.get(3).children().get(0).children().get(0).getCurrentPath(), equalTo("grp1.adm[0].firstname"));
        assertThat(data.get(3).children().get(0).children().get(0).asString(), equalTo("2"));
        assertThat(data.get(3).children().get(1).getCurrentPath(), equalTo("grp1.adm[1]"));
        assertThat(data.get(3).children().get(1).asString(), equalTo(null));
        assertThat(data.get(3).children().get(1).children().size(), equalTo(1));
        assertThat(data.get(3).children().get(1).children().get(0).getCurrentPath(), equalTo("grp1.adm[1].firstname"));
        assertThat(data.get(3).children().get(1).children().get(0).asString(), equalTo("3"));
    }
}
