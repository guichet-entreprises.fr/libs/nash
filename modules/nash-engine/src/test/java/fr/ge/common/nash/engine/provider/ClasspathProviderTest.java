/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.provider;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.transition.TransitionDescription;
import fr.ge.common.nash.engine.provider.ClasspathProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Tests {@link ClasspathProvider}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class ClasspathProviderTest extends AbstractTest {

    private static final String BASE_PACKAGE_PATH = Engine.class.getPackage().getName().replace('.', '/');

    /**
     * Tests {@link ClasspathProvider#load(String)}.
     *
     * @throws Exception
     *             an exception
     */
    @Test
    public void testLoad() throws Exception {
        final ClasspathProvider provider = new ClasspathProvider(this.getClass(), String.format("/%s/transition/fooBar-1.0", BASE_PACKAGE_PATH));
        assertThat(provider.asBytes("description.xml"), equalTo(this.resourceAsBytes(String.format("/%s/transition/fooBar-1.0/description.xml", BASE_PACKAGE_PATH))));
    }

    /**
     * Tests {@link ClasspathProvider#asBean(String, Class)}.
     *
     * @throws Exception
     *             an exception
     */
    @Test
    public void testLoadAsBean() throws Exception {
        final ClasspathProvider provider = new ClasspathProvider(this.getClass(), String.format("/%s/transition/fooBar-1.0", BASE_PACKAGE_PATH));
        final TransitionDescription desc = provider.asBean("description.xml", TransitionDescription.class);
        assertThat(desc, //
                allOf( //
                        hasProperty("reference", //
                                hasProperty("code", equalTo("fooBar")) //
                        ), //
                        hasProperty("title", equalTo("Foo Bar")) //
                ) //
        );
    }

}
