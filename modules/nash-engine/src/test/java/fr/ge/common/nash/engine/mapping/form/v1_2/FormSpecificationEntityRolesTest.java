package fr.ge.common.nash.engine.mapping.form.v1_2;

import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.mapping.form.v1_2.roles.RoleElement;
import fr.ge.common.utils.test.AbstractTest;

public class FormSpecificationEntityRolesTest extends AbstractTest {

    FormSpecificationEntities fse;
    FormSpecificationEntityRoles fsr1;
    FormSpecificationEntityRoles fsr2;
    RoleElement role_user;
    RoleElement role_referent;
    String ENTITY_PATH1 = "GE/CA/2018-AAA-BBB-CCC-69";
    String ENTITY_PATH2 = "GE/CA/2018-AAA-BBB-CCC-77";
    String ROLE_USER = "user";
    String ROLE_REFERENT = "referent";
    ArrayList<RoleElement> roles;
    ArrayList<FormSpecificationEntityRoles> entities;

    @Before
    public void setup() {

        this.fse = new FormSpecificationEntities();
        this.fsr1 = new FormSpecificationEntityRoles();
        this.fsr2 = new FormSpecificationEntityRoles();
        this.fsr1.setFullPath(ENTITY_PATH1);
        this.fsr2.setFullPath(ENTITY_PATH2);
        this.role_user = new RoleElement();
        this.role_user.setRole(ROLE_USER);
        this.role_referent = new RoleElement();
        this.role_referent.setRole(ROLE_REFERENT);
        this.fsr1.setRoles(new ArrayList<RoleElement>());
        this.fsr1.getRoles().add(role_user);
        this.fsr1.getRoles().add(role_referent);
        this.fsr2.setRoles(new ArrayList<RoleElement>());
        this.fsr2.getRoles().add(role_user);
        this.fsr2.getRoles().add(role_referent);

        this.entities = new ArrayList<FormSpecificationEntityRoles>();
        this.entities.add(fsr1);
        this.entities.add(fsr2);
        fse.setEntities(entities);

        this.roles = new ArrayList<RoleElement>();
        this.roles.add(role_user);
        this.roles.add(role_referent);
    }

    @Test
    public void testBeans() throws Exception {

        assertEquals(this.fse.getEntities(), entities);
        assertEquals(this.fsr1.getRoles(), roles);
        assertEquals(this.fsr1.getFullPath(), this.ENTITY_PATH1);
        assertEquals(this.fsr1.getRoles(), roles);
    }

    @Test
    public void testMarshall() throws Exception {
        String resultingXML = JaxbFactoryImpl.instance().asString(this.fse);
        String expectedXML = this.resourceAsString("roles.xml");
        assertThat(resultingXML, not(isEmptyString()));
        assertThat(expectedXML, not(isEmptyString()));

        if (expectedXML != null && !expectedXML.isEmpty()) {
            assertEquals(expectedXML.replaceAll("\\s", ""), resultingXML.replaceAll("\\s", ""));
        } else {
            fail("XML didn't generate as expected");
        }
    }

    @Test
    public void testUnmarshall() throws Exception {
        FormSpecificationEntities rolesXML = JaxbFactoryImpl.instance().unmarshal(this.resourceAsBytes("roles.xml"), FormSpecificationEntities.class);
        FormSpecificationEntityRoles entity = rolesXML.getEntities().get(0);
        assertEquals(entity.getFullPath(), this.ENTITY_PATH1);
        assertEquals(entity.getRoles().get(0).getRole(), this.ROLE_USER);
        assertEquals(entity.getRoles().get(1).getRole(), this.ROLE_REFERENT);
    }

}
