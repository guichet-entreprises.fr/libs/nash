/**
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.test.builder;

import java.util.Arrays;
import java.util.List;

import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DefaultElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;

/**
 * Class FormSpecificationDataBuilder.
 *
 * @author Christian Cougourdan
 */
public class FormSpecificationDataBuilder {

    /** id. */
    private final String id;

    /** def. */
    private DefaultElement def;

    /** group. */
    private List<GroupElement> group;

    /**
     * Instantie un nouveau form specification data builder.
     *
     * @param id
     *            id
     */
    public FormSpecificationDataBuilder(final String id) {
        this.id = id;
    }

    /**
     * Build.
     *
     * @return form specification data
     */
    public FormSpecificationData build() {
        final FormSpecificationData spec = new FormSpecificationData();

        spec.setId(this.id);
        spec.setDefault(this.def);
        spec.setGroups(this.group);

        return spec;
    }

    /**
     * Group.
     *
     * @param group
     *            group
     * @return form specification data builder
     */
    public FormSpecificationDataBuilder group(final GroupElement... group) {
        this.group = Arrays.asList(group);
        return this;
    }

    /**
     * Def.
     *
     * @param def
     *            def
     * @return form specification data builder
     */
    public FormSpecificationDataBuilder def(final DefaultElement def) {
        this.def = def;
        return this;
    }

}
