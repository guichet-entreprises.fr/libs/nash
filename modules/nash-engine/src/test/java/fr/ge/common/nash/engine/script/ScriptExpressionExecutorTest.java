/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.script;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.core.exception.ExpressionException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.bridge.UtilProcessorBridge;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.nash.test.AbstractBeanResourceTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class ScriptExpressionExecutorTest extends AbstractBeanResourceTest {

    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private Engine engine;

    @Autowired
    private ScriptExpressionExecutorFactory factory;

    @Test
    public void testInit() throws Exception {
        final ScriptExpressionExecutor executor = this.factory.getScriptExpressionExecutor();
        executor.eval("log.debug('Go !!!');", null);
    }

    @Test
    public void testSyntaxError() throws Exception {
        final ScriptExpressionExecutor executor = this.factory.getScriptExpressionExecutor();
        try {
            executor.eval("incorrect script", null);
            fail("Expected ExpressionException");
        } catch (final ExpressionException ex) {
            assertEquals("<eval>:1:10 Expected ; but found script\nincorrect script\n          ^ in <eval> at line number 1 at column number 10", ex.getMessage());
            assertEquals(1, ex.getLineNumber());
            assertEquals(10, ex.getColumnNumber());
        }
    }

    @Test
    public void testScriptError() throws Exception {
        final ScriptExpressionExecutor executor = this.factory.getScriptExpressionExecutor();
        try {
            executor.eval("throw new Error();", null);
            fail("Expected ExpressionException");
        } catch (final ExpressionException ex) {
            assertEquals("Error in <eval> at line number 1 at column number 0", ex.getMessage());
            assertEquals(1, ex.getLineNumber());
            assertEquals(0, ex.getColumnNumber());
        }
    }

    @Test
    public void testStringExpression() throws Exception {
        final boolean value = this.applicationContext.getBean(NashScriptEngine.class).eval("'ok' != 'ko'", null, Boolean.class);

        assertThat(value, equalTo(true));
    }

    @Test
    public void testLibValue() throws Exception {
        final Object value = this.applicationContext.getBean(NashScriptEngine.class).eval("typeof Value", null);
        assertThat(value, equalTo("function"));
    }

    @Test
    public void testLibValueObjectArrayContains() throws Exception {
        final Object value = this.applicationContext.getBean(NashScriptEngine.class).eval( //
                "Value('id').of($grp.fld).contains('2')", //
                true, //
                new ScriptExecutionContext() //
                        .add(Scopes.PAGE.toContextKey(), this.mapper.readValue("{\"grp\":{\"fld\":[{\"id\":\"2\",\"label\":\"option 2\"}]}}", Map.class)) //
                        .addBridge(new UtilProcessorBridge(null)) //
        );

        assertThat(value, equalTo(true));
    }

    @Test
    public void testLibValueObjectContains() throws Exception {
        final Object value = this.applicationContext.getBean(NashScriptEngine.class).eval( //
                "Value('id').of($grp.fld).contains('2')", //
                true, //
                new ScriptExecutionContext() //
                        .add(Scopes.PAGE.toContextKey(), this.mapper.readValue("{\"grp\":{\"fld\":{\"id\":\"2\",\"label\":\"option 2\"}}}", Map.class)) //
                        .addBridge(new UtilProcessorBridge(null)) //
        );

        assertThat(value, equalTo(true));
    }

    @Test
    public void testRequire() throws Exception {
        final Object value = this.applicationContext.getBean(NashScriptEngine.class).eval("_logger.debug('hello'); _logger.debug(Value); 'ok';", null);

        assertThat(value, notNullValue());
    }

    @Test(expected = ExpressionException.class)
    public void testPackageExcluded() {
        this.applicationContext.getBean(NashScriptEngine.class).eval("var map = new java.lang.Thread();", null, Thread.class);
    }

    @Test(expected = TechnicalException.class)
    @Ignore
    public void testInfinitiLoop() {
        this.applicationContext.getBean(NashScriptEngine.class).eval("while(true){}", null, Object.class);
    }

    @Test
    public void testExternalLibs() throws Exception {
        final ScriptExpressionExecutor executor = this.factory.getScriptExpressionExecutor();
        executor.eval("libs.log.debug('Go !!!'); libs.cplxNameWithDiffChars();", null);
    }

    @Test
    public void testRecordLibs() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);

        loader.processes().preExecute(loader.stepsMgr().current());
    }

}
