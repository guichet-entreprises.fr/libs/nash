/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.bridge.HangoutProcessorBridge;
import fr.ge.common.nash.engine.manager.processor.result.RedirectProcessResult;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.test.builder.DataElementBuilder;
import fr.ge.common.nash.engine.test.builder.FormSpecificationDataBuilder;
import fr.ge.common.nash.engine.test.builder.GroupElementBuilder;
import fr.ge.common.nash.engine.test.builder.ProcessElementBuilder;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class AttachmentProcessorTest.
 *
 * @author Adil Bsibiss
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class ProxyOutProcessorTest extends AbstractTest {

    /** The endpoint. */
    protected static final String ENDPOINT = "http://localhost:8888/test";

    /** The server. */
    protected Server server;

    /** The service. */
    protected ITestService service;

    /** The context. */
    protected EngineContext<?> context;

    /** The specification provider. */
    @Mock
    private IProvider specProvider;

    /** The bridge. */
    @Mock
    private HangoutProcessorBridge bridge;

    /** The processor. */
    @InjectMocks
    private ProxyOutProcessor processor;

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(this.specProvider.asBean(any(String.class), any())).thenCallRealMethod();

        final Properties properties = new Properties();
        this.context = this.applicationContext.getBean(SpecificationLoader.class, new Configuration(properties), this.specProvider).buildEngineContext();

        this.server = null;
    }

    /**
     * Sets the up rest server.
     *
     * @throws Exception
     *             the exception
     */
    private void setUpRestServer() throws Exception {
        this.service = mock(ITestService.class);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.service);
        serverFactory.setProvider(jsonProvider);
        this.server = serverFactory.create();
    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             the exception
     */
    @After
    public void tearDown() throws Exception {
        if (null != this.server) {
            this.server.stop();
            this.server.destroy();
        }
    }

    /**
     * Builds a process.
     *
     * @return the process
     */
    private ProcessElement buildProcess() {
        final String scriptContent = new StringBuilder() //
                .append("var nfo = nash.hangout.stamp({") //
                .append("'files' : [") //
                .append("{'document':null, 'zoneId':'id1'},") //
                .append("{'document':null, 'zoneId':'id2'}") //
                .append("],") //
                .append("'civility' : 'Monsieur',") //
                .append("'lastName' : 'LAST',") //
                .append("'firstName' : 'First',") //
                .append("'email' : 'first.last@domain.com',") //
                .append("'phone' : '06 00 00 00 00'") //
                .append("});") //
                .append("return nfo;") //
                .toString();
        return new ProcessElementBuilder() //
                .id("stampingInit") //
                .type("proxy.out") //
                .input("/step01/data.xml") //
                .output("proxy-calling.xml") //
                .value(scriptContent) //
                .build();
    }

    /**
     * Test validate conditions no output name.
     */
    @Test
    public void testValidateConditionsNoOutputName() {
        // prepare
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(new ProcessElement()));

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider, Mockito.never()).remove(any(String.class));
        assertTrue(actual);
    }

    /**
     * Test validate conditions no param file.
     */
    @Test
    public void testValidateConditionsNoParamFile() {
        // prepare
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider).remove(eq("proxy-calling.xml"));
        assertTrue(actual);
    }

    /**
     * Test validate conditions param file with no id.
     */
    @Test
    public void testValidateConditionsParamFileWithNoId() {
        // prepare
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        when(this.specProvider.asBytes(eq("proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling-param-file-no-id.xml"));

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider).asBytes(eq("proxy-calling.xml"));
        assertFalse(actual);
    }

    /**
     * Test validate conditions no result file.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testValidateConditionsNoResultFile() throws Exception {
        // prepare
        this.setUpRestServer();
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        when(this.specProvider.asBytes(eq("proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling.xml"));
        when(this.specProvider.asBytes(eq("/step01/data.xml"))).thenReturn(this.resourceAsBytes("data/step01/data.xml"));
        when(this.service.validate(any())).thenReturn(Response.ok("true").build());

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider).asBytes(eq("proxy-calling.xml"));
        verify(this.specProvider, times(4)).asBytes(eq("/step01/data.xml"));
        assertTrue(actual);
    }

    /**
     * Test validate conditions token not valid.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testValidateConditionsTokenNotValid() throws Exception {
        // prepare
        this.setUpRestServer();
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        when(this.specProvider.asBytes(eq("proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling.xml"));
        when(this.specProvider.asBytes(eq("/step01/data.xml"))).thenReturn(this.resourceAsBytes("data/step01/data.xml"));
        // when(this.specProvider.asBytes(eq("resultFileName.xml"))).thenReturn("a
        // result file".getBytes());
        when(this.service.validate(any(String.class))).thenReturn(Response.ok(false).build());

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider).asBytes(eq("proxy-calling.xml"));
        verify(this.specProvider).asBytes(eq("/step01/data.xml"));
        assertTrue(actual);
    }

    /**
     * Test validate conditions dont regenerate.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testValidateConditionsDontRegenerate() throws Exception {
        // prepare
        this.setUpRestServer();
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        when(this.specProvider.asBytes(eq("proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling.xml"));
        when(this.specProvider.asBytes(eq("/step01/data.xml"))).thenReturn(this.resourceAsBytes("data/step01/data.xml"));
        // when(this.service.validate(any(String.class))).thenReturn(Response.ok(true).build());

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider).asBytes(eq("proxy-calling.xml"));
        verify(this.specProvider).asBytes(eq("/step01/data.xml"));
        assertTrue(actual);
    }

    /**
     * Test execute.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testExecute() {
        // prepare
        final EngineContext<ProcessElement> ctx = this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess());
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(ctx);
        final Map<String, Object> model = new HashMap<>();
        final FormSpecificationData formSpecificationData = new FormSpecificationData("testExecute");
        when(this.bridge.stamp(any(Map.class))).thenReturn(formSpecificationData);
        when(this.bridge.getName()).thenCallRealMethod();

        // call
        final Object actual = this.processor.execute(this.processor.getScript(), ctx.buildExpressionContext(model));

        // check
        verify(this.bridge).stamp(any(Map.class));
        verify(this.bridge, times(2)).getName();
        assertThat(actual, notNullValue());
        assertThat(actual, equalTo(formSpecificationData));
    }

    /**
     * Test after execute.
     */
    @Test
    public void testAfterExecute() {
        // prepare
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        final FormSpecificationData formSpecificationData = new FormSpecificationDataBuilder("testAfterExecute").group( //
                new GroupElementBuilder("params").datas( //
                        new DataElementBuilder("urlRedirection").build() //
                ).build()).build();
        when(this.specProvider.asBytes(eq("/step01/data.xml"))).thenReturn(this.resourceAsBytes("data/step01/data.xml"));

        // call
        final Object actual = this.processor.afterExecute(formSpecificationData);

        // check
        verify(this.specProvider).asBytes(eq("/step01/data.xml"));
        verify(this.specProvider).save(eq("proxy-calling.xml"), any(FormSpecificationData.class));
        assertThat(actual, notNullValue());
        assertTrue(actual instanceof FormSpecificationData);
        final FormSpecificationData actualFsd = (FormSpecificationData) actual;
        assertThat(actualFsd.getGroups(), notNullValue());
        assertThat(actualFsd.getGroups().get(0), notNullValue());
        assertThat(actualFsd.getGroups().get(0).getData(), notNullValue());
        assertThat(actualFsd.getGroups().get(0).getData().get(1), notNullValue());
        assertThat(actualFsd.getGroups().get(0).getData().get(1).getId(), equalTo("hash"));
        assertThat(actualFsd.getGroups().get(0).getData().get(1).getValue().toString(), //
                equalTo("\"5D65A5258CB15A290A7C2F2BBD1A261E4EE3FB4373BD1056D8B6C8173C503E97\""));
    }

    /**
     * Test bind.
     */
    @Test
    public void testBind() {
        // prepare
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        final FormSpecificationData formSpecificationData = new FormSpecificationDataBuilder("testAfterExecute").group( //
                new GroupElementBuilder("params").datas( //
                        new DataElementBuilder("urlRedirection").type("String") //
                                .value("http://localhost:8888/test/v1/proxy/redirection?token=xxx").build() //
                ).build()).build();

        // call
        final Object actual = this.processor.bind(formSpecificationData);

        // check
        assertThat(actual, notNullValue());
        assertTrue(actual instanceof RedirectProcessResult);
        final RedirectProcessResult actualRpr = (RedirectProcessResult) actual;
        assertThat(actualRpr.isInterrupting(), equalTo(true));
        assertThat(actualRpr.getContent().getUrl(), equalTo("http://localhost:8888/test/v1/proxy/redirection?token=xxx"));
    }

    /**
     * Test validate conditions with result file generated.
     *
     * @throws Exception
     */
    @Test
    public void testValidateConditionsWithResultFile() throws Exception {
        // prepare
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        when(this.specProvider.asBytes(eq("proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling.xml"));
        when(this.specProvider.asBytes(eq("resultFileName.xml"))).thenReturn(this.resourceAsBytes("data/step02/resultFileName.xml"));

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider).asBytes(eq("proxy-calling.xml"));
        assertFalse(actual);
    }

    /**
     * Test validate conditions with valid proxy calling file and missing result
     * file.
     *
     * @throws Exception
     */
    @Test
    public void testValidateConditionsWithValidProxyCallingFile() throws Exception {
        // prepare
        this.setUpRestServer();
        // prepare
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        when(this.specProvider.asBytes(eq("proxy-calling.xml"))).thenReturn(this.resourceAsBytes("data/step02/proxy-calling.xml"));
        when(this.specProvider.asBytes(eq("/step01/data.xml"))).thenReturn(this.resourceAsBytes("data/step01/data.xml"));
        when(this.service.validate(any(String.class))).thenReturn(Response.ok(false).build());

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider).asBytes(eq("proxy-calling.xml"));
        verify(this.specProvider).asBytes(eq("/step01/data.xml"));
        assertTrue(actual);
    }

    /**
     * The Interface ITestService.
     */
    public static interface ITestService {

        /**
         * Validate.
         *
         * @param token
         *            the token
         * @return the response
         */
        @GET
        @Path("/v1/proxy/validate")
        Response validate(@QueryParam("token") final String token);

    }

    /**
     * Test validate conditions with result file generated.
     *
     * @throws Exception
     */
    @Test
    public void testHangoutWithExpiredSession() throws Exception {
        // prepare
        this.setUpRestServer();
        // prepare
        this.processor.init();
        this.processor.setBridge(this.bridge);
        this.processor.setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.specProvider).buildEngineContext().target(this.buildProcess()));
        when(this.specProvider.asBytes(eq("proxy-calling.xml"))).thenReturn(this.resourceAsBytes("expires/step02/proxy-calling.xml"));
        when(this.specProvider.asBytes(eq("/step01/data.xml"))).thenReturn(this.resourceAsBytes("expires/step01/data.xml"));
        when(this.service.validate(any(String.class))).thenReturn(Response.ok(false).build());

        // call
        final boolean actual = this.processor.validateConditions();

        // check
        verify(this.specProvider).asBytes(eq("proxy-calling.xml"));
        verify(this.specProvider).asBytes(eq("/step01/data.xml"));
        verify(this.specProvider).remove(eq("proxy-calling.xml"));
        assertTrue(actual);
    }

}
