/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor.bridge.document;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.processor.ScriptProcessor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.test.builder.ProcessElementBuilder;
import fr.ge.common.nash.test.AbstractBeanResourceTest;

/**
 * The Class ProxyInProcessorTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class PdfDocumentFillOverflowTest extends AbstractBeanResourceTest {

    /** provider. */
    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.asBytes("masque.pdf")).thenReturn(this.resourceAsBytes("masque.pdf"));
        when(this.provider.asBytes("annexe.pdf")).thenReturn(this.resourceAsBytes("annexe.pdf"));
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
        when(this.provider.asBytes("description.xml")).thenReturn(this.resourceAsBytes("description.xml"));
        when(this.provider.getAbsolutePath("data-generated.xml")).thenReturn("data-generated.xml");
    }

    /**
     * Process.
     *
     * @param scriptFilename
     *            script filename
     * @return form specification data
     */
    private FormSpecificationData process(final String scriptFilename) {
        final ProcessElement processElement = new ProcessElementBuilder() //
                .id("review") //
                .type("script") //
                .output("data-generated.xml") //
                .value(this.resourceAsString(scriptFilename)) //
                .build();

        final Map<String, Object> model = new HashMap<>();
        final ScriptProcessor processor = new ScriptProcessor() //
                .setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.provider).buildEngineContext().target(processElement));

        return processor.execute(model).getContent();
    }

    /**
     * Test fill a document with overflow.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFillDocumentWithOveflow() throws Exception {
        final FormSpecificationData actual = this.process("fill-document-with-overflow.js");

        assertThat(actual, //
                allOf( //
                        hasProperty("description", nullValue()), //
                        hasProperty("help", nullValue()), //
                        hasProperty("default", nullValue()), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );
        assertThat(actual, notNullValue());

        assertThat(actual.getGroups().get(0), //
                allOf( //
                        hasProperty("id", equalTo("generated")), //
                        hasProperty("data", hasSize(1)) //
                ) //
        );

        assertThat(actual.getGroups().get(0).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("modele_sanitaire")), //
                        hasProperty("label", equalTo("Prévisualisation du modèle de certificat avec ajout des exigences"))) //
        );

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.provider).save(eq("generated/generated.modele_sanitaire-1-modele_sanitaire.pdf"), eq("application/pdf"), captor.capture());

        // -->Checking the merged file
        final byte[] modelBytes = captor.getValue();
        assertThat(modelBytes, notNullValue());

        // -->Reading it and checking the number of pages and some fields
        final PdfReader modelReader = new PdfReader(modelBytes);
        assertThat(modelReader.getNumberOfPages(), equalTo(2));

        final PdfStamper modelStamper = new PdfStamper(modelReader, new ByteArrayOutputStream());
        assertThat(modelStamper.getAcroFields().getField("adresse_expediteur"), nullValue());
        assertThat(modelStamper.getAcroFields().getField("exigences"), nullValue());
        assertThat(modelStamper.getAcroFields().getField("nom_expediteur"), notNullValue());
    }

    /**
     * Test fill a document without overflow.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFillDocumentWithoutOveflow() throws Exception {
        final FormSpecificationData actual = this.process("fill-document.js");

        assertThat(actual, //
                allOf( //
                        hasProperty("description", nullValue()), //
                        hasProperty("help", nullValue()), //
                        hasProperty("default", nullValue()), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );
        assertThat(actual, notNullValue());

        assertThat(actual.getGroups().get(0), //
                allOf( //
                        hasProperty("id", equalTo("generated")), //
                        hasProperty("data", hasSize(1)) //
                ) //
        );

        assertThat(actual.getGroups().get(0).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("modele_sanitaire")), //
                        hasProperty("label", equalTo("Prévisualisation du modèle de certificat avec ajout des exigences"))) //
        );

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.provider).save(eq("generated/generated.modele_sanitaire-1-modele_sanitaire.pdf"), eq("application/pdf"), captor.capture());

        // -->Checking the merged file
        final byte[] modelBytes = captor.getValue();
        assertThat(modelBytes, notNullValue());

        // -->Reading it and checking the number of pages and some fields
        final PdfReader modelReader = new PdfReader(modelBytes);
        assertThat(modelReader.getNumberOfPages(), equalTo(1));

        final PdfStamper modelStamper = new PdfStamper(modelReader, new ByteArrayOutputStream());
        assertThat(modelStamper.getAcroFields().getField("adresse_expediteur"), nullValue());
        assertThat(modelStamper.getAcroFields().getField("exigences"), nullValue());
    }

    /**
     * Test fill a document without overflow using phto mask.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFillDocumentWithoutOveflowForPhyto() throws Exception {
        when(this.provider.asBytes("masque.pdf")).thenReturn(this.resourceAsBytes("masque-phyto.pdf"));
        final FormSpecificationData actual = this.process("fill-document-phyto.js");

        assertThat(actual, //
                allOf( //
                        hasProperty("description", nullValue()), //
                        hasProperty("help", nullValue()), //
                        hasProperty("default", nullValue()), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );
        assertThat(actual, notNullValue());

        assertThat(actual.getGroups().get(0), //
                allOf( //
                        hasProperty("id", equalTo("generated")), //
                        hasProperty("data", hasSize(1)) //
                ) //
        );

        assertThat(actual.getGroups().get(0).getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("modele_phyto")), //
                        hasProperty("label", equalTo("Prévisualisation du modèle de certificat avec ajout des exigences"))) //
        );

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.provider).save(eq("generated/generated.modele_phyto-1-modele_phyto.pdf"), eq("application/pdf"), captor.capture());

        // -->Checking the merged file
        final byte[] modelBytes = captor.getValue();
        assertThat(modelBytes, notNullValue());

        // -->Reading it and checking the number of pages and some fields
        final PdfReader modelReader = new PdfReader(modelBytes);
        assertThat(modelReader.getNumberOfPages(), equalTo(1));

        final PdfStamper modelStamper = new PdfStamper(modelReader, new ByteArrayOutputStream());
        assertThat(modelStamper.getAcroFields().getField("declaration_supp"), nullValue());
    }
}
