/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.manager.processor.IProcessResult;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.test.AbstractBeanResourceTest;
import fr.ge.common.utils.ResourceUtil;

/**
 * Class ProcessModelManagerTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class ProcessModelManagerTest extends AbstractBeanResourceTest {

    /** provider. */
    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private Engine engine;

    /**
     * Set up.
     *
     * @throws Exception
     *     exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.getAbsolutePath(any(String.class))).thenCallRealMethod();
        when(this.provider.asBytes("types.xml")).thenReturn(this.resourceAsBytes("spec/types.xml"));
        when(this.provider.asBytes("description.xml")).thenReturn(this.resourceAsBytes("description.xml"));
        // when(this.provider.asBytes("send.js")).thenReturn(this.resourceAsBytes("spec/send.js"));
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
    }

    /**
     * Test simple.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testSimple() throws Exception {
        final FormSpecificationProcess preprocess = this.resourceAsBean("spec/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("spec/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));

        this.assertData("data-generated.xml", "preprocess", "John Doe", null);
    }

    @Test
    public void testSuccessive() throws Exception {
        this.engine.setEngineProperties(new Properties());

        final IProvider provider = new ZipProvider(this.resourceAsBytes("successive.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);

        assertThat(provider.asBean("post-data-01.xml", FormSpecificationData.class), nullValue());
        assertThat(provider.asBean("post-data-02.xml", FormSpecificationData.class), nullValue());
        assertThat(provider.asBean("post-data-03.xml", FormSpecificationData.class), nullValue());

        final IProcessResult<?> result = loader.processes().postExecute(loader.stepsMgr().current());

        assertThat(result, notNullValue());

        final FormSpecificationData p1 = provider.asBean("post-data-01.xml", FormSpecificationData.class);
        assertThat(p1, notNullValue());

        final FormSpecificationData p2 = provider.asBean("post-data-02.xml", FormSpecificationData.class);
        assertThat(p2, notNullValue());

        final FormSpecificationData p3 = provider.asBean("post-data-03.xml", FormSpecificationData.class);
        assertThat(p3, notNullValue());
    }

    private void assertData(final String resourceName, final String id, final String label, final String value) {
        final FormSpecificationData actual = this.retrieveSavedFile(resourceName);
        this.assertData(actual, id, label, value);
    }

    /**
     * Tests the "if" condition.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testIfAttributeAndTagTrue() throws Exception {
        final FormSpecificationProcess preprocess = this.resourceAsBean("specAttributeAndTagTrue/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("specAttributeAndTagTrue/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));

        this.assertData("data-generated.xml", "preprocess", "John Doe", null);
    }

    /**
     * Tests the "if" condition.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testIfAttributeTrue() throws Exception {
        final FormSpecificationProcess preprocess = this.resourceAsBean("specAttributeTrue/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("specAttributeTrue/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));
        this.assertData("data-generated.xml", "preprocess", "John Doe", null);
    }

    /**
     * Tests the "if" condition.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testIfAttributeFalse() throws Exception {
        final FormSpecificationProcess preprocess = this.resourceAsBean("specAttributeFalse/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("specAttributeFalse/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));
        verify(this.provider, never()).save(eq("data-generated.xml"), any(), any(byte[].class));
    }

    private FormSpecificationData retrieveSavedFile(final String resourceName) {
        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.provider).save(eq(resourceName), any(), captor.capture());
        return JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationData.class);
    }

    /**
     * Tests the "if" condition.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testIfTagTrue() throws Exception {
        final FormSpecificationProcess preprocess = this.resourceAsBean("specTagTrue/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("specTagTrue/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));
        this.assertData("data-generated.xml", "preprocess", "John Doe", null);
    }

    /**
     * Tests the "if" condition.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testIfTagTrueInput() throws Exception {
        when(this.provider.asBytes("input.xml")).thenReturn(this.resourceAsBytes("specTagTrueInput/input.xml"));

        final FormSpecificationProcess preprocess = this.resourceAsBean("specTagTrueInput/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("specTagTrueInput/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));
        this.assertData("data-generated.xml", "preprocess", "John Doe", null);
    }

    /**
     * Tests the "if" condition.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testIfTagFalse() throws Exception {
        final FormSpecificationProcess preprocess = this.resourceAsBean("specTagFalse/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("specTagFalse/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));
        verify(this.provider, never()).save(eq("data-generated.xml"), any(), any(byte[].class));
    }

    /**
     * Tests the "if" condition.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testIfTagFalseInput() throws Exception {
        when(this.provider.asBytes("input.xml")).thenReturn(this.resourceAsBytes("specTagFalseInput/input.xml"));

        final FormSpecificationProcess preprocess = this.resourceAsBean("specTagFalseInput/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("specTagFalseInput/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));
        verify(this.provider, never()).save(eq("data-generated.xml"), any(), any(byte[].class));
    }

    /**
     * Assert data.
     *
     * @param actual
     *     actual
     * @param id
     *     id
     * @param label
     *     label
     * @param value
     *     value
     */
    protected void assertData(final FormSpecificationData actual, final String id, final String label, final String value) {
        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo(id)), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        assertThat(actual.getGroups(), //
                contains( //
                        this.matcherGroup( //
                                "preprocess", //
                                "Pièces justificatives", //
                                this.matcherData("identityMain", "Test for \"" + label + "\"", "Test description with \"" + label + "\" as label.", "File", value) //
                        ) //
                ) //
        );
    }

    /**
     * Test no update.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testNoUpdate() throws Exception {
        when(this.provider.asBytes("data-generated.xml")).thenReturn(this.resourceAsBytes("spec/generated.xml"));

        final FormSpecificationProcess preprocess = this.resourceAsBean("spec/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("spec/data.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));
        this.assertData("data-generated.xml", "attachments", "John Doe", "1:file01.png, 2:file02.png");
    }

    /**
     * Test update.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testUpdate() throws Exception {
        when(this.provider.asBytes("data-generated.xml")).thenReturn(this.resourceAsBytes("spec/generated.xml"));

        final FormSpecificationProcess preprocess = this.resourceAsBean("spec/preprocess.xml", FormSpecificationProcess.class);
        final FormSpecificationData data = this.resourceAsBean("spec/data-updated.xml", FormSpecificationData.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(RecursiveDataModelExtractor.create(this.provider).extract(data));
        this.assertData("data-generated.xml", "attachments", "Jane Doe", "1:file01.png, 2:file02.png");
    }

    /**
     * Test no data element.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testNoDataElement() throws Exception {
        final FormSpecificationProcess preprocess = this.resourceAsBean("spec/preprocess-empty.xml", FormSpecificationProcess.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(preprocess).withProvider(this.provider));

        mgr.execute(null);
        assertThat(this.retrieveSavedFile("data-generated.xml"), hasProperty("id", equalTo("preprocess")));
    }

    /**
     * Test multiple process.
     *
     * @throws Exception
     *     exception
     */
    @Test
    public void testMultipleProcess() throws Exception {
        when(this.provider.asBytes("description.xml")).thenReturn(ResourceUtil.resourceAsBytes("description.xml", Engine.class));
        when(this.provider.asBytes("data-01.xml")).thenReturn(null, this.resourceAsBytes("spec/data-01.xml"));
        when(this.provider.asBytes("data-02.xml")).thenReturn(null, this.resourceAsBytes("spec/data-02.xml"));

        final FormSpecificationProcess processes = this.resourceAsBean("spec/process-multiple.xml", FormSpecificationProcess.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(processes).withProvider(this.provider));

        mgr.execute(null);

        final FormSpecificationData actual = this.retrieveSavedFile("data-03.xml");
        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("proc03")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        assertThat(actual.getGroups().get(0), //
                this.matcherGroup("grp03", "Group 03 Title", this.matcherData("fld03", "Field 03 Title", null, "String", "Value 01 -> 02 -> 03")) //
        );

        verify(this.provider).save(eq("data-01.xml"), eq("text/xml"), any());

        verify(this.provider, times(2)).asBytes("data-01.xml");
        verify(this.provider).save(eq("data-02.xml"), eq("text/xml"), any());

        verify(this.provider, times(2)).asBytes("data-01.xml");
        verify(this.provider).save(eq("data-03.xml"), eq("text/xml"), any());
    }

    @Test
    public void testMultipleInput() throws Exception {
        when(this.provider.asBytes("description.xml")).thenReturn(ResourceUtil.resourceAsBytes("description.xml", Engine.class));
        when(this.provider.asBytes("data-01.xml")).thenReturn(this.resourceAsBytes("spec/data-01.xml"));
        when(this.provider.asBytes("data-02.xml")).thenReturn(this.resourceAsBytes("spec/data-02.xml"));

        final FormSpecificationProcess processes = this.resourceAsBean("spec/process-multiple-input.xml", FormSpecificationProcess.class);

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(processes).withProvider(this.provider));

        mgr.execute(null);

        final FormSpecificationData actual = this.retrieveSavedFile("data-03.xml");
        assertThat(actual, //
                allOf( //
                        hasProperty("id", equalTo("proc01")), //
                        hasProperty("groups", hasSize(1)) //
                ) //
        );

        assertThat(actual.getGroups().get(0), //
                this.matcherGroup("grp01", "Group 01 Title", this.matcherData("fld01", "Field 01 Title", null, "String", "Value 01 -> Value 01 -> 02")) //
        );

        verify(this.provider).asBytes("data-01.xml");
        verify(this.provider).asBytes("data-02.xml");
        verify(this.provider).save(eq("data-03.xml"), eq("text/xml"), any());
    }

    @Test
    public void testPdfCreation() throws Exception {
        when(this.provider.asBytes("description.xml")).thenReturn(ResourceUtil.resourceAsBytes("description.xml", Engine.class));
        when(this.provider.asBytes("data-generated.xml")).thenReturn(null);
        when(this.provider.asBytes("models/cerfa14004-02.pdf")).thenReturn(this.resourceAsBytes("spec/models/cerfa_14004-02.pdf"));

        final ProcessElement process = new ProcessElement();
        process.setId("single");
        process.setOutput("data-generated.xml");
        process.setType("script");
        process.setValue(this.resourceAsString("spec/pdf-creation.js"));

        final FormSpecificationProcess processes = new FormSpecificationProcess();
        processes.setId("pdfCreation");
        processes.setProcesses(Arrays.asList(process));

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(processes).withProvider(this.provider));

        mgr.execute(null);

        assertThat(this.retrieveSavedFile("data-generated.xml"), notNullValue());

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.provider).save(eq("generated/generated.cerfa14004-1-cerfa14004.pdf"), eq("application/pdf"), captor.capture());

        final PDDocument pdf = PDDocument.load(captor.getValue());

        assertThat(pdf.getDocumentCatalog().getAcroForm().getFields(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("fullyQualifiedName", equalTo("lastname")), //
                                        hasProperty("valueAsString", equalTo("Doe")) //
                                ), //
                                allOf( //
                                        hasProperty("fullyQualifiedName", equalTo("firstname")), //
                                        hasProperty("valueAsString", equalTo("John")) //
                                ) //
                        ) //
                ) //
        );
    }

    @Test
    public void testPdfCreationTemplateNotFound() throws Exception {
        when(this.provider.asBytes("description.xml")).thenReturn(ResourceUtil.resourceAsBytes("description.xml", Engine.class));
        when(this.provider.asBytes("data-generated.xml")).thenReturn(null);
        when(this.provider.asBytes("models/cerfa14004-02.pdf")).thenReturn(new byte[] {});

        final ProcessElement process = new ProcessElement();
        process.setId("single");
        process.setOutput("data-generated.xml");
        process.setType("script");
        process.setValue(this.resourceAsString("spec/pdf-creation.js"));

        final FormSpecificationProcess processes = new FormSpecificationProcess();
        processes.setId("pdfCreation");
        processes.setProcesses(Arrays.asList(process));

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(processes).withProvider(this.provider));

        try {
            mgr.execute(null);
            fail("ScriptException expected");
        } catch (final Exception ex) {
            assertThat(ex, //
                    hasProperty("message", //
                            equalTo("Loading document : resource [models/cerfa14004-02.pdf] not found") //
                    ) //
            );
        }
    }

    @Test
    public void testPdfCreationNew() throws Exception {
        when(this.provider.asBytes("description.xml")).thenReturn(ResourceUtil.resourceAsBytes("description.xml", Engine.class));
        when(this.provider.asBytes("data-pdf-creation.xml")).thenReturn(this.resourceAsBytes("spec/data-pdf-creation.xml"));
        when(this.provider.asBytes("data-generated.xml")).thenReturn(null);
        when(this.provider.asBytes("models/cerfa14004-02.pdf")).thenReturn(this.resourceAsBytes("spec/models/cerfa_14004-02.pdf"));
        when(this.provider.asBytes("uploaded/grp01.fld01-1-pdf-attachment.png")).thenReturn(this.resourceAsBytes("spec/uploaded/grp01.fld01-1-pdf-attachment.png"));

        final ProcessElement process = new ProcessElement();
        process.setId("single");
        process.setInput("data-pdf-creation.xml");
        process.setOutput("data-generated.xml");
        process.setType("script");
        process.setValue(this.resourceAsString("spec/pdf-creation-new.js"));

        final FormSpecificationProcess processes = new FormSpecificationProcess();
        processes.setId("pdfCreation");
        processes.setProcesses(Arrays.asList(process));

        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(processes).withProvider(this.provider));

        mgr.execute(null);

        assertThat(this.retrieveSavedFile("data-generated.xml"), notNullValue());

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.provider).save(eq("generated/generated.cerfa14004-1-cerfa14004.pdf"), eq("application/pdf"), captor.capture());

        final PDDocument pdf = PDDocument.load(captor.getValue());

        assertThat(pdf.getDocumentCatalog().getAcroForm().getFields(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("fullyQualifiedName", equalTo("lastname")), //
                                        hasProperty("valueAsString", equalTo("Doe [2017-05-REC-AAA-42]")) //
                                ), //
                                allOf( //
                                        hasProperty("fullyQualifiedName", equalTo("firstname")), //
                                        hasProperty("valueAsString", equalTo("John")) //
                                ) //
                        ) //
                ) //
        );
    }

    @Test
    @Ignore
    public void testRestServices() throws Exception {
        when(this.provider.asBytes("generated-meta.xml")).thenReturn(this.resourceAsBytes("spec/send-meta.xml"));
        when(this.provider.asBytes("/3-review/generated/generated.formulaire-1-PI Inspect - PI 2.pdf")).thenReturn(this.resourceAsBytes("spec/send-review.pdf"));

        final FormSpecificationProcess processes = this.resourceAsBean("spec/send-process.xml", FormSpecificationProcess.class);

        final Properties properties = new Properties();
        properties.setProperty("exchange.baseUrl", "http://exchange-server.int.guichet-partenaires.loc:7081/api");
        properties.setProperty("exchange.email.sender", "batman@gotham.org");

        this.engine.setEngineProperties(properties);
        final ProcessModelManager mgr = new ProcessModelManager(this.engine.loader(this.provider).buildEngineContext().target(processes).withProvider(this.provider));

        // final ByteDataSource source = new
        // ByteDataSource(this.resourceAsBytes("spec/send-meta.xml"));
        // source.setContentType("application/pdf");
        // source.setName("attachment.pdf");
        //
        // final Attachment att = new
        // AttachmentBuilder().id("file").mediaType("application/pdf").object(this.resourceAsBytes("spec/send-meta.xml")).build();
        // final Response response =
        // javax.ws.rs.client.ClientBuilder.newClient() //
        // .property("http.connection.timeout", 500) //
        // .property("http.receive.timeout", 1000) //
        // .target("http://exchange-server.int.guichet-partenaires.loc:7081/api/email/send")
        // //
        // .queryParam("sender", "batman@gotham.org") //
        // .queryParam("recipient", "test-scn@yopmail.com") //
        // .queryParam("object", "Dossier soumis") //
        // .queryParam("content", "test") //
        // .queryParam("attachmentPDFName", "test.pdf") //
        // .request().post(Entity.entity(new MultipartBody(att),
        // MediaType.MULTIPART_FORM_DATA));
        //
        // assertThat(new String(response.readEntity(byte[].class)),
        // equalTo(""));
        // assertThat(response.getStatus(), equalTo(200));

        // new AttachmentBuilder().id("file");
        // final MultipartBody data = new MultipartBody(new
        // Attachment("attachment", new DataHandler(source), null));
        // ClientBuilder.newClient().target("").request().post(data);
        // final FormSpecificationData actual =
        mgr.execute(null);
    }

    @Test
    public void testExecutionOfProcessWithoutStatus() throws Exception {

        final IProvider provider = new ZipProvider(this.resourceAsBytes("processWithoutStatus.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);

        loader.executeActiveStep(null);

        final FormSpecificationData specData = loader.data(loader.lastStepDone());
        final GroupElement groupElement = (GroupElement) specData.getGroups().get(0).getData().get(0);
        final DataElement dataElement = (DataElement) groupElement.getData().get(0);
        assertEquals("\"executed\"", dataElement.getValue().toString());

    }

    @Test
    public void testExecutionOfProcessWithDoneStatus() throws Exception {

        final IProvider provider = new ZipProvider(this.resourceAsBytes("processWithDoneStatus.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);

        loader.executeActiveStep(null);

        final FormSpecificationData specData = loader.data(loader.lastStepDone());
        final GroupElement groupElement = (GroupElement) specData.getGroups().get(0).getData().get(0);
        final DataElement dataElement = (DataElement) groupElement.getData().get(0);
        assertEquals("\"executed\"", dataElement.getValue().toString());

    }

    @Test
    public void testExecutionOfProcessWithTodoStatus() throws Exception {

        final IProvider provider = new ZipProvider(this.resourceAsBytes("processWithTODOStatus.zip"));
        final SpecificationLoader loader = this.engine.loader(provider);

        loader.executeActiveStep(null);

        final FormSpecificationData specData = loader.data(loader.lastStepDone());
        final GroupElement groupElement = (GroupElement) specData.getGroups().get(0).getData().get(0);
        final DataElement dataElement = (DataElement) groupElement.getData().get(0);
        assertEquals("\"executed\"", dataElement.getValue().toString());

    }

    /**
     * Matcher group.
     *
     * @param id
     *     id
     * @param label
     *     label
     * @param dataMatchers
     *     data matchers
     * @return the matcher<? super group element>
     */
    @SafeVarargs
    private final Matcher<? super GroupElement> matcherGroup(final String id, final String label, final Matcher<? super DataElement>... dataMatchers) {
        final List<Matcher<? super GroupElement>> matchers = new ArrayList<>();

        matchers.add(hasProperty("id", equalTo(id)));
        matchers.add(hasProperty("label", equalTo(label)));

        if (ArrayUtils.isEmpty(dataMatchers)) {
            matchers.add(hasProperty("data", hasSize(0)));
        } else {
            matchers.add(hasProperty("data", hasSize(dataMatchers.length)));
            if (dataMatchers.length == 1) {
                matchers.add(hasProperty("data", hasItem(dataMatchers[0])));
            } else {
                matchers.add(hasProperty("data", containsInAnyOrder(Arrays.asList(dataMatchers))));
            }
        }

        return allOf(matchers);
    }

    /**
     * Matcher data.
     *
     * @param id
     *     id
     * @param label
     *     label
     * @param description
     *     description
     * @param type
     *     type
     * @param value
     *     value
     * @return the matcher<? super data element>
     */
    private Matcher<? super DataElement> matcherData(final String id, final String label, final String description, final String type, final String value) {
        final List<Matcher<? super DataElement>> matchers = new ArrayList<>();

        matchers.add(hasProperty("id", equalTo(id)));
        matchers.add(hasProperty("label", equalTo(label)));
        matchers.add(hasProperty("description", null == description ? nullValue() : equalTo(description)));
        matchers.add(hasProperty("type", equalTo(type)));

        if (null == value) {
            matchers.add(hasProperty("value", nullValue()));
        } else {
            matchers.add(hasProperty("value", hasProperty("content", equalTo(value))));
        }

        return allOf(matchers);
    }

}
