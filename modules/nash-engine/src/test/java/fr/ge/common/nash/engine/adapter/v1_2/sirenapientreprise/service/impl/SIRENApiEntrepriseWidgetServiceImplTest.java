/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.engine.adapter.v1_2.sirenapientreprise.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.adapter.AbstractRestTest;
import fr.ge.common.nash.engine.adapter.v1_2.sirenapientreprise.service.ISIRENApiEntrepriseExternalService;

/**
 * Tests {@link SIRENApiEntrepriseWidgetServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/data-sirenapientreprise-context.xml", "/spring/test-context.xml" })
public class SIRENApiEntrepriseWidgetServiceImplTest extends AbstractRestTest {

    /** External service. */
    @Autowired
    private ISIRENApiEntrepriseExternalService externalService;

    /**
     * Setup.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Before
    public void setup() throws Exception {
        Mockito.reset(this.externalService);
    }

    /**
     * Tests {@link SIRENApiEntrepriseWidgetServiceImpl#getBySiren(String, String)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testGetBySiren() throws Exception {
        // prepare
        when(this.externalService.getBySiren(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class))) //
                .thenReturn("info");

        // call
        final Response response = this.client().accept(MediaType.APPLICATION_JSON) //
                .path("/123456789") //
                .query("formTitle", "Eleveur de poulet").get();

        // verify
        verify(this.externalService).getBySiren(eq("123456789"), eq("t0k3n"), eq("Tiers"), eq("SIR3T"), eq("Eleveur de poulet"));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(response.readEntity(String.class), equalTo("info"));
    }

}
