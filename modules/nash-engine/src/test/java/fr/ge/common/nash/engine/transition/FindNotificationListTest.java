/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.transition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;

/**
 *
 * @author ijijon
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class FindNotificationListTest extends AbstractTransitionTest {

    protected IService service;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public Object getService() {
        return this.service = mock(IService.class);
    }

    @Test
    public void testWithMailinglist() throws Exception {

        final String AUTHORITY_ID = "AUTHORITY_ID";
        final String AUTHORITY_LABEL = "AUTHORITY_LABEL";

        // Prepare authority mock :
        final HashMap<String, Object> authority = new HashMap<String, Object>();
        authority.put("entityId", AUTHORITY_ID);
        authority.put("label", AUTHORITY_LABEL);

        final HashMap<String, Object> details = new HashMap<String, Object>();
        final ArrayList<String> mails = new ArrayList<String>();
        mails.add("testMail1@yopmail.com");
        mails.add("testMail2@yopmail.com");
        details.put("notifications", mails);
        authority.put("details", details);

        // Mock service:
        when(this.service.getByEntityId(any())).thenReturn(authority);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        final FormSpecificationData specData = loader.data(loader.lastStepDone());
        final GroupElement groupElement = (GroupElement) specData.getGroups().get(0).getData().get(1);
        final DataElement mail1 = (DataElement) groupElement.getData().get(0);
        final DataElement mail2 = (DataElement) groupElement.getData().get(1);
        assertEquals("\"testMail1@yopmail.com\"", mail1.getValue().toString());
        assertEquals("\"testMail2@yopmail.com\"", mail2.getValue().toString());

        try (OutputStream out = new FileOutputStream(Paths.get("target/resource.zip").toFile())) {
            IOUtils.write(provider.asBytes(), out);
        }
    }

    @Test
    public void testWithoutMailinglist() throws Exception {

        final String AUTHORITY_ID = "AUTHORITY_ID";
        final String AUTHORITY_LABEL = "AUTHORITY_LABEL";

        // Prepare authority mock :
        final HashMap<String, Object> authority = new HashMap<String, Object>();
        authority.put("entityId", AUTHORITY_ID);
        authority.put("label", AUTHORITY_LABEL);

        final HashMap<String, Object> details = new HashMap<String, Object>();
        authority.put("details", details);

        // Mock service:
        when(this.service.getByEntityId(any())).thenReturn(authority);

        final Properties properties = new Properties();
        properties.put("directory.baseUrl", ENDPOINT);
        final Configuration configuration = new Configuration(properties);

        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final SpecificationLoader loader = this.applicationContext.getBean(SpecificationLoader.class, configuration, provider);

        this.execute(loader);
        this.execute(loader);

        final FormSpecificationData specData = loader.data(loader.lastStepDone());
        final GroupElement groupElement = (GroupElement) specData.getGroups().get(0).getData().get(1);
        final DataElement mailinglist = (DataElement) groupElement.getData().get(0);
        assertNull(mailinglist.getValue());

        try (OutputStream out = new FileOutputStream(Paths.get("target/resource.zip").toFile())) {
            IOUtils.write(provider.asBytes(), out);
        }
    }

    public static interface IService {

        @GET
        @Path("/v1/authority/{entityId}")
        @Produces(MediaType.APPLICATION_JSON)
        HashMap<String, Object> getByEntityId(@PathParam("entityId") String entityId);

    }

}
