/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.processor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class PdfProcessorTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class PdfProcessorTest extends AbstractTest {

    /** provider. */
    private final IProvider provider = mock(IProvider.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.provider);
        when(this.provider.asBean(any(String.class), any())).thenCallRealMethod();
    }

    /**
     * Test simple.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSimple() throws Exception {
        when(this.provider.asBytes("description.xml")).thenReturn(ResourceUtil.resourceAsBytes("description.xml", Engine.class));
        when(this.provider.asBytes("data-generated.xml")).thenReturn(this.resourceAsBytes("data-generated.xml"));
        when(this.provider.asBytes("uploaded/preprocess.identityMain-1-file01.png")).thenReturn(this.resourceAsBytes("file01.png"));
        when(this.provider.asBytes("uploaded/preprocess.identityMain-2-file02.png")).thenReturn(this.resourceAsBytes("file02.png"));
        when(this.provider.asBytes("uploaded/preprocess.identityMain-3-file03.jpg")).thenReturn(this.resourceAsBytes("file03.jpg"));
        when(this.provider.asBytes("uploaded/preprocess.identityMain-4-file04.pdf")).thenReturn(this.resourceAsBytes("file04.pdf"));
        when(this.provider.getAbsolutePath("data.xml")).thenReturn("data.xml");

        final ProcessElement processElement = new ProcessElement();
        processElement.setId("postprocess");
        processElement.setInput("data-generated.xml");
        processElement.setOutput("data.xml");
        processElement.setType("pdf");

        final PdfProcessor processor = new PdfProcessor() //
                .setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.provider).buildEngineContext().target(processElement));

        final FormSpecificationData actual = processor.execute((Map<String, Object>) null).getContent();

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.provider).save(any(String.class), eq("application/pdf"), captor.capture());
        // Files.write(Paths.get("test.pdf"), captor.getValue());

        assertEquals(this.resourceAsString("data.xml"), new String(JaxbFactoryImpl.instance().asByteArray(actual, "../css/data.css"), StandardCharsets.UTF_8));
    }

    /**
     * Test simple.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testMultipleFiles() throws Exception {
        when(this.provider.asBytes("description.xml")).thenReturn(ResourceUtil.resourceAsBytes("description.xml", Engine.class));
        when(this.provider.asBytes("data-generated.xml")).thenReturn(this.resourceAsBytes("data-generated-multiple.xml"));
        when(this.provider.asBytes("uploaded/preprocess.identity1-1-file01.png")).thenReturn(this.resourceAsBytes("file01.png"));
        when(this.provider.asBytes("uploaded/preprocess.identity2-1-file01.png")).thenReturn(this.resourceAsBytes("file01.png"));
        when(this.provider.getAbsolutePath("data.xml")).thenReturn("data.xml");

        final ProcessElement processElement = new ProcessElement();
        processElement.setId("postprocess");
        processElement.setInput("data-generated.xml");
        processElement.setOutput("data.xml");
        processElement.setType("pdf");

        final PdfProcessor processor = new PdfProcessor() //
                .setContext(this.applicationContext.getBean(SpecificationLoader.class, null, this.provider).buildEngineContext().target(processElement));

        final FormSpecificationData actual = processor.execute((Map<String, Object>) null).getContent();

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.provider, times(2)).save(any(String.class), eq("application/pdf"), captor.capture());

        assertThat(actual.getGroups().get(0), hasProperty("data", hasSize(3)));
        assertThat(actual.getGroups().get(0).getData().get(0), allOf(hasProperty("id", equalTo("identity1")), hasProperty("value", notNullValue())));
        assertThat(actual.getGroups().get(0).getData().get(1), allOf(hasProperty("id", equalTo("identity2")), hasProperty("value", notNullValue())));
        assertThat(actual.getGroups().get(0).getData().get(2), allOf(hasProperty("id", equalTo("identity3")), hasProperty("value", nullValue())));
    }
}
