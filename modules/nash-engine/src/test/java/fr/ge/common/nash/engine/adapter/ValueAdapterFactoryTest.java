/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.adapter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.v1_0.BooleanValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.DateValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileReadOnlyValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.IntegerValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.StringValueAdapter;
import fr.ge.common.nash.engine.adapter.v42_1.FooBarValueAdapter;
import fr.ge.common.nash.engine.adapter.v42_1.StringOverride2ValueAdapter;
import fr.ge.common.nash.engine.adapter.v42_1.StringOverride3ValueAdapter;
import fr.ge.common.nash.engine.adapter.v42_1.StringOverrideValueAdapter;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.provider.TransitionFactory;
import fr.ge.common.nash.engine.test.builder.DataElementBuilder;
import fr.ge.common.nash.engine.test.builder.DefaultElementBuilder;
import fr.ge.common.nash.engine.test.builder.FormSpecificationDataBuilder;
import fr.ge.common.nash.engine.util.TypeDeclaration;
import fr.ge.common.nash.engine.util.ValueAdapterResourceTypeEnum;

/**
 * Tests {@link ValueAdapterFactory}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/engine-context.xml", "/spring/test-context.xml" })
public class ValueAdapterFactoryTest {

    /**
     * Test types per version.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testTypesPerVersion() throws Exception {

        // get 1.0 version types
        assertEquals(ValueAdapterFactory.type("1.0", "string").getClass(), StringValueAdapter.class);
        assertEquals(ValueAdapterFactory.type("1.0", "boolean").getClass(), BooleanValueAdapter.class);
        assertEquals(ValueAdapterFactory.type("1.0", "integer").getClass(), IntegerValueAdapter.class);
        assertEquals(ValueAdapterFactory.type("1.0", "date").getClass(), DateValueAdapter.class);
        assertEquals(ValueAdapterFactory.type("1.0", "file").getClass(), FileValueAdapter.class);
        assertEquals(ValueAdapterFactory.type("1.0", "filereadonly").getClass(), FileReadOnlyValueAdapter.class);
        assertThat(ValueAdapterFactory.type("1.0", "barbaz"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("1.0", "???"), is(nullValue()));

        // get 42.0 version types
        assertThat(ValueAdapterFactory.type("42.0", "string"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.0", "boolean"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.0", "integer"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.0", "date"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.0", "file"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.0", "filereadonly"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.0", "foobar"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.0", "???"), is(nullValue()));

        // get 42.1 version types
        assertEquals(ValueAdapterFactory.type("42.1", "string").getClass(), StringOverrideValueAdapter.class);
        assertThat(ValueAdapterFactory.type("42.1", "boolean"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.1", "integer"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.1", "date"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.1", "file"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.1", "filereadonly"), is(nullValue()));
        assertEquals(ValueAdapterFactory.type("42.1", "foobar").getClass(), FooBarValueAdapter.class);
        assertThat(ValueAdapterFactory.type("42.1", "???"), is(nullValue()));

        // get 42.2 version types
        assertEquals(ValueAdapterFactory.type("42.2", "string").getClass(), StringOverrideValueAdapter.class);
        assertThat(ValueAdapterFactory.type("42.2", "boolean"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.2", "integer"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.2", "date"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.2", "file"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.2", "filereadonly"), is(nullValue()));
        assertEquals(ValueAdapterFactory.type("42.2", "foobar").getClass(), FooBarValueAdapter.class);
        assertThat(ValueAdapterFactory.type("42.2", "???"), is(nullValue()));

        // get 42.3 version types
        assertThat(ValueAdapterFactory.type("42.3", "string"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.3", "boolean"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.3", "integer"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.3", "date"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.3", "file"), is(nullValue()));
        assertThat(ValueAdapterFactory.type("42.3", "filereadonly"), is(nullValue()));
        assertEquals(ValueAdapterFactory.type("42.3", "foobar").getClass(), FooBarValueAdapter.class);
        assertThat(ValueAdapterFactory.type("42.3", "???"), is(nullValue()));
    }

    /**
     * Test types per version latest.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testTypesPerVersionLatest() throws Exception {
        assertThat(ValueAdapterFactory.type("latest", "string"), is(notNullValue()));
    }

    /**
     * Test types per version case insensitive.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testTypesPerVersionCaseInsensitive() throws Exception {
        assertThat(ValueAdapterFactory.type("1.0", "string"), is(notNullValue()));
        assertThat(ValueAdapterFactory.type("1.0", "String"), is(notNullValue()));
        assertThat(ValueAdapterFactory.type("1.0", "stRing"), is(notNullValue()));

        System.out.println(TransitionFactory.loadAllTransitions());
    }

    /**
     * Test extract version.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testExtractVersion() throws Exception {
        assertEquals("1.0", ValueAdapterFactory.extractAdapterVersion(StringValueAdapter.class.getName()).toString());
        assertEquals("1.0.1", ValueAdapterFactory.extractAdapterVersion(StringValueAdapter.class.getName().replace("v1_0", "v1_0_1")).toString());
    }

    /**
     * Test adapter options.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testAdapterOptions() throws Exception {
        final IValueAdapter<?> actual = ValueAdapterFactory.type("1.2", "String(min: 3, max: 10)");

        assertThat( //
                actual, //
                allOf( //
                        is(instanceOf(fr.ge.common.nash.engine.adapter.v1_2.StringValueAdapter.class)), //
                        hasProperty("min", equalTo(3)), //
                        hasProperty("max", equalTo(10)) //
                ) //
        );
    }

    /**
     * Test adapter options unknown.
     *
     * @throws Exception
     *             exception
     */
    @Test(expected = TechnicalException.class)
    public void testAdapterOptionsUnknown() throws Exception {
        ValueAdapterFactory.type("1.2", "String(min: 3, max: 10, nullable: true)");
    }

    /**
     * Test adapter options not all.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testAdapterOptionsNotAll() throws Exception {
        final IValueAdapter<?> actual = ValueAdapterFactory.type("1.2", "String(max: 20)");

        assertThat( //
                actual, //
                allOf( //
                        is(instanceOf(fr.ge.common.nash.engine.adapter.v1_2.StringValueAdapter.class)), //
                        hasProperty("min", equalTo(0)), //
                        hasProperty("max", equalTo(20)) //
                ) //
        );
    }

    /**
     * Test parsed type.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testParsedType() throws Exception {
        final TypeDeclaration actual = TypeDeclaration.fromString("String");

        assertThat(actual, allOf(hasProperty("name", equalTo("String")), hasProperty("optionString", nullValue())));
    }

    /**
     * Test parsed type with options.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testParsedTypeWithOptions() throws Exception {
        final TypeDeclaration actual = TypeDeclaration.fromString("String(min: 3, max: 10)");

        assertThat(actual, allOf(hasProperty("name", equalTo("String")), hasProperty("optionString", equalTo("min: 3, max: 10"))));
    }

    /**
     * Test parsed type unparsable.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testParsedTypeUnparsable() throws Exception {
        final TypeDeclaration actual = TypeDeclaration.fromString("(min: 3, max: 10)String");

        assertThat(actual, allOf(hasProperty("name", equalTo("(min: 3, max: 10)String")), hasProperty("optionString", nullValue())));
    }

    /**
     * Test parsed type null.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testParsedTypeNull() throws Exception {
        final TypeDeclaration actual = TypeDeclaration.fromString(null);

        assertThat(actual, nullValue());
    }

    /**
     * Test value adapter info.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testValueAdapterInfo() throws Exception {
        final IValueAdapter<?> valueAdapter = ValueAdapterFactory.type("42.1", "String");
        final ValueAdapterInfo actual = ValueAdapterInfo.fromObject(valueAdapter);

        assertThat(actual, //
                allOf( //
                        hasProperty("name", equalTo("String")), //
                        hasProperty("description", nullValue()) //
                ) //
        );

        assertThat(actual.getOptions(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("name", equalTo("min")), //
                                        hasProperty("type", equalTo(int.class)), //
                                        hasProperty("description", equalTo("Value minimum size")) //
                                ), //
                                allOf( //
                                        hasProperty("name", equalTo("max")), //
                                        hasProperty("type", equalTo(int.class)), //
                                        hasProperty("description", equalTo("Value maximum size")) //
                                ), //
                                allOf( //
                                        hasProperty("name", equalTo("pattern")), //
                                        hasProperty("type", equalTo(String.class)), //
                                        hasProperty("description", equalTo("Regular expression")) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Tests
     * {@link ValueAdapterFactory#getValueAdapterResource(ValueAdapterResourceTypeEnum, Class)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetValueAdapterResource() throws Exception {
        final byte[] resource = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.PREVIEW, StringOverrideValueAdapter.class);
        assertThat(resource, notNullValue());
        assertEquals("StringOverride preview", new String(resource, StandardCharsets.UTF_8));
    }

    /**
     * Tests
     * {@link ValueAdapterFactory#getValueAdapterResource(ValueAdapterResourceTypeEnum, Class)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetValueAdapterResourceDontInherit() throws Exception {
        final byte[] resource = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.PREVIEW, StringOverride2ValueAdapter.class);
        assertThat(resource, notNullValue());
        assertEquals("StringOverride2 preview", new String(resource, StandardCharsets.UTF_8));
    }

    /**
     * Tests
     * {@link ValueAdapterFactory#getValueAdapterResource(ValueAdapterResourceTypeEnum, Class)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetValueAdapterResourceInherit() throws Exception {
        final byte[] resource = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.PREVIEW, StringOverride3ValueAdapter.class);
        assertThat(resource, notNullValue());
        assertEquals("StringOverride2 preview", new String(resource, StandardCharsets.UTF_8));
    }

    /**
     * Tests
     * {@link ValueAdapterFactory#getValueAdapterResource(ValueAdapterResourceTypeEnum, Class)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetValueAdapterResourceNotFound() throws Exception {
        final byte[] resource = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.PREVIEW, FooBarValueAdapter.class);
        assertThat(resource, nullValue());
    }

    /**
     * Tests
     * {@link ValueAdapterFactory#getValueAdapterResource(ValueAdapterResourceTypeEnum, Class)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetValueAdapterMultipleResourceNoIndex() throws Exception {
        final byte[] resource = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.STYLE, StringOverrideValueAdapter.class);
        assertEquals(".red {\n" //
                + "\tbackground-color: red;\n" //
                + "}\n" //
                + ".orange {\n" //
                + "\tbackground-color: orange;\n" //
                + "}\n", new String(resource, StandardCharsets.UTF_8));
    }

    /**
     * Tests
     * {@link ValueAdapterFactory#getValueAdapterResource(ValueAdapterResourceTypeEnum, Class)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetValueAdapterMultipleResourceIndex1() throws Exception {
        final byte[] resource = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.STYLE, StringOverride2ValueAdapter.class);
        assertNotNull(resource);
        assertEquals(".red {\n" //
                + "\tbackground-color: red;\n" //
                + "}\n" //
                + ".orange {\n" //
                + "\tbackground-color: orange;\n" //
                + "}\n", new String(resource, StandardCharsets.UTF_8));
    }

    /**
     * Tests
     * {@link ValueAdapterFactory#getValueAdapterResource(ValueAdapterResourceTypeEnum, Class)}
     * .
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetValueAdapterMultipleResourcePrefixSuffix() throws Exception {
        final byte[] resource = ValueAdapterFactory.getValueAdapterResource(ValueAdapterResourceTypeEnum.SCRIPT, StringOverrideValueAdapter.class);
        assertEquals("$(function() {\n" //
                + "\tconsole.log(\"I am the first script !\");\n" //
                + "});\n" //
                + "$(function() {\n" //
                + "\tconsole.log(\"I am the second script !\");\n" //
                + "});\n", new String(resource, StandardCharsets.UTF_8));
    }

    /**
     * Test type from default.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testTypeFromDefault() throws Exception {
        final FormSpecificationData spec = new FormSpecificationDataBuilder("test").def( //
                new DefaultElementBuilder().data( //
                        new DataElementBuilder().type("Date").build() //
                ).build() //
        ).build();

        final IValueAdapter<?> actual = ValueAdapterFactory.type(spec, new DataElementBuilder().build());

        assertTrue(actual instanceof DateValueAdapter);
    }

}
