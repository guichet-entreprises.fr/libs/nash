/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.manager.parser;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.nash.engine.test.AbstractBeanResourceTest;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.exception.TechnicalException;

/**
 *
 * @author Christian Cougourdan
 */
public class TreeParserWithContextTest extends AbstractBeanResourceTest {

    @Test
    public void testSimple() throws Exception {
        final FormSpecificationData step = this.resourceAsBean("data.xml", FormSpecificationData.class);

        final BiConsumer<IElement<?>, ScriptExecutionContext> grpAction = (elm, ctx) -> {
            if (!(elm instanceof GroupElement)) {
                throw new TechnicalException("GroupElement object expected");
            } else {
                final Object grpModel = ctx.getModel().get(Scopes.GROUP.toContextKey());
                if (StringUtils.isEmpty(elm.getParentPath())) {
                    if (null != grpModel) {
                        throw new TechnicalException("Group model must be null");
                    }
                } else {
                    if (find(elm.getParentPath(), CoreUtil.cast(ctx.getModel().get(Scopes.STEP.toContextKey()))) != grpModel) {
                        throw new TechnicalException("Group model does not corresponding to parent group");
                    }
                }
            }
        };

        final BiConsumer<IElement<?>, ScriptExecutionContext> fldAction = (elm, ctx) -> {
            if (!(elm instanceof DataElement)) {
                throw new TechnicalException("DataElement object expected");
            } else {
                final Object grpModel = ctx.getModel().get(Scopes.GROUP.toContextKey());
                if (StringUtils.isEmpty(elm.getParentPath())) {
                    throw new TechnicalException("Parent path expected");
                } else {
                    if (find(elm.getParentPath(), CoreUtil.cast(ctx.getModel().get(Scopes.STEP.toContextKey()))) != grpModel) {
                        throw new TechnicalException("Group model does not corresponding to parent group");
                    }
                }
            }
        };

        final Map<String, Object> recordModel = RecursiveDataModelExtractor.create(null).extract(step);

        final Map<String, Object> stepModel = CoreUtil.cast(recordModel.get(step.getId()));
        final Map<String, Object> pageModel = CoreUtil.cast(stepModel.get(step.getGroups().get(0).getId()));

        final Map<String, Object> model = new HashMap<>();
        model.put(Scopes.RECORD.toContextKey(), recordModel);
        model.put(Scopes.STEP.toContextKey(), stepModel);
        model.put(Scopes.PAGE.toContextKey(), pageModel);

        final TreeParserWithContext parser = TreeParserWithContext.create(grpAction, fldAction);
        final Map<String, Object> actual = parser.parse(step.getGroups().get(0), model);

        assertThat(actual, notNullValue());
    }

    private static Object find(final String path, final Map<String, Object> model) {
        final Matcher m = Pattern.compile("^([^\\[.]+)(?:\\[([^\\]]*)\\])?(?:\\.(.+))?$").matcher(path);
        if (m.matches()) {
            Object found = model.get(m.group(1));
            if (null != m.group(2)) {
                final int idx = Optional.of(m.group(2)).filter(StringUtils::isNotEmpty).map(Integer::parseInt).orElse(0);
                found = ((List<Object>) found).get(idx);
            }
            if (found == null || StringUtils.isEmpty(m.group(3))) {
                return found;
            } else {
                return find(m.group(3), CoreUtil.cast(found));
            }
        }
        return model;
    }

}
