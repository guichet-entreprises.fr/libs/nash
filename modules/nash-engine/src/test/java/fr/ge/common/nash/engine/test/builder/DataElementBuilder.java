/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.test.builder;

import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.IWrappedValueElement;

/**
 * Class DataElementBuilder.
 *
 * @author Christian Cougourdan
 */
public class DataElementBuilder extends AbstractDisplayableElementBuilder<DataElementBuilder> {

    /** type. */
    private String type;

    /** mandatory. */
    private Boolean mandatory;

    /** value. */
    private ValueElement value;

    /** display condition. */
    private String displayCondition;

    /** display condition. */
    private String trigger;

    /**
     * Instantie un nouveau data element builder.
     */
    public DataElementBuilder() {
        // Nothing to do
    }

    /**
     * Instantie un nouveau data element builder.
     *
     * @param id
     *            id
     */
    public DataElementBuilder(final String id) {
        this.id(id);
    }

    /**
     * Build.
     *
     * @return data element
     */
    public DataElement build() {
        final DataElement dataElement = new DataElement();

        dataElement.setType(this.type);
        dataElement.setMandatory(this.mandatory);
        dataElement.setValue(this.value);
        dataElement.setDisplayCondition(this.displayCondition);
        dataElement.setTrigger(this.trigger);

        return super.build(dataElement);
    }

    /**
     * Mandatory.
     *
     * @param mandatory
     *            mandatory
     * @return data element builder
     */
    public DataElementBuilder mandatory(final Boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    /**
     * Type.
     *
     * @param type
     *            type
     * @return data element builder
     */
    public DataElementBuilder type(final String type) {
        this.type = type;
        return this;
    }

    /**
     * Value.
     *
     * @param value
     *            value
     * @return data element builder
     */
    public DataElementBuilder value(final String value) {
        this.value = null == value ? null : new ValueElement(value);
        return this;
    }

    /**
     * Value.
     *
     * @param value
     *            value
     * @return data element builder
     */
    public DataElementBuilder value(final IWrappedValueElement value) {
        this.value = null == value ? null : new ValueElement(value);
        return this;
    }

    /**
     * Value.
     *
     * @param value
     *            value
     * @return data element builder
     */
    public DataElementBuilder value(final ValueElement value) {
        this.value = value;
        return this;
    }

    /**
     * Display condition.
     *
     * @param displayCondition
     *            display condition
     * @return data element builder
     */
    public DataElementBuilder displayCondition(final String displayCondition) {
        this.displayCondition = displayCondition;
        return this;
    }

    /**
     * Display condition.
     *
     * @param displayCondition
     *            display condition
     * @return data element builder
     */
    public DataElementBuilder trigger(final String trigger) {
        this.trigger = trigger;
        return this;
    }

}
