/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.engine.mapping.form.v1_2;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.hamcrest.Matcher;

import fr.ge.common.nash.engine.adapter.v1_2.value.Phone;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;

/**
 * Class FormSpecificationDataTest.
 *
 * @author Christian Cougourdan
 */
public class FormSpecificationDataTest extends AbstractFormSpecificationTest<FormSpecificationData> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<FormSpecificationData> getSpecClass() {
        return FormSpecificationData.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getCssReference() {
        return "../css/data.css";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    protected Matcher<FormSpecificationData> matcher() {
        return allOf( //
                notNullValue(), //
                allOf( //
                        hasProperty("id", equalTo("form01")), //
                        hasProperty("label", equalTo("Form 01 label")), //
                        hasProperty("description", equalTo("Form 01 short description.")), //
                        hasProperty("help", equalTo("Need some help for form 01 ?")), //
                        hasProperty("warnings", hasItem(hasProperty("description", equalTo("Form 01 Warning !!!")))) //
                ), //
                hasProperty("groups", //
                        contains( //
                                this.groupMatcher( //
                                        "group01", //
                                        "Group 01 label", //
                                        "Group 01 description.", //
                                        "Group 01 help", //
                                        "Group 01 warning", //
                                        this.dataMatcher("field011", "Field 011 label", null, "Field 011 description.", "Field 011 help", "Field 011 warning", "field 011 value", null, null, null), //
                                        this.dataMatcher("field012", "Field 012 label", null, null, null, null, null, true, null, "$field011") //
                                ), //
                                this.groupMatcher( //
                                        "group02", //
                                        "Group 02 label", //
                                        null, //
                                        null, //
                                        null, //
                                        this.dataMatcher("field021", "Field 021 label", "Integer", null, null, null, "42", null, "1 == 1", null) //
                                ), //
                                this.groupMatcher( //
                                        "group03", //
                                        "Group 03 label", //
                                        null, //
                                        null, //
                                        null, //
                                        this.dataMatcher("field031", "Field 031 label", "Phone", null, null, null, new Phone("fr", "+33123456789", "+33 1 23 45 67 89"), null, null, null) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Data matcher.
     *
     * @param id
     *            id
     * @param label
     *            label
     * @param type
     *            type
     * @param desc
     *            desc
     * @param help
     *            help
     * @param warning
     *            warning
     * @param value
     *            value
     * @param mandatory
     *            mandatory
     * @param ruleAsTag
     *            rule as tag
     * @param ruleAsAttr
     *            rule as attr
     * @return matcher
     */
    private Matcher<DataElement> dataMatcher(final String id, final String label, final String type, final String desc, final String help, final String warning, final Object value,
            final Boolean mandatory, final String ruleAsTag, final String ruleAsAttr) {

        final List<Matcher<? super DataElement>> matchers = new ArrayList<>();

        matchers.add(hasProperty("id", equalTo(id)));
        matchers.add(hasProperty("label", equalTo(label)));
        matchers.add(hasProperty("type", null == type ? nullValue() : equalTo(type)));
        matchers.add(hasProperty("description", null == desc ? nullValue() : equalTo(desc)));
        matchers.add(hasProperty("help", null == help ? nullValue() : equalTo(help)));
        matchers.add(hasProperty("warnings", null == warning ? nullValue() : contains(hasProperty("description", equalTo(warning)))));
        matchers.add(hasProperty("value", this.valueMatcher(value)));
        matchers.add(hasProperty("mandatory", equalTo(mandatory)));
        matchers.add(hasProperty("ruleAsTag", null == ruleAsTag ? nullValue() : equalTo(ruleAsTag)));
        matchers.add(hasProperty("ruleAsAttr", null == ruleAsAttr ? nullValue() : equalTo(ruleAsAttr)));

        return allOf(matchers);
    }

    /**
     * Value matcher.
     *
     * @param value
     *            value
     * @return matcher
     */
    private Matcher<Object> valueMatcher(final Object value) {
        if (value instanceof Phone) {
            final Phone phone = (Phone) value;
            return hasProperty("content", //
                    hasProperty("elements", //
                            contains( //
                                    Arrays.asList( //
                                            this.identifedValueMatcher("country", phone.getCountry()), //
                                            this.identifedValueMatcher("e164", phone.getE164()), //
                                            this.identifedValueMatcher("international", phone.getInternational()) //
                                    ) //
                            ) //
                    ) //
            );
        } else if (value instanceof String) {
            return hasProperty("content", equalTo(value));
        } else {
            return nullValue();
        }
    }

    /**
     * Identifed value matcher.
     *
     * @param key
     *            key
     * @param value
     *            value
     * @return matcher
     */
    private Matcher<Object> identifedValueMatcher(final String key, final Object value) {
        return allOf(hasProperty("id", equalTo(key)), hasProperty("value", equalTo(value)));
    }

    /**
     * Group matcher.
     *
     * @param id
     *            id
     * @param label
     *            label
     * @param desc
     *            desc
     * @param help
     *            help
     * @param warning
     *            warning
     * @param dataMatchers
     *            data matchers
     * @return matcher
     */
    @SuppressWarnings("unchecked")
    private Matcher<GroupElement> groupMatcher(final String id, final String label, final String desc, final String help, final String warning, final Matcher<? super DataElement>... dataMatchers) {
        final List<Matcher<? super GroupElement>> matchers = new ArrayList<>();

        matchers.add(hasProperty("id", equalTo(id)));
        matchers.add(hasProperty("label", equalTo(label)));
        matchers.add(hasProperty("description", null == desc ? nullValue() : equalTo(desc)));

        matchers.add(hasProperty("help", null == help ? nullValue() : equalTo(help)));
        matchers.add(hasProperty("warnings", null == warning ? nullValue() : contains(hasProperty("description", equalTo(warning)))));

        if (ArrayUtils.isEmpty(dataMatchers)) {
            matchers.add(hasProperty("data", nullValue()));
        } else {
            matchers.add(hasProperty("data", contains(dataMatchers)));
        }

        return allOf(matchers);
    }

}
