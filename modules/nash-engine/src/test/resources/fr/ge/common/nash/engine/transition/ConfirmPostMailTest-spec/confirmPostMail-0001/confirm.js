log.info('===> mail block preprocess');

var addr = _input.result.address;
var docPath = _input.parameters.content.attachment;

var data = nash.instance.load('display.xml');
data.bind('request', {
    'input': {
       'referenceId' : addr.referenceId,
       'recipientName' : addr.recipientName,
       'recipientNameCompl' : addr.recipientNameCompl,
       'recipientAddressName' : addr.recipientAddressName,
       'recipientAddressNameCompl' : addr.recipientAddressNameCompl,
       'recipientPostalCode' : addr.recipientPostalCode,
       'recipientCity' : addr.recipientCity
    },
    'content': {
        'attachment': docPath
    }
});
