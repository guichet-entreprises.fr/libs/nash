var model = {};

var model = {};

model ['civiliteNomPrenom']         = $step01.grp01.civility + $step01.grp01.firstname + $step01.grp01.lastname;
model['adresse']                    = $step01.grp02.street;
model['villePays']                  = $step01.grp02.city;
model['telephoneFixe']              = '';
model['telephoneMobile']            = '';
model['courriel']                   = $step01.grp02.mail;
model['dateNaissance']              = '';
model['villePaysNaissance']         = '';
model['nationalite']                = '';
model['date']                       = $step01.grp03.when;
model['lieuSignature']              = '';
model['signature']                  = $step01.grp03.signature;
model['civiliteNomPrenomSignature'] = '';

var doc = pdf.create('models/template', model);
var docAsPdf = pdf.save('document.pdf', doc);



return spec.create({
    id : 'step03',
    label : 'Step 03',
    groups : [ spec.createGroup({
        id : 'grp01',
        label : 'Group 01',
        data : [ spec.createData({
            id : 'doc01',
            label : 'Document 01',
            type : 'FileReadOnly',
            value : [ docAsPdf ]
        }) ]
    }) ]
});
