var doc = nash.doc.load('models/model.pdf') //
    .apply({
        'firstname': _record.step01.grp01.identity.firstname,
        'lastname': _record.step01.grp01.identity.lastname,
        'addr_zipcode': _record.step01.grp01.address.zipcode,
        'addr_city': _record.step01.grp01.address.city
    });



_record.step02.pre02x1.attachment.forEach(function (elm) {
    doc.append(elm);
});



var data = [ spec.createData({
    id : 'record',
    label : 'Generated document',
    type : 'FileReadOnly',
    value : [ doc.save('document.pdf') ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Generated documents',
    data : data
}) ];

return spec.create({
    id : 'step03',
    label : 'Generated documents',
    groups : groups
});