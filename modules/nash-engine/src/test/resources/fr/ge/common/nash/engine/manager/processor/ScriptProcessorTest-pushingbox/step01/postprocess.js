log.info("Calling pushing box function");
var status = pushingbox();

return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.createGroup({
        id : 'pushingboxGroup',
        label : 'Pushing box call',
        data : [ spec.createData({
            id : 'status',
            label : 'Pushing box call status',
            type : 'Boolean',
            value : status
        }) ]
    }) ]
}); 
