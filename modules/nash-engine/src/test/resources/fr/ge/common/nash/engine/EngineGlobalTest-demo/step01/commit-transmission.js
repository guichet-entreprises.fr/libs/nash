/*
 * Call GS API to retrieve record transmission code
 */
log.debug('Commit record transmission code from GS-API');

var response = nash.service.request('https://poc-ge.fr.capgemini.com/gs-api/gs/api/v1/teledossiers/soumettre/{code}', _INPUT_.remote.code) //
    .header('Application', 'GUICHET_ENTREPRISES') //
    .header('CodeDemarche', 'CERFA_11531') //
    .post(null);


var remoteRecord = response.asObject();
log.debug('Retrieved value : {}', remoteRecord);

return;
