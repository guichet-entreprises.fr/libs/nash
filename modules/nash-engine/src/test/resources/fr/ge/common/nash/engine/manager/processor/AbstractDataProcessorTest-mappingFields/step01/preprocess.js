var conftypeChannel = {
    'referential': {
        'text': [
            {"id":"backOffice","label":"Transmission du dossier via BackOffice"},
            {"id":"ftp","label":"Transmission du dossier via FTP"},
            {"id":"address","label":"Transmission du dossier via un courrier"},
            {"id":"email","label":"Transmission du dossier via un courriel"}
        ]
    }
};

return spec.create({
    id : 'configStep01',
    label : "Configuration des canaux d'envoi de dossiers",
    help :"Cet écran vous permet de choisir les différents canaux de transmission des dossiers, les champs ci-dessous vous permettront de renseigner les informations nécessaires en fonction de votre canal choisi.",
    warnings : [
        { id : 'warningConfiguration', label : "En sélectionnant plusieurs canaux, le dossier sera envoyer pour traitement à TOUS les canaux." }
    ],
    groups : [ 
        spec.createGroup({
            id : 'transmissionChannelList',
            label:"Liste des canals de transmission",
            data : [ 
                spec.createGroup({
                    id: "configurationChannelsPanel",
                    label: "Liste des canaux de transmission",
                    help : 'Help value', 
                    data : [
                        spec.createData({
                            id : 'configurationChoicesCheckBoxes',
                            label : "Veuillez choisir vos canaux de prédilection :",
                            type : 'CheckBoxes',
                            conftype : conftypeChannel,
                            value : {
                                "list": {
                                    "text": [
                                       {"id": "backOffice", "label": "Transmission du dossier via BackOffice" },
                                       {"id":"ftp","label":"Transmission du dossier via FTP"}
                                     ]
                                }
                            }
                        }),
                        spec.createData({
                            id : 'configurationChoicesRadios',
                            label : "Veuillez choisir vos canaux de prédilection :",
                            type : 'Radios',
                            conftype : conftypeChannel,
                            value : {
                                "text": [
                                   {"id": "backOffice", "label": "Transmission du dossier via BackOffice" }
                                 ]
                            }
                        }),
                        spec.createData({
                            id : 'checkFtpChoice',
                            label : 'Check FTP Choice',
                            type : 'String',
                            ruleAsAttr : "Value('id').of(configStep01.transmissionChannelList.configurationChannelsPanel.configurationChoicesCheckBoxes).contains('ftp')",
                            value : 'Yes'
                        })
                    ]
                })
            ]
        })
    ]
});
