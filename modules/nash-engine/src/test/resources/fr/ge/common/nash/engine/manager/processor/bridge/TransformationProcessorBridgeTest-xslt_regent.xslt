<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"  xmlns:nash="http://www.ge.fr/schema/1.2/form-manager"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"   exclude-result-prefixes="nash xsl xsi">

    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

    <xsl:template match="/nash:form">
        <DemandeRectificationErreurOuOmissionMaterielleContenueDansActeEtatCivil
    Cerfa="11531*01">
            <IdentiteDemandeur>
                <IdentitePersonnePhysiqueAvecSurnom>
                    <Identification />
                    <Type />
                    <Name />
                    <Description />
                    <AccessRights />
                    <Classification />
                    <Role />
                    <Language />
                    <PersonnePhysiqueAvecSurnom>
                        <Identification />
                        <FamilyName>
                            <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='infoperso']/nash:data[@id='nomFamille']/nash:value/text()"/>
                        </FamilyName>
                        <Name/>
                        <PreferredName />
                        <Alias />
                        <GivenName>
                            <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='infoperso']/nash:data[@id='prenoms']/nash:value/text()"/>
                        </GivenName>
                        <UsualGivenName />
                        <PreferredGivenName />
                        <Gender />
                        <Civility>
                            <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='infoperso']/nash:data[@id='civilite']/nash:value/nash:text/text()"/>
                        </Civility>
                        <Title />
                        <MaritalStatus />
                    </PersonnePhysiqueAvecSurnom>
                    <Profession />
                </IdentitePersonnePhysiqueAvecSurnom>
                <Adresse>
                    <Identification />
                    <InHouseMail>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='adresse']/nash:data[id='etageEscalierAppartement']/nash:value/text()"/>
                    </InHouseMail>
                    <BuildingName>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='adresse']/nash:data[@id='immeubleBatimentResidence']/nash:value/text()"/>
                    </BuildingName>
                    <BuildingNumber>10</BuildingNumber>
                    <BlockName>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='adresse']/nash:data[@id='extensionNumeroRue']/nash:value/text()"/>
                    </BlockName>
                    <RoadType>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='adresse']/nash:data[@id='typeVoie']/nash:value/text()"/>
                    </RoadType>
                    <StreetName>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='adresse']/nash:data[@id='nomVoie']/nash:value/text()"/>
                    </StreetName>
                    <PlotIdentification/>
                    <PostOfficeBox/>
                    <CEDEX />
                    <CEDEXOffice />
                    <Postcode>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='adresse']/nash:data[@id='codePostal']/nash:value/text()"/>
                    </Postcode>
                    <CityName>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='adresse']/nash:data[@id='localite']/nash:value/text()"/>
                    </CityName>
                    <CountrySubDivision />
                    <LineOne />
                    <LineTwo />
                    <LineThree />
                    <LineFour />
                    <LineFive />
                    <LineSix />
                    <LineSeven />
                </Adresse>
                <TelephoneFrance>
                    <Identification />
                    <Channel />
                    <URI />
                    <LocalNumber>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='infosContact']/nash:data[@id='telephone']/nash:value/nash:list/nash:text[@id='e164']/text()"/>
                    </LocalNumber>
                    <CompleteNumber />
                    <CountryNumber />
                    <ExtensionNumber />
                    <AreaNumber />
                    <Access />
                    <Use />
                    <HTMLPreferred />
                </TelephoneFrance>
                <Telecopie>
                    <Identification />
                    <Channel />
                    <URI />
                    <LocalNumber>
                        <xsl:value-of select="nash:group[@id='infopersoGroup']/nash:group[@id='infopersoG']/nash:group[@id='infosContact']/nash:data[@id='telecopie']/nash:value/nash:list/nash:text[@id='e164']/text()"/>
                    </LocalNumber>
                    <CompleteNumber />
                    <CountryNumber />
                    <ExtensionNumber />
                    <AreaNumber />
                    <Access />
                    <Use />
                    <HTMLPreferred />
                </Telecopie>
            </IdentiteDemandeur>
            <DemandeRectificationActe>
                <ActeNaissance>
                    <OuiNon>1</OuiNon>
                    <PrecisionActe>
                        <DateObtention day="01" month="08" year="2017" />
                        <LieuObtentionActe>
                            <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actenaiss']/nash:data[@id='lieuAN']/nash:value/text()"/>
                        </LieuObtentionActe>
                        <Pays>
                            <Identification>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actenaiss']/nash:data[@id='PaysAN']/nash:text/@id"/>
                            </Identification>
                            <Name>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actenaiss']/nash:data[@id='paysAN']/nash:value/nash:text/text()"/>
                            </Name>
                        </Pays>
                    </PrecisionActe>
                    <IdentitePersonneConcernee>
                        <NomsPrenomsPersonneConcernee>
                            <Identification />
                            <FamilyName>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actenaiss']/nash:group[@id='personneAN']/nash:data[@id='nomfamilleAN']/nash:value/text()"/>
                            </FamilyName>
                            <Name/>
                            <PreferredName />
                            <Alias />
                            <GivenName>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actenaiss']/nash:group[@id='personneAN']/nash:data[@id='prenomsAN']/nash:value/text()"/>
                            </GivenName>
                            <UsualGivenName />
                            <PreferredGivenName />
                            <Gender />
                            <Civility />
                            <Title />
                            <MaritalStatus />
                        </NomsPrenomsPersonneConcernee>
                        <DateNaissance day="01" month="08" year="1992" />
                        <LieuObtentionActe>
                            <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actenaiss']/nash:group[@id='personneAN']/nash:data[@id='lieuNaissanceAN']/nash:value/nash:text//text()"/>
                        </LieuObtentionActe>
                        <Pays>
                            <Identification>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actenaiss']/nash:group[@id='personneAN']/nash:data[@id='paysNaissanceAN']/nash:value/nash:text/@id"/>
                            </Identification>
                            <Name>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actenaiss']/nash:group[@id='personneAN']/nash:data[@id='paysNaissanceAN']/nash:value/nash:text/text()"/>
                            </Name>
                        </Pays>
                    </IdentitePersonneConcernee>
                </ActeNaissance>
                <ActeReconnaissance>
                    <OuiNon>1</OuiNon>
                    <PrecisionActe>
                        <DateObtention day="01" month="07" year="2017" />
                        <LieuObtentionActe>
                            <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acterecon']/nash:data[@id='lieuAR']/nash:value/text()"/>
                        </LieuObtentionActe>
                        <Pays>
                            <Identification>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acterecon']/nash:data[@id='PaysAR']/nash:value/nash:text/@id"/>
                            </Identification>
                            <Name>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acterecon']/nash:data[@id='PaysAR']/nash:value/nash:text/text()"/>
                            </Name>
                        </Pays>
                    </PrecisionActe>
                    <IdentitePersonneConcernee>
                        <NomsPrenomsPersonneConcernee>
                            <Identification />
                            <FamilyName>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acterecon']/nash:group[@id='personneAR']/nash:data[@id='nomfamilleAR']/nash:value/text()"/>
                            </FamilyName>
                            <Name/>
                            <PreferredName />
                            <Alias />
                            <GivenName>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acterecon']/nash:group[@id='personneAR']/nash:data[@id='prenomsAR']/nash:value/text()"/>
                            </GivenName>
                            <UsualGivenName />
                            <PreferredGivenName />
                            <Gender />
                            <Civility />
                            <Title />
                            <MaritalStatus />
                        </NomsPrenomsPersonneConcernee>
                        <DateNaissance day="01" month="08" year="1992" />
                        <LieuObtentionActe>
                            <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acterecon']/nash:group[@id='personneAR']/nash:data[@id='lieuNaissanceAR']/nash:value/text()"/>
                        </LieuObtentionActe>
                        <Pays>
                            <Identification>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acterecon']/nash:group[@id='personneAR']/nash:data[@id='paysNaissanceAR']/nash:value/nash:text/@id"/>
                            </Identification>
                            <Name>
                                <xsl:value-of select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acterecon']/nash:group[@id='personneAR']/nash:data[@id='paysNaissanceAR']/nash:value/nash:text/text()"/>
                            </Name>
                        </Pays>
                    </IdentitePersonneConcernee>
                </ActeReconnaissance>
                <ActeMariage>
                    <xsl:variable name="actemar" select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='actemar']"/>
                    <OuiNon>1</OuiNon>
                    <PrecisionActe>
                        <DateObtention day="01" month="06" year="2017" />
                        <LieuObtentionActe>
                            <xsl:value-of select="$actemar/nash:data[@id='lieuAM']/nash:value/text()"/>
                        </LieuObtentionActe>
                        <Pays>
                            <Identification>
                                <xsl:value-of select="$actemar/nash:data[@id='paysAM']/nash:value/nash:text/@id"/>
                            </Identification>
                            <Name>
                                <xsl:value-of select="$actemar/nash:data[@id='paysAM']/nash:value/nash:text/text()"/>
                            </Name>
                        </Pays>
                    </PrecisionActe>
                    <IdentiteEpouse>
                        <NomPrenomEpouse>
                            <Identification />
                            <FamilyName>
                                <xsl:value-of select="$actemar/nash:group[@id='entreAM']/nash:data[@id='nomNaissanceEpouseAM']/nash:value/text()"/>
                            </FamilyName>
                            <Name />
                            <PreferredName />
                            <Alias />
                            <GivenName>
                                <xsl:value-of select="$actemar/nash:group[@id='entreAM']/nash:data[@id='prenomsEpouseAM']/nash:value/text()"/>
                            </GivenName>
                            <UsualGivenName />
                            <PreferredGivenName />
                            <Gender />
                            <Civility />
                            <Title />
                            <MaritalStatus />
                        </NomPrenomEpouse>
                        <DateNaissance day="01" month="05" year="1992" />
                        <LieuDeNaissance>
                            <xsl:value-of select="$actemar/nash:group[@id='entreAM']/nash:data[@id='lieuNaissanceEpouseAM']/nash:value/text()"/>
                        </LieuDeNaissance>
                        <Identification>
                            <xsl:value-of select="$actemar/nash:group[@id='entreAM']/nash:data[@id='paysNaissanceEpouseAM']/nash:value/nash:text/@id"/>
                        </Identification>
                        <Name>
                            <xsl:value-of select="$actemar/nash:group[@id='entreAM']/nash:data[@id='paysNaissanceEpouseAM']/nash:value/nash:text/text()"/>
                        </Name>
                    </IdentiteEpouse>
                    <IdentiteEpoux>
                        <NomPrenomEpoux>
                            <Identification />
                            <FamilyName>
                                <xsl:value-of select="$actemar/nash:group[@id='etAM']/nash:data[@id='nomNaissanceEpouxAM']/nash:value/text()"/>
                            </FamilyName>
                            <Name />
                            <PreferredName />
                            <Alias />
                            <GivenName>
                                <xsl:value-of select="$actemar/nash:group[@id='etAM']/nash:data[@id='prenomsEpouxAM']/nash:value/text()"/>
                            </GivenName>
                            <UsualGivenName />
                            <PreferredGivenName />
                            <Gender />
                            <Civility />
                            <Title />
                            <MaritalStatus />
                        </NomPrenomEpoux>
                        <DateNaissance day="01" month="04" year="1992" />
                        <LieuDeNaissance>
                            <xsl:value-of select="$actemar/nash:group[@id='etAM']/nash:data[@id='lieuNaissanceEpouxAM']/nash:value/text()"/>
                        </LieuDeNaissance>
                        <Pays>
                            <Identification>
                                <xsl:value-of select="$actemar/nash:group[@id='etAM']/nash:data[@id='paysNaissanceEpouxAM']/nash:value/nash:text/@id"/>
                            </Identification>
                            <Name>
                                <xsl:value-of select="$actemar/nash:group[@id='etAM']/nash:data[@id='paysNaissanceEpouxAM']/nash:value/nash:text/text()"/>
                            </Name>
                        </Pays>
                    </IdentiteEpoux>
                </ActeMariage>
                <ActeDeces>
                    <xsl:variable name="acteDeces" select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acteDeces']"/>
                    <OuiNon>1</OuiNon>
                    <PrecisionActe>
                        <DateObtention day="01" month="02" year="2017" />
                        <LieuObtentionActe>
                            <xsl:value-of select="$acteDeces/nash:data[@id='lieuAD']/nash:value/text()"/>
                        </LieuObtentionActe>
                        <Pays>
                            <Identification>
                                <xsl:value-of select="$acteDeces/nash:data[@id='paysAD']/nash:value/nash:text/@id"/>
                            </Identification>
                            <Name>
                                <xsl:value-of select="$acteDeces/nash:data[@id='paysAD']/nash:value/nash:text/text()"/>
                            </Name>
                        </Pays>
                    </PrecisionActe>
                    <IdentitePersonneConcernee>
                        <NomsPrenomsPersonneConcernee>
                            <Identification />
                            <FamilyName>
                                <xsl:value-of select="$acteDeces/nash:group[@id='personneAD']/nash:data[@id='nomFamilleAD']/nash:value/text()"/>
                            </FamilyName>
                            <Name/>
                            <PreferredName />
                            <Alias />
                            <GivenName>
                                <xsl:value-of select="$acteDeces/nash:group[@id='personneAD']/nash:data[@id='prenomsAD']/nash:value/text()"/>
                            </GivenName>
                            <UsualGivenName />
                            <PreferredGivenName />
                            <Gender />
                            <Civility />
                            <Title />
                            <MaritalStatus />
                        </NomsPrenomsPersonneConcernee>
                        <xsl:variable name="acteDeces" select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='acteDeces']"/>
                        <xsl:variable name="dateDeces" select="$acteDeces/nash:group[@id='personneAD']/nash:data[@id='dateNaissanceAD']/nash:value/text()"/>
                        <xsl:variable name="daysDeces" select="($dateDeces div (60*60*24)) mod 31"/>
                        <xsl:variable name="monthDeces" select="($dateDeces div (60*60*24*31)) mod 12"/>
                        <xsl:variable name="yearDeces" select="$dateDeces div (60*60*24*31*12)"/>
                        <xsl:element name="DateNaissance">
                            <xsl:attribute name="day"><xsl:value-of select="$daysDeces"/></xsl:attribute> 
                            <xsl:attribute name="month"><xsl:value-of select="$monthDeces"/></xsl:attribute>
                            <xsl:attribute name="year"><xsl:value-of select="$yearDeces"/></xsl:attribute>                          
                        </xsl:element>
                        <LieuObtentionActe>
                            <xsl:value-of select="$acteDeces/nash:group[@id='personneAD']/nash:data[@id='lieuNaissanceAD']/nash:value/text()"/>
                        </LieuObtentionActe>
                        <Pays>
                            <Identification>
                                <xsl:value-of select="$acteDeces/nash:group[@id='personneAD']/nash:data[@id='paysNaissanceAD']/nash:value/nash:text/@id"/>
                            </Identification>
                            <Name>
                                <xsl:value-of select="$acteDeces/nash:group[@id='personneAD']/nash:data[@id='paysNaissanceAD']/nash:value/nash:text/text()"/>
                            </Name>
                        </Pays>
                    </IdentitePersonneConcernee>
                </ActeDeces>
            </DemandeRectificationActe>
            <xsl:variable name="rectifications" select="nash:group[@id='infosComplementaires']/nash:group[@id='infosCompGroup']/nash:group[@id='rectifications']"/>
            <PrecisionRectificationActe>
                <xsl:value-of select="$rectifications/nash:data[@id='rectificationsDemandees']/nash:value/text()"/>
            </PrecisionRectificationActe>
            <PreuvesDemande>
                <xsl:value-of select="$rectifications/nash:data[@id='motifsRectifications']/nash:value/text()"/>
            </PreuvesDemande>
            <DateSignature day="18" month="08" year="2017" />
            <CodeRoutage>TGI</CodeRoutage>
            <CodePostalJuridiction>
                <xsl:value-of select="nash:group[@id='modeEnvoiGroup']/nash:group[@id='modeEnvoiG']/nash:group[@id='modeEnvoi']/nash:data[@id='codePostalJuridiction']/nash:value/nash:text/@id"/>
            </CodePostalJuridiction>
        </DemandeRectificationErreurOuOmissionMaterielleContenueDansActeEtatCivil>
    </xsl:template>
</xsl:stylesheet>