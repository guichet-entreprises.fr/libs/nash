var data = nash.instance.load('step01.xml');

data.bind('page01.grp03', {
    'grp03x1' : {
        'fld01' : 'Field #01 (#03x1) new value'
    },
    'grp03x2': [{
        'fld01': 'Field #01 (#03x2.0) new value',
        'fld02': 'Field #02 (#03x2.0) new value'
    }, {
        'fld01': 'Field #01 (#03x2.1) new value'
    }]
});

