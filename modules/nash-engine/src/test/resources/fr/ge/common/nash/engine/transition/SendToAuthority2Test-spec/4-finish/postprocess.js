//prepare info to send

var params=[];
params.push('codeCommune:01006 ');
var algo = "trouver_rm";

var others = [];

others.push({
	id:'id01',
	label:'/2-attachment/uploaded/attachmentPreprocess.pjDomicile-1-imAge.jpg'
});
others.push({
	id:'id02',
	label:'/2-attachment/uploaded/attachmentPreprocess.pjIDDeclarantSignataire-1-p.png'
});

var data = nash.instance.load("output.xml");
data.bind("parameters",{
	"algo" : {
		"algo" : algo,
		"params" : params
	},
	"attachment" : {
		"cerfa" : "/3-review/generated/generated.record-1-P0_Agricole.pdf",
		"regent" : "regent.xml",
		"xmltc" : "xmltc.xml",
		"others" : others
	}
});

data.bind("result",{
	"funcId" : "2018-07-NXF-CX7501"
})