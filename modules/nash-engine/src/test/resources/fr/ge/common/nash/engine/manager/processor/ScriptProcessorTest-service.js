log.debug('Calling external service ...');

var response = nash.service.request("${ws.external.url}/v1/test/download") //
    .connectionTimeout(500).receiveTimeout(1000) //
    .accept("bytes") //
    .get();

log.debug('Service return status {}', response.status);
