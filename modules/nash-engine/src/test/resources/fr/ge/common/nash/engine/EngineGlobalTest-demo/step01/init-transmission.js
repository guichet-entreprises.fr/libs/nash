/*
 * Call GS API to retrieve record transmission code
 */
log.debug('Retrieve record transmission code from GS-API');

var baseUrl = 'https://poc-ge.fr.capgemini.com';

var response = nash.service.request(baseUrl + '/gs-api/gs/api/v1/teledossiers') //
    .header('Application', 'GUICHET_ENTREPRISES') //
    .header('CodeDemarche', 'CERFA_11531') //
    .post(null);


var remoteRecord = response.asObject();
log.debug('Retrieved value : {}', remoteRecord.numero);

var groups = [ spec.createGroup({
    id : 'remote',
    label : 'Confirmation',
    description :
        'Nous avons bien transmis votre démarche concernant le formulaire cerfa 11531*01.\n\n'
        + 'La référence de votre démarche est le : `' + remoteRecord.numero + '`.\n'
        + 'Cette référence doit être conservée\n'
        + 'Elle vous sera utile lors de vos éventuels échanges pour le suivi de votre demande avec l\'(les) organisme(s) concerné(s).\n\n'
        + 'Si vous avec inscrit un courriel, votre référence de démarche vous sera également transmise par courriel dans les prochaines minutes.',
    data : [ spec.createData({
        id : 'code',
        label : 'Numéro télédossier',
        type : 'String',
        value : remoteRecord.numero
    }) ]
}) ];

return spec.create({
    id : 'finish',
    label : 'Demande de rectification d\'une erreur ou d\'une omission matérielle contenue dans un acte de l\'état civil',
    groups : groups
});
