/*
 *
 * Search for authority identity
 *
 */

var serviceParams = {
    'zipCode' : $step01.grp02.zipcode
};

var response = nash.service.request('${ws.external.url}/v1/test/{code}/execute', 'Algo_Code') //
        .header('Application', 'Nash') //
        .connectionTimeout(500) //
        .receiveTimeout(1000) //
        .accept('json') //
        .post(serviceParams);


var authority = response.asObject();


//var data = nash.specification.load(_INPUT_NAME_);
//
//data.grp01.funcId = authority.funcId;
//data.grp01.label = authority.label;
//
//return data;

return spec.create({
    id : 'findDest',
    label : "Conseil en propriété industrielle - établissement de l'authorité compétente",
    groups : [ spec.createGroup({
        id : 'identification',
        label : "Identification de l'établissement de l'authorité compétente",
        data : [ spec.createData({
            id : 'funcId',
            label : "Code de l'établissement de l'authorité compétente",
            description : 'Code interne',
            type : 'String',
            value : authority.code
        }), spec.createData({
            id : 'label',
            label : "Description de l'établissement",
            description : 'Description',
            type : 'String',
            value : authority.label
        })
	]
    }) ]
});
