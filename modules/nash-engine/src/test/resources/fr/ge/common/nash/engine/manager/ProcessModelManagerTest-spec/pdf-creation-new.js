var cerfa = nash.doc //
    .load('models/cerfa14004-02.pdf') //
    .apply({
        firstname : 'John',
        lastname : 'Doe [' + nash.record.description().recordUid + ']'
    });

if ($data01.grp01.fld01) {
    $data01.grp01.fld01.forEach(function (elm) {
        cerfa.append(elm);
    })
}

return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.group({
        id : 'generated',
        label : 'Generated form',
        data : [ spec.data({
            id : 'cerfa14004',
            label : 'Cerfa 14004*2',
            description : 'Cerfa obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfa.save('cerfa14004.pdf') ]
        }) ]
    }) ]
});
