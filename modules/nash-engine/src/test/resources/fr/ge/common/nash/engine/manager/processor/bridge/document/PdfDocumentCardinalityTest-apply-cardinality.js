var pdfModel = nash.doc.load('model.pdf');

var formFields = [];
formFields['prenomUsageDeclarant'] = _INPUT_.grp01.fld01;
formFields['dateNaissance'] = _INPUT_.grp01.fld02;

log.debug('date de naissance : {}', formFields['dateNaissance']);

var pdfResult = pdfModel.apply(formFields);

var data = [ spec.createData({
    id : 'record',
    label : 'Modèle',
    description : 'Prévisualisation du modèle',
    type : 'FileReadOnly',
    value : [ pdfResult.save('model.pdf') ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Prévisualisation du modèle',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Volet Prévisualisation du modèle',
    groups : groups
});