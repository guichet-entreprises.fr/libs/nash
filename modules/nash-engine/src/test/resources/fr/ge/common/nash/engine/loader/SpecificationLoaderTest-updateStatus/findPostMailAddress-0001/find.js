var funcId = _input.result.funcId;

_log.info('===> find post mail address to "{}"', funcId);

// call directory with funcId to find all information of Authorithy
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', funcId) //
.accept('json') //
.get();

// result
var receiverInfo = response.asObject();

var addr = {};
var nfo = receiverInfo.details || undefined;

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}

var who = map(receiverInfo, {
    'funcId' : 'entityId', //
    'funcLabel' : 'label' //
});

if (nfo == undefined || nfo.transferChannels == undefined || nfo.transferChannels.address == undefined || nfo.transferChannels.address.addressDetail == undefined) {
    addr = {
        'recipientName' : null,
        'recipientNameCompl' : null,
        'recipientAddressName' : null,
        'recipientAddressNameCompl' : null,
        'recipientPostalCode' : null,
        'recipientCity' : null
    };
} else {
    addr = map( //
    nfo.transferChannels.address.addressDetail, {
        'recipientName' : 'recipientName', //
        'recipientNameCompl' : 'recipientNameCompl', //
        'recipientAddressName' : 'addressName', //
        'recipientAddressNameCompl' : 'addressNameCompl', //
        'recipientPostalCode' : 'postalCode', //
        'recipientCity' : 'cityName' //
    });
    addr['referenceId'] = nash.record.description().recordUid;
}

var content = map(_input.parameters.content, {
    'attachment' : 'attachment'
});

_log.info("Preparing display file with input : {}, output : {} and content : {}", who, addr, content);
var data = nash.instance.load("display.xml");
data.bind('request', {
    'input' : who,
    'output' : addr,
    'content' : content
});
