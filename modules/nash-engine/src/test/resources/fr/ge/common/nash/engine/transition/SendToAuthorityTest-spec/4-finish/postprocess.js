//prepare info to send
var algo = "trouver Cfe";
var secteur1 = "Agricole";
var typePersonne = "PP";
var formJuridique = "EIRL";
var optionCMACCI = "NON";
var codeCommune = '75001';

var others = [];

others.push({
	id:'id01',
	label:'/2-attachment/uploaded/attachmentPreprocess.pjDomicile-1-imAge.jpg'
});
others.push({
	id:'id02',
	label:'/2-attachment/uploaded/attachmentPreprocess.pjIDDeclarantSignataire-1-p.png'
});

var data = nash.instance.load("output.xml");
data.bind("parameters",{
	"algo" : {
		"algo" : algo,
		"formJuridique" : formJuridique,
		"optionCMACCI" : optionCMACCI,
		"codeCommune" : codeCommune,
		"typePersonne" : typePersonne,
		"secteur1" : secteur1
	},
	"attachment" : {
		"cerfa" : "/3-review/generated/generated.record-1-P0_Agricole.pdf",
		"regent" : "regent.xml",
		"xmltc" : "xmltc.xml",
		"others" : others
	}
});

data.bind("result",{
	"funcId" : "2018-07-NXF-CX7501"
})