
log.info('Approach selected by user : {}', $context.action.choices.choice.getId());

target = _CONFIG_.get('forms.company.'+$context.action.choices.choice.getId()+'.public.url');

nash.record.stepsMgr().requestDone(nash.record.currentStepPosition());

log.info('Now redirecting user to {}', target);

return { url: target };