//check inout information

var docs = $step01.grp01.files;
_log.info("Documents are {}", docs);

var files = [];
for (var i = 0; i < docs.length; ++i) {
    files.push({
        'document': docs[i],
        'zoneId': 'id' + i
    });
}
var nfo = nash.hangout.stamp({
    'files' : files,
    'civility' : $step01.grp01.civility,
    'lastName' : $step01.grp01.lastName,
    'firstName' : $step01.grp01.firstName,
    'email' : $step01.grp01.email,
    'phone' : $step01.grp01.phone
});

return nfo;
