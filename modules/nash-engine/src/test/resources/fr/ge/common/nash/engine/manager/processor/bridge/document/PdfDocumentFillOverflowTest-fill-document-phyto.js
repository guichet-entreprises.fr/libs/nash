var pdfModel = nash.doc.load('masque.pdf');
var pdfAnnexe = nash.doc.load('annexe.pdf');

var requirements = [
                {
                    "articles" : [
                        "L’orge n’a pas été produite en région Rhône Alpes"
                    ]
                }
            ];

logger.info("Calling fillOverflow operation");
var pdfFillOverflow = pdfModel.fillOverflow(
	'declaration_supp',
    pdfModel.buildPhrases(requirements),
	pdfAnnexe,
	'exigences'
);

return spec.create({
    id : 'review',
    label : 'Volet Prévisualisation du modèle de certificat',
    groups : [ spec.createGroup({
        id : 'generated',
		label : 'Prévisualisation du modèle de certificat de l’administration',
        data : [ spec.createData({
            id : 'modele_phyto',
			label : 'Prévisualisation du modèle de certificat avec ajout des exigences',
            type : 'FileReadOnly',
            value : [ pdfFillOverflow.save('modele_phyto.pdf') ]
        }) ]
    }) ]
});
