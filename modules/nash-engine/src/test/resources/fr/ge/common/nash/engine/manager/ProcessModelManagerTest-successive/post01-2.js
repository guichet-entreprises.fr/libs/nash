return spec.create({
    label : 'Step 02',
    groups : [ spec.group({
        id : 'grp02',
        label : 'Group 02 Title',
        data : [ spec.data({
            id : 'fld02a',
            label : 'Field 02 Title',
            type : 'String',
            value : _input.grp01.fld01a + ' -> 02'
        }), spec.data({
            id : 'fld02b',
            label : 'Field 02 Title',
            type : 'String',
            value : _INPUT_.grp01.fld01b + ' -> 02'
        }) ]
    }) ]
});
