return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.group({
        id : 'generated',
        label : 'Generated form',
        data : [ spec.data({
            id : 'status',
            label : 'Record status',
            type : 'ComboBox',
            conftype : {
                'referential': {
                    'text': [
                      { 'id': 'opened', 'label' : 'Ouvert'},
                      { 'id': 'closed', 'label' : 'Closed'},
                      { 'id': 'indetermined', 'label' : 'Indetermined' }
                    ]
                  }
            }
        }) ]
    }) ]
}); 
