var table = [
    {
        "---": "CREATE",
        "ENT/Création": 0,
        "ENT/Modification": 0,
        "ENT/Cessation": 0
    },
    
    { 
        "---": "FINISH",
        "ENT/Création": 0,
        "ENT/Modification": 0,
        "ENT/Cessation": 0
    },
    
    { 
        "---": "PROCESS",
        "ENT/Création": 0,
        "ENT/Modification": 0,
        "ENT/Cessation": 0
        }
  ];

var template_table ="<table class='table table-striped'> <thead> " +
    "<tr> {{#each array.[0]}}<th>{{@key}}</th>{{/each}}</tr></thead>" +
    "<tbody>{{#each array}}<tr> {{#each this}}  <td>{{this}}</td>{{/each}}</tr>{{/each}}</tbody>" +
    "</table>"  

var Handlebars = require('handlebars');
var template = Handlebars.compile(template_table);

var result = template({ array: table });

log.debug(result);

return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.group({
        id : 'result',
        label : 'Result',
        data : [ spec.data({
            id : 'Data',
            label : 'Result data',
            type : 'Description',
            value : result
        }) ]
    }) ]
});
