/*
 * Call GS API to send record documents
 */
log.debug('Send record documents GS-API');

var ref = _INPUT_.remote.code;
var doc = $step01.grp01.documents[0];

var response = nash.service.request('https://poc-ge.fr.capgemini.com/gs-api/gs/api/v1/teledossiers/{code}/documents', ref)
//var response = nash.service.request('http://FRRNSSIANM6GDEV06:8085/gs-api/gs/api/v1/teledossiers/{code}/documents', ref)
//var response = nash.service.request('http://nash.int.guichet-entreprises.fr:8000/gs-api/gs/api/v1/teledossiers/{code}/documents', ref)
    .header('Application', 'GUICHET_ENTREPRISES')
    .header('CodeDemarche', 'CERFA_11531')
    .dataType('form')
    .accept('json')
    .post({
        metadata: {
            codeTypeDocument : '11531',
            hashSha256 : doc.hash.toLowerCase(),
            intitule : doc.label
        },
        destinataires: [ {
            id : 'TGI_3500',
            type : 'Juridiction'
        } ],
        file: doc
    });



log.debug('Retrieved value : {}', response.asObject());

var groups = [ spec.createGroup({
    id : 'remote',
    label : 'Documents transmis'
}) ];

return spec.create({
    id : 'finish',
    label : 'Demande de rectification d\'une erreur ou d\'une omission matérielle contenue dans un acte de l\'état civil',
    groups : groups
});
