destFuncId = $findDest.identification.funcId
emailObject = "Demande d'autorisation PI"
emailContent = "Bonjour, Je vous prie de trouver ci-joinet la demande. Florent"
emailPJ = ''

return spec.create({
    id : 'prepareSend',
    label : "Preparation de l'envoi",
    groups : [ spec.createGroup({
        id : 'email',
        label : "Contenu email",
        data : [ spec.createData({
            id : 'destFuncId',
            label : "Email Destinataire",
            type : 'Email',
            value : [ codeAuth ]
        }), spec.createData({
            id : 'emailObject',
            label : "Description de l'�tablissement",
            description : 'Description',
            type : 'String',
            value : [ authLabel ]
        }), spec.createData({
            id : 'emailContent',
            label : "Description de l'�tablissement",
            description : 'Description',
            type : 'String',
            value : [ authLabel ]
        }), spec.createData({
            id : 'emailPJ',
            label : "PJ email",
            description : 'Description',
            type : 'FileReadOnly',
            value : [ authLabel ]
        })
	]
    }) ]
});
