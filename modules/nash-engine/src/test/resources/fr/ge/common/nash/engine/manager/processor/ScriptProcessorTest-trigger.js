return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.group({
        id : 'generated',
        label : 'Generated form',
        data : [ spec.data({
            id : 'status',
            label : 'Record status',
            type : 'ComboBox',
            trigger : 'submit()'
        }) ]
    }) ]
}); 
