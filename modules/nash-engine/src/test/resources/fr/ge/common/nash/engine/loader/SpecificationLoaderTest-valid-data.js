// TODO : call to directory WS

var dataContactName = spec.createData({
    id : 'name',
    label : 'Contact name',
    type : 'String',
	mandatory:true,
	value : 'data'
});

var groupContactInfo = spec.createGroup({
    id : 'contactInfo',
    label : 'Contact information',
    data : [ dataContactName]
});

var specFindReceiver = spec.create({
    id : 'findReceiver',
    label : 'Find receiver',
    groups : [ groupContactInfo ]
});

return specFindReceiver;