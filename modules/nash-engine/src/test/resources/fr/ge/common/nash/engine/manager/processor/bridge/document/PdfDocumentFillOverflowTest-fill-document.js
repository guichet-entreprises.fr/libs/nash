var pdfModel = nash.doc.load('masque.pdf');
var pdfAnnexe = nash.doc.load('annexe.pdf');

var requirements = [
	'Remplissage du champs *exigences*'
	,'Remplissage du champs **exigences**'
];


logger.info("Calling fillOverflow operation");
var pdfFillOverflow = pdfModel.fillOverflow(
	'adresse_expediteur',
	requirements,
	pdfAnnexe,
	'exigences'
);

return spec.create({
    id : 'review',
    label : 'Volet Prévisualisation du modèle de certificat',
    groups : [ spec.createGroup({
        id : 'generated',
		label : 'Prévisualisation du modèle de certificat de l’administration',
        data : [ spec.createData({
            id : 'modele_sanitaire',
			label : 'Prévisualisation du modèle de certificat avec ajout des exigences',
            type : 'FileReadOnly',
            value : [ pdfFillOverflow.save('modele_sanitaire.pdf') ]
        }) ]
    }) ]
});
