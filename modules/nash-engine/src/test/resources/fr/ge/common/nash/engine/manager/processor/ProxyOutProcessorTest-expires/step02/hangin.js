// check inout information

var proxyResult = _INPUT_.proxyResult;

return spec.create({
    id : 'proxy',
    label : 'Résultat de l\'appel au proxy',
    groups : [ spec.createGroup({
        id : 'result',
        label : 'Résultat de l\'appel au proxy',
        data : [ spec.createData({
            id : 'status',
            label : 'Status',
            type : 'String',
            value : proxyResult.status
        }), //
        spec.createData({
            id : 'message',
            label : 'Message',
            type : 'String',
            value : proxyResult.message
        }), //
        spec.createData({
            id : 'files',
            label : 'Documents',
            type : 'FileReadOnly',
            value : proxyResult.files
        }) ]
    }) ]
});
