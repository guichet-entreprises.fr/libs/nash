return spec.create({
    label : 'Step 01',
    groups : [ spec.group({
        id : 'grp01',
        label : 'Group 01 Title',
        data : [ spec.data({
            id : 'fld01a',
            label : 'Field 01a Title',
            type : 'String',
            value : _input.grp01.fld01 + ' -> 01'
        }), spec.data({
            id : 'fld01b',
            label : 'Field 01b Title',
            type : 'String',
            value : _input.grp01.fld01 + ' -> 01'
        }) ]
    }) ]
});
