var cerfa = pdf.create('cerfa14004-02', {
    firstname : 'John',
    lastname : 'Doe'
});

var cerfaPdf = pdf.save('cerfa14004.pdf', cerfa);

return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.group({
        id : 'generated',
        label : 'Generated form',
        data : [ spec.data({
            id : 'cerfa14004',
            label : 'Cerfa 14004*2',
            description : 'Cerfa obtenu à partir des données saisies',
            type : 'File',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
