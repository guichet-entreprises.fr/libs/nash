var cerfa = pdf.create('models/cerfa14004-02.pdf', {
    firstname : 'John',
    lastname : 'Doe'
});

var cerfaPdf = pdf.save('generated/cerfa14004.pdf', cerfa);

return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.group({
        id : 'generated',
        label : 'Generated form',
        data : [ spec.data({
            id : 'cerfa14004',
            label : 'Cerfa 14004*2',
            description : 'Cerfa obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
