// create record from spec '2017-09-AAA-BBB-42' from instance '${nash.web.url}' on instance '${nash.web.url}'
var record = nash.instance.from('${ws.external.url}') //
    .createRecord('2017-09-SPC-AAA-02');

// load data from specific file
var data = record.load('step01/data.xml')

// set a field value
data.set('grp01.fld01', 'new value');

// add files
record.addFile('step01/data-added.xml', nash.util.resourceFromPath('/step01/data.xml'));
record.addFile('step02/data-01.xml', $step01.grp01.fld03[0]);

// execute current step
record.execute();

// execute until a specific step
//record.execute('resume');

// retrieve record remote url
var recordUid = record.save();

return { url: recordUid, message: "All is ok" };
