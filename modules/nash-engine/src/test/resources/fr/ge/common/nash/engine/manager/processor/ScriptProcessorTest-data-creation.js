var cerfa = pdf.create('cerfa14004-02', $data);

var cerfaPdf = pdf.save('cerfa14004.pdf', cerfa);

return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.group({
        id : 'generated',
        label : 'Generated form',
        data : [ spec.data({
            id : 'firstCerfa',
            label : 'Cerfa 14004*2',
            description : 'Cerfa obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
