return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.group({
        id : 'generated',
        label : 'Generated form',
        data : [ spec.data({
            id : 'status',
            label : 'Record status',
            type : 'RemoteContent',
            conftype : {
            	param : [ {'name' : 'test', 'value' : 'value01'},  {'name' : 'test1', 'value' : 'value02'} ]
            }
        }) ]
    }) ]
}); 
