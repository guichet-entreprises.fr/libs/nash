_log.info("===> postprocess findPostMailAddress");


//call directory with funcId to find all information of Authorithy

var destFuncId = _input.request.input.funcId;
var destFuncLabel = _input.request.input.funcLabel;

function map(src, mapping) {
    var dst = {};
    Object.keys(mapping).forEach(function(key) {
        dst[key] = src[mapping[key]];
    });
    return dst;
}


var address = map(_input.request.output, {
    'referenceId' : 'referenceId',
    'recipientName' : 'recipientName',
    'recipientNameCompl' : 'recipientNameCompl',
    'recipientAddressName' : 'recipientAddressName',
    'recipientAddressNameCompl' : 'recipientAddressNameCompl',
    'recipientPostalCode' : 'recipientPostalCode',
    'recipientCity' : 'recipientCity',
});

_log.info("===> address information : {}", address);
nash.instance //
    .load('output.xml') //
    .bind('result', {
    	'funcId' : destFuncId,
    	'funcLabel': destFuncLabel,
        'address' : address
    });
