// input functionnal id of authority
var grp = _input.result;
var destFuncId = !grp.funcId ? '' : grp.funcId;

var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', destFuncId) //
.connectionTimeout(10000) //
.receiveTimeout(10000) //
.accept('json') //
.get();

var receiverInfo = response.asObject();

var funcId = !receiverInfo.entityId ? null : receiverInfo.entityId;
var funcLabel = !receiverInfo.label ? null : receiverInfo.label;
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;

var nbrChannels = 0
if (contactInfo.transferChannels != null) {
    var backOffice = !contactInfo.transferChannels.backoffice ? null : contactInfo.transferChannels.backoffice;
    var ftp = !contactInfo.transferChannels.ftp ? null : contactInfo.transferChannels.ftp;
    var email = !contactInfo.transferChannels.email ? null : contactInfo.transferChannels.email;
    var address = !contactInfo.transferChannels.address ? null : contactInfo.transferChannels.address;
}
var nbrChannel = 0
var id = "";
var label = "";

if (null != backOffice && backOffice.state == "enabled") {
    nbrChannel++;
    id = "backOfficeChannel";
    label = "Backoffice";
}
if (null != ftp && ftp.state == "enabled") {
    nbrChannel++;
    id = "ftpChannel"
    label = "FTP"
}
if (null != email && email.state == "enabled") {
    nbrChannel++;
    id = "emailChannel"
    label = "Courrier éléctronique"
}
if (null != address && address.state == "enabled") {
    nbrChannel++;
    id = "addressChannel"
    label = "Courrier Papier"
}
var data = nash.instance.load("display.xml");
data.bind("result", {
    funcId : {
        funcId : funcId,
        labelId : funcLabel
    },
    nbrChannel : nbrChannel + '',
    activeChannels : {
        activeChannel : {
            id : id,
            label : label
        }
    }
})
