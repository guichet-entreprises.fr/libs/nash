// TODO : appel à directory

var dataContactName = spec.createData({
    id : 'name',
    label : 'Contact name',
    type : 'String',
	mandatory:true
});

var groupContactInfo = spec.createGroup({
    id : 'contactInfo',
    label : 'Contact information',
    data : [ dataContactName]
});
var specFindReceiver = spec.create({
    id : 'findReceiver',
    label : 'Find receiver',
    groups : [ groupContactInfo ]
});
return specFindReceiver;