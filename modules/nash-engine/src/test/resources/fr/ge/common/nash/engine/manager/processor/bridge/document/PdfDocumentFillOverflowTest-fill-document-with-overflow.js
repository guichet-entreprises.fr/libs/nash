var pdfModel = nash.doc.load('masque.pdf');
var pdfAnnexe = nash.doc.load('annexe.pdf');

var requirements = [
                {
                    "articles" : [
                        "Le lait et les produits laitiers décrits ci-dessus ont été produits, transformés et stockés dans des régions considérées indemnes de fièvre aphteuse  la France depuis le 03 juillet 2001 - par l\'Agence Canadienne d\'Inspection des Aliments) / *the milk and milk products described above have been produced, processed and stored in regions considered as free from foot and mouth disease  France since july 3rd 2001- by CFIA (Canadian Food Inspection Agency).*",
                        "Le lait et les produits laitiers décrits ci-dessus ont été soumis à l'un des traitements suivants / *The milk and the milk products described above have been subjected to one of the following treatments:*"
                    ],
                    "blocs" : [
                        "minimum de 72C pour au moins 15 secondes / *minimum of 72C for at least 15 seconds",
                        "minimum de 140C pour au moins 5 secondes / *minimum of 140°C for at least 5 seconds",
                        "pH inférieur à 5 pour un minimum de 2 heures / *pH lower than 5 for a minimum of 2 hours.*"
                    ]
                },
                {
                    "articles" : [
                        "Le lait et les produits laitiers décrits ci-dessus ont été produits, transformés et stockés dans des régions considérées indemnes de fièvre aphteuse  la France depuis le 03 juillet 2001 - par l\'Agence Canadienne d\'Inspection des Aliments) / *the milk and milk products described above have been produced, processed and stored in regions considered as free from foot and mouth disease  France since july 3rd 2001- by CFIA (Canadian Food Inspection Agency).*"
                    ]
                }
            ];

logger.info("Calling fillOverflow operation");
var pdfFillOverflow = pdfModel.fillOverflow(
	'adresse_expediteur',
	pdfModel.buildPhrases(requirements),
	pdfAnnexe,
	'exigences'
);

return spec.create({
    id : 'review',
    label : 'Volet Prévisualisation du modèle de certificat',
    groups : [ spec.createGroup({
        id : 'generated',
		label : 'Prévisualisation du modèle de certificat de l’administration',
        data : [ spec.createData({
            id : 'modele_sanitaire',
			label : 'Prévisualisation du modèle de certificat avec ajout des exigences',
            type : 'FileReadOnly',
            value : [ pdfFillOverflow.save('modele_sanitaire.pdf') ]
        }) ]
    }) ]
});
