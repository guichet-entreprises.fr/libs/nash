return spec.create({
    label : 'Step 03',
    groups : [ spec.group({
        id : 'grp03',
        label : 'Group 03 Title',
        data : [ spec.data({
            id : 'fld03a',
            label : 'Field 03a Title',
            type : 'String',
            value : _input.grp02.fld02a + ' -> 03'
        }), spec.data({
            id : 'fld03b',
            label : 'Field 03b Title',
            type : 'String',
            value : _INPUT_.grp02.fld02b + ' -> 03'
        }) ]
    }) ]
});
