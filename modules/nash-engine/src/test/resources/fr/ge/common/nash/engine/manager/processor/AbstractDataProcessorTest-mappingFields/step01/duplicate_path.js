return spec.create({
    id : 'configStep01',
    label : "Configuration des canaux d'envoi de dossiers",
    help :"Cet écran vous permet de choisir les différents canaux de transmission des dossiers, les champs ci-dessous vous permettront de renseigner les informations nécessaires en fonction de votre canal choisi.",
    warnings : [
        { id : 'warningConfiguration', label : "En sélectionnant plusieurs canaux, le dossier sera envoyer pour traitement à TOUS les canaux." }
    ],
    groups : [ 
        spec.createGroup({
            id : 'transmissionChannelList',
            label:"Liste des canals de transmission",
            data : [ 
                spec.createData({
                    id : 'checkFtpChoice',
                    label : 'Check FTP Choice',
                    type : 'String',
                    value : 'Yes'
                }), 
                spec.createData({
                    id : 'checkFtpChoice',
                    label : 'Check FTP Choice',
                    type : 'String',
                    value : 'Yes'
                })
            ]
        })
    ]
});
