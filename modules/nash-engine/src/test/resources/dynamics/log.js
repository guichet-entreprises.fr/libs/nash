(function () {

    function log() { console.log.apply(null, arguments); }

    return {
        fatal: function fatal() { log.apply(null, [ '[fatal]' ].concat(Array.prototype.slice.call(arguments))); },
        error: function error() { log.apply(null, [ '[error]' ].concat(Array.prototype.slice.call(arguments))); },
        warn: function warn() { log.apply(null, [ '[warn]' ].concat(Array.prototype.slice.call(arguments))); },
        info: function info() { log.apply(null, [ '[info]' ].concat(Array.prototype.slice.call(arguments))); },
        debug: function debug() { log.apply(null, [ '[debug]' ].concat(Array.prototype.slice.call(arguments))); },
        trace: function trace() { log.apply(null, [ '[trace]' ].concat(Array.prototype.slice.call(arguments))); }
    };

})();
