FROM tomcat:8.0-jre8-alpine

MAINTAINER christian.cougourdan@capgemini.com

ENV CATALINA_BASE $CATALINA_HOME

ENV APP_NAME nash-ws-server
ENV APP_DB_NAME frm
ENV APP_DB_USERNAME frm_usr
ENV APP_DB_PASSWORD frm_pwd
ENV APP_DB_CONNECTION_INITIAL 2
ENV APP_DB_CONNECTION_MAX 5
ENV APP_DB_CONNECTION_IDLE 3
ENV APP_DB_DDL_AUTO update
ENV APP_LOG_LEVEL info


RUN \
    rm -rf ${CATALINA_BASE}/webapps/* \
    && mkdir -p /app/conf

ADD setenv.sh ${CATALINA_BASE}/bin/
ADD postgresql.jar ${CATALINA_HOME}/lib/
ADD ${APP_NAME}.war /app/
ADD ${APP_NAME}-config.zip /tmp/
ADD context.xml ${CATALINA_BASE}/conf/Catalina/localhost/${APP_NAME}.xml

RUN \
    cd /app/conf \
    && unzip /tmp/${APP_NAME}-config.zip \
    && rm -f /tmp/${APP_NAME}-config.zip \
    && sed -i.bak -e 's/@jndi\.db\.datasource@/jdbc\/nash-manager/g ; \
                s/@hibernate\.dialect@/org\.hibernate\.dialect\.PostgreSQL82Dialect/g ; \
                s/@hibernate\.hbm2ddl\.auto@/${app.db.ddl.auto}/g ; \
                s/@hibernate\.show_sql@/false/g ; \
                s/@mas\.tracker\.service\.url@/http:\/\/tracker:8080\/tracker-server\/api/g ; \
                s/@mas\.forms\.service\.url@/http:\/\/forge-back:8080\/nash-ws-server\/api/g ; \
                s/@storage\.record\.root@/\/data\/records/g ; \ 
                s/@environment@/Developpement/g ; \
                s/@frontUrl@/http:\/\/www\.dev\.guichet-entreprises\.fr/g ; \
                s/@log\.file\.technical@/\/app\/log\/tech.log/g ; \
                s/@log\.file\.functional@/\/app\/log\/func.log/g ; \
                s/@log\.file\.thirdParty@/\/app\/log\/thirdParty.log/g ; \
                s/@log\.max_size@/10 MB/g ; \
                s/@log\.max_index@/20/g ; \
                s/@log\.level@/warn/g ; \
                s/@log\.level_fwk@/warn/g ; \
                s/@log\.level_app@/${sys:app.log.level}/g' \
                /app/conf/*


CMD [ "catalina.sh", "run" ]
