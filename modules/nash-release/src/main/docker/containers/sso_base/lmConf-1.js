{
    "nullAuthnLevel" : 2,
    "oidcAuthnLevel" : 1,
    "notificationStorageOptions" : {
        "dirName" : "/var/lib/lemonldap-ng/notifications"
    },
    "portalDisplayRegister" : "0",
    "oidcServiceMetaDataTokenURI" : "token",
    "grantSessionRules" : {},
    "demoExportedVars" : {
        "cn" : "cn",
        "mail" : "mail",
        "uid" : "uid"
    },
    "samlStorageOptions" : {},
    "notification" : 1,
    "portalSkinBackground" : "1280px-Cedar_Breaks_National_Monument_partially.jpg",
    "https" : 1,
    "httpOnly" : 0,
    "domain" : "dev.guichet-entreprises.fr",
    "trustedDomains" : "auth.dev.guichet-entreprises.fr, forms-admin.dev.guichet-partenaires.fr, forms-support.dev.guichet-partenaires.fr, forms-www.dev.guichet-entreprises.fr",
    "persistentStorageOptions" : {
        "LockDirectory" : "/var/lib/lemonldap-ng/psessions/lock",
        "Directory" : "/var/lib/lemonldap-ng/psessions"
    },
    "casStorageOptions" : {},
    "securedCookie" : 1,
    "sessionDataToRemember" : {},
    "portalRequireOldPassword" : 1,
    "cfgAuthorIP" : "192.196.56.1",
    "captchaStorage" : "Apache::Session::File",
    "dbiExportedVars" : {
        "email" : "email",
        "uid" : "login",
        "displayName" : "display_name"
    },
    "dbiPasswordMailCol" : "email",
    "portalOpenLinkInNewWindow" : 0,
    "portalSkinRules" : {},
    "oidcOPMetaDataJWKS" : {},
    "samlIDPMetaDataOptions" : null,
    "mailUrl" : "https://auth.dev.guichet-entreprises.fr/mail.pl",
    "captcha_size" : 6,
    "samlIDPMetaDataXML" : {},
    "userDB" : "Choice",
    "oidcStorageOptions" : {},
    "reloadUrls" : {
        "reload.dev.guichet-entreprises.fr" : "https://reload.dev.guichet-entreprises.fr/reload"
    },
    "applicationList" : {
        "0001-cat" : {
            "0002-app" : {
                "type" : "application",
                "options" : {
                    "name" : "Form Admin",
                    "description" : "Upload, test, and deploy form specifications",
                    "uri" : "https://forms-admin.dev.guichet-partenaires.fr/form-admin",
                    "logo" : "network.png",
                    "display" : "auto"
                }
            },
            "type" : "category",
            "catname" : "Guichet Entreprises"
        }
    },
    "portalPingInterval" : 60000,
    "cfgNum" : "1",
    "hideOldPassword" : 0,
    "portalSkin" : "bootstrap",
    "maintenance" : 0,
    "locationRules" : {
        "forms-admin.dev.guichet-partenaires.fr" : {
            "(?#Logout)^/[^/]+/logout" : "logout_app_sso",
            "default" : "accept"
        }
    },
    "remoteGlobalStorageOptions" : {},
    "CAS_proxiedServices" : {},
    "portal" : "https://auth.dev.guichet-entreprises.fr/",
    "portalCheckLogins" : 1,
    "exportedHeaders" : {
        "forms-admin.dev.guichet-partenaires.fr" : {
            "Auth-User" : "$mail",
            "Display-Name" : "$displayName ? $displayName : $givenName . \" \" . $sn"
        }
    },
    "userPivot" : "login",
    "oidcServiceMetaDataRegistrationURI" : "register",
    "oidcServiceMetaDataJWKSURI" : "jwks",
    "loginHistoryEnabled" : 1,
    "dbiAuthChain" : "dbi:Pg:dbname=postgres;host=db",
    "captcha_mail_enabled" : 0,
    "cda" : "1",
    "captcha_login_enabled" : 0,
    "cfgAuthor" : "anonymous",
    "macros" : {
        "_whatToTrace" : "$_auth eq 'SAML' ? \"$_user\\@$_idpConfKey\" : \"$_user\""
    },
    "oidcServiceMetaDataCheckSessionURI" : "checksession",
    "facebookExportedVars" : {},
    "dbiAuthPassword" : "postgres",
    "oidcOPMetaDataExportedVars" : {
        "france-connect" : {
            "sn" : "family_name",
            "uid" : "sub",
            "mail" : "email",
            "displayName" : "preferred_username",
            "givenName" : "given_name",
            "cn" : "name"
        }
    },
    "registerDB" : "Demo",
    "dbiUserPassword" : "postgres",
    "dbiUserUser" : "postgres",
    "cfgDate" : 1475249704,
    "groups" : {},
    "samlSPMetaDataExportedAttributes" : null,
    "oidcOPMetaDataOptions" : {
        "france-connect" : {
            "oidcOPMetaDataOptionsCheckJWTSignature" : 1,
            "oidcOPMetaDataOptionsClientID" : "96c8e287074ce627a899c43557ae267cdc3a6004327afb56",
            "oidcOPMetaDataOptionsIDTokenMaxAge" : 30,
            "oidcOPMetaDataOptionsUseNonce" : 1,
            "oidcOPMetaDataOptionsScope" : "openid profile email",
            "oidcOPMetaDataOptionsTokenEndpointAuthMethod" : "client_secret_post",
            "oidcOPMetaDataOptionsDisplayName" : "France Connect",
            "oidcOPMetaDataOptionsDisplay" : "",
            "oidcOPMetaDataOptionsJWKSTimeout" : 0,
            "oidcOPMetaDataOptionsIcon" : "https://prismic-io.s3.amazonaws.com/franceconnect/04dca7f3632d590270dc86c51332b3d99ebca358_logo_franceconnect.png",
            "oidcOPMetaDataOptionsClientSecret" : "ab454b2e80486a5f3fd012f5878922d510d29cfb585bf205",
            "oidcOPMetaDataOptionsMaxAge" : 0
        }
    },
    "oidcRPCallbackGetParam" : "oidc_callback",
    "globalStorage" : "Lemonldap::NG::Common::Apache::Session::SOAP",
    "oidcOPMetaDataJSON" : {
        "france-connect" : "{\n\"issuer\": \"https://fcp.integ01.dev-franceconnect.fr\",\n\"authorization_endpoint\": \"https://fcp.integ01.dev-franceconnect.fr/api/v1/authorize\",\n\"token_endpoint\": \"https://fcp.integ01.dev-franceconnect.fr/api/v1/token\",\n\"userinfo_endpoint\": \"https://fcp.integ01.dev-franceconnect.fr/api/v1/userinfo\",\n\"end_session_endpoint\":\"https://fcp.integ01.dev-franceconnect.fr/api/v1/logout?force\"\n}"
    },
    "captcha_register_enabled" : 1,
    "registerUrl" : "https://auth.dev.guichet-entreprises.fr/register.pl",
    "dbiAuthPasswordHash" : "encrypt",
    "webIDExportedVars" : {},
    "authChoiceModules" : {
        "1_OpenID" : "OpenIDConnect;OpenIDConnect;Null",
        "1_DBI" : "DBI;DBI;DBI"
    },
    "samlSPMetaDataXML" : null,
    "localSessionStorage" : "Cache::FileCache",
    "mailOnPasswordChange" : 0,
    "oidcServiceMetaDataUserInfoURI" : "userinfo",
    "slaveExportedVars" : {},
    "notificationStorage" : "File",
    "oidcRPMetaDataOptions" : {},
    "exportedVars" : {
        "UA" : "HTTP_USER_AGENT"
    },
    "portalDisplayResetPassword" : "0",
    "dbiUserChain" : "dbi:Pg:dbname=postgres;host=db",
    "vhostOptions" : {
        "forms-admin.dev.guichet-partenaires.fr" : {
            "vhostPort" : -1,
            "vhostMaintenance" : 0,
            "vhostHttps" : -1
        }
    },
    "dbiAuthLoginCol" : "login",
    "globalStorageOptions" : {
        "proxy" : "http://auth.dev.guichet-entreprises.loc/index.pl/sessions"
    },
    "Soap" : "1",
    "dbiAuthUser" : "postgres",
    "authChoiceParam" : "lmAuth",
    "dbiAuthTable" : "sso_user",
    "timeout" : 72000,
    "captchaStorageOptions" : {},
    "portalUserAttr" : "_user",
    "persistentStorage" : "Apache::Session::File",
    "ldapExportedVars" : {},
    "samlSPMetaDataOptions" : null,
    "cfgLog" : "",
    "authentication" : "Choice",
    "logoutServices" : {},
    "oidcRPMetaDataExportedVars" : null,
    "issuerDBGetParameters" : {},
    "openIdExportedVars" : {},
    "useRedirectOnError" : 0,
    "oidcServiceMetaDataEndSessionURI" : "logout",
    "oidcServiceMetaDataAuthorizeURI" : "authorize",
    "key" : "!#^\"J2Px-GB^;k@z",
    "casAttributes" : {},
    "whatToTrace" : "_whatToTrace",
    "oidcServiceMetaDataIssuer" : "https://auth.dev.guichet-entreprises.fr",
    "dbiAuthPasswordCol" : "password",
    "cookieName" : "_llng",
    "dbiUserTable" : "sso_user",
    "portalAntiFrame" : 1,
    "samlIDPMetaDataExportedAttributes" : null,
    "localSessionStorageOptions" : {
        "default_expires_in" : 600,
        "cache_depth" : 3,
        "namespace" : "lemonldap-ng-sessions",
        "directory_umask" : "007",
        "cache_root" : "/tmp"
    },
    "googleExportedVars" : {},
    "oidcServiceMetaDataAuthnContext" : {},
    "post" : {
        "forms-admin.dev.guichet-partenaires.fr" : {}
    },
    "passwordDB" : "Choice",
    "useRedirectOnForbidden" : 0
}