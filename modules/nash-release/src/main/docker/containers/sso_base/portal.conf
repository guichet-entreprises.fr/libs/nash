#====================================================================
# Apache configuration for LemonLDAP::NG Portal
#====================================================================

PassEnv SSODOMAIN LOCALDOMAIN

<VirtualHost "*:443">
    ServerName auth.${SSODOMAIN}
    ServerAlias auth.${LOCALDOMAIN}

    ErrorLog /proc/self/fd/2
    CustomLog /proc/self/fd/1 common

    SSLEngine on
    SSLCertificateFile "/usr/local/apache2/certs/server.crt"
    SSLCertificateKeyFile "/usr/local/apache2/certs/server.key"

    # DocumentRoot
    DocumentRoot /var/lib/lemonldap-ng/portal/
    <Directory /var/lib/lemonldap-ng/portal/>
        <IfVersion >= 2.3>
            Require all granted
        </IfVersion>
        <IfVersion < 2.3>
            Order Deny,Allow
            Allow from all
        </IfVersion>
        Options +ExecCGI +FollowSymLinks
    </Directory>

    # Perl script
    <Files *.pl>
        SetHandler perl-script
        PerlResponseHandler ModPerl::Registry
    </Files>

    <IfModule mod_dir.c>
        DirectoryIndex index.pl index.html
    </IfModule>

    # SOAP functions for sessions management (disabled by default)
    <Location /index.pl/adminSessions>
        <IfVersion >= 2.3>
            Require all denied
        </IfVersion>
        <IfVersion < 2.3>
            Order Deny,Allow
            Deny from all
        </IfVersion>
    </Location>

    # SOAP functions for sessions access (disabled by default)
    <Location /index.pl/sessions>
        <IfVersion >= 2.3>
            Require all denied
        </IfVersion>
        <IfVersion < 2.3>
            Order Deny,Allow
            Deny from all
        </IfVersion>
    </Location>

    # SOAP functions for configuration access (disabled by default)
    <Location /index.pl/config>
        <IfVersion >= 2.3>
            Require all denied
        </IfVersion>
        <IfVersion < 2.3>
            Order Deny,Allow
            Deny from all
        </IfVersion>
    </Location>

    # SOAP functions for notification insertion (disabled by default)
    <Location /index.pl/notification>
        <IfVersion >= 2.3>
            Require all denied
        </IfVersion>
        <IfVersion < 2.3>
            Order Deny,Allow
            Deny from all
        </IfVersion>
    </Location>

    # SAML2 Issuer
    <IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteRule ^/saml/metadata /metadata.pl
        RewriteRule ^/saml/.* /index.pl
    </IfModule>

    # CAS Issuer
    <IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteRule ^/cas/.* /index.pl
    </IfModule>

    # OpenID Issuer
    <IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteRule ^/openidserver/.* /index.pl
    </IfModule>

    # OpenID Connect Issuer
    <IfModule mod_rewrite.c>
        RewriteEngine On
        #RewriteCond %{HTTP:Authorization} .
        #RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
        RewriteRule ^/oauth2/.* /index.pl
        RewriteRule ^/.well-known/openid-configuration$ /openid-configuration.pl
    </IfModule>

    # Get Issuer
    <IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteRule ^/get/.* /index.pl
    </IfModule>

    <Location />
        <IfModule mod_deflate.c>
                AddOutputFilterByType DEFLATE text/html text/plain text/xml text/javascript text/css
                SetOutputFilter DEFLATE
                BrowserMatch ^Mozilla/4 gzip-only-text/html
                BrowserMatch ^Mozilla/4\.0[678] no-gzip
                BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
                SetEnvIfNoCase Request_URI \.(?:gif|jpe?g|png)$ no-gzip dont-vary
        </IfModule>
        <IfModule mod_headers.c>
                Header append Vary User-Agent env=!dont-vary
        </IfModule>
    </Location>
    <Location /skins/>
        <IfModule mod_expires.c>
                ExpiresActive On
                ExpiresDefault "access plus 1 month"
        </IfModule>
    </Location>

    # Uncomment this if site if you use SSL only
    Header set Strict-Transport-Security 15768000
</VirtualHost>

##############################################
## Best performance under ModPerl::Registry ##
##############################################

# Uncomment this to increase performance of Portal:
<Perl>
    require Lemonldap::NG::Portal::SharedConf;
    Lemonldap::NG::Portal::SharedConf->compile(
        qw(delete header cache read_from_client cookie redirect unescapeHTML));
    # Uncomment this line if you use Lemonldap::NG menu
    #require Lemonldap::NG::Portal::Menu;
    # Uncomment this line if you use portal SOAP capabilities
    require SOAP::Lite;
</Perl>
