
translate() {
    env | grep '^APP_' | while read src ; do
        value=${src#*=}
        key=$( echo "${src%%=*}" | tr '[:upper:]' '[:lower:]' | sed -e 's/_/./g' )
        echo -n " -D${key}=\"${value}\""
    done
}

CATALINA_OPTS="${CATALINA_OPTS} $( translate )"

