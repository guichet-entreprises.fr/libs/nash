#!/bin/bash
#######################################################################################
# Nom du fichier  : artifact.sh
# Description     : shell de récupération d'un artifact depuis le repository maven
#
# Auteur   : Capgemini
# Date     : 2016-08-11
# Version  : 1.0.0
#######################################################################################
# 1.0.0 - 2016-08-11 - Capgemini : Creation
#######################################################################################


#######################################################################################
# Constantes
#######################################################################################

# Log level :
#   - 0 : FATAL
#   - 1 : ERROR
#   - 2 : WARN
#   - 3 : INFO
#   - 4 : DEBUG
declare -r -i LOG_LEVEL=4

declare -r SCRIPT_DIR=$( cd $( dirname "$0" ) ; pwd )
declare -r SCRIPT_NAME=$( basename "$0" )
declare -r MVN_SNAPSHOTS=http://GE_DEV:GE%404rchiva@svn.projet-ge.fr:8080/archiva/repository/snapshots
declare -r MVN_RELEASES=http://GE_DEV:GE%404rchiva@svn.projet-ge.fr:8080/archiva/repository/internal



#######################################################################################
# Parametres
#######################################################################################

declare APP_GROUP_ID=""
declare APP_ARTIFACT_ID=""
declare APP_VERSION=""
declare APP_CLASSIFIER=""
declare APP_TYPE="war"

#######################################################################################
# Fonctions utilitaires
#######################################################################################

log_msg() {
    local RES_COL=60
    local MOVE_TO_COL="echo -en \\033[${RES_COL}G"
    local SETCOLOR_SUCCESS="echo -en \\033[1;32m"
    local SETCOLOR_FAILURE="echo -en \\033[1;31m"
    local SETCOLOR_WARNING="echo -en \\033[1;33m"
    local SETCOLOR_NORMAL="echo -en \\033[0;39m"

    local type=$1
    shift

    echo -n $*
    $MOVE_TO_COL

    echo -n "["
    case "$type" in
        "success"|"success:"*)
            $SETCOLOR_SUCCESS
            if [ -z "${type##success:}" ] ; then
                echo -n "  OK  "
            else
                echo -n " ${type##success:} "
            fi
            ;;
        "failure"|"failure:"*)
            $SETCOLOR_FAILURE
            if [ -z "${type##failure:}" ] ; then
                echo -n "FAILED"
            else
                echo -n " ${type##failure:} "
            fi
            ;;
        "warning"|"warning:"*)
            $SETCOLOR_WARNING
            if [ -z "${type##warning:}" ] ; then
                echo -n "WARNING"
            else
                echo -n " ${type##warning:} "
            fi
            ;;
        *)
            echo -n " $type "
            ;;
    esac

    $SETCOLOR_NORMAL
    echo "]"
}

log_success() {
    log_msg success "$@"
}

log_failure() {
    log_msg failure "$@"
    exit 1
}

log_info() {
    if [ ${LOG_LEVEL} -ge 3 ] ; then
        echo "$*"
    fi
}

log_debug() {
    if [ ${LOG_LEVEL} -ge 4 ] ; then
        echo "$*"
    fi
}


#######################################################################################
# Usage
#######################################################################################

usage() {
    cat <<EOF
Usage : ${SCRIPT_NAME} [-options]
where options include :
    -h      print this help message
    -g <groupId>
            application maven group id
    -a <artifactId>
            application maven artifact id
    -v <version>
            application maven version
    -c <classifier>
            artifact classifier
    -t <type>
            artifact type, default "war"

EOF
}




while getopts ":g:a:v:h:" opt ; do
    case $opt in
        g)
            APP_GROUP_ID="${OPTARG}"
            ;;
        a)
            APP_ARTIFACT_ID="${OPTARG}"
            ;;
        v)
            APP_VERSION="${OPTARG}"
            ;;
        c)
            APP_CLASSIFIER="${OPTARG}"
            ;;
        t)
            APP_TYPE="${OPTARG}"
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "Option invalide : ${OPTARG}" >&2
            ;;
        :)
            echo "Option -${OPTARG} requiert un paramètre" >&2
            ;;
    esac
done


if [ -z "$APP_GROUP_ID" ] || [ -z "$APP_ARTIFACT_ID" ] || [ -z "$APP_VERSION" ] ; then
    usage
    exit 1
fi


log_msg "${APP_GROUP_ID}" "Group ID ..."
log_msg "${APP_ARTIFACT_ID}" "Artifact ID ..."
log_msg "${APP_VERSION}" "Version ..."
log_msg "${APP_CLASSIFIER}" "Classifier ..."
log_msg "${APP_TYPE}" "Type ..."


#######################################################################################
# Build maven URL and path
#
# Parameter(s) :
#   - APP_GROUP_ID
#   - APP_ARTIFACT_ID
#   - APP_VERSION
#   - MVN_RELEASES
#   - MVN_SNAPSHOTS
#
# Output :
#   - MVN_PATH_PREFIX : maven repository URL (release or snapshot)
#   - MVN_PATH_RELATIVE : maven artifact path, without version
#   - VERSION : application version, updated if "latest" or "release" are specified
#
#######################################################################################
function retrieveMavenPaths() {
    log_debug
    log_debug "*** Retrieving maven paths ***"

    MVN_PATH_PREFIX=""
    MVN_PATH_RELATIVE="$( echo ${APP_GROUP_ID} | tr . / )/${APP_ARTIFACT_ID}"
    VERSION=""

    if [ "${APP_VERSION}" == "latest" ] || [ "${APP_VERSION}" == "release" ] ; then

        . <( wget -q -O - "${MVN_RELEASES}/${MVN_PATH_RELATIVE}/maven-metadata.xml" | sed -ne 's/^.*<\([^>]\+\)>\([^<]\+\)<\/\1>.*$/\1ReleaseVersion=\2/gp' | grep '^\(latest\|release\|lastUpdated\)ReleaseVersion' )
        . <( wget -q -O - "${MVN_SNAPSHOTS}/${MVN_PATH_RELATIVE}/maven-metadata.xml" | sed -ne 's/^.*<\([^>]\+\)>\([^<]\+\)<\/\1>.*$/\1SnapshotVersion=\2/gp' | grep '^\(latest\|lastUpdated\)SnapshotVersion' )

        local VERSION_RELEASE=${releaseReleaseVersion}
        local VERSION_LATEST=${latestReleaseVersion}

        local MVN_PATH_LATEST_PREFIX=${MVN_RELEASES}

        if [ -z "${VERSION_LATEST}" ] ; then
            VERSION_LATEST=${latestSnapshotVersion}
            MVN_PATH_LATEST_PREFIX=${MVN_SNAPSHOTS}
        elif [ ! -z "${lastUpdatedSnapshotVersion}" ] && [ ${lastUpdatedSnapshotVersion} -gt ${lastUpdatedReleaseVersion} ] ; then
            VERSION_LATEST=${latestSnapshotVersion}
            MVN_PATH_LATEST_PREFIX=${MVN_SNAPSHOTS}
        fi


        case "${APP_VERSION}" in
            "latest")
                [ -z "${VERSION_LATEST}" ] && log_failure "Effective version ..."
                VERSION="${VERSION_LATEST}"
                MVN_PATH_PREFIX=${MVN_PATH_LATEST_PREFIX}
                ;;
            "release")
                [ -z "${VERSION_RELEASE}" ] && log_failure "Effective version ..."
                VERSION="${VERSION_RELEASE}"
                MVN_PATH_PREFIX=${MVN_RELEASES}
                ;;
            *)
                VERSION=${APP_VERSION}
                ;;
        esac

        log_msg "${VERSION}" "Effective version ..."
    else
        VERSION=${APP_VERSION}
        if [ "${VERSION#*-}" == "SNAPSHOT" ] ; then
            MVN_PATH_PREFIX=${MVN_SNAPSHOTS}
        else
            MVN_PATH_PREFIX=${MVN_RELEASES}
        fi
    fi

    log_debug "MVN_PATH_PREFIX=\"${MVN_PATH_PREFIX}\""
    log_debug "MVN_PATH_RELATIVE=\"${MVN_PATH_RELATIVE}\""
    log_debug "VERSION=\"${VERSION}\""
}


#######################################################################################
# Build artifact resource name
#
# Parameter(s) :
#   - MVN_PATH_PREFIX
#   - MVN_PATH_RELATIVE
#   - VERSION
#
# Output :
#   - ARTIFCAT_RESOURCE_NAME : default is "${APP_ARTIFACT_ID}-${VERSION}"
#
#######################################################################################
function retrieveArtifactResourceName() {
    log_debug
    log_debug "*** Retrieving artifact resource name ***"

    if [ "${VERSION#*-}" != "SNAPSHOT" ] ; then
        ARTIFACT_RESOURCE_NAME=${VERSION}
    else
        . <( wget -q -O - "${MVN_PATH_PREFIX}/${MVN_PATH_RELATIVE}/${VERSION}/maven-metadata.xml" | sed -ne 's/^.*<\([^>]\+\)>\([^<]\+\)<\/\1>.*$/snapshot_\1=\2/gp' )

        if [ ${PIPESTATUS[0]} -ne 0 ] || [ -z "${snapshot_timestamp}" ] || [ -z "${snapshot_buildNumber}" ] ; then
            log_failure "Retrieving artifact resource name ..."
        else
            ARTIFACT_RESOURCE_NAME="${VERSION%%-*}-${snapshot_timestamp}-${snapshot_buildNumber}"
        fi
    fi
    ARTIFACT_RESOURCE_NAME="${APP_ARTIFACT_ID}-${ARTIFACT_RESOURCE_NAME}"

    log_debug "ARTIFCAT_RESOURCE_NAME=\"${ARTIFACT_RESOURCE_NAME}\""
}



#######################################################################################
# Download artifact
#
# Parameter(s) :
#   - APP_ARTIFACT_ID
#   - VERSION
#   - ARTIFACT_RESOURCE_NAME
#   - MVN_PATH_PREFIX
#   - MVN_PATH_RELATIVE
#
# Output :
#   - ARTIFCAT_RESOURCE_NAME : default is "${APP_ARTIFACT_ID}-${VERSION}"
#
#######################################################################################
function download() {
    wget --progress=dot:mega -O "${APP_ARTIFACT_ID}.war" "${MVN_PATH_PREFIX}/${MVN_PATH_RELATIVE}/${VERSION}/${ARTIFACT_RESOURCE_NAME}.war"
    wget -O "${APP_ARTIFACT_ID}-config.zip" "${MVN_PATH_PREFIX}/${MVN_PATH_RELATIVE}/${VERSION}/${ARTIFACT_RESOURCE_NAME}-config.zip"
}



retrieveMavenPaths
retrieveArtifactResourceName
download
