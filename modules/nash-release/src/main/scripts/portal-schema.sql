CREATE EXTENSION pgcrypto;

CREATE OR REPLACE FUNCTION encrypt(t TEXT, s TEXT default NULL) RETURNS text AS $$
    SELECT crypt(t, COALESCE(s, gen_salt('bf')))
$$ LANGUAGE sql;

CREATE TABLE sso_user (
    login VARCHAR(20) NOT NULL,
    password VARCHAR(80) NOT NULL,
    email VARCHAR(200),
    display_name VARCHAR(200),
    PRIMARY KEY (login)
);
