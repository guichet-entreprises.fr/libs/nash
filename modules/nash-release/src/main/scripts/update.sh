#!/bin/bash

declare TAG=$( date +'%Y%m%d-%H%M%S' )
declare WORKDIR="/srv/build-${TAG}"
declare IMG_TO_BUILD="db, front, sso_base, sso_portal, sso_forge, app_forge, app_rest"

declare GROUP_ID="fr.ge.common.form-manager"
declare VERSION="latest"


declare HOST
declare SVN_USERNAME
declare SVN_PASSWORD

while getopts ":u:p:h:" opt ; do
    case $opt in
        u)
            SVN_USERNAME="${OPTARG}"
            ;;
        p)
            SVN_PASSWORD="${OPTARG}"
            ;;
        h)
            HOST="${OPTARG}"
            ;;
        \?)
            echo "Option invalide : ${OPTARG}" >&2
            ;;
        :)
            echo "Option -${OPTARG} requiert un paramètre" >&2
            ;;
    esac
done


[ -z "${SVN_USERNAME}" ] && read  -p 'SVN username : ' SVN_USERNAME
[ -z "${SVN_PASSWORD}" ] && read -sp 'SVN password : ' SVN_PASSWORD



################################################################################
# Building image
################################################################################
function buildImage() {
    local NAME="$1"
    local WAR="$2"
    local WORKDIR_CONTAINER="${WORKDIR}/release/src/main/docker/containers/${NAME}"
    shift 2

    cat <<EOF
################################################################################
# Build ${NAME} container
################################################################################
EOF

    [ ! -d "${WORKDIR_CONTAINER}" ] && return

    cd ${WORKDIR_CONTAINER}

    if [ ! -z "${WAR}" ] ; then
        ${WORKDIR}/release/src/main/scripts/artifact.sh -a ${WAR} -g ${GROUP_ID} -v ${VERSION}
    fi

    while [ ! -z "$1" ] ; do
        IFS=: read groupId artifactId version <<< "$1"
        if [ ! -z "${groupId}" ] && [ ! -z "${artifactId}" ] && [ ! -z "${version}" ] ; then
            wget -O ${artifactId}.jar https://repo1.maven.org/maven2/$( echo "${groupId}" | sed 's/\./\//g' )/${artifactId}/${version}/${artifactId}-${version}.jar
        fi
        shift
    done

    docker build --rm -t form-mgr/${NAME}:${TAG} . \
    && docker tag form-mgr/${NAME}:${TAG} form-mgr/${NAME}:latest
}


function build() {
    local NAME="$1"
    case "${NAME}" in 
        "app_forge")
            buildImage forge form-forge
            ;;
        "app_rest")
            buildImage rest form-rest-server org.postgresql:postgresql:9.4.1208
            ;;
        *)
            buildImage ${NAME}
            ;;
    esac
}


mkdir -p ${WORKDIR}
( cd ${WORKDIR} \
    && svn export --no-auth-cache --non-interactive --trust-server-cert --username ${SVN_USERNAME} --password ${SVN_PASSWORD} https://svn.projet-ge.fr/svn/trunk/form-manager/modules/form-release release \
)

if [ $? -ne 0 ] ; then
    exit 1
fi

for name in $( echo ${IMG_TO_BUILD} | sed 's/,/ /g' ) ; do
    build $name
done


################################################################################
# Stop running environment
################################################################################
( cd /srv \
    && [ -r "docker-compose.yml" ] \
    && docker-compose down \
)


################################################################################
# Update environment configuration
################################################################################
if [ -r "${WORKDIR}/release/src/main/docker/compose/${HOST}/docker-compose.yml" ] ; then
    cd /srv \
    && cp ${WORKDIR}/release/src/main/docker/compose/${HOST}/docker-compose.yml .
fi


docker-compose up -d

[ ! -z "${WORKDIR}" ] && rm -rf ${WORKDIR}
