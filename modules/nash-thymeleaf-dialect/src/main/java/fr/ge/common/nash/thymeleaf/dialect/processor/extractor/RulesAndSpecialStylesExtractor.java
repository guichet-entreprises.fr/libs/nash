/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.processor.extractor;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.engine.bean.TypedExpression;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.parser.ElementParser;
import fr.ge.common.nash.engine.manager.parser.IElementAction;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.script.NashScriptEngine;
import fr.ge.common.nash.thymeleaf.dialect.Constants;

/**
 * The Class RulesAndSpecialStylesExtractor.
 *
 * @author Christian Cougourdan
 */
public class RulesAndSpecialStylesExtractor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RulesAndSpecialStylesExtractor.class);

    /** The engine. */
    private final NashScriptEngine scriptEngine;

    /**
     * Instantiates a new rules and special styles extractor.
     *
     * @param loader
     *            the loader
     */
    public RulesAndSpecialStylesExtractor(final SpecificationLoader loader) {
        this.scriptEngine = loader.getScriptEngine();
    }

    /**
     * Extract. <code>
     *  {
     *      "rules": {
     *          "[id]": {
     *              "[display|trigger]": {@link TypedExpression},
     *              ...
     *          }
     *      },
     *      "styles": { ... }
     *  }
     * </code>
     *
     * @param group
     *            the group
     * @return the map
     */
    public Map<String, Object> extract(final GroupElement group) {
        final Map<String, Map<String, TypedExpression>> rules = new HashMap<>();
        final Map<String, Map<String, String>> styles = new HashMap<>();

        final IElementAction<String> rulesAction = (data, element, spec) -> {
            final String key = data + element.getId();
            Optional.ofNullable(this.buildConditionExpression(element)) //
                    .filter(m -> !m.isEmpty()) //
                    .ifPresent(m -> rules.put(key, m));
            return key + '.';
        };

        final IElementAction<String> stylesAction = (data, element, spec) -> {
            final String width = Optional.ofNullable(element) //
                    .filter(DataElement.class::isInstance).map(DataElement.class::cast) //
                    .map(DataElement::getWidth) //
                    .filter(StringUtils::isNotEmpty) //
                    .orElse(null);

            if (null != width) {
                final String key = data + element.getId();
                Map<String, String> elementStyles = styles.get(key);
                if (null == elementStyles) {
                    styles.put(key, elementStyles = new HashMap<>());
                }
                elementStyles.put("width", width);
            }

            return data;
        };

        ElementParser.create(rulesAction, rulesAction.then(stylesAction)).parse("", group);

        final Map<String, Object> model = new HashMap<>();
        model.put(Constants.MODEL_RULES, rules);
        model.put(Constants.MODEL_STYLES, styles);
        return model;
    }

    private Map<String, TypedExpression> buildConditionExpression(final IElement<?> elm) {

        final String elementType = TYPES_BY_CLASS.get(elm.getClass());
        if (null == elementType) {
            LOGGER.warn("unable to retrieve element type for {}", elm);
        } else {
            final Map<String, TypedExpression> exprByType = new HashMap<>();

            GETTERS_BY_TYPE.forEach((conditionType, accessor) -> {
                final String conditionScript = accessor.apply(elm);
                if (StringUtils.isNotEmpty(conditionScript)) {
                    exprByType.put( //
                            conditionType, //
                            new TypedExpression(elementType, this.scriptEngine.parse(conditionScript, true)) //
                    );
                }
            });

            return exprByType;
        }

        return null;
    }

    private static final Map<Class<?>, String> TYPES_BY_CLASS;

    static {
        final Map<Class<?>, String> m = new HashMap<>();

        m.put(DataElement.class, "field");
        m.put(GroupElement.class, "group");

        TYPES_BY_CLASS = Collections.unmodifiableMap(m);
    }

    private static final Map<String, Function<IElement<?>, String>> GETTERS_BY_TYPE;

    static {
        final Map<String, Function<IElement<?>, String>> m = new HashMap<>();

        m.put("display", IElement::getDisplayCondition);
        m.put("trigger", IElement::getTrigger);
        m.put("minOccurs", IElement::getMinOccurs);
        m.put("maxOccurs", IElement::getMaxOccurs);

        GETTERS_BY_TYPE = Collections.unmodifiableMap(m);
    }

}
