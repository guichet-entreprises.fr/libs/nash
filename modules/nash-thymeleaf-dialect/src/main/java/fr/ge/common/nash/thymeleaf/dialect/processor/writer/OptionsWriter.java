/**
 *
 */
package fr.ge.common.nash.thymeleaf.dialect.processor.writer;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.util.TypeDeclaration;
import fr.ge.common.utils.ResourceUtil;

/**
 * @author bsadil
 *
 */
public final class OptionsWriter {

    private static final ObjectWriter WRITER = new ObjectMapper() //
            .configure(Feature.ALLOW_SINGLE_QUOTES, true) //
            .configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true) //
            .writer();

    private OptionsWriter() {
        // Nothing to do
    }

    public static String parse(final SpecificationLoader loader, final String option) {

        if (option != null && StringUtils.contains(option, "$")) {
            final String processedUrl = ResourceUtil.buildUrl(option, loader.getConfiguration().asMap(), new Object[0]);
            return processedUrl;
        }
        return option;

    }

    public static String write(final TypeDeclaration type) throws JsonProcessingException {
        if (type.getOptions().isEmpty()) {
            return null;
        } else {
            return WRITER.writeValueAsString(type.getOptions());
        }
    }

}
