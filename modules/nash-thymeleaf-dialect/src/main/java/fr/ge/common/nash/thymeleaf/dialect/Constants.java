/**
 *
 */
package fr.ge.common.nash.thymeleaf.dialect;

import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.script.expression.ScriptExpression;

/**
 * Class Constants.
 *
 * @author Christian Cougourdan
 */
public final class Constants {

    /** Current record data specification as {@link FormSpecificationData}. */
    public static final String MODEL_FORM = "form";

    /** Current record group as {@link GroupElement}. */
    public static final String MODEL_GROUP = "group";

    /** All {@link FormSpecificationData} until current one. */
    public static final String MODEL_CONTEXT = "context";

    /** {@link SpecificationLoader} plugged on current record. */
    public static final String MODEL_LOADER = "loader";

    /**
     * Record data model extracted from context (all {@link FormSpecificationData}
     * until current one).
     */
    public static final String MODEL_DATA = "data";

    /**
     * Types as {@link String} referenced by data element id path.
     */
    public static final String MODEL_TYPES = "types";

    /**
     * Display conditions as {@link ScriptExpression} referenced by data element id
     * path.
     */
    public static final String MODEL_RULES = "rules";

    /**
     * Specific styles
     */
    public static final String MODEL_STYLES = "styles";

    /** Data element options hashcode. */
    public static final String MODEL_DATA_OPTIONS = "dataTypeOptions";

    /** Data element type as {@link IValueAdapter}}. */
    public static final String MODEL_DATA_TYPE = "type";

    /** Data element value. */
    public static final String MODEL_DATA_VALUE = "value";

    /** La constante MODEL_GROUP_CONTEXT. */
    public static final String MODEL_GROUP_CONTEXT = "node";

    /** La constante MODEL_PREVIOUS_PAGE. */
    public static final String MODEL_PREVIOUS_PAGE = "previousPage";

    /** La constante MODEL_PARENT_PREFIX. */
    public static final String MODEL_PARENT_PREFIX = "parentPrefix";

    /** La constante MODEL_PREFIX. */
    public static final String MODEL_PREFIX = "prefix";

    /** The Constant MODEL_PATH_AS_HTML_ID. */
    public static final String MODEL_PATH_AS_HTML_ID = "pathAsHtmlId";

    public static final String MODEL_RULE_KEY = "ruleKey";

    /** La constante MODEL_FRAGMENT. */
    public static final String MODEL_FRAGMENT = "fragment";

    /** La constante MODEL_FRAGMENT_ROOT. */
    public static final String MODEL_FRAGMENT_ROOT = "fragmentRoot";

    /** La constante MODEL_TARGET. */
    public static final String MODEL_TARGET = "target";

    /** La constante MODEL_OCCURRENCE_INDEX. */
    public static final String MODEL_OCCURRENCE_INDEX = "occurrenceIndex";

    /** La constante MODEL_OCCURRENCES_TOTAL. */
    public static final String MODEL_OCCURRENCES_TOTAL = "occurrencesTotal";

    /** La constante MODEL_SEVERAL_OCCURRENCES. */
    public static final String MODEL_SEVERAL_OCCURRENCES = "severalOccurrences";

    /** La constante MODEL_REPEATABLE. */
    public static final String MODEL_REPEATABLE = "repeatable";

    /** La constante MODEL_FIRST. */
    public static final String MODEL_FIRST = "first";

    /** La constante MODEL_LAST. */
    public static final String MODEL_LAST = "last";

    /** La constante MODEL_MIN_OCCURS. */
    public static final String MODEL_MIN_OCCURS = "minOccurs";

    /** La constante MODEL_MAX_OCCURS. */
    public static final String MODEL_MAX_OCCURS = "maxOccurs";

    /**
     * La constante "save-quit" pour les boutons de soumission de formulaire.
     */
    public static final String MODEL_BUTTON_SAVE = "saveQuit";

    /** La constante "next" pour les boutons de soumission de formulaire. */
    public static final String MODEL_BUTTON_NEXT = "nextPage";

    /** La constante "finish" pour les boutons de soumission de formulaire. */
    public static final String MODEL_BUTTON_FINISH = "nextStep";

    /** La constante "finalize" pour les boutons de soumission de formulaire. */
    public static final String MODEL_BUTTON_FINALIZE = "finalize";

    /** La constante "previous" pour les boutons de soumission de formulaire. */
    public static final String MODEL_BUTTON_PREVIOUS = "previous";

    /** Current step index model constant. */
    public static final String MODEL_STEP_INDEX = "idxStep";

    /** Current record data specification as {@link FormSpecificationData}. */
    public static final String MODEL_STEP = "step";

    /** Current page index model constant. */
    public static final String MODEL_PAGE_INDEX = "idxPage";

    /** Current record group as {@link GroupElement}. */
    public static final String MODEL_PAGE = "page";

    /**
     * Instantie un nouveau constants.
     */
    private Constants() {
        // Nothing to do
    }

}
