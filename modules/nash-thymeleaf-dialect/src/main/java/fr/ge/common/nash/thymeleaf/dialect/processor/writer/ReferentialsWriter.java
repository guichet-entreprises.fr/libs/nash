/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.processor.writer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialExternalElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialTextElement;
import fr.ge.common.nash.engine.referential.FieldProcessedReferentialsData;
import fr.ge.common.nash.engine.script.expression.ScriptExpression;
import fr.ge.common.utils.HashUtil;
import fr.ge.common.utils.ResourceUtil;

/**
 *
 * @author Christian Cougourdan
 */
public final class ReferentialsWriter {

    private ReferentialsWriter() {
        // Nothing to do
    }

    public static CharSequence write(final SpecificationLoader loader, final Map<String, FieldProcessedReferentialsData> fieldsProcessedReferentialsData) {
        final Map<String, String> conditionsAsJson = new HashMap<>();
        final String referentialsAsString = fieldsProcessedReferentialsData.entrySet().stream().map(entry -> {
            final String fieldId = entry.getKey();
            final String referentialsAsJson = entry.getValue().getTexts().stream().map(referentialTextElement -> {
                final String conditionHashes = write(loader, referentialTextElement, conditionsAsJson);

                return String.format("{id:\"%s\",label:\"%s\",icon:\"%s\",translation:%s,displayConditions:[%s]}", //
                        referentialTextElement.getId(), //
                        referentialTextElement.getLabel().replaceAll("[\\\\\"]", "\\\\$0").replace("\\", "\\\\"), //
                        referentialTextElement.getIcon(), //
                        Optional.ofNullable(referentialTextElement.getTranslation()).map(str -> '"' + str + '"').orElse(null), //
                        conditionHashes //
                );
            }).collect(Collectors.joining(","));
            final ReferentialExternalElement external = entry.getValue().getExternal();
            String externalAsString = null;
            if (external != null) {
                final String processedUrl = ResourceUtil.buildUrl(external.getUrl().getValue(), loader.getConfiguration().asMap(), new Object[0]);
                externalAsString = String.format("{url:{inputVarName:\"%s\",value:\"%s\"},base:\"%s\",id:\"%s\",label:\"%s\"}", //
                        external.getUrl().getInputVarName(), processedUrl, //
                        external.getBase(), external.getId(), external.getLabel().replaceAll("[\\\\\"]", "\\\\$0").replace("\\", "\\\\"));
            }
            return String.format("\"%s\":{values:[%s],external:%s}", fieldId, referentialsAsJson, externalAsString);
        }).collect(Collectors.joining(","));
        return String.format("{fields:{%s},displayConditions:{%s}}", //
                referentialsAsString, //
                conditionsAsJson.entrySet().stream().map(entry -> String.format("\"%s\":%s", entry.getKey(), entry.getValue())).collect(Collectors.joining(",")) //
        );
    }

    private static String write(final SpecificationLoader loader, final ReferentialTextElement referentialTextElement, final Map<String, String> conditionsAsJson) {
        String conditionHashes;
        if (null == referentialTextElement.getDisplayConditions()) {
            conditionHashes = "";
        } else {
            conditionHashes = referentialTextElement.getDisplayConditions().stream().map(displayConditionAsString -> {
                final String conditionHash = HashUtil.hashAsString(displayConditionAsString);
                if (!conditionsAsJson.containsKey(conditionHash)) {
                    final ScriptExpression displayCondition = loader.getScriptEngine().parse(displayConditionAsString, true);
                    conditionsAsJson.put(conditionHash, //
                            String.format("{dependsOn:[%s],predicate:function(){ return (%s); }}", //
                                    displayCondition.getDependencies().stream().map(dep -> '"' + dep + '"').collect(Collectors.joining(",")), //
                                    displayCondition.getExpression() //
                    ));
                }
                return '"' + conditionHash + '"';
            }).collect(Collectors.joining(","));
        }
        return conditionHashes;
    }

}
