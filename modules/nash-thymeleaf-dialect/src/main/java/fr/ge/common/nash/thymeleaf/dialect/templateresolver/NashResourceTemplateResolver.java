/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.templateresolver;

import java.util.Collections;
import java.util.Map;

import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templateresolver.AbstractConfigurableTemplateResolver;
import org.thymeleaf.templateresource.ITemplateResource;
import org.thymeleaf.util.Validate;

import fr.ge.common.nash.thymeleaf.dialect.NashDialect;
import fr.ge.common.nash.thymeleaf.dialect.templateresource.NashResourceTemplateResource;

/**
 * Class NashResourceTemplateResolver.
 *
 * @author Christian Cougourdan
 */
public class NashResourceTemplateResolver extends AbstractConfigurableTemplateResolver {

    /** La constante PREFIX. */
    private static final String PREFIX = "fm:";

    private static final String BASE_PATH = NashDialect.class.getPackage().getName().replace('.', '/');

    /**
     * Instantie un nouveau form resource template resolver.
     */
    public NashResourceTemplateResolver() {
        this.setResolvablePatterns(Collections.singleton(PREFIX + '*'));
        this.setTemplateAliases(Collections.singletonMap(PREFIX + "data", BASE_PATH + "/processor/data"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ITemplateResource computeTemplateResource(final IEngineConfiguration configuration, final String ownerTemplate, final String template, final String resourceName,
            final String characterEncoding, final Map<String, Object> templateResolutionAttributes) {

        return new NashResourceTemplateResource(resourceName, characterEncoding);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String computeResourceName(final IEngineConfiguration configuration, final String ownerTemplate, final String template, final String prefix, final String suffix,
            final Map<String, String> templateAliases, final Map<String, Object> templateResolutionAttributes) {

        Validate.notNull(template, "Template name cannot be null");

        String unaliasedName = templateAliases.get(template);
        if (unaliasedName == null) {
            unaliasedName = template;
        }

        if (unaliasedName.startsWith(PREFIX)) {
            unaliasedName = unaliasedName.substring(PREFIX.length());
        }

        return unaliasedName + ".html";
    }

}