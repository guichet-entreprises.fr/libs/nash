/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.exceptions.TemplateInputException;
import org.thymeleaf.standard.expression.Assignation;
import org.thymeleaf.standard.expression.AssignationSequence;
import org.thymeleaf.standard.expression.AssignationUtils;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.script.NashScriptEngine;
import fr.ge.common.nash.engine.script.ScriptExecutionContext;
import fr.ge.common.nash.engine.script.expression.translator.VariableScopeTranslator.Scopes;
import fr.ge.common.nash.thymeleaf.dialect.Constants;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class FragmentUtil.
 *
 * @author Christian Cougourdan
 */
public final class FragmentUtil {

    /**
     * Instantiates a new fragment util.
     */
    private FragmentUtil() {
        // Nothing to do
    }

    /**
     * Parses the value.
     *
     * @param <T>
     *            the generic type
     * @param context
     *            context
     * @param attributeValue
     *            the attribute value
     * @param expectedClass
     *            the expected class
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public static <T> T parseValue(final ITemplateContext context, final String attributeValue, final Class<T> expectedClass) {
        final IEngineConfiguration configuration = context.getConfiguration();
        final IStandardExpressionParser expressionParser = StandardExpressions.getExpressionParser(configuration);

        final IStandardExpression expression = expressionParser.parseExpression(context, attributeValue);

        final Object obj = expression.execute(context);

        if (null != obj && expectedClass.isAssignableFrom(obj.getClass())) {
            return (T) obj;
        } else {
            return null;
        }
    }

    /**
     * Parses the value.
     *
     * @param context
     *            context
     * @param attributeValue
     *            the attribute value
     * @return the object
     */
    public static Object parseValue(final ITemplateContext context, final String attributeValue) {
        return parseValue(context, attributeValue, Object.class);
    }

    /**
     * Parses the assignation.
     *
     * @param context
     *            context
     * @param attributeValue
     *            the attribute value
     * @param expected
     *            the expected
     * @return the map
     */
    public static Map<String, Object> parseAssignation(final ITemplateContext context, final String attributeValue, final Map<String, Class<?>> expected) {
        final Map<String, Object> model = new HashMap<>();

        final AssignationSequence parametersAsSeq = AssignationUtils.parseAssignationSequence(context, attributeValue, false);
        for (final Assignation assignation : parametersAsSeq) {
            final String key = (String) assignation.getLeft().execute(context);
            final Object value = assignation.getRight().execute(context);
            final Class<?> expectedClass = expected.get(key);

            if (value == null) {
                throw new TemplateInputException("Expecting value for parameter " + key);
            } else if (null == expectedClass || expectedClass.isAssignableFrom(value.getClass())) {
                model.put(key, value);
            } else {
                throw new TemplateInputException("Unexpected \"" + key + "\" parameter type : " + value.getClass().getName());
            }
        }

        return model;
    }

    public static Map<String, RepeatableStats> buildChildrenRepeatableStats(final ITemplateContext context, final GroupElement root) {
        final Map<String, RepeatableStats> model = new HashMap<>();

        Optional.ofNullable(root) //
                .map(GroupElement::getData) //
                .orElse(Collections.emptyList()) //
                .stream() //
                .filter(IElement::isRepeatable).forEach(elm -> {
                    final ElementPath path = ElementPath.create(elm.getPath());
                    RepeatableStats stats = model.get(path.getPath());

                    if (null == stats) {
                        stats = buildRepeatableStats(context, elm);
                    } else {
                        stats.incrementTotalCount();
                    }

                    model.put(path.getPath(), stats);
                });

        return model;
    }

    public static RepeatableStats buildRepeatableStats(final ITemplateContext context, final IElement<?> element) {
        final NashScriptEngine scriptEngine = ((SpecificationLoader) context.getVariable("specLoader")).getScriptEngine();
        final Map<String, Object> recordModel = CoreUtil.cast(context.getVariable(Constants.MODEL_CONTEXT));

        final ScriptExecutionContext ctx = new ScriptExecutionContext() //
                .add(Scopes.RECORD.toContextKey(), recordModel);

        final ElementPath path = ElementPath.create(element.getPath());
        final FormSpecificationData data = CoreUtil.cast(context.getVariable(Constants.MODEL_FORM));
        if (null != recordModel && null != data) {
            final Map<String, Object> stepModel = CoreUtil.cast(recordModel.get(data.getId()));
            ctx.add(Scopes.STEP.toContextKey(), stepModel);
            if (null != stepModel && null != path && StringUtils.isNotEmpty(path.getStepId())) {
                ctx.add(Scopes.PAGE.toContextKey(), stepModel.get(path.getStepId()));
            }

            return new RepeatableStats( //
                    Optional.ofNullable(element.getMinOccurs()).map(occurs -> (Number) scriptEngine.eval(occurs, true, ctx)).map(Number::intValue).orElse(0), //
                    Optional.ofNullable(element.getMaxOccurs()).map(occurs -> (Number) scriptEngine.eval(occurs, true, ctx)).map(Number::intValue).orElse(Integer.MAX_VALUE) //
            );
        } else {
            return new RepeatableStats(0, Integer.MAX_VALUE);
        }

    }

    public static class RepeatableStats {

        private final int minOccurs;

        private final int maxOccurs;

        private int index;

        private int totalCount = 1;

        @Override
        public String toString() {
            return String.format("{ minOccurs: %d, maxOccurs: %d, index: %d, totalCount: %d, first: %s, last: %s }", this.minOccurs, this.maxOccurs, this.index, this.totalCount, this.isFirst(),
                    this.isLast());
        }

        public RepeatableStats(final int minOccurs, final int maxOccurs) {
            this.minOccurs = minOccurs;
            this.maxOccurs = maxOccurs;
        }

        public int getMinOccurs() {
            return this.minOccurs;
        }

        public int getMaxOccurs() {
            return this.maxOccurs;
        }

        public int getIndex() {
            return this.index;
        }

        public void setIndex(final int index) {
            this.index = index;
        }

        public int getTotalCount() {
            return this.totalCount;
        }

        public void setTotalCount(final int totalCount) {
            this.totalCount = totalCount;
        }

        public int incrementTotalCount() {
            return ++this.totalCount;
        }

        public boolean isFirst() {
            return this.index <= 0;
        }

        public boolean isLast() {
            return this.index == (this.totalCount - 1);
        }

    }

}
