/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.processor;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.context.IEngineContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateInputException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.processor.AbstractStandardFragmentInsertionTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import fr.ge.common.nash.core.bean.ElementPath;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.thymeleaf.dialect.Constants;
import fr.ge.common.nash.thymeleaf.dialect.util.FragmentUtil;
import fr.ge.common.nash.thymeleaf.dialect.util.FragmentUtil.RepeatableStats;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class DataAttrProcessor.
 *
 * @author Christian Cougourdan
 */
public class DataAttrProcessor extends AbstractStandardFragmentInsertionTagProcessor {

    /** La constante MODEL_DATA. */
    private static final String MODEL_DATA = "data";

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DataAttrProcessor.class);

    /** The Constant ELEMENT_NAME. */
    public static final String ATTR_NAME = "data";

    /**
     * Default constructor.
     *
     * @param dialectPrefix
     *            dialect prefix
     */
    public DataAttrProcessor(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, ATTR_NAME, 0, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag, final AttributeName attributeName, final String attributeValue,
            final IElementTagStructureHandler structureHandler) {

        final Map<String, Object> localVariables = this.localVariables(context, attributeValue);
        ((IEngineContext) context).setVariables(localVariables);

        super.doProcess(context, tag, attributeName, this.fragment(localVariables), structureHandler);
    }

    /**
     * {@inheritDoc}
     */
    protected Map<String, Object> localVariables(final ITemplateContext context, final String attributeValue) {
        // handle exceptions
        final Object obj = FragmentUtil.parseValue(context, attributeValue);

        Optional.ofNullable(obj).orElseThrow(() -> new TemplateInputException("Form data element not defined"));
        Optional.ofNullable(obj).filter(IElement.class::isInstance).orElseThrow(() -> new TemplateInputException("Bad type of form data element"));

        final IElement<?> element = (IElement<?>) obj;
        final ElementPath elementPath = Optional.ofNullable(element.getPath()) //
                .map(ElementPath::create) //
                .orElseThrow(() -> new TemplateInputException("Form data element path is invalid"));

        final Map<String, Object> model = new HashMap<>();

        // convert the the raw context information
        final FormSpecificationData spec = Optional.ofNullable(context.getVariable(Constants.MODEL_FORM)) //
                .filter(FormSpecificationData.class::isInstance) //
                .map(FormSpecificationData.class::cast) //
                .orElse(null);

        final boolean isFragment = Optional.ofNullable(context.getVariable(Constants.MODEL_FRAGMENT)) //
                .filter(Boolean.class::isInstance) //
                .map(Boolean.class::cast) //
                .orElse(false);

        final String contextTarget = Optional.ofNullable(context.getVariable(Constants.MODEL_TARGET)) //
                .map(Object::toString) //
                .orElse(StringUtils.EMPTY);

        // is it a fragment context and is the current element the root ?
        final boolean isFragmentRoot = isFragment && element.getPath().replaceAll("\\[[^\\]]+\\]", "[]").equals(contextTarget.replaceAll("\\[[^\\]]+\\]", "[]"));
        model.put(Constants.MODEL_FRAGMENT, isFragment);
        model.put(Constants.MODEL_FRAGMENT_ROOT, isFragmentRoot);

        if (element.isRepeatable()) {
            RepeatableStats stats = (RepeatableStats) Optional.ofNullable(context.getVariable("repeatableStats")) //
                    .map(Map.class::cast) //
                    .map(map -> map.get(elementPath.getPath())) //
                    .orElse(null);

            if (null == stats) {
                /*
                 * Processing fragment, ie repeatable element is the root one
                 */
                final Map<String, Object> recordContext = CoreUtil.cast(context.getVariable("context"));
                ((SpecificationLoader) context.getVariable("specLoader")).elements().clear(spec, element, recordContext);

                element.setId(elementPath.getId() + "[0]");
                element.setPath(elementPath.getParentPath() + "." + element.getId());

                stats = FragmentUtil.buildRepeatableStats(context, element);
                stats.setTotalCount(Optional.ofNullable(context.getVariable(Constants.MODEL_OCCURRENCES_TOTAL)).map(Object::toString).map(Integer::valueOf).orElse(1));
            }

            stats.setIndex(elementPath.getIndex());

            model.put(Constants.MODEL_REPEATABLE, true);
            model.put("stats", stats);
        } else {
            model.put(Constants.MODEL_REPEATABLE, false);
            model.remove("stats");
        }

        // set the prefix and field type
        if (element instanceof DataElement) {
            model.putAll(this.dataSpecificLocalVariables(context, spec, element));
        } else if (element instanceof GroupElement) {
            model.putAll(this.groupSpecificLocalVariables(context, spec, element));
        }

        if (null != context.getVariable("specLoader")) {
            model.put("errorState", ((SpecificationLoader) context.getVariable("specLoader")).hasErrorState());
        } else {
            model.put("errorState", false);
        }

        model.put(MODEL_DATA, obj);

        if (!isFragmentRoot) {
            model.put("jsonValue", null);
        }
        return model;
    }

    private Map<String, Object> dataSpecificLocalVariables(final ITemplateContext context, final FormSpecificationData spec, final IElement<?> element) {
        final Map<String, Object> model = new HashMap<>();

        final DataElement dataElement = (DataElement) element;
        final String typeAsString = dataElement.getType();
        if (null != typeAsString) {
            final IValueAdapter<?> type = ValueAdapterFactory.type(spec, dataElement);
            model.put(Constants.MODEL_DATA_TYPE, type);
            if (type != null) {
                model.put(Constants.MODEL_DATA_VALUE, type.get(null, dataElement));
            }
        }

        final String parentPrefix = Optional.ofNullable(context.getVariable(Constants.MODEL_PREFIX)).map(Object::toString).orElse(StringUtils.EMPTY);
        model.putIfAbsent(Constants.MODEL_PREFIX, parentPrefix);
        model.put(Constants.MODEL_PATH_AS_HTML_ID, (parentPrefix + dataElement.getId()).replaceAll("[.\\]\\[]+", "-").replaceAll("-$", ""));
        model.put(Constants.MODEL_RULE_KEY, (parentPrefix + dataElement.getId()).replaceAll("\\[[^\\]]*\\]\\.?", "-0-").replaceAll("\\.", "-").replaceAll("-{2,}", "-").replaceAll("-$", ""));

        return model;
    }

    private Map<String, Object> groupSpecificLocalVariables(final ITemplateContext context, final FormSpecificationData spec, final IElement<?> element) {
        final Map<String, Object> model = new HashMap<>();

        final String parentPrefix = Optional.ofNullable(context.getVariable(Constants.MODEL_PREFIX)).map(Object::toString).orElse(StringUtils.EMPTY);
        final String groupPath = parentPrefix + element.getId();
        model.put(Constants.MODEL_PARENT_PREFIX, parentPrefix);
        model.put(Constants.MODEL_PREFIX, groupPath + '.');
        model.put(Constants.MODEL_PATH_AS_HTML_ID, groupPath.replaceAll("[.\\]\\[]+", "-").replaceAll("-$", ""));
        model.put(Constants.MODEL_RULE_KEY, groupPath.replaceAll("\\[[^\\]]*\\]\\.?", "-0-").replaceAll("\\.", "-").replaceAll("-{2,}", "-").replaceAll("-$", ""));
        model.put("repeatableStats", FragmentUtil.buildChildrenRepeatableStats(context, (GroupElement) element));
        model.put("children", element.getData());

        return model;
    }

    /**
     * Template.
     *
     * @param localVariables
     *            the local variables
     * @return the string
     */
    protected String fragment(final Map<String, Object> localVariables) {
        String templateName = null;

        final Object obj = localVariables.get(MODEL_DATA);

        if (null == obj) {
            throw new TechnicalException("no data presents");
        } else if (obj instanceof DataElement) {
            final DataElement dataElement = (DataElement) obj;
            final IValueAdapter<?> dataType = (IValueAdapter<?>) localVariables.get(Constants.MODEL_DATA_TYPE);

            if (null == dataType) {
                if (dataElement != null) {
                    LOGGER.warn("No data element type specified for '" + dataElement.getId() + "'");
                }
                templateName = PATH_TEMPLATE_BASE + "/no-type";
            } else {
                templateName = dataType.template();
            }
        } else if (obj instanceof GroupElement) {
            if (((GroupElement) obj).isRepeatable()) {
                templateName = PATH_TEMPLATE_BASE + "/group-many";
            } else {
                templateName = PATH_TEMPLATE_BASE + "/group";
            }
        } else {
            throw new TechnicalException("unknown element of type " + obj.getClass().getName());
        }

        return "fm:" + templateName;
    }

    private static final String PATH_TEMPLATE_BASE = DataAttrProcessor.class.getPackage().getName().replace('.', '/');

}
