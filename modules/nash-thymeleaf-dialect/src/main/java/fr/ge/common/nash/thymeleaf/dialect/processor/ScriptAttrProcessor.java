/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.processor;

import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.context.IEngineContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.DataModelManager;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.referential.FieldProcessedReferentialsData;
import fr.ge.common.nash.engine.referential.ReferentialReader;
import fr.ge.common.nash.engine.util.TypeDeclaration;
import fr.ge.common.nash.thymeleaf.dialect.Constants;
import fr.ge.common.nash.thymeleaf.dialect.processor.extractor.RulesAndSpecialStylesExtractor;
import fr.ge.common.nash.thymeleaf.dialect.processor.writer.OptionsWriter;
import fr.ge.common.nash.thymeleaf.dialect.processor.writer.ReferentialsWriter;
import fr.ge.common.nash.thymeleaf.dialect.processor.writer.RulesWriter;
import fr.ge.common.nash.thymeleaf.dialect.processor.writer.StylesWriter;
import fr.ge.common.nash.thymeleaf.dialect.util.FragmentUtil;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.ResourceUtil;

/**
 * The Class ScriptAttrProcessor.
 *
 * @author Christian Cougourdan
 */
public class ScriptAttrProcessor extends AbstractAttributeTagProcessor {

    /** The Constant ATTRIBUTE_NAME. */
    public static final String ATTR_NAME = "script";

    private static final String TEMPLATE_NAME = "script.html";

    /** The Constant EXPECTED_PARAMETER_CLASS. */
    private static final Map<String, Class<?>> EXPECTED_PARAMETER_CLASS;

    static {
        final Map<String, Class<?>> map = new HashMap<>();

        map.put(Constants.MODEL_FORM, FormSpecificationData.class);
        map.put(Constants.MODEL_GROUP, GroupElement.class);

        EXPECTED_PARAMETER_CLASS = Collections.unmodifiableMap(map);
    }

    /** The object mapper. */
    private final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Instantiates a new script attr processor.
     *
     * @param dialectPrefix
     *            dialect prefix
     */
    public ScriptAttrProcessor(final String dialectPrefix) {
        super(TemplateMode.HTML, dialectPrefix, null, false, ATTR_NAME, true, 650, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag, final AttributeName attributeName, final String attributeValue,
            final IElementTagStructureHandler structureHandler) {
        final Map<String, Object> localVariables = this.localVariables(context, attributeValue);

        final String template = this.fragment(localVariables);

        // localVariables.forEach((name, value) ->
        // structureHandler.setLocalVariable(name, value));
        ((IEngineContext) context).setVariables(localVariables);

        structureHandler.replaceWith(template, false);
    }

    /**
     * {@inheritDoc}
     */
    protected String fragment(final Map<String, Object> localVariables) {
        String template = ResourceUtil.resourceAsString(TEMPLATE_NAME, this.getClass());

        try {
            template = template.replace("[[${formAsJson}]]", Base64.getEncoder().encodeToString(this.objectMapper.writeValueAsBytes(localVariables.get(Constants.MODEL_DATA))));
        } catch (final JsonProcessingException ex) {
            throw new TemplateProcessingException("Error while serialize form data values", ex);
        }

        final Map<String, TypeDeclaration> typeByField = CoreUtil.cast(localVariables.get(Constants.MODEL_TYPES));

        final SpecificationLoader specLoader = (SpecificationLoader) localVariables.get(Constants.MODEL_LOADER);

        final FormSpecificationData spec = (FormSpecificationData) localVariables.get(Constants.MODEL_FORM);
        final GroupElement group = (GroupElement) localVariables.get(Constants.MODEL_GROUP);
        final String version = Optional.ofNullable(spec.getVersion()).orElse("latest");

        final Map<String, Object> values = new HashMap<>();
        values.put("root", localVariables.get("root"));
        values.put("form.id", spec.getId());
        values.put("version", version);
        values.put("group.id", StringUtils.defaultString(group.getId()));
        values.put("rules", RulesWriter.write(CoreUtil.cast(localVariables.get(Constants.MODEL_RULES))));
        values.put("styles", StylesWriter.write(CoreUtil.cast(localVariables.get(Constants.MODEL_STYLES))));

        if (specLoader != null) {
            if (specLoader.description() != null) {
                values.put("desc.lang", this.escape(specLoader.description().getLang()));
                values.put("desc.recordUid", this.escape(specLoader.description().getRecordUid()));
                values.put("desc.title", this.escape(specLoader.description().getTitle()));
                values.put("desc.description", this.escape(specLoader.description().getDescription()));
            }
            values.put("desc.currentStep", this.escape(String.valueOf(specLoader.currentStepPosition())));
            if (localVariables.get("stepElement") != null) {
                final StepElement displayedStep = (StepElement) localVariables.get("stepElement");
                values.put("desc.displayedStep", this.escape(String.valueOf(displayedStep.getPosition())));
            }
        }

        final Collection<String> valueAdaptersOptionsAsJson = new LinkedHashSet<>();
        for (final Entry<String, TypeDeclaration> entry : typeByField.entrySet()) {
            final TypeDeclaration type = entry.getValue();
            if (type.getOptionString() != null) {
                type.setOptionString(OptionsWriter.parse(specLoader, type.getOptionString()));
            }
            if (!type.getOptions().isEmpty()) {
                try {
                    valueAdaptersOptionsAsJson.add(String.format("\"%s\": %s", entry.getKey(), OptionsWriter.write(type)));
                } catch (final JsonProcessingException ex) {
                    throw new TemplateProcessingException("Error while serialize data type options", ex);
                }
            }
        }

        final Map<String, FieldProcessedReferentialsData> fieldsProcessedReferentialsData = new ReferentialReader().read(specLoader, spec, group);

        values.put("valueAdaptersOptionsAsJson", this.escape(String.join(", ", valueAdaptersOptionsAsJson)));
        values.put("referentials", ReferentialsWriter.write(specLoader, fieldsProcessedReferentialsData));

        return CoreUtil.searchAndReplace(template, "(?:\\[\\[\\$\\{)([a-zA-Z0-9_.]+)(?:\\}\\]\\])", m -> {
            return Optional.ofNullable(values.get(m.group(1))).map(str -> str.toString().replace("$", "\\$")).orElse(null);
        });

    }

    private String escape(final CharSequence value) {
        return (null == value ? "" : value.toString()).replaceAll("\\\\", "\\\\\\\\").replaceAll("(?<![\\\\])'", "\\\\\\\\'");
    }

    private <R> R expect(final Map<String, Object> model, final String propertyName, final Class<R> expectedClass) {
        final Object propertyValue = model.get(propertyName);
        if (null == propertyValue) {
            throw new TechnicalException("Expected property " + propertyName + " not specified");
        } else if (expectedClass.isInstance(propertyValue)) {
            return CoreUtil.cast(propertyValue);
        } else {
            throw new TechnicalException("Unexpected \"" + propertyName + "\" property type : having " + propertyValue.getClass().getSimpleName() + " expected " + expectedClass.getSimpleName());
        }
    }

    /**
     * {@inheritDoc}
     */
    protected Map<String, Object> localVariables(final ITemplateContext context, final String attributeValue) {
        final Map<String, Object> model = FragmentUtil.parseAssignation(context, attributeValue, EXPECTED_PARAMETER_CLASS);

        final SpecificationLoader loader = this.expect(model, Constants.MODEL_LOADER, SpecificationLoader.class);
        final FormSpecificationData spec = this.expect(model, Constants.MODEL_FORM, FormSpecificationData.class);
        final GroupElement group = this.expect(model, Constants.MODEL_GROUP, GroupElement.class);
        final Map<String, TypeDeclaration> types = DataModelManager.types(spec, group);

        final Map<String, Object> dataContext = model.containsKey(Constants.MODEL_CONTEXT) ? CoreUtil.cast(model.get(Constants.MODEL_CONTEXT)) : Collections.emptyMap();

        final Map<String, Object> data = new HashMap<>();
        dataContext.forEach((key, value) -> data.put(key, value));
        RecursiveDataModelExtractor.create(null).extract(spec).forEach((key, value) -> data.put(key, value));

        model.putAll(this.registerRulesAndSpecialStyles(loader, group));

        model.put(Constants.MODEL_DATA, data);
        model.put(Constants.MODEL_TYPES, types);
        model.put("root", FragmentUtil.parseValue(context, "@{/}"));

        return model;
    }

    private Map<String, Object> registerRulesAndSpecialStyles(final SpecificationLoader loader, final GroupElement group) {
        return new RulesAndSpecialStylesExtractor(loader).extract(group);
    }

}
