/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.thymeleaf.dialect.IProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import fr.ge.common.nash.engine.support.thymeleaf.TypeAttrProcessor;
import fr.ge.common.nash.thymeleaf.dialect.processor.DataAttrProcessor;
import fr.ge.common.nash.thymeleaf.dialect.processor.MarkdownAttrProcessor;
import fr.ge.common.nash.thymeleaf.dialect.processor.PageAttrProcessor;
import fr.ge.common.nash.thymeleaf.dialect.processor.ScriptAttrProcessor;

/**
 * The Class NashDialect.
 *
 * @author Christian Cougourdan
 */
public class NashDialect implements IProcessorDialect {

    /** La constante DIALECT_NAME. */
    public static final String DIALECT_NAME = "Form Thymeleaf Dialect";

    /** La constante DIALECT_PREFIX. */
    public static final String DIALECT_PREFIX = "form";

    /** Engine properties. */
    @Deprecated
    private Properties engineProperties;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return DIALECT_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPrefix() {
        return DIALECT_PREFIX;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDialectProcessorPrecedence() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<IProcessor> getProcessors(final String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<>();

        processors.add(new PageAttrProcessor(dialectPrefix));
        processors.add(new DataAttrProcessor(dialectPrefix));
        processors.add(new ScriptAttrProcessor(dialectPrefix));
        processors.add(new MarkdownAttrProcessor(TemplateMode.HTML, dialectPrefix));
        processors.add(new TypeAttrProcessor(dialectPrefix));

        return processors;
    }

    /**
     * Sets the engine properties.
     *
     * @param engineProperties
     *            the new engine properties
     */
    @Deprecated
    public void setEngineProperties(final Properties engineProperties) {
        this.engineProperties = new Properties(engineProperties);
    }

}
