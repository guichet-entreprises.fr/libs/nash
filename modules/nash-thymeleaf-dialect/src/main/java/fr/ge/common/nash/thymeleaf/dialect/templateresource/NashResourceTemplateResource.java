/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.templateresource;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import org.thymeleaf.templateresource.ITemplateResource;
import org.thymeleaf.util.StringUtils;

/**
 * Class NashResourceTemplateResource.
 *
 * @author Christian Cougourdan
 */
public class NashResourceTemplateResource implements ITemplateResource {

    /** path. */
    private final String path;

    /** character encoding. */
    private final String characterEncoding;

    /** class loader. */
    private final ClassLoader classLoader;

    /**
     * Instantie un nouveau form resource template resource.
     *
     * @param path
     *            path
     * @param characterEncoding
     *            character encoding
     */
    public NashResourceTemplateResource(final String path, final String characterEncoding) {
        this.path = path;
        this.characterEncoding = characterEncoding;
        this.classLoader = Thread.currentThread().getContextClassLoader();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return this.path;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getBaseName() {
        return this.path;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exists() {
        return this.classLoader.getResource(this.path) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Reader reader() throws IOException {
        final InputStream inputStream = this.classLoader.getResourceAsStream(this.path);
        if (inputStream == null) {
            throw new FileNotFoundException(String.format("ClassLoader resource \"%s\" does not exist", this.path));
        }

        if (!StringUtils.isEmptyOrWhitespace(this.characterEncoding)) {
            return new BufferedReader(new InputStreamReader(new BufferedInputStream(inputStream), this.characterEncoding));
        }

        return new BufferedReader(new InputStreamReader(new BufferedInputStream(inputStream), StandardCharsets.UTF_8));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ITemplateResource relative(final String relativeLocation) {
        return null;
    }

}
