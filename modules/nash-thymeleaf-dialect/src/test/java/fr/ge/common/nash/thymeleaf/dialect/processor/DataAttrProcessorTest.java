/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.processor;

import org.junit.Test;
import org.thymeleaf.testing.templateengine.engine.AbstractThymeleafTest;

/**
 * The Class DataAttrProcessorTest.
 *
 * @author Christian Cougourdan
 */
public class DataAttrProcessorTest extends AbstractThymeleafTest {

    /**
     * Test simple.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSimple() throws Exception {
        this.test("simple.thtest");
    }

    /**
     * Test mandatory.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMandatory() throws Exception {
        this.test("mandatory.thtest");
    }

    /**
     * Test help.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testHelp() throws Exception {
        this.test("help.thtest");
    }

    /**
     * Test help empty.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testHelpEmpty() throws Exception {
        this.test("help-empty.thtest");
    }

    /**
     * Test hide.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testHide() throws Exception {
        this.test("hide.thtest");
    }

    /**
     * Test desc help warning.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDescHelpWarning() throws Exception {
        this.test("desc-help-warning.thtest");
    }

    /**
     * Test warning no label.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testWarningNoLabel() throws Exception {
        this.test("warning-no-label.thtest");
    }

    /**
     * Test group.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGroup() throws Exception {
        this.test("group.thtest");
    }

    /**
     * Test group multiple.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGroupMultiple() throws Exception {
        this.test("group-multiple.thtest");
    }

    @Test
    public void testGroupFoldYes() throws Exception {
        this.test("group-fold-yes.thtest");
    }

    @Test
    public void testGroupFoldNo() throws Exception {
        this.test("group-fold-no.thtest");
    }

    /**
     * Test null type.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testNullType() throws Exception {
        this.test("null-type.thtest");
    }

    /**
     * Test unknown type.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUnknownType() throws Exception {
        this.test("unknown-type.thtest");
    }

    /**
     * Test enhanced description.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testEnhancedDescription() throws Exception {
        this.test("enhanced-desc.thtest");
    }

}
