/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.thymeleaf.dialect.processor;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.standard.StandardDialect;
import org.thymeleaf.templateresolver.AbstractConfigurableTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresource.ITemplateResource;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.support.thymeleaf.NashMessageResolver;
import fr.ge.common.nash.test.AbstractBeanResourceTest;
import fr.ge.common.nash.thymeleaf.dialect.NashDialect;
import fr.ge.common.nash.thymeleaf.dialect.templateresolver.NashResourceTemplateResolver;
import fr.ge.common.nash.thymeleaf.dialect.templateresource.NashResourceTemplateResource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "/spring/test-context.xml" })
public abstract class AbstractThymeleafTest extends AbstractBeanResourceTest {

    @Autowired
    private ApplicationContext applicationContext;

    protected static class TestResourceTemplateResolver extends AbstractConfigurableTemplateResolver {

        public TestResourceTemplateResolver(final Object target) {
            this.setPrefix(target.getClass().getName().replaceAll("[.]", "/") + "-");
        }

        @Override
        protected ITemplateResource computeTemplateResource(final IEngineConfiguration configuration, final String ownerTemplate, final String template, final String resourceName,
                final String characterEncoding, final Map<String, Object> templateResolutionAttributes) {

            return new NashResourceTemplateResource(resourceName + "/template.html", characterEncoding);
        }

    }

    public SpecificationLoader loader(final String dataResource) throws Exception {
        return this.loader("description.xml", dataResource);
    }

    public SpecificationLoader loader(final String descriptionResource, final String dataResource) throws Exception {
        final IProvider provider = spy(IProvider.class);
        when(provider.asBytes(descriptionResource)).thenReturn(this.resourceAsBytes("/description.xml"));
        when(provider.asBytes("step01.xml")).thenReturn(this.resourceAsBytes(dataResource));

        return this.applicationContext.getBean(SpecificationLoader.class, this.getConfiguration(), provider);
    }

    protected Configuration getConfiguration() {
        return new Configuration(new Properties());
    }

    public TemplateEngine engine() throws Exception {
        final NashResourceTemplateResolver nashResourceTemplateResolver = new NashResourceTemplateResolver();
        nashResourceTemplateResolver.setSuffix(".html");

        final ITemplateResolver testResourceTemplateResolver = new TestResourceTemplateResolver(this);

        final TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolvers(new HashSet<>(Arrays.asList(new ITemplateResolver[] { nashResourceTemplateResolver, testResourceTemplateResolver })));
        templateEngine.setMessageResolvers(new HashSet<>(Arrays.asList(/* new TestEngineMessageResolver(), */ new NashMessageResolver())));
        templateEngine.setDialects(new HashSet<>(Arrays.asList(new StandardDialect(), new NashDialect())));

        return templateEngine;
    }

}