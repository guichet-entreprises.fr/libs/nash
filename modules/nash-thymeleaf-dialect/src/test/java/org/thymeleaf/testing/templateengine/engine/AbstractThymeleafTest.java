/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.thymeleaf.testing.templateengine.engine;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Properties;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.testing.templateengine.exception.TestEngineExecutionException;
import org.thymeleaf.testing.templateengine.resource.ITestResource;
import org.thymeleaf.testing.templateengine.testable.ITest;
import org.thymeleaf.testing.templateengine.testable.ITestable;

import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.utils.test.AbstractTest;

/**
 * The Class AbstractThymeleafTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml", "classpath:spring/engine-context.xml" })
public abstract class AbstractThymeleafTest extends AbstractTest {

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Test.
     *
     * @param input
     *            the input
     * @return the test executor
     * @throws Exception
     *             the exception
     */
    protected TestExecutor test(final String input) throws Exception {
        final TestExecutor executor = this.execute(input);

        Assert.assertTrue(executor.isAllOK());

        return executor;
    }

    /**
     * Execute.
     *
     * @param input
     *            the input
     * @return the test executor
     * @throws Exception
     *             the exception
     */
    protected TestExecutor execute(final String input) throws Exception {
        final TestExecutor executor = new TestExecutor();

        final String testableName = this.getClass().getResource(this.getClass().getSimpleName() + "Resources/" + input).toString();
        final TestExecutionContext context = new TestExecutionContext();
        final String executionId = context.getExecutionId();

        TestExecutor.setThreadExecutionId(executionId);

        try {

            final ITestable testable = executor.getTestableResolver().resolve(executionId, testableName);
            if (testable == null) {
                throw new TestEngineExecutionException("Resource \"" + testableName + "\" could not be resolved.");
            }

            if (testable instanceof ITest) {
                final Map<String, ITestResource> inputs = ((ITest) testable).getAdditionalInputs();
                ((ITest) testable).getContext().getVariables().put("specLoader", (ctx, locale) -> this.loader());
            }

            context.setTemplateEngine(this.engine());

            executor.getReporter().executionStart(context.getExecutionId());

            final TestExecutionResult result = executor.executeTestable(testable, context);

            executor.getReporter().executionEnd(context.getExecutionId(), result.getTotalTestsOk(), result.getTotalTestsExecuted(), result.getTotalTimeNanos());

        } catch (final TestEngineExecutionException e) {
            throw e;
        } catch (final Exception e) {
            throw new TestEngineExecutionException("Error executing testable \"" + testableName + "\"", e);
        }

        return executor;
    }

    /**
     * Engine.
     *
     * @return template engine
     */
    protected TemplateEngine engine() {
        return this.templateEngine;
    }

    protected Configuration getConfiguration() {
        return new Configuration(new Properties());
    }

    protected SpecificationLoader loader() {
        final IProvider provider = spy(IProvider.class);
        when(provider.asBytes("description.xml")).thenReturn(this.resourceAsBytes("/description.xml"));
        return this.applicationContext.getBean(SpecificationLoader.class, this.getConfiguration(), provider);
    }

}
