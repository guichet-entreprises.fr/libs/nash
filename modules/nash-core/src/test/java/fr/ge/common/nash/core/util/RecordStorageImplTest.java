/**
 *
 */
package fr.ge.common.nash.core.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.utils.test.AbstractTest;

/**
 * Class RecordStorageImplTest.
 *
 * @author bsadil
 */
public class RecordStorageImplTest extends AbstractTest {

    private static final Path BASEPATH = Paths.get("target", "records");

    @Before
    public void setUp() throws Exception {
        this.remove(BASEPATH.toString());
    }

    /**
     * Store record test.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void storeRecordTest() throws Exception {
        final String code = "2016-12-AWS-QPS-30";
        final byte[] resourceAsBytes = this.resourceAsBytes("core.zip");
        final InputStream stream = new ByteArrayInputStream(resourceAsBytes);

        final RecordStorageImpl recordStorage = new RecordStorageImpl();
        recordStorage.setRootStorage(BASEPATH.toString());
        recordStorage.storeRecord(code, stream, "R&D/Recette");

        final Path baseRecordPath = this.getRecordBasePath();
        Assert.assertEquals(baseRecordPath.toFile().exists(), true);

        final File fileZip = baseRecordPath.resolve(code + ".zip").toFile();
        Assert.assertEquals(fileZip.exists(), true);

        final File fileSha1 = baseRecordPath.resolve(code + ".sha1").toFile();
        Assert.assertEquals(fileSha1.exists(), true);

    }

    /**
     * File already exist test.
     *
     * @throws TemporaryException
     *             temporary exception
     */
    @Test(expected = TemporaryException.class)
    public void fileAlreadyExistTest() throws Exception {
        final String code = "2016-12-AWS-QPS-30";

        final Path basePath = this.getRecordBasePath();
        basePath.toFile().mkdirs();
        Files.write(basePath.resolve(code + ".zip"), "...".getBytes(StandardCharsets.UTF_8));

        final byte[] resourceAsBytes = this.resourceAsBytes("core.zip");
        final InputStream stream = new ByteArrayInputStream(resourceAsBytes);

        final RecordStorageImpl recordStorage = new RecordStorageImpl();
        recordStorage.setRootStorage(BASEPATH.toString());
        recordStorage.storeRecord(code, stream, "R&D/Recette");
    }

    /**
     * Store record permission test.
     *
     * @throws Exception
     *             exception
     */
    @Ignore
    @Test(expected = TemporaryException.class)
    public void storeRecordPermissionTest() throws Exception {
        final String code = "2016-12-AWS-QPS-36";
        final byte[] resourceAsBytes = this.resourceAsBytes("core.zip");
        final InputStream stream = new ByteArrayInputStream(resourceAsBytes);
        // final String timeStamp = new
        // SimpleDateFormat("yyyy-MM").format(Calendar.getInstance().getTime());
        // final String timeStamp1 = new
        // SimpleDateFormat("dd").format(Calendar.getInstance().getTime());
        // final File absoluteDirectory = new File("jar:jfr.jar!target/records/"
        // + timeStamp + "/" + timeStamp1 + "/recette");
        final File absoluteDirectory = new File("target/jfr.jar");
        absoluteDirectory.createNewFile();
        absoluteDirectory.setExecutable(false);
        absoluteDirectory.setReadable(false);
        absoluteDirectory.setWritable(false);

        // if (!absoluteDirectory.exists()) {
        // absoluteDirectory.mkdirs();
        // }

        final RecordStorageImpl recordStorage = new RecordStorageImpl();
        recordStorage.setRootStorage("jar:target/jfr.jar!records/");

        try {
            recordStorage.storeRecord(code, stream, "recette");
        } finally {
            absoluteDirectory.setWritable(true);
            absoluteDirectory.delete();
        }
    }

    private Path getRecordBasePath() {
        return BASEPATH.resolve(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM/dd"))).resolve("R&D/Recette");
    }

}
