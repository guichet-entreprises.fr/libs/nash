/**
 * 
 */
package fr.ge.common.nash.core.bean;

import org.junit.Assert;
import org.junit.Test;

import fr.ge.common.nash.core.bean.RecordStatusEnum;

/**
 * Class RecordStatusEnumTest.
 *
 * @author bsadil
 */
public class RecordStatusEnumTest {

    /**
     * Testget status.
     */
    @Test
    public void testgetStatus() {
        Assert.assertEquals("finalize", RecordStatusEnum.FINALIZE.getStatus());
        Assert.assertEquals("in_progress", RecordStatusEnum.IN_PROGRESS.getStatus());
    }

    /**
     * Testget etat from id.
     */
    @Test
    public void testgetEtatFromId() {
        Assert.assertEquals(RecordStatusEnum.FINALIZE, RecordStatusEnum.getStatusFromId(RecordStatusEnum.FINALIZE.getStatus()));
        Assert.assertEquals(null, RecordStatusEnum.getStatusFromId("test"));

    }

}
