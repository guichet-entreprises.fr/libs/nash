/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.core.writer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.xml.stream.XMLStreamWriter;

import org.junit.Test;

import fr.ge.common.nash.core.writer.EmptyNamespaceWriter;

/**
 * Class EmptyNamespaceWriterTest.
 *
 * @author Christian Cougourdan
 */
public class EmptyNamespaceWriterTest {

    /**
     * Test simple.
     *
     * @throws Exception          exception
     */
    @Test
    public void testSimple() throws Exception {
        final XMLStreamWriter streamWriter = mock(XMLStreamWriter.class);
        final EmptyNamespaceWriter writer = new EmptyNamespaceWriter(streamWriter);

        writer.writeStartElement("localName");
        writer.writeStartElement("namespaceURI", "localName");
        writer.writeStartElement("prefix", "localName", "namespaceURI");
        verify(streamWriter).writeStartElement("localName");
        verify(streamWriter).writeStartElement("namespaceURI", "localName");
        verify(streamWriter).writeStartElement("", "localName", "namespaceURI");

        writer.writeEmptyElement("localName");
        writer.writeEmptyElement("namespaceURI", "localName");
        writer.writeEmptyElement("prefix", "localName", "namespaceURI");
        verify(streamWriter).writeEmptyElement("localName");
        verify(streamWriter).writeEmptyElement("namespaceURI", "localName");
        verify(streamWriter).writeEmptyElement("prefix", "localName", "namespaceURI");

        writer.writeStartDocument();
        writer.writeStartDocument("version");
        writer.writeStartDocument("encoding", "version");
        verify(streamWriter).writeStartDocument();
        verify(streamWriter).writeStartDocument("version");
        verify(streamWriter).writeStartDocument("encoding", "version");

        writer.writeEndElement();
        writer.writeEndDocument();
        verify(streamWriter).writeEndElement();
        verify(streamWriter).writeEndDocument();

        writer.close();
        writer.flush();
        verify(streamWriter).close();
        verify(streamWriter).flush();

        writer.writeAttribute("localName", "value");
        writer.writeAttribute("namespaceUri", "localName", "value");
        writer.writeAttribute("prefix", "namespaceUri", "localName", "value");
        verify(streamWriter).writeAttribute("localName", "value");
        verify(streamWriter).writeAttribute("namespaceUri", "localName", "value");
        verify(streamWriter).writeAttribute("prefix", "namespaceUri", "localName", "value");

        writer.writeNamespace("prefix", "namespaceURI");
        writer.writeDefaultNamespace("namespaceURI");
        writer.writeComment("data");
        writer.writeProcessingInstruction("target");
        writer.writeProcessingInstruction("target", "data");
        writer.writeCData("data");
        writer.writeDTD("dtd");
        writer.writeEntityRef("name");
        verify(streamWriter, times(0)).writeNamespace("prefix", "namespaceURI");
        verify(streamWriter, times(0)).writeDefaultNamespace("namespaceURI");
        verify(streamWriter).writeComment("data");
        verify(streamWriter).writeProcessingInstruction("target");
        verify(streamWriter).writeProcessingInstruction("target", "data");
        verify(streamWriter).writeCData("data");
        verify(streamWriter).writeDTD("dtd");
        verify(streamWriter).writeEntityRef("name");

        writer.writeCharacters("text");
        writer.writeCharacters("text".toCharArray(), 42, 69);
        writer.getPrefix("uri");
        writer.setPrefix("prefix", "uri");
        writer.setDefaultNamespace("uri");
        writer.getProperty("name");
        verify(streamWriter).writeCharacters("text");
        verify(streamWriter).writeCharacters("text".toCharArray(), 42, 69);
        verify(streamWriter).getPrefix("uri");
        verify(streamWriter).setPrefix("prefix", "uri");
        verify(streamWriter).setDefaultNamespace("uri");
        verify(streamWriter).getProperty("name");
    }

}
