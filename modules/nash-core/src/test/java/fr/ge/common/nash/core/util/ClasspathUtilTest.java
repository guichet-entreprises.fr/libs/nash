/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.core.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.dozer.DozerBeanMapper;
import org.junit.Test;

/**
 * Tests {@link ClasspathUtil}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class ClasspathUtilTest {

    /**
     * Tests {@link ClasspathUtil#getClassesFromPackage(String, Class)}.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetClassesFromPackage() throws Exception {
        final List<String> actual = ClasspathUtil.getClassesFromPackage("fr.ge.common.nash.core.excep", ClasspathUtil.class);
        // for (final String className : actual) {
        // System.out.println("*** testGetClassesFromPackage : " + className);
        // }
        assertThat(actual, //
                hasItems("fr.ge.common.nash.core.exception.FunctionalException", //
                        "fr.ge.common.nash.core.exception.RecordNotFoundException", //
                        "fr.ge.common.nash.core.exception.TechnicalException", //
                        "fr.ge.common.nash.core.exception.TemporaryException", //
                        "fr.ge.common.nash.core.exception.ExpressionException" //
                ) //
        );
        assertThat(actual, hasSize(16));
    }

    /**
     * Tests {@link ClasspathUtil#getClassesFromPackage(String, Class)}.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetClassesFromPackageOutsideEmpty() throws Exception {
        final List<String> actual = ClasspathUtil.getClassesFromPackage("org.dozer.converters", ClasspathUtil.class);
        // for (final String className : actual) {
        // System.out.println("*** testGetClassesFromPackageOutsideEmpty : " +
        // className);
        // }
        assertThat(actual, hasSize(0));
    }

    /**
     * Tests {@link ClasspathUtil#getClassesFromPackage(String, Class)}.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testGetClassesFromPackageOutside() throws Exception {
        final List<String> actual = ClasspathUtil.getClassesFromPackage("org.dozer.converters", DozerBeanMapper.class);
        // for (final String className : actual) {
        // System.out.println("*** testGetClassesFromPackageOutside : " +
        // className);
        // }
        assertThat(actual, hasItems("org.dozer.converters.ByteConverter", "org.dozer.converters.CalendarConverter"));
        assertThat(actual, hasSize(16));
    }

}
