/**
 *
 */
package fr.ge.common.nash.core.bean;

import org.junit.Assert;
import org.junit.Test;

/**
 * Class StepStatusEnumTest.
 *
 * @author bsadil
 */
public class StepStatusEnumTest {

    /**
     * Testget status.
     */
    @Test
    public void testgetStatus() {
        Assert.assertEquals("todo", StepStatusEnum.TO_DO.getStatus());
        Assert.assertEquals("done", StepStatusEnum.DONE.getStatus());
        Assert.assertEquals("in_progress", StepStatusEnum.IN_PROGRESS.getStatus());
    }

    /**
     * Testget etat from id.
     */
    @Test
    public void testFromCode() {
        Assert.assertEquals(StepStatusEnum.TO_DO, StepStatusEnum.fromCode(StepStatusEnum.TO_DO.getStatus()));
        Assert.assertEquals(null, StepStatusEnum.fromCode("test"));

    }

}
