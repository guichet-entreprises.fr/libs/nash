/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.core.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.junit.Test;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl.JaxbWorker;

/**
 * Class JaxbFactoryImplTest.
 *
 * @author Christian Cougourdan
 */
public class JaxbFactoryImplTest {

    /** The Constant XML_SOURCE_KO. */
    private static final String XML_SOURCE_KO = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><other></other>";

    /** The Constant XML_SOURCE_OK. */
    private static final String XML_SOURCE_OK = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<description xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" xsi:schemaLocation=\"http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd\">\n    <title>Test Title</title>\n</description>\n";

    /** The Constant XML_SOURCE_OK_WITH_CSS. */
    private static final String XML_SOURCE_OK_WITH_CSS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<?xml-stylesheet type=\"text/css\" href=\"../css/data.css\"?>\n<description xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" xsi:schemaLocation=\"http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd\">\n    <title>Test Title</title>\n</description>\n";

    /** The Constant XML_SOURCE_WITH_UNKNOWN_ELEMENTS. */
    private static final String XML_SOURCE_WITH_UNKNOWN_ELEMENTS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<description xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" xsi:schemaLocation=\"http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd\">\n    <title>Test Title</title>\n    <unknown>...</unknown>\n</description>\n";

    /**
     * Test context error.
     *
     * @throws Exception
     *             the exception
     */
    @Test(expected = TechnicalException.class)
    public void testContextError() throws Exception {
        JaxbFactoryImpl.instance().worker(Serializable.class);
    }

    /**
     * Test context ok.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testContextOk() throws Exception {
        final JaxbWorker worker = JaxbFactoryImpl.instance().worker(XmlTestBean.class);
        assertNotNull(worker);
    }

    /**
     * Test unmarshal error.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUnmarshalError() throws Exception {
        try {
            JaxbFactoryImpl.instance().unmarshal(XML_SOURCE_KO.getBytes(), XmlTestBean.class);
            fail("TechnicalException has to be thrown");
        } catch (final TechnicalException ex) {
            assertEquals("Unable to unmarshall bean " + XmlTestBean.class.getName(), ex.getMessage());
        } catch (final Exception ex) {
            fail("Unexpected exception " + ex.getClass().getName());
        }
    }

    /**
     * Test unmarshal null source.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUnmarshalNullSource() throws Exception {
        final XmlTestBean actual = JaxbFactoryImpl.instance().unmarshal((byte[]) null, XmlTestBean.class);

        assertNull(actual);
    }

    /**
     * Test unmarshal nominal.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUnmarshalNominal() throws Exception {
        final XmlTestBean actual = JaxbFactoryImpl.instance().unmarshal(XML_SOURCE_OK.getBytes(), XmlTestBean.class);

        assertNotNull(actual);
        assertEquals("1.0", actual.getVersion());
        assertEquals("Test Title", actual.getTitle());
        assertNull(actual.getUid());
        assertNull(actual.getAnything());
    }

    /**
     * Test as byte null source.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testAsByteNullSource() throws Exception {
        final byte[] actual = JaxbFactoryImpl.instance().asByteArray(null);

        assertEquals(0, actual.length);
    }

    /**
     * Test as byte nominal.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testAsByteNominal() throws Exception {
        final XmlTestBean bean = new XmlTestBean();
        bean.setUid(null);
        bean.setTitle("Test Title");
        bean.setVersion("1.0");

        final byte[] actual = JaxbFactoryImpl.instance().asByteArray(bean);

        assertNotNull(actual);

        final String actualAsString = new String(actual);

        assertEquals(XML_SOURCE_OK, actualAsString);
    }

    /**
     * Test as byte nominal with css.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testAsByteNominalWithCss() throws Exception {
        final XmlTestBean bean = new XmlTestBean();
        bean.setUid(null);
        bean.setTitle("Test Title");
        bean.setVersion("1.0");

        final byte[] actual = JaxbFactoryImpl.instance().asByteArray(bean, "../css/data.css");

        assertNotNull(actual);

        final String actualAsString = new String(actual);

        assertEquals(XML_SOURCE_OK_WITH_CSS, actualAsString);
    }

    /**
     * Test as byte marshal error.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testAsByteMarshalError() throws Exception {
        try {
            JaxbFactoryImpl.instance().asByteArray(this);
            fail();
        } catch (final TechnicalException ex) {
            assertEquals("Unable to marshal bean " + this.getClass().getName(), ex.getMessage());
        } catch (final Throwable ex) {
            fail("");
        }

    }

    /**
     * Test unmarshal unknown source element.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUnmarshalUnknownSourceElement() throws Exception {
        final XmlTestBean actual = JaxbFactoryImpl.instance().unmarshal(XML_SOURCE_WITH_UNKNOWN_ELEMENTS.getBytes(), XmlTestBean.class);

        assertNotNull(actual);
        assertEquals("1.0", actual.getVersion());
        assertEquals("Test Title", actual.getTitle());
        assertNull(actual.getUid());
        assertNotNull(actual.getAnything());
        assertEquals(1, actual.getAnything().size());
    }

    /**
     * Test marshal unknown source element.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMarshalUnknownSourceElement() throws Exception {
        final XmlTestBean bean = JaxbFactoryImpl.instance().unmarshal(XML_SOURCE_WITH_UNKNOWN_ELEMENTS.getBytes(), XmlTestBean.class);
        final byte[] actual = JaxbFactoryImpl.instance().asByteArray(bean);

        assertNotNull(actual);

        final String actualAsString = new String(actual);

        assertEquals(XML_SOURCE_WITH_UNKNOWN_ELEMENTS, actualAsString);
    }

    /**
     * Test fragment.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFragment() throws Exception {
        final XmlTestBean bean = new XmlTestBean().setUid("2016-11-ONE-UID-42").setTitle("Xml Test Bean").setVersion("1.2");
        final String actual = JaxbFactoryImpl.instance().fragmentAsString(bean);

        assertEquals("<description version=\"1.2\">\n    <uid>2016-11-ONE-UID-42</uid>\n    <title>Xml Test Bean</title>\n</description>", actual);
    }

    /**
     * The Class XmlTestBean.
     */
    @XmlRootElement(name = "description")
    @XmlAccessorType(XmlAccessType.FIELD)
    private static class XmlTestBean implements Serializable {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;

        /** The uid. */
        private String uid;

        /** The title. */
        private String title;

        /** The version. */
        @XmlAttribute(required = true)
        private String version;

        /** The anything. */
        @XmlAnyElement(lax = true)
        private List<Serializable> anything;

        /**
         * Gets the uid.
         *
         * @return the uid
         */
        public String getUid() {
            return this.uid;
        }

        /**
         * Sets the uid.
         *
         * @param uid
         *            the uid to set
         * @return xml test bean
         */
        public XmlTestBean setUid(final String uid) {
            this.uid = uid;
            return this;
        }

        /**
         * Gets the title.
         *
         * @return the title
         */
        public String getTitle() {
            return this.title;
        }

        /**
         * Sets the title.
         *
         * @param title
         *            the title to set
         * @return xml test bean
         */
        public XmlTestBean setTitle(final String title) {
            this.title = title;
            return this;
        }

        /**
         * Gets the version.
         *
         * @return the version
         */
        public String getVersion() {
            return this.version;
        }

        /**
         * Sets the version.
         *
         * @param version
         *            the version to set
         * @return xml test bean
         */
        public XmlTestBean setVersion(final String version) {
            this.version = version;
            return this;
        }

        /**
         * Gets the anything.
         *
         * @return the anything
         */
        public List<Serializable> getAnything() {
            return this.anything;
        }

    }

}
