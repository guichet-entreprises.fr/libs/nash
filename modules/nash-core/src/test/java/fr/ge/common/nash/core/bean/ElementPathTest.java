/**
 *
 */
package fr.ge.common.nash.core.bean;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;

import java.util.Arrays;

import org.junit.Test;

/**
 * Tests {@link ElementPath}.
 *
 * @author jpauchet
 */
public class ElementPathTest {

    /**
     * Tests {@link ElementPath#create(String)}.
     */
    @Test
    public void testCreateNominal() {
        final ElementPath elementPath = ElementPath.create("xxx[21].yyy.zzz[42]");
        assertThat(elementPath, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("originalPath", equalTo("xxx[21].yyy.zzz[42]")), //
                                hasProperty("path", equalTo("xxx[21].yyy.zzz")), //
                                hasProperty("parentPath", equalTo("xxx[21].yyy")), //
                                hasProperty("stepId", equalTo("xxx[21]")), //
                                hasProperty("pageId", equalTo("yyy")), //
                                hasProperty("id", equalTo("zzz")), //
                                hasProperty("index", equalTo(42)), //
                                hasProperty("depth", equalTo(3)) //
                        ) //
                ) //
        );
    }

    /**
     * Tests {@link ElementPath#create(String)}.
     */
    @Test
    public void testCreateNoIndex() {
        final ElementPath elementPath = ElementPath.create("xxx.yyy");
        assertThat(elementPath, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("originalPath", equalTo("xxx.yyy")), //
                                hasProperty("path", equalTo("xxx.yyy")), //
                                hasProperty("parentPath", equalTo("xxx")), //
                                hasProperty("stepId", equalTo("xxx")), //
                                hasProperty("pageId", nullValue()), //
                                hasProperty("id", equalTo("yyy")), //
                                hasProperty("index", equalTo(0)), //
                                hasProperty("depth", equalTo(2)) //
                        ) //
                ) //
        );
    }

    /**
     * Tests {@link ElementPath#create(String)}.
     */
    @Test
    public void testCreateOneLevel() {
        final ElementPath elementPath = ElementPath.create("xxx");
        assertThat(elementPath, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("originalPath", equalTo("xxx")), //
                                hasProperty("path", equalTo("xxx")), //
                                hasProperty("parentPath", equalTo("")), //
                                hasProperty("stepId", nullValue()), //
                                hasProperty("pageId", nullValue()), //
                                hasProperty("id", equalTo("xxx")), //
                                hasProperty("index", equalTo(0)), //
                                hasProperty("depth", equalTo(1)) //
                        ) //
                ) //
        );
    }

    @Test
    public void testWithIndex() {
        final ElementPath elementPath = ElementPath.create("xxx[21].yyy.zzz[42]").withIndex(3);
        assertThat(elementPath, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("originalPath", equalTo("xxx[21].yyy.zzz[3]")), //
                                hasProperty("path", equalTo("xxx[21].yyy.zzz")), //
                                hasProperty("parentPath", equalTo("xxx[21].yyy")), //
                                hasProperty("stepId", equalTo("xxx[21]")), //
                                hasProperty("pageId", equalTo("yyy")), //
                                hasProperty("id", equalTo("zzz")), //
                                hasProperty("index", equalTo(3)), //
                                hasProperty("depth", equalTo(3)) //
                        ) //
                ) //
        );
    }

    @Test
    public void testWithNullIndex() {
        final ElementPath elementPath = ElementPath.create("xxx[21].yyy.zzz[42]").withIndex(null);
        assertThat(elementPath, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("originalPath", equalTo("xxx[21].yyy.zzz")), //
                                hasProperty("path", equalTo("xxx[21].yyy.zzz")), //
                                hasProperty("parentPath", equalTo("xxx[21].yyy")), //
                                hasProperty("stepId", equalTo("xxx[21]")), //
                                hasProperty("pageId", equalTo("yyy")), //
                                hasProperty("id", equalTo("zzz")), //
                                hasProperty("index", equalTo(0)), //
                                hasProperty("depth", equalTo(3)) //
                        ) //
                ) //
        );
    }

}
