/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.core.util;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for scanning the classpath.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public final class ClasspathUtil {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ClasspathUtil.class);

    /** Backslash double escaped. */
    private static final String BACKSLASH_DOUBLE_ESCAPED = "\\\\";

    /** Backslash escaped. */
    private static final String BACKSLASH_ESCAPED = "\\";

    /** Class extension. */
    private static final String CLASS_EXTENSION = ".class";

    /** Class extension escaped. */
    private static final String CLASS_EXTENSION_ESCAPED = "\\.class";

    /** Dot double escaped. */
    private static final String DOT_DOUBLE_ESCAPED = "\\\\.";

    /** Dot escaped. */
    private static final String DOT_ESCAPED = "\\.";

    /** File. */
    private static final String FILE = "file";

    /** JAR. */
    private static final String JAR = "jar";

    /** JAR extension. */
    private static final String JAR_EXTENSION = ".jar";

    /** Package separator. */
    private static final char PACKAGE_SEPARATOR = '.';

    /** Package separator. */
    private static final String PACKAGE_SEPARATOR_STR = ".";

    /** Zip path separator. */
    private static final char ZIP_PATH_SEPARATOR = '/';

    /**
     * Constructor.
     */
    private ClasspathUtil() {
        // Nothing to do.
    }

    /**
     * Gets the classes of a package, provided a class from which the jar is
     * retrieved.
     *
     * @param packageToScan
     *            the prefix to scan with, like "fr.my.prefix.to.scan"
     * @param classFromTargetJar
     *            the class from which the jar is retrieved
     * @return the classes
     */
    public static List<String> getClassesFromPackage(final String packageToScan, final Class<?> classFromTargetJar) {
        List<String> packageClasses = new ArrayList<>();
        final URL jarClassUrl = classFromTargetJar.getResource(classFromTargetJar.getSimpleName() + CLASS_EXTENSION);
        try {
            if (JAR.equals(jarClassUrl.getProtocol())) {
                packageClasses = getClassesFromPackageJarProtocol(packageToScan, jarClassUrl);
            } else if (FILE.equals(jarClassUrl.getProtocol())) {
                packageClasses = getClassesFromPackageFileProtocol(packageToScan);
            }
        } catch (final IOException e) {
            LOGGER.error("Classes from package " + packageToScan + " could not be loaded", e);
        }
        return packageClasses;
    }

    /**
     * Escapes a String.
     *
     * @param notEscapedString
     *            the String to escape
     * @return the escaped String
     */
    private static String getEscapedString(final String notEscapedString) {
        return notEscapedString.replace(BACKSLASH_ESCAPED, BACKSLASH_DOUBLE_ESCAPED).replace(DOT_ESCAPED, DOT_DOUBLE_ESCAPED);
    }

    /**
     * Gets the classes of a package, provided a class from which the jar is
     * retrieved, for the jar protocol.
     *
     * @param packageToScan
     *            the prefix to scan with, like "fr.my.prefix.to.scan"
     * @param jarClassUrl
     *            the JAR class URL
     * @return the classes
     * @throws IOException
     *             an IO exception
     */
    private static List<String> getClassesFromPackageJarProtocol(final String packageToScan, final URL jarClassUrl) throws IOException {
        final String packageToScanSlashes = packageToScan.replace(PACKAGE_SEPARATOR, ZIP_PATH_SEPARATOR);
        final List<String> packageClasses = new ArrayList<>();
        final JarURLConnection urlConnection = (JarURLConnection) jarClassUrl.openConnection();
        final JarFile jar = urlConnection.getJarFile();
        final Enumeration<JarEntry> entries = jar.entries();
        while (entries.hasMoreElements()) {
            final JarEntry entry = entries.nextElement();
            if (!entry.isDirectory() && entry.getName().startsWith(packageToScanSlashes)) {
                String className = entry.getName();
                if (className.endsWith(CLASS_EXTENSION)) {
                    className = className.split(CLASS_EXTENSION_ESCAPED)[0].replace(ZIP_PATH_SEPARATOR, PACKAGE_SEPARATOR);
                    packageClasses.add(className);
                }
            }
        }
        return packageClasses;
    }

    /**
     * Gets the classes of a package, provided a class from which the jar is
     * retrieved, for the jar protocol.
     *
     * @param packageToScan
     *            the prefix to scan with, like "fr.my.prefix.to.scan"
     * @return the classes
     * @throws MalformedURLException
     *             a malformed URL exception
     */
    private static List<String> getClassesFromPackageFileProtocol(final String packageToScan) throws MalformedURLException {
        final String packageToScanSlashes = packageToScan.replace(PACKAGE_SEPARATOR_STR, File.separator);
        final List<String> packageClasses = new ArrayList<>();
        final URL[] classpathEntries = ((URLClassLoader) Thread.currentThread().getContextClassLoader()).getURLs();

        for (final URL classpathEntry : classpathEntries) {
            if (!classpathEntry.getPath().endsWith(JAR_EXTENSION)) {
                final String classpathEntryWithPackage = classpathEntry + packageToScanSlashes;
                File file = new File(new URL(classpathEntryWithPackage).getPath());
                final String classpathEntryWithoutProtocol = getEscapedString(new File(new URL(classpathEntry.toString()).getPath()).getPath());
                String subPackagePrefix = null;
                if (!file.exists()) {
                    subPackagePrefix = file.getName();
                    file = file.getParentFile();
                }
                packageClasses.addAll(getClassesFromPackageFileProtocolRec(file, subPackagePrefix, classpathEntryWithoutProtocol));
            }
        }
        return packageClasses;
    }

    /**
     * Gets the classes of a package recursively.
     *
     * @param parent
     *            the parent file
     * @param subPackagePrefix
     *            the optional subpackage prefix
     * @param classpathEntryWithoutProtocol
     *            the classpath entry without protocol
     * @return the classes
     */
    private static List<String> getClassesFromPackageFileProtocolRec(final File parent, final String subPackagePrefix, final String classpathEntryWithoutProtocol) {
        final List<String> packageClasses = new ArrayList<>();

        Predicate<File> pathFilter = null;
        if (null == subPackagePrefix) {
            pathFilter = f -> true;
        } else {
            final String prefix = parent.getAbsolutePath() + File.separator + subPackagePrefix;
            pathFilter = f -> f.getAbsolutePath().startsWith(prefix);
        }

        if (parent.exists()) {
            if (parent.isDirectory()) {
                Arrays.stream(Optional.ofNullable(parent.listFiles()).orElse(new File[] {})) //
                        .filter(pathFilter) //
                        .forEach(child -> packageClasses.addAll(getClassesFromPackageFileProtocolRec(child, null, classpathEntryWithoutProtocol)));
            } else if (parent.isFile()) {
                String className = parent.getPath().split(classpathEntryWithoutProtocol)[1];
                if (className.endsWith(CLASS_EXTENSION)) {
                    className = className.split(CLASS_EXTENSION_ESCAPED)[0].substring(1).replace(File.separator, PACKAGE_SEPARATOR_STR);
                    packageClasses.add(className);
                }
            }
        }
        return packageClasses;
    }

}
