/**
 *
 */
package fr.ge.common.nash.core.bean;

/**
 * Enum StepStatusEnum.
 *
 * @author bsadil
 */
public enum StepStatusEnum {

    /** in progress. */
    IN_PROGRESS("in_progress"),

    /** done. */
    DONE("done", true),

    /** to do. */
    TO_DO("todo"),

    /** sealed. **/
    SEALED("sealed", true),

    /** error. **/
    ERROR("error");

    /** status. */
    private final String status;

    private final boolean closed;

    StepStatusEnum(final String status) {
        this(status, false);
    }

    /**
     * Instantiate a new step status.
     *
     * @param status
     *     status
     */
    StepStatusEnum(final String status, final boolean closed) {
        this.status = status;
        this.closed = closed;
    }

    /**
     * Get status.
     *
     * @param id
     *     le id
     * @return status from id
     */
    public static StepStatusEnum fromCode(final String id) {
        for (final StepStatusEnum status : StepStatusEnum.values()) {
            if (status.getStatus().equals(id)) {
                return status;
            }
        }

        return null;
    }

    /**
     * Getter on attribute {@link #status}.
     *
     * @return the status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Is this a closed status ?
     *
     * @return true if this is a closed status (eg DONE or SEALED)
     */
    public boolean isClosed() {
        return this.closed;
    }

}
