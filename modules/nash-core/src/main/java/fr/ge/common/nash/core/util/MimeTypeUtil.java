/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.core.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class MimeTypeUtil.
 */
public final class MimeTypeUtil {

    /** La constante MIME_TYPE_PATTERN. */
    private static final Pattern MIME_TYPE_PATTERN = Pattern.compile(".*\\.([^.]+)");

    /** La constante MIME_TYPES. */
    private static final Map<String, String> MIME_TYPES;

    static {
        final Map<String, String> map = new HashMap<>();

        map.put("txt", "text/plain");
        map.put("xml", "text/xml");
        map.put("html", "text/html");
        map.put("css", "text/css");
        map.put("xsl", "text/xsl");
        map.put("xslt", "text/xsl");
        map.put("pdf", "application/pdf");
        map.put("zip", "application/zip");
        map.put("jpg", "image/jpeg");

        MIME_TYPES = Collections.unmodifiableMap(map);
    }

    /**
     * Instantie un nouveau mime type utils.
     */
    private MimeTypeUtil() {
        // Nothing to do
    }

    /**
     * Type.
     *
     * @param resourcePath
     *            resource path
     * @return string
     */
    public static String type(final String resourcePath) {
        final Matcher matcher = MIME_TYPE_PATTERN.matcher(resourcePath);

        String mimeType = null;

        if (matcher.matches()) {
            mimeType = MIME_TYPES.get(matcher.group(1).toLowerCase());
        } else {
            mimeType = MIME_TYPES.get(resourcePath);
        }

        if (null == mimeType) {
            return "application/octet-stream";
        }

        return mimeType;
    }

}
