/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.core.exception;

/**
 * The Class ExpressionException.
 *
 * @author Christian Cougourdan
 */
public class ExpressionException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The expression. */
    private final String expression;

    /** The line number. */
    private final int lineNumber;

    /** The column number. */
    private final int columnNumber;

    /** The filename. */
    private final String filename;

    /**
     * Instantiates a new expression exception.
     *
     * @param message
     *            the message
     * @param expression
     *            the expression
     * @param lineNumber
     *            the line number
     * @param columnNumber
     *            the column number
     */
    public ExpressionException(final String message, final String expression, final int lineNumber, final int columnNumber) {
        this(message, null, expression, lineNumber, columnNumber);
    }

    /**
     * Instantiates a new expression exception.
     *
     * @param message
     *            the message
     * @param filename
     *            the filename
     * @param expression
     *            the expression
     * @param lineNumber
     *            the line number
     * @param columnNumber
     *            the column number
     */
    public ExpressionException(final String message, final String filename, final String expression, final int lineNumber, final int columnNumber) {
        this(message, filename, expression, lineNumber, columnNumber, null);
    }

    /**
     * Instantiates a new expression exception.
     *
     * @param message
     *            the message
     * @param filename
     *            the filename
     * @param expression
     *            the expression
     * @param lineNumber
     *            the line number
     * @param columnNumber
     *            the column number
     * @param cause
     *            cause exception
     */
    public ExpressionException(final String message, final String filename, final String expression, final int lineNumber, final int columnNumber, final Exception cause) {
        super(message, cause);
        this.filename = filename;
        this.expression = expression;
        this.lineNumber = lineNumber;
        this.columnNumber = columnNumber;
    }

    /**
     * Instantiates a new expression exception.
     *
     * @param filename
     *            the filename
     * @param cause
     *            the cause
     */
    public ExpressionException(final String filename, final ExpressionException cause) {
        this(cause.getMessage(), filename, cause.getExpression(), cause.getLineNumber(), cause.getColumnNumber(), cause);
    }

    /**
     * Gets the expression.
     *
     * @return the expression
     */
    public String getExpression() {
        return this.expression;
    }

    /**
     * Gets the line number.
     *
     * @return the line number
     */
    public int getLineNumber() {
        return this.lineNumber;
    }

    /**
     * Gets the column number.
     *
     * @return the column number
     */
    public int getColumnNumber() {
        return this.columnNumber;
    }

    /**
     * Gets the filename.
     *
     * @return the filename
     */
    public String getFilename() {
        return this.filename;
    }

}
