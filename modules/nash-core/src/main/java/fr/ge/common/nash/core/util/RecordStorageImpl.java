/**
 *
 */
package fr.ge.common.nash.core.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.storage.IRecordStorage;

/**
 * Class RecordStorageImpl.
 *
 * @author bsadil
 */
public class RecordStorageImpl implements IRecordStorage {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordStorageImpl.class);

    /** Checksum buffer size. */
    private static final int CHECKSUM_BUFFER_SIZE = 8192;

    /** the ZIP extension of record. */
    protected static final String ZIP_EXTENSION = ".zip";

    /** THE SHA1 extension of record. */
    protected static final String SHA1_EXTENSION = ".sha1";

    /** THE MD5 extension of record. */
    protected static final String MD5_EXTENSION = ".md5";

    /** THE SHA1 algorithm of record. */
    protected static final String SHA1 = "sha1";

    /** THE MD5 algorithm of record. */
    protected static final String MD5 = "md5";

    /** the SEPARATOR. */
    protected static final char SEPARATOR = '-';

    /** the root storage of record. */
    private String rootStorage;

    /**
     * {@inheritDoc}
     *
     * @throws TemporaryException
     */
    @Override
    public void storeRecord(final String code, final InputStream stream, final String storagePath) throws TemporaryException {
        final Path baseStoragePath = this.createDirectory(storagePath, code);
        final File baseStorageFile = baseStoragePath.toFile();

        if (!baseStorageFile.exists()) {
            baseStorageFile.mkdirs();
        }

        if (baseStorageFile.canWrite()) {
            final File dstZipRecordFile = baseStoragePath.resolve(code + ZIP_EXTENSION).toFile();

            if (dstZipRecordFile.exists()) {
                LOGGER.info("the Zip file for record [{}] already exist in [{}]", code, baseStoragePath);
                throw new TemporaryException("The Zip file already exist");
            }

            try {
                Files.copy(stream, dstZipRecordFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (final IOException ex) {
                LOGGER.error("Error when copying file for record [{}]", code, ex);
                throw new TemporaryException("Error when copying file for record code", ex);
            }

            this.writeChecksum(dstZipRecordFile, baseStoragePath, code, SHA1);
            this.writeChecksum(dstZipRecordFile, baseStoragePath, code, MD5);

            LOGGER.info("create Zip file for record code [{}] in  this path Storage [{}]", code, baseStoragePath);
        } else {
            LOGGER.error("Access denied when copying file for record [{}]", code);
            throw new TemporaryException("Permission denied");
        }

    }

    /**
     * Write checksum.
     *
     * @param srcRecordZip
     *            the src record zip
     * @param baseStoragePath
     *            the base storage path
     * @param code
     *            the code
     * @param hashMethod
     *            the hash method
     * @throws TemporaryException
     *             the temporary exception
     */
    private void writeChecksum(final File srcRecordZip, final Path baseStoragePath, final String code, final String hashMethod) throws TemporaryException {
        final Path dstPath = baseStoragePath.resolve(code + '.' + hashMethod);
        final byte[] checksum = new HexBinaryAdapter().marshal(this.createChecksum(srcRecordZip, hashMethod)).getBytes(StandardCharsets.UTF_8);

        try {
            Files.write(dstPath, checksum, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
        } catch (final IOException ex) {
            throw new TemporaryException("Unable to write checksum file [" + dstPath.getFileName() + "]");
        }
    }

    /**
     * Create directory <deposit date - YYYY-MM/DD>/<functional tree>.
     *
     * @param storagePath
     *            functional tree
     * @param code
     *            the record code like "2016-12-EZK-ZWM-50"
     * @return <deposit date - YYYY-MM/DD>/<functional tree>
     */
    private Path createDirectory(final String storagePath, final String code) {
        final Temporal now = LocalDateTime.now();
        Path basePath = Paths.get(this.rootStorage, DateTimeFormatter.ofPattern("yyyy-MM").format(now), DateTimeFormatter.ofPattern("dd").format(now));
        for (final String ref : StringUtils.split(storagePath, File.separator)) {
            basePath = basePath.resolve(ref);
        }

        LOGGER.debug("the path storage for record code {} is {}", code, basePath.toString());

        return basePath;
    }

    /**
     * Read the file and calculate checksum with a specific algorithm.
     *
     * @param file
     *            the file to read
     * @param type
     *            type
     * @return file checksum
     * @throws TemporaryException
     *             the temporary exception
     */
    public byte[] createChecksum(final File file, final String type) throws TemporaryException {
        try (InputStream fis = new FileInputStream(file)) {
            final MessageDigest digest = MessageDigest.getInstance(type);
            int n = 0;
            final byte[] buffer = new byte[CHECKSUM_BUFFER_SIZE];

            while (n != -1) {
                n = fis.read(buffer);
                if (n > 0) {
                    digest.update(buffer, 0, n);
                }
            }
            return digest.digest();
        } catch (final IOException ex) {
            throw new TemporaryException("Unable to read record file : " + ex.getMessage());
        } catch (final NoSuchAlgorithmException ex) {
            throw new TechnicalException("Digest algorithm [" + type + "] not found" + ex.getMessage());
        }
    }

    /**
     * Setter on attribute {@link #rootStorage}.
     *
     * @param rootStorage
     *            the rootStorage to set
     */
    public void setRootStorage(final String rootStorage) {
        this.rootStorage = rootStorage;
    }

    /**
     * {@inheritDoc}
     *
     * @throws TemporaryException
     */
    @Override
    public void storeRecord(final String code, final byte[] bytes, final String storagePath) throws TemporaryException {
        this.storeRecord(code, new ByteArrayInputStream(bytes), storagePath);
    }

}
