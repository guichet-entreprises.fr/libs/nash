/**
 *
 */
package fr.ge.common.nash.core.bean;

/**
 * Event types.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum EventTypeEnum {

    /** ON_CREATE_RECORD. */
    ON_CREATE_RECORD("onCreateRecord"),

    /** BEFORE_UPDATE_STEP_STATUS. */
    BEFORE_UPDATE_STEP_STATUS("beforeUpdateStepStatus"),

    /** ON_FINISH_RECORD. */
    ON_FINISH_RECORD("onFinishRecord");

    /** status. */
    private String name;

    /**
     * Constructor.
     *
     * @param name
     *            the name
     */
    EventTypeEnum(final String name) {
        this.name = name;
    }

    /**
     * Gets a user type from its name.
     *
     * @param name
     *            the name
     * @return the user type
     */
    public static EventTypeEnum getFromName(final String name) {
        for (final EventTypeEnum userType : EventTypeEnum.values()) {
            if (userType.getName().equals(name)) {
                return userType;
            }
        }
        return null;
    }

    /**
     * Getter on attribute {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter on attribute {@link #name}.
     *
     * @param name
     *            the new value of attribute name
     */
    public void setName(final String name) {
        this.name = name;
    }

}
