/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.core.writer;

import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;

/**
 * Empty namespace writer.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class EmptyNamespaceWriter implements XMLStreamWriter {

    /** Writer. */
    private final XMLStreamWriter writer;

    /** Empty namespace context. */
    private static final NamespaceContext EMPTY_NAMESPACE_CONTEXT = new NamespaceContext() {

        /**
         * {@inheritDoc}
         */
        @Override
        public String getNamespaceURI(final String prefix) {
            return "";
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getPrefix(final String namespaceURI) {
            return "";
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Iterator<String> getPrefixes(final String namespaceURI) {
            return null;
        }

    };

    /**
     * Constructor.
     *
     * @param writer
     *            the writer
     */
    public EmptyNamespaceWriter(final XMLStreamWriter writer) {
        this.writer = writer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NamespaceContext getNamespaceContext() {
        return EMPTY_NAMESPACE_CONTEXT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeStartElement(final String localName) throws XMLStreamException {
        this.writer.writeStartElement(localName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeStartElement(final String namespaceURI, final String localName) throws XMLStreamException {
        this.writer.writeStartElement(namespaceURI, localName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeStartElement(final String prefix, final String localName, final String namespaceURI) throws XMLStreamException {
        this.writer.writeStartElement(StringUtils.EMPTY, localName, namespaceURI);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeEmptyElement(final String namespaceURI, final String localName) throws XMLStreamException {
        this.writer.writeEmptyElement(namespaceURI, localName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeEmptyElement(final String prefix, final String localName, final String namespaceURI) throws XMLStreamException {
        this.writer.writeEmptyElement(prefix, localName, namespaceURI);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeEmptyElement(final String localName) throws XMLStreamException {
        this.writer.writeEmptyElement(localName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeEndElement() throws XMLStreamException {
        this.writer.writeEndElement();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeEndDocument() throws XMLStreamException {
        this.writer.writeEndDocument();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws XMLStreamException {
        this.writer.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush() throws XMLStreamException {
        this.writer.flush();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeAttribute(final String localName, final String value) throws XMLStreamException {
        this.writer.writeAttribute(localName, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeAttribute(final String prefix, final String namespaceURI, final String localName, final String value) throws XMLStreamException {
        this.writer.writeAttribute(prefix, namespaceURI, localName, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeAttribute(final String namespaceURI, final String localName, final String value) throws XMLStreamException {
        this.writer.writeAttribute(namespaceURI, localName, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeNamespace(final String prefix, final String namespaceURI) throws XMLStreamException {
        // Do nothing.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeDefaultNamespace(final String namespaceURI) throws XMLStreamException {
        // Do nothing.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeComment(final String data) throws XMLStreamException {
        this.writer.writeComment(data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeProcessingInstruction(final String target) throws XMLStreamException {
        this.writer.writeProcessingInstruction(target);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeProcessingInstruction(final String target, final String data) throws XMLStreamException {
        this.writer.writeProcessingInstruction(target, data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeCData(final String data) throws XMLStreamException {
        this.writer.writeCData(data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeDTD(final String dtd) throws XMLStreamException {
        this.writer.writeDTD(dtd);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeEntityRef(final String name) throws XMLStreamException {
        this.writer.writeEntityRef(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeStartDocument() throws XMLStreamException {
        this.writer.writeStartDocument();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeStartDocument(final String version) throws XMLStreamException {
        this.writer.writeStartDocument(version);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeStartDocument(final String encoding, final String version) throws XMLStreamException {
        this.writer.writeStartDocument(encoding, version);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeCharacters(final String text) throws XMLStreamException {
        this.writer.writeCharacters(text);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeCharacters(final char[] text, final int start, final int len) throws XMLStreamException {
        this.writer.writeCharacters(text, start, len);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPrefix(final String uri) throws XMLStreamException {
        return this.writer.getPrefix(uri);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPrefix(final String prefix, final String uri) throws XMLStreamException {
        this.writer.setPrefix(prefix, uri);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDefaultNamespace(final String uri) throws XMLStreamException {
        this.writer.setDefaultNamespace(uri);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setNamespaceContext(final NamespaceContext context) throws XMLStreamException {
        this.writer.setNamespaceContext(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getProperty(final String name) {
        return this.writer.getProperty(name);
    }

}
