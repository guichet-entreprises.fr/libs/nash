/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.core.bean;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

/**
 * Element path.
 *
 * @author jpauchet
 */
public class ElementPath {

    /** The Constant ELEMENT_PATH_PATTERN. */
    private static final Pattern FULL_PATH_PATTERN = Pattern.compile("((?:(([^\\.]+)(?:\\.([^\\.]+))*)\\.)?([^\\.\\[]+))(?:\\[([0-9]*)\\])?");

    private static final Pattern PATH_ELEMENT_PATTERN = Pattern.compile("([^\\[]+)(?:\\[([0-9]*)\\])?");

    /**
     * The Enum PatternIndex.
     */
    private enum FullPathPatternIndex {

        /** The path. */
        PATH,
        /** The parent. */
        PARENT,
        /** The step. */
        STEP,
        /** The page. */
        PAGE,
        /** The id. */
        ID,
        /** The index. */
        INDEX;

        /**
         * Gets the.
         *
         * @return the int
         */
        int get() {
            return Arrays.binarySearch(values(), this) + 1;
        }
    }

    private enum PathElementPatternIndex {

        KEY,

        INDEX;

        int get() {
            return Arrays.binarySearch(values(), this) + 1;
        }

    }

    /** The original path. */
    private final String fullPath;

    /** The path without the occurrence index. */
    private final String path;

    /** The parent path. */
    private final String parentPath;

    /** The id (without the occurrence index). */
    private final String id;

    /** The occurrence index. */
    private final Integer index;

    /** The step id. */
    private final String stepId;

    /** The page id. */
    private final String pageId;

    private final int depth;

    private String pathAsHtml;

    private String pathAsRuleKey;

    /**
     * Constructor.
     *
     * @param fullPath
     *            the original path
     * @param path
     *            the path
     * @param parentPath
     *            the parent path
     * @param stepId
     *            the step id
     * @param pageId
     *            the page id
     * @param id
     *            the id
     * @param index
     *            the index
     */
    private ElementPath(final String fullPath, final String path, final String parentPath, final String stepId, final String pageId, final String id, final Integer index) {
        this.fullPath = fullPath;
        this.path = path;
        this.parentPath = parentPath;
        this.stepId = stepId;
        this.pageId = pageId;
        this.id = id;
        this.index = index;
        this.depth = path.split("\\.").length;
    }

    /**
     * Creates the.
     *
     * @param path
     *            the path
     * @return the element path
     */
    public static ElementPath create(final String path) {
        final Matcher pathMatcher = FULL_PATH_PATTERN.matcher(path);
        if (pathMatcher.matches()) {
            return new ElementPath( //
                    path, //
                    Optional.ofNullable(pathMatcher.group(FullPathPatternIndex.PATH.get())).orElse(""), //
                    Optional.ofNullable(pathMatcher.group(FullPathPatternIndex.PARENT.get())).orElse(""), //
                    Optional.ofNullable(pathMatcher.group(FullPathPatternIndex.STEP.get())).orElse(null), //
                    Optional.ofNullable(pathMatcher.group(FullPathPatternIndex.PAGE.get())).orElse(null), //
                    Optional.ofNullable(pathMatcher.group(FullPathPatternIndex.ID.get())).orElse(""), //
                    Optional.ofNullable(pathMatcher.group(FullPathPatternIndex.INDEX.get())).filter(StringUtils::isNotEmpty).map(Integer::valueOf).orElse(0) //
            );
        } else {
            return null;
        }
    }

    /**
     * Gets the original path.
     *
     * @return the original path
     * @deprecated use {@link getFullPath()} instead
     */
    @Deprecated
    public String getOriginalPath() {
        return this.fullPath;
    }

    /***
     * Gets the full path, including index
     *
     * @return the full path
     */
    public String getFullPath() {
        return this.fullPath;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Gets the parent path.
     *
     * @return the parent path
     */
    public String getParentPath() {
        return this.parentPath;
    }

    /**
     * Gets the step id.
     *
     * @return the stepId
     */
    public String getStepId() {
        return this.stepId;
    }

    /**
     * Gets the page id.
     *
     * @return the pageId
     */
    public String getPageId() {
        return this.pageId;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public Integer getIndex() {
        return this.index;
    }

    /**
     * @return the depth
     */
    public int getDepth() {
        return this.depth;
    }

    public Collection<Token> tokenize() {
        return Arrays.stream(this.fullPath.split("\\.")).map(Token::create).collect(Collectors.toList());
    }

    public String asHtml() {
        if (null == this.pathAsHtml) {
            this.pathAsHtml = this.fullPath.replaceAll("[.\\]\\[]+", "-").replaceAll("-$", "");
        }
        return this.pathAsHtml;
    }

    public String asRuleKey() {
        if (null == this.pathAsRuleKey) {
            this.pathAsRuleKey = this.fullPath.replaceAll("\\[[^\\]]*\\]\\.?", "-0-").replaceAll("\\.", "-").replaceAll("-{2,}", "-").replaceAll("-$", "");
        }
        return this.pathAsRuleKey;
    }

    public ElementPath withIndex(final Integer newIndex) {
        return new ElementPath(null == newIndex ? this.path : String.format("%s[%d]", this.path, newIndex), this.path, this.parentPath, this.stepId, this.pageId, this.id,
                null == newIndex ? 0 : newIndex);
    }

    public static class Token {

        private final String key;

        private final Integer index;

        private Token(final String key, final Integer index) {
            this.key = key;
            this.index = index;
        }

        public static Token create(final String src) {
            final Matcher m = PATH_ELEMENT_PATTERN.matcher(src);

            if (m.matches()) {
                return new Token( //
                        m.group(PathElementPatternIndex.KEY.get()), //
                        Optional.ofNullable(m.group(PathElementPatternIndex.INDEX.get())) //
                                .filter(StringUtils::isNotEmpty) //
                                .map(Integer::parseInt) //
                                .orElse(null) //
                );
            } else {
                return null;
            }
        }

        /**
         * @return the key
         */
        public String getKey() {
            return this.key;
        }

        /**
         * @return the index
         */
        public Integer getIndex() {
            return this.index;
        }

        @Override
        public String toString() {
            if (null == this.index) {
                return this.key;
            } else {
                return String.format("%s[%d]", this.key, this.index);
            }
        }

    }

}
