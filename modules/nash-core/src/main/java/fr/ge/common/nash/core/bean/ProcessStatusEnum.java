/**
 *
 */
package fr.ge.common.nash.core.bean;

/**
 * Enum ProcessStatusEnum.
 *
 * @author aakkou
 */
public enum ProcessStatusEnum {

    /** in progress. */
    IN_PROGRESS("in_progress"),

    /** done. */
    DONE("done"),

    /** to do. */
    TO_DO("todo"),

    /** sealed. **/
    SEALED("sealed");

    /** status. */
    private String status;

    /**
     * Instantie un nouveau process status enum.
     *
     * @param status
     *            status
     */
    ProcessStatusEnum(final String status) {
        this.status = status;
    }

    /**
     * Get status.
     *
     * @param id
     *            le id
     * @return status from id
     */
    public static ProcessStatusEnum getEtatFromId(final String id) {
        for (final ProcessStatusEnum status : ProcessStatusEnum.values()) {
            if (status.getStatus().equals(id)) {
                return status;
            }
        }

        return null;
    }

    /**
     * Getter on attribute {@link #status}.
     *
     * @return the status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter on attribute {@link #status}.
     *
     * @param status
     *            the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

}
