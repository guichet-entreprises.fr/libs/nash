/**
 *
 */
package fr.ge.common.nash.core.bean;

/**
 * Steps users.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public enum StepUserTypeEnum {

    /** USER. */
    USER("user"),

    /** GE. */
    GE("ge"),

    /** PARTNER. */
    PARTNER("partner");

    /** status. */
    private String name;

    /**
     * Constructor.
     *
     * @param name
     *            the name
     */
    StepUserTypeEnum(final String name) {
        this.name = name;
    }

    /**
     * Gets a user type from its name.
     *
     * @param name
     *            the name
     * @return the user type
     */
    public static StepUserTypeEnum getFromName(final String name) {
        for (final StepUserTypeEnum userType : StepUserTypeEnum.values()) {
            if (userType.getName().equals(name)) {
                return userType;
            }
        }
        return null;
    }

    /**
     * Getter on attribute {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter on attribute {@link #name}.
     *
     * @param name
     *            the new value of attribute name
     */
    public void setName(final String name) {
        this.name = name;
    }

}
