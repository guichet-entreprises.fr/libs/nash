/**
 * 
 */
package fr.ge.common.nash.core.storage;

import java.io.InputStream;

import fr.ge.common.nash.core.exception.TemporaryException;

/**
 * Interface IRecordStorage.
 *
 * @author bsadil
 */
public interface IRecordStorage {

    /**
     * storeRecord.
     *
     * @param code            code
     * @param bytes            bytes
     * @param storagePath            storage path
     * @throws TemporaryException          temporary exception
     */
    void storeRecord(String code, byte[] bytes, String storagePath) throws TemporaryException;

    /**
     * storeRecord.
     *
     * @param code            code
     * @param stream            stream
     * @param storagePath            storage path
     * @throws TemporaryException          temporary exception
     */
    void storeRecord(String code, InputStream stream, String storagePath) throws TemporaryException;
}
