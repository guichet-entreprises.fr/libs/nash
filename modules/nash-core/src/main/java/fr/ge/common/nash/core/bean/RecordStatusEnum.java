/**
 *
 */
package fr.ge.common.nash.core.bean;

/**
 * Enum RecordStatusEnum.
 *
 * @author bsadil
 */
public enum RecordStatusEnum {

    /** in progress. */
    IN_PROGRESS("in_progress"),

    /** finalize. */
    FINALIZE("finalize");

    /** status. */
    private String status;

    /**
     * Instantie un nouveau record status enum.
     *
     * @param status
     *            status
     */
    RecordStatusEnum(final String status) {
        this.status = status;
    }

    /**
     * Get status.
     *
     * @param id
     *            le id
     * @return status from id
     */
    public static RecordStatusEnum getStatusFromId(final String id) {
        for (final RecordStatusEnum status : RecordStatusEnum.values()) {
            if (status.getStatus().equals(id)) {
                return status;
            }
        }

        return null;
    }

    /**
     * Getter on attribute {@link #status}.
     *
     * @return the status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter on attribute {@link #status}.
     *
     * @param status
     *            the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

}
