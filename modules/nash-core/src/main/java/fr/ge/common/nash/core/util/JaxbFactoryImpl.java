/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.core.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.writer.EmptyNamespaceWriter;

/**
 * The Class JaxbFactoryImpl.
 *
 * @author Christian Cougourdan
 */
public class JaxbFactoryImpl {

    /** The Constant instance. */
    private static final JaxbFactoryImpl _INSTANCE = new JaxbFactoryImpl();

    /** The worker cache. */
    private final Map<Class<?>, JaxbWorker> workerCache = new ConcurrentHashMap<>();

    /**
     * Hidden constructor.
     */
    private JaxbFactoryImpl() {
        // Nothing to do
    }

    /**
     * Return factory instance singleton.
     *
     * @return factory instance
     */
    public static JaxbFactoryImpl instance() {
        return _INSTANCE;
    }

    /**
     * Retrieve JAXB context for specific class.
     *
     * @param clazz
     *            target class
     * @return JAXB context instance
     */
    public JaxbWorker worker(final Class<?> clazz) {
        JaxbWorker worker = this.workerCache.get(clazz);

        if (null == worker) {
            worker = new JaxbWorker(clazz);
            this.workerCache.put(clazz, worker);
        }

        return worker;
    }

    @SuppressWarnings("unchecked")
    public <T> T unmarshal(final InputStream resourceAsStream, final Class<T> clazz) {
        if (null == resourceAsStream) {
            return null;
        }

        final JaxbWorker worker = this.worker(clazz);

        try {
            return (T) worker.getUnmarshaller().unmarshal(resourceAsStream);
        } catch (final JAXBException e) {
            throw new TechnicalException("Unable to unmarshall bean " + clazz.getName(), e);
        }
    }

    /**
     * Unmarshal xml bean from byte array content.
     *
     * @param <T>
     *            the generic type
     * @param beanAsBytes
     *            bean as byte array
     * @param clazz
     *            bean class
     * @return class instance
     */
    public <T> T unmarshal(final byte[] beanAsBytes, final Class<T> clazz) {
        if (null == beanAsBytes) {
            return null;
        }

        try (InputStream stream = new ByteArrayInputStream(beanAsBytes)) {
            return this.unmarshal(stream, clazz);
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to unmarshall bean " + clazz.getName(), ex);
        }
    }

    /**
     * Marshal bean as a string.
     *
     * @param <T>
     *            the generic type
     * @param bean
     *            bean to serialize
     * @return bean as string
     */
    public <T> String asString(final T bean) {
        return new String(this.asByteArray(bean), StandardCharsets.UTF_8);
    }

    /**
     * Marshal bean as a byte array.
     *
     * @param <T>
     *            the generic type
     * @param bean
     *            bean to serialize
     * @return bean as byte array
     */
    public <T> byte[] asByteArray(final T bean) {
        return this.asByteArray(bean, null);
    }

    /**
     * As byte array.
     *
     * @param <T>
     *            the generic type
     * @param bean
     *            the bean
     * @param stylesheet
     *            the stylesheet
     * @return the byte[]
     */
    public <T> byte[] asByteArray(final T bean, final String stylesheet) {
        if (null == bean) {
            return new byte[] {};
        }

        final Marshaller marshaller = this.marshaller(bean, "1.2");

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".getBytes(StandardCharsets.UTF_8));
            if (null != stylesheet) {
                out.write(String.format("\n<?xml-stylesheet type=\"%s\" href=\"%s\"?>", MimeTypeUtil.type(stylesheet), stylesheet).getBytes(StandardCharsets.UTF_8));
            }

            marshaller.marshal(bean, out);

            return out.toByteArray();
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to create output buffer", ex);
        } catch (final JAXBException ex) {
            throw new TechnicalException("Unable to marshal bean " + bean.getClass().getName(), ex);
        }
    }

    /**
     * Gets a fragment as a string.
     *
     * @param <T>
     *            the generic type
     * @param bean
     *            the bean
     * @return the fragment as a string
     */
    public <T> String fragmentAsString(final T bean) {
        if (null == bean) {
            return StringUtils.EMPTY;
        }

        final String fragment = this.marshal(bean);

        String formattedFragment = null;
        if (!StringUtils.isEmpty(fragment)) {
            try (StringWriter transformerWriter = new StringWriter()) {
                final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
                factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                final DocumentBuilder builder = factory.newDocumentBuilder();
                final Document document = builder.parse(new ByteArrayInputStream(fragment.getBytes(StandardCharsets.UTF_8)));
                final Source source = new DOMSource(document);
                final Result result = new StreamResult(transformerWriter);
                final Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.transform(source, result);
                formattedFragment = transformerWriter.toString().replaceAll("\r", "").trim();
            } catch (TransformerException | ParserConfigurationException | SAXException | IOException e) {
                throw new TechnicalException(StringUtils.EMPTY, e);
            }
        }

        return formattedFragment;
    }

    /**
     * Marshall.
     *
     * @param <T>
     *            type générique
     * @param bean
     *            bean
     * @return string
     * @throws FactoryConfigurationError
     *             factory configuration error
     */
    private <T> String marshal(final T bean) throws javax.xml.stream.FactoryConfigurationError {
        final Marshaller marshaller = this.marshaller(bean, null);

        String fragment = null;
        try (StringWriter writer = new StringWriter()) {
            final XMLStreamWriter xmlWriter = new EmptyNamespaceWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(writer));
            marshaller.marshal(bean, xmlWriter);
            xmlWriter.flush();
            fragment = writer.toString();
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to create output buffer", ex);
        } catch (final JAXBException | XMLStreamException ex) {
            throw new TechnicalException("Unable to marshal bean " + bean.getClass().getName(), ex);
        }

        return fragment;
    }

    /**
     * Marshaller.
     *
     * @param <T>
     *            type générique
     * @param bean
     *            bean
     * @param version
     *            the version
     * @return marshaller
     */
    private <T> Marshaller marshaller(final T bean, final String version) {
        return this.worker(bean.getClass()).getMarshaller(version);
    }

    /**
     * The Class JaxbWorker.
     */
    static class JaxbWorker {

        /** The clazz. */
        private final Class<?> clazz;

        /** The context. */
        private final JAXBContext context;

        /**
         * Instantiates a new jaxb worker.
         *
         * @param clazz
         *            the clazz
         */
        public JaxbWorker(final Class<?> clazz) {
            this.clazz = clazz;

            try {
                this.context = JAXBContext.newInstance(clazz);
            } catch (final JAXBException ex) {
                throw new TechnicalException("Unable to load JAXB context for class " + clazz.getName(), ex);
            }
        }

        /**
         * Gets the marshaller.
         *
         * @param version
         *            the version
         * @return the marshaller
         */
        public Marshaller getMarshaller(final String version) {
            try {
                final Marshaller marshaller = this.context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.name());

                if (!StringUtils.isEmpty(version)) {
                    marshaller.setProperty( //
                            Marshaller.JAXB_SCHEMA_LOCATION, //
                            MessageFormat.format( //
                                    "http://www.ge.fr/schema/{0}/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/{0}/form-manager-{0}.xsd", //
                                    version //
                            ) //
                    );
                }

                return marshaller;
            } catch (final JAXBException ex) {
                throw new TechnicalException("Unable to create marshaller for class " + this.clazz.getName(), ex);
            }
        }

        /**
         * Gets the unmarshaller.
         *
         * @return the unmarshaller
         */
        public Unmarshaller getUnmarshaller() {
            try {
                final Unmarshaller unmarshaller = this.context.createUnmarshaller();
                unmarshaller.setListener(new Unmarshaller.Listener() {
                });
                return unmarshaller;
            } catch (final JAXBException e) {
                throw new TechnicalException("Unable to create unmarshaller for class " + this.clazz.getName(), e);
            }
        }

    }

}
