# Nash Forge : Dossier d'installation

<!-- MACRO{toc} -->

## Livrables


| GroupID           | Nom du fichier                         | Description                          |
| ----------------- | -------------------------------------- | ------------------------------------ |
| fr.ge.common.nash | nas-client-forge-*x.y.z*.war           | Module applicatif                    |
| fr.ge.common.nash | nas-client-forge-*x.y.z*-config.zip    | Fichier(s) de paramétrage applicatif |

\* *x.y.z* représente la version livrée


## Configuration du socle logiciel

Doivent être provisionnés sur les serveurs les composants suivant :

| Noeud applicatif  | Description                              | Composants techniques requis |
| ----------------- | ---------------------------------------- | ---------------------------- |
| VH_NASH_FORGE     | Frontal du module applicatif ***forge*** | Apache                       |
| APP_NASH_FORGE    | Module applicatif ***forge***            | Tomcat 8.0.x                 |


## Installation du virtual host

| Nom              | Description       | PROD                               |
| ---------------- | ----------------- | ---------------------------------- |
| Module **forge** | URL de l'instance | nash-client-forge.${public_domain} |

Pour l'exemple, dans le répertoire ``/etc/apache2/sites-enabled``, ajouter un virtualhost (ie **nash-client-forge.${public_domain}.conf**) redirigeant vers l'application :

```
# ************************************
# Vhost template in module puppetlabs-apache
# Managed by Puppet
# ************************************
 
<VirtualHost *:8080>
  ServerName nash-client-forge.${public_domain}
 
  ## Vhost docroot
  DocumentRoot "/var/www/nash"
 
  ## Directories, there should at least be a declaration for /var/www/nash
 
  <Location "/status">
    Require valid-user
    AuthType Basic
    AuthName "status"
    AuthUserFile /usr/passwd/status/htpasswd
  </Location>
 
  <Directory "/var/www/nash">
    Options None
    AllowOverride None
    Require all denied
  </Directory>
 
  ## Logging
  ErrorLog "/var/log/apache2/nash-client-forge.${public_domain}_error.log"
  ServerSignature Off
  CustomLog "/var/log/apache2/nash-client-forge.${public_domain}_access.log" combined
 
  JkMount   /* nash-client-forge
 
</VirtualHost>
```

Il est nécessaire d'activer le module **mod_jk** d'Apache gérant la communication et la répartition de charge entre Apache et les serveurs Tomcat.

Ajouter dans le répertoire ``/etc/apache2`` le fichier ``workers.properties``, celui-ci contenant la configuration du module Apache :

``` properties
worker.list=jkstatus,nash-client-forge
 
worker.maintain=240
worker.jkstatus.type=status
worker.server1.port=8009
worker.server1.host=nash-client-forge.${public_domain}
worker.server1.type=ajp13
worker.server1.lbfactor=1
worker.server1.connection_pool_size=250
worker.server1.connection_pool_timeout=600
worker.server1.socket_timeout=900
worker.server1.socket_keepalive=True
 
worker.nash-client-forge.balance_workers=server1
worker.nash-client-forge.type=lb
```



## Installation de l'archive Web

L'installation se fait sous ``/var/lib/tomcat8``.

L'archive Web de l'application doit être déposé sous ``/srv/appli/nash/nash-client-forge``.

La configuration tomcat nécessaire pour le démarrage de l'application se trouve sous ``/var/lib/tomcat8/conf/``.

* Configuration du serveur Catalina : ``Catalina/nash-client-forge.${public_domain}/ROOT.xml``
Le context doit pointer sur le war décrit plus haut.

``` xml : Catalina/.../ROOT.xml
<?xml version="1.0" encoding="UTF-8"?>

        <Context reloadable="true" docBase="/srv/appli/nash/nash-ws-forge/nash-ws-server.war">

        <Resources className="org.apache.catalina.webresources.StandardRoot">
            <PreResources className="org.apache.catalina.webresources.DirResourceSet"
                base="/srv/conf/nash/nash-client-forge"
                webAppMount="/WEB-INF/classes" />
        </Resources>

</Context>
```

Ajout de l'hôte virtuel sur la configuration du serveur : ``/var/lib/tomcat8/conf/server.xml`` .

Ajout de la ligne :

``` xml
<Host name="nash-client-forge.${public_domain}"
      appBase="webapps_nash-client-forge"
      unpackWARs="true"
      autoDeploy="true" />
```

## Configuration de l'application

La configuration externalisée de l'application doit être déposée sous ``/srv/conf/nash/nash-client-forge``.

### Fichier ``application.properties``

| Paramètre                            | Description                                               | Valeur                                   |
| ------------------------------------ | --------------------------------------------------------- | ---------------------------------------- |
| environment                          | Nom de l'environnement                                    | "Développement", "Recette", ... . Ne pas remplir pour cacher le bandeau présent sur toutes les pages |
| ui.theme.default                     | Thème appliqué                                            | ge                                       |
| engine.user.type                     | Type d'utilisateur géré                                   | user                                     |
| mas.tracker.service.url              | URL racine WS ***Tracker***                               | http://{VH_TRACKER_SERVER}/api           |
| mas.tracker.service.author           | Tracker : code auteur                                     | Nash                                     |
| ws.tracker.check.url                 | AppStatus ***Tracker*** : URL                             | http://{VH_TRACKER_SERVER}/status        |
| ws.tracker.check.username            | AppStatus ***Tracker*** : username                        |                                          |
| ws.tracker.check.password            | AppStatus ***Tracker*** : password                        |                                          |
| ws.nash.forge.url                    | URL racine WS Nash ***Forge***                            | http://{VH_NASH_WS_FORGE}/api            |
| ws.nash.forge.check.url              | AppStatus ***Nash Forge*** : URL                          | http://{VH_NASH_WS_FORGE}/status         |
| ws.nash.forge.check.username         | AppStatus ***Nash Forge*** : username                     |                                          |
| ws.nash.forge.check.password         | AppStatus ***Nash Forge*** : password                     |                                          |
| ws.nash.web.url                      | URL racine WS ***Nash Web***                              | http://{VH_NASH_WS_WEB}/api              |
| ws.nash.web.check.url                | AppStatus ***Nash Web*** : URL                            | http://{VH_NASH_WS_WEB}/status           |
| ws.nash.web.check.username           | AppStatus ***Nash Web*** : username                       |                                          |
| ws.nash.web.check.password           | AppStatus ***Nash Web*** : password                       |                                          |
| ws.nash.url                          | URL racine WS ***Nash Web***                              | \${ws.nash.forge.url}                    |
| ws.markov.url                        | URL racine WS ***Markov***                                | http://{VH_NASH_WS_MARKOV}/api           |
| ws.markov.public.url                 | URL publique ***Markov***                                 | http://{VH_NASH_WS_MARKOV}/              |
| ws.markov.check.url                  | AppStatus ***Markov*** : URL                              | http://{VH_NASH_WS_MARKOV}/status        |
| ws.markov.check.username             | AppStatus ***Markov*** : username                         |                                          |
| ws.markov.check.password             | AppStatus ***Markov*** : password                         |                                          |
| feedbackUrl                          | URL script JS Feedback                                    |                                          |
| storage.record.root                  | Chemin répertoire de stockage des dossiers validés        |                                          |
| frontUrl                             | URL public de l'IHM                                       | http://www.guichet-qualification.fr/fr/  |
| ct.authentification.url.modification | URL du profil utilisateur                                 |                                          |
| ct.authentification.url.logout       | URL de déconnexion                                        |                                          |
| ct.authentification.url.exclude      | Expression régulière d'exclusion des pages authentifiées  | ^.*/(status|(css|fonts|images|js)/.*)$   |
| ct.authentification.url.alive        | URL de maintient de la connexion utilisateur              |                                          |
| ct.authentification.webservice.url   | URL racine d'interrogation du module d'authentification   |                                          |
| apientreprise.baseUrl                | URL de l'API Entreprises                                  |                                          |
| apientreprise.token                  | Token fourni via TPS                                      |                                          |
| apientreprise.context                | Voir https://www.apientreprise.fr/documentation           | Tiers                                    |
| apientreprise.recipient              | Voir https://www.apientreprise.fr/documentation           |                                          |
| stamper.check.url                    | AppStatus ***Stamper*** : URL                             | http://{VH_PAYMENT_STAMPER}/status       |
| stamper.check.username               | AppStatus ***Stamper*** : username                        |                                          |
| stamper.check.password               | AppStatus ***Stamper*** : password                        |                                          |
| payment.check.url                    | AppStatus ***Payment*** : URL                             | http://{VH_PAYMENT_SERVER}/status        |
| payment.check.username               | AppStatus ***Payment*** : username                        |                                          |
| payment.check.password               | AppStatus ***Payment*** : password                        |                                          |
| ws.regent.check.url                  | AppStatus ***Regent*** : URL                              | http://{VH_REGENT_SERVER}/status         |
| ws.regent.check.username             | AppStatus ***Regent*** : username                         |                                          |
| ws.regent.check.password             | AppStatus ***Regent*** : password                         |                                          |



### Fichier ``engine.properties``

| Paramètre                          | Description                      | Valeur |
| ---------------------------------- | -------------------------------- | ------ |
| exchange.baseUrl                   | URL racine WS ***Exchange***       | http://{VH_EXCHANGE_SERVER}/api |
| exchange.email.sender              | Emetteur des mails                                               |  |
| slack.baseUrl                      | URL du service ***Incoming Webhook*** de ***Slack***             |  |
| directory.baseUrl                  | URL racine WS ***Directory***                                    | http://{VH_DIRECTORY_SERVER}/api |
| nash.baseUrl                       | URL racine WS ***Nash***                                         | http://{VH_NASH_WS}/api |
| nashBo.baseUrl                     | URL racine WS ***Nash Backoffice***                              |  |
| timeout.loop                       |                                                                  |  |
| stamper.proxy.url.public           | URL publique de ***Stamper***                                    | http://{VH_STAMPER_PUBLIC} |
| stamper.proxy.create.session.url   | URL privé de création de session ***Stamper***                   | http://{VH_STAMPER_PRIVATE}/private/api/v1/proxy/create |
| stamper.proxy.validate.session.url | URL privée de validation de session ***Stamper***                | http://{VH_STAMPER_PRIVATE}/private/api/v1/proxy/verify/{token} |
| stamper.proxy.ui.callback.url      | URL publique de redirection vers ***Nash Forge***                | http://{VH_NASH_FORGE}/record/{code}/{step}/process/{processPhase}/{processId} |
| stamper.proxy.service.callback.url | URL privée de callback de mise à jour de la formalité ***Nash*** | http://{VH_NASH_WS_FORGE}/api/v1/Record/code/{code}/step/{step}/phase/{processPhase}/process/{processId}/{resultFile} |
| payment.proxy.url.public           | URL publique de ***Payment***                                    | http://{VH_PAYMENT_PUBLIC} |
| payment.proxy.create.session.url   | URL privé de création de session ***Payment***                   | http://{VH_PAYMENT_PUBLIC}/private/api/v1/proxy/create |
| payment.proxy.validate.session.url | URL privée de validation de session ***Payment***                | http://{VH_PAYMENT_PUBLIC}/private/api/v1/proxy/verify/{token} |
| payment.proxy.ui.callback.url      | URL publique de redirection vers ***Nash Forge***                | http://{VH_NASH_FORGE}/record/{code}/{step}/process/{processPhase}/{processId} |
| payment.proxy.service.callback.url | URL privée de callback de mise à jour de la formalité ***Nash*** | http://{VH_NASH_WS_FORGE}/api/v1/Record/code/{code}/step/{step}/phase/{processPhase}/process/{processId}/{resultFile} |
| tracker.baseUrl                    | URL racine WS ***Tracker***                                      | http://{VH_TRACKER_SERVER} |
| tracker.author                     | Auteur des messages postés sur ***Tracker***                     | Nash |
| dashboard.bo.url                   | URL racine WS ***Dashboard***                                    | http://{VH_WS_DASHBOARD}/api |
| regent.baseUrl                     | URL racine WS ***Regent***                                       | http://{VH_REGENT_SERVER}/api |


### Fichier ``log4j2.xml``

| Paramètre           | Description                                                  | Valeur                                  |
| ------------------- | ------------------------------------------------------------ | --------------------------------------- |
| log.level           | Niveau de log par défaut                                     | "debug", "info", ...                    |
| log.level_app       | Niveau de log propre au module applicatif                    | "debug", "info", ...                    |
| log.level_fwk       | Niveau de log des librairies externes                        | "debug", "info", ...                    |
| log.file.technical  | Chemin du fichier contenant les logs techniques              | /srv/log/nash/nash-forge/tech.log       |
| log.file.functional | Chemin du fichier contenant les logs fonctionnels            | /srv/log/nash/nash-forge/func.log       |
| log.file.thirdParty | Chemin du fichier contenant les logs des libraires externes  | /srv/log/nash/nash-forge/thirdParty.log |
| log.max_size        | Taille maximale des fichiers de logs journalisés             | 10 MB                                   |
| log.max_index       | Index maximum des fichiers de logs journalisés               | 20                                      |


