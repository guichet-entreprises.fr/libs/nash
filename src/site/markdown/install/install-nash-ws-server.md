# Nash WS Server : Dossier d'installation

<!-- MACRO{toc} -->

## Livrables

| GroupID           | Nom du fichier                      | Description |
| ----------------- | ----------------------------------- | ----------- |
| fr.ge.common.nash | nas-ws-server-*x.y.z*.war           | Module applicatif |
| fr.ge.common.nash | nas-ws-server-*x.y.z*-config.zip    | Fichier(s) de paramétrage applicatif |
| fr.ge.common.nash | nas-ws-data-*x.y.z*-ddl.tar.gz      | Librairie du module applicatif contenant les scripts d'initialisation de la base de données utilisable en l'état avec **FlywayDB** |

\* *x.y.z* représente la version livrée


> **Note :** Le module **nash-ws-server** doit être déployé pour chaque applicatif, ie ***forge*** (rédacteur de formalité, interne), ***web*** (déclarant, public) ...

> Chaque instance doit posséder sa propre base de données.



## Configuration du socle logiciel
Doivent être provisionnés sur les serveurs les composants suivant :

| Noeud applicatif  | Description                   | Composants techniques requis |
| ----------------- | ----------------------------- | ---------------------------- |
| VH_NASH_WS        | Frontal du module applicatif  | Apache                       |
| APP_NASH_WS       | Module applicatif             | Tomcat 8.0.x                 |
| DB_NASH           | Base de données               | PostgreSQL 9.2.x             |



## Installation de la base de données

Les scripts SQL versionnés se trouvent dans le livrable nash-ws-data-*x.y.z*-ddl.tar.gz . Ils sont à exécuter avec l'outil **FlywayDB**.


## Installation du virtual host

| Nom                   | Description               | PROD                          |
| --------------------- | ------------------------- | ----------------------------- |
| Services Web interne  | URL de l'instance interne | nash-ws-${app}.${local_domain} |

Pour l'exemple, dans le répertoire ``/etc/apache2/sites-enabled``, ajouter un virtualhost (ie **nash-ws-${app}.${local_domain}.conf**) redirigeant vers l'application :

```
# ************************************
# Vhost template in module puppetlabs-apache
# Managed by Puppet
# ************************************
 
<VirtualHost *:8080>
  ServerName nash-ws-${app}.${local_domain}
 
  ## Vhost docroot
  DocumentRoot "/var/www/nash"
 
  ## Directories, there should at least be a declaration for /var/www/nash
 
  <Location "/status">
    Require valid-user
    AuthType Basic
    AuthName "status"
    AuthUserFile /usr/passwd/status/htpasswd
  </Location>
 
  <Directory "/var/www/nash">
    Options None
    AllowOverride None
    Require all denied
  </Directory>
 
  ## Logging
  ErrorLog "/var/log/apache2/nash-ws-${app}.${local_domain}_error.log"
  ServerSignature Off
  CustomLog "/var/log/apache2/nash-ws-${app}.${local_domain}_access.log" combined
 
  JkMount   /* nash-ws-${app}
 
</VirtualHost>
```

Il est nécessaire d'activer le module **mod_jk** d'Apache gérant la communication et la répartition de charge entre Apache et les serveurs Tomcat.

Ajouter dans le répertoire ``/etc/apache2`` le fichier ``workers.properties``, celui-ci contenant la configuration du module Apache :

``` properties
worker.list=jkstatus,nash-ws-${app}
 
worker.maintain=240
worker.jkstatus.type=status
worker.server1.port=8009
worker.server1.host=nash-ws-${app}.${local_domain}
worker.server1.type=ajp13
worker.server1.lbfactor=1
worker.server1.connection_pool_size=250
worker.server1.connection_pool_timeout=600
worker.server1.socket_timeout=900
worker.server1.socket_keepalive=True
 
worker.nash-ws-${app}.balance_workers=server1
worker.nash-ws-${app}.type=lb
```



## Installation de l'archive Web

L'installation se fait sous ``/var/lib/tomcat8``.

L'archive Web de l'application doit être déposé sous ``/srv/appli/nash/nash-ws-${app}``.

La configuration tomcat nécessaire pour le démarrage de l'application se trouve sous ``/var/lib/tomcat8/conf/``.

* Configuration du serveur Catalina : ``Catalina/nash-ws-${app}.${local_domain}/ROOT.xml``
 Le context doit pointer sur le war décrit plus haut.
 On doit également décrire les propriétés de connexion de l'application à la base de données **nash_${app}** .

``` xml : Catalina/.../ROOT.xml
<?xml version="1.0" encoding="UTF-8"?>
 
        <Context reloadable="true" docBase="/srv/appli/nash/nash-ws-${app}/nash-ws-server.war">
 
        <Resources className="org.apache.catalina.webresources.StandardRoot">
            <PreResources className="org.apache.catalina.webresources.DirResourceSet"
                base="/srv/conf/nash/nash-ws-${app}"
                webAppMount="/WEB-INF/classes" />
        </Resources>
 
        <Resource name="jdbc/nash_${app}" auth="Container"
                type="javax.sql.DataSource" driverClassName="org.postgresql.Driver"
                url="jdbc:postgresql://data.${local_domain}:5432/nash_${app}?charSet=UNICODE"
                defaultAutoCommit="true" initialSize="1"
                validationQuery="SELECT 1"
                username="nash_${app}_app" password="nash_${app}_pwd"
                maxTotal="20" maxIdle="10" maxWaitMillis="10000"
                removeAbandonedOnBorrow="true" removeAbandonedTimeout="60" logAbandoned="true" />
 
</Context>
```

Ajout de l'hôte virtuel sur la configuration du serveur : ``/var/lib/tomcat8/conf/server.xml`` .

Ajout de la ligne :

``` xml
<Host name="nash-ws-${app}.${local_domain}"
      appBase="webapps_nash-ws-${app}"
      unpackWARs="true"
      autoDeploy="true" />
```

## Configuration de l'application

La configuration externalisée de l'application doit être déposée sous ``/srv/conf/nash/nash-ws-${app}`` .

### Fichier ``application.properties``

| Paramètre          | Description                      | Valeur |
| ------------------ | -------------------------------- | ------ |
| environment        | Nom de l'environnement           | "Développement", "Recette", ... . Ne pas remplir pour cacher le bandeau présent sur toutes les pages |
| jndi.db.datasource | Nom JNDI de la source de données | jdbc/nash_${app} |


### Fichier ``log4j2.xml``

| Paramètre           | Description                                                  | Valeur                                        |
| ------------------- | ------------------------------------------------------------ | --------------------------------------------- |
| log.level           | Niveau de log par défaut                                     | "debug", "info", ...                          |
| log.level_app       | Niveau de log propre au module applicatif                    | "debug", "info", ...                          |
| log.level_fwk       | Niveau de log des librairies externes                        | "debug", "info", ...                          |
| log.file.technical  | Chemin du fichier contenant les logs techniques              | /srv/log/nash/nash-ws-${app}/tech.log         |
| log.file.functional | Chemin du fichier contenant les logs fonctionnels            | /srv/log/nash/nash-ws-${app}/func.log         |
| log.file.thirdParty | Chemin du fichier contenant les logs des libraires externes  | /srv/log/nash/nash-ws-${app}/thirdParty.log   |
| log.max_size        | Taille maximale des fichiers de logs journalisés             | 10 MB                                         |
| log.max_index       | Index maximum des fichiers de logs journalisés               | 20                                            |


