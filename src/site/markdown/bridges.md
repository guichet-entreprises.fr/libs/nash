
## Poster un message dans Tracker
**nash.tracker.post(message)**
 - message : String

## Ajouter le lien entre le dossier courant et une référence à Tracker 
```xml
var uid = '2018-09-ABW-NZF-42';
nash.tracker.link(uid);
```
 - uid : référence du dossier à lier au dossier courant 

## Générer un nouvel uid 
```xml
var uid = nash.util.generateUid();
```
Retourne un nouvel uid.
## Convertir un objet en Json
```xml
nash.util.convertToJson(object);
```
 - object : objet.
Retourne l'objet au format json.

## Encoder un tableau de byte en base 64
```xml
var contentBase64 = nash.util.convertByteToBase64(contentBytes);
```
 - contentBytes : byte[] 
Retourne le contenue encodé en base 64.


## Décoder chaîne en base 64 en un tableau de bytes
```xml
var contentBytes = nash.util.convertBase64ToByte(contentBase64);
```
 - contentBase64 : byte[] 
Retourne le contenue encodé en base 64.

## Vérifier si un objet est une liste
```xml
var isList = nash.util.isList(object);
```
 - object : objet 
Retourne un booleen : True si la condition est vérifiée. False si elle ne l'est pas.

# TransitionProcessorBridge
## Ajouter une step à une formalité
```xml
nash.transition.createStep(bindings);
```
 - bindings : bindings 
 Ajoute une étape à une formalité et actualise le description.xml

## Copier un fichier
```xml
var source = '/1-data/file.pdf';
var destination = '/2-review/file.pdf';
nash.transition.copy(source, destination);
```

# TransformationProcessorBridge
## Transformer un xml en un autre avec une xslt
```xml
var source = '/1-data/file.xml';
var destination = '/1-data/newFile.xml';
var normesXSLT= '/1-data/normes.xslt';
nash.xml.transform(source, normesXSLT, destination);
```

**nash.xml.transform(source, xslt, destination)**
 - source : chemin du fichier à modifier
 - normesXSLT: chemin du fichier contenant les normes xslt
 - destination : chemin de destination du fichier généré 


## Extraire une balise d'un fichier XML à partir de son xpath
```xml
var xmlRegentFile =  '/XML_REGENT.xml' ;
var numeroLiasse = nash.xml.extract(xmlRegentFile, '/REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02');
numeroLiasse = JSON.parse(numeroLiasse);
var numeroLiasseValue = numeroLiasse.C02 ;
```

**nash.xml.extract(xmlFilePath, expression)**
 - xmlFilePath : chemin du fichier xml à parser
 - expression: xpath de la balise à récupérer depuis le fichier xml
 
 
# ServiceProcessorBridge
## Construire une url avec paramètres
```xml
var urlWithParam = nash.service.request(url, params);
```
 - url : String
 - params : Object

# ScriptProcessorBridge
## Charger un objet à partir de son chemin
```xml
var sourcePath = '/1-data/data.xml';
nash.instance.wrap(resourcePath);
```

## Charger un data à partir de son nom
```xml
var sourcePath = '/1-data/data.xml';
nash.instance.load(resourcePath);
```

## Créer un data
```xml
var data = nash.instance.createData(bindings);
```
 - values: Bindings

## Créer un groupe
```xml
var group = nash.instance.createGroup(bindings);
```
 - values: Bindings

# DocumentProcessorBridge
## Créer un nouveau fichier pdf
```xml
var doc = nash.doc.create();
```

## Charger un fichier pdf
```xml
var sourcePath = '/1-data/cerfa.pdf';
var cerfa = nash.doc.load(resourcePath);
```

# RecordWrapper
## Ajouter des metas à un dossier
```xml
var metas = [
        {
            'name' : 'uid',
            'value': 'UID-PARENT-1'	
        },
        {
            'name' : 'uid',
            'value': 'UID-PARENT-2'	
        }
    ];
nash.record.meta(metas);
```
- metas : List&lt;Map&lt;String, String&gt;&gt;

## Récupérer les metas d'un dossier
```xml
var metas = nash.record.getMeta();
```

## Spécifier un auteur pour un dossier
```xml
var auteur = 'toto';
nash.record.setAuthor(auteur);
```

## Récupérer  l'UID d'un dossier
```xml
var uid = nash.record.getSourceUid();
```
## Assigner un uid à un dossier
```xml
var uid = 'TOTO';
nash.record.setSourceUid(uid);
```

## Enregistrer un dossier
```xml
var uid = nash.record.save();
```
- Record : dossier à enregistrer.
La méthode retourne l'uid du dossier après enregistrement.

## Récupérer  le fichier description.xml d'un dossier
```xml
var description = nash.record.getDescription();
```

## Ajouter un fichier à un dossier
```xml
var destination = '/5-dashboard/file.pdf';
var fileToAdd = 'file.pdf';
nash.record.addFile(destination, fileToAdd);
```

## Charger un fichier
```xml
var resourcePath = '/1-data/file.pdf';
var file = nash.record.load(resourcePath);
```

## Setter une url
```xml
var url = 'www.www.www';
nash.record.setBaseUrl(url)
```

# PdfDocument
## Ajouter une page blanche à un pdf
```xml
nash.DocumentProcessor.appendBlankPage();
```

## Lister les champs d'un pdf
```xml
var fields = nash.DocumentProcessor.getFields();
```
Retourne une Map&lt;String, PdfFieldDescription&gt;

## Ajouter du texte à un endroit précis d'un pdf
```xml
nash.DocumentProcessor.fillOverflow(zoneId, phrases, overflowContent, overflowZoneId);
```
- zoneId : String =&gt; zone ou ajouter le texte
- phrases : List&lt;Object&gt; =&gt; liste des phrases à insérer dans le pdf
- overflowContent : PdfDocument
- overflowZoneId : String =&gt; zone ou mettre le texte en cas de débordement dans la première

## Ajouter du texte à un endroit précis d'un pdf
```xml
nash.DocumentProcessor.fillOverflow(zoneId, phrases, overflowContent, overflowZoneId);
```
- zoneId : String =&gt; zone ou ajouter le texte
- phrases : List&lt;Object&gt; =&gt; liste des phrases à insérer dans le pdf
- overflowContent : PdfDocument
- overflowZoneId : String =&gt; zone ou mettre le texte en cas de débordement dans la première



# TrackerProcessorBridge
## Fait un appel à tracker pour lier une référence au numéro de dossier en cours
```xml
var liasseNumber = 'H10000123456';

nash.tracker.link(liasseNumber);
```

**nash.tracker.post(reference)**
 - reference : numéro de la référence à lier avec l'id du dossier en cours
