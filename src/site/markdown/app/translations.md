
# Translations

v1.1 - 12/12/2016

This documentation describes how all types of labels are translated in the forms manager.

## Types of labels to translate

This is a list of the different types of labels and whether they're translated during the HTML generation or with a JS script:

* main PO file
    * UI (menus, etc.) => HTML
    * JS libraries
        * Parsley => JS
        * widgets
            * bootstrap-datepicker => JS
            * ...
* forms upload process validation PO file
    * error messages => HTML
* types PO files
    * description / options (types development tool) => HTML
    * HTML templates => HTML
    * custom validation => JS
* forms PO files
    * forms questions labels, descriptions, help, warnings, ... => HTML
    * forms JS preprocess or JS postprocess => JS
    * referentials options labels => JS

## How to translate

Thymeleaf uses a template engine for the execution of HTML templates. The forms manager uses a specific template engine, FormsTemplateEngine, to reference FormsMessageResolver. FormsMessageResolver allows to search translations from different sources, depending on what objects are found in the templates context :

* from the types if a value adapter is found
* from the forms if a specification loader is found
* from the main PO file otherwise

The Spring message source mechanism is replaced with the MessageReader of the forms manager. MessageReader allows to load a resource bundle :

* from a base name, with the classic resource bundle mechanism
* from a value adapter, with the classic resource bundle mechanism
* from a specification loader, loading a resource bundle from a memory object

A locale can be provided but it is optional. It defaults to Spring locale context holder if it is not present.

The MessageReader delegates to the MessageFormatter the responsibility to retrieve and format the messages from the resource bundle. The MessageReader is also able to extract a set of keys/values from the resource bundle in order to, for example, enhance the JS translations set (custom validation, ...).

![Forms questions translations](../images/uml/forms-questions-translations.png "Forms questions translations")
