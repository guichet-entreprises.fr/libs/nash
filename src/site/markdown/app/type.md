
# Adding a data type

v1.2 - 13/12/2016

This documentation describes how a Java developer can add a simple data type.

<!-- MACRO{toc} -->


## Type folder

The files related to a type must be added in a package or folder fr.ge.common.form.engine.adapter.vM&#95;m, where M is the major version number and m is the minor version number of the version from which the type will be available. In the rest of the document, "&lt;version&gt;" refers to this location. For example, if we want to add a type FooBar in version 42.3, a folder fr.gouv.ge.form.engine.adapter.v42&#95;3 must be created, and the implementation of FooBar put inside. In order to say that version 42.3 inherits from version 42.2 except the type BarBaz, a package-info file must be created with the annotation @InheritTypes(version = "42.2", except="BarBaz").

## Needed files

* ValueAdapter (mandatory)
* Thymeleaf template (mandatory)
* JS files (at least one)
* CSS files (optional)
* PO translation files (optional)
* PNG format preview (mandatory)

### ValueAdapter

A ValueAdapter file must be added to the definition of a type, in order to define its behavior (values conversion, etc.). It must be placed in the package &lt;version&gt; (Java side).

This file must respect the naming "&lt;name-of-the-type&gt;ValueAdapter.java".

The ValueAdapter must implement the IValueAdapter interface or extend an already existing type. If we want to design various types from an existing type, it is not always necessary to override all the methods of the parent type. It can be sufficient to define a new type and make a reference to a new template.

### Thymeleaf template

A Thymeleaf template must be added to the definition of a type, in order to generate the HTML of the fields of this type. This file must be placed in &lt;version&gt;.&lt;ValueAdapter-name&gt;Ressources (ressources side).

This file must respect the naming "template.html".

This template can mainly be parameterized by the following objects :

| Object            | Description                           |
|-------------------|---------------------------------------|
| ${prefix}         | Prefix of the identifier of the field |
| ${data.id}        | Identifier of the field               |
| ${data.mandatory} | Is the field mandatory ?              |
| ${value}          | Default value of the field            |

The name of the template must be referenced by the ValueAdapter.

### JS files

Javascript files can be added to the definition of a type (at least one), in order to add dynamic behaviors. These files must be placed in &lt;version&gt;.&lt;ValueAdapter-name&gt;Ressources (ressources side).

These files must respect the naming "script&lt;index&gt;.js", where the value of index, which determines the order of inclusion, is 1 for the first script and is incremented for each other scipt. It is recommended to indicate the original name of the scripts in a README.txt file.

The following libraries are provided in order to develop the Javascript :

* The libraries declared in bower.json (jQuery 2.2.4 and jQuery UI 1.11.4 included)
* The specific libraries declared in commons.js

You can use any of the provided libraries by including them with the require.js notation.

Value adapters are expected to implement a minimum interface, like so :

    define([ 'jquery', 'other/dependency' ], function($) {
    
        var obj = {
            initialize : function(field, opts) {
                // initialize the component by calling external libraries, for example
            },
            validate : function(data, opts) {
                return {
                    validated : false
                    value : "value if validated",
                    message : "error message if not validated"
                };
            }
        };
    
        return obj;
    });

The methods of this interface include some arguments :

| Argument          | Description                                             |
|-------------------|-------------------------------------------------------- |
| field             | The div of the field which has the data-field attribute |
| opts              | A map of type options names with their values           |
| data              | A formContentData object (see formContentReader.js)     |

### CSS files

Optional CSS files can possibly be added to the definition of a type, in order to change the style. These files must be placed in &lt;version&gt;.&lt;ValueAdapter-name&gt;Ressources (ressources side).

These files must respect the naming "style&lt;index&gt;.css", where the value of index, which determines the order of inclusion, is 1 for the first style sheet and is incremented for each other style sheet.

The CSS 3 is authorized.

### PO files

Optional PO files can possibly be added to the definition of a type, in order for its description to be translated in the development tool listing the types. These files must be placed in a tree structure of PO files which must be located in &lt;version&gt;.&lt;ValueAdapter-name&gt;Ressources/translation (ressources side).

These files must respect the naming "messages.po".

For example, the french translation will be located in &lt;version&gt;.&lt;ValueAdapter-name&gt;Ressources/translation/fr/LC&#95;MESSAGES/messages.po.

### PNG file

A preview of a field of the new type must be added to the definition of the type, in order to expand the types gallery. This file must be placed in &lt;version&gt;.&lt;ValueAdapter-name&gt;Ressources (ressources side).

This file must respect the naming "preview.png".
