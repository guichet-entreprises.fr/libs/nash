
# Referentials

v1.0 - 12/12/2016

This documentation describes how referential fields are displayed and saved in the forms manager.

## Steps to display and save a referential component

A referential is composed of texts. This is a list of the steps to process XML files, display a referential component for a field and save the selected value:

* gather all the referential tags linked to the field:
    * referentials in external files
    * referentials in the type configuration of the field
* merge the texts:
    * for each text, build a list of the display conditions of all occurrences of this text with the same id
    * keep the first label which is not empty among all the occurrences
* translate all the texts labels, using the translation system
* send the mapping between a field id and its referential texts to the client
* process the list of display conditions of each text, joining them with an "AND" operator, and keeping a cache of already evaluated conditions (JS)
* add referential texts to the referential component if they must be displayed (JS)
* get back the submitted field name with its selected referential texts ids
* gather
* merge
* get back the labels of the selected referential texts

![Display referential component](../images/uml/display-referential-component.png "Display referential component")

## Example

### XML

description.xml:

    <referentials>
        <referential id="fruits">referentials/fruits.xml</referential>
    </referentials>

fruits.xml:

    <referential>
        <text id="ba">Banana</text>
        <text id="or">Orange</text>
        <text id="peac">Peach</text>
        <text id="ma">Mango</text>
        <text id="pear">Pear</text>
        <text id="pu">Pumpkin</text>
    </referential>

data.xml:

    <data id="referentialField1" label="Referential field 1" type="ComboBox(ref:'fruits')">
        <conftype>
            <referential if="condition1">
                <text id="ba" if="condition2"></text>
                <text id="ap">Apple</text>
            </referential>
            <referential if="false">
                <text id="ma"></text>
            </referential>
        </conftype>
    </data>
    <data id="referentialField2" label="Referential field 2" type="ComboBox">
        <conftype>
            <referential if="condition3">
                <text id="opt1">Label 1</text>
                <text id="opt2">Label 2</text>
            </referential>
        </conftype>
    </data>

### Gather

referentialField1:

    referential:
        text id="ba" label="Banana"
        text id="or" label="Orange"
        text id="peac" label="Peach"
        text id="ma" label="Mango"
        text id="pear" label="Pear"
        text id="pu" label="Pumpkin"
    referential if="condition1":
        text id="ba" if="condition2"
        text id="ap" label="Apple"
    referential if="false":
        text id="ma"

referentialField2:

    referential if="condition3":
        text id="opt1" label="Label 1"
        text id="opt2" label="Label 2"

### Merge

referentialField1:

    text id="ba" if="[condition1, condition2]" label="Banana"
    text id="or" label="Orange"
    text id="peac" label="Peach"
    text id="ma" if="[false]" label="Mango"
    text id="pear" label="Pear"
    text id="pu" label="Pumpkin"
    text id="ap" if="[condition1]" label="Apple"

referentialField2:

    text id="opt1" if="[condition3]" label="Label 1"
    text id="opt2" if="[condition3]" label="Label 2"

### Translate

referentialField1:

    text id="ba" if="[condition1, condition2]" label="Banana" translation="Banane"
    text id="or" label="Orange" translation="Orange"
    text id="peac" label="Peach" translation="Pêche"
    text id="ma" if="[false]" label="Mango" translation="Mangue"
    text id="pear" label="Pear" translation="Poire"
    text id="pu" label="Pumpkin" translation="Citrouille"
    text id="ap" if="[condition1]" label="Apple" translation="Pomme"

referentialField2:

    text id="opt1" if="[condition3]" label="Label 1" translation="Libellé 1"
    text id="opt2" if="[condition3]" label="Label 2" translation="Libellé 2"

### Process the display conditions (JS)

referentialField1:

    text id="ba" if="condition1 AND condition2" label="Banana" translation="Banane"
    text id="or" label="Orange" translation="Orange"
    text id="peac" label="Peach" translation="Pêche"
    text id="ma" if="false" label="Mango" translation="Mangue"
    text id="pear" label="Pear" translation="Poire"
    text id="pu" label="Pumpkin" translation="Citrouille"
    text id="ap" if="condition1" label="Apple" translation="Pomme"

referentialField2:

    text id="opt1" if="condition3" label="Label 1" translation="Libellé 1"
    text id="opt2" if="condition3" label="Label 2" translation="Libellé 2"

### Add referential texts to the referential component (JS)

referentialField1:

    Banana (fr : Banane)
    Orange (fr : Orange)
    Peach (fr : Pêche)
    Pear (fr : Poire)
    Pumpkin (fr : Citrouille)
    Apple (fr : Pomme)

referentialField2:

    Label 1 (fr : Libellé 1)
    Label 2 (fr : Libellé 2)
