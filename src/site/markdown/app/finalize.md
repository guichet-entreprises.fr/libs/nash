# Documentation of finalize step:
v1.0 - 09/01/2017

In this Step we display to the user this html page.

<img alt="Kinematic" src="../images/spec/finalize_page.png" style="height: 400px; border: 1px solid black;" />

When the user click on finalize buttom, we save his record in database and we store zip file in file system.

# Storage of record in file system:

To store record in file system we use four variables to construct the absolute path of zip record.

* the list of variable is:
    * Home path = /data/records/
    * Deposit date = YYYY-MM/DD
    * Functional tree = the value of reference-id  in description.xml  file.(example : /scn/gq/poulet)
	 * Uid of record  like  2016-12-HSG-QHF-35

* the path of zip record is: /home_path/deposit_date/functional_tree/uid_of_record.zip.

# Managing error of file system: 

If we don’t have write access in file system we display this html page to the user, to ask him to try again later and we log stack trace of the error. 

<img alt="Kinematic" src="../images/spec/error_page.png" style="height: 400px; border: 1px solid black;" />

# Download Zip Record:

After this step the user can’t change anything in his record, but he can download it in zip file format.

<img alt="Kinematic" src="../images/spec/download_record.png" style="height: 400px; border: 1px solid black;" />
