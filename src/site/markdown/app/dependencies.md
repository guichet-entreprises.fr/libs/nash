
# Dependencies

v1.0 - 13/12/2016

This documentation describes the Maven dependencies of the forms manager and shows the modules that are to be packaged and deployed.

![Dependencies](../images/uml/dependencies.png "Dependencies")
