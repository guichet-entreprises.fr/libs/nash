
# Tutorial

v1.3 - 14/11/2016

## Install your environment with auto-completion

In this tutorial, we will install the specification development environment and create a form from scratch. If you do not need auto-completion, go to the next paragraph to install a light environment.

Go to the <a href="https://eclipse.org/downloads/">Eclipse web site</a> and click on the "Download 64 bit" button.
<br /><img alt="Eclipse site" src="../images/spec/tutorial/eclipse-site.png" style="height: 300px; border: 1px solid black;" />

Click on the "download" button.
<br /><img alt="Eclipse download" src="../images/spec/tutorial/eclipse-download-1.png" style="height: 300px; border: 1px solid black;" />

Save the installer where you prefer.
<br /><img alt="Eclipse download" src="../images/spec/tutorial/eclipse-download-2.png" style="height: 300px; border: 1px solid black;" />

Once downloaded, double-click on the installer and choose the profile "Eclipse IDE for Java EE Developers".
<br /><img alt="Eclipse profile" src="../images/spec/tutorial/eclipse-profile.png" style="height: 300px; border: 1px solid black;" />

Install the programme where you prefer and click on the "install" button.
<br /><img alt="Eclipse installation folder" src="../images/spec/tutorial/eclipse-installation-folder.png" style="height: 300px; border: 1px solid black;" />

Accept the Eclipse Foundation Software User Agreement.
<br /><img alt="Eclipse user agreement" src="../images/spec/tutorial/eclipse-user-agreement.png" style="height: 300px; border: 1px solid black;" />

Wait for the installation.
<br /><img alt="Eclipse installing" src="../images/spec/tutorial/eclipse-installing.png" style="height: 300px; border: 1px solid black;" />

Accept the licenses.
<br /><img alt="Eclipse licenses" src="../images/spec/tutorial/eclipse-licenses.png" style="height: 300px; border: 1px solid black;" />

Once the installation is finished, click on the "launch" button or on the Eclipse shortcut.
<br /><img alt="Eclipse installation finished" src="../images/spec/tutorial/eclipse-installation-finished.png" style="height: 300px; border: 1px solid black;" />

Wait for Eclipse to launch.
<br /><img alt="Eclipse launch" src="../images/spec/tutorial/eclipse-launch.png" style="height: 300px; border: 1px solid black;" />

Choose your workspace. You may use the default path.
<br /><img alt="Eclipse select workspace" src="../images/spec/tutorial/eclipse-select-workspace.png" style="height: 300px; border: 1px solid black;" />

Close Eclipse "Welcome" tab.
<br /><img alt="Eclipse welcome" src="../images/spec/tutorial/eclipse-welcome.png" style="height: 300px; border: 1px solid black;" />
