
# Specification of a form

v1.5 - 12/12/2016

This documentation describes the procedure to specify a new form.

## Table of contents

* [Defining the steps](#Defining_the_steps)
* [Content of a step](#Content_of_a_step)
    * [Data collecting](#Data_collecting)
    * [Preprocess/postprocess](#Preprocesspostprocess)
        * [Attachments](#Attachments)
        * [PDF](#PDF)
        * [Script](#Script)
    * [Referential](#Referential)

## Defining the steps

In order to start a new form, a folder must be created and a file "description.xml" placed inside, which looks like:

    <?xml version="1.0" encoding="UTF-8" ?>
    <description lang="fr" version="1.2" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">
        <reference-id revision="1">cerfa14004*02</reference-id>
        <title>Gîte ou meublé de tourisme</title>
        <description>Déclaration en mairie des meublés de tourisme</description>
        <referentials>
            <referential id="fruits">referentials/fruits.xml</referential>
        </referentials>
        <translations default="en">
            <for lang="en">messages/en.po</for>
            <for lang="es">messages/es.po</for>
        </translations>
        <progress step-id="context" />
        <steps>
            <step id="context" data="0-context/context.xml" icon="question" />
            <step id="data" data="1-data/data.xml" icon="pencil" />
            <step id="attachment" data="2-attachment/data-generated.xml" preprocess="2-attachment/preprocess.xml" postprocess="2-attachment/postprocess.xml" icon="paperclip" />
            <step id="review" data="3-review/data-generated.xml" preprocess="3-review/preprocess.xml" icon="thumbs-o-up" />
            <step id="finished" data="4-finish/greetings.xml" preprocess="4-finish/preprocess.xml" icon="trophy" />
        </steps>
    </description>

A "&lt;description&gt;" tag must be at the root of the document and contains the following attributes:

| Name    | Mandatory | Description                                                                  |
| ------- | --------- | ---------------------------------------------------------------------------- |
| lang    | No        | ISO 639-1 code ('fr', 'en', ...) of the language used for designing the form |
| version | Yes       | Version of the forms manager to use                                          |

The "&lt;description&gt;" tag can contain the following sub-tags:

| Name         | Mandatory | Description                                                                              |
| ------------ | --------- | ---------------------------------------------------------------------------------------- |
| reference-id | Yes       | Technical identifier of the form (for example, the name of the CERFA it generates)       |
| title        | Yes       | Title of the form that appears in the list of suggested forms                            |
| description  | No        | Description of the administrative process corresponding to the form                      |
| referentials | No        | Referentials of the form, used by referential components (see below)                     |
| translations | No        | Translations of the form, used if the user language is not the form language (see below) |
| progress     | Yes       | Technical identifier of the first step of the form (see below)                           |
| steps        | Yes       | List of the steps of the form (see below)                                                |

The "&lt;referentials&gt;" tag contains a set of "&lt;referential&gt;" sub-tags, each of which contains the following attributes:

| Name              | Mandatory | Description                                                                                |
| ----------------- | --------- | ------------------------------------------------------------------------------------------ |
| id                | Yes       | Technical identifier of the referential, that will be referenced by referential components |
| &lt;XML value&gt; | Yes       | Relative path to the referential file                                                      |

The "&lt;translations&gt;" tag contains the following attributes:

| Name    | Mandatory | Description                                                                                                                                                |
| ------- | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| default | No        | ISO 639-1 code ('fr', 'en', ...) of the default translation language, to force a translation being displayed if the user language is not the form language |

The "&lt;translations&gt;" tag contains a set of "&lt;for&gt;" sub-tags, each of which contains the following attributes:

| Name              | Mandatory | Description                                                                                                                               |
| ----------------- | --------- | ----------------------------------------------------------------------------------------------------------------------------------------- |
| lang              | Yes       | ISO 639-1 ('fr', 'en', ...) code of a translation language                                                                                |
| &lt;XML value&gt; | Yes       | Relative path to the translation file in the PO format: https://www.gnu.org/savannah-checkouts/gnu/gettext/manual/html_node/PO-Files.html |

The "&lt;steps&gt;" tag contains a set of "&lt;step&gt;" sub-tags, each of which contains the following attributes:

| Name        | Mandatory | Description                                                                        |
| ----------- | --------- | ---------------------------------------------------------------------------------- |
| id          | Yes       | Technical identifier of the step                                                   |
| icon        | No        | Icon that depicts the step, chosen from http://fontawesome.io/icons/               |
| data        | No        | Relative path to the file describing data collecting, which file may not exist yet |
| preprocess  | No        | Relative path to the file describing the processes before data collecting          |
| postprocess | No        | Relative path to the file describing the processes after data collecting           |


Thereby, the example of XML file above gives us a tree structure which looks like:

<img alt="Draft of a form zip" src="../images/spec/zip-beginning.png" style="height: 300px; border: 1px solid black;" />

## Content of a step

As explained previously, a step can contain several configuration files.

### Data collecting

Data collecting is described by the configuration file referenced in "data", which looks like:

    <?xml version="1.0" encoding="UTF-8" ?>
    <form id="data" label="Déclaration en mairie des meubles de tourisme" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">
        <description>
            La loi vous oblige à remplir ce formulaire et à l'adresser au maire de la commune [...]
        </description>
        <help>
            (1) Art. L. 324-1-1 : « Toute personne qui offre à la location un meublé de tourisme [...]
        </help>
        <warning id="globalWarning" label="Ceci est un avertissement" />
        <default>
            <data mandatory="true" type="String" />
        </default>
        <group id="notifierIdentification" label="Identification du déclarant">
            <description>
                La loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés s'applique [...]
            </description>
            <data id="lastname" label="Votre nom" />
            <data id="firstname" label="Votre prénom" />
            <data id="phoneNumber" label="Votre n° de téléphone" mandatory="false" />
        </group>
        <group id="furnituredHomeIdentification" label="Identification du meublé de tourisme">
            <data id="isClassed" label="Le meublé est-il classé ?" type="Boolean" />
            <data id="classificationDate" label="Date de classement" if="$isClassed" type="Date" />
            <data id="classificationDevel" label="Niveau de classement" if="$isClassed" type="ComboBox">
                <help>nombre d'étoiles</help>
                <conftype>
                    <referential>
                        <text id="1">1 étoile</text>
                        <text id="2">2 étoiles</text>
                        <text id="3">3 étoiles</text>
                        <text id="4">4 étoiles</text>
                    </referential>
                </conftype>
            </data>
        </group>
        <group id="periods" label="Périodes prévisionnelles de location">
            <data id="allYear" label="Toute l'année" type="Boolean" />
            <data id="forecastPeriods" label="Si non, préciser la ou les périodes prévisionnelles de location" type="Date">
                <if>not($allYear)</if>
            </data>
        </group>
        <group id="signature">
            <data id="location" label="Fait à" />
            <data id="date" label="Le" type="Date" />
            <data id="isSigned" label="Signature" type="Boolean" />
            <warning id="editWarning" label="Avertissement">Tout changement concernant les informations [...]</warning>
        </group>
    </form>

A "&lt;form&gt;" tag must be at the root of the document and contains the following attributes:

| Name  | Mandatory | Description                                                          |
| ----- | --------- | -------------------------------------------------------------------- |
| id    | Yes       | Technical identifier of the step (for example, the name of the file) |
| label | No        | Label of the step                                                    |

The "&lt;form&gt;" tag can contain the following sub-tags:

| Name        | Mandatory | Description                                                                               |
| ----------- | --------- | ----------------------------------------------------------------------------------------- |
| description | No        | Description of the step that appears on the screen                                        |
| help        | No        | Help that appears in a frame, under the description                                       |
| warning     | No        | Warning that appears in a frame, under the help                                           |
| default     | No        | Default configuration of the fields                                                       |
| group       | No        | Group of fields (there can be several)                                                    |
| type        | No        | Definition of a complex type, allowing to copy-paste a same group of fields several times |

The "&lt;warning&gt;" tag contains the following attributes:

| Name  | Mandatory | Description                             |
| ----- | --------- | --------------------------------------- |
| id    | Yes       | Technical identifier of the warning     |
| label | Yes       | Text to display                         |
| if    | No        | Display condition (see <a href="tools/index.html">Tools</a>) |

The "&lt;default&gt;" tag can contain a "&lt;data&gt;" sub-tag which contains the following attributes:

| Name      | Mandatory | Description                                      |
| --------- | --------- | ------------------------------------------------ |
| mandatory | No        | Must the fields be filled by default ?           |
| type      | No        | Default type of the fields (see <a href="tools/index.html">Tools</a>) |

The "&lt;group&gt;" tag contains the following attributes:

| Name  | Mandatory | Description                                                                           |
| ----- | --------- | ------------------------------------------------------------------------------------- |
| id    | Yes       | Technical identifier of the group                                                     |
| label | No        | Label of the group                                                                    |
| if    | No        | Display condition (if not in an attribute) (see <a href="tools/index.html">Tools</a>) |

The "&lt;group&gt;" tag can contain the following sub-tags:

| Name        | Mandatory | Description                                                                        |
| ----------- | --------- | ---------------------------------------------------------------------------------- |
| description | No        | Description of the group of fields that appear on top of the group                 |
| help        | No        | Help that appears in a frame, under the description                                |
| warning     | No        | Warning that appears in a frame, under the help                                    |
| data        | No        | Definition of a field (there can be several)                                       |
| group       | No        | Sub-group of fields (there can be several)                                         |
| if          | No        | Display condition (if not in a sub-tag) (see <a href="tools/index.html">Tools</a>) |

The "&lt;data&gt;" tag contains the following attributes:

| Name      | Mandatory | Description                                                                           |
| --------- | --------- | ------------------------------------------------------------------------------------- |
| id        | Yes       | Technical identifier of the field                                                     |
| label     | No        | Label of the field                                                                    |
| mandatory | No        | Must the field be filled by default ?                                                 |
| type      | No        | Type of the field (see <a href="tools/index.html">Tools</a>)                          |
| if        | No        | Display condition (if not in an attribute) (see <a href="tools/index.html">Tools</a>) |

The "&lt;data&gt;" tag can contain the following sub-tags:

| Name        | Mandatory | Description                                                                        |
| ----------- | --------- | ---------------------------------------------------------------------------------- |
| description | No        | Description of the step that appears on top of the field                           |
| help        | No        | Help that appears in a frame, under the description                                |
| warning     | No        | Warning that appears in a frame, under the help                                    |
| if          | No        | Display condition (if not in a sub-tag) (see <a href="tools/index.html">Tools</a>) |
| value       | No        | Default value, valid regarding the type of the field                               |
| conftype    | No        | Additional type configuration                                                      |

The "&lt;conftype&gt;" tag can contain the following sub-tags:

| Name        | Mandatory | Description                                                |
| ----------- | --------- | ---------------------------------------------------------- |
| referential | No        | See the <a href="#Referential">"Referential" paragraph</a> |

### Preprocess/postprocess

Processes before data collecting are described by the configuration file referenced in "preprocess". Processes after data collecting are described by the configuration file referenced in "postprocess". The file format of the "preprocess" and "postprocess" processes is the same.

A "&lt;processes&gt;" tag must be at the root of the document and contains the following attributes:

| Name | Mandatory | Description                                  |
| ---- | --------- | -------------------------------------------- |
| id   | Yes       | Technical identifier of the set of processes |

The "&lt;processes&gt;" tag can contain several occurrences of the "&lt;process&gt;" sub-tag, which contains the following attributes:

| Name   | Mandatory | Description                                             |
| ------ | --------- | ------------------------------------------------------- |
| id     | Yes       | Technical identifier of the process                     |
| type   | No        | The type of process: "attachment", "pdf" or "script"    |
| input  | No        | The name of the process input file                      |
| output | No        | The name of the process output file                     |
| script | No        | The name of the file which contains a script to execute |

#### Attachments

Example of process of type "attachment":

    <?xml version="1.0" encoding="UTF-8"?>
    <processes id="attachmentPreprocess" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">
        <process id="preprocess" type="attachment" input="types.xml" output="data-generated.xml" script="preprocess.js" />
    </processes>

File "types.xml":

    <?xml version="1.0" encoding="UTF-8"?>
    <form xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">
        <type id="identity" label="Pièce d'identité {{$label}}">
            <description>
                Une copie recto/verso de la carte nationale d'identité, [...]
            </description>
        </type>
        <type id="famillyBook" label="Livret de famille">
            <description></description>
        </type>
    </form>

File "preprocess.js":

    attachment('identity', 'identityMain', {
        label: $data.notifierIdentification.firstname + ' ' + $data.notifierIdentification.lastname
    });

In this example, this process will be executed before data collecting of the current step. The script "preprocess.js" will retrieve the information filled in the group "notifierIdentification" of the previous step, which identifier is "data". This information will be used to perform substitutions in the file type which identifier is "identity" in "types.xml". In the name of this file type, the variable "{{$label}}" will thus be replaced by "user.firstname + ' ' + user.lastname". The file resulting of these substitutions is configured to be named "data-generated.xml". It will contain the name of the sent attachment, in a variable which identifier is "identityMain".

By default, all attachment fields are mandatory. If you want to make them optional, use "false" as the "mandatory" parameter.

    attachment('identity', 'identityMain', {
        label: $data.notifierIdentification.firstname + ' ' + $data.notifierIdentification.lastname
    }, { mandatory:"false"} /* mandatory */);

#### PDF

Example of process of type "pdf":

    <?xml version="1.0" encoding="UTF-8"?>
    <processes id="attachmentPostprocess" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">
        <process id="postprocess" type="pdf" input="data-generated.xml" output="data.xml" />
    </processes>

In this example, this process will be executed after data collecting of the currents step. The input file "data-generated.xml", which was potentially created by a "preprocess" process, contains the name of the file to convert to PDF. The output file "data.xml" is a copy of the input file "data-generated.xml", in which the name of the file to convert was substituted with the name of the file converted to the PDF format.

#### Script

Example of process of type "script":

    <?xml version="1.0" encoding="UTF-8"?>
    <processes id="review" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">
        <process id="preprocess" type="script" output="data-generated.xml" script="preprocess.js" />
    </processes>

File "preprocess.js":

    var cerfaFields = {};
    cerfaFields['title']             = ($data.notifierId.title=='Mister');
    cerfaFields['firstname']         = $data.notifierId.firstname;
    cerfaFields['lastname']          = $data.notifierId.lastname;
    cerfafield['titleLastFirstname'] = $data.notifierId.title + ' ' + $data.notifierId.lastname + $data.notifierId.firstname; 
    / The PDF field 'titleLastFirstname' will be filled with the concatenation of the data.xml fields 'title', 'lastname' and 'firstname'./
    cerfaFields['phoneNumber']       = $data.notifierId.phoneNumber != null  $data.notifierId.phoneNumber : '';
    / The PDF field 'phoneNumber' will be filled with the data.xml file 'phoneNumber' only if this field is different of "null". Otherwise "null" will be displayed on the PDF file./
    cerfaFields['booleanYes']        = $data.idboolean ? true : ''; 
    / The PDF field 'booleanYes' will be checked if the data.xml field 'idboolean' is checked./
    cerfaFields['booleanNo']         = $data.idboolean ? '' : true; 
    / The PDF field 'booleanYes' will be checked if the data.xml field 'idboolean' is not checked./
    cerfaFields['booleanCheckBox']   = $data.idbooleanCheckBox; 
    / The PDF field 'booleanCheckBox will be checked if the data.xml field 'idbooleanCheckBox' id checked./
    cerfaFields['CheckBoxesChoice1'] = (Value('id').of($data.idcheckBoxes).contains('1') ? true : ''); 
    / The PDF field 'CheckBoxesChoice1' will be checked if the choice 1 of the data.xml filed 'idcheckBoxes' is checked./

    var cerfa = pdf.create('models/cerfa14004-02.pdf', cerfaFields);
    var cerfaPdf = pdf.save('cerfa14004-02.pdf', cerfa);
    
    return spec.create({
        id : 'review',
        label : 'Review',
        groups : [ spec.createGroup({
            id : 'generated',
            label : 'Formulaires générés',
            data : [ spec.createData({
                id : 'cerfa14004',
                label : 'Cerfa 14004*2',
                description : 'Cerfa obtenu à partir des données saisies',
                type : 'FileReadOnly',
                value : [ cerfaPdf ]
            }) ]
        }) ]
    });

In this example, the process will be executed before data collecting of the current step. The script "preprocess.js" will retrieve the information filled in the group "notifierIdentification" of the previous step, which identifier is "data". This information will be used to perform substitutions in a fillable PDF file (see <a href="tools/index.html">Tools</a>). Here, it is the file "models/cerfa14004-02.pdf". In this file, the field "firstname" will thus be filled with the value "user.firstname" and the field "lastname" will be filled with the value "user.lastname". Then, the PDF is saved in a field of file type, inside a group, itself inside a file of type "data". The output file is configured to be named "data-generated.xml".

### Referential

A referential is described by the file referenced in "referential", or by a type configuration. When in a separate file, it looks like:

    <?xml version="1.0" encoding="UTF-8" ?>
    <referential xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://www.ge.fr/schema/form-manager-1.2.xsd">
        <text id="ba">Banana</text>
        <text id="or">Orange</text>
        <text id="peac">Peach</text>
        <text id="ma">Mango</text>
        <text id="pear">Pear</text>
        <text id="pu">Pumpkin</text>
    </referential>

A "&lt;referential&gt;" tag must be at the root of the document if in a separate file, or at the root of type configuration otherwise, and contains the following attributes:

| Name  | Mandatory | Description                                                                                                                      |
| ----- | --------- | -------------------------------------------------------------------------------------------------------------------------------- |
| if    | No        | Display condition (if not in a sub-tag and the referential is not in a separate file) (see <a href="tools/index.html">Tools</a>) |

The "&lt;referential&gt;" tag contains a set of "&lt;text&gt;" sub-tags, each of which contains the following attributes:

| Name              | Mandatory | Description                                                                                                                         |
| ----------------- | --------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| id                | Yes       | Technical id of the referential option                                                                                              |
| if                | No        | Display condition (if not in an attribute and the referential is not in a separate file) (see <a href="tools/index.html">Tools</a>) |
| &lt;XML value&gt; | No        | Label of the referential option                                                                                                     |
