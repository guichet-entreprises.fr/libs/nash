
# Tutorial

v1.4 - 28/02/2017

## Table of contents

* [Install your environment](#Install_your_environment)
* [Download your template](#Download_your_template)
* ["Description" file](#aDescription_file)
* ["Data" step](#aData_step)
* ["Attachment" step](#aAttachment_step)
* ["Review" step](#aReview_step)
* ["Finished" step](#aFinished_step)
* [Upload your form](#Upload_your_form)


## Install your environment

You can either install complete developpement environment, enabling XML completion, using [Eclipse](install_eclipse.html), or using lightweight tools using [NotePad++](install_notepad.html)


## Download your template

In this section, we will download a template from the documentation.

Go to the forms manager documentation web site and click on "Specification development documentation".
<br /><img alt="Doc root" src="../images/spec/tutorial/doc-root.png" style="height: 300px; border: 1px solid black;" />

Click on the "Presentation" sub-menu on the left.
<br /><img alt="Doc menu presentation" src="../images/spec/tutorial/doc-menu-presentation.png" style="height: 300px; border: 1px solid black;" />

Scroll down and click on the "basic form template" link to download it.
<br /><img alt="Doc download template" src="../images/spec/tutorial/doc-download-template.png" style="height: 300px; border: 1px solid black;" />

Unzip the "basic form template" and open the folder.
<br /><img alt="Folder root" src="../images/spec/tutorial/folder-root-1.png" style="height: 300px; border: 1px solid black;" />

## "Description" file

By opening the "**description.xml**" file you will see the following information :

````xml
<?xml version="1.0" encoding="UTF-8" ?>
<?xml-stylesheet href="css/description.css" type="text/css"?>
<description version="1.2" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">
    <reference-id revision="1">advancedTemplate</reference-id>
    <title>Advanced template</title>
    <description>Form description</description>
    <referentials>
        <referential id="fruits">referentials/fruits.xml</referential>
    </referentials>
    <translations>
        <for lang="fr">translations/fr.po</for>
    </translations>
    <progress step-id="context" />
    <steps>
        <step id="context" data="0-context/context.xml" icon="question" />
        <step id="data" data="1-data/data.xml" icon="pencil" />
        <step id="attachment" data="2-attachment/data-generated.xml" preprocess="2-attachment/preprocess.xml" postprocess="2-attachment/postprocess.xml" icon="paperclip" />
        <step id="review" data="3-review/data-generated.xml" preprocess="3-review/preprocess.xml" icon="thumbs-o-up" />
        <step id="finished" data="4-finish/greetings.xml" icon="trophy" />
    </steps>
</description>
````

The three first lines are used to describe the xml environment used by developers. You don't have to change them.

In the **&lt;reference-id>** line, you have to change the current information **>advancesTemplate<** with the following one : **>SCN/GQ/QPxxx/PExx<**. (*For exemple, for the PE01 (elementary procedure number 1) of the QP001 (Professional qualification of Mountain guide*), you will write :  
**&lt;refence-id revision="1">SCN/GQ/QP001/PE01&lt;/reference-id>**.) This tag is mandatory as well as the path SCN/GQ/ who should not be change. This path is used to save the created PDF files.

In the **&lt;title>** line, you can change the title **>Advanced Template<** by any text you want. This title will be shown on the "Forms" window (column "Name") and on the "Record" window (column "Title") of the forms manager application. This line is mandatory.

In the **&lt;description>** line you can change the current information **>Form description<**. This information will be shown in the "Home" window of the forms manager application. The &lt;description> tag is optional.

The **&lt;referentials>** tag is used to declare all referential that will be used in the "Data" step. This tag is optional.

The **&lt;translations>** tag is used to declare the files that will be used to translate the forms. This tag is optional.

The **&lt;progess>** tag is used to declare the first step the tool will execute. This tag is mandatory.

The **&lt;steps>** tag is mandatory and is used to list the different steps used to elaborate the final PDF file. You don't have to modify this section. Each step is described in the following sections of this document.


If you want to change the icon for each step (not necessary), you can follow the following instructions :

1/ In order to find an icon, go to the <a href="http://fontawesome.io/icons/">"font awesome" web site</a>.
<br /><img alt="Font awesome" src="../images/spec/tutorial/font-awesome-1.png" style="height: 300px; border: 1px solid black;" />

2/ Scroll down to find an icon you like. It will be the icon of the new step. For example, you can use "paperclip".
<br /><img alt="Font awesome" src="../images/spec/tutorial/font-awesome-2.png" style="height: 300px; border: 1px solid black;" />

3/ Type "paperclip" in the "icon" attribute of your "&lt;step&gt;".
<br /><img alt="File data main icon" src="../images/spec/tutorial/file-data-main-icon-2.png" style="height: 300px; border: 1px solid black;" />


You could find more detailed information about tags and sub-tags used in each step in the  <a href="reference.html">reference</a> documentation.


## "Data" step

The "Data" step is used to design a form to collect data.
The "data.xml" file is in the "data" sub-folder.

Example of content of "data.xml" :

````xml
<?xml version="1.0" encoding="UTF-8" ?>
<?xml-stylesheet href="../css/data.css" type="text/css"?>
<form id="dataform-id" label="This is the data step." xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://svn.projet-ge.fr:8080/archiva/repository/xsd/form-manager/form-manager/1.2/form-manager-1.2.xsd">

  <description>"data" step description</description>
  <help>"data" step help</help>
    <warning>"data" step warning</warning>

  <default>
    <data mandatory="true" type="String" />
  </default>

  <group id="dataGroup-id" label="Data group">
    <group id="identity" label="Your identity">
      <description>This is the identity group description.</description>
      <data id="lastname" label="Your last name" />
      <data id="firstname" label="Your first name" />
    </group>

    <group id="refs" label="Referentials">
      <data id="refExample" label="Referential example" type="ComboBox(ref:'fruits')" />
    </group>
  </group>

</form>
````

The two first lines are used to describe the xml environment used by developers. You don't have to change them.

In the **&lt;form>** tag you can change the "**id**". This "id" should be unique.  
You can change the "**label**" too. This label will be shown in the breadcrumb when  the final user will use the form.

The **&lt;warning>** and **&lt;help>** tags are used to display warnings and helps to the final user. You can add warnings and helps at any level : form, group and data.

The **&lt;default>** tag is used to select default information for "type" and "mandatory". For example **&lt;default>&lt;data mandatory="true" type="String" />&lt;/default>** means that all &lt;data> tags without information about the type and the mandatory of the data are initialized with type = String and mandatory=true.

The **&lt;group>** tag is used to organize the data presentation. The level 1 group allow to present data in a new page, its label is shown in the breadcrumb. The level 2 group allow to present data on a different section in the same page, its label is shown on the top of the section.

The **&lt;data>** tag is used to ask final user to enter the required informations on the form. The collected data have an id (each id should be unique), a label (the information who is shown on the form), a type (string, date, ...) and an information about the mandatory character or not (true or false).

In order to easily copy-paste in the file some configured fields, go to the forms manager application and click on the “Types” sub-menu of the “Devs” menu :
<br /><img alt="App menu types" src="../images/spec/tutorial/app-menu-types.png" style="border: 1px solid black;" /> 

A screen with the list of all available field types is displayed.
<br /><img alt="App screen types" src="../images/spec/tutorial/app-screen-types-1.png" style="height: 300px; border: 1px solid black;" />

Change the page of the types table to find the “String” type and click on it. You find a description of the type, its options, some XML to put in a form and the resulting view of a “String” type with the current options.
<br /><img alt="App screen types" src="../images/spec/tutorial/app-screen-types-2.png" style="height: 300px; border: 1px solid black;" />

You can change the options, as follows, and click on the "Copy" button. It will put the XML code in your clipboard.
<br /><img alt="App screen types" src="../images/spec/tutorial/app-screen-types-3.png" style="height: 300px; border: 1px solid black;" />

Then you can past the XML code in your clipboard in the Data.xml file.
<br /><img alt="File data main fields" src="../images/spec/tutorial/file-data-main-fields.png" style="height: 300px; border: 1px solid black;" />

## "Attachment" step

The "**Attachment**" step is used to allow attachments upload.
All files used for this step are in the "attachment" sub-folder : "preprocess.xml, "preprocess.js", "types.xml" and  postprocess.xml.

The "**preprocess.xml**" file contains the following information. An "input" file and a "script" file are referenced :
<br /><img alt="File attachment preprocess" src="../images/spec/tutorial/file-attachment-preprocess.png" style="height: 300px; border: 1px solid black;" />

You don't have to change the contents of this file.

The "**postprocess.xml**" file contains the following information. It will convert the attachments to the PDF format.
<br /><img alt="File attachment postprocess" src="../images/spec/tutorial/file-attachment-postprocess.png" style="height: 300px; border: 1px solid black;" />

You don't have to change the contents of this file.

The "**types.xml**" file contains the following information :

````xml
<?xml version="1.0" encoding="UTF-8" ?>
<?xml-stylesheet href="../css/types.css" type="text/css"?>
<form xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://www.ge.fr/schema/form-manager-1.2.xsd">
  <type id="pjID" label="Merci {{$labelPJID}}. Les documents suivants doivent être joints à la demande (en format .PDF .JPG ou .PNG) :">
         <description>Copie de la pièce d’identité.</description>
    </type>
  <type id="pjJustificatifExercice">
    <description>Copie de justificatif d'activité.</description>
  </type>
  <type id="pjAssurance">
    <description>Copie d'attestation d'assurance.</description>
  </type>
</form>
````

The three first lines : &lt;?xml version>, &lt;?xml-stylesheet> and &lt;form xmlns> should not be changed.

Each &lt;type&gt; tag is an attachment type, with a label and a description. Add or delete &lt;type> lines as needed (one for each attachment file) and change the id's and labels (there should be one &lt;type> tag by attachment needed). Each "id" should be unique. The labels used in this file will be shown to the final user as well as the &lt;description> or &lt;help> information.

The "$labelPJID" is a variable initialized in the "preprocess.js" file.

The "**preprocess.js**" file will add an attachment field to the form. It contains the following information :

````xml
var user = $dataform-id.dataGroup-id.identity;
attachment('pjID', 'pjID', {labelPJID: user.civilite + ' ' + user.nomDeclarant + ' '+ user.prenomDeclarant,mandatory:"false"});
attachment('pjJustificatifExercice', 'pjJustificatifExercice',{ mandatory:"false"});
attachment('pjAssurance', 'pjAssurance');
````

There should be one line for each &lt;type> tag listed in the types.xml file.

The "labelPJID" variable (used in the types.xml file) is initialized with the title, last name and first name variables, previously defined in the data.xml file. The path should be specified to help the "preprocess.js" file to find these variables in the data.xml file : $form-id.group1-id.group2-id.data-id. To avoid rewriting this path several times it's possible to initialize it on a variable, using the "var user =" command.

By default, each attachment file is mandatory. You have to add the following command { mandatory:"false"} in the attachment line to make it optional.


## "Review" step

This step is used to allow the review of the generated PDF.
The "review" sub-folder contains a "models" sub-folder and two files : "preprocess.xml" and "preprocess.js".

The "**models**" sub-folder should contains the fillable PDF in which all necessary fields have been created. 

The "**preprocess.xml**" file contains the following information :
<br /><img alt="File review preprocess" src="../images/spec/tutorial/file-review-preprocess.png" style="height: 300px; border: 1px solid black;" />

You don't have to change the contents of this file.

The "**preprocess.js**" file contains the following information :

````xml
var cerfaFields = {};

cerfaFields['lastname']  = $dataform-id.dataGroup-id.identity.lastname;
cerfaFields['firstname'] = $dataform-id.dataGroup-id.identity.firstname;

var cerfa = pdf.create('models/name of the fillable PDF file.pdf', cerfaFields);
var cerfaPdf = pdf.save('name of the output PDF file.pdf', cerfa);

return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Formulaires générés',
        data : [ spec.createData({
            id : 'cerfa14004',
            label : 'Cerfa 14004*2',
            description : 'Cerfa obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]}) ]}) ]});
````

The lines ("var cerfaFields", "cerfaFields" and "var cerfa") could be copy/paste by following these additional instructions : 

1/ Go to the forms manager application and click on the "PDF" sub-menu of the "Devs" menu.
<br /><img alt="App menu PDF" src="../images/spec/tutorial/app-menu-pdf.png" style="border: 1px solid black;" />

2/ A screen with an upload field is displayed.
<br /><img alt="App screen PDF" src="../images/spec/tutorial/app-screen-pdf-1.png" style="height: 300px; border: 1px solid black;" />

3/ Select your fillable PDF in the upload field. You get a list of all the PDF fields, a preview of the PDF and some Javascript code.
<br /><img alt="App screen PDF" src="../images/spec/tutorial/app-screen-pdf-2.png" style="height: 300px; border: 1px solid black;" />

4/ Scroll down and click on the "Copy" button. It will put the Javascript code in your clipboard.
<br /><img alt="App screen PDF" src="../images/spec/tutorial/app-screen-pdf-3.png" style="height: 300px; border: 1px solid black;" />

5/ Paste the Javascript code in the "preprocess.js" file :
<br /><img alt="File review preproces js" src="../images/spec/tutorial/file-review-preproces-js-1.png" style="height: 300px; border: 1px solid black;" />

The "preprocess.js" file should contains one "cerfaFields" line for each input field created in the PDF file. You should now link each PDF field with the corresponding variable specified in the data.xml file, by specifying the path (form-id.group-id.group-id.data-id). 

For exemple, for the "lastname" and "firstname" fields of the fillable PDF, you will have :

````xml
cerfaFields['lastname']  = $dataform-id.dataGroup-id.identity.lastname;
cerfaFields['firstname'] = $dataform-id.dataGroup-id.identity.firstname;
````

The following lines are used to specify the input file (the fillable PDF stored in the "models" sub-folder : be sure the names are the same), and the output file :

````xml
var cerfa = pdf.create('models/name of the fillable PDF file.pdf', cerfaFields);
var cerfaPdf = pdf.save('name of the output PDF file.pdf', cerfa);
````

The following code is used to create a form configuration file for the "review" step, on the fly :

````xml
return spec.create({
    id : 'review',
    label : 'Write here the label used in each step and shown in the breadcrumb trail',
    groups : [
````

The following code is then used to create a "generated" group in the step :

````xml
groups : [ spec.createGroup({
        id : 'generated',
        label : 'Formulaire généré',
        data : [
````

And the following code is used to create a "generated" field in the group, which refers to the output PDF and has a field type allowing the form user to view it :

````xml
data : [ spec.createData({
            id : 'generated',
            label : 'Form name ',
            description : 'Formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]}) ]}) ]});
````

The "id" and "label" could be changed, as well as the "description".

## "Finished" step

This section is used to design a form step to thank the user for completing the form.

The "finished" sub-folder contains the following files : "greetings.xml", "preprocess.js" and "preprocess.xml".

The "**greetings.xml**" file contains the following information :

````xml
<?xml version="1.0" encoding="UTF-8" ?>
<?xml-stylesheet href="../css/data.css" type="text/css"?>
<form id="finish" label="Demande de certificat professionnel" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://www.ge.fr/schema/form-manager-1.2.xsd">

    <default>
        <data mandatory="true" type="String" />
    </default>

    <group id="thanks" label="Remerciements ...">
        <description>Merci d'avoir traité cette formalité sur Guichet-Qualifications. Votre dossier sera transmis à l'autorité compétente pour traitement.</description>
    </group>

</form>
````

You can change the label and the description.

The "**preprocess.js**" file contains the following information :

````xml
return spec.create({
    id : 'finish',
    label : 'Remerciements ...',
    groups : [ spec.createGroup({
        id : 'thanks',
        description: 'Merci d\'avoir utilisé le nouveau gestionnaire de formalité !'
    }) ]
});
````

You can change the label and the description.

And the "**preprocess.xml**" file contains the following information :

````xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet href="../css/process.css" type="text/css"?>
<processes id="finish" xmlns="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ge.fr/schema/1.2/form-manager http://www.ge.fr/schema/form-manager-1.2.xsd">
    <process id="preprocess" type="script" script="preprocess.js" />
</processes>
````

You don't have to change this file.


## Upload your form

In this section, we will upload the whole form to the forms manager.

Go to your unzipped folder, select all the files and create a zip.
<br /><img alt="Folder root" src="../images/spec/tutorial/folder-root-2.png" style="height: 300px; border: 1px solid black;" />

In order to upload your finished form, go to the forms manager application and click on the "Forms" menu.
<br /><img alt="App menu forms" src="../images/spec/tutorial/app-menu-forms.png" style="border: 1px solid black;" />

A screen with the list of all the uploaded forms is displayed.
<br /><img alt="App screen forms" src="../images/spec/tutorial/app-screen-forms-1.png" style="height: 300px; border: 1px solid black;" />

Select your zipped file in the upload field. The screen is refreshed and your form appears in the list. You just have to click on the flag to make it public.
<br /><img alt="App screen forms" src="../images/spec/tutorial/app-screen-forms-2.png" style="height: 300px; border: 1px solid black;" />

The zipped form resulting of this tutorial can be downloaded <a href="../templates/tutorial_template.zip">here</a>.