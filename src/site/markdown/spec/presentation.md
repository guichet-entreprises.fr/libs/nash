
# Presentation

v1.2 - 19/10/2016

## What is a form ?

<img alt="Display" src="../images/spec/display.png" style="height: 300px; border: 1px solid black;" />

A form is a set of web form pages allowing to type informations, in order to achieve an administrative process. Each step of the form is composed of fields groups. Each of these groups is displayed on a different web form page. The forms manager allows to specify the steps sequence and the order of the fields they contain. Forms are self-supporting so, once a form is uploaded, it is totally independent from other forms.

<img alt="Overview" src="../images/spec/overview.png" />

## Tutorial

You will find a step by step tutorial <a href="tutorial.html">here</a>.

## Tools

In order to assist you during the development of a specification, there is a <a href="tools/index.html">documentation on development tools</a>.

## Development

A form is usually composed of several folders, one for each step. A step has a data file for configuring <a href="reference.html#Data_collecting">data gathering</a> and may also have a <a href="reference.html#Preprocesspostprocess">preprocess and/or postprocess</a> configuration file. <a href="reference.html">A complete reference</a> of the structure of these configuration files is available. You can start with the <a href="../templates/tutorial_template.zip"> tutorial form template</a> to  begin with a form which already has several steps.

In order to edit your configuration files, you will need any text editor allowing to type <a href="https://en.wikipedia.org/wiki/XML">XML</a>. For example, you can use Notepad++ or <a href="https://eclipse.org/downloads/">Eclipse</a>.

## Deployment

Once the specification development is finished, it can be deployed on the forms manager. The process of deployment is described <a href="deployment.html">here</a>.
