
# Expressions

Les expressions permettent de conditionner l'affichage des champs d'une formalité.

Dans le cas suivant, le champ `carColor` sera affiché si et seulement si le champ `hasCar` est renseigné avec la valeur `vrai` :

        ...
        <data id="hasCar" type="Boolean" />
        <data id="carColor" type="String" if="$hasCar" />
        ...


## Contexte d'exécution

| Objet   | Description                |
| ------- | -----------                |
| context | Contexte du dossier        |
| form    | Dossier en cours de saisie |

## Objets et propriétés

Propriétés courantes dans le groupe (notation relative) :

    $lastname

    $address.city

Propriétés globales (notation absolue) :

    $form.lastname

    $form.address.city

## Opérateurs

Opérateur       | Description
----------------|------------
$a == $b        | Egal
$a != $b        | Différent
$a < $b         | Strictement inférieur
$a <= $b        | Inférieur ou égal
$a > $b         | Strictement supérieur
$a >= $b        | Supérieur ou égal
!(...)          | Négation
$cond ? $a : $b | Opérateur ternaire

## Fonctions sur les listes

### `.find(...)`

Recherche du premier élément dans la liste correspondant au critère.

Paramètre :

*   `fn` : fonction de comparaison prenant en paramètre l'élément en cours et la position de celui-ci dans la liste.

L'exemple suivant renvoie le premier de la liste dans la propriété `id` est paire :

    $table.find(row -> (row.id % 2) == 0)


### `.filter(...)`

Filtrage des éléments dans la liste suivant la fonction de comparaison passée en paramètre.

Paramètre :
* `fn` : fonction de comparaison prenant en paramètre l'élément en cours et la position de celui-ci dans la liste.


L'exemple suivant renvoie tous les éléments de la liste dans la propriété `id` est paire :

    $table.filter(row -> (row.id % 2) == 0)

