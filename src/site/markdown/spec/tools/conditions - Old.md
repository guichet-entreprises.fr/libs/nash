
# Expressions

v1.1 - 20/10/2016

Expressions allow to condition the display of a form fields.

In the following case, the `carColor` field will be displayed if and only if the `hasCar` field is filled with the value `true` :

        ...
        <data id="hasCar" type="Boolean" />
        <data id="carColor" type="String" if="$hasCar" />
        ...


## Execution context

| Object  | Description         |
| ------- | ------------------- |
| context | Record context      |
| form    | Record being filled |

## Objects and properties

Current properties in the group (relative notation) :

    $lastname

    $address.city

Global properties (absolute notation) :

    $form.lastname

    $form.address.city

## Operators

Operator             | Description
---------------------|------------
`$a == $b`         | Equal
`$a != $b`         | Different
`$a < $b`          | Strictly inferior
`$a <= $b`         | Inferior or equal
`$a > $b`          | Strictly superior
`$a >= $b`         | Superior or equal
`$a or $b`         | logical OR
`$a and $b`        | logical AND
`not $cond`        | Negation
`$cond ? $a : $b` | Ternary operator

Operators can be regrouped using parentheses :

    (null == $a or $a == $b) and (null == $c or $c == $d)

Will not have the same value as :

    null == $a or ($a == $b and null == $c) or $c == $d

