
# Tools

v1.1 - 19/10/2016

By going to the administration console of the forms manager, the menu "Devs" is available. Several sub-menus allow to select tools, in order to ease the creation of specifications.

* The <a href="conditions.html">"Rules" tool</a> allows to test fields display conditions.

<img alt="Expressions" src="../../images/spec/tools/expressions.png" style="height: 300px; border: 1px solid black;" />

* The "Types" tool and its gallery allow to list all the available field types in a given version, have a preview and copy the <a href="../reference.html#Data_collecting">"&lt;data&gt;" tags</a> corresponding to the desired fields.

<img alt="Types" src="../../images/spec/tools/types.png" style="height: 300px; border: 1px solid black;" />

* The "PDF" tool allows to find the fields of a fillable PDF and copy the fields filling Javascript template, in order to <a href="../reference.html#Script">implement a process of type "script"</a>. After copying the template, you just have to complete it like this :

    cerfaFields['cerfa1FieldName'] = $stepId.formField1Group.formField1Name;<br />
    cerfaFields['cerfa2FieldName'] = $stepId.formField2Group.formField2Name;

<img alt="PDF" src="../../images/spec/tools/pdf.png" style="height: 300px; border: 1px solid black;" />
