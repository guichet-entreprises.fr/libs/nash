
# Expressions

v1.2 - 04/05/2017

Expressions allow to condition the display of a form fields.

Conditions in the data.xml file could be write in two different ways :

=> in the data tag :

````xml
     <data id="dataID" type="String" if="condition" />
````

=> or using the following syntax :

````xml
     <data id="dataID" type="String">
        <if>condition</if>
     </data>
````

Conditions on Boolean, BooleanCheckBox and BooleanComboBox type fields :

````xml
     <$questionBoolean> or <$questionBoolean == true> <!--Will return true if the "questionBoolean" is filled with 'true'-->
     
     <$questionBoolean == false> <!--Will return true if the "questionBoolean" is filled with 'false'-->
````

Conditions on CheckBoxes, ComboBox, ComboBoxMultiple and Radios type fields :

````xml
      <data id="idBox" type="CheckBoxes"> <!--or ComboBox, or ComboBoxMultiple, or Radios-->
        <conftype>
          <referential>
            <text id="1">choice 1</text>
            <text id="2">choice 2</text>
          </referential>
        </conftype>
      </data>

      <Value('id').of($idBox).contains('1')"> <!--Will test if the choice 1 is checked-->
````


## Execution context

| Object  | Description         |
| ------- | ------------------- |
| context | Record context      |
| form    | Record being filled |

## Objects and properties

Current properties in the group (relative notation) :

    $lastname

    $address.city

Global properties (absolute notation) :

    $form.lastname

    $form.address.city

## Operators

Operator             | Description
---------------------|------------
`$a == $b`         | Equal
`$a != $b`         | Different
`$a < $b`          | Strictly inferior
`$a <= $b`         | Inferior or equal
`$a > $b`          | Strictly superior
`$a >= $b`         | Superior or equal
`$a or $b`         | logical OR
`$a and $b`        | logical AND
`not $cond`        | Negation
`$cond ? $a : $b` | Ternary operator

Operators can be regrouped using parentheses :

    (null == $a or $a == $b) and (null == $c or $c == $d)

Will not have the same value as :

    null == $a or ($a == $b and null == $c) or $c == $d

