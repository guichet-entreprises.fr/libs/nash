
# Tutorial

v1.3 - 14/11/2016

## Install your environment without auto-completion

In this section, we will install a light environment, which is faster to install.

Go to the <a href="https://notepad-plus-plus.org/fr/">Notepad++ web site</a> and click on the "download" link.
<br /><img alt="Notepad++ site" src="../images/spec/tutorial/notepad-site.png" style="height: 300px; border: 1px solid black;" />

Select the 32-bit or 64-bit package and click on the "download" button.
<br /><img alt="Notepad++ download" src="../images/spec/tutorial/notepad-download-1.png" style="height: 300px; border: 1px solid black;" />

Save the installer where you prefer.
<br /><img alt="Notepad++ download" src="../images/spec/tutorial/notepad-download-2.png" style="height: 300px; border: 1px solid black;" />

Once downloaded, double-click on the installer and choose the installation folder.
<br /><img alt="Notepad++ installation folder" src="../images/spec/tutorial/notepad-installation-folder.png" style="height: 300px; border: 1px solid black;" />

Check everything and select only your language in the first profile screen.
<br /><img alt="Notepad++ profile" src="../images/spec/tutorial/notepad-profile-1.png" style="height: 300px; border: 1px solid black;" />

Uncheck everything in the second profile screen and click on the "install" button.
<br /><img alt="Notepad++ profile" src="../images/spec/tutorial/notepad-profile-2.png" style="height: 300px; border: 1px solid black;" />

Once the installation is finished, check the "Run" checkbox before clicking on the "finish" button or use the Notepad++ shortcut.
<br /><img alt="Notepad++ installation finished" src="../images/spec/tutorial/notepad-installation-finished.png" style="height: 300px; border: 1px solid black;" />

Go to the plugin manager.
<br /><img alt="Notepad++ plugin manager" src="../images/spec/tutorial/notepad-plugin-manager-1.png" style="height: 300px; border: 1px solid black;" />

Select the "XML tools" and click on the "Install" button. This will restart Notepad++.
<br /><img alt="Notepad++ plugin manager" src="../images/spec/tutorial/notepad-plugin-manager-2.png" style="height: 300px; border: 1px solid black;" />

Without auto-completion, you need to validate your document structure. Use XML tools "Validate now" option, use the CTRL+ALT+SHIFT+M shortcut, or enable the auto-validation which triggers when you save your file, to raise document structure errors.
<br /><img alt="Notepad++ plugin manager" src="../images/spec/tutorial/notepad-validate-xsd.png" style="height: 300px; border: 1px solid black;" />
